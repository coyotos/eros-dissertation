/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#define STRICT
#include <Windows.h>

int WINAPI WinMain(HINSTANCE, HINSTANCE, PSTR pCmdLine, int) {

	if ((int) ShellExecute(NULL, NULL, pCmdLine,
		NULL, NULL, SW_SHOWNORMAL) <= 32) {

		TCHAR sz[1024];
		wsprintf(sz, "Unable to start HTML browser.\n"
			"For a guide to this CD, open \"index.html\" by hand.");
		MessageBox(NULL, sz, TEXT("Starting CD"), MB_OK|MB_ICONWARNING);
	}

	return 0;
}
