OPTS=""
# OPTS="$OPTS -print-size"
# OPTS="$OPTS -T"   # generate TRANS.TBL files
# OPTS="$OPTS -R"   # generate SUSP and RR records for rock ridge -- replaces -R
OPTS="$OPTS -r"     # generate *better* SUSP and RR records for rock ridge
OPTS="$OPTS -f"     # follow symbolic links
#                   # -- replaces -R

mkhybrid -A EROS_v1_0 -copyright $1/Copyright -l -J -L \
    -o NEW-CD \
    -P 'Extremely Reliable OS Group, http://www.eros-os.org' \
    -p 'Jonathan S. Shapiro' -V 'EROS v1.0' -no-mac-files -no-desktop \
    -hide-joliet $1/Copyright \
    -hide-joliet $1/Manifest \
    -hide-hfs $1/'*'.txt \
    -hide-hfs $1/autorun.inf \
    -hide $1/'*'.txt \
    -hide $1/autorun.inf \
    -v -v $OPTS $1
