/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <memory.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include <a.out.h>

#include <sys/fcntl.h>
#include <sys/stat.h>

#include <disk/DiskNode.hxx>
#include <disk/PagePot.hxx>
#include <disk/DiskCkpt.hxx>
#include <erosimg/App.hxx>
#include <erosimg/Volume.hxx>
#include <erosimg/DiskDescrip.hxx>
#include <erosimg/ExecImage.hxx>
#include <eros/Reserve.h>
#include <new.h>

#define BOOT "/eros/lib/boot/boot"
#define min(x,y) ((x) > (y) ? (y) : (x))

#define RESERVE_BYTES (MAX_CPU_RESERVE * sizeof(CpuReserveInfo))
#define RESERVES_PER_PAGE (EROS_PAGE_SIZE / sizeof(CpuReserveInfo))
#define RESERVE_PAGES ((RESERVE_BYTES + EROS_PAGE_SIZE - 1) / EROS_PAGE_SIZE)

void
Volume::InitVolume()
{
  working_fd = -1;
  target_fd = -1;
  topDiv = 0;
  topLogLid = 0;
  lastAvailLogLid = 0;
  firstAvailLogLid = (2 * EROS_OBJECTS_PER_FRAME);

  dskCkptHdr0 = (DiskCheckpoint*) new uint8_t[EROS_PAGE_SIZE];
  dskCkptHdr1 = (DiskCheckpoint*) new uint8_t[EROS_PAGE_SIZE];
  curDskCkpt = dskCkptHdr0;
  oldDskCkpt = dskCkptHdr1;
  
  rewriting = true;

  ckptDir = 0;
  maxCkptDirent = 0;
  nCkptDirent = 0;

  threadDir = 0;
  maxThreadDirent = 0;
  nThreadDirent = 0;

  reserveTable = new CpuReserveInfo[MAX_CPU_RESERVE];
  bzero(reserveTable, sizeof(CpuReserveInfo) * MAX_CPU_RESERVE);
  
  // Following just for now...
  for (int i = 0; i < STD_CPU_RESERVE; i++) {
    reserveTable[i].index = i;
    reserveTable[i].normPrio = i;
    reserveTable[i].rsrvPrio = -2;
  }
  for (int i = STD_CPU_RESERVE; i < MAX_CPU_RESERVE; i++) {
    reserveTable[i].index = i;
    reserveTable[i].normPrio = -2;
    reserveTable[i].rsrvPrio = -2;
  }
  
  needSyncDivisions = false;
  needSyncHdr = false;
  needSyncCkptLog = false;
  needDivInit = false;

  curLogPotLid = UNDEF_LID;
}

Volume::Volume()
{
  InitVolume();
#if 0
  divTable = (Division *) divPage;
  badmap = (BadEnt*) &divTable[NDIVENT];
#endif
}

Volume::~Volume()
{
  Close();
}

CpuReserveInfo
Volume::GetReserve(uint32_t ndx)
{
  if (ndx >= MAX_CPU_RESERVE)
    Diag::fatal(1, "Reserve %d is out of range\n");
  
  return reserveTable[ndx];
}

void
Volume::SetReserve(const CpuReserveInfo& cri)
{
  if (cri.index >= MAX_CPU_RESERVE)
    Diag::fatal(1, "Reserve %d is out of range\n");
  
  reserveTable[cri.index] = cri;
}

bool
Volume::Read(uint32_t offset, void *buf, uint32_t sz)
{
  assert(working_fd >= 0);

  if (::lseek(working_fd, (int) offset, SEEK_SET) < 0)
    return false;

  if (::read(working_fd, buf, (int) sz) != (int) sz)
    return false;

  return true;
}

bool
Volume::Write(uint32_t offset, const void *buf, uint32_t sz)
{
  assert(working_fd >= 0);

  if (::lseek(working_fd, (int) offset, SEEK_SET) < 0)
    return false;

  if (::write(working_fd, buf, (int) sz) != (int) sz)
    return false;

  return true;
}


// This version finds an empty spot big enough for the division and puts it
// there.
int
Volume::AddDivision(DivType type, uint32_t sz, const OID& oid)
{
  uint32_t nObFrames = sz / EROS_PAGE_SECTORS;
  OID endOid;

  switch (type) {
  case dt_Kernel:
  case dt_Object:
    nObFrames -= (nObFrames / PAGES_PER_PAGE_CLUSTER);

    if (nObFrames < 1)
      Diag::fatal(1, "Range too small\n");

    if (nObFrames % PAGES_PER_PAGE_CLUSTER)
      nObFrames -= 1;		// at least one page pot

    if (nObFrames < 1)
      Diag::fatal(1, "Range too small\n");

    // Take out one more for the first page, which is used to capture
    // seqno of most recent checkpoint.
    nObFrames--;

    endOid = oid + (nObFrames * EROS_OBJECTS_PER_FRAME);
    break;

  case dt_Log:
    endOid = oid + (nObFrames * EROS_OBJECTS_PER_FRAME);
    break;

  default:
    Diag::fatal(1, "Attempt to set OID on inappropriate division type\n");
    break;
  }
  
  if ((type != dt_Log) && (oid % EROS_OBJECTS_PER_FRAME))
    Diag::fatal(1, "Starting OID for range must be multiple of %d\n",
		EROS_OBJECTS_PER_FRAME);
    
  int div = AddAdjustableDivision(type, sz);
  
  if (type == dt_Log && endOid > topLogLid) {
    topLogLid = endOid;
    lastAvailLogLid = endOid;
  }
    
  divTable[div].startOid = oid;
  divTable[div].endOid = endOid;

  Diag::printf("Division %d: %6d %6d [%6s] OID=[",
	       div, divTable[div].start, divTable[div].end,
	       divTable[div].TypeName());
  Diag::print(divTable[div].startOid);
  Diag::printf(", ");
  Diag::print(divTable[div].endOid);
  Diag::printf(")\n");
  
  return div;
}

// This version finds an empty spot big enough for the division and puts it
// there.
int
Volume::AddDivision(DivType type, uint32_t sz)
{
  switch (type) {
  case dt_Object:
  case dt_Log:
    Diag::fatal(1, "Attempt to create %s division without OID\n",
	      Division::TypeName(type));
    break;
  default:
    break;
  }
  
  int div = AddAdjustableDivision(type, sz);
  Diag::printf("Division %d: %6d %6d [%6s]\n",
	       div, divTable[div].start, divTable[div].end,
	       divTable[div].TypeName());

  return div;
}

int
Volume::AddAdjustableDivision(DivType type, uint32_t sz)
{
  int i;
  uint32_t start = 0;

  assert(working_fd >= 0);
  
      // make sure we do not collide with another division:
  for (i = 0; i < topDiv; i++) {
    uint32_t end = start + sz;
	// if some other division starts or ends in our proposed range,
	// go to the end of the colliding division:
    if ((divTable[i].start >= start && divTable[i].start < end) ||
	(divTable[i].end > start && divTable[i].end <= end)) {
      start = divTable[i].end;
      i = -1;			// restart for loop
    }
  }
  
  int div = DoAddDivision(type, start, sz);

  return div;
}

int
Volume::AddFixedDivision(DivType type, uint32_t start, uint32_t sz)
{
  int div = DoAddDivision(type, start,  sz);
  
  Diag::printf("Division %d: %6d %6d [%6s]\n",
	       div, start, start+sz, divTable[div].TypeName());

  return div;
}

// The versions that follow add a division at a fixed location:
int
Volume::DoAddDivision(DivType type, uint32_t start, uint32_t sz)
{
  assert(working_fd >= 0);
  
  if (topDiv >= NDIVENT)
    Diag::fatal(4, "Too many divisions\n");
  
  uint32_t end = start + sz;
  
  // Need to verify that this division does not overlap some other
  // division if it has nonzero size
  
  int i;
  if (sz) {
    for (i = 0; i < topDiv; i++) {
      if ((divTable[i].start != divTable[i].end) &&
	  (start >= divTable[i].start && start < divTable[i].end) ||
	  (end > divTable[i].start && end <= divTable[i].end))
	Diag::fatal(4, "Division range error\n");
    }
  }
  
  int div = topDiv;
  topDiv++;
  
  divTable[div].type = type;
  divTable[div].start = start;
  divTable[div].end = end;
  divTable[div].startOid = 0;
  divTable[div].endOid = 0;
  
  if (type == dt_DivTbl) {
    // Perfectly okay to have lots of division tables, but only two
    // end up in the volume header:
    
    // assert(!volHdr.AltDivTable);
    if (!volHdr.DivTable)
      volHdr.DivTable = start;
    else
      volHdr.AltDivTable = start;
  }

  needSyncHdr = 1;		// if nothing else, volume size changed
  
#if 0
  if (type == dt_Spare)
    spareDivNdx = div;
#endif
  
  divNeedsInit[div] = true;
  needDivInit = true;

#if 0
  switch (type) {
  case dt_Swap:
  case dt_Page:
  case dt_Node:
  case dt_Log:
    divNeedsInit[div] = true;
    needDivInit = true;
    break;
  default:
    divNeedsInit[div] = false;
    break;
  }
#endif

  needSyncDivisions = true;
  
  return div;
}

void
Volume::FormatObjectDivision(int ndx)
{
  assert(working_fd >= 0);

  if (ndx >= topDiv)
    Diag::fatal(1, "Attempt to format nonexistent division\n");
  
  Division& d = divTable[ndx];
  
  for (OID oid = d.startOid; oid < d.endOid; oid += EROS_OBJECTS_PER_FRAME) {
    VolPagePot pagePot;
    ReadPagePotEntry(oid, pagePot);
    pagePot.type = FRM_TYPE_ZDPAGE;
    pagePot.count = 0;
    WritePagePotEntry(oid, pagePot);
    oid++;
  }

  // Set up a first page with suitable ckpt sequence number:
  uint8_t buf[EROS_PAGE_SIZE];
  uint64_t *seqNo = (uint64_t *) buf;

  *seqNo = 1;

  Write(d.start * EROS_SECTOR_SIZE, buf, EROS_PAGE_SIZE);
}

// Once the log division has been zeroed, we simply need to write
// suitable checkpoint header pages:
void
Volume::FormatLogDivision(int ndx)
{
  assert(working_fd >= 0);

  if (ndx >= topDiv)
    Diag::fatal(1, "Attempt to format nonexistent division\n");
  
  Division& d = divTable[ndx];
  
  if (d.startOid == 0) {
    oldDskCkpt->sequenceNumber = 0;
    oldDskCkpt->hasMigrated = true;
    oldDskCkpt->maxLogLid = topLogLid;
    oldDskCkpt->nRsrvPage = 0;
    oldDskCkpt->nThreadPage = 0;
    oldDskCkpt->nDirPage = 0;

    curDskCkpt->sequenceNumber = 1;
    curDskCkpt->hasMigrated = false;
    curDskCkpt->maxLogLid = topLogLid;
    curDskCkpt->nRsrvPage = 0;
    curDskCkpt->nThreadPage = 0;
    curDskCkpt->nDirPage = 0;

    WriteLogPage((lid_t)0, (uint8_t *) dskCkptHdr0);
    WriteLogPage((lid_t)(1*EROS_OBJECTS_PER_FRAME), (uint8_t *) dskCkptHdr1);
  }

  needSyncCkptLog = true;
}

void
Volume::ZeroDivision(int ndx)
{
  assert(working_fd >= 0);

  if (ndx >= topDiv)
    Diag::fatal(1, "Attempt to zero nonexistent division\n");
  
  uint8_t buf[EROS_SECTOR_SIZE];
  bzero(buf, EROS_SECTOR_SIZE);
  
  Division& d = divTable[ndx];
  
  for (uint32_t i = d.start; i < d.end; i++)
    if ( !Write(i * EROS_SECTOR_SIZE, buf, EROS_SECTOR_SIZE) )
      Diag::fatal(1, "Couldn't zero division %d\n", ndx);

  divNeedsInit[ndx] = false;
}

void
Volume::SetVolFlag(VolHdr::Flags flag)
{
  volHdr.BootFlags |= flag;
  needSyncHdr = 1;
}

void
Volume::ClearVolFlag(VolHdr::Flags flag)
{
  volHdr.BootFlags &= ~flag;
  needSyncHdr = 1;
}

void
Volume::SetIplKey(const DiskKey& key)
{
  volHdr.iplKey = key;
  
  volHdr.BootFlags |= VolHdr::VF_IPL;
  needSyncHdr = 1;
}

void
Volume::SetIplSysId(uint64_t dw)
{
  volHdr.iplSysId = dw;

  needSyncHdr = 1;
}

void
Volume::WriteKernelImage(int div, const ExecImage& image)
{
  ZeroDivision(div);
  FormatObjectDivision(div);
  divNeedsInit[div] = false;
  
  assert(working_fd >= 0);
  
  if (div >= topDiv)
    Diag::fatal(1, "Attempt to write image %s on nonexistent division\n",
		image.GetName().str());

  if (image.NumRegions() != 1)
    Diag::fatal(1, "Image %s has inappropriate number of regions\n",
		image.GetName().str());

  const ExecRegion& er = image.GetRegion(0);

  // If the image was built correctly, it should start at a page boundary:
  if (er.vaddr % EROS_PAGE_SIZE)
    Diag::fatal(1, "ELF Image %s has start vaddr at nonpage boundary\n",
		image.GetName().str());

  uint32_t bytes = er.filesz;
  const uint8_t *fileImage = image.GetImage();
  fileImage += er.offset;
  
  Division& d = divTable[div];
  OID oid = d.startOid;

  uint32_t divPages = (d.endOid - d.startOid) / EROS_OBJECTS_PER_FRAME;
  if (divPages * EROS_PAGE_SIZE < er.filesz)
    Diag::fatal(1, "Image \"%s\" (%d bytes) will not fit in division %d (%d pages)\n",
		image.GetName().str(), bytes, div, divPages);

  while (bytes) {
    uint8_t buf[EROS_PAGE_SIZE];
    
    memset(buf, 0, EROS_PAGE_SIZE);
    memcpy(buf, fileImage, min (bytes, EROS_PAGE_SIZE));
    WriteDataPage(oid, buf);

    fileImage += EROS_PAGE_SIZE;
    bytes -= min(bytes, EROS_PAGE_SIZE);
    oid += EROS_OBJECTS_PER_FRAME;
  }
}

// NOTE!!! This only works for dense images, such as the kernel and
// bootstrap code.  Domain images MUST be handled through the
// DomainImage class.  Offset is in bytes.
void
Volume::WriteImageAtDivisionOffset(int div, const ExecImage& image,
				   uint32_t offset)
{
  assert(working_fd >= 0);
  
  if (div >= topDiv)
    Diag::fatal(1, "Attempt to write image %s on nonexistent division\n",
		image.GetName().str());

  if (image.NumRegions() != 1)
    Diag::fatal(1, "Image %s has inappropriate number of regions\n",
		image.GetName().str());

  const ExecRegion& er = image.GetRegion(0);

  // If the image was built correctly, it should start at a page boundary:
  if (er.vaddr % EROS_PAGE_SIZE)
    Diag::fatal(1, "ELF Image %s has start vaddr at nonpage boundary\n",
		image.GetName().str());
    
  uint32_t secs = er.filesz / EROS_SECTOR_SIZE;
  if (er.filesz % EROS_SECTOR_SIZE)
    secs++;
  
  uint32_t imageSz = secs * EROS_SECTOR_SIZE;
  
  // The following copy is necessary because the file itself may
  // contain garbage or other stuff at the tail of the alleged region,
  // so we must zero-extend to the end of the sector by hand.
  
  uint8_t *imageBuf = new uint8_t[imageSz];
  bzero(imageBuf, imageSz);

  const uint8_t *fileImage = image.GetImage();
  
  memcpy(imageBuf, &fileImage[er.offset], er.filesz);

  Division& d = divTable[div];
  
  uint32_t divStart = d.start * EROS_SECTOR_SIZE;
  uint32_t divEnd   = d.end * EROS_SECTOR_SIZE;

  if (divStart + offset + imageSz > divEnd)
    Diag::fatal(1, "Image \"%s\" will not fit in division %d\n",
		image.GetName().str(), div);

  Write(divStart + offset, imageBuf, imageSz);
  
  delete [] imageBuf;

  divNeedsInit[div] = false;
}

void
Volume::DelDivision(int ndx)
{
  int i;
  assert(working_fd >= 0);

  if (ndx >= topDiv)
    Diag::fatal(1, "Attempt to delete nonexistent division\n");

  switch (divTable[ndx].type) {
  case dt_Boot:
  case dt_DivTbl:
      Diag::fatal(1, "Division type %s cannot be deleted\n",
		divTable[ndx].TypeName());
      break;
  break;
  case dt_Spare:
    // Can only delete spare division if no spare sectors are
    // in use.
    break;
  }

  for (i = ndx+1; i < topDiv; i++)
    divTable[i-1] = divTable[i];
  divTable[i].type = dt_Unused;

  topDiv--;
  needSyncHdr = 1;		// volume size may have changed
}

void
Volume::WriteBootImage(const char *bootName)
{
  assert(working_fd >= 0);

  if (bootName) {
    ExecImage bootImage;
    if ( !bootImage.SetImage(bootName) ) {
      App.SetExitValue(1);
      App.Exit();			// diagnostics generated by SegImage.
    }

    WriteImageAtDivisionOffset(0, bootImage, 0);
  }
  else {
    ZeroDivision(0);
  }
  
  uint8_t buf[EROS_PAGE_SIZE];

  if (!Read(0, buf, EROS_PAGE_SIZE))
    Diag::fatal(1, "Unable to read boot sector\n");
  
  // copy leading jump into volume header
  memcpy(volHdr.code, buf, sizeof(volHdr.code));

  // Recompute the image size from the division table:
  volHdr.VolSectors = 0;
  if (bootName)
    volHdr.BootFlags |= VolHdr::VF_BOOT;
  else
    volHdr.BootFlags &= ~VolHdr::VF_BOOT;
  
  for (int i = 0; i < topDiv; i++) {
    if (divTable[i].end > volHdr.VolSectors)
      volHdr.VolSectors = divTable[i].end;
  }
  
  // put the volume header on the boot sector
  memcpy(buf, &volHdr, sizeof(volHdr));

  if (!Write(0, buf, EROS_PAGE_SIZE))
    Diag::fatal(1, "Unable to read boot sector\n");

  needSyncHdr = 0;
}

int
Volume::Create(const char* targname, const char* bootName)
{
#if 0
  if (!pDisk)
    Diag::fatal(2, "Disk model undefined\n");
#endif
  
  assert(working_fd == -1);
  assert(target_fd == -1);
  
  InitVolume();
  
  volHdr.HdrVersion = VOLHDR_VERSION;
  volHdr.PageSize = EROS_PAGE_SIZE;
  volHdr.DivTable = 0;
  volHdr.AltDivTable = 0;
  volHdr.BootFlags = 0;
  volHdr.BootSectors = DISK_BOOTSTRAP_SECTORS;
  volHdr.VolSectors = 0;
  volHdr.zipLen = 0;
  volHdr.iplKey = NumberKey(0);
  volHdr.iplSysId = 0ll;
  volHdr.signature[0] = 'E';
  volHdr.signature[1] = 'R';
  volHdr.signature[2] = 'O';
  volHdr.signature[3] = 'S';
  
  /* Open the target file */
  target_fd = ::open(targname, O_RDWR|O_TRUNC);

  if (target_fd < 0 && errno == ENOENT)
    target_fd = ::open(targname, O_RDWR|O_CREAT, 0666);

  if (target_fd < 0)
    return 0;
  
  /* Either way, target is now clobbered, so arrange to delete it if
     we screw up. */
  App.AddTarget(targname);
  
  /* We assume uncompressed until told otherwise */
  working_fd = target_fd;
  
  int i;
  for (i = 0; i < NDIVENT; i++)
    divNeedsInit[i] = false;

      // Add division table entries for standard items:
  AddFixedDivision(dt_Boot, 0, DISK_BOOTSTRAP_SECTORS);
  AddFixedDivision(dt_DivTbl, DISK_BOOTSTRAP_SECTORS,
		   EROS_PAGE_SECTORS); 

#if 0
  if (pDisk->badsecs)
    AddDivision(dt_Spare, pDisk->badsecs);
#endif

  // Must initialize divisions first, or writing boot page and
  // sync'ing division tables will fail!
  InitDivisions();
  
  // We write the boot page and division page immediately.  If
  // this fails, the disk is quite bad.
  
  WriteBootImage(bootName);
  
  SyncDivTables();
  
  return 1;
}

int
Volume::Open(const char* targname, bool rewriting)
{
  assert(working_fd == -1);
  assert(target_fd == -1);
  
  InitVolume();
  
  /* Open the target file descriptor -- we will do nothing to disturb
     its content until the call to Volume::Close() */
  if ((target_fd = ::open(targname, O_RDWR)) < 0)
    return 0;

  working_fd = target_fd;
  
  if (! Read(0, &volHdr, sizeof(volHdr)) )
    Diag::fatal(3, "Couldn't read volume header\n");

  DecompressTarget();
  
  needSyncHdr = 0;
  
  if ( !Read(EROS_SECTOR_SIZE * volHdr.DivTable,
	   &divTable, NDIVENT * sizeof(Division)) )
    Diag::fatal(3, "Couldn't read primary division table\n");

  needSyncDivisions = 0;
  needSyncCkptLog = 0;

  int i;
  for (i = 0; i < NDIVENT; i++)
    divNeedsInit[i] = false;

      // interpret the newly loaded tables:
  for (i = 0; i < NDIVENT; i++) {
    if (divTable[i].type == dt_Unused) {
      topDiv = i;
      break;
    }

    if (divTable[i].type == dt_Log &&
	divTable[i].endOid > topLogLid) {
      topLogLid = divTable[i].endOid;
      lastAvailLogLid = divTable[i].endOid;
    }
  }

  LoadLogHeaders();

  if (rewriting == false && topLogLid) {
    // We are opening for volume debugging rather than writing an
    // erosimage onto the volume.  We must therefore read in the
    // directory of the existing checkpoint log.

    LoadLogDirectory();
  }
  
  return 1;
}

void
Volume::LoadLogDirectory()
{
  assert(curDskCkpt);
  
  uint32_t logEnt = 0;

  // Read in the reserve table:
  for (uint32_t d = 0; d < curDskCkpt->nRsrvPage; d++, logEnt++) {
    uint8_t dirPage[EROS_PAGE_SIZE];

    ReadLogPage(curDskCkpt->dirPage[logEnt], dirPage);
    ReserveDirPage *rdp = (ReserveDirPage*) dirPage;

    for (uint32_t entry = 0; entry < rdp->nDirent; entry++) {
      CpuReserveInfo& cri = rdp->entry[entry];
	
      memcpy(&reserveTable[cri.index], &cri, sizeof(CpuReserveInfo));
    }
  }

  // Read the thread directory from the current checkpoint.
  for (uint32_t d = 0; d < curDskCkpt->nThreadPage; d++, logEnt++) {
    uint8_t dirPage[EROS_PAGE_SIZE];

    ReadLogPage(curDskCkpt->dirPage[logEnt], dirPage);
    ThreadDirPage *tdp = (ThreadDirPage*) dirPage;

    for (uint32_t entry = 0; entry < tdp->nDirent; entry++) {
      ThreadDirent& tde = tdp->entry[entry];
	
      AddThread(tde.oid, tde.allocCount, tde.schedNdx);
    }
  }

  // Read the object directory from the current checkpoint.
  if (curDskCkpt->hasMigrated == false) {
    for (uint32_t d = 0; d < curDskCkpt->nDirPage; d++, logEnt++) {
      uint8_t dirPage[EROS_PAGE_SIZE];

      ReadLogPage(curDskCkpt->dirPage[logEnt], dirPage);
      CkptDirPage *dp = (CkptDirPage*) dirPage;

      for (uint32_t entry = 0; entry < dp->nDirent; entry++) {
	CkptDirent& de = dp->entry[entry];
	
	AddDirent(de.oid, de.count, de.lid, de.type);
      }
    }
  }
}

lid_t
Volume::AllocLogPage()
{
  if (lastAvailLogLid <= firstAvailLogLid)
    Diag::fatal(3, "Log pages exhausted\n");

  lastAvailLogLid -= EROS_OBJECTS_PER_FRAME;

  return lastAvailLogLid;
}

lid_t
Volume::AllocLogDirPage()
{
  if (lastAvailLogLid <= firstAvailLogLid)
    Diag::fatal(3, "Log pages exhausted\n");

  lid_t theLid = firstAvailLogLid;
  firstAvailLogLid += EROS_OBJECTS_PER_FRAME;
  return theLid;
}

void
Volume::GrowCkptDir()
{
  AllocLogDirPage();

  maxCkptDirent += CkptDirPage::maxDirEnt;

  CkptDirent *newDir = new CkptDirent[maxCkptDirent];
  if (ckptDir)
    memcpy(newDir, ckptDir, nCkptDirent * sizeof(CkptDirent));

  delete [] ckptDir;
  ckptDir = newDir;
}

void
Volume::GrowThreadDir()
{
  AllocLogDirPage();

  maxThreadDirent += ThreadDirPage::maxDirEnt;

  ThreadDirent *newDir = new ThreadDirent[maxThreadDirent];
  if (threadDir)
    memcpy(newDir, threadDir, nThreadDirent * sizeof(ThreadDirent));

  delete [] threadDir;
  threadDir = newDir;
}

ThreadDirent*
Volume::LookupThread(OID oid)
{
  for (uint32_t i = 0; i < nThreadDirent; i++) {
    if ( threadDir[i].oid == oid )
      return &threadDir[i];
  }

  return 0;
}

CkptDirent*
Volume::LookupObject(OID oid)
{
  for (uint32_t i = 0; i < nCkptDirent; i++) {
    if ( ckptDir[i].oid == oid )
      return &ckptDir[i];
  }

  return 0;
}

void
Volume::AddDirent(OID oid, ObCount allocCount, lid_t lid, uint8_t ckObType)
{
  CkptDirent* cpd = LookupObject(oid);
  
  if (cpd == 0) {
    if (maxCkptDirent == nCkptDirent)
      GrowCkptDir();

    cpd = &ckptDir[nCkptDirent];
    nCkptDirent++;
  }

  cpd->oid = oid;
  cpd->count = allocCount;
  cpd->lid = lid;
  cpd->type = ckObType;

  needSyncCkptLog = true;
}

bool
Volume::AddThread(const OID& oid, ObCount allocCount, uint16_t rsrvNdx)
{
  ThreadDirent* tde = LookupThread(oid);
  if (tde == 0) {
    if (maxThreadDirent == nThreadDirent)
      GrowThreadDir();

    tde = &threadDir[nThreadDirent];
    nThreadDirent++;
  }

  tde->oid = oid;
  tde->allocCount = allocCount;
  tde->schedNdx = rsrvNdx;

  needSyncCkptLog = true;

  return true;
}

void
Volume::LoadLogHeaders()
{
  ReadLogPage(0, (uint8_t*) dskCkptHdr0);
  ReadLogPage(1*EROS_OBJECTS_PER_FRAME, (uint8_t*) dskCkptHdr1);

  if (dskCkptHdr0->sequenceNumber > dskCkptHdr1->sequenceNumber) {
    curDskCkpt = dskCkptHdr0;
    oldDskCkpt = dskCkptHdr1;
  }
  else {
    curDskCkpt = dskCkptHdr1;
    oldDskCkpt = dskCkptHdr0;
  }
}

void
Volume::SyncHdr()
{
  assert(working_fd >= 0);

  char buf[EROS_PAGE_SIZE];
  
  Read(0, buf, EROS_PAGE_SIZE);

  // Recompute the image size from the division table:
  volHdr.VolSectors = 0;
  for (int i = 0; i < topDiv; i++) {
    if (divTable[i].end > volHdr.VolSectors)
      volHdr.VolSectors = divTable[i].end;
  }
  
  memcpy(buf, &volHdr, sizeof(volHdr));

  Write(0, buf, EROS_PAGE_SIZE);

  needSyncHdr = 0;
}

void
Volume::SyncCkptLog()
{
  if (!needSyncCkptLog)
    return;
  
  assert(working_fd >= 0);

  assert((maxCkptDirent % CkptDirPage::maxDirEnt) == 0);
  assert((maxThreadDirent % ThreadDirPage::maxDirEnt) == 0);

  uint32_t dirPgCount = 0;
  uint32_t startDirPage = 0;
  uint32_t curDirLid = 2*EROS_OBJECTS_PER_FRAME;

  uint8_t dirPage[EROS_PAGE_SIZE];

  // Write out the reserve table:
  CpuReserveInfo *cri = reserveTable;
  uint32_t residual = MAX_CPU_RESERVE;
  startDirPage = dirPgCount;
  
  ReserveDirPage *rdp = (ReserveDirPage*) dirPage;

  while (residual) {
    uint32_t count = residual;
    if (count > ReserveDirPage::maxDirEnt)
      count = ReserveDirPage::maxDirEnt;

    rdp->nDirent = count;
    
    memcpy(rdp->entry, cri, sizeof(CpuReserveInfo) * count);
    WriteLogPage(curDirLid, dirPage);

    curDskCkpt->dirPage[dirPgCount] = curDirLid;

    curDirLid += EROS_OBJECTS_PER_FRAME;
    dirPgCount++;
    cri += count;
    residual -= count;
  }
	 
  curDskCkpt->nRsrvPage = dirPgCount - startDirPage;

  // Write out the thread directory:

  ThreadDirent* tde = threadDir;
  residual = nThreadDirent;
  startDirPage = dirPgCount;

  ThreadDirPage *tdp = (ThreadDirPage*) dirPage;

  while (residual) {
    uint32_t count = residual;
    if (count > ThreadDirPage::maxDirEnt)
      count = ThreadDirPage::maxDirEnt;

    tdp->nDirent = count;

    memcpy(tdp->entry, tde, sizeof(ThreadDirent) * count);
    WriteLogPage(curDirLid, dirPage);

    curDskCkpt->dirPage[dirPgCount] = curDirLid;

    curDirLid += EROS_OBJECTS_PER_FRAME;
    dirPgCount++;
    tde += count;
    residual -= count;
  }
  
  curDskCkpt->nThreadPage = dirPgCount - startDirPage;

  // Write out the object directory:

  CkptDirent* cpd = ckptDir;
  residual = nCkptDirent;
  startDirPage = dirPgCount;

  CkptDirPage *dp = (CkptDirPage*) dirPage;

  while (residual) {
    uint32_t count = residual;
    if (count > CkptDirPage::maxDirEnt)
      count = CkptDirPage::maxDirEnt;

    dp->nDirent = count;

    memcpy(dp->entry, cpd, sizeof(CkptDirent) * count);
    WriteLogPage(curDirLid, dirPage);

    curDskCkpt->dirPage[dirPgCount] = curDirLid;

    curDirLid += EROS_OBJECTS_PER_FRAME;
    dirPgCount++;
    cpd += count;
    residual -= count;
  }
  
  curDskCkpt->nDirPage = dirPgCount - startDirPage;

  if (curDskCkpt->nDirPage)
    curDskCkpt->hasMigrated = false;
  else
    curDskCkpt->hasMigrated = true;

  // I don't know which one is current -- just rewrite them both.
  WriteLogPage((lid_t)0, (uint8_t *) dskCkptHdr0);
  WriteLogPage((lid_t)1*EROS_OBJECTS_PER_FRAME, (uint8_t *) dskCkptHdr1);
  
  needSyncCkptLog = 0;
}

void
Volume::SyncDivTables()
{
  if (!needSyncDivisions)
    return;
  
  assert(working_fd >= 0);
  
  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    
    if (d.type == dt_DivTbl) {
      uint8_t buf[EROS_PAGE_SIZE];
      
      uint32_t divStart = d.start * EROS_SECTOR_SIZE;

      if ( !Read(divStart, buf, EROS_PAGE_SIZE) )
	Diag::fatal(1, "Unable to read division table\n");

      memcpy(buf, divTable, NDIVENT * sizeof(Division));
      
      if ( !Write(divStart, buf, EROS_PAGE_SIZE) )
	Diag::fatal(1, "Unable to write division table\n");
    }
  }

  needSyncDivisions = false;
}


void
Volume::InitDivisions()
{
  int i;

  for (i = 0; i < topDiv; i++)
    if (divNeedsInit[i]) {
      ZeroDivision(i);

      if (divTable[i].type == dt_Object || divTable[i].type == dt_Kernel)
	FormatObjectDivision(i);

      if (divTable[i].type == dt_Log)
	FormatLogDivision(i);

      divNeedsInit[i] = false;
    }

  needDivInit = false;
}

void
Volume::ResetVolume()
{
  // Used by sysgen to ensure that writing new data on an existing
  // volume causes no problems.

  for (int i = 0; i < topDiv; i++) {
    if (divTable[i].type == dt_Object)
      FormatObjectDivision(i);

    if (divTable[i].type == dt_Log)
      FormatLogDivision(i);
  }
}

void
Volume::Close()
{
  // If we are exiting on an error, there is no need to save anything,
  // since the file has been deleted.
  if (App.Aborting())
    return;
  
  InitDivisions();
  SyncHdr();
  SyncDivTables();
  SyncCkptLog();

  if (working_fd == -1)
    return;
  
  if (fsync(working_fd) == -1)
    Diag::fatal(1, "Final fsync failed with errno %d\n", errno);
  
  if (CompressTarget() < 0)
    Diag::fatal(1, "Unable to compress target volume -- errno %d\n", errno);
  
  ::close(target_fd);
  if (target_fd != working_fd)
    close(working_fd);
  
  target_fd = -1;
  working_fd = -1;
#if defined(__linux__) && 0
  sync();
  sleep(5);			// linux sync is busted - schedules
				// the writes but doesn't do them.
  sync();
  sleep(5);			// linux sync is busted - schedules
				// the writes but doesn't do them.
#endif
}

uint32_t
Volume::GetLogFrameVolOffset(int ndx, const OID& loc)
{
  const Division& d = divTable[ndx];

  assert(d.Contains(loc));

  uint32_t relPage = (uint32_t) (loc - d.startOid);
  relPage /= EROS_OBJECTS_PER_FRAME;
  uint32_t divStart = d.start * EROS_SECTOR_SIZE;
  uint32_t pageOffset = relPage * EROS_PAGE_SIZE;

  return divStart + pageOffset;
}
    
uint32_t
Volume::GetOidFrameVolOffset(int ndx, const OID& oid)
{
  const Division& d = divTable[ndx];

  assert(d.Contains(oid));

  assert ((d.startOid % EROS_OBJECTS_PER_FRAME) == 0);

  uint32_t relPage = (uint32_t) ((oid - d.startOid) / EROS_OBJECTS_PER_FRAME);
  relPage += (relPage / DATA_PAGES_PER_PAGE_CLUSTER);
  relPage += 1;			// clusters precede data

  relPage += 1;			// skip ckpt sequence pg
  
  uint32_t divStart = d.start * EROS_SECTOR_SIZE;
  uint32_t pageOffset = relPage * EROS_PAGE_SIZE;

  return divStart + pageOffset;
}
    
// Return true/false according to whether this is a valid object OID
// given the frame type:
bool
Volume::ValidOid(int ndx, const OID& oid)
{
  const Division& d = divTable[ndx];

  assert(d.Contains(oid));

  assert (d.type != dt_Log);

  uint32_t obOffset = (uint32_t) (oid % EROS_OBJECTS_PER_FRAME);

  VolPagePot frameInfo;
  GetPagePotInfo(oid, frameInfo);
  
  switch (frameInfo.type) {
  case FRM_TYPE_ZDPAGE:
  case FRM_TYPE_DPAGE:
  case FRM_TYPE_ZCPAGE:
  case FRM_TYPE_CPAGE:
    return (obOffset == 0) ? true : false;

  case FRM_TYPE_NODE:
    return (obOffset < DISK_NODES_PER_PAGE) ? true : false;
  default:
    return false;
  }
}

// given a division index and a OID, return the volume-relative
// offset of the object in that division.
uint32_t
Volume::GetOidVolOffset(int ndx, const OID& oid)
{
  const Division& d = divTable[ndx];

  assert(d.Contains(oid));

  assert (d.type != dt_Log);
  
  uint32_t frameStart = GetOidFrameVolOffset(ndx, oid);

  uint32_t obOffset = (uint32_t) (oid % EROS_OBJECTS_PER_FRAME);

  if (obOffset) {
    VolPagePot frameInfo;
    GetPagePotInfo(oid, frameInfo);
  
    switch (frameInfo.type) {
    case FRM_TYPE_ZDPAGE:
    case FRM_TYPE_DPAGE:
    case FRM_TYPE_ZCPAGE:
    case FRM_TYPE_CPAGE:
      obOffset *= EROS_PAGE_SIZE;
      break;
    case FRM_TYPE_NODE:
      obOffset *= sizeof(DiskNode);
      break;
    default:
      Diag::fatal(1, "Unknown object type tag!\n");
    }
  }

  if (obOffset >= EROS_PAGE_SIZE)
    Diag::fatal(1, "Bad OID (gives improper offset)\n");
    
  return frameStart + obOffset;
}

bool
Volume::GetPagePotInfo(const OID& oid, VolPagePot& pot)
{
  CkptDirent* cpd = LookupObject(oid);
    
  if (cpd) {
    pot.count = cpd->count;
    pot.type = cpd->type;

    return true;
  }
  
  return ReadPagePotEntry(oid, pot);
}

// Object I/O support:
bool
Volume::ReadDataPage(const OID& oid, uint8_t *buf)
{
  VolPagePot pp;

  if (oid % EROS_OBJECTS_PER_FRAME) {
    Diag::printf("Page OID must be multiple of %d (0x%x)\n",
		 EROS_OBJECTS_PER_FRAME,
		 EROS_OBJECTS_PER_FRAME);
    return false;
  }
  
  if ( GetPagePotInfo(oid, pp) == false ) {
    Diag::printf("Requested OID invalid\n");
    return false;
  }

  switch(pp.type) {
  case FRM_TYPE_ZDPAGE:
    memset(buf, 0, EROS_PAGE_SIZE);
    return true;
  case FRM_TYPE_DPAGE:
    break;
  default:
    Diag::printf("Requested OID not a page frame\n");
    return false;
  }
  
  CkptDirent* cpd = LookupObject(oid);
    
  if (cpd) {
    assert (CONTENT_LID(cpd->lid));
    assert (cpd->type != FRM_TYPE_ZDPAGE);
    
    if (ReadLogPage(cpd->lid, buf) == false)
      return false;

    return true;
  }

  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    
    if (((d.type == dt_Object) || (d.type == dt_Kernel)) && d.Contains(oid)) {
      if (ValidOid(div, oid) == false) {
	Diag::printf("Requested OID invalid\n");
	return false;
      }
      
      VolPagePot frameInfo;
      ReadPagePotEntry(oid, frameInfo);
      if ((frameInfo.type != FRM_TYPE_DPAGE) &&
	  (frameInfo.type != FRM_TYPE_ZDPAGE)) {
	Diag::printf("Non-page frame\n");
	return false;
      }
      
      uint32_t offset = GetOidVolOffset(div, oid);

      return Read(offset, buf, EROS_PAGE_SIZE);
    }
  }

  return false;
}

bool
Volume::WriteDataPage(const OID& oid, const uint8_t *buf)
{
  bool isZeroPage = true;

  for (uint32_t i = 0; i < EROS_PAGE_SIZE; i++)
    if (buf[i]) {
      isZeroPage = false;
      break;
    }
    
  CkptDirent* cpd = LookupObject(oid);
    
  if (cpd && cpd->type != FRM_TYPE_DPAGE && cpd->type != FRM_TYPE_ZDPAGE)
    return false;
  
  // If a location has already been fabricated, write it there even if
  // it is a zero page.
  if ( cpd && CONTENT_LID(cpd->lid) ) {
    if (WriteLogPage(cpd->lid, buf) == false)
      return false;

    return true;
  }
  
  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    
    if (((d.type == dt_Object) || (d.type == dt_Kernel)) && d.Contains(oid)) {
      VolPagePot frameInfo;
      ReadPagePotEntry(oid, frameInfo);

      assert ((oid % EROS_OBJECTS_PER_FRAME) == 0);

      uint8_t wantTag = isZeroPage ? FRM_TYPE_ZDPAGE : FRM_TYPE_DPAGE;
      if (frameInfo.type != wantTag) {
	Diag::debug(1, "Re-tagging frame for OID 0x%08x%08x to %d\n",
		    (uint32_t) (oid >> 32),
		    (uint32_t) (oid),
		    wantTag);
	frameInfo.type = wantTag;
	WritePagePotEntry(oid, frameInfo);
      }

      uint32_t offset = GetOidVolOffset(div, oid);

      if ( !Write(offset, buf, EROS_PAGE_SIZE) )
	Diag::fatal(5, "Volume write failed at offset %d.\n", offset);

      return true;
    }
  }
  
  if (rewriting == false)
    return false;
  
  // If we get here, we didn't find what we wanted.  Allocate space
  // for this page in the checkpoint log.

  Diag::fatal(1, "Cannot write page with OID 0x%08x%08x -- no home location\n",
	      (uint32_t)(oid>>32), (uint32_t)oid);
  
  lid_t lid = ZERO_LID;
  uint8_t type = FRM_TYPE_ZDPAGE;

  if (isZeroPage == false) {
    lid = AllocLogPage();
    type = FRM_TYPE_DPAGE;
  
    if (WriteLogPage(lid, buf) == false)
      return false;
  }
  
  AddDirent(oid, 0, lid, type);

  return true;
}

// FIX: This has a bad bug, in that it checks the page pot before it
// checks the log entry.
bool
Volume::ReadCapPage(const OID& oid, DiskKey *kbuf)
{
  VolPagePot pp;

  if (oid % EROS_OBJECTS_PER_FRAME) {
    Diag::printf("Cap Page OID must be multiple of %d (0x%x)\n",
		 EROS_OBJECTS_PER_FRAME,
		 EROS_OBJECTS_PER_FRAME);
    return false;
  }
  
  if ( GetPagePotInfo(oid, pp) == false ) {
    Diag::printf("Requested OID invalid\n");
    return false;
  }

  switch(pp.type) {
  case FRM_TYPE_ZCPAGE:
    for (unsigned i = 0; i < EROS_PAGE_SIZE/sizeof(DiskKey); i++)
      kbuf[i] = DiskKey();
    return true;
  case FRM_TYPE_CPAGE:
    break;
  default:
    Diag::printf("Requested OID not a cpage frame\n");
    return false;
  }
  
  CkptDirent* cpd = LookupObject(oid);
    
  if (cpd) {
    assert (CONTENT_LID(cpd->lid));
    assert (cpd->type != FRM_TYPE_ZCPAGE);
    
    if (ReadLogPage(cpd->lid, (uint8_t *) kbuf) == false)
      return false;

    return true;
  }

  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    
    if (((d.type == dt_Object) || (d.type == dt_Kernel)) && d.Contains(oid)) {
      if (ValidOid(div, oid) == false) {
	Diag::printf("Requested OID invalid\n");
	return false;
      }
      
      VolPagePot frameInfo;
      ReadPagePotEntry(oid, frameInfo);
      if ((frameInfo.type != FRM_TYPE_CPAGE) &&
	  (frameInfo.type != FRM_TYPE_ZCPAGE)) {
	Diag::printf("Non-cpage frame\n");
	return false;
      }
      
      uint32_t offset = GetOidVolOffset(div, oid);

      return Read(offset, kbuf, EROS_PAGE_SIZE);
    }
  }

  return false;
}

bool
Volume::WriteCapPage(const OID& oid, const DiskKey *kbuf)
{
  bool isZeroPage = true;

  for (uint32_t i = 0; i < (EROS_PAGE_SIZE/sizeof(DiskKey)); i++) {
    if (kbuf[i].IsZeroKey() == false) {
      isZeroPage = false;
      break;
    }
  }
    
  CkptDirent* cpd = LookupObject(oid);
    
  if (cpd &&
      cpd->type != FRM_TYPE_CPAGE &&
      cpd->type != FRM_TYPE_ZCPAGE
      )
    return false;
  
  // If a location has already been fabricated, write it there even if
  // it is a zero page.
  if ( cpd && CONTENT_LID(cpd->lid) ) {
    if (WriteLogPage(cpd->lid, (uint8_t *) kbuf) == false)
      return false;

    return true;
  }
  
  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    
    if (((d.type == dt_Object) || (d.type == dt_Kernel)) && d.Contains(oid)) {
      VolPagePot frameInfo;
      ReadPagePotEntry(oid, frameInfo);

      assert ((oid % EROS_OBJECTS_PER_FRAME) == 0);

      uint8_t wantTag = isZeroPage ? FRM_TYPE_ZCPAGE : FRM_TYPE_CPAGE;
      if (frameInfo.type != wantTag) {
	Diag::debug(1, "Re-tagging frame for OID 0x%08x%08x to %d\n",
		    (uint32_t) (oid >> 32),
		    (uint32_t) (oid),
		    wantTag);
	frameInfo.type = wantTag;
	WritePagePotEntry(oid, frameInfo);
      }

      uint32_t offset = GetOidVolOffset(div, oid);

      if ( !Write(offset, kbuf, EROS_PAGE_SIZE) )
	Diag::fatal(5, "Volume write failed at offset %d.\n", offset);

      return true;
    }
  }
  
  if (rewriting == false)
    return false;
  
  // If we get here, we didn't find what we wanted.  Allocate space
  // for this page in the checkpoint log.

  Diag::fatal(1, "Cannot write page with OID 0x%08x%08x -- no home location\n",
	      (uint32_t)(oid>>32), (uint32_t)oid);
  
  lid_t lid = ZERO_LID;
  uint8_t type = FRM_TYPE_ZCPAGE;

  if (isZeroPage == false) {
    lid = AllocLogPage();
    type = FRM_TYPE_CPAGE;
  
    if (WriteLogPage(lid, (uint8_t *)kbuf) == false)
      return false;
  }
  
  AddDirent(oid, 0, lid, type);

  return true;
}

// Log I/O support:
bool
Volume::ReadLogPage(const lid_t lid, uint8_t *buf)
{
  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    
    if (d.type == dt_Log && d.Contains(lid)) {
      uint32_t offset = GetLogFrameVolOffset(div, lid);

      return Read(offset, buf, EROS_PAGE_SIZE);
    }
  }

  return false;
}

bool
Volume::WriteLogPage(const lid_t lid, const uint8_t *buf)
{
  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    
    if (d.type == dt_Log && d.Contains(lid)) {
      uint32_t offset = GetLogFrameVolOffset(div, lid);

      if ( !Write(offset, buf, EROS_PAGE_SIZE) )
	Diag::fatal(5, "Volume write failed at offset %d.\n", offset);
    }
  }

  return true;
}

// given a division index and a OID, return the volume-relative
// offset of the page pot for that OID
uint32_t
Volume::GetOidPagePotVolOffset(int ndx, const OID& oid)
{
  const Division& d = divTable[ndx];
  assert(d.type == dt_Object);
  assert(d.Contains(oid));
  
  assert ((d.startOid % EROS_OBJECTS_PER_FRAME) == 0);
  
  uint32_t relPage = (uint32_t) ((oid - d.startOid) / EROS_OBJECTS_PER_FRAME);
  uint32_t whichCluster = (uint32_t) (relPage / DATA_PAGES_PER_PAGE_CLUSTER);
  uint32_t pagePotFrame = whichCluster * PAGES_PER_PAGE_CLUSTER;

  pagePotFrame ++;		// for ckpt sequence number pg

  uint32_t pageOffset = pagePotFrame * EROS_PAGE_SIZE;
  uint32_t divStart = d.start * EROS_SECTOR_SIZE;
  
  return divStart + pageOffset;
}

bool
Volume::ReadPagePotEntry(const OID& oid, VolPagePot& pagePot)
{
  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    uint8_t data[EROS_PAGE_SIZE];
    
    if ( d.type == dt_Object && d.Contains(oid) ) {
      bool result;

      assert ((d.startOid % EROS_OBJECTS_PER_FRAME) == 0);

      uint32_t offset = GetOidPagePotVolOffset(div, oid);

      result = Read(offset, data, EROS_PAGE_SIZE);
      if (result == false)
	return result;

      uint32_t relPage = (uint32_t) ((oid - d.startOid) / EROS_OBJECTS_PER_FRAME);
      
      PagePot* pp = (PagePot *) data;
      uint32_t potEntry = relPage % DATA_PAGES_PER_PAGE_CLUSTER;
      pagePot.type = pp->type[potEntry];
      pagePot.count = pp->count[potEntry];

      return true;
    }
  }

  return false;
}

bool
Volume::WritePagePotEntry(const OID& oid, const VolPagePot& pagePot)
{
  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    uint8_t data[EROS_PAGE_SIZE];
    
    if ( d.type == dt_Object && d.Contains(oid) ) {
      bool result;

      assert ((d.startOid % EROS_OBJECTS_PER_FRAME) == 0);

      uint32_t offset = GetOidPagePotVolOffset(div, oid);

      result = Read(offset, data, EROS_PAGE_SIZE);
      if (result == false)
	return result;

      uint32_t relPage = (uint32_t) ((oid - d.startOid) / EROS_OBJECTS_PER_FRAME);
      
      PagePot* pp = (PagePot *) data;
      uint32_t potEntry = relPage % DATA_PAGES_PER_PAGE_CLUSTER;
      pp->type[potEntry] = pagePot.type;
      pp->count[potEntry] = pagePot.count;

      if ( !Write(offset, data, EROS_PAGE_SIZE) )
	Diag::fatal(5, "Volume write failed at offset %d.\n", offset);

      return true;
    }
  }

  return false;
}

bool
Volume::ReadNode(const OID& oid, DiskNode& node)
{
  CkptDirent* ccd = LookupObject(oid);
    
  if (ccd && ccd->type == FRM_TYPE_NODE && ccd->lid != UNDEF_LID) {
    uint8_t logPage[EROS_PAGE_SIZE];
    DiskNode* logPot = (DiskNode *) logPage;

    assert (CONTENT_LID(ccd->lid));
    
    if (ReadLogPage(ccd->lid, logPage) == false)
      return false;

    for (uint32_t i = 0; i < DISK_NODES_PER_PAGE; i++) {
      if (logPot[i].oid == oid) {
	memcpy(&node, &logPot[i], sizeof(DiskNode));
	return true;
      }
    }

    return false;
  }
  
  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    
    if (d.type == dt_Object && d.Contains(oid)) {
      if (ValidOid(div, oid) == false) {
	Diag::printf("Requested OID invalid\n");
	return false;
      }
      
      VolPagePot frameInfo;
      ReadPagePotEntry(oid, frameInfo);
      if (frameInfo.type != FRM_TYPE_NODE) {
	Diag::printf("Non-node frame\n");
	return false;
      }
      
      uint32_t offset = GetOidVolOffset(div, oid);

      return Read(offset, &node, sizeof(DiskNode));
    }
  }

  return false;
}

bool
Volume::WriteNodeToLog(const OID& oid, const DiskNode& node)
{
  assert (oid == node.oid);
  
  if (curLogPotLid == UNDEF_LID) {
    curLogPotLid = AllocLogPage();
  }

  uint8_t logPage[EROS_PAGE_SIZE];
  DiskNode* logPot = (DiskNode *) logPage;

  if (ReadLogPage(curLogPotLid, logPage) == false)
    return false;

  uint8_t ndx = curLogPotLid % EROS_OBJECTS_PER_FRAME;
  
  memcpy(&logPot[ndx], &node, sizeof(DiskNode));

  if (WriteLogPage(curLogPotLid, logPage) == false)
    return false;

  AddDirent(oid, 0, curLogPotLid, FRM_TYPE_NODE);
  curLogPotLid++;
  
  if (curLogPotLid % EROS_OBJECTS_PER_FRAME == DISK_NODES_PER_PAGE)
    curLogPotLid = UNDEF_LID;

  return true;
}

bool
Volume::WriteNode(const OID& oid, const DiskNode& node)
{
  CkptDirent* ccd = LookupObject(oid);
    
  if (ccd && ccd->type != FRM_TYPE_NODE)
    return false;
  
  if (ccd && CONTENT_LID(ccd->lid)) {
    uint8_t logPage[EROS_PAGE_SIZE];
    DiskNode* logPot = (DiskNode *) logPage;

    if (ReadLogPage(ccd->lid, logPage) == false)
      return false;

    for (uint32_t i = 0; i < DISK_NODES_PER_PAGE; i++) {
      if (logPot[i].oid == oid) {
	memcpy(&logPot[i], &node, sizeof(DiskNode));

	if (WriteLogPage(ccd->lid, logPage) == false)
	  return false;

	return true;
      }
    }

    return false;
  }
  
  for (int div = 0; div < topDiv; div++) {
    Division& d = divTable[div];
    
    if (d.type == dt_Object && d.Contains(oid)) {
      VolPagePot frameInfo;
      ReadPagePotEntry(oid, frameInfo);

      if (frameInfo.type != FRM_TYPE_NODE) {
	Diag::debug(1, "Re-tagging frame for OID 0x%08x%08x to %d\n",
		    (uint32_t) (oid >> 32),
		    (uint32_t) (oid),
		    FRM_TYPE_NODE);

	OID frameOID = oid & ~(EROS_OBJECTS_PER_FRAME - 1u);

	if ( ! FormatNodeFrame(div, frameOID) )
	  Diag::fatal(5, "Unable to format node frame 0x%08x%08x\n",
		      (uint32_t) (frameOID >> 32),
		      (uint32_t) frameOID);

	frameInfo.type = FRM_TYPE_NODE;
	WritePagePotEntry(oid, frameInfo);
      }

      uint32_t offset = GetOidVolOffset(div, oid);

      if ( !Write(offset, &node, sizeof(DiskNode)) )
	Diag::fatal(5, "Volume write failed at offset %d.\n", offset);

      return true;
    }
  }

  if (rewriting == false)
    return false;
  
  // If we get here, we didn't find what we wanted.  Allocate space
  // for this node in the checkpoint log.

  Diag::fatal(1, "Cannot write node with OID 0x%08x%08x -- no home location\n",
	      (uint32_t)(oid>>32), (uint32_t)oid);
  if ( !WriteNodeToLog(oid, node) )
    Diag::fatal(5, "Volume write to log failed\n");

  return true;
}

	
bool
Volume::FormatNodeFrame(int div, OID frameOID)
{
  char buf[EROS_PAGE_SIZE];
  bzero (buf, EROS_PAGE_SIZE);
	
  DiskNode *pdn = (DiskNode *) buf;
	
  for (int nd = 0; nd < EROS_NODES_PER_FRAME; nd++) {
    pdn[nd].oid = frameOID + nd;
    for (uint32_t slot = 0; slot < EROS_NODE_SIZE; slot++)
      pdn[nd][slot] = NumberKey(0);
  }

  uint32_t offset = GetOidVolOffset(div, frameOID);

  return Write(offset, buf, EROS_PAGE_SIZE);
}

bool
Volume::ContainsNode(const OID& oid)
{
  CkptDirent* ccd = LookupObject(oid);
  if (ccd && ccd->type == FRM_TYPE_NODE)
    return true;
  
  VolPagePot frameInfo;
  if (ReadPagePotEntry(oid, frameInfo) == false)
    return false;
  
  if (frameInfo.type != FRM_TYPE_NODE)
    return false;
  
  return true;
}

bool
Volume::ContainsPage(const OID& oid)
{
  CkptDirent* ccd = LookupObject(oid);
  if (ccd && (ccd->type == FRM_TYPE_DPAGE || ccd->type == FRM_TYPE_ZDPAGE))
    return true;
  
  VolPagePot frameInfo;
  if (ReadPagePotEntry(oid, frameInfo) == false)
    return false;
  
  if (frameInfo.type != FRM_TYPE_DPAGE && frameInfo.type != FRM_TYPE_ZDPAGE)
    return false;
  
  return true;
}
