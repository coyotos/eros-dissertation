/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <erosimg/Diag.hxx>
#include <erosimg/App.hxx>

int Diag::debuglvl = 0;

void
Diag::printf(const char* msg, ...)
{
  va_list ap;

  va_start(ap, msg);
    
  vfprintf(stdout, msg, ap);

  va_end(ap);
}

void
Diag::print(OID oid)
{
  Diag::printf("0x%08x%08x", (uint32_t) (oid >> 32), (uint32_t) oid);
}

void
Diag::print(ObCount count)
{
  Diag::printf("0x%08x", count);
}

void
Diag::debug(int lvl, const char* msg, ...)
{
  if (lvl && lvl > debuglvl)
    return;
  
  va_list ap;

  va_start(ap, msg);
    
  if (lvl)
    fprintf(stderr, "%s: L%-3d ", App.name(), lvl);

  vfprintf(stderr, msg, ap);

  va_end(ap);
}

void
Diag::fatal(int code, const char* msg, ...)
{
  va_list ap;

  va_start(ap, msg);
    
  fprintf(stderr, "%s: fatal ", App.name());
  vfprintf(stderr, msg, ap);

  App.SetExitValue(code);
  App.Exit();

  va_end(ap);
}

void
Diag::error(int code, const char* msg, ...)
{
  va_list ap;

  va_start(ap, msg);
    
  fprintf(stderr, "%s: error ", App.name());
  vfprintf(stderr, msg, ap);

  App.SetExitValue(code);
  
  if (App.IsInteractive() == false)
    App.Exit();
  
  va_end(ap);
}

