/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <eros/target.h>
#include <eros/Key.h>
#include <disk/DiskKey.hxx>
#include <erosimg/Diag.hxx>

#define DEF_MISCKEY(name) #name,
#define OLD_MISCKEY(name) "OBSOLETE" #name,

static const char *MiscKeyNames[MiscKeyType::NKEYS] = {
#include <disk/MiscKey.def>
};

void
PrintDiskKey(const DiskKey& key)
{
  switch(key.GetType()) {
  case KtNumber:
    Diag::printf("KtNum(0x%08x %08x %08x)", key.nk.value[2],
		 key.nk.value[1], key.nk.value[0]);
    break;
  case KtDataPage:
    Diag::printf("KtDataPage(OID=");
    Diag::print(key.unprep.oid);
    Diag::printf(",blss=%x", key.GetBlss());
    if (key.IsPrepared())
      Diag::printf(",P");
    if (key.IsReadOnly())
      Diag::printf(",RO");
    Diag::printf(")");
    break;
  case KtCapPage:
    Diag::printf("KtCapPage(OID=");
    Diag::print(key.unprep.oid);
    Diag::printf(",blss=%x", key.GetBlss());
    if (key.IsPrepared())
      Diag::printf(",P");
    if (key.IsReadOnly())
      Diag::printf(",RO");
    Diag::printf(")");
    break;
  case KtNode:
  case KtSegment:
    {
      char *typeName = 0;
      if (key.GetType() == KtNode)
	typeName = "KtNode";
      else
	typeName = "KtSegment";

      Diag::printf("%s(OID=", typeName);
      Diag::print(key.unprep.oid);
      if (key.GetBlss())
	Diag::printf(",blss=%d", key.GetBlss());
      if (key.IsNoCall())
	Diag::printf(",NC");
      if (key.IsReadOnly())
	Diag::printf(",RO");
      if (key.IsWeak())
	Diag::printf(",WK");
      Diag::printf(")");
      break;
    }
  case KtProcess:
    Diag::printf("KtDom(OID=");
    Diag::print(key.unprep.oid);
    Diag::printf(")");
    break;
  case KtTimer:
    Diag::printf("KtTimer(???)");
    break;
  case KtSched:
    Diag::printf("KtSched(prio=%d)", key.subType);
    break;
  case KtRange:
    {
      OID start = key.rk.oid;
      OID top = key.rk.oid + key.rk.count;
      if (key.subType == 1)
	top = UINT64_MAX;
      
      Diag::printf("KtRange(OID=");
      Diag::print(start);
      Diag::printf(":");
      Diag::print(top);
      Diag::printf(")");
      break;
    }
  case KtDevice:
    Diag::printf("KtDev(ty=%d)", key.subType);
    break;
  case KtMisc:
    Diag::printf("KtMisc(ty=%s)", MiscKeyNames[key.subType]);
    break;
  case KtStart:
    Diag::printf("KtStart(OID=");
    Diag::print(key.unprep.oid);
    Diag::printf(",data=%d)", key.keyData);
    break;
  case KtResume:
    Diag::printf("KtResume(OID=");
    Diag::print(key.unprep.oid);
    Diag::printf(")");
    break;
  }
}
