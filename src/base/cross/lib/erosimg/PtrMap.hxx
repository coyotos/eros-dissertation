#ifndef __PTRMAP_HXX__
#define __PTRMAP_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Versions of the non-inline routines exist in the library to map
// pointers to Words, to uint64_ts, to DiskKeys, and to void *

template <class Value> class PtrMap {
  struct vw {
    const void *ptr;
    Value value;
  } * ptrs;
  unsigned nPtrs;
  unsigned maxPtrs;

  void do_add(const void *ptr, const Value& value);

public:
  PtrMap()
  {
    ptrs = 0;
    nPtrs = 0;
    maxPtrs = 0;
  }
  ~PtrMap();

  bool Contains(const void *ptr);
  bool Lookup(const void *ptr, Value& value);

  void Reset()
  {
    nPtrs = 0;
  }

  void Add(const void *ptr, const Value& value)
  {
    if (!Contains(ptr))
      do_add(ptr, value);
  }

  unsigned Size()
  {
    return nPtrs;
  }
} ;

#endif // __PTRMAP_HXX__
