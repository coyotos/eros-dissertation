#ifndef __EXECIMAGE_HXX__
#define __EXECIMAGE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Helper class to encapsulate the different binary formats we need to
// be prepared to deal with.  This way the rest of the eros library
// doesn't need to know the formatting details.

#include <eros/target.h>
#include <erosimg/ExecArch.hxx>
#include <erosimg/Intern.hxx>

// If the ExecRegion structure looks suspiciously similar to the
// equivalent structure in the ELF binary format, that's because it
// is. Since I helped design ELF, this is no surprise.  The heart of
// the problem here is that the amount of flexibility inherent in ELF
// is considerable, so no lesser way of capturing the binary image
// will suffice in ExecImage.  The good news is that we need only pay
// attention to the program headers - the section headers are for our
// purposes irrelevant.
//

struct ExecRegion {
  uint32_t vaddr;
  uint32_t memsz;
  uint32_t filesz;
  uint32_t offset;
  enum {			// same as ELF, which is not by accident!
    ER_X = 0x1,
    ER_W = 0x2,
    ER_R = 0x4,
  };
  uint32_t perm;
}; 

class ExecImage {
  const char *imageTypeName;
  InternedString name;

  uint8_t* image;
  uint32_t imgsz;

  ExecRegion* regions;
  uint32_t nRegions;
  
  bool InitElf();
  bool InitAout();
  
  uint32_t  entryPoint;
  
  void ResetImage();
  
public:
  ExecImage();
  ~ExecImage();

#if 0
  ExecArch::Architecture GetArchitecture() const;
#endif

  bool SetImage(const InternedString& imageName);
  
  InternedString GetName() const
  { return name; }

  const char* ImageTypeName() const
  { return imageTypeName; }

  uint32_t EntryPt() const
  { return entryPoint; }
  
  bool GetSymbolValue(const InternedString& symName, uint32_t&);
  
  uint32_t NumRegions() const
  { return nRegions; }

  const ExecRegion& GetRegion(uint32_t w) const
  { return regions[w]; }

  const uint8_t * GetImage() const
  { return image; }
  uint32_t GetImageSz() const
  { return imgsz; }
};

#endif // __EXECIMAGE_HXX__
