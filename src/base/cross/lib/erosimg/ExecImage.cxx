/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <sys/fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <erosimg/App.hxx>
#include <erosimg/Intern.hxx>
#include <erosimg/ExecImage.hxx>

ExecImage::ExecImage()
{
  image = 0;
  regions = 0;
  ResetImage();
}

ExecImage::~ExecImage()
{
  delete [] image;
}

void
ExecImage::ResetImage()
{
  name = InternedString("");
  entryPoint = 0;
  imageTypeName = "unknown";
  nRegions = 0;

  if (image)
    delete [] image;
  image = 0;

  if (regions)
    delete [] regions;
  regions = 0;
}

bool
ExecImage::SetImage(const InternedString& imageName)
{
  name = imageName;
  
  const char *fileName = App::BuildPath(name.str());
  
  int imagefd = ::open(fileName, O_RDONLY);
  if (imagefd < 0) {
    Diag::error(1, "Unable to open image file \"%s\"\n", name.str());
    return false;
  }

  struct stat statbuf;
  if (fstat(imagefd, &statbuf) < 0) {
    Diag::error(1, "Can't stat image file \"%s\"\n", name.str());
    ::close(imagefd);
    return false;
  }

  imgsz = statbuf.st_size;
  image = new uint8_t[imgsz];
  
  if (::read(imagefd, image, statbuf.st_size) != statbuf.st_size) {
    Diag::error(1, "Can't read image file \"%s\"\n", name.str());
    ::close(imagefd);
    return false;
  }

  bool win = false;

  if (win == false)
    win=InitElf();

#ifdef SUPPORT_AOUT
  if (win == false)
    win=InitElf();
#endif
  
  // Tries ELF first, then a.out format:
  if (!win) {
    Diag::fatal(1, "Couldn't interpret image\n");
    ::close(imagefd);
    return false;
  }

  ::close(imagefd);
  
  return true;
}

#if 0
uint32_t
ExecImage::GetSize(uint32_t unitSize) const
{
  uint32_t imageSize = txtSize + rodataSize + dataSize + bssSize;
  uint32_t units = (imageSize + (unitSize - 1)) / unitSize;
  return units;
}

uint32_t
ExecImage::GetRoSize(uint32_t unitSize) const
{
  uint32_t units = txtSize / unitSize;

  return units;
}

uint32_t
ExecImage::GetContentSize(uint32_t unitSize) const
{
  uint32_t imageSize = txtSize + rodataSize + dataSize + bssSize;
  uint32_t units = ((txtSize + dataSize) + (unitSize - 1)) / unitSize;

  Diag::printf("Image \"%s\" txt %d data %d bss %d sz %d units %d * %d\n",
	name.str(), txtSize, dataSize, bssSize, imageSize, units, unitSize);

  return units;
}

char *
ExecImage::GetBuffer(uint32_t unitSize) const
{
  uint32_t imageSize = txtSize + rodataSize + dataSize + bssSize;
  uint32_t units = (imageSize + (unitSize-1)) / unitSize;

  // allocate image buffer - round up to sector multiple
  char * imageBuf = new char[units * unitSize];
  bzero(imageBuf, units * unitSize);

  char *buf = imageBuf;

  ::lseek(imagefd, txtOffset, SEEK_SET);
  if (::read(imagefd, buf, txtSize) != txtSize)
    Diag::fatal(1, "Couldn't read text image\n");
  buf += txtSize;

  ::lseek(imagefd, rodataOffset, SEEK_SET);
  if (::read(imagefd, buf, rodataSize) != rodataSize)
    Diag::fatal(1, "Couldn't read rodata image\n");
  buf += rodataSize;

  ::lseek(imagefd, dataOffset, SEEK_SET);
  if (::read(imagefd, buf, dataSize) != dataSize)
    Diag::fatal(1, "Couldn't read data image\n");
  buf += dataSize;

  return imageBuf;
}
#endif

