/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <erosimg/Intern.hxx>

InternedString::InternEntry **InternedString::internTable = 0;

unsigned
InternedString::Signature(const char* s, int len)
{
  int i;
  unsigned sig = 0;

  for (i = 0; i < (len + 1)/2; i++) {
    sig = sig * 27 + s[i];
    sig = sig * 27 + s[len - 1 - i];
  }

  return (unsigned) sig;
}

InternedString::InternEntry *
InternedString::Lookup(const char* s, int len, unsigned strsig)
{
  unsigned ndx = strsig % 256;

  InternEntry *pEntry = internTable[ndx];

  while(pEntry) {
    if (pEntry->signature == strsig) {
      if ((strncmp(pEntry->string, s, len) == 0)
	  && (pEntry->string[len] == 0))
	return pEntry;
    }

    pEntry = pEntry->pNext;
  }

  return 0;
}

InternedString::InternEntry*
InternedString::Intern(const char* s)
{
  if (s)
    return Intern(s, strlen(s));
  else
    return 0;
}

InternedString::InternEntry*
InternedString::Intern(const char* s, int len)
{
  if (!internTable) {
    internTable = new InternEntry*[256];
    for (int i = 0; i < 256; i++)
      internTable[i] = 0;
  }

  InternEntry *pEntry;
  
  unsigned sig = Signature(s, len);

  if ( (pEntry = Lookup(s, len, sig)) )
    return pEntry;

  pEntry = new InternEntry;
  pEntry->signature = sig;
  pEntry->string = new char[len+ 1];
  pEntry->string[len] = 0;
  memcpy(pEntry->string, s, len);

  unsigned ndx = sig % 256;

  pEntry->pNext = internTable[ndx];
  internTable[ndx] = pEntry;

  return pEntry;
}
