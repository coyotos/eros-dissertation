#ifndef __PARSE_HXX__
#define __PARSE_HXX__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <disk/LowVolume.hxx>
#include <disk/ErosTypes.h>
#include <erosimg/Intern.hxx>
#include <erosimg/ArchDescrip.hxx>

// Parsing assistance class

class DiskKey;

class Parse {
public:
  // Parsing Support:

  static bool MatchStart(const char* &s, const char* txt)
  { s = txt; return 1; }
  static bool MatchKeyword(const char *&s, const char *kwd);
  static bool Match(const char *&s, const char *str);
  static bool MatchIdent(const char *&s, InternedString& ident);
  static bool MatchFileName(const char *&s, InternedString& fileName);
  static bool MatchWord(const char *&s, uint32_t& w);
  static bool MatchKeyData(const char *&s, uint32_t& kd);
  static bool MatchSlot(const char *&s, uint32_t& slot);
  static bool MatchOID(const char *&s, OID& oid);
  static bool MatchOIDVal(const char *&s, OID& oid);
  static bool MatchNumKeyVal(const char *&s, uint32_t& hi,
			     uint32_t& mid,uint32_t& lo);
  static bool MatchEOL(const char *&s);
  static bool MatchKey(const char *&s, DiskKey& key);

  static bool MatchArchitecture(const char *&s, InternedString& arch);
  static bool MatchRegister(const char *&s, InternedString& arch,
			    RegDescrip*&);
  static bool MatchRegValue(const char *&s, RegDescrip*,
			    InternedString& value);

  static void TrimLine(char *s);
};

#endif // __PARSE_HXX__
