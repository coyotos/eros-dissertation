/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <eros/target.h>
#include <erosimg/Intern.hxx>
#include <erosimg/ExecArch.hxx>
#include <erosimg/ErosImage.hxx>
#include <erosimg/Parse.hxx>
#include <erosimg/App.hxx>
#include <eros/Key.h>
#include <eros/RangeKey.h>
#include <disk/DiskNode.hxx>
#include <disk/DiskLSS.hxx>

#define PAGE_ALLOC_QUANTA 16
#define NODE_ALLOC_QUANTA 16
#define DIR_ALLOC_QUANTA 16
#define THREAD_ALLOC_QUANTA 16

extern void PrintDiskKey(const DiskKey&);

ErosHeader::ErosHeader()
{
  // First initialize the domain file image header:
  
  memset(signature, 0, 8);
  strcpy(signature, "ErosImg");
  imageByteSex = 0;

  version = EROS_IMAGE_VERSION;
  architecture = ExecArch::arch_unknown;

  nDirEnt = 0;
  nThread = 0;
  nPages = 0;
  nZeroPages = 0;
  nZeroCapPages = 0;
  nZeroPages = 0;
  nNodes = 0;
  strSize = 0;

  dirOffset = sizeof(ErosHeader);
  threadOffset = sizeof(ErosHeader);
  pageOffset = sizeof(ErosHeader);
  capPageOffset = sizeof(ErosHeader);
  nodeOffset = sizeof(ErosHeader);
  strTableOffset = sizeof(strTableOffset);
}

ErosImage::ErosImage()
{
  pageImages = 0;
  capPageImages = 0;
  nodeImages = 0;
  dir = 0;
  threadDir = 0;

  maxPage = 0;
  maxCapPage = 0;
  maxNode = 0;
  maxDir = 0;
  maxThread = 0;

  DiskKey key = AddNode();
  AddDirEnt("volsize", key);
}

ErosImage::~ErosImage()
{
  delete [] pageImages;
  delete [] capPageImages;
  delete [] nodeImages;
  delete [] dir;
  delete [] threadDir;
}

InternedString
ErosImage::GetString(int ndx) const
{
  return InternedString(pool.Get(ndx));
}

void
ErosImage::AddThread(const char* name, const DiskKey& key)
{
  InternedString is(name);

  AddThread(is, key);
}

void
ErosImage::AddDirEnt(const char* name, const DiskKey& key)
{
  InternedString is(name);

  AddDirEnt(is, key);
}

void
ErosImage::AssignDirEnt(const char* name, const DiskKey& key)
{
  InternedString is(name);

  AssignDirEnt(is, key);
}

void
ErosImage::AddThread(const InternedString& name, const DiskKey& key)
{
  uint32_t nameNdx = pool.Add(name);

  for (uint32_t i = 0; i < nThread; i++)
    if (threadDir[i].name == nameNdx)
      Diag::fatal(5, "Duplicate name \"%s\" added to image file\n", name.str());
  
  if (nThread >= maxThread)
    GrowThreadTable(maxThread + THREAD_ALLOC_QUANTA);

  threadDir[nThread].key = key;
  threadDir[nThread].name = nameNdx;
  nThread++;
}

void
ErosImage::AddDirEnt(const InternedString& name, const DiskKey& key)
{
  assert ( (bool) name );
  assert (name.str() != 0);
  
  uint32_t nameNdx = pool.Add(name);

  for (uint32_t i = 0; i < nDirEnt; i++)
    if (dir[i].name == nameNdx)
      Diag::fatal(5, "Duplicate name \"%s\" added to image file\n", name.str());
  
  if (nDirEnt >= maxDir)
    GrowDirTable(maxDir + DIR_ALLOC_QUANTA);

  dir[nDirEnt].key = key;
  dir[nDirEnt].name = nameNdx;
  nDirEnt++;
}

void
ErosImage::AssignDirEnt(const InternedString& name, const DiskKey& key)
{
  assert ( (bool) name );
  assert (name.str() != 0);
  
  uint32_t nameNdx = pool.Add(name);

  for (uint32_t i = 0; i < nDirEnt; i++)
    if (dir[i].name == nameNdx) {
      dir[i].key = key;
      return;
    }
  
  if (nDirEnt >= maxDir)
    GrowDirTable(maxDir + DIR_ALLOC_QUANTA);

  dir[nDirEnt].key = key;
  dir[nDirEnt].name = nameNdx;
  nDirEnt++;
}

bool
ErosImage::DelDirEnt(const char* name)
{
  InternedString is(name);

  return DelDirEnt(is);
}

bool
ErosImage::DelDirEnt(const InternedString& name)
{
  assert ( (bool) name );
  assert (name.str() != 0);
  
  uint32_t nameNdx = pool.Add(name);

  for (uint32_t i = 0; i < nDirEnt; i++) {
    if (dir[i].name == nameNdx) {
      for (uint32_t ent = i; ent < (nDirEnt - 1); ent++)
	dir[ent] = dir[ent+1];
      nDirEnt--;
      return true;
    }
  }

  return false;
}

bool
ErosImage::GetDirEnt(const char* name, DiskKey& key)
{
  InternedString is(name);

  return GetDirEnt(is, key);
}

bool
ErosImage::GetDirEnt(const InternedString& name, DiskKey& key)
{
  uint32_t nameNdx = pool.Add(name);

  for (uint32_t i = 0; i < nDirEnt; i++) {
    if (dir[i].name == nameNdx) {
      key = dir[i].key;
      return true;
    }
  }

  return false;
}

bool
ErosImage::GetThreadEnt(const char* name, DiskKey& key)
{
  InternedString is(name);

  return GetThreadEnt(is, key);
}

bool
ErosImage::GetThreadEnt(const InternedString& name, DiskKey& key)
{
  uint32_t nameNdx = pool.Add(name);

  for (uint32_t i = 0; i < nThread; i++) {
    if (threadDir[i].name == nameNdx) {
      key = threadDir[i].key;
      return true;
    }
  }

  return false;
}

void
ErosImage::SetDirEnt(const InternedString& name, const DiskKey& key)
{
  uint32_t nameNdx = pool.Add(name);

  for (uint32_t i = 0; i < nDirEnt; i++) {
    if (dir[i].name == nameNdx) {
      dir[i].key = key;
      return;
    }
  }

  Diag::fatal(1, "No directory entry for \"%s\"\n", name.str());
}

void
ErosImage::GrowDirTable(uint32_t newMax)
{
  if (maxDir < newMax) {
    maxDir = newMax;
    maxDir += (DIR_ALLOC_QUANTA - 1);
    maxDir -= (maxDir % DIR_ALLOC_QUANTA);

    Directory *newDir = new Directory[maxDir];
    if (dir)
      memcpy(newDir, dir, nDirEnt * sizeof(Directory)); 

    delete [] dir;
    dir = newDir;
  }
}

void
ErosImage::GrowThreadTable(uint32_t newMax)
{
  if (maxThread < newMax) {
    maxThread = newMax;
    maxThread += (THREAD_ALLOC_QUANTA - 1);
    maxThread -= (maxThread % THREAD_ALLOC_QUANTA);

    Directory *newThreadDir = new Directory[maxDir];
    if (threadDir)
      memcpy(newThreadDir, threadDir, nThread * sizeof(Directory)); 

    delete [] threadDir;
    threadDir = newThreadDir;
  }
}

void
ErosImage::GrowNodeTable(uint32_t newMax)
{
  if (maxNode < newMax) {
    maxNode = newMax;
    maxNode += (NODE_ALLOC_QUANTA - 1);
    maxNode -= (maxNode % NODE_ALLOC_QUANTA);

    DiskNode *newNodeImages = new DiskNode[maxNode];
    if (nodeImages)
      memcpy(newNodeImages, nodeImages, nNodes * sizeof(DiskNode)); 

    delete [] nodeImages;
    nodeImages = newNodeImages;
  }
}

void
ErosImage::GrowPageTable(uint32_t newMax)
{
  if (maxPage < newMax) {
    maxPage = newMax;
    maxPage += (PAGE_ALLOC_QUANTA - 1);
    maxPage -= (maxPage % PAGE_ALLOC_QUANTA);

    uint8_t *newPageImages = new uint8_t[maxPage * EROS_PAGE_SIZE];
    if (pageImages)
      memcpy(newPageImages, pageImages, nPages * EROS_PAGE_SIZE); 

    delete [] pageImages;
    pageImages = newPageImages;
  }
}

void
ErosImage::GrowCapPageTable(uint32_t newMax)
{
  if (maxCapPage < newMax) {
    maxCapPage = newMax;
    maxCapPage += (PAGE_ALLOC_QUANTA - 1);
    maxCapPage -= (maxCapPage % PAGE_ALLOC_QUANTA);

    uint8_t *newCapPageImages = new uint8_t[maxCapPage * EROS_PAGE_SIZE];
    if (capPageImages)
      memcpy(newCapPageImages, capPageImages, nCapPages * EROS_PAGE_SIZE); 

    delete [] capPageImages;
    capPageImages = newCapPageImages;
  }
}

DiskKey
ErosImage::AddZeroDataPage(bool readOnly)
{
  OID oid(nZeroPages++);
  
  DataPageKey key(oid, readOnly);
  key.SetPrepared();

  return key;
}

DiskKey
ErosImage::AddDataPage(const uint8_t *buf, bool readOnly)
{
  if (nPages >= maxPage)
    GrowPageTable(maxPage + PAGE_ALLOC_QUANTA);

  memcpy(&pageImages[nPages * EROS_PAGE_SIZE], buf, EROS_PAGE_SIZE);
  
  OID  oid(nPages++);

  DataPageKey key(oid, readOnly);

  return key;
}

DiskKey
ErosImage::AddZeroCapPage(bool readOnly)
{
  OID oid(nZeroCapPages++);
  
  CapPageKey key(oid, readOnly);
  key.SetPrepared();

  return key;
}

DiskKey
ErosImage::AddCapPage(const uint8_t *buf, bool readOnly)
{
  if (nCapPages >= maxCapPage)
    GrowCapPageTable(maxCapPage + PAGE_ALLOC_QUANTA);

  memcpy(&capPageImages[nPages * EROS_PAGE_SIZE], buf, EROS_PAGE_SIZE);
  
  OID  oid(nCapPages++);

  CapPageKey key(oid, readOnly);

  return key;
}

void
ErosImage::GetDataPageContent(uint32_t ndx, uint8_t* buf)
{
  if (ndx >= nPages)
    Diag::fatal(5, "Not that many pages in image file\n");
  
  memcpy(buf, &pageImages[ndx*EROS_PAGE_SIZE], EROS_PAGE_SIZE);
}

void
ErosImage::GetCapPageContent(uint32_t ndx, DiskKey* kbuf)
{
  if (ndx >= nCapPages)
    Diag::fatal(5, "Not that many cap pages in image file\n");
  
  memcpy(kbuf, &capPageImages[ndx*EROS_PAGE_SIZE], EROS_PAGE_SIZE);
}

void
ErosImage::GetNodeContent(uint32_t ndx, DiskNode& node)
{
  if (ndx >= nNodes)
    Diag::fatal(5, "Not that many nodes in image file\n");
  
  memcpy(&node, &nodeImages[ndx], sizeof(DiskNode));
}

DiskKey
ErosImage::AddNode(bool readOnly)
{
  if (nNodes >= maxNode)
    GrowNodeTable(maxNode + NODE_ALLOC_QUANTA);

  DiskNode& node = nodeImages[nNodes];
  OID  oid(nNodes++);
  node.allocCount = 0;
  node.callCount = 0;
  node.oid = oid;

  for (unsigned int i = 0; i < EROS_NODE_SIZE; i++)
    node[i] = NumberKey(0);

  NodeKey key(oid, readOnly);

  return key;
}

#if 0
void
ErosImage::Import(const ErosImage& image)
{
  // Step 1: Grow the various tables to hold the new entries:

  GrowDirTable(nDirEnt + image.nDirEnt);
  GrowThreadTable(nThread + image.nThread);
  GrowPageTable(nPages + image.nPages);
  GrowNodeTable(nNodes + image.nNodes-1);

  // Step 2: Append the other image's pages and nodes WITHOUT bumping
  // our counts:
  
  memcpy(&pageImages[nPages], image.pageImages,
	 image.nPages * EROS_PAGE_SIZE);
  // Skip the first node, which is the volsize node:
  memcpy(&nodeImages[nNodes], image.nodeImages + 1,
	 (image.nNodes - 1) * sizeof(DiskNode));

  // Step 3: Relocate the newly added nodes and directory entries:
  for (uint32_t ndx = 0; ndx < image.nNodes; ndx++) {
    DiskNode& node = nodeImages[ndx + nNodes];

    for (uint32_t slot = 0; slot < EROS_NODE_SIZE; slot++) {
      DiskKey& key = node[slot];
      
      // If the key's OID needs to be adjusted, do so:
      if (key.IsType(KtDataPage) && key.IsPrepared() == false)
	key.unprep.oid += nPages;
      else if (key.IsType(KtDataPage) && key.IsPrepared())
	key.unprep.oid += nZeroPages;
      else if (key.IsNodeKeyType())
	key.unprep.oid += (nNodes - 1);
    }
  }

  // Step 4: add the foreign directory table entries:
  for (uint32_t ndx = 0; ndx < image.nDirEnt; ndx++) {
    Directory& dirEnt = image.dir[ndx];
    DiskKey key = dirEnt.key;

    // Check the string table entry:
    InternedString name = image.GetString(dirEnt.name);
    InternedString vs("volsize");
    
    if (name == vs)
      continue;
    
    // If the key's OID needs to be adjusted, do so:
    if (key.IsType(KtDataPage) && key.IsPrepared() == false)
      key.unprep.oid += nPages;
    else if (key.IsType(KtDataPage) && key.IsPrepared())
      key.unprep.oid += nZeroPages;
    else if (key.IsNodeKeyType())
      key.unprep.oid += (nNodes - 1);

    // Add the resulting entry to our directory using the AddDirEnt
    // routine in order to check for collisions.
    AddDirEnt(name, key);
  }

  // Step 5: add the foreign thread table entries:
  for (uint32_t ndx = 0; ndx < image.nThread; ndx++) {
    Directory& threadEnt = image.threadDir[ndx];
    DiskKey key = threadEnt.key;
    InternedString name = image.GetString(threadEnt.name);

    // If the key's OID needs to be adjusted, do so:
    if (key.IsType(KtDataPage) && key.IsPrepared() == false)
      key.unprep.oid += nPages;
    else if (key.IsType(KtDataPage) && key.IsPrepared())
      key.unprep.oid += nZeroPages;
    else if (key.IsNodeKeyType())
      key.unprep.oid += (nNodes - 1);

    // Add the resulting entry to our directory using the AddDirEnt
    // routine in order to check for collisions.
    AddThread(name, key);
  }

  // Note that any strings in the foreign image that were really
  // referenced were copied somewhere in the nonsense above!
  
  // Step 5: Update the count fields that are not already up to date:
  nPages += image.nPages;
  nZeroPages += image.nZeroPages;
  nNodes += image.nNodes;
}
#endif

DiskKey
ErosImage::GetNodeSlot(const DiskKey& nodeKey, uint32_t slot)
{
  if (slot >= EROS_NODE_SIZE)
    Diag::fatal(5,"Slot value too high\n");
  
  if (nodeKey.IsNodeKeyType() == false)
    Diag::fatal(5,"GetNodeSlot expects node key!\n");

  uint32_t ndx = nodeKey.unprep.oid;
  return nodeImages[ndx][slot];
}

DiskKey
ErosImage::GetNodeSlot(uint32_t nodeNdx, uint32_t slot)
{
  if (slot >= EROS_NODE_SIZE)
    Diag::fatal(5,"Slot value too high\n");
  
  return nodeImages[nodeNdx][slot];
}

void
ErosImage::SetNodeSlot(const DiskKey& nodeKey, uint32_t slot,
		       const DiskKey& key)
{
  if (slot >= EROS_NODE_SIZE)
    Diag::fatal(5,"Slot value too high\n");
  
  if (nodeKey.IsNodeKeyType() == false)
    Diag::fatal(5,"GetNodeSlot expects node key!\n");

  uint32_t ndx = nodeKey.unprep.oid;
  nodeImages[ndx][slot] = key;
}

void
ErosImage::SetArchitecture(ExecArch::Architecture arch)
{
  ExecArch::ArchInfo ai = ExecArch::GetArchInfo(arch);
  imageByteSex = ai.byteSex;
  architecture = arch;
}

void
ErosImage::ValidateImage(const char* target)
{
  if (nDirEnt == 0)
    Diag::fatal(2, "No directory entries in \"%s\"!\n", target);

  if (!nodeImages)
    Diag::fatal(2, "Missing volsize node!\n", target);

  DiskNode& node = nodeImages[0];
  node[OT_Node] = NumberKey(nNodes);
  node[OT_DataPage] = NumberKey(nPages + nZeroPages);
  node[OT_CapPage] = NumberKey(nCapPages + nZeroCapPages);
#ifdef EROS_PROCS_PER_FRAME
  node[OT_Process] = NumberKey(0);	// number of processes
#endif
}

void
ErosImage::WriteToFile(const char *target)
{
  ValidateImage(target);
  
  int tfd = ::open(target, O_RDWR|O_TRUNC);
  if (tfd < 0 && errno == ENOENT) {
    tfd = ::open(target, O_RDWR|O_CREAT, 0666);
    App.AddTarget(target);
  }
  
  if (tfd < 0)
    Diag::fatal(2, "Unable to open target file \"%s\"\n", target);

  // Pin down what all the offsets will be.  We will write the pieces
  // in the following order:
  //
  //    image header
  //	image directory
  //    page content images
  //    node content images
  //    string table
  //

  dirOffset = sizeof(ErosHeader);
  threadOffset = dirOffset;
  threadOffset += nDirEnt * sizeof(Directory);
  pageOffset = threadOffset;
  pageOffset += nThread * sizeof(Directory);
  capPageOffset = pageOffset;
  capPageOffset += nPages * EROS_PAGE_SIZE;
  nodeOffset = capPageOffset;
  nodeOffset += nCapPages * EROS_PAGE_SIZE;
  strTableOffset = nodeOffset;
  strTableOffset += nNodes * sizeof(DiskNode);
  strSize = pool.Size();

  int sz = sizeof(ErosHeader);
  if (::write(tfd, this, sz) != sz)
    Diag::fatal(3, "Unable to write image file header\n");

  sz = nDirEnt * sizeof(Directory);
  if (::write(tfd, dir, sz) != sz)
    Diag::fatal(3, "Unable to write image directory\n");

  sz = nThread * sizeof(Directory);
  if (::write(tfd, threadDir, sz) != sz)
    Diag::fatal(3, "Unable to write image thread list\n");

  sz = nPages * EROS_PAGE_SIZE;
  if (::write(tfd, pageImages, sz) != sz)
    Diag::fatal(3, "Unable to write page images\n");

  sz = nCapPages * EROS_PAGE_SIZE;
  if (::write(tfd, capPageImages, sz) != sz)
    Diag::fatal(3, "Unable to write capability page images\n");

  sz = nNodes * sizeof(DiskNode);
  if (::write(tfd, nodeImages, sz) != sz)
    Diag::fatal(3, "Unable to write node images\n");

  if (pool.WriteToFile(tfd) != (int) strSize)
    Diag::fatal(3, "Unable to write string pool\n");

  ::close(tfd);
}

void
ErosImage::ReadFromFile(const char *source)
{
  nNodes = 0;
  nPages = 0;
  nCapPages = 0;
  nZeroPages = 0;
  delete [] pageImages;
  pageImages = 0;
  delete [] capPageImages;
  capPageImages = 0;
  delete [] nodeImages;
  nodeImages = 0;
  nDirEnt = 0;
  delete [] dir;
  nThread = 0;
  delete [] threadDir;
  
  int sfd = ::open(source, O_RDONLY);
  if (sfd < 0)
    Diag::fatal(2, "Unable to open domain file \"%s\"\n", source);

  struct stat statbuf;

  if (::fstat(sfd, &statbuf) < 0)
    Diag::fatal(2, "Can't stat domain file \"%s\"\n", source);
    
  // Step 1: read the image file header:
  
  if (::read(sfd, this, sizeof(ErosHeader)) != sizeof(ErosHeader))
    Diag::fatal(2, "Cannot read image header from \"%s\"\n", source);

  // Step 1b: verify that the actual length of the file matches our
  // expectations:

  if (version != EROS_IMAGE_VERSION)
    Diag::fatal(2, "Image file version in \"%s\" is obsolete\n", source);
    
  int expect = sizeof(ErosHeader);
  expect += nDirEnt * sizeof(Directory);
  expect += nThread * sizeof(Directory);
  expect += nPages * EROS_PAGE_SIZE;
  expect += nNodes * sizeof(DiskNode);
  expect += strSize;

  if (expect != statbuf.st_size)
    Diag::fatal(2, "Domain image file \"%s\" is malsized.\n", source);

  // Step 2: read image directory:

  maxDir = nDirEnt;
  if (maxDir % DIR_ALLOC_QUANTA) {
    maxDir -= (maxDir % DIR_ALLOC_QUANTA);
    maxDir += DIR_ALLOC_QUANTA;
  }

  dir = new Directory[maxDir];
  int sz = nDirEnt * sizeof(Directory);
  
  if (::lseek(sfd, dirOffset, SEEK_SET) < 0)
    Diag::fatal(2, "Cannot seek to image directory in \"%s\"\n", source);

  if (::read(sfd, dir, sz) != sz)
    Diag::fatal(2, "Cannot read image directory from \"%s\"\n", source);
  
  // Step 3: read thread directory:

  maxThread = nThread;
  if (maxThread % THREAD_ALLOC_QUANTA) {
    maxThread -= (maxThread % THREAD_ALLOC_QUANTA);
    maxThread += THREAD_ALLOC_QUANTA;
  }

  threadDir = new Directory[maxDir];
  sz = nThread * sizeof(Directory);
  
  if (::lseek(sfd, threadOffset, SEEK_SET) < 0)
    Diag::fatal(2, "Cannot seek to thread list in \"%s\"\n", source);

  if (::read(sfd, threadDir, sz) != sz)
    Diag::fatal(2, "Cannot read thread list from \"%s\"\n", source);
  
  // Step 4: read page images:

  maxPage = nPages;
  if (maxPage % PAGE_ALLOC_QUANTA) {
    maxPage -= (maxPage % PAGE_ALLOC_QUANTA);
    maxPage += PAGE_ALLOC_QUANTA;
  }

  pageImages = new uint8_t[maxPage * EROS_PAGE_SIZE];
  sz = nPages * EROS_PAGE_SIZE;
  
  if (::lseek(sfd, pageOffset, SEEK_SET) < 0)
    Diag::fatal(2, "Cannot seek to page images in \"%s\"\n", source);

  if (::read(sfd, pageImages, sz) != sz)
    Diag::fatal(2, "Cannot read page images from \"%s\"\n", source);
  
  // Step 5: read cap page images:

  maxCapPage = nCapPages;
  if (maxCapPage % PAGE_ALLOC_QUANTA) {
    maxCapPage -= (maxCapPage % PAGE_ALLOC_QUANTA);
    maxCapPage += PAGE_ALLOC_QUANTA;
  }

  capPageImages = new uint8_t[maxCapPage * EROS_PAGE_SIZE];
  sz = nCapPages * EROS_PAGE_SIZE;
  
  if (::lseek(sfd, capPageOffset, SEEK_SET) < 0)
    Diag::fatal(2, "Cannot seek to capability page images in \"%s\"\n", source);

  if (::read(sfd, capPageImages, sz) != sz)
    Diag::fatal(2, "Cannot read capability page images from \"%s\"\n", source);
  
  // Step 6: read node images:

  maxNode = nNodes;
  if (maxNode % NODE_ALLOC_QUANTA) {
    maxNode -= (maxNode % NODE_ALLOC_QUANTA);
    maxNode += NODE_ALLOC_QUANTA;
  }

  nodeImages = new DiskNode[maxNode];
  sz = nNodes * sizeof(DiskNode);
  
  if (::lseek(sfd, nodeOffset, SEEK_SET) < 0)
    Diag::fatal(2, "Cannot seek to node images in \"%s\"\n", source);

  if (::read(sfd, nodeImages, sz) != sz)
    Diag::fatal(2, "Cannot read node images from \"%s\"\n", source);
  
  // Step 7: read string pool:

  if (::lseek(sfd, strTableOffset, SEEK_SET) < 0)
    Diag::fatal(2, "Cannot seek to string table in \"%s\"\n", source);

  if (pool.ReadFromFile(sfd, strSize) == false)
    Diag::fatal(2, "Cannot load string table from \"%s\"\n", source);

  ::close(sfd);
}

// Given a segmode key that is the root of a segment tree, add the
// specified page key at the specified offset in that segment,
// inserting any needed nodes along the way.
//
// DO NOT traverse a red segment boundary to do so - if you need to
// add something to the contained segment, keep a handle to it and add
// it directly.
//
// Returns a key to the new segment root.  If the segment has grown,
// this may not be a key to the same node as the original segment key.
//
// The red segment expansion code found here assumes that it will not
// encounter an oversize subsegment.  I'll have to get that right
// (whatever that means) in the fault handling domain, but it doesn't
// seem necessary here.  You can still install an oversized subsegment
// if you do it with a SEGMENT key as opposed to a node key -- the
// insertion code will not cross a segment boundary, because segments
// are supposed to be opaque.
//
DiskKey
ErosImage::DoAddPageToBlackSegment(const DiskKey& segRoot,
				   uint64_t segOffset,
				   const DiskKey& pageKey,
				   uint64_t path,
				   bool expandRedSegment)
{
  // segOffsetBLSS holds the LSS of the smallest segment that could
  // conceivably contain segOffset.
  uint32_t segOffsetBLSS = LSS::BiasedLSS(segOffset);
  uint32_t rootBLSS = segRoot.GetBlss();
  
#if 0
  Diag::debug(2, "pageAddr=0x%04x%08x%08x\n",
	   pageAddr.hi, pageAddr.mid, pageAddr.lo);
#endif

  if ( segRoot.IsRedSegmentKey() || segRoot.IsType(KtSegment) )
    Diag::fatal(4, "AddPageToSegment: Cannot traverse subsegment\n");

  if (rootBLSS < segOffsetBLSS) {
    // Inserting a page whose offset BLSS is too large - need to grow
    // the subsegment.  The code for altering the path is a bit
    // tricky, since in effect we are backing up in the path
    // traversal.

    DiskKey newRoot = AddNode();
    newRoot.SetBlss(segOffsetBLSS);

    if (expandRedSegment) {
      Diag::printf("Expanding red seg...\n");
      uint64_t pathMask = LSS::Mask(segOffsetBLSS);
      uint64_t slotPath = path & ~pathMask;
      slotPath |= 3u;		// background window key
      uint64_t slotIncr = 1;
      slotIncr <<= segOffsetBLSS * EROS_NODE_LGSIZE;

      for (unsigned int i = 0; i < EROS_NODE_SIZE; i++) {
	uint64_t bkWindowValue = slotPath;
	
	DiskKey slotKey =
	  NumberKey(0, (uint32_t) (bkWindowValue >> 32),
		    (uint32_t) (bkWindowValue));
	SetNodeSlot(newRoot, i, slotKey);
	slotPath += slotIncr;
      }
    }
    
    SetNodeSlot(newRoot, 0, segRoot);
    
    return DoAddPageToBlackSegment(newRoot, segOffset, pageKey, path,
				   expandRedSegment);
  }

  if (rootBLSS > segOffsetBLSS) {
    uint32_t slot = 0; /* segOffset.blssSlotNdx(segOffsetBLSS); */
    DiskKey subSeg = GetNodeSlot(segRoot, slot);
	
    DiskKey newSlotKey =
      DoAddPageToBlackSegment(subSeg, segOffset, pageKey, path,
			      expandRedSegment);

    SetNodeSlot(segRoot, slot, newSlotKey);
      
    return segRoot;
  }

  // The page key might replace the current key:
  if (rootBLSS == EROS_PAGE_BLSS && segOffsetBLSS == EROS_PAGE_BLSS)
    return pageKey;
	 
  // page key goes somewhere beneath current tree:
  uint32_t slot = LSS::SlotNdx(segOffset, segOffsetBLSS);
  segOffsetBLSS--;
  uint64_t subSegOffset = segOffset;
  subSegOffset &= LSS::Mask(segOffsetBLSS);

  DiskKey subSeg = GetNodeSlot(segRoot, slot);
  DiskKey newSubSeg =
    DoAddPageToBlackSegment(subSeg, subSegOffset,
			    pageKey, path, expandRedSegment);
  SetNodeSlot(segRoot, slot, newSubSeg);

  return segRoot;
}

DiskKey
ErosImage::AddPageToSegment(const DiskKey& segRoot,
			    uint64_t segOffset,
			    const DiskKey& pageKey)
{
  if (segRoot.IsNodeKeyType() && segRoot.unprep.oid >= OID(nNodes))
    Diag::fatal(4, "Segment root node not in image file\n");

  if (segRoot.IsType(KtProcess))
    Diag::fatal(4, "AddPageToSegment: Domain key passed as segment root\n");
  if (segRoot.IsType(KtStart))
    Diag::fatal(4, "AddPageToSegment: Start key passed as segment root\n");
  if (segRoot.IsType(KtResume))
    Diag::fatal(4, "AddPageToSegment: Resume key passed as segment root\n");

  if (segRoot.IsType(KtDataPage) &&
      segRoot.IsPrepared() == false &&
      segRoot.unprep.oid >= OID(nPages))
    Diag::fatal(4, "Segment root page not in image file\n");

  if (segRoot.IsType(KtDataPage) &&
      segRoot.IsPrepared() &&
      segRoot.unprep.oid >= OID(nZeroPages))
    Diag::fatal(4, "Segment root page not in image file\n");

  if (pageKey.IsType(KtDataPage) == false &&
      pageKey.IsType(KtCapPage) == false &&
      pageKey.IsType(KtNumber) == false)
    Diag::fatal(4, "AddPageToSegment expects data or cap page key\n");

  uint32_t segOffsetBLSS = LSS::BiasedLSS(segOffset);
  uint32_t rootBLSS = segRoot.GetBlss();
  uint32_t segBLSS = pageKey.GetBlss();

  if (segRoot.IsType(KtNumber) == false &&
      segOffsetBLSS <= segBLSS &&
      rootBLSS <= segOffsetBLSS)
    Diag::fatal(4, "Inserted page and offset would replace entire existing segment.\n");
    
  // It is permissable for the root segment node to be a red segment.
  // If so, we must verify that we are not about to get ourselves in
  // trouble by growing the red segment to suitable size and fabricate
  // a black segment key to pass down into the actual insertion
  // routine:

  DiskKey rootKey = segRoot;
  
  if ( segRoot.IsRedSegmentKey() ) {
    DiskKey fmtKey = GetNodeSlot(segRoot, RedSegFormat);
    uint32_t segBlss = REDSEG_GET_BLSS(fmtKey.nk);
    uint32_t newSegBlss = segBlss;
    uint32_t initialSlots = REDSEG_GET_INITIAL_SLOTS(fmtKey.nk);

    uint32_t segOffsetBLSS = LSS::BiasedLSS(segOffset);
    if (segOffsetBLSS > segBlss)
      newSegBlss = segOffsetBLSS;
    
    uint32_t slot = LSS::SlotNdx(segOffset, segOffsetBLSS);
    if (slot >= initialSlots)
      newSegBlss++;

    if (newSegBlss > segBlss) {
      // must grow the red segment by rewriting the format key:
      REDSEG_SET_BLSS(fmtKey.nk, newSegBlss);
      SetNodeSlot(segRoot, RedSegFormat, fmtKey);
    }

    // Now fabricate a black segment key to the segment's root node
    // whose BLSS matches that of the red segment:
    
    rootKey.SetBlss(newSegBlss);
  }

  bool wasSeg = false;
  
  if (rootKey.IsType(KtSegment)) {
    wasSeg = true;
    rootKey.SetType(KtNode);
  }
  
  DiskKey newSegRoot =
    DoAddPageToBlackSegment(rootKey, segOffset, pageKey, segOffset,
			    segRoot.IsRedSegmentKey() ? true : false);

  if (wasSeg)
    newSegRoot.SetType(KtSegment);
  
  if ( segRoot.IsRedSegmentKey() )
    return segRoot;
  return newSegRoot;
}

#ifndef max
#define max(x,y) ( ((x) > (y)) ? (x) : (y) )
#endif

DiskKey
ErosImage::DoAddSubsegToBlackSegment(const DiskKey& segRoot,
				     uint64_t segOffset,
				     const DiskKey& segKey)
{
  // segOffsetBLSS holds the BLSS of the smallest segment that could
  // conceivably contain segOffset.
  uint32_t segOffsetBLSS = LSS::BiasedLSS(segOffset);
  uint32_t rootBLSS = segRoot.GetBlss();
  uint32_t segBLSS = segKey.GetBlss();
  
#if 0
  Diag::debug(2, "pageAddr=0x%04x%08x%08x\n",
	   pageAddr.hi, pageAddr.mid, pageAddr.lo);
#endif

  if ( segRoot.IsRedSegmentKey() || segRoot.IsType(KtSegment) )
    Diag::fatal(4, "AddPageToSegment: Cannot traverse subsegment\n");

  if (rootBLSS < segOffsetBLSS) {
    // Inserting a segment whose offset BLSS is too large - need to
    // grow a new root.

    DiskKey newRoot = AddNode();
    newRoot.SetBlss(segOffsetBLSS);
    SetNodeSlot(newRoot, 0, segRoot);
    
    return DoAddSubsegToBlackSegment(newRoot, segOffset, segKey);
  }

  if (rootBLSS > segOffsetBLSS && rootBLSS > segBLSS) {
    uint32_t slot = LSS::SlotNdx(segOffset, segOffsetBLSS);
    DiskKey subSeg = GetNodeSlot(segRoot, slot);
	
    DiskKey newSlotKey = DoAddSubsegToBlackSegment(subSeg, segOffset, segKey);

    SetNodeSlot(segRoot, slot, newSlotKey);
      
    return segRoot;
  }

  // The new segment might replace the current segment:
  if ( rootBLSS <= segBLSS )
    return segKey;
	 
  // segment key goes somewhere beneath current tree:
  uint32_t slot = LSS::SlotNdx(segOffset, segOffsetBLSS);
  segOffsetBLSS--;
  uint64_t subSegOffset = segOffset;
  subSegOffset &= LSS::Mask(segOffsetBLSS);

  DiskKey subSeg = GetNodeSlot(segRoot, slot);
  DiskKey newSubSeg = DoAddSubsegToBlackSegment(subSeg, subSegOffset, segKey);
  SetNodeSlot(segRoot, slot, newSubSeg);

  return segRoot;
}

DiskKey
ErosImage::AddSubsegToSegment(const DiskKey& segRoot,
			      uint64_t segOffset,
			      const DiskKey& segKey)
{
  if (segRoot.IsNodeKeyType() && segRoot.unprep.oid >= OID(nNodes))
    Diag::fatal(4, "Segment root node not in image file\n");

  if (segKey.IsNodeKeyType() && segKey.unprep.oid >= OID(nNodes))
    Diag::fatal(4, "Segment node not in image file\n");

  if (segRoot.IsType(KtProcess))
    Diag::fatal(4, "AddSubsegToSegment: Domain key passed as segment root\n");
  if (segRoot.IsType(KtStart))
    Diag::fatal(4, "AddSubsegToSegment: Start key passed as segment root\n");
  if (segRoot.IsType(KtResume))
    Diag::fatal(4, "AddSubsegToSegment: Resume key passed as segment root\n");

  if (segKey.IsType(KtProcess))
    Diag::fatal(4, "AddSubsegToSegment: Domain key passed as segment\n");
  if (segKey.IsType(KtStart))
    Diag::fatal(4, "AddSubsegToSegment: Start key passed as segment\n");
  if (segKey.IsType(KtResume))
    Diag::fatal(4, "AddSubsegToSegment: Resume key passed as segment\n");

  if (segKey.IsType(KtNode) == false &&
      segKey.IsType(KtSegment) == false &&
      segKey.IsType(KtDataPage) == false)
    Diag::fatal(4, "AddSubsegToSegment: added subseg must be segment, "
		"segtree, or page\n");

  if (segRoot.IsType(KtDataPage) &&
      segRoot.IsPrepared() == false &&
      segRoot.unprep.oid >= OID(nPages))
    Diag::fatal(4, "Segment root page not in image file\n");

  if (segRoot.IsType(KtDataPage) &&
      segRoot.IsPrepared() &&
      segRoot.unprep.oid >= OID(nZeroPages))
    Diag::fatal(4, "Segment root page not in image file\n");

  if (segKey.IsType(KtDataPage) &&
      segKey.IsPrepared() == false &&
      segKey.unprep.oid >= OID(nPages))
    Diag::fatal(4, "Segment page not in image file\n");

  if (segKey.IsType(KtDataPage) &&
      segKey.IsPrepared() &&
      segKey.unprep.oid >= OID(nZeroPages))
    Diag::fatal(4, "Segment page not in image file\n");

#if 0
  if (segKey.IsSegModeType() == false)
    Diag::fatal(4, "AddSubsegToSegment expects segment key\n");
#endif

  uint32_t segOffsetBLSS = LSS::BiasedLSS(segOffset);
  uint32_t rootBLSS = segRoot.GetBlss();
  uint32_t segBLSS = segKey.GetBlss();

  if (segRoot.IsType(KtNumber) == false &&
      segOffsetBLSS <= segBLSS &&
      rootBLSS <= segOffsetBLSS)
    Diag::fatal(4, "Inserted segment and offset would replace existing segment.\n");
    
  DiskKey rootKey = segRoot;
  
  if ( segRoot.IsRedSegmentKey() ) {
    DiskKey fmtKey = GetNodeSlot(segRoot, RedSegFormat);
    uint32_t segBlss = REDSEG_GET_BLSS(fmtKey.nk);
    uint32_t newSegBlss = segBlss;
    uint32_t initialSlots = REDSEG_GET_INITIAL_SLOTS(fmtKey.nk);

    uint32_t segOffsetBLSS = LSS::BiasedLSS(segOffset);
    if (segOffsetBLSS > segBlss)
      newSegBlss = segOffsetBLSS;
    
    uint32_t slot = LSS::SlotNdx(segOffset, segOffsetBLSS);
    if (slot >= initialSlots)
      newSegBlss++;

    if (newSegBlss > segBlss) {
      // must grow the red segment by rewriting the format key:
      REDSEG_SET_BLSS(fmtKey.nk, newSegBlss);
      SetNodeSlot(segRoot, RedSegFormat, fmtKey);
    }

    // Now fabricate a black segment key to the segment's root node
    // whose BLSS matches that of the red segment:
    
    rootKey.SetBlss(newSegBlss);
  }

  if (rootKey.IsType(KtSegment))
    rootKey.SetType(KtNode);
  
  DiskKey newSegRoot = DoAddSubsegToBlackSegment(rootKey, segOffset, segKey);
  if ( segRoot.IsRedSegmentKey() )
    return segRoot;
  return newSegRoot;
}

bool
ErosImage::DoGetPageInSegment(const DiskKey& segRoot,
			      uint64_t segOffset,
			      DiskKey& pageKey)
{
  // pageSegLSS holds the LSS of the smallest segment that could
  // conceivably contain pageAddr.
  uint32_t segOffsetBLSS = LSS::BiasedLSS(segOffset);
  uint32_t rootBLSS = segRoot.GetBlss();
  
  if (segOffsetBLSS > rootBLSS)
    return false;

  if (segOffsetBLSS < rootBLSS) {
    DiskKey subSeg = GetNodeSlot(segRoot, 0);
    return DoGetPageInSegment(subSeg, segOffset, pageKey);
  }

  // Handle single page or empty segments:
  if (segOffsetBLSS == rootBLSS && segOffsetBLSS == EROS_PAGE_BLSS) {
    pageKey = segRoot;
    return (segRoot.IsType(KtDataPage)) ? true : false;
  }

  // segOffsetBlss == rootBLSS, not a page:
  // page key goes somewhere beneath current tree:
  uint32_t slot = LSS::SlotNdx(segOffset, segOffsetBLSS);
  segOffsetBLSS--;
  uint64_t subSegOffset = segOffset;
  subSegOffset &= LSS::Mask(segOffsetBLSS);

  DiskKey subSeg = GetNodeSlot(segRoot, slot);
  return DoGetPageInSegment(subSeg, subSegOffset, pageKey);
}

bool
ErosImage::GetPageInSegment(const DiskKey& segRoot,
			    uint64_t segOffset,
			    DiskKey& pageKey)
{
  if (segRoot.IsNodeKeyType() && segRoot.unprep.oid >= OID(nNodes))
    Diag::fatal(4, "Segment root node not in image file\n");

  if (segRoot.IsType(KtProcess))
    Diag::fatal(4, "GetPageInSegment: Domain key passed as segment root\n");
  if (segRoot.IsType(KtStart))
    Diag::fatal(4, "GetPageInSegment: Start key passed as segment root\n");
  if (segRoot.IsType(KtResume))
    Diag::fatal(4, "GetPageInSegment: Resume key passed as segment root\n");

  if (segRoot.IsType(KtDataPage) &&
      segRoot.IsPrepared() == false &&
      segRoot.unprep.oid >= OID(nPages))
    Diag::fatal(4, "Segment root page not in image file\n");

  if (segRoot.IsType(KtDataPage) &&
      segRoot.IsPrepared() &&
      segRoot.unprep.oid >= OID(nZeroPages))
    Diag::fatal(4, "Segment root page not in image file\n");

  return DoGetPageInSegment(segRoot, segOffset, pageKey);
}

void
ErosImage::SetPageWord(DiskKey& pageKey, uint32_t offset,
		       uint32_t value)
{
  if (pageKey.IsType(KtDataPage) == false)
    Diag::fatal(5, "ErosImage::SetPageWord requires page key!\n");
  
  if (offset % 4)
    Diag::fatal(5, "ErosImage::SetPageWord offset must be word address!\n");
    
  // Okay, here's the tricky part.  It's quite possible that the page
  // key we were handed was a zero page key.  If so, we fabricate a
  // new (nonzero) page to replace it and change all of the existing
  // keys to this page to point to the new page:
  if (pageKey.IsPrepared()) {
    uint8_t buf[EROS_PAGE_SIZE];
    memset(buf, 0, EROS_PAGE_SIZE);
    
    DiskKey newPage = AddDataPage(buf);

    // Relocate all of the keys in the image to reflect the removal of
    // this zero page.
    for (uint32_t nodeNdx = 0; nodeNdx < nNodes; nodeNdx++) {
      for (uint32_t keyNdx = 0; keyNdx < EROS_NODE_SIZE; keyNdx++) {
	DiskKey& key = nodeImages[nodeNdx][keyNdx];

	if (key.IsType(KtDataPage) == false || key.IsPrepared() == false)
	  continue;
	
	// It's a zero page key.  May need relocation: 

	if (key.unprep.oid == pageKey.unprep.oid) {
	  // This is a key to the old (zero) page.  Do an in-place
	  // swap for the new key, leaving all old attributes untouched.
	  key.unprep.oid = newPage.unprep.oid;
	}

	if (key.unprep.oid > pageKey.unprep.oid) {
	  // This is a key to a zero page that is AFTER the one we are
	  // removing. Decrement it's oidLo by 1:
	  key.unprep.oid = key.unprep.oid-1;
	}
      }
    }

    for (uint32_t dirNdx = 0; dirNdx < nDirEnt; dirNdx++) {
	DiskKey& key = dir[dirNdx].key;

	if (key.IsType(KtDataPage) == false || key.IsPrepared() == false)
	  continue;
	
	// It's a zero page key.  May need relocation: 

	if (key.unprep.oid == pageKey.unprep.oid) {
	  // This is a key to the old (zero) page.  Do an in-place
	  // swap for the new key, leaving all old attributes untouched.
	  key.unprep.oid = newPage.unprep.oid;
	}

	if (key.unprep.oid > pageKey.unprep.oid) {
	  // This is a key to a zero page that is AFTER the one we are
	  // removing. Decrement it's oidLo by 1:
	  key.unprep.oid = key.unprep.oid-1;
	}
    }
    
    // We have replaced a zero page with a nonZero page, and removed
    // all references to the old zero page, relocating accordingly.
    // We now have one less zero page:
    nZeroPages--;

    // Finally, we need to relocate the page key we were handed too!
    pageKey.unprep.oid = newPage.unprep.oid;
  }
  
  uint32_t pageNdx = pageKey.unprep.oid;
  uint8_t *pageContent = &pageImages[pageNdx*EROS_PAGE_SIZE];

  *((uint32_t *) &pageContent[offset]) = value;
}

void
ErosImage::PrintNode(const DiskKey& nodeKey)
{
  for (unsigned int i = 0; i < EROS_NODE_SIZE; i++) {
    DiskKey key = GetNodeSlot(nodeKey, i);

    if (key.IsZeroKey())
      continue;
    
    Diag::printf("  [%2d] ", i);
    PrintDiskKey(key);
    Diag::printf("\n");
  }
}

void
ErosImage::PrintPage(const DiskKey& pageKey)
{
  if (pageKey.IsPrepared()) {
    Diag::printf("Page OID=");
    Diag::print(pageKey.unprep.oid);
    Diag::printf(" flags=Z (zero page)\n");
  }
  else {
    Diag::printf("Page OID=");
    Diag::print(pageKey.unprep.oid);
    Diag::printf("\n");

    uint8_t *bufp = &pageImages[((uint32_t)pageKey.unprep.oid) * EROS_PAGE_SIZE];
    for (int i = 0; i < 8; i++) {
      Diag::printf("    ");
      for (int j = 0; j < 16; j++) {
	Diag::printf("%02x", *bufp);
	bufp++;
      }
      Diag::printf("\n");
    }
    Diag::printf("...\n");
  }
  Diag::printf("\n");
}

void
ErosImage::PrintCapPage(const DiskKey& capPageKey)
{
  if (capPageKey.IsPrepared()) {
    Diag::printf("CapPage OID=");
    Diag::print(capPageKey.unprep.oid);
    Diag::printf(" flags=Z (zero cap page)\n");
  }
  else {
    Diag::printf("CapPage OID=");
    Diag::print(capPageKey.unprep.oid);
    Diag::printf("\n");

    uint8_t *bufp =
      &capPageImages[((uint32_t)capPageKey.unprep.oid) * EROS_PAGE_SIZE];

    DiskKey* kbufp = (DiskKey*) bufp;
    
    for (int i = 0; i < 32; i += 2) {
      Diag::printf("  ");
      PrintDiskKey(kbufp[i]);
      Diag::printf("  ");
      PrintDiskKey(kbufp[i+1]);
      Diag::printf("\n");
    }
    Diag::printf("...\n");
  }
  Diag::printf("\n");
}

// Given a node key, pretend it's a domain and print it out:
void
ErosImage::PrintDomain(const DiskKey& domRoot)
{
  if (domRoot.IsNodeKeyType() == false)
    Diag::fatal(4, "Non-node key passed to PrintDomain\n");

#ifdef ProcGenRegs
  DiskKey genRegs = GetNodeSlot(domRoot, ProcGenRegs);
#endif
  DiskKey genKeys = GetNodeSlot(domRoot, ProcGenKeys);

  Diag::printf("Domain root:\n");
  for (unsigned int i = 0; i < EROS_NODE_SIZE; i++) {
    Diag::printf("  [%2d] ", i);
    DiskKey key = GetNodeSlot(domRoot, i);
    
    PrintDiskKey(key);

    // keeper key must be start key:
    if (i == ProcKeeper &&
	key.IsType(KtStart) == false &&
	key.IsType(KtNumber) == false)
      Diag::printf(" (malformed)");

    // address space key must be segmode key:
    if (i == ProcAddrSpace &&
	key.IsType(KtSegment) == false &&
	key.IsType(KtNode) == false &&
	key.IsType(KtDataPage) == false)
      Diag::printf(" (malformed)");
      
#ifdef ProcGenRegs
    if (i == ProcGenRegs && !key.IsNodeKeyType())
      Diag::printf(" (malformed)");
#endif
    if (i == ProcGenKeys && !key.IsNodeKeyType())
      Diag::printf(" (malformed)");
    
    Diag::printf("\n");
  }

#ifdef ProcGenRegs
  if (genRegs.IsNodeKeyType()) {
    Diag::printf("General Registers:\n");
    for (unsigned int i = 0; i < EROS_NODE_SIZE; i++) {
      Diag::printf("  [%2d] ", i);
      DiskKey key = GetNodeSlot(genRegs, i);
    
      PrintDiskKey(key);

      if (key.IsType(KtNumber) == false)
	Diag::printf(" (malformed)");

      Diag::printf("\n");
    }
  }
#endif

  if (genKeys.IsNodeKeyType()) {
    Diag::printf("General Keys:\n");
    for (unsigned int i = 0; i < EROS_NODE_SIZE; i++) {
      Diag::printf("  [%2d] ", i);
      DiskKey key = GetNodeSlot(genKeys, i);
    
      PrintDiskKey(key);
      Diag::printf("\n");
    }
  }
}

void
ErosImage::PrintSegment(const DiskKey& segKey)
{
  DoPrintSegment(0, segKey, 0, 0, false);
}

void
ErosImage::DoPrintSegment(uint32_t slot, const DiskKey& segKey,
			  uint32_t indent, const char *annotation,
			  bool startKeyOK)
{
  // Only print top-level number keys:
  if (indent && segKey.IsType(KtNumber) &&
      segKey.nk.value[2] == 0 && segKey.nk.value[1] == 0 &&
      segKey.nk.value[0] == 0)
    return;

  for (uint32_t i = 0; i < indent; i++) 
    Diag::printf("  ");

  switch(segKey.GetType()) {
  case KtNode:
  case KtSegment:
    Diag::printf("[%2d] :", slot);
    PrintDiskKey(segKey);
    if (annotation)
      Diag::printf(" (%s)", annotation);
    Diag::printf("\n");

    if ( segKey.IsRedSegmentKey() ) {
      DiskKey fmtKey = GetNodeSlot(segKey, RedSegFormat);

      uint32_t initialSlots = EROS_NODE_SIZE - 3;
      uint32_t bgslot = RedSegBackground;
      uint32_t kprslot = RedSegKeeper;
      
      bool goodFormatKey = fmtKey.IsValidFormatKey();

      if (goodFormatKey) {
	initialSlots = REDSEG_GET_INITIAL_SLOTS(fmtKey.nk);
	bgslot = REDSEG_GET_BG_SLOT(fmtKey.nk);
	kprslot = REDSEG_GET_KPR_SLOT(fmtKey.nk);
      }

      if (goodFormatKey == false) {
	for (int i = 0; i < RedSegFormat; i++) {
	  DiskKey key = GetNodeSlot(segKey, i);
	  DoPrintSegment(i, key, indent+1, 0, false);
	}
	DoPrintSegment(RedSegFormat, fmtKey, indent+1, "format, malformed", 0);

	return;
      }

      // Have valid format key and valid kpr/bg slots:
      
      for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
	char *annotation = 0;
	bool startKeyOK = false;
	
	DiskKey key = GetNodeSlot(segKey, i);
	if (i == bgslot)
	  annotation = "background";
	else if (i == kprslot) {
	  annotation = "keeper";
	  startKeyOK = true;
	}
	else if (i == RedSegFormat)
	  annotation = "format";
	
	DoPrintSegment(i, key, indent+1, annotation, startKeyOK);
      }
    }
    else {
      for (unsigned int i = 0; i < EROS_NODE_SIZE; i++) {
	DiskKey key = GetNodeSlot(segKey, i);
	DoPrintSegment(i, key, indent+1, 0, false);
      }
    }
    break;
  case KtDataPage:
  case KtNumber:
    Diag::printf("[%2d] ", slot);
    PrintDiskKey(segKey);
    if ((segKey.nk.value[0] & 3) == 3)
      Diag::printf(" (background window)");
    if (annotation)
      Diag::printf(" (%s)", annotation);
    Diag::printf("\n");
    break;
  case KtStart:
  case KtResume:
    {
      if (startKeyOK) {
	Diag::printf("[%2d] ", slot);
	PrintDiskKey(segKey);
	if (annotation)
	  Diag::printf(" (%s)", annotation);
	Diag::printf("\n");
	break;
      }
      /* fall through */
    }
  default:
    Diag::printf("[%2d] ", slot);
    PrintDiskKey(segKey);
    if (annotation)
      Diag::printf(" (%s, malformed)\n", annotation);
    else
      Diag::printf("(malformed)\n");
  }
}
