#ifndef __APP_HXX__
#define __APP_HXX__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <erosimg/Intern.hxx>
#include <erosimg/Diag.hxx>

class CDA;
class DiskKey;

// Application class
class App {
  InternedString appName;
  bool isAborting;
  bool isInteractive;
  uint32_t exitCode;
  
  struct FileList {
    char* fileName;
    int isScratch;
    FileList* next;
    
    FileList(const char* name);
    ~FileList();
  } ;
  FileList *fileList;

  void CleanFiles();
public:
  App(const InternedString name);
  ~App();

  const char* name()
  { return appName.str(); }
  
  void AddScratch(const char* name);
  void AddTarget(const char* name);

  // Can be called by destructors for short circuiting if desired.
  bool Aborting()
  {
    return isAborting;
  }

  void SetExitValue(uint32_t exitValue)
  {
    exitCode = exitValue;
  }
  
  void Exit();

  void ExitWithCode(uint32_t value)
  {
    SetExitValue(value);
  }

  void SetInteractive()
  {
    isInteractive = true;
  }
  bool IsInteractive()
  {
    return isInteractive;
  }

  static const char* BuildPath(const char*);
};

extern class App App;

#endif // __APP_HXX__
