/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <bfd.h>

#include <erosimg/App.hxx>
#include "Intern.hxx"
#include "ExecImage.hxx"

extern char* target;

bool
ExecImage::GetSymbolValue(const InternedString& symName, uint32_t& value)
{
  bfd *bfd_file;
  long symcount;
  PTR minisyms;
  unsigned int size;
  bfd_byte *from, *fromend;
  char **matching;
  
  bfd_init ();

  const char *imageFileName = App::BuildPath(name.str());

#if 0
  Diag::printf("Fetching symbols from \"%s\"\n", imageFileName);
#endif

  bfd_file = bfd_openr(imageFileName, "default");

#if 0
  Diag::printf("bfd * ix 0x%08x\n", bfd_file);
#endif

  if (bfd_check_format_matches (bfd_file, bfd_object, &matching) == 0) {
    Diag::printf("\"%s\" is not an executable\n", bfd_get_filename
		 (bfd_file));
    return false;
  }

  if (!(bfd_get_file_flags (bfd_file) & HAS_SYMS)) {
    Diag::printf("No symbols in \"%s\".\n", bfd_get_filename
		 (bfd_file));
    return false;
  }

  symcount = bfd_read_minisymbols (bfd_file, (bfd_boolean)0, &minisyms, &size);
  if (symcount < 0) {
    Diag::printf("File \"%s\" had no symbols.\n", name.str());
    return false;
  }

  from = (bfd_byte *) minisyms;
  fromend = from + symcount * size;

  asymbol *store = bfd_make_empty_symbol (bfd_file);

  for (; from < fromend; from += size) {
    asymbol *sym;

    sym = bfd_minisymbol_to_symbol (bfd_file, (bfd_boolean)0, from, store);
    if (sym == NULL)
      Diag::fatal(3, "Could not fetch symbol info!\n");

    if (strcmp(bfd_asymbol_name (sym), symName.str()) == 0) {
      symbol_info syminfo;

      bfd_get_symbol_info (bfd_file, sym, &syminfo);

      value = syminfo.value;
      return true;
    }
  }

  return false;
}

