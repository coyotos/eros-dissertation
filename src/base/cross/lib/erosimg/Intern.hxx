#ifndef __INTERN_HXX__
#define __INTERN_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

class InternedString {
  struct InternEntry {
    unsigned signature;
    char *string;
    InternEntry *pNext;
  };
  
  static InternEntry **internTable;

  static unsigned Signature(const char *s, int len);

  // These return index into internTable:
  static InternEntry* Lookup(const char *s, int len, unsigned strsig);
  static InternEntry* Intern(const char *);
  static InternEntry* Intern(const char *, int len);

  InternEntry* pEntry;
public:
  InternedString()
  { pEntry = 0; }
  InternedString(const char *str)
  { pEntry = Intern(str); }
  InternedString(const char *str, int len)
  { pEntry = Intern(str, len); }
  InternedString(const InternedString& other)
  { pEntry = other.pEntry; }

  bool operator==(const InternedString& other) const
  { return pEntry == other.pEntry; }
  bool operator!=(const InternedString& other) const
  { return pEntry != other.pEntry; }

  InternedString& operator=(const InternedString& other)
  { pEntry = other.pEntry; return *this; }

  const char* str() const
  { return pEntry->string; }

  unsigned Signature() const
  { return pEntry->signature; }
  
  operator bool() const
  { return pEntry ? true : false; }
};

#endif // __INTERN_HXX__
