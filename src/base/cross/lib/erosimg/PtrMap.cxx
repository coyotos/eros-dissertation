/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>

#include <eros/target.h>
#include <erosimg/PtrMap.hxx>

template <class Value>
PtrMap<Value>::~PtrMap()
{
  delete [] ptrs;
}

// Inefficient as hell, but it works:
template <class Value>
bool PtrMap<Value>::Contains(const void *ptr)
{
  for (uint32_t i = 0; i < nPtrs; i++)
    if (ptrs[i].ptr == ptr)
      return true;

  return false;
}

// Inefficient as hell, but it works:
template <class Value>
bool
PtrMap<Value>::Lookup(const void *ptr, Value& w)
{
  for (uint32_t i = 0; i < nPtrs; i++)
    if (ptrs[i].ptr == ptr) {
      w = ptrs[i].value;
      return true;
    }

  return false;
}

template <class Value>
void
PtrMap<Value>::do_add(const void *ptr, const Value& value)
{
  if (nPtrs >= maxPtrs) {
    maxPtrs += 1024;

    vw* newptrs = new vw[maxPtrs];
    if (ptrs)
      memcpy(newptrs, ptrs, nPtrs * sizeof(void *)); 

    delete [] ptrs;
    ptrs = newptrs;
  }

  ptrs[nPtrs].ptr = ptr;
  ptrs[nPtrs++].value = value;
}

// HAND EXPANSIONS:

template class PtrMap<uint32_t>;
template class PtrMap<uint64_t>;
template class PtrMap<void *>;

#include <disk/DiskKey.hxx>
template class PtrMap<DiskKey>;
