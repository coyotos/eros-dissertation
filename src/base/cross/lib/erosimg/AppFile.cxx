/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <unistd.h>
#include <erosimg/App.hxx>
  
void
App::CleanFiles()
{
  while (fileList) {
    if (fileList->isScratch || exitCode)
      unlink(fileList->fileName);
    FileList* cur = fileList;
    fileList = fileList->next;

    delete cur;
  }
}

void
App::AddScratch(const char* fileName)
{
  FileList *fl = new FileList(fileName);
  fl->next = fileList;
  fileList = fl;
  fl->isScratch = 1;
}

void
App::AddTarget(const char* fileName)
{
  FileList *fl = new FileList(fileName);
  fl->next = fileList;
  fileList = fl;
  fl->isScratch = 0;
}

App::FileList::FileList(const char* name)
{
  fileName = new char[strlen(name)+1];
  strcpy(fileName, name);
  isScratch = 0;
}

App::FileList::~FileList()
{
  delete fileName;
}
