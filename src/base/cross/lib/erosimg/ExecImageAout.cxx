/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <sys/fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <a.out.h>

#include <erosimg/App.hxx>
#include <erosimg/Parse.hxx>
#include <erosimg/Intern.hxx>
#include <erosimg/ExecImage.hxx>


bool ExecImage::InitAout()
{
  struct exec& exehdr = *((struct exec*) image);

  switch (N_MAGIC(exehdr)) {
  case OMAGIC:			// linked with -N
    imageTypeName = "OMAGIC";
    break;
  case NMAGIC:			// linked with -n
    imageTypeName = "NMAGIC";
    break;
  case ZMAGIC:			// linked normally
    imageTypeName = "ZMAGIC";
    break;
#ifdef QMAGIC
  case QMAGIC:			// linux qmagic - demand paged with
				// exec header in text region, page
				// 0 unmapped. 
    imageTypeName = "QMAGIC";
    Diag::fatal(0, "Do not use LINUX qmagic format.\n");
#endif
  default:
    return false;
  }


  entryPoint = exehdr.a_entry;

  nRegions = 0;

  if (N_MAGIC(exehdr) == OMAGIC) {
    // Text+Data+BSS in one region, RWX:
    nRegions = 1;
    regions = new ExecRegion[nRegions];
    regions[0].perm = ExecRegion::ER_R | ExecRegion::ER_W | ExecRegion::ER_X;
    regions[0].vaddr = exehdr.a_entry;
    regions[0].memsz = exehdr.a_text + exehdr.a_data + exehdr.a_bss;
    regions[0].filesz = exehdr.a_text + exehdr.a_data;
    regions[0].offset = N_TXTOFF(exehdr);
  }
  else {
    // All other formats use distinct permissions, bss abutted to
    // data.
    if (exehdr.a_text)
      nRegions++;
    if (exehdr.a_data || exehdr.a_bss)
      nRegions++;
    
    regions = new ExecRegion[nRegions];

    nRegions = 0;
    if (exehdr.a_text) {
      regions[nRegions].perm = ExecRegion::ER_R | ExecRegion::ER_X;
      regions[nRegions].vaddr = exehdr.a_entry;
      regions[nRegions].memsz = exehdr.a_text;
      regions[nRegions].filesz = exehdr.a_text;
      regions[nRegions].offset = N_TXTOFF(exehdr);
      nRegions++;
    }
    if (exehdr.a_data || exehdr.a_bss) {
      regions[nRegions].perm = ExecRegion::ER_R | ExecRegion::ER_W;
      regions[nRegions].vaddr = exehdr.a_entry;
      regions[nRegions].memsz = exehdr.a_data + exehdr.a_bss;
      regions[nRegions].filesz = exehdr.a_data;
      regions[nRegions].offset = N_TXTOFF(exehdr) + exehdr.a_text;
      nRegions++;
    }
  }

  return true;
}
