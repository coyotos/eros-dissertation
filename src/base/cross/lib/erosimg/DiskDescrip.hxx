#ifndef __DISKDESCRIP_HXX__
#define __DISKDESCRIP_HXX__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>

struct DiskDescrip {
    static DiskDescrip Descriptors[];

  public:
    const char *diskName;
    unsigned long secsize;
    uint32_t heads;
    uint32_t secs;
    uint32_t cyls;
    unsigned long badsecs;	/* default number of sectors to 
				 * reserve for bad sector handling
				 */
    const char* imageName;

    uint32_t TotSectors() const
      { return secs * heads * cyls; }
    
    static const DiskDescrip* Lookup(const char* type);
    static const DiskDescrip* GetDefault();
} ;


#endif // __DISKDESCRIP_HXX__
