/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <erosimg/Intern.hxx>
#include <erosimg/ExecArch.hxx>
#include <erosimg/Diag.hxx>

ExecArch::Architecture
ExecArch::FromString(InternedString& name)
{
  if (name == InternedString("i486")) {
    return ExecArch::arch_i486;
  }
  else {
    Diag::fatal(1, "Unknown architecture \"%s\"\n", name.str());
  }

  return ExecArch::arch_unknown;
}

static const char * const byteSexNames[] = {
  "unknown"
  "big endian",			// big endian
  "little endian",		// little endian
  "permuted",			// PDP-11, but you never know when it
				// will reappear
  "we32k",			// western electric 32000 family (data 
				// little, code big)
};

struct ArchInfo {
  uint32_t byteSex;
  const char *name;
};

static const ExecArch::ArchInfo archInfo[ExecArch::NUM_ARCH] = {
  { ExecArch::bs_unknown,  "unknown" },
  { ExecArch::bs_unknown,  "neutral" },
  { ExecArch::bs_unknown,  "i486" }
} ;

const char*
ExecArch::GetByteSexName(uint32_t bs)
{
  if (bs >= ExecArch::NUM_BS)
    return byteSexNames[bs_unknown];
  
  return byteSexNames[bs];  
}

const ExecArch::ArchInfo&
ExecArch::GetArchInfo(uint32_t arch)
{
  if (arch >= ExecArch::NUM_ARCH)
    return archInfo[arch_unknown];
  
  return archInfo[arch];
}
