#ifndef __EROSIMAGE_HXX__
#define __EROSIMAGE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <disk/DiskKey.hxx>
#include <erosimg/StringPool.hxx>
#include <erosimg/Intern.hxx>
#include <erosimg/ExecArch.hxx>

#define EROS_IMAGE_VERSION 3

// An EROS Image is a *relocatable* image of a named object.  The
// image file holds some structured object, identified by the key in
// the header, and some number of nodes and pages.  The nodes and
// pages are assumed to be numbered starting at CDA=0 and have rising
// consecutive CDA's.
//
// Every ErosImage file has a name in it's header, which is the
// human-readable name of the ErosImage object.  It may someday become
// useful to allow multiple named objects in an ErosImage file, in
// which event we shall have to add a table.
//
// Most keys in an ErosImage file are numbered relative to the image
// file, and have their 'rHazard' bit set to indicate this.  Absolute
// keys do not have their 'rHazard' bit set.
//
// Pages that hold only zeros are not actually stored in the file.
// Such pages are written with ascending CDA's beginning at
//    0x8000 0000 0000
//
// Keys to such pages have their 'rHazard' bit set.
//
// THE FOLLOWING PARAGRAPH IS OBSOLETE, BUT IS RETAINED TO CAPTURE
// INTENT SO WE CAN REVISIT IT:
//
// ErosImage files may also contain references to objects not included
// in the image.  Such objects are indicated by name.  Keys to those
// objects have their 'wHazard' bit set, and the cdalo field of the
// key gives an index into the string table.  It is expected that the
// sysgen utility will resolve such references when the ErosImage
// files are all bound together into a system image.
//

struct ErosHeader {
  char		signature[8];	// "ErosImg\0"
  uint32_t 	imageByteSex;	// 0 == little endian, else big endian
  uint32_t	version;	// image file version
  uint32_t	architecture;

  struct Directory {
    uint32_t	name;		// index into string pool
    DiskKey	key;
  };

  uint32_t	nDirEnt;	// number of directory entries
  uint32_t	dirOffset;	// location in file of image directory.

  uint32_t	nThread;	// number of thread entries
  uint32_t	threadOffset;	// location in file of thread directory
  
  uint32_t	nPages;		// total number of page images
  uint32_t	nZeroPages;	// total number of zero page images
  uint32_t	pageOffset;	// location in file of first page description

  uint32_t	nCapPages;	// total number of capability page images
  uint32_t	nZeroCapPages;	// total number of zero capability page images
  uint32_t	capPageOffset;	// location in file of first page description

  uint32_t	nNodes;		// total number of page images
  uint32_t	nodeOffset;	// location in file of first page description

  uint32_t	strSize;	// size of string table
  uint32_t	strTableOffset;	// file offset of string table

  ErosHeader();
};

class ErosImage : public ErosHeader {
protected:
  StringPool pool;
  
  uint8_t *pageImages;
  uint8_t *capPageImages;
  struct DiskNode *nodeImages;
  struct Directory *dir;
  struct Directory *threadDir;

  uint32_t maxPage;
  uint32_t maxCapPage;
  uint32_t maxNode;
  uint32_t maxDir;
  uint32_t maxThread;
  
  void ValidateImage(const char* target);
  bool DoGetPageInSegment(const DiskKey& segRoot,
			  uint64_t segOffset,
			  DiskKey& pageKey);

  DiskKey DoAddPageToBlackSegment(const DiskKey& segRoot,
				  uint64_t segOffset,
				  const DiskKey& pageKey,
				  uint64_t path,
				  bool expandRedSegment);

  DiskKey DoAddSubsegToBlackSegment(const DiskKey& segRoot,
				    uint64_t segOffset,
				    const DiskKey& segKey);

  void DoPrintSegment(uint32_t slot, const DiskKey&, uint32_t indentLevel,
		      const char *annotation, bool startKeyOK);
  
  void GrowNodeTable(uint32_t newMax);
  void GrowPageTable(uint32_t newMax);
  void GrowCapPageTable(uint32_t newMax);
  void GrowDirTable(uint32_t newMax);
  void GrowThreadTable(uint32_t newMax);
public:
  ErosImage();
  ~ErosImage();

  const StringPool& GetStringPool() const
  { return pool; }

  void SetArchitecture(ExecArch::Architecture);

  InternedString GetString(int ndx) const;

  void WriteToFile(const char *target);
  void ReadFromFile(const char *source);

  // Add the respective objects, returning a key to the object added.
  // Set the write bit in the disk key if so requested.

  DiskKey AddDataPage(const uint8_t *buf, bool readOnly = false);
  DiskKey AddZeroDataPage(bool readOnly = false);

  // Don't the buf ptrs want to be DiskKey*?
  DiskKey AddCapPage(const uint8_t *buf, bool readOnly = false);
  DiskKey AddZeroCapPage(bool readOnly = false);

  DiskKey AddNode(bool readOnly = false);

  // Name is unnecessary -- it is included only for use in listings.
  void AddThread(const char *name, const DiskKey&);
  void AddThread(const InternedString& name, const DiskKey&);
  bool GetThreadEnt(const char *name, DiskKey&);
  bool GetThreadEnt(const InternedString& name, DiskKey&);
  
  void GetReserve(uint32_t index, struct CpuReserve& rsrv);
  void SetReserve(const struct CpuReserve& rsrv);
  
  void AddDirEnt(const char *name, const DiskKey& key);
  void AddDirEnt(const InternedString& name, const DiskKey& key);
  // Assign is like add, but will over-write old entry if there is one.
  void AssignDirEnt(const char *name, const DiskKey& key);
  void AssignDirEnt(const InternedString& name, const DiskKey& key);
  bool GetDirEnt(const char *name, DiskKey&);
  bool GetDirEnt(const InternedString& name, DiskKey&);
  void SetDirEnt(const InternedString& name, const DiskKey& key);
  bool DelDirEnt(const char *name);
  bool DelDirEnt(const InternedString& name);

#if 0
  // Import the contents of another image into this one.
  void Import(const ErosImage& image);
#endif
  
  // We cannot just hand out a DiskNode&, because the disk node images
  // move around.  It's easier to manipulate things this way instead:
  void SetNodeSlot(const DiskKey& nodeKey, uint32_t slot, const DiskKey& key);
  DiskKey GetNodeSlot(const DiskKey& nodeKey, uint32_t slot);
  DiskKey GetNodeSlot(uint32_t nodeNdx, uint32_t slot);

  // Construction support for segments.  Given a segment root key and
  // an offset, hand back a new segment root key:
  DiskKey AddPageToSegment(const DiskKey& segRoot,
			   uint64_t segOffset,
			   const DiskKey& pageKey);

  DiskKey AddSubsegToSegment(const DiskKey& segRoot,
			     uint64_t segOffset,
			     const DiskKey& pageKey);

  bool GetPageInSegment(const DiskKey& segRoot,
			uint64_t segOffset,
			DiskKey& pageKey);

  void SetPageWord(DiskKey& pageKey, uint32_t offset, uint32_t value);

  void PrintDomain(const DiskKey&);
  void PrintSegment(const DiskKey&);
  void PrintNode(const DiskKey&);
  void PrintPage(const DiskKey&);
  void PrintCapPage(const DiskKey&);

  // Return by value, since directory can move:
  Directory GetDirEnt(uint32_t ndx) const
  { return dir[ndx]; }
  Directory GetThreadEnt(uint32_t ndx) const
  { return threadDir[ndx]; }
  void GetDataPageContent(uint32_t pageNdx, uint8_t *buf);
  void GetCapPageContent(uint32_t pageNdx, DiskKey* buf);
  void GetNodeContent(uint32_t nodeNdx, DiskNode& node);
};

#endif // __EROSIMAGE_HXX__
