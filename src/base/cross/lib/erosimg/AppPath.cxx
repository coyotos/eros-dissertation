/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <erosimg/App.hxx>

const char * DEFAULT_BUILD_ROOT = "/eros";

char buf[PATH_MAX];

const char* App::BuildPath(const char* path)
{
  if (strncmp(path, "/eros/", 6) != 0) {
    strcpy(buf, path);
  }
  else {
    const char *buildroot = getenv("EROS_ROOT");
    if (buildroot == 0)
      buildroot = DEFAULT_BUILD_ROOT;

    strcpy(buf, buildroot);
    strcat(buf, &path[5]);
  }

  return buf;
}
