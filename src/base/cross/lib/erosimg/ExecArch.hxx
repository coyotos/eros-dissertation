#ifndef __EXECARCH_HXX__
#define __EXECARCH_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

struct ExecArch {
  struct ArchInfo {
    uint32_t byteSex;
    const char *name;
  };
  
  // ALWAYS ADD TO THE END OF THESE ENUMS TO ENSURE COMPATIBILITY
  enum ByteSex {
    bs_unknown,
    bs_big,			// big endian
    bs_little,			// little endian
    bs_permuted,		// PDP-11 permuted - you never know
				// when it will reappear
    bs_we32k,			// WE32000 family (data little, code 
				// big) -- this was REALLY dumb.
    NUM_BS
  };

  enum Architecture {
    arch_unknown,
    arch_neutral,
    arch_i486,
    NUM_ARCH
  };

  static Architecture FromString(InternedString& name);
  static const char* GetByteSexName(uint32_t);
  static const ArchInfo& GetArchInfo(uint32_t);
} ;

#endif // __EXECARCH_HXX__
