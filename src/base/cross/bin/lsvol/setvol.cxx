/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <getopt.h>
#include <stdlib.h>
#include <erosimg/App.hxx>
#include <erosimg/ExecImage.hxx>
#include <erosimg/Volume.hxx>
#include <erosimg/DiskDescrip.hxx>

class App App("setvol");
const char* targname;

int wantRamDisk = 0;
int wantCompress = 0;
int wantBoot = 0;
int wantDebug = 0;
const char *kernel_name = 0;
const char *boot_name = 0;
#if 0
int showbad = 0;
#endif

int main(int argc, char *argv[])
{
  int c;
  extern int optind;
  extern char *optarg;
  int opterr = 0;
  
  while ((c = getopt(argc, argv, "dDzZrRk:b:B")) != -1) {
    switch(c) {
    case 'r':
      wantRamDisk = 1;
      break;
    case 'R':
      wantRamDisk = -1;
      break;
    case 'z':
      wantCompress = 1;
      break;
    case 'Z':
      wantCompress = -1;
      break;
    case 'k':
      kernel_name = optarg;
      break;
    case 'b':
      wantBoot = 1;
      boot_name = optarg;
      break;
    case 'B':
      wantBoot = -1;
      boot_name = 0;
      break;
    case 'd':
      wantDebug = 1;
      break;
    case 'D':
      wantDebug = -1;
      break;
    default:
      opterr++;
    }
  }
  
      /* remaining arguments describe node and/or page space divisions */
  argc -= optind;
  argv += optind;
  
  if (wantCompress == 0 && wantRamDisk == 0
      && kernel_name == 0 && wantBoot == 0
      && wantDebug == 0)
    opterr++;
  
  if (argc != 1)
    opterr++;
  
  if (opterr)
    Diag::fatal(1, "Usage: setvol [-r|-R] [-z|-Z] [-d|-D] [-b image|-B] [-k kernel_name] file\n");
  
  targname = *argv;
  
  Volume vol;
  
  vol.Open(targname, false);
  
  if (kernel_name) {
    ExecImage kernelImage;
    if ( !kernelImage.SetImage(kernel_name) ) {
      Diag::error(1, "Couldn't load kernel image\n");
      return 1;
    }
      
    if (kernelImage.NumRegions() != 1) {
      Diag::error(1, "%s: kernel image improperly linked. Use '-n'!\n",
		  kernel_name);
      return 1;
    }

    for (int i = 0; i < vol.MaxDiv(); i++) {
      const Division& d = vol.GetDivision(i);
      if (d.type == dt_Kernel)
	vol.WriteKernelImage(i, kernelImage);
    }
  }
  
  if (wantBoot != 0)
    vol.WriteBootImage(boot_name);
  
  if (wantRamDisk > 0)
    vol.SetVolFlag(VolHdr::VF_RAMDISK);
  else if (wantRamDisk < 0) {
    vol.ClearVolFlag(VolHdr::VF_COMPRESSED);
    vol.ClearVolFlag(VolHdr::VF_RAMDISK);
  }
  
  if (wantDebug > 0)
    vol.SetVolFlag(VolHdr::VF_DEBUG);
  else if (wantDebug < 0) {
    vol.ClearVolFlag(VolHdr::VF_DEBUG);
  }
  
  if (wantCompress > 0) {
    const VolHdr& vh = vol.GetVolHdr();
    if ((vh.BootFlags & VolHdr::VF_RAMDISK) == 0)
      Diag::error(1,
		  "Compressed volumes must presently be "
		  "ramdisks. Use '-r'!\n");
    vol.SetVolFlag(VolHdr::VF_COMPRESSED);
  }
  else if (wantCompress < 0)
    vol.ClearVolFlag(VolHdr::VF_COMPRESSED);

  vol.Close();

  App.Exit();
}
