/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <erosimg/App.hxx>
#include <erosimg/Volume.hxx>

void
PrintCkptDir(Volume& vol)
{
  Diag::printf("Checkpoint header:\n");
  Diag::printf("  sequenceNumber        0x%08x%08x\n",
	       (uint32_t) (vol.curDskCkpt->sequenceNumber >> 32),
	       (uint32_t) vol.curDskCkpt->sequenceNumber);
  Diag::printf("  hasMigrated           %c\n",
	       vol.curDskCkpt->hasMigrated ? 'y' : 'n');
  Diag::printf("  maxLogLid             0x%x\n",
	       vol.curDskCkpt->maxLogLid);
  Diag::printf("  nDirPage              %d\n",
	       vol.curDskCkpt->nDirPage);
  Diag::printf("  nThreadPage           %d\n",
	       vol.curDskCkpt->nThreadPage);
  Diag::printf("  nReservePage          %d\n",
	       vol.curDskCkpt->nRsrvPage);
		 
  uint32_t rsrvStart = 0;
  uint32_t rsrvEnd = vol.curDskCkpt->nRsrvPage;
  uint32_t thrdStart = rsrvEnd;
  uint32_t thrdEnd = thrdStart + vol.curDskCkpt->nThreadPage;
  uint32_t dirStart = thrdEnd;
  uint32_t dirEnd = dirStart + vol.curDskCkpt->nDirPage;

  Diag::printf("\nCheckpoint reserve pages at log locations:\n");
  for (uint32_t pg = rsrvStart; pg < rsrvEnd; ) {
    for (; pg < rsrvEnd; pg++)
      Diag::printf("  0x%08x", vol.curDskCkpt->dirPage[pg]);
    Diag::printf("\n");
  }

  Diag::printf("\nCheckpoint thread pages at log locations:\n");
  for (uint32_t pg = thrdStart; pg < thrdEnd; ) {
    for (; pg < thrdEnd; pg++)
      Diag::printf("  0x%08x", vol.curDskCkpt->dirPage[pg]);
    Diag::printf("\n");
  }

  Diag::printf("\nCheckpoint directory pages at log locations:\n");
  for (uint32_t pg = dirStart; pg < dirEnd; ) {
    for (; pg < dirEnd; pg++)
      Diag::printf("  0x%08x", vol.curDskCkpt->dirPage[pg]);
    Diag::printf("\n");
  }

  Diag::printf("\nReserves:\n");
  for (uint32_t i = 0; i < vol.NumReserve(); i++) {
    CpuReserveInfo r = vol.GetReserve(i);
    if (r.normPrio == -2 && r.rsrvPrio == -2)
      continue;
    
    Diag::printf("[%3d] period 0x%08x%08x duration 0x%08x%08x\n"
		 "     quanta 0x%08x%08x normPrio %d rsrvPrio %d\n",
		 r.index,
		 (uint32_t) (r.period >> 32),
		 (uint32_t) (r.period),
		 (uint32_t) (r.duration >> 32),
		 (uint32_t) (r.duration),
		 (uint32_t) (r.quanta >> 32),
		 (uint32_t) (r.quanta),
		 r.normPrio,
		 r.rsrvPrio);
  }

  Diag::printf("\nThreads:\n");
  for (uint32_t i = 0; i < vol.NumThread(); i++) {
    ThreadDirent tde = vol.GetThread(i);
    Diag::printf("OID=0x%08x%08x  ac=0x%08x  ",
		 (uint32_t) (tde.oid>>32), (uint32_t) tde.oid, tde.allocCount);
    if (i%2 == 1)
      Diag::printf("\n");
  }
  if (vol.NumThread() % 2 != 0)
    Diag::printf("\n");

  Diag::printf("\nP/N OID                     AllocCount LogLoc\n");
  for (uint32_t i = 0; i < vol.NumDirent(); i++) {
    CkptDirent de = vol.GetDirent(i);

    char pageNode = (de.type == FRM_TYPE_NODE) ? 'N' : 'P';
      
    const char *cty = "??";
    switch(de.type) {
    case FRM_TYPE_ZDPAGE:
      cty = "zd";
      break;
    case FRM_TYPE_DPAGE:
      cty = "dp";
      break;      
    case FRM_TYPE_ZCPAGE:
      cty = "zc";
      break;
    case FRM_TYPE_CPAGE:
      cty = "cp";
      break;      
    case FRM_TYPE_NODE:
      cty = "nd";
      break;
    }
    
    if (de.lid == ZERO_LID)
      Diag::printf("%c   %s OID=0x%08x%08x  %-8d   <zero>\n",
		   pageNode,
		   cty,
		   (uint32_t) (de.oid >> 32),
		   (uint32_t) de.oid,
		   de.count);
    else if (de.lid == UNDEF_LID)
      Diag::printf("%c   %s OID=0x%08x%08x  %-8d   <UNDEF!>\n",
		   pageNode,
		   cty,
		   (uint32_t) (de.oid >> 32),
		   (uint32_t) de.oid,
		   de.count);
    else 
      Diag::printf("%c   %s OID=0x%08x%08x  %-8d   0x%08x\n",
		   pageNode,
		   cty,
		   (uint32_t) (de.oid >> 32),
		   (uint32_t) de.oid,
		   de.count,
		   de.lid);
  }
}
