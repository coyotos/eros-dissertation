/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <erosimg/App.hxx>
#include <erosimg/Volume.hxx>

void
PrintDivTable(Volume& vol)
{
  Diag::printf("%-4s %-8s %-8s %-8s %-10s\n",
	       "Div", "Start", "End", "Size", "Type/Info");
    
  int i;
  for (i = 0; i < vol.MaxDiv(); i++) {
    const Division& d = vol.GetDivision(i);
      
    switch(d.type) {
    case dt_Boot:
    case dt_DivTbl:
    case dt_Spare:
    case dt_FailStart:
      Diag::printf("%-4d %-8d %-8d %-8d %-10s\n",
		   i, d.start, d.end, d.end - d.start, d.TypeName());
      break;
    case dt_Object:
    case dt_Log:
    case dt_Kernel:
      Diag::printf("%-4d %-8d %-8d %-8d %-10s\n%32sOID=[",
		   i, d.start, d.end, d.end-d.start, d.TypeName(), "");
      Diag::print(d.startOid);
      Diag::printf(", ");
      Diag::print(d.endOid);
      Diag::printf(")\n");
      break;
    }
  }
}
