/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <erosimg/App.hxx>
#include <erosimg/Volume.hxx>

extern void PrintDiskKey(const DiskKey&);

void
PrintVolHdr(Volume& vol)
{
  const VolHdr& vh = vol.GetVolHdr();

  Diag::printf("Volume header:\n");
#if 0
  Diag::printf("  sectors        %d\n", vh.sectors);
  Diag::printf("  cylinders      %d\n", vh.cylinders);
  Diag::printf("  heads          %d\n", vh.heads);
  Diag::printf("  Total Sectors  %d\n", vh.totSectors);
#endif
  Diag::printf("  %-20s %d\n", "Hdr Version", vh.HdrVersion);
  Diag::printf("  %-20s %d\n", "Page Size", vh.PageSize);
  Diag::printf("  %-20s %d\n", "Pri Div Tbl", vh.DivTable);
  Diag::printf("  %-20s %d\n", "Alt Div Tbln", vh.AltDivTable);

  Diag::printf("  %-20s 0x%02x", "Flags", vh.BootFlags);

  if (vh.BootFlags) {
    Diag::printf(" [");
    bool haveshown = false;
    if (vh.BootFlags & VolHdr::VF_BOOT) {
      Diag::printf("%sBOOT", haveshown ? "," : "");
      haveshown = true;
    }
    if (vh.BootFlags & VolHdr::VF_RAMDISK) {
      Diag::printf("%sRAMDISK", haveshown ? "," : "");
      haveshown = true;
    }
    if (vh.BootFlags & VolHdr::VF_COMPRESSED) {
      Diag::printf("%sCOMPRESSED", haveshown ? "," : "");
      haveshown = true;
    }
    if (vh.BootFlags & VolHdr::VF_IPL) {
      Diag::printf("%sIPL", haveshown ? "," : "");
      haveshown = true;
    }
    Diag::printf("]\n");
  }
  else
    Diag::printf("\n");

  Diag::printf("  %-20s %d\n", "Boot Sectors", vh.BootSectors);
  Diag::printf("  %-20s %d\n", "Vol Sectors", vh.VolSectors);
  Diag::printf("  %-20s %d\n", "Zipped Length", vh.zipLen);
  Diag::printf("  %-20s '%c%c%c%c'\n", "Signature",
	       vh.signature[0],
	       vh.signature[1],
	       vh.signature[2],
	       vh.signature[3]
	       );
  Diag::printf("  %-20s ", "IPL key:");
  PrintDiskKey(vh.iplKey);
  Diag::printf("\n");
  Diag::printf("  %-20s 0x%08x%08x\n", "IPL sysid:",
	       (uint32_t) (vh.iplSysId >> 32), (uint32_t) vh.iplSysId);
  Diag::printf("\n");
}
