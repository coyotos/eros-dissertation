/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include <disk/DiskNode.hxx>
#include <disk/PagePot.hxx>

#include <erosimg/App.hxx>
#include <erosimg/Parse.hxx>
#include <erosimg/ExecImage.hxx>
#include <erosimg/Volume.hxx>
#include <erosimg/DiskDescrip.hxx>

class App App("mkvol");
Volume vol;
  
const char* targname;
const char* volmap;
const char *bootName = 0;
const char *kernelName = 0;

void ProcessVolMap()
{
  FILE* f = fopen(App::BuildPath(volmap), "r");
  
  if (!f)
    Diag::fatal(1, "Couldn't open volume map file\n");
  
  char buf[EROS_PAGE_SIZE];
  int line = 0;
  
  while(fgets(buf, EROS_PAGE_SIZE, f)) {
    InternedString fileName;
    uint32_t sz;
    OID oid;
    line++;
    
    Parse::TrimLine(buf);
    const char* rest = buf;
    
    // blank lines and lines containing only comments are fine:
    if (*rest == 0)
      continue;
    
    if (Parse::MatchStart(rest, buf) &&
	Parse::MatchKeyword(rest, "kernel") &&
	Parse::MatchWord(rest, sz) &&
	Parse::MatchOID(rest, oid) &&
	Parse::MatchEOL(rest) ) {

      sz *= EROS_PAGE_SECTORS;
      
      int kerndiv = vol.AddDivision(dt_Kernel, sz, oid);

      if (kernelName) {
	ExecImage kernelImage;
	if ( !kernelImage.SetImage(kernelName) )
	  App.ExitWithCode(1);
      
	if (kernelImage.NumRegions() != 1)
	  Diag::fatal(1, "%s: kernel image improperly linked. Use '-n'!\n",
		      fileName.str());

#if 0
	if (!sz) {
	  sz = kernelImage.GetRegion(0).filesz;
	  sz = (sz + (EROS_PAGE_SIZE - 1)) / EROS_PAGE_SIZE;
	}
#endif

	vol.WriteKernelImage(kerndiv, kernelImage);
      }
    }
    else if (Parse::MatchStart(rest, buf) &&
	     Parse::MatchKeyword(rest, "spare") &&
	     Parse::MatchWord(rest, sz) &&
	     Parse::MatchEOL(rest) ) {
      if (!sz)
	Diag::fatal(1, "%s, line %d: division size needed for spare division.\n",
		    volmap, line); 
	
      // round up to a page worth!
      if (sz % EROS_PAGE_SECTORS) {
	sz -= (sz % EROS_PAGE_SECTORS);
	sz += EROS_PAGE_SECTORS;
      }
      vol.AddDivision(dt_Spare, sz);
    }
    else if (Parse::MatchStart(rest, buf) &&
	     Parse::MatchKeyword(rest, "object") &&
	     Parse::MatchWord(rest, sz) &&
	     Parse::MatchOID(rest, oid) &&
	     Parse::MatchEOL(rest) ) {
      vol.AddDivision(dt_Object, sz * EROS_PAGE_SECTORS, oid);
    }
    else if (Parse::MatchStart(rest, buf) &&
	     Parse::MatchKeyword(rest, "cklog") &&
	     Parse::MatchWord(rest, sz) &&
	     Parse::MatchOID(rest, oid) &&
	     Parse::MatchEOL(rest) ) {

      int diskPages = sz;
      vol.AddDivision(dt_Log, diskPages * EROS_PAGE_SECTORS, oid);
    }
    else if (Parse::MatchStart(rest, buf) &&
	     Parse::MatchKeyword(rest, "ramdisk") &&
	     Parse::MatchEOL(rest) ) {
      vol.SetVolFlag(VolHdr::VF_RAMDISK);
    }
    else if (Parse::MatchStart(rest, buf) &&
	     Parse::MatchKeyword(rest, "compressed") &&
	     Parse::MatchEOL(rest) ) {
      vol.SetVolFlag(VolHdr::VF_COMPRESSED);
    }
    else if (Parse::MatchStart(rest, buf) &&
	     Parse::MatchKeyword(rest, "divtable") &&
	     Parse::MatchEOL(rest) ) {
      int diskPages = (sz + (DISK_NODES_PER_PAGE - 1)) / DISK_NODES_PER_PAGE;
      vol.AddDivision(dt_DivTbl, diskPages * EROS_PAGE_SECTORS, oid);
    }
    else {
      Diag::fatal(1, "%s, line %d: syntax error.\n", volmap, line);
    }
  }

  fclose(f);
}

int main(int argc, char *argv[])
{
  int c;
  extern int optind;
  extern char *optarg;
  int opterr = 0;
  
  while ((c = getopt(argc, argv, "b:k:")) != -1) {
    switch(c) {
    case 'b':
      bootName = optarg;
      break;
    case 'k':
      kernelName = optarg;
      break;
      
    default:
      opterr++;
    }
  }
  
  /* remaining arguments describe node and/or page space divisions */
  argc -= optind;
  argv += optind;
  
  if (argc != 2)
    opterr++;
  
  if (opterr)
    Diag::fatal(1, "Usage: mkvol -b bootimage -k kernimage volmap volume-file\n");
  
  volmap = argv[0];
  targname = argv[1];

  if (!vol.Create(targname, bootName))
    Diag::fatal(2, "Couldn't open target file \"%s\"\n", targname);
  
  if (volmap)
    ProcessVolMap();
  
  vol.Close();
  
  App.Exit();
}
