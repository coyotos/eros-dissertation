/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <disk/DiskNode.hxx>
#include <disk/PagePot.hxx>

#include <erosimg/App.hxx>
#include <erosimg/Parse.hxx>
#include <erosimg/Volume.hxx>
#include <erosimg/ErosImage.hxx>
#include <new.h>

class App App("sysgen");
Volume vol;

const char* targname;
const char* erosimage;
uint32_t nThreads = 0;
FILE *map_file = 0;

inline uint32_t
min(uint32_t w0, uint32_t w1)
{
  return (w0 < w1) ? w0 : w1;
}

void
RelocateKey(DiskKey& key, OID nodeBase, OID pageBase,
	    OID capPageBase, uint32_t nPages, uint32_t nCapPages)
{
  if ( key.IsType(KtDataPage) ) {
    OID oid = pageBase + (key.unprep.oid * EROS_OBJECTS_PER_FRAME);

    if (key.IsPrepared()) {
      key.InitType(KtDataPage);
      key.SetUnprepared();
      oid += (nPages * EROS_OBJECTS_PER_FRAME);
    }

    assert (oid < 0x100000000llu);
    
    key.unprep.oid = oid;
  }
  else if ( key.IsType(KtCapPage) ) {
    OID oid = capPageBase + (key.unprep.oid * EROS_OBJECTS_PER_FRAME);

    if (key.IsPrepared()) {
      key.InitType(KtCapPage);
      key.SetUnprepared();
      oid += (nCapPages * EROS_OBJECTS_PER_FRAME);
    }

    assert (oid < 0x100000000llu);

    key.unprep.oid = oid;
  }
  else if (key.IsNodeKeyType()) {
    OID oid = key.unprep.oid;
    OID frame = oid / DISK_NODES_PER_PAGE;
    OID offset = oid % DISK_NODES_PER_PAGE;
    frame *= EROS_OBJECTS_PER_FRAME;
    frame += nodeBase; /* JONADAMS: add in the node base */
    oid = frame + offset;

    assert (oid < 0x100000000llu);

    key.unprep.oid = oid;
  }
}

int
main(int argc, char *argv[])
{
  int c;
  extern int optind;
  extern char *optarg;
  int opterr = 0;
  
  while ((c = getopt(argc, argv, "m:")) != -1) {
    switch(c) {
    case 'm':
      map_file = fopen(optarg, "w");
      if (map_file == NULL)
	return 0;
      
      App.AddTarget(optarg);
      break;
    default:
      opterr++;
    }
  }
  
  argc -= optind;
  argv += optind;
  
  if (argc != 2)
    opterr++;
  
  if (opterr)
    Diag::fatal(1, "Usage: sysgen [-m mapfile] volume-file eros-image\n");
  
  targname = argv[0];
  erosimage = argv[1];
  
  if ( !vol.Open(targname, true) )
    Diag::fatal(1, "Could not open \"%s\"\n", targname);
  
  vol.ResetVolume();
  
  ErosImage image;
  image.ReadFromFile(erosimage);

  uint32_t nSubMaps        = 0; // number of pages of submaps                   
  
  uint32_t nObjectRange = 0;
  for (int i = 0; i < vol.MaxDiv(); i++) {
    const Division& d = vol.GetDivision(i);
    if (d.type == dt_Object) {
      RangeKey rk(d.startOid, d.endOid);
      NodeKey nk((OID)0);
      image.SetNodeSlot(nk, 3 + nObjectRange, rk);

      if (d.startOid == 0) {
	// FIX: this should use OBCOUNT_MAX
	if (d.endOid - d.startOid >= (uint64_t) UINT32_MAX)
	  Diag::fatal(1, "Object range w/ start OID=0x0 too "
		      "large for sysgen\n");
	  
	/* store information about the size of the maps that need
	 * to be set up for the SpaceBank.
	 */
#define DIVRNDUP(x,y) (((x) + (y) - 1)/(y))
        uint32_t framesInRange = (d.endOid-d.startOid)/EROS_OBJECTS_PER_FRAME;

	nSubMaps = DIVRNDUP(framesInRange,8*EROS_PAGE_SIZE);
#undef DIVRNDUP
      }

      nObjectRange++;
    }
  }
    
  uint32_t nPages = image.nPages;
  uint32_t nZeroPages = image.nZeroPages;
  uint32_t nCapPages = image.nCapPages;
  uint32_t nZeroCapPages = image.nZeroCapPages;
  uint32_t nNodes = image.nNodes;
  uint32_t nThread = image.nThread;
  DiskKey key;
  
  uint32_t nodeFrames = (nNodes + DISK_NODES_PER_PAGE - 1) / DISK_NODES_PER_PAGE;
  uint32_t pageFrames = nPages + nZeroPages;

  // Originally, Jon Adams had this doing the map initialization
  // here.  That functionality has been moved to the space bank.  All
  // we need to do here is make sure that we pre-allocate the right
  // number of frames so that when the space bank initializes the free
  // frame list it won't step on anything important.
  
  OID nodeBase = EROS_OBJECTS_PER_FRAME * nSubMaps;
  OID pageBase = nodeBase + (EROS_OBJECTS_PER_FRAME * nodeFrames);
  OID capPageBase = pageBase + (EROS_OBJECTS_PER_FRAME * pageFrames);

  // Copy all of the nodes, relocating the page key and cappage key
  // OID's appropriately:
  for (uint32_t ndx = 0; ndx < nNodes; ndx++) {
    DiskNode node;

    image.GetNodeContent(ndx, node);

    // Relocate zero page keys:
    for (uint32_t slot = 0; slot < EROS_NODE_SIZE; slot++) {
      DiskKey& key = node[slot];

      RelocateKey(key, nodeBase, pageBase, capPageBase, nPages, nCapPages);
    }
    
    OID frame = ndx / DISK_NODES_PER_PAGE;
    OID offset = ndx % DISK_NODES_PER_PAGE;
    frame *= EROS_OBJECTS_PER_FRAME;
    frame += nodeBase; /* JONADAMS: add in the node base */
    OID oid = frame + offset;

    node.oid = oid;
    
    vol.WriteNode(oid, node);

    if (map_file != NULL)
      fprintf(map_file, "image node ndx 0x%lx => disk node oid 0x%08lx%08lx\n",
	      ndx, (uint32_t) (oid >> 32), (uint32_t) oid);
  }

  // Write the contentful pages:
  for (uint32_t ndx = 0; ndx < nPages; ndx++) {
    uint8_t buf[EROS_PAGE_SIZE];
    
    image.GetDataPageContent(ndx, buf);

    OID oid = (ndx * EROS_OBJECTS_PER_FRAME) + pageBase;
    vol.WriteDataPage(oid, buf);

    if (map_file != NULL)
      fprintf(map_file, "image dpage ndx 0x%lx => disk page oid 0x%08lx%08lx\n",
	      ndx, (uint32_t) (oid >> 32), (uint32_t) oid);
  }

  // Zero the non-contentful pages:
  for (uint32_t ndx = 0; ndx < nZeroPages; ndx++) {
    // Following is redundant, but useful until zero pages are
    // implemented:
    
    uint8_t buf[EROS_PAGE_SIZE];
    memset(buf, 0, EROS_PAGE_SIZE);
    
    OID oid = ((ndx + nPages) * EROS_OBJECTS_PER_FRAME) + pageBase;
    vol.WriteDataPage(oid, buf);

    if (map_file != NULL)
      fprintf(map_file, "image zdpage ndx 0x%lx => disk page oid 0x%08lx%08lx\n",
	      ndx, (uint32_t) (oid >> 32), (uint32_t) oid);

#if 0
    PagePot pagePot;
    pagePot.flags = PagePot::ZeroPage;
    
    vol.WritePagePotEntry(oid, pagePot);
#endif
  }

  // Write the contentful capability pages:
  for (uint32_t ndx = 0; ndx < nCapPages; ndx++) {
    DiskKey kbuf[EROS_PAGE_SIZE/sizeof(DiskKey)];
    
    image.GetCapPageContent(ndx, kbuf);

    OID oid = (ndx * EROS_OBJECTS_PER_FRAME) + capPageBase;
    vol.WriteCapPage(oid, kbuf);

    if (map_file != NULL)
      fprintf(map_file, "image cpage ndx 0x%lx => disk page oid 0x%08lx%08lx\n",
	      ndx, (uint32_t) (oid >> 32), (uint32_t) oid);
  }

  // Zero the non-contentful capability pages:
  for (uint32_t ndx = 0; ndx < nZeroCapPages; ndx++) {
    // Following is redundant, but useful until zero pages are
    // implemented:
    
    DiskKey kbuf[EROS_PAGE_SIZE/sizeof(DiskKey)];
    
    OID oid = ((ndx + nCapPages) * EROS_OBJECTS_PER_FRAME) + capPageBase;
    vol.WriteCapPage(oid, kbuf);

    if (map_file != NULL)
      fprintf(map_file, "image zcpage ndx 0x%lx => disk page oid 0x%08lx%08lx\n",
	      ndx, (uint32_t) (oid >> 32), (uint32_t) oid);
  }

  InternedString ipls("IPL");

  for (uint32_t ndx = 0; ndx < nThread; ndx++) {
    ErosImage::Directory d;
    d = image.GetThreadEnt(ndx);

    DiskKey schedKey = image.GetNodeSlot(d.key, ProcSched);

    InternedString name = image.GetString(d.name);
    if (name == ipls)
      continue;

    OID oldOID = d.key.unprep.oid;
    
    RelocateKey(d.key, nodeBase, pageBase, capPageBase, nPages, nCapPages);

    if (map_file != NULL)
      fprintf(map_file, "image thread ndx 0x%08lx => vol thread oid 0x%08lx%08lx\n",
		 (uint32_t) oldOID,
		 (uint32_t) (d.key.unprep.oid >> 32), (uint32_t) d.key.unprep.oid);

    Diag::printf("Adding thread. Image oid 0x%08x%08x => vol oid 0x%08x%08x\n",
		 (uint32_t) (oldOID >>32), (uint32_t) oldOID,
		 (uint32_t) (d.key.unprep.oid >> 32), (uint32_t) d.key.unprep.oid);
    vol.AddThread(d.key.unprep.oid, d.key.unprep.count, schedKey.subType);
    nThreads++;
  }
  
  // See if there is an IPL entry:
  if (image.GetThreadEnt("IPL", key)) {
    RelocateKey(key, nodeBase, pageBase, capPageBase, nPages, nCapPages);
    vol.SetIplKey(key);
  }
  else if (nThreads == 0)
    Diag::printf("Warning: no running domains!\n");
  
  if (map_file != NULL)
    fclose(map_file);

  vol.Close();
  
  App.Exit();
}
