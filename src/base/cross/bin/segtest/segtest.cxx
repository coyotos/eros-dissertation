/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <sys/fcntl.h>
#include <stdio.h>

#include <getopt.h>
#include <stdlib.h>

#include <erosimg/App.hxx>
#include <erosimg/ErosImage.hxx>
#include <erosimg/Parse.hxx>

extern void PrintDiskKey(const DiskKey&);

class App App("segtest");

int main(int argc, char *argv[])
{
  int c;
  extern int optind;
#if 0
  extern char *optarg;
#endif
  int opterr = 0;
  
  while ((c = getopt(argc, argv, "")) != -1) {
    switch(c) {
    default:
      opterr++;
    }
  }
  
  argc -= optind;
  argv += optind;
  
  if (argc != 0)
    opterr++;
  
  if (opterr)
    Diag::fatal(1, "Usage: segtest\n");
  
  ErosImage ei;

  char buf[EROS_PAGE_SIZE];

  DiskKey segKey = NumberKey(0);

  for(;;) {
    printf("root key, offset: ");
    fflush(stdout);

    if (!fgets(buf, EROS_PAGE_SIZE, stdin))
      continue;
    
    Parse::TrimLine(buf);
    const char* rest = buf;
    
    // blank lines and lines containing only comments are fine:
    if (*rest == 0)
      continue;
    
    DiskKey rootKey;
    uint32_t w;

    if (Parse::MatchStart(rest, buf) &&
	Parse::MatchKey(rest, rootKey) &&
	Parse::Match(rest, ",") &&
	Parse::MatchWord(rest, w) &&
	Parse::MatchEOL(rest) ) {

      printf("Add page at offset 0x%x to segment ", (unsigned) w);
      fflush(stdout);
      PrintDiskKey(rootKey);
      printf("\n");

      DiskKey pageKey = ei.AddZeroDataPage();
      segKey = ei.AddPageToSegment(rootKey, w, pageKey);
      ei.PrintSegment(segKey);
    }
    if (Parse::MatchStart(rest, buf) &&
	Parse::Match(rest, "$") &&
	Parse::Match(rest, ",") &&
	Parse::MatchWord(rest, w) &&
	Parse::MatchEOL(rest) ) {

      printf("Add page at offset 0x%x to segment ", (unsigned) w);
      fflush(stdout);
      PrintDiskKey(segKey);
      printf("\n");

      DiskKey pageKey = ei.AddZeroDataPage();
      segKey = ei.AddPageToSegment(segKey, w, pageKey);
      ei.PrintSegment(segKey);
    }
  }

  App.Exit();
}
