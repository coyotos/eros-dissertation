%{
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <sys/fcntl.h>
#include <sys/stat.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include <eros/target.h>
#include <erosimg/App.hxx>
#include <erosimg/ErosImage.hxx>
#include <erosimg/ExecImage.hxx>
#include <erosimg/PtrMap.hxx>
#include <erosimg/Parse.hxx>
#include <eros/Key.h>
#include <eros/ProcessState.h>
#include <disk/DiskKey.hxx>
#include <disk/DiskLSS.hxx>
#include <disk/MiscKeyType.hxx>

/* Made this a structure to avoid construction problems */
#include "ParseType.h"

bool showparse = false;

#define SHOWPARSE(s) if (showparse) Diag::printf(s)
#define SHOWPARSE1(s,x) if (showparse) Diag::printf(s,x)
     
extern void PrintDiskKey(const DiskKey&);

class App App("mkimage");

InternedString NoArch("Unknown Architecture");
InternedString CurArch = NoArch;

InternedString current_file("<stdin>");
int current_line = 1;
extern FILE *yyin;

int num_errors = 0;  /* hold the number of syntax errors encountered. */

void ShowImageDirectory(const ErosImage& image);
void ShowImageThreadDirectory(const ErosImage& image);

bool
AddProgramSegment(ErosImage& image,
		  InternedString fileName,
		  DiskKey &key);

bool
GetProgramSymbolValue(InternedString fileName,
		      InternedString symName,
		      uint32_t& value);

bool
AddRawSegment(ErosImage& image,
	      InternedString fileName,
	      DiskKey &key);

bool
AddZeroSegment(ErosImage& image,
	       DiskKey &key,
	       uint32_t nPages);

bool
AddEmptySegment(ErosImage& image,
	       DiskKey &key,
	       uint32_t nPages);

DiskKey
AddDomain(ErosImage& image);

bool
GetMiscKeyType(InternedString s, uint32_t& ty);

bool
strtonkv(InternedString& is, NumKeyValue& nkv);

/* returns false on error */
bool QualifyKey(uint32_t, DiskKey in, DiskKey& out);

#define ATTRIB_RO    0x1
#define ATTRIB_NC    0x2
#define ATTRIB_WEAK  0x4

#define YYSTYPE ParseType

extern void yyerror(const char *);
extern int yylex();
extern int yylex (YYSTYPE *lvalp);

ErosImage image;

#if EROS_NODE_SIZE == 16
uint32_t DomRootRestriction[EROS_NODE_SIZE] = {
  RESTRICT_SCHED,		/* dr00: schedule slot */
  RESTRICT_START,		/* dr01: keeper slot */
  RESTRICT_SEGMODE,		/* dr02: address space */
  RESTRICT_ZERO,		/* dr03: reserved */
  RESTRICT_START,		/* dr04: brand */
  RESTRICT_NUMBER,		/* dr05: trap_code */
  RESTRICT_NUMBER,		/* dr06: rcv descrip register */
  RESTRICT_NUMBER,		/* dr07: snd descrip register */
  RESTRICT_NUMBER,		/* dr08: inv ctl register */
  RESTRICT_NUMBER,		/* dr09: pc, sp register */
  0,				/* dr10: architecture defined */
  0,				/* dr11: architecture defined */
  0,				/* dr12: architecture defined */
  RESTRICT_ZERO,		/* dr13: reserved -- alt msg buf */
  RESTRICT_GENREGS,		/* dr14: gen regs */
  RESTRICT_KEYREGS,		/* dr15: key regs */
};
#elif EROS_NODE_SIZE == 32
uint32_t DomRootRestriction[EROS_NODE_SIZE] = {
  RESTRICT_SCHED,		/* dr00: schedule slot */
  RESTRICT_START,		/* dr01: keeper slot */
  RESTRICT_SEGMODE,		/* dr02: address space */
  RESTRICT_ZERO,		/* dr03: reserved */
  RESTRICT_START,		/* dr04: brand */
  RESTRICT_NUMBER,		/* dr05: trap_code */
  RESTRICT_ZERO,		/* dr06: reserved -- alt msg buf */
  0,				/* dr07: snd descrip register */
  0,				/* dr08: inv ctl register */
  RESTRICT_NUMBER,		/* dr09: pc, sp register */
  0,				/* dr10: architecture defined */
  0,				/* dr11: architecture defined */
  0,				/* dr12: architecture defined */
  0,				/* dr13: architecture defined */
  0,				/* dr14: architecture defined */
  0,				/* dr15: architecture defined */
  0,				/* dr16: architecture defined */
  0,				/* dr17: architecture defined */
  0,				/* dr18: architecture defined */
  0,				/* dr19: architecture defined */
  0,				/* dr20: architecture defined */
  0,				/* dr21: architecture defined */
  0,				/* dr22: architecture defined */
  0,				/* dr23: architecture defined */
  0,				/* dr24: architecture defined */
  0,				/* dr25: architecture defined */
  0,				/* dr26: architecture defined */
  0,				/* dr27: architecture defined */
  0,				/* dr28: architecture defined */
  0,				/* dr29: architecture defined */
  0,				/* dr30: architecture defined */
  RESTRICT_KEYREGS,		/* dr31: key regs */
};
#endif

uint32_t CalcRedSegRestriction(DiskKey segKey, uint32_t slot);
bool CheckRestriction(uint32_t restriction, DiskKey key);

%}

/* Following will not work until I hand-rewrite the lexer */
/* %pure_parser */

%token <NONE> RO NC WEAK SENSE NODE PAGE NEW DOMAIN HIDE PROGRAM SMALL
/* %token <NONE> IMPORT */
%token <NONE> SEGMENT SEGTREE NULLKEY ZERO WITH PAGES KW_BLSS RED EMPTY
%token <NONE> ARCH PRINT SPACE REG KEEPER START BACKGROUND PRIORITY
%token <NONE> BRAND GENREG SLOT ROOT OREQ KEY SUBSEG AT CLONE
%token <NONE> PC SP ALL SLOTS KEYS STRING LIMIT IPL RUN AS
%token <NONE> RANGE NUMBER SCHED MISC VOLSIZE DIRECTORY PRIME
%token <NONE> SYMBOL CAPABILITY
%token <is> NAME FILENAME HEX OCT BIN DEC WORD

%type <key> key segkey domain startkey
%type <key> qualified_key node
%type <key> schedkey numberkey
/* Following are bare key names of appropriate type -- need to be
   distinguished to avoid shift/reduce errors: */
%type <key> segmode slot
%type <w> qualifier segtype blss number keyData slotno
%type <w> offset priority
%type <nk> numkey_value
%type <oid> oid
%type <rd> arch_reg

%%

start:  /* empty */
        | start stmt
	;

stmt:   stmt ';' 
            { if (App.IsInteractive())
	        ShowImageDirectory(image);
	      SHOWPARSE("=== line -> stmt \\n\n"); }
        | error ';' {
	  yyerrok;
	}
	;

stmt:	/* IMPORT FILENAME {
	   SHOWPARSE("=== stmt -> IMPORT FILENAME\n");
	   ErosImage importImage;
	   importImage.ReadFromFile($2.str());

	   image.Import(importImage);
        }

       | */ HIDE NAME {
	   SHOWPARSE("=== stmt -> HIDE NAME\n");

	   if (image.DelDirEnt($2) == false) {
	     Diag::printf("%s:%d: \"%s\" is not in the image directory\n",
			 current_file.str(), current_line, $2.str());
	     num_errors++;
	     YYERROR;
	   }
        }

       | NAME '=' qualified_key {
	 /* no restrictions */
	   SHOWPARSE("=== stmt -> NAME = qualified_key\n");
	   image.AssignDirEnt($1, $3);
        }

       | slot '=' qualified_key {
	   SHOWPARSE("=== stmt -> slot = key\n");

	   if ( !CheckRestriction($<restriction>1, $3) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   image.SetNodeSlot($1, $<slot>1, $3);
        }

       | PRINT FILENAME {
	   /* Really print quoted string... */
	   SHOWPARSE("=== stmt -> PRINT FILENAME\n");
	   Diag::printf("%s\n", $2.str());
        }

       | PRINT DIRECTORY {
	   SHOWPARSE("=== stmt -> PRINT DIRECTORY\n");
	   ShowImageDirectory(image);
        }

       | PRINT key {
	   SHOWPARSE("=== stmt -> PRINT key\n");
      
	   PrintDiskKey($2);
	   Diag::printf("\n");
        }

       | PRINT SEGMENT segkey {
	   SHOWPARSE("=== stmt -> PRINT SEGMENT segkey\n");
	   image.PrintSegment($3);
        }

       | PRINT PAGE key {
	   SHOWPARSE("=== stmt -> PRINT PAGE key\n");
	   if ($3.IsType(KtDataPage) == false) {
	     Diag::printf("%s:%d: must be page key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   image.PrintPage($3);
        }

       | PRINT NODE key {
	   SHOWPARSE("=== stmt -> PRINT NODE key\n");

	   if ($3.IsNodeKeyType() == false) {
	     Diag::printf("%s:%d: must be node key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   image.PrintNode($3);
        }

       | PRINT DOMAIN key {
	   SHOWPARSE("=== stmt -> PRINT DOMAIN key\n");
      
	   if ($3.IsNodeKeyType() == false) {
	     Diag::printf("%s:%d: must be node key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   image.PrintDomain($3);
        }

       | ARCH NAME {
	   SHOWPARSE("=== stmt -> ARCH NAME\n");
	   ExecArch::Architecture arch = ExecArch::FromString($2);

	   if (arch == ExecArch::arch_unknown) {
	     Diag::printf("%s:%d: unknown architecture \"%s\".\n",
			 current_file.str(), current_line, $2.str());
	     num_errors++;
	     YYERROR;
	   }
      
	   CurArch = $2;
        }

       | node SPACE '=' segkey {
	   SHOWPARSE("=== stmt -> domain SPACE = segkey\n");
	   if ( !CheckRestriction(DomRootRestriction[ProcAddrSpace], $4) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   image.SetNodeSlot($1, ProcAddrSpace, $4);
        }

       | node PC '=' number {
	   SHOWPARSE("=== stmt -> domain PC = number\n");
	   DiskKey key = image.GetNodeSlot($1, ProcPCandSP);
	   if (key.IsType(KtNumber) == false) {
	     Diag::printf("%s:%d: Slot did not hold number key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   key.nk.value[0] = $4;
	   image.SetNodeSlot($1, ProcPCandSP, key);
        }

       | node SP '=' number {
	   SHOWPARSE("=== stmt -> domain SP = number\n");
	   DiskKey key = image.GetNodeSlot($1, ProcPCandSP);
	   if (key.IsType(KtNumber) == false) {
	     Diag::printf("%s:%d: Slot did not hold number key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   key.nk.value[1] = $4;
	   image.SetNodeSlot($1, ProcPCandSP, key);
        }

       | node PC '=' numberkey {
	   SHOWPARSE("=== stmt -> domain PC = numberkey\n");
	   if ( !CheckRestriction(DomRootRestriction[ProcPCandSP], $4) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   DiskKey k = image.GetNodeSlot($1, ProcPCandSP);
	   k.nk.value[0] = $4.nk.value[0];
	   image.SetNodeSlot($1, ProcPCandSP, k);
        }

       | node PRIORITY '=' schedkey {
	   SHOWPARSE("=== stmt -> domain PRIORITY = schedkey\n");
	   if ( !CheckRestriction(DomRootRestriction[ProcSched], $4) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   image.SetNodeSlot($1, ProcSched, $4);
        }

       | node BRAND '=' startkey {
	   SHOWPARSE("=== stmt -> domain BRAND = startkey\n");
	   if ( !CheckRestriction(DomRootRestriction[ProcBrand], $4) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   image.SetNodeSlot($1, ProcBrand, $4);
        }

       | node DOMAIN KEEPER '=' startkey {
	   SHOWPARSE("=== stmt -> domain BRAND = startkey\n");
	   if ( !CheckRestriction(DomRootRestriction[ProcKeeper], $5) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   image.SetNodeSlot($1, ProcKeeper, $5);
        }

       | node SEGMENT KEEPER '=' startkey {
	   SHOWPARSE("=== stmt -> domain BRAND = startkey\n");
	   if ( !CheckRestriction(RESTRICT_START, $5) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   image.SetNodeSlot($1, RedSegKeeper, $5);
	   DiskKey key = image.GetNodeSlot($1, RedSegFormat);
	   REDSEG_SET_KPR_SLOT(key.nk, RedSegKeeper);
	   image.SetNodeSlot($1, RedSegFormat, key);
        }

       | node BACKGROUND SEGMENT '=' segkey {
	   SHOWPARSE("=== stmt -> node BACKGROUND SEGMENT = segkey\n");

	   if ( !CheckRestriction(RESTRICT_SEGMODE, $5) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   image.SetNodeSlot($1, RedSegBackground, $5);
	   DiskKey key = image.GetNodeSlot($1, RedSegFormat);
	   REDSEG_SET_BG_SLOT(key.nk, RedSegBackground);
	   image.SetNodeSlot($1, RedSegFormat, key);
        }

       | slot OREQ numberkey {
	   SHOWPARSE("=== stmt -> slot |= numberkey\n");
	   DiskKey key = image.GetNodeSlot($1, $<slot>1);

	   if ( !CheckRestriction($<restriction>1, $3) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   if (key.IsType(KtNumber) == false) {
	     Diag::printf("%s:%d: Operator '|=' requires a number key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   key.nk.value[2] |= $3.nk.value[2];
	   key.nk.value[1] |= $3.nk.value[1];
	   key.nk.value[0] |= $3.nk.value[0];

	   image.SetNodeSlot($1, $<slot>1, $3);
        }

       | node KEY REG slotno '=' qualified_key {
	   SHOWPARSE("=== stmt -> domain KEY REG keyreg_slotno = qualified_key\n");
	   if ( !CheckRestriction($4 ? 0 : RESTRICT_ZERO, $6) ) {
	     Diag::printf("%s:%d: key does not meet slot restriction\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
	   DiskKey genKeys = image.GetNodeSlot($1, ProcGenKeys);
	   image.SetNodeSlot(genKeys, $4, $6);
        }

       | segmode offset '=' WORD number {
	   SHOWPARSE("=== stmt -> segment offset = WORD number\n");
	   DiskKey pageKey;
      
	   if (image.GetPageInSegment($1, $2, pageKey) == false) {
	     pageKey = image.AddZeroDataPage();
	     image.AddPageToSegment($1, $2, pageKey);
	   }
	  
	   uint32_t pageOffset = $2 & EROS_PAGE_MASK;
	   image.SetPageWord(pageKey, pageOffset, $5);
        }

/*
       | segmode SLOT slot '=' key {
	   SHOWPARSE("=== stmt -> node SLOT slot = key\n");
	   image.SetNodeSlot($1, $3, $5);
        }
	*/
       | segmode ALL SLOTS '=' key {
	   SHOWPARSE("=== stmt -> node ALL SLOTS = key\n");
	   for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) 
	     image.SetNodeSlot($1, i, $5);
        }
       | IPL domain {
	   SHOWPARSE("=== stmt -> IPL domain\n");

	   /* Set the domain to the "running" state (runState == 1) */
	   DiskKey key = image.GetNodeSlot($2, ProcTrapCode);
	   key.nk.value[2] &= key.nk.value[2] & 0xffffff00;
	   key.nk.value[2] |= RS_Running;
	   image.SetNodeSlot($2, ProcTrapCode, key);
	   
	   image.AddThread("IPL", $2);
        }

       | RUN domain {
	   SHOWPARSE("=== stmt -> RUN domain\n");

	   /* Set the domain to the "running" state (runState == 1) */
	   DiskKey key = image.GetNodeSlot($2, ProcTrapCode);
	   key.nk.value[2] &= key.nk.value[2] & 0xffffff00;
	   key.nk.value[2] |= RS_Running;
	   image.SetNodeSlot($2, ProcTrapCode, key);
      
	   image.AddThread($<is>2, $2);
        }

       | domain REG arch_reg '=' HEX {
	   SHOWPARSE("=== stmt -> domain REG arch_reg = HEX\n");
	   uint32_t valueLen = strlen($5.str() - 2);
	   if (valueLen > $3->len * 2) {
	     Diag::printf("%s:%d: value \"%s\" exceeds "
			  "register size.\n",
			  current_file.str(), current_line, $5.str());
	     num_errors++;
	     YYERROR;
	   }
	   $3->WriteValue(image, $1, $5);
        }
        ;

qualified_key: qualifier key {
          if ( !QualifyKey($1, $2, $$) ) {
	     num_errors++;
	     YYERROR;
	  }
        }
       ;

key:   NULLKEY {
	  SHOWPARSE("=== key -> NULLKEY\n");
	  $$ = NumberKey(0);
	}

       | VOLSIZE {
	  SHOWPARSE("=== key -> VOLSIZE \n");
	  OID oid = 0x0;
	  $$ = NodeKey(oid);
	 }
       | NUMBER '(' numkey_value ')' {
	  SHOWPARSE("=== key -> NUMBER ( numkey_value )\n");
	  $$ = NumberKey($3.hi, $3.mid, $3.lo);
	 }
       | RANGE '(' oid ':' oid ')' {
	  SHOWPARSE("=== key -> RANGE ( oid : oid )\n");
	  $$ = RangeKey($3, $5);
	 }
       | PRIME RANGE {
	  SHOWPARSE("=== key -> PRIME RANGE\n");
	  OID lo = 0;
	  OID hi = UINT64_MAX;
	  $$ = RangeKey(lo, hi);
	 }
       | SCHED '(' priority ')' {
	  SHOWPARSE("=== key -> SCHED ( priority )\n");
	  $$ = SchedKey($3);
         }
       | MISC NAME {
	  SHOWPARSE("=== key -> MISC NAME\n");
	  uint32_t miscType;
	  if (GetMiscKeyType($2, miscType) == false) {
	    Diag::printf("%s:%d: unknown misc key type \"%s\"\n",
			 current_file.str(), current_line, $2.str());
	    num_errors++;
	    YYERROR;
	  }
	  
	  $$ = MiscKey(miscType);
         }
       | segtype FILENAME {
	  SHOWPARSE("=== key -> segtype FILENAME\n");
	  DiskKey key;
	  if ( !AddRawSegment(image, $2, key) ) {
	    num_errors++;
	    YYERROR;
	  }

	  if ($1)
	    key.SetType(KtSegment);
	  $$ = key;
       }

       | SYMBOL FILENAME NAME {
	   SHOWPARSE("=== key -> FILENAME NAME\n");

	   uint32_t new_pc;
	   
	   DiskKey key;
	   
	   if ( GetProgramSymbolValue($2, $3, new_pc) ) {
	     key = NumberKey(0);
	   }
	   else {
	     Diag::printf("%s:%d: Image \"%s\" did not have symbol \"%s\"\n",
			 $2.str(), $3.str());
	     num_errors++;
	     YYERROR;
	   }

	   $$ = key;
        }

       | PROGRAM segtype FILENAME {
	   SHOWPARSE("=== key -> = PROGRAM segtype FILENAME\n");

	   DiskKey key;

	   if (! AddProgramSegment(image, $3, key) ) {
	     num_errors++;
	     YYERROR;
	   }

	   if ($2) {
	     if ( key.IsType(KtDataPage) || key.IsType(KtCapPage) ) {
	       DiskKey segKey = image.AddNode();
	       segKey.SetBlss(key.GetBlss() + 1);
	       segKey.InitType(KtSegment);
	       segKey.SetUnprepared();

	       image.SetNodeSlot(segKey, 0, key);

	       key = segKey;
	     }
      
	     key.SetType(KtSegment);
	   }

	   $$ = key;
        }

       | SMALL PROGRAM FILENAME {
	   SHOWPARSE("=== key -> SMALL PROGRAM FILENAME\n");

	   DiskKey key;

	   if (! AddProgramSegment(image, $3, key) ) {
	     num_errors++;
	     YYERROR;
	   }

	   if (key.GetBlss() > (EROS_PAGE_BLSS + 1)) {
	     Diag::printf("%s:%d: binary image too large for small program\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   if ( key.IsType(KtDataPage) || key.IsType(KtCapPage) ) {
	     DiskKey segKey = image.AddNode();
	     segKey.SetBlss(key.GetBlss() + 1);

	     image.SetNodeSlot(segKey, 0, key);

	     key = segKey;
	   }

	   $$ = key;
        }

       | ZERO segtype WITH number PAGES {
	  SHOWPARSE("=== key -> ZERO segtype WITH number PAGES\n");
	  DiskKey key;

	  if ( !AddZeroSegment(image, key, $4) ) {
	    num_errors++;
	    YYERROR;
	  }
	  
	  if ($2)
	    key.SetType(KtSegment);
	  $$ = key;
       }
       | EMPTY segtype WITH number PAGES {
	  SHOWPARSE("=== key -> EMPTY segtype WITH number PAGES\n");
	  DiskKey key;

	  if ( !AddEmptySegment(image, key, $4) ) {
	    num_errors++;
	    YYERROR;
	  }
	  
	  if ($2)
	    key.SetType(KtSegment);
	  $$ = key;
       }
       | RED segtype WITH KW_BLSS blss {
	  SHOWPARSE("=== key -> RED segtype WITH KW_BLSS blss\n");
	  DiskKey key;

	  key = image.AddNode();

	  /* Fabricate format key with 13 initial slots, normal (node key)
	     invocation convention, no background key or keeper key. */
	  DiskKey fmtKey = NumberKey(0);
	  REDSEG_SET_INITIAL_SLOTS(fmtKey.nk, EROS_NODE_SIZE-3);
	  REDSEG_SET_RESERVED_SLOTS(fmtKey.nk, EROS_NODE_SIZE-1);
	  image.SetNodeSlot(key, RedSegFormat, fmtKey);
	  image.SetNodeSlot(key, RedSegBackground, NumberKey(0));
      
	  if ($2)
	    key.SetType(KtSegment);
	  key.SetBlss(1);

	  $$ = key;
       }

       | NEW qualifier NODE {
	  SHOWPARSE("=== key -> NEW qualifier NODE\n");
	  DiskKey key = image.AddNode();

	  SHOWPARSE("Reduce NAME = qualifier NODE\n");
	  if ( !QualifyKey($2, key, key) ) {
	    num_errors++;
	    YYERROR;
	  }

	  $$ = key;
        }

       | NEW qualifier PAGE {
	  SHOWPARSE("=== key -> NEW qualifier PAGE\n");
	  DiskKey key = image.AddZeroDataPage();

	  if ( !QualifyKey($2, key, key) ) {
	    num_errors++;
	    YYERROR;
	  }

	  $$ = key;
        }

       | NEW qualifier CAPABILITY PAGE {
	  SHOWPARSE("=== key -> NEW qualifier CAPABILITY PAGE\n");
	  DiskKey key = image.AddZeroCapPage();

	  if ( !QualifyKey($2, key, key) ) {
	    num_errors++;
	    YYERROR;
	  }

	  $$ = key;
        }

       | NAME {
	  SHOWPARSE("=== key -> qualifier NAME\n");
	  DiskKey key;
      
	  if (image.GetDirEnt($1, key) == false) {
	    Diag::printf("%s:%d: unknown object \"%s\"\n",
			 current_file.str(), current_line, $1.str());
	    num_errors++;
	    YYERROR;
	  }
      
	  $$ = key;
        }

       | CLONE node {
	  SHOWPARSE("=== key -> CLONE key\n");
	  DiskKey nodeKey = $2;
      
	  DiskKey key = image.AddNode();
	  for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
	    DiskKey tmp = image.GetNodeSlot(nodeKey, i);
	    image.SetNodeSlot(key, i, tmp);
	  }

	  $$ = key;
        }

       | key WITH KW_BLSS keyData {
	  SHOWPARSE("=== key -> key NAME KW_BLSS keyData\n");
      
	  DiskKey key = $1;

	  /*

	  if (image.GetDirEnt($1, key) == false) {
	    Diag::printf("%s:%d: unknown object \"%s\"\n",
			 current_file.str(), current_line, $1.str());
	    num_errors++;
	    YYERROR;
	  }
	  */
	  
	  if (key.IsSegModeType() == false) {
	    Diag::printf("%s:%d: can only set BLSS on segmode keys\n",
			 current_file.str(), current_line);
	    num_errors++;
	    YYERROR;
	  }

	  key.SetBlss($4);
	  key.SetType((enum KeyType)$1.GetType());

	  $$ = key;
        }

       | key AS qualifier SEGMENT KEY {
	  SHOWPARSE("=== key -> key AS SEGMENT KEY\n");
      
	  DiskKey key = $1;

	  if (key.IsSegModeType() == false) {
	    Diag::printf("%s:%d: can only retype segmode keys\n",
			 current_file.str(), current_line);
	    num_errors++;
	    YYERROR;
	  }

	  key.SetType(KtSegment);

	  if ( !QualifyKey($3, key, key) ) {
	    num_errors++;
	    YYERROR;
	  }

	  $$ = key;
        }

       | key AS qualifier NODE KEY {
	  SHOWPARSE("=== key -> key AS NODE KEY\n");
      
	  DiskKey key = $1;

	  if (key.IsSegModeType() == false) {
	    Diag::printf("%s:%d: can only retype segmode keys\n",
			 current_file.str(), current_line);
	    num_errors++;
	    YYERROR;
	  }

	  key.SetType(KtNode);

	  if ( !QualifyKey($3, key, key) ) {
	    num_errors++;
	    YYERROR;
	  }

	  $$ = key;
        }

       | key WITH qualifier PAGE AT offset {
	  SHOWPARSE("=== key -> qualifier NAME with page_qualifier PAGE AT offset\n");
	   DiskKey key = $1;
      
	   /*
	   if (image.GetDirEnt($1, key) == false) {
	     Diag::printf("%s:%d: unknown object \"%s\"\n",
			 current_file.str(), current_line, $1.str());
	    num_errors++;
	     YYERROR;
	   }
	   */
	   
	   if (key.IsSegModeType() == false) {
	     Diag::printf("%s:%d: can only add pages to segments!\n",
			  current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   DiskKey pageKey =
	     image.AddZeroDataPage($3 & (ATTRIB_RO) ? true : false);
      
	   uint64_t offset = $6;
	   {
	     uint32_t segOffsetBLSS = LSS::BiasedLSS(offset);
	     uint32_t rootBLSS = key.GetBlss();
	     uint32_t segBLSS = pageKey.GetBlss();

	     if (segOffsetBLSS <= segBLSS && rootBLSS <= segOffsetBLSS) {
	       Diag::printf("%s:%d: Inserted page and offset would "
			    "replace existing segment.\n",
			    current_file.str(), current_line);
	       num_errors++;
	       YYERROR;
	     }
	   }

	   key = image.AddPageToSegment(key, offset, pageKey);
      
	   $$ = key;
        }

       | key WITH qualifier CAPABILITY PAGE AT offset {
	  SHOWPARSE("=== key -> qualifier NAME with page_qualifier CAPABILITY PAGE AT offset\n");
	   DiskKey key = $1;
      
	   /*
	   if (image.GetDirEnt($1, key) == false) {
	     Diag::printf("%s:%d: unknown object \"%s\"\n",
			 current_file.str(), current_line, $1.str());
	    num_errors++;
	     YYERROR;
	   }
	   */
	   
	   if (key.IsSegModeType() == false) {
	     Diag::printf("%s:%d: can only add pages to segments!\n",
			  current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   DiskKey pageKey =
	     image.AddZeroCapPage($3 & (ATTRIB_RO) ? true : false);
      
	   uint64_t offset = $7;
	   {
	     uint32_t segOffsetBLSS = LSS::BiasedLSS(offset);
	     uint32_t rootBLSS = key.GetBlss();
	     uint32_t segBLSS = pageKey.GetBlss();

	     if (segOffsetBLSS <= segBLSS && rootBLSS <= segOffsetBLSS) {
	       Diag::printf("%s:%d: Inserted page and offset would "
			    "replace existing segment.\n",
			    current_file.str(), current_line);
	       num_errors++;
	       YYERROR;
	     }
	   }

	   key = image.AddPageToSegment(key, offset, pageKey);
      
	   $$ = key;
        }

       | key WITH qualifier SUBSEG segkey AT offset {
	  SHOWPARSE("=== key -> qualifier NAME with qualifier SUBSEG AT offset\n");
	  DiskKey key = $1;
      
	  /*
	  if (image.GetDirEnt($1, key) == false) {
	    Diag::printf("%s:%d: unknown object \"%s\"\n",
			 current_file.str(), current_line, $1.str());
	    num_errors++;
	    YYERROR;
	  }
	  */

	  if (key.IsSegModeType() == false) {
	    Diag::printf("%s:%d: can only add subsegments to segments!\n",
			 current_file.str(), current_line);
	    num_errors++;
	    YYERROR;
	  }

	  DiskKey subSeg = $5;

	  if (subSeg.IsSegModeType() == false) {
	    Diag::printf("%s:%d: qualifiers only permitted on"
			 " segmode keys\n",
			 current_file.str(), current_line);
	    num_errors++;
	    YYERROR;
	  }

	  uint64_t offset = $7;
	  {
	    uint32_t segOffsetBLSS = LSS::BiasedLSS(offset);
	    uint32_t rootBLSS = key.GetBlss();
	    uint32_t segBLSS = subSeg.GetBlss();

	    if (segOffsetBLSS <= segBLSS && rootBLSS <= segOffsetBLSS) {
	      Diag::printf("%s:%d: Inserted segment and offset would "
			   "replace existing segment.\n",
			   current_file.str(), current_line);
	      num_errors++;
	      YYERROR;
	    }
	  }

	  if ( !QualifyKey($3, subSeg, subSeg) ) {
	    num_errors++;
	    YYERROR;
	  }
	   
	  key = image.AddSubsegToSegment(key, offset, subSeg);

	  $$ = key;
        }

       | NEW DOMAIN {
	  SHOWPARSE("=== key -> NEW DOMAIN\n");
	  DiskKey key = AddDomain(image);
      
	  RegDescrip::InitProcess(image, key, CurArch);
      
	  key.SetType(KtProcess);

	  $$ = key;
        }

       | START domain keyData {
	  SHOWPARSE("=== key -> START domain keyData\n");
	  DiskKey key = $2;
	  key.SetType(KtStart);
	  key.subType = 0;
	  key.keyData = $3;

	  $$ = key;
        }
       ;

domain:  NAME {
	  SHOWPARSE("=== domain -> NAME\n");
	   DiskKey key;
      
	   if (image.GetDirEnt($1, key) == false) {
	     Diag::printf("%s:%d: unknown object \"%s\"\n",
			 current_file.str(), current_line, $1.str());
	     num_errors++;
	     YYERROR;
	   }

           if (key.IsType(KtProcess) == false &&
	       key.IsType(KtStart) == false &&
	       key.IsType(KtResume) == false) {
	     Diag::printf("%s:%d: must be domain name\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   $<is>$ = $1;
	   $$ = key;
        }
        ;

node:  NAME {
	  SHOWPARSE("=== node -> NAME\n");
	   DiskKey key;
      
	   if (image.GetDirEnt($1, key) == false) {
	     Diag::printf("%s:%d: unknown object \"%s\"\n",
			 current_file.str(), current_line, $1.str());
	     num_errors++;
	     YYERROR;
	   }

           if (key.IsNodeKeyType() == false) {
	     Diag::printf("%s:%d: must be node key type\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   $$ = key;
        }
       | slot {
	   SHOWPARSE("=== node -> slot\n");
	   DiskKey key = image.GetNodeSlot($1, $<slot>1);
	   
           if (key.IsNodeKeyType() == false) {
	     Diag::printf("%s:%d: must be node key type\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   $$ = key;
        }
        ;

segmode:  NAME {
	  SHOWPARSE("=== segmode -> NAME\n");
	   DiskKey key;
      
	   if (image.GetDirEnt($1, key) == false) {
	     Diag::printf("%s:%d: unknown object \"%s\"\n",
			 current_file.str(), current_line, $1.str());
	     num_errors++;
	     YYERROR;
	   }

	   if ( key.IsSegKeyType() ) {
	     $$ = key;
	   }
	   else {
	     Diag::printf("%s:%d: must be segmode key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }
        }
        ;

/*
domainkey:  key {
	  SHOWPARSE("=== domainkey -> key\n");
	   if ($1.IsType(KtProcess) == false) {
	     Diag::printf("%s:%d: must be domain key\n",
			 current_file.str(), current_line);
	    num_errors++;
	     YYERROR;
	   }

	   $$ = $1;
        }
        ;
	*/

startkey:  key {
	  SHOWPARSE("=== startkey -> key\n");
      
	   if ($1.IsType(KtStart) == false) {
	     Diag::printf("%s:%d: \"%s\" must be start key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   $$ = $1;
        }
        ;

segkey:  key {
	  SHOWPARSE("=== segkey -> key\n");
	   if ($1.IsSegModeType() == false &&
	       $1.IsZeroKey() == false &&
	       ($1.IsType(KtMisc) && $1.subType ==
		MiscKeyType::TimePage) == false) {
	     Diag::printf("%s:%d: must be segment or "
			 "segtree key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   $$ = $1;
        }
        ;

schedkey:  key {
	  SHOWPARSE("=== schedkey -> key\n");
           if ($1.IsType(KtSched) == false) {
	     Diag::printf("%s:%d: must be schedule key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   $$ = $1;
        }
        ;

numberkey:  key {
	  SHOWPARSE("=== numberkey -> key\n");
           if ($1.IsType(KtNumber) == false) {
	     Diag::printf("%s:%d: must be number key\n",
			 current_file.str(), current_line);
	     num_errors++;
	     YYERROR;
	   }

	   $$ = $1;
        }
        ;

arch_reg:  NAME {
	  SHOWPARSE("=== arch_reg -> NAME\n");
            /* See if it's really an architecture name: */
           RegDescrip *rd = RegDescrip::Lookup(CurArch, $1);

	   if (rd == 0) {
	     Diag::printf("%s:%d: \"%s\" is not a valid "
			 "register name\n",
			 current_file.str(), current_line, $1.str());
	     num_errors++;
	     YYERROR;
	   }

	   $$ = rd;
        }
        ;

segtype:  SEGMENT {
	   SHOWPARSE("=== segtype -> SEGMENT\n");
          $$ = 1;
	   }
        | SEGTREE {
	   SHOWPARSE("=== segtype -> SEGTREE\n");
	  $$ = 0;
	}
        ;

qualifier:  RO {
	  SHOWPARSE("=== qualifier -> RO\n");
          $$ = ATTRIB_RO;
	   }
        |  RO NC {
	  SHOWPARSE("=== qualifier -> RO NC\n");
	  $$ =  ATTRIB_RO | ATTRIB_NC;
	   }
        |  RO NC WEAK {
	  SHOWPARSE("=== qualifier -> RO NC\n");
	  $$ =  ATTRIB_RO | ATTRIB_NC | ATTRIB_WEAK;
	   }
        |  RO WEAK {
	  SHOWPARSE("=== qualifier -> RO NC\n");
	  $$ =  ATTRIB_RO | ATTRIB_WEAK;
	   }
        |  RO WEAK NC {
	  SHOWPARSE("=== qualifier -> RO NC\n");
	  $$ =  ATTRIB_RO | ATTRIB_NC | ATTRIB_WEAK;
	   }
        |  NC {
	  SHOWPARSE("=== qualifier -> NC\n");
	  $$ = ATTRIB_NC;
	   }
        |  NC RO {
	  SHOWPARSE("=== qualifier -> NC RO\n");
	  $$ =  ATTRIB_RO | ATTRIB_NC;
	   }
        |  NC RO WEAK {
	  SHOWPARSE("=== qualifier -> NC RO\n");
	  $$ =  ATTRIB_RO | ATTRIB_NC | ATTRIB_WEAK;
	   }
        |  NC WEAK {
	  SHOWPARSE("=== qualifier -> NC RO\n");
	  $$ =  ATTRIB_RO | ATTRIB_NC;
	   }
        |  NC WEAK RO {
	  SHOWPARSE("=== qualifier -> NC RO\n");
	  $$ =  ATTRIB_RO | ATTRIB_NC | ATTRIB_WEAK;
	   }
        |  WEAK {
	  SHOWPARSE("=== qualifier -> NC\n");
	  $$ = ATTRIB_WEAK;
	   }
        |  WEAK NC {
	  SHOWPARSE("=== qualifier -> NC\n");
	  $$ = ATTRIB_WEAK | ATTRIB_NC;
	   }
        |  WEAK NC RO {
	  SHOWPARSE("=== qualifier -> NC\n");
	  $$ = ATTRIB_WEAK | ATTRIB_NC | ATTRIB_RO;
	   }
        |  WEAK RO {
	  SHOWPARSE("=== qualifier -> NC\n");
	  $$ = ATTRIB_WEAK | ATTRIB_RO;
	   }
        |  WEAK RO NC {
	  SHOWPARSE("=== qualifier -> NC\n");
	  $$ = ATTRIB_WEAK | ATTRIB_NC | ATTRIB_RO;
	   }
        |  SENSE {
	  SHOWPARSE("=== qualifier -> SENSE\n");
	  $$ = ATTRIB_WEAK | ATTRIB_NC | ATTRIB_RO;
	   }
        | /* nothing */ {
	  $$ = 0;
	   }
        ;

/* OID can only be hex to simplify conversion */
oid:   HEX {
          SHOWPARSE("=== oid -> HEX\n");
         int len = strlen($1.str());
	 if (len > 18) {
	   Diag::printf("%s:%d: oid value too large\n",
		       current_file.str(), current_line);
	   num_errors++;
	   YYERROR;
	 }

	 char digits[19];
	 strcpy(digits, $1.str());

	 uint32_t lo;
	 uint32_t hi = 0;
	 
	 if ( len > 10 ) {
	   /* has significant upper digits... */
	   lo = strtoul(digits + (len - 8), 0, 16);
	   digits[len - 8] = 0;
	   hi = strtoul(digits, 0, 16);
	 }
	 else {
	   /* has significant upper digits... */
	   lo = strtoul(digits, 0, 16);
	 }

	 uint64_t oid = hi;
	 oid <<= 32;
	 oid |= (uint64_t) lo;

	 $$ = oid;
       }
       ;

blss:  number {
          SHOWPARSE("=== blss -> number\n");
	 if ($1 >= 23) {
	   Diag::printf("%s:%d: blss value too large\n",
		       current_file.str(), current_line);
	   num_errors++;
	   YYERROR;
	 }
       }
       ;

priority:  number {
          SHOWPARSE("=== priority -> number\n");
	 if ($1 >= 16) {
	   Diag::printf("%s:%d: priority value too large\n",
		       current_file.str(), current_line);
	   num_errors++;
	   YYERROR;
	 }
       }
       ;

slot: NAME '[' slotno ']' {
         SHOWPARSE("=== slot -> NAME [ slotno ]\n");

	 DiskKey key;
	 if (image.GetDirEnt($1, key) == false) {
	   Diag::printf("%s:%d: unknown object \"%s\"\n",
			current_file.str(), current_line, $1.str());
	   num_errors++;
	   YYERROR;
	 }


	 $$ = key;
	 $<slot>$ = $3;

	 uint32_t restriction = 0;

	 if (key.IsNodeKeyType() == false) {
	   Diag::printf("%s:%d: must be node key type\n",
			current_file.str(), current_line);
	   num_errors++;
	   YYERROR;
	 }

	 if (key.IsType(KtProcess) || key.IsType(KtStart) ||
	     key.IsType(KtResume)) {
	   restriction |= DomRootRestriction[$3];
	 }
	 else if ( key.IsType(KtSegment) && key.IsRedSegmentKey() ) {
	   restriction |= CalcRedSegRestriction(key, $3);
	 }
	 $<restriction>$ = restriction;
         SHOWPARSE1("=== +++ restriction = 0x%x\n", restriction);
       }
       | slot '[' slotno ']'  {
	 uint32_t restriction = 0;
	 
	 if ($<restriction>1 & (RESTRICT_ZERO|RESTRICT_NUMBER|RESTRICT_SCHED)) {
	   Diag::printf("%s:%d: cannot indirect through number key\n",
			current_file.str(), current_line);
	   num_errors++;
	   YYERROR;
	 }

	 if ($<restriction>1 & RESTRICT_GENREGS)
	   restriction |= RESTRICT_NUMBER;

	 if (($<restriction>1 & RESTRICT_KEYREGS) && $3 == 0)
	   restriction |= RESTRICT_ZERO;
	 
	 DiskKey key = image.GetNodeSlot($1, $<slot>1);

	 $<slot>$ = $3;
	 $$ = key;
	 
	 if ( !key.IsSegKeyType() ) {
	   Diag::printf("%s:%d: LHS of '[slot]' must be segmode key\n",
			current_file.str(), current_line);
	   num_errors++;
	   YYERROR;
	 }

	 if (key.IsType(KtProcess) || key.IsType(KtStart) ||
	     key.IsType(KtResume)) {
	   restriction |= DomRootRestriction[$3];
	 }
	 else if ( key.IsType(KtSegment) && key.IsRedSegmentKey() ) {
	   restriction |= CalcRedSegRestriction($1, $3);
	 }

	 $<restriction>$ = restriction;
         SHOWPARSE1("=== +++ restriction = 0x%x\n", restriction);
        }

slotno:  number {
          SHOWPARSE("=== slotno -> number\n");
	 if ($1 > EROS_NODE_SIZE) {
	   Diag::printf("%s:%d: slot number too large\n",
		       current_file.str(), current_line);
	   num_errors++;
	   YYERROR;
	 }
       }
       ;

offset:  number {
          SHOWPARSE("=== offset -> number\n");

  /* This is WRONG -- the offset should be an nkv -- a  96 bit quantity */
        $$ = $1;
       }
       ;

keyData:  number {
          SHOWPARSE("=== keyData -> number\n");
	 if ($1 > 0xffffu) {
	   Diag::printf("%s:%d: key data value too large\n",
		       current_file.str(), current_line);
	   num_errors++;
	   YYERROR;
	 }
       }
       ;

number:  HEX {
          SHOWPARSE("=== number -> HEX\n");
         unsigned long value = strtoul($1.str(), 0, 0);
	 $$ = value;
       }
     | DEC {
          SHOWPARSE("=== number -> DEC\n");
         unsigned long value = strtoul($1.str(), 0, 0);
	 $$ = value;
       }

     | OCT {
          SHOWPARSE("=== number -> OCT\n");
         unsigned long value = strtoul($1.str(), 0, 0);
	 $$ = value;
       }

     | BIN {
          SHOWPARSE("=== number -> BIN\n");
         unsigned long value = strtoul($1.str()+2, 0, 2);
	 $$ = value;
       }
     ;

numkey_value:  HEX {
          SHOWPARSE("=== numkey_value -> HEX\n");
         NumKeyValue nkv;
         strtonkv($1, nkv);
	 $$ = nkv;
       }
     | DEC {
          SHOWPARSE("=== numkey_value -> DEC\n");
         NumKeyValue nkv;
         strtonkv($1, nkv);
	 $$ = nkv;
       }

     | OCT {
          SHOWPARSE("=== numkey_value -> OCT\n");
         NumKeyValue nkv;
         strtonkv($1, nkv);
	 $$ = nkv;
       }

     | BIN {
          SHOWPARSE("=== numkey_value -> BIN\n");
         NumKeyValue nkv;
         strtonkv($1, nkv);
	 $$ = nkv;
       }
     ;

%%

class StrBuf {
  char *s;
  uint32_t limit;
  uint32_t m_len;
  
  void Grow()
    {
      char *news = new char[limit + 4096];
      limit += 4096;
      strcpy(s, news);
      delete [] s;
      s = news;
    }
public:
  StrBuf()
    {
      s = new char[4096];
      limit = 4096;
      m_len = 0;
    }

  StrBuf& operator << (char c)
    {
      if (m_len + 1 >= limit)
	Grow();

      s[m_len++] = c;
      s[m_len] = 0;
      return *this;
    }

  StrBuf& operator << (int i)
    {
      char str[20];
      sprintf(str, "%d", i);
      int str_len = strlen(str);
      
      while (m_len + str_len >= limit)
	Grow();

      strcat(s, str);
      m_len += str_len;
      return *this;
    }

  StrBuf& operator << (unsigned u)
    {
      char str[20];
      sprintf(str, "%u", u);
      int str_len = strlen(str);
      
      while (m_len + str_len >= limit)
	Grow();

      strcat(s, str);
      m_len += str_len;
      return *this;
    }

  StrBuf& operator << (const char *str)
    {
      uint32_t str_len = strlen(str);
      while (m_len + str_len >= limit)
	Grow();

      strcat(s, str);
      m_len += str_len;
      return *this;
    }

  StrBuf& operator << (const InternedString& is)
    {
      (*this) << is.str();
      return *this;
    }

  const char *str() const
    {
      return s;
    }

  int len() const
    {
      return m_len;
    }

  StrBuf& operator << (const StrBuf& sb)
    {
      (*this) << sb.str();
      return *this;
    }
};

int
main(int argc, char *argv[])
{
  int c;
  extern int optind;
#if 0
  extern char *optarg;
#endif
  int opterr = 0;
  const char *target;
  bool verbose = false;
  bool use_std_inc_path = true;
  bool use_std_def_list = true;
  
  StrBuf cpp_cmd;
  StrBuf cpp_cmd_args;
  cpp_cmd << "/lib/cpp ";
    
  /* Set up standard EROS include search path: */
  StrBuf stdinc;
  stdinc << "-I" << App::BuildPath("/eros/include") << ' ';

  StrBuf stddef;
  stddef << ' ';
#if 0
  /* All .map files should now just include target.map */
  stddef << "-DEROS_NODE_SIZE=" << EROS_NODE_SIZE << ' ';
#endif

  while ((c = getopt(argc, argv, "o:dvn:I:A:D:")) != -1) {
    const char cc = c;

    switch(c) {
    case 'v':
      verbose = true;
      break;

    case 'o':
      target = optarg;
      break;

    case 'd':
      showparse = true;
      break;

      /* CPP OPTIONS */
    case 'I':
      /* no spaces in what we hand to cpp: */
      cpp_cmd_args << '-' << cc << App::BuildPath(optarg) << ' ';
      break;

    case 'D':
    case 'A':
      /* no spaces in what we hand to cpp: */
      cpp_cmd_args << '-' << cc << optarg << ' ';
      break;
    case 'n':
      if (strcmp(optarg, "ostdinc") == 0) {
	use_std_inc_path = false;
	cpp_cmd_args << '-' << cc << optarg << ' ';
      }
      else if (strcmp(optarg, "ostddef") == 0) {
	use_std_def_list = false;
	cpp_cmd_args << '-' << cc << optarg << ' ';
      }
      else
	opterr++;
      break;
      
    default:
      opterr++;
    }
  }
  
  argc -= optind;
  argv += optind;
  
  if (argc > 1)
    opterr++;

#if 0
  if (argc == 0)
    opterr++;
#endif
  
  if (target == 0)
    opterr++;

  if (opterr)
    Diag::fatal(1, "Usage: mkimage -o target [-v] [-d] [-nostdinc] [-Idir]"
		" [-Ddef] [-Aassert] descrip_file\n");

  if (use_std_def_list)		/* std defines come first */
    cpp_cmd << stddef;
  
  cpp_cmd << cpp_cmd_args;

  if (use_std_inc_path)		/* std includes come last */
    cpp_cmd << stdinc;

  if (argc == 1) {
    current_file = App::BuildPath(argv[0]);
    cpp_cmd << current_file;
  }
    
#if 0
  fprintf(stderr, "Command to cpp is: %s\n", cpp_cmd.str());
#endif
  
  yyin = popen(cpp_cmd.str(), "r");
  
  if (!yyin)
    Diag::fatal(1, "Couldn't open description file\n");

  if (verbose)
    App.SetInteractive();

  yyparse();

  image.WriteToFile(target);
  
  pclose(yyin);

  if (num_errors != 0u) App.SetExitValue(1u);

  App.Exit();
}

extern "C" {
  int yywrap();
}

int
yywrap()
{
  return 1;
}

void
yyerror(const char * /* msg */)
{
  Diag::printf("%s:%d: syntax error\n",
	       current_file.str(), current_line);
  num_errors++;
}

bool
AddRawSegment(ErosImage& image,
	      InternedString fileName,
	      DiskKey& segKey)
{
  const char *source = fileName.str();
  
  struct stat statbuf;

  int sfd = ::open(source, O_RDONLY);
  if (sfd < 0) {
    Diag::printf("Unable to open segment file \"%s\"\n", source);
    return false;
  }

  if (::fstat(sfd, &statbuf) < 0) {
    Diag::printf("Can't stat segment file \"%s\"\n", source);
    ::close(sfd);
    return false;
  }

  uint32_t nPages = statbuf.st_size / EROS_PAGE_SIZE;
  if (statbuf.st_size % EROS_PAGE_SIZE)
    nPages++;

  uint8_t *buf = new uint8_t[nPages * EROS_PAGE_SIZE];
  memset(buf, 0, nPages * EROS_PAGE_SIZE);

  if (::read(sfd, buf, statbuf.st_size) != statbuf.st_size) {
    Diag::printf("Cannot read segment image \"%s\"\n", source);
    ::close(sfd);
    return false;
  }

  ::close(sfd);

  segKey = NumberKey(0);
  
  for (uint32_t pg = 0;  pg < nPages; pg++) {
    uint32_t pageAddr = pg * EROS_PAGE_SIZE;
    DiskKey pageKey = image.AddDataPage(&buf[pg * EROS_PAGE_SIZE]);
    segKey = image.AddPageToSegment(segKey, pageAddr, pageKey);
  }

  return true;
}

bool
AddZeroSegment(ErosImage& image,
	       DiskKey& segKey,
	       uint32_t nPages)
{
  segKey = NumberKey(0);
  
  for (uint32_t pg = 0;  pg < nPages; pg++) {
    uint32_t pageAddr = pg * EROS_PAGE_SIZE;
    DiskKey pageKey = image.AddZeroDataPage();
    segKey = image.AddPageToSegment(segKey, pageAddr, pageKey);
  }

  return true;
}

bool
AddEmptySegment(ErosImage& image,
		DiskKey& segKey,
		uint32_t nPages)
{
  segKey = NumberKey(0);
  
  for (uint32_t pg = 0;  pg < nPages; pg++) {
    uint32_t pageAddr = pg * EROS_PAGE_SIZE;
    DiskKey nullKey = NumberKey(0);
    segKey = image.AddPageToSegment(segKey, pageAddr, nullKey);
  }

  return true;
}

bool
GetProgramSymbolValue(InternedString fileName,
		      InternedString symName,
		      uint32_t& value)
{
  ExecImage ei;
  if ( !ei.SetImage(fileName) )
    return false;

  bool ok = ei.GetSymbolValue(symName, value);

#if 0
  Diag::printf("Value of \"%s\" in \"%s\" is 0x%08x\n"
	       "  entry pt is 0x%08x\n", symName.str(),
	       fileName.str(), value,
	       ei.EntryPt());
#endif
  
  return ok;
}

// This is by far the most complicated segment construction process.
// The source of complexity is that both ELF and a.out (under suitable
// conditions) will create one or more pages that are mapped at
// multiple addresses.  The common one is that the last page of code
// and the first page of data are usually written in the same physical
// page and multiply mapped.  In truth, this is a pretty stupid thing
// to do, since it allows the user to rewrite the last bit of the
// code segment, but for the sake of compatibility we allow it.
//
// Processing is not as complicated as it looks.
// 
// 1. For each unique page in the input file, we add a page to the
//    ErosImage file.
// 2. For each MAPPING of a page in the input file, we add a page to
//    the segment.
//
// The end result is that we make another copy of everything in the
// ExecImage file, which is arguably unfortunate, but much simpler
// than the other approaches I've been able to come up with.  Keep in
// mind that the ExecImage instance is very temporary.
//
// The trick to this is that some pages either will not originate in
// the binary file or will need to be duplicated 

bool
AddProgramSegment(ErosImage& image,
		  InternedString fileName,
                  DiskKey& segKey)
{
  ExecImage ei;
  if ( !ei.SetImage(fileName) )
    return false;
  
  const uint8_t* imageBuf = ei.GetImage();

  segKey = NumberKey(0);
  DiskKey pageKey = NumberKey(0);

  PtrMap<DiskKey> map;
  
  // Run through the regions in the executable image, finding all of
  // the associated pages and adding them to the ErosImage file at
  // most once (use the PtrMap to remember which pages we have seen).
  // Add these page keys to the segment at the appropriate address.
  //
  // Where the page is all zeros, there may be no corresponding page
  // in the exec image file.  We assume that such pages are not
  // shared.
  //
  // Pages may be only partial in the exec image file.  In that case
  // we zero-extend them.  Such pages are copied into the ErosImage
  // exactly once.
  
  for (uint32_t i = 0; i < ei.NumRegions(); i++) {
    const ExecRegion& er = ei.GetRegion(i);

#if 0
    char perm[4] = "\0\0\0";
    char *pbuf = perm;
    if (er.perm & ExecRegion::ER_R)
      *pbuf++ = 'R';
    if (er.perm & ExecRegion::ER_W)
      *pbuf++ = 'W';
    if (er.perm & ExecRegion::ER_X)
      *pbuf++ = 'X';

    Diag::printf("va=0x%08x   memsz=0x%08x   filesz=0x%08x"
		 "   offset=0x%08x   %s\n",
		 er.vaddr, er.memsz, er.filesz, er.offset, perm);
#endif

    uint32_t topfileva = er.vaddr + er.filesz;
    uint32_t topva = er.vaddr + er.memsz;
    uint32_t startva = er.vaddr & ~EROS_PAGE_MASK;
    uint32_t startoffset = er.offset;
    bool readOnly = false;
    
    /* In case not a paged image, back up the offset to the page
       boundary.  Note that contrary to previous assumptions, the VA
       and the offset will not be congruent mod page size UNLESS the
       image is marked as paged. */
    if (startva != er.vaddr)
      startoffset -= (er.vaddr - startva);

    if ((er.perm & ExecRegion::ER_W) == 0)
      readOnly = true;
      
    for (uint32_t va = startva; va < topva; va += EROS_PAGE_SIZE) {
      if (va >= topfileva) {
	// Page has no image in the exec image file. Add a zero page:
#if 0
        // FIX: figure out someday why this occurs!
	if (er.perm & ExecRegion::ER_X)
	  Diag::printf("WARNING: executable zero page\n");
#endif
	
	pageKey = image.AddZeroDataPage(readOnly);
      }
      else {
	// Page has image in the exec image file.  See if we have
	// already copied this page into the domain image before we
	// copy it again:
	
	uint32_t offset = startoffset + (va - startva);

	if (map.Lookup(imageBuf + offset, pageKey)) {
	  // We have a page key, but it's read-only attribute may be
	  // incorrect for this mapping, so adjust it:

	  if (readOnly)
	    pageKey.SetReadOnly();
	}
	else {
	  uint8_t pagebuf[EROS_PAGE_SIZE];

	  bzero((void *) pagebuf, EROS_PAGE_SIZE);

	  uint32_t sz = EROS_PAGE_SIZE;
	  if (topfileva - va < EROS_PAGE_SIZE)
	    sz = (topfileva - va);

	  memcpy(pagebuf, imageBuf + offset, sz);

	  pageKey = image.AddDataPage(pagebuf, readOnly);
	}
      }

      segKey = image.AddPageToSegment(segKey, va, pageKey);
    }
  }

  return true;
}

DiskKey
AddDomain(ErosImage& image)
{
  DiskKey rootKey = image.AddNode();
#ifdef ProcGenRegs
  DiskKey regsKey = image.AddNode();
#endif
  DiskKey keysKey = image.AddNode();

#ifdef ProcGenRegs
  image.SetNodeSlot(rootKey, ProcGenRegs, regsKey);
#endif
  image.SetNodeSlot(rootKey, ProcGenKeys, keysKey);

  // Domain is initially available:
  DiskKey key = image.GetNodeSlot(rootKey, ProcTrapCode);
  key.nk.value[2] &= key.nk.value[2] & 0xffffff00;
  key.nk.value[2] |= RS_Available;
  image.SetNodeSlot(rootKey, ProcTrapCode, key);

  return rootKey;
}

void
ShowImageDirectory(const ErosImage& image)
{
  Diag::printf("Image directory:\n");

  for (uint32_t i = 0; i < image.nDirEnt; i++) {
    ErosImage::Directory d = image.GetDirEnt(i);
    Diag::printf("  [%2d]", i);
    Diag::printf("  %-16s  ", image.GetString(d.name).str());
    PrintDiskKey(d.key);
    Diag::printf("\n");
  }
}

void
ShowImageThreadDirectory(const ErosImage& image)
{
  Diag::printf("Image threads:\n");

  for (uint32_t i = 0; i < image.nThread; i++) {
    ErosImage::Directory d = image.GetThreadEnt(i);
    Diag::printf("  [%2d]", i);
    Diag::printf("  %-16s  ", image.GetString(d.name).str());
    PrintDiskKey(d.key);
    Diag::printf("\n");
  }
}

#define DEF_MISCKEY(name) { #name, 1 },
#define OLD_MISCKEY(name) { #name, 0 },

static const struct {
  char *name;
  int isValid;
} MiscKeyNames[MiscKeyType::NKEYS] = {
#include <disk/MiscKey.def>
};

bool
GetMiscKeyType(InternedString s, uint32_t& ty)
{
  const char *sstr = s.str();
  
  for (uint32_t i = 0; i < MiscKeyType::NKEYS; i++) {
    if (strcasecmp(sstr, MiscKeyNames[i].name) == 0) {
      if (MiscKeyNames[i].isValid == 0)
	return false;
      ty = i;
      return true;
    }
  }

  return false;
}

/* mod 65536 multiply-step, which we use extremely inefficiently to do
   conversion on number key values. */
bool
nkv_mulstep(NumKeyValue& nkv, uint16_t multiplier)
{
  uint32_t nkv_parts[6];
  nkv_parts[0] = nkv.lo & 0xffffu;
  nkv_parts[1] = nkv.lo >> 16;
  nkv_parts[2] = nkv.mid & 0xffffu;
  nkv_parts[3] = nkv.mid >> 16;
  nkv_parts[4] = nkv.hi & 0xffffu;
  nkv_parts[5] = nkv.hi >> 16;

  uint32_t residual = 0;
  
  /* Note that the largest possibly carry is 0xFFFEu, and
     (0xFFFFu * 0xFFFFu) + 0xFFFEu < 0xFFFFFFFFu, so we don't need to
     worry about carry extension.  This is obvious to anyone who
     really knows modular arithmetic well, but I have a tendancy to
     forget it. */
  for (int i = 0; i < 6; i++) {
    nkv_parts[i] *= multiplier;
    nkv_parts[i] += residual;
    residual = nkv_parts[i] >> 16;
    nkv_parts[i] &= 0xFFFFu;
  }

  nkv.lo = (nkv_parts[1] << 16) | nkv_parts[0];
  nkv.mid = (nkv_parts[3] << 16) | nkv_parts[2];
  nkv.hi = (nkv_parts[5] << 16) | nkv_parts[4];
  
  return (residual ? false : true);
}

/* mod 65536 add-step, which we use extremely inefficiently to do
   conversion on number key values. */
bool
nkv_addstep(NumKeyValue& nkv, uint16_t addend)
{
  uint32_t nkv_parts[6];
  nkv_parts[0] = nkv.lo & 0xffffu;
  nkv_parts[1] = nkv.lo >> 16;
  nkv_parts[2] = nkv.mid & 0xffffu;
  nkv_parts[3] = nkv.mid >> 16;
  nkv_parts[4] = nkv.hi & 0xffffu;
  nkv_parts[5] = nkv.hi >> 16;

  /* Note that the largest possibly carry is 0xFFFEu, and
     (0xFFFFu * 0xFFFFu) + 0xFFFEu < 0xFFFFFFFFu, so we don't need to
     worry about carry extension.  This is obvious to anyone who
     really knows modular arithmetic well, but I have a tendancy to
     forget it. */
  for (int i = 0; i < 6; i++) {
    nkv_parts[i] += addend;
    addend = nkv_parts[i] >> 16;
    nkv_parts[i] &= 0xFFFFu;
  }

  nkv.lo = (nkv_parts[1] << 16) | nkv_parts[0];
  nkv.mid = (nkv_parts[3] << 16) | nkv_parts[2];
  nkv.hi = (nkv_parts[5] << 16) | nkv_parts[4];
  
  return (addend ? false : true);
}

uint16_t
decimal_value(char c)
{
  /* also suitable for octal, binary */
  return c - '0';
}

uint16_t
hex_value(char c)
{
  if (isdigit(c))
    return c - '0';

  /* else it's a hex alpha */
  return tolower(c) - 'a' + 10;
}

bool
strtonkv(InternedString& is, NumKeyValue& nkv)
{
  uint16_t base = 10;		/* until proven otherwise */
  uint32_t offset = 0;		/* until proven otherwise */
  uint16_t (*digit_value)(char) = decimal_value;
  
  if (is.str()[0] == '0') {
    if (tolower(is.str()[1]) == 'x') {
      base = 16;
      offset = 2;
      digit_value = hex_value;
    }
    else if (tolower(is.str()[1]) == 'b') {
      base = 2;
      offset = 2;
    }
    else {
      base = 8;
      offset = 1;
    }
  }

  nkv.hi = 0;
  nkv.mid = 0;
  nkv.lo = 0;

  const char *digits = is.str() + offset;

  for ( ; *digits; digits++) {
    if (!nkv_mulstep(nkv, base))
      return false;
    if (!nkv_addstep(nkv, digit_value(*digits)))
      return false;
  }

  return true;
}

// FIX: Should this honor the reserved slot counts?
uint32_t
CalcRedSegRestriction(DiskKey segKey, uint32_t slot)
{
  if (slot == RedSegFormat)
    return RESTRICT_NUMBER;
  
  DiskKey fmtKey = image.GetNodeSlot(segKey, RedSegFormat);

  if (slot < REDSEG_GET_INITIAL_SLOTS(fmtKey.nk))
    return RESTRICT_SEGMODE;
    
  if (slot == REDSEG_GET_KPR_SLOT(fmtKey.nk))
    return RESTRICT_START;
    
  if (slot == REDSEG_GET_BG_SLOT(fmtKey.nk))
    return RESTRICT_SEGMODE;

  else
    return RESTRICT_ZERO;
}

bool
CheckRestriction(uint32_t restriction, DiskKey key)
{
  if ((restriction & RESTRICT_ZERO) && !key.IsZeroKey())
    return false;
  
  if ((restriction & RESTRICT_NUMBER) && !key.IsType(KtNumber))
    return false;
  
  if ((restriction & RESTRICT_SCHED) && !key.IsType(KtSched))
    return false;
  
  if ((restriction & RESTRICT_START) && !key.IsType(KtStart) &&
      !key.IsZeroKey()) 
    return false;
  
  if ((restriction & RESTRICT_SEGMODE) &&
      ! (key.IsSegModeType() || key.IsType(KtNumber)))
    return false;
  
  if ((restriction & (RESTRICT_KEYREGS | RESTRICT_GENREGS)) &&
      !key.IsType(KtNode))
    return false;

  return true;
}

/* ???APPLIES the qualifications specified by qualifier to the key. */
bool
QualifyKey(uint32_t qualifier, DiskKey key, DiskKey& out)
{
  if (qualifier & ATTRIB_WEAK) {
    switch(key.GetType()) {
    case KtNode:
    case KtSegment:
      key.SetWeak();
      break;

    case KtNumber:		/* don't whine about these */
    case KtDataPage:
      break;

    default:
      Diag::printf("%s:%d: WEAK qualifier not permitted"
		   " for key type\n",
		   current_file.str(), current_line);
      return false;
    }
  }

  if (qualifier & ATTRIB_RO) {
    switch(key.GetType()) {
    case KtNode:
    case KtDataPage:
    case KtSegment:
      key.SetReadOnly();
      break;

    case KtNumber:		/* don't whine about these */
      break;

    default:
      Diag::printf("%s:%d: RO qualifier not permitted"
		   " for key type\n",
		   current_file.str(), current_line);
      return false;
    }
  }

  if (qualifier & ATTRIB_NC) {
    switch(key.GetType()) {
    case KtNode:
    case KtSegment:
      key.SetNoCall();
      break;

    case KtNumber:		/* don't whine about these */
    case KtDataPage:
      break;
      
    default:
      Diag::printf("%s:%d: NC qualifier not permitted"
		   " for key type\n",
		   current_file.str(), current_line);
      return false;
    }
  }

  out = key;
  return true;
}
