/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Given a UNIX  ELF format binary file, produce an EROS image file
// from that binary file.
#include <sys/fcntl.h>
#include <sys/stat.h>

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <erosimg/App.hxx>
#include <erosimg/ErosImage.hxx>
#include <erosimg/ExecImage.hxx>
#include <erosimg/PtrMap.hxx>
#include <erosimg/Parse.hxx>
#include <eros/Key.h>

extern void PrintDiskKey(const DiskKey&);

class App App("lsimage");

void ShowImageDirectory(const ErosImage& image);
void ShowImageThreadDirectory(const ErosImage& image);


int main(int argc, char *argv[])
{
  int c;
  extern int optind;
#if 0
  extern char *optarg;
#endif
  int opterr = 0;
  const char *source;
  bool showHeaders = false;
  bool showDir = false;
  bool showThreadDir = false;
  bool showNodes = false;
  bool showStrings = false;
  
  while ((c = getopt(argc, argv, "ndths")) != -1) {
    switch(c) {
    case 'n':
      showNodes = true;
      break;      
    case 'd':
      showDir = true;
      break;      
    case 't':
      showThreadDir = true;
      break;      
    case 'h':
      showHeaders = true;
      break;      
    case 's':
      showStrings = true;
      break;      
    default:
      opterr++;
    }
  }
  
  argc -= optind;
  argv += optind;
  
  if (argc != 1)
    opterr++;
  
  if (opterr)
    Diag::fatal(1, "Usage: lsimage [-n|-t|-d|-h|-s] image_file\n");
  
  source = argv[0];

  if (!showHeaders && !showDir && !showNodes && !showThreadDir)
    showDir = true;

  ErosImage image;
  image.ReadFromFile(source);
  
  if (showHeaders) {
    ExecArch::ArchInfo ai = ExecArch::GetArchInfo(image.architecture);

    Diag::printf("Image Headers:\n");
    Diag::printf("  Signature:          %s\n", image.signature);
    Diag::printf("  Byte Sex:           %s\n",
		 ExecArch::GetByteSexName(ai.byteSex)); 
    Diag::printf("  Version:            %d\n", image.version);
    Diag::printf("  Platform            %s\n", ai.name);
    
    Diag::printf("  Directory Entries:  %d\n", image.nDirEnt);
    Diag::printf("  Directory Offset:   %d\n", image.dirOffset);
    Diag::printf("  Thread Entries:     %d\n", image.nThread);
    Diag::printf("  Thread Offset:      %d\n", image.threadOffset);
    Diag::printf("  Data Pages:         %d\n", image.nPages);
    Diag::printf("  Zero Data Pages:    %d\n", image.nZeroPages);
    Diag::printf("  Page Offset:        %d\n", image.pageOffset);
    Diag::printf("  Cap Pages:          %d\n", image.nCapPages);
    Diag::printf("  Zero Cap Pages:     %d\n", image.nZeroCapPages);
    Diag::printf("  Cap Page Offset:    %d\n", image.capPageOffset);
    Diag::printf("  Nodes:              %d\n", image.nNodes);
    Diag::printf("  Nodes Offset:       %d\n", image.nodeOffset);
    Diag::printf("  Str Tbl Size:       %d\n", image.strSize);
    Diag::printf("  Str Tbl Offset:     %d\n", image.strTableOffset);
  }

  if (showDir)
    ShowImageDirectory(image);
  
  if (showThreadDir)
    ShowImageThreadDirectory(image);
  
  if (showNodes) {
    Diag::printf("Image nodes:\n");

    for (uint32_t ndx = 0 ; ndx < image.nNodes; ndx++) {
      Diag::printf("  Node %d\n", ndx);
      for (uint32_t slot = 0; slot < EROS_NODE_SIZE; slot++) {
	DiskKey key = image.GetNodeSlot(ndx, slot);
	Diag::printf("    [%2d]  ", slot);
	PrintDiskKey(key);
	Diag::printf("\n");
      }
    }
  }
  
  if (showStrings) {
    Diag::printf("String table:\n");
    const StringPool strPool = image.GetStringPool();
    const char *buf = strPool.GetPoolBuffer();
    int sz = strPool.Size();

    int pos = 0;
    while (pos < sz) {
      Diag::printf("  %3d  \"%s\"\n", pos, &buf[pos]);
      pos += strlen(&buf[pos]);
      pos++;
    }
  }
  

  App.Exit();
}

void ShowImageDirectory(const ErosImage& image)
{
  Diag::printf("Image directory:\n");

  for (uint32_t i = 0; i < image.nDirEnt; i++) {
    ErosImage::Directory d = image.GetDirEnt(i);
    Diag::printf("  [%2d]", i);
    Diag::printf("  %-16s  ", image.GetString(d.name).str());
    PrintDiskKey(d.key);
    Diag::printf("\n");
  }
}

void ShowImageThreadDirectory(const ErosImage& image)
{
  Diag::printf("Image threads:\n");

  for (uint32_t i = 0; i < image.nThread; i++) {
    ErosImage::Directory d = image.GetThreadEnt(i);
    Diag::printf("  [%2d]", i);
    Diag::printf("  %-16s  ", image.GetString(d.name).str());
    PrintDiskKey(d.key);
    Diag::printf("\n");
  }
}

