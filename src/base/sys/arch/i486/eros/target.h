#ifndef __TARGET_I486_H__
#define __TARGET_I486_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/* Basic type declarations for the target platform, used pervasively
   within the kernel code and the volume construction utilities.
 
   This file is included from both C and C++ code, so it needs to be
   handled carefully.  */

#if !(defined(i386) || defined(i486))
#error "Wrong target file included"
#endif

#ifdef __GNUC__
#if __GNUC__ >= 2
#if __GNUC_MINOR__ >= 91
/* newer compilers do not want the __volatile__ qualifier */
#define GNU_INLINE_ASM __asm__
#endif
#endif

#ifndef GNU_INLINE_ASM
#define GNU_INLINE_ASM __asm__ __volatile__
#endif
#endif /* __GNUC__ */

#define NORETURN __attribute__ ((noreturn))

#define BITFIELD_PACK_LOW	/* bitfields pack low-order bits first */

#define EROS_HAVE_FPU

#ifdef __KERNEL__

#define EROS_SAVE_AREA_SIZE  108
#define USES_MAPPING_PAGES
#define MAPPING_ENTRIES_PER_PAGE 1024

/* The current version of the x86 kernel uses the segment mechanism to
   remap the kernel.  We will want it that way for the windowing
   tricks later anyway.

   On the plus side, this means that a kernel virtual address is also
   a kernel physical address. On the minus side, this means that
   domain virtual addresses must now be referenced through the domain
   segment selector. */
#define VTOP(va) ((uint32_t) (va))
#define PTOV(pa) ((uint32_t) (pa))
#define KVTOL(kva) (VTOP(kva) + KVA)

#endif /* __KERNEL__ */

#ifndef NULL
#define NULL (0L)
#endif

/* This is needed for cross-build compatibility */
#ifndef __BIT_TYPES_DEFINED__
#if defined(__FreeBSD__) && defined(_MACHINE_TYPES_H_)
#define __BIT_TYPES_DEFINED__
#endif /* defined(__FreeBSD__) && defined(_MACHINE_TYPES_H_) */
#endif /* __BIT_TYPES_DEFINED__ */

/* Basic EROS type definitions: */
#ifndef __BIT_TYPES_DEFINED__	
/* avoid conflict with linux hdrs in cross code: */
typedef signed char         int8_t;
typedef signed short        int16_t;
typedef signed long         int32_t;
typedef signed long long    int64_t;

/* EROS does not use these, but UNIX does.  Define them here so that
   we can just define __BIT_TYPES_DEFINED__ without conflict: */
typedef unsigned char       u_int8_t;
typedef unsigned short      u_int16_t;
typedef unsigned long       u_int32_t;
typedef unsigned long long  u_int64_t;
#define __BIT_TYPES_DEFINED__

#endif /* __BIT_TYPES_DEFINED__ */

typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
/* The Linux weenies cannot make up their minds about how uint32_t
   should be defined.  The definition is conditionalized here in a
   linux-sensitive way because I'm tired of twiddling it one way or
   the other. */
#if defined(linux) && defined(_STDINT_H)
typedef unsigned int        uint32_t;
#else
typedef unsigned long       uint32_t;
#endif
typedef unsigned long long  uint64_t;

typedef struct uint80_t {
  uint16_t	v[5];
}                           uint80_t;

typedef uint80_t floatval_t;

typedef unsigned int    size_t;	/* should be 32 bits */
typedef uint32_t	io_t;	/* io address */
typedef uint32_t	klva_t;	/* kernel linear virtual address */
typedef uint32_t        kva_t;	/* kernel virtual address */
typedef uint32_t	kpa_t;	/* kernel physical address */
typedef uint32_t        uva_t;	/* user virtual address */
typedef uint32_t        ula_t;	/* user virtual address */

typedef uint32_t	fixreg_t; /* fixed-point natural register size */

// Number of hardware interrupt lines
#define NUM_HW_INTERRUPT 16

#define TARGET_LONG_MAX (2147483647) /* max value of a "long int" */
#define TARGET_LONG_MIN (-TARGET_LONG_MAX-1) /* min value of a "long int" */
#define TARGET_ULONG_MAX 4294967295u

#define EROS_FRAME_FROM_OID(oid) (oid & ~(EROS_OBJECTS_PER_FRAME-1))
#define EROS_OBNDX_IN_FRAME(oid) (oid & (EROS_OBJECTS_PER_FRAME-1))

/* Some shorthands just to avoid typing: */
typedef unsigned long  u_long;
typedef unsigned int   u_int;
typedef unsigned short u_short;
typedef unsigned char  u_char;

typedef uint32_t SwapLoc;		/* Swap location. */

#endif /* __TARGET_I486_H__ */

