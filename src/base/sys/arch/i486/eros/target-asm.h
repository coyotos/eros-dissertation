#ifndef __TARGET_ASM_I486_H__
#define __TARGET_ASM_I486_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/*
 * Basic type declarations for the target platform, used pervasively
 * within the kernel code and the volume construction utilities.
 *
 * This file is included from Assembler, C and C++ code, so it needs
 * to be handled very carefully.  In particular, parentheses mean
 * something in assembler and should be avoided.
 */

#if !(defined(i386) || defined(i486))
#error "In appropriate target file"
#endif

#ifndef __U
#define __U(x) x##u
#endif

#define EROS_REG_MAX  UINT32_MAX
#define EROS_SREG_MAX INT32_MAX
#define EROS_SREG_MIN INT32_MIN
#define EROS_REG_BITS UINT32_BITS

#define EROS_PAGE_SIZE		__U(0x1000)
#define EROS_MESSAGE_LIMIT	__U(0x10000)
#define EROS_SECTOR_SIZE	512
#define EROS_PAGE_SECTORS	8 /* Page_size / sector_size */

#define EROS_PAGE_ADDR_BITS 12
#define EROS_PAGE_MASK __U(0xfff)
#define EROS_PAGE_BLSS 2

#if 1
#define EROS_NODE_SIZE		__U(0x20)
#define EROS_NODE_LGSIZE	__U(0x5)
#define EROS_NODE_SLOT_MASK	__U(0x1f)
#define EROS_PROCESS_KEYREGS    __U(32)
#else
#define EROS_NODE_SIZE		__U(0x10)
#define EROS_NODE_LGSIZE	__U(0x4)
#define EROS_NODE_SLOT_MASK	__U(0xf)
#define EROS_PROCESS_KEYREGS    __U(16)
#endif

/* The following should be a multiple of EROS_PAGE_SECTORS */
#define DISK_BOOTSTRAP_SECTORS	64

#define EROS_ADDRESS_BITS	32
#define EROS_FIXREG_BITS	32 /* size of native fixreg */

#if EROS_NODE_SIZE == 16
#define EROS_ADDRESS_BLSS	7
#elif EROS_NODE_SIZE == 32
#define EROS_ADDRESS_BLSS	6
#endif

/* Following MUST be a power of 2!!! */
#define EROS_OBJECTS_PER_FRAME  256

#if EROS_NODE_SIZE == 16
#define EROS_NODES_PER_FRAME	15
#elif EROS_NODE_SIZE == 32
#define EROS_NODES_PER_FRAME	7
#else
#error "Node size not understood"
#endif

#if 0
/* This was for 16 slot nodes and 96 bit address spaces */
#define MAX_BLSS       23	/* 2^96 byte space */
#define MAX_RED_BLSS   24	/* above + keeper */
#else
/* This is for 32 slot nodes and 64 bit address spaces */
#define MAX_BLSS       13	/* 2^96 byte space */
#define MAX_RED_BLSS   14	/* above + keeper */
#endif

#ifdef __KERNEL__

/* This must be defined here for the benefit of the bootstrap code,
   which must agree with the kernel about the loaded virtual address.
   Actually, this is no longer really necessary, and we should perhaps
   consider removing that dependency from the bootstrap code.

   2/3/98 -- the bootstrap no longer knows or cares about kernel
   virtual address.  It attempts (minimally) to honor the load
   address, but otherwise pays no attention to kernel binary
   addresses. */

#ifdef SMALL_SPACES
/* Reserve 0.25 Gbytes of address space from 0xC000000..0xCFFFFFF for
   use as small spaces */
#define KVA		__U(0xD0000000)
#define KUVA		__U(0x30000000) /* user va 0 as kernel sees it */
/* sndbuf is only used in slow IPC path */
#define KVA_FSTBUF	__U(0x2f000000) /* top kva - 4096 pages.  Used 
					   to map page *directories* */
#define KVA_PTEBUF	__U(0x2fc00000) /* top kva - 1024 pages */
#define SMALL_SPACE_PAGES 32         /* 128 Kbytes */
#else
#define KVA		__U(0xC0000000)
#define KUVA		__U(0x40000000) /* user va 0 as kernel sees it */
/* sndbuf is only used in slow IPC path */
#define KVA_FSTBUF	__U(0x3f000000) /* top kva - 4096 pages.  Used 
					   to map page *directories* */
#define KVA_PTEBUF	__U(0x3fc00000) /* top kva - 1024 pages */
#endif
#define UMSGTOP         __U(0xC0000000)
#define LARGE_SPACE_PAGES 0xD0000         /* 3 Gbytes */

#define KERNPBASE	__U(0x00101000) /* phys addr where kernel is loaded */
#define KERNBOOTBASE	KERNPBASE   /* address to which boot should branch */
#define KERNPAGEDIR	__U(0x00100000) /* phys addr of kernel page table */

#endif

#endif /* __TARGET_ASM_I486_H__ */
