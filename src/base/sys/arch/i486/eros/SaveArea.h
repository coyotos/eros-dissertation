#ifndef __SAVEAREA_H__
#define __SAVEAREA_H__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

#include "fixregs.h" 

#ifdef __KERNEL__
inline bool
sa_IsProcess(const fixregs_t* fx)
{
  if ( ((fx->CS & 0x3u) != 0x0u) || (fx->EFLAGS & EFLAGS_Virt8086) )
    return true;
  return false;
}

inline bool
sa_IsInterrupt(const fixregs_t* fx)
{
  return ((fx->EFLAGS & EFLAGS_Virt8086 == 0) &&
	  (fx->CS & 0x3u == 0));
}

inline bool
sa_IsKernel(const fixregs_t* fx)
{
  if ( ((fx->CS & 0x3u) < 0x3u) &&
       ((fx->EFLAGS & EFLAGS_Virt8086) == 0) )
    return true;
  return false;
}

void DumpFixRegs(const fixregs_t*);

#endif /* __KERNEL__ */

#endif /* __SAVEAREA_H__ */
