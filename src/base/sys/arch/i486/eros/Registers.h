#ifndef __REGISTERS_I486_H__
#define __REGISTERS_I486_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/* Architecture-specific register set structure */

#define DOMAIN_CODE_SEG   0x23
#define DOMAIN_DATA_SEG   0x2b

struct Registers {
  uint32_t arch;			/* architecture */
  uint32_t len;			/* length of returned structure */
  uint32_t pc;
  uint32_t sp;
  uint32_t faultCode;
  uint32_t faultInfo;
  uint32_t domState;
  uint32_t domFlags;
  
  /* architecture-specific registers start here: */
  uint32_t EDI;
  uint32_t ESI;
  uint32_t EBP;
  uint32_t EBX;
  uint32_t EDX;
  uint32_t ECX;
  uint32_t EAX;
  uint32_t EFLAGS;
  uint16_t CS;
  uint16_t SS;
  uint16_t ES;
  uint16_t DS;
  uint16_t FS;
  uint16_t GS;
#if 0
  uint32_t	   fp_ctrlReg;
  uint32_t	   fp_statusReg;
  uint32_t	   fp_tagWord;
  uint32_t	   fip;			/* floating instruction pointer */
  uint32_t	   fcs;			/* floating code seg */
  uint32_t	   fdp;			/* floating data pointer */
  uint32_t	   fds;			/* floating data segment */
  uint16_t st[8][5];		/* register values */
#endif
};

#endif /* __REGISTERS_I486_H__ */
