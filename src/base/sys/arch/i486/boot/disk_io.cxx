/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <SysInfo.hxx>
#include "boot.h"
#include "debug.h"

extern "C" {
  extern void printf(const char*, ...);
  /* /where/ is the PHYSICAL memory address! */
  extern void read_sectors(unsigned char drive, unsigned long startSec,
			   unsigned long nsec, void *buf);
  extern void get_diskinfo(unsigned char drive, BiosGeometry *bg);

  extern void halt();
  extern void twiddle();
  extern unsigned bios_diskinfo(unsigned drive);
  int biosread(unsigned drive, unsigned cyl, unsigned head, unsigned
	       sec, unsigned nsec, unsigned short bufseg, void *buf);
  extern void finish_load();
  extern void end();
  extern void ppcpy(void *from, void *to, size_t len);
  extern int getchar(int);
}

extern SysInfo si;

void
get_diskinfo(unsigned char drive, BiosGeometry *bg)
{
  uint32_t geomInfo = bios_diskinfo(drive);

  bg->drive = drive;
  bg->secs = geomInfo & 0xffu;
  bg->cyls = geomInfo >> 16;
  bg->heads = (geomInfo >> 8) & 0xffu;

  /* numbers reported by bios are max hd, cyl.  We want max+1. The spt
     value turns out to be okay because it is max relative to 1 */
  bg->heads++;
  bg->cyls++;

  bg->spcyl = bg->secs * bg->heads;
}

#define SCRATCH_SEG 0x1000
#define SCRATCH_OFFSET 0x0
#define SCRATCH_PA (void *) 0x10000
#define SCRATCH_NSEC 128

extern "C" {
  extern void putchar(int);
}

// Read partition starting with sector 'sec', fetching 'count'
// sectors into 'buf'.  'buf' is a PHYSICAL address.  We use the
// area at 0x1000:0000 as a scratch buffer for I/O.
void
read_sectors(unsigned char drive, unsigned long sector,
	     unsigned long count, void *buf)
{
  DEBUG(disk)
    printf("\nread_sectors(0x%x, %ld, %ld, 0x%lx)\n",
	   drive, sector, count, (long)buf);

  if (drive == RAM_DISK_ID) {

    if (si.ramdiskAddress == 0) {
      printf("Ram disk load but no ram disk\n");
      halt();
    }
      
    uint32_t addr = si.ramdiskAddress + sector * EROS_SECTOR_SIZE;
    ppcpy((void *) addr, buf, count * EROS_SECTOR_SIZE);
    return;
  }
  
  BiosGeometry bg;
  get_diskinfo(drive, &bg);

  if (bg.spcyl == 0 || bg.secs == 0) {
    printf("\nread_sectors: BIOS returned invalid information for disk %d.\n",
           drive);
    halt();
  } 
 
  uint8_t *cbuf = (uint8_t *) buf;
  
  while (count) {
    unsigned sec = sector; // bias by partition start

    unsigned cyl = sec/bg.spcyl;
    sec %= bg.spcyl;
  
    unsigned head = sec/bg.secs;
    sec %= bg.secs;

    // never read past the end of the track:
    unsigned nsec = bg.secs - sec;
    if (nsec > count)
      nsec = count;

    if (nsec > SCRATCH_NSEC)
      nsec = SCRATCH_NSEC;
	
    twiddle();

    if (biosread(drive, cyl, head, sec, nsec,
		 SCRATCH_SEG, SCRATCH_OFFSET) != 0) {
      printf("Error: C:%d H:%d S:%d\n", cyl, head, sec);
      halt();
    }

    ppcpy(SCRATCH_PA, cbuf, nsec * EROS_SECTOR_SIZE);

    sector += nsec;
    count -= nsec;
    cbuf += nsec * EROS_SECTOR_SIZE;
  }
}
