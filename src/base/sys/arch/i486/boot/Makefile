#
# Copyright (C) 1998, 1999, Jonathan S. Shapiro.
#
# This file is part of the EROS Operating System.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
default: install

EROS_SRC=../../../../..
CROSS_BUILD=yes
include $(EROS_SRC)/build/lib/make/makevars.mk


ETAGDIRS=.
COMPRESS=-DSUPPORT_COMPRESSED

OBJECTS=	boot1.o 
OBJECTS+=	boot2.o 
OBJECTS+=	cons_io.o bios.o asm.o sysinfo.o disk_io.o
#
# EVERYTHING above must fit within the low 7680 bytes of
# the bootstrap program, or it will not load!!!
#
OBJECTS += main.o ramdisk.o hibios.o loadkern.o crc32.o 
#
# Following is temporary
#
OBJECTS += mem.o
#
# Following are needed from zlib:
#
ifdef COMPRESS
OBJECTS += inflate.o infblock.o infcodes.o inftrees.o infutil.o
OBJECTS += adler32.o inffast.o malloc.o
endif

CLEANLIST=*.image

DIRS=
TOP=../../..
INC=-nostdinc -I../include -I$(TOP)
OPTIM=-O2
TARGETS=boot
DEF=-DEROS -D__KERNEL__ -DDYNAMIC_CRC_TABLE $(COMPRESS)
#DEF=-DDEBUG -DEROS -D__KERNEL__

VPATH=$(TOP)/zlib:.

include $(EROS_SRC)/build/lib/make/makerules.mk

all: $(TARGETS)

tst: tst.cxx
	$(GPLUS)  $(INC) tst.cxx -o tst

boot: $(OBJECTS)
	$(LD) -Ttext 0 -static -o boot -N $(OBJECTS)

install: all
	$(INSTALL) -d $(EROS_ROOT)/lib/
	$(INSTALL) -d $(EROS_ROOT)/lib/$(EROS_TARGET)
	$(INSTALL) -d $(EROS_ROOT)/lib/$(EROS_TARGET)/image
	$(INSTALL) -m 0644 $(TARGETS) $(EROS_ROOT)/lib/$(EROS_TARGET)/image/

-include .*.m
