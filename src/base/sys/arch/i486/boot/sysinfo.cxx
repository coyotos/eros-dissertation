/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <SysInfo.hxx>
#include "boot.h"

extern "C" {
  extern void printf(const char*, ...);
  extern void
    sysinfo_set_boot_disk(unsigned short hddrv, unsigned short cylsec);
  void
    get_diskinfo(unsigned char drive, BiosGeometry *bg);
}

SysInfo si __attribute__ ((section (".text")));

void
sysinfo_set_boot_disk(unsigned short hddrv, unsigned short cylsec)
{
  si.boot_drive = hddrv & 0xffu;
  si.boot.head = (hddrv >> 8) & 0xffu;
  si.boot.cyl = ((cylsec << 2) & 0x300u) | ((cylsec >> 8) & 0xffu);
  si.boot.sec = cylsec & 0x3fu;

  si.load_drive = hddrv & 0xffu;

  /* correct for the fact that our interfaces all use zero-relative
     sector numbers. */
  
  si.boot.sec--;
  
  /* fetch the drive geometry info for the boot drive: */
  get_diskinfo(si.boot_drive, &si.bootGeom);

  /* compute the starting sector number for the boot partition: */
  si.boot.startSec =
    si.boot.cyl * si.bootGeom.spcyl +
    si.boot.head * si.bootGeom.secs +
    si.boot.sec;

  /* until proven otherwise: */
  si.ramdiskSz = 0x0;
  si.ramdiskAddress = 0x0;
}
