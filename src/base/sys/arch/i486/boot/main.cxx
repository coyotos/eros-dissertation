/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/LowVolume.hxx>
#include <SysInfo.hxx>
#include "boot.h"
#include "io.h"
#include "debug.h"

#if 0
struct VolHdr;
#endif

struct BiosMemInfo {
  uint32_t  continuationValue;	// handed to/from the bios
  uint32_t  BaseAddrLow;	// low 32 bits of base address
  uint32_t  BaseAddrHigh;	// high 32 bits of base address
  uint32_t  LengthLow;		// low 32 bits of length
  uint32_t  LengthHigh;		// high 32 bits of length
  uint32_t  Type;		// address type of this range
} ;

struct UrukMemInfo {
  uint32_t  len;		// handed to/from the bios
  uint32_t  BaseAddrLow;	// low 32 bits of base address
  uint32_t  BaseAddrHigh;	// high 32 bits of base address
  uint32_t  LengthLow;		// low 32 bits of length
  uint32_t  LengthHigh;		// high 32 bits of length
  uint32_t  Type;		// address type of this range
} ;

#define AddrRangeMemory   1
#define AddrRangeReserved 2

extern "C" {
  void ProbeMachine(SysInfo *);
  void Interact(SysInfo *);
  void LoadRamDisk(SysInfo *);
  void LoadKernel(SysInfo *);
  void StartKernel(uint32_t pa, SysInfo *);
  void printf(const char*, ...);
  void halt();

  extern void get_diskinfo(unsigned char drive, BiosGeometry *bg);
  unsigned memsize(unsigned loOrHi);
  void gateA20(void);
  unsigned GetDisplayMode();

  extern void read_sectors(unsigned char drive, unsigned long startSec,
			   unsigned long nsec, void *buf);
  extern int getchar(int);
  extern void halt(void);
  extern void waitkbd(void);

  extern int QueryAddressMap(BiosMemInfo &bmi);
  extern int get_mem_map(UrukMemInfo *umi, uint32_t continuation);
}

extern SysInfo si;

int
main()
{
  ProbeMachine(&si);

  Interact(&si);
  
  si.bootFlags = BootFlags;
  
  if (BootFlags & VolHdr::VF_RAMDISK) {
    LoadRamDisk(&si);
  }
  else if (BootFlags & VolHdr::VF_COMPRESSED) {
    printf("Compressed volumes must be marked for ramdisk load.\n");
    halt();
  }

#if 0
  if (BootFlags & VolHdr::VF_DEBUG) {
    printf("Press any key to load kernel\n");
    waitkbd();
  }
#endif

  LoadKernel(&si);
  /* AFTER LoadKernel can no longer do disk I/O */

#if 0
  printf("Press any key to spin down disk drive\n");
  waitkbd();
#endif

  // spin down floppy drive to minimize wear.  0x3f2 is the floppy
  // controller's device output register.  0x08 says spin everything
  // down.  It would be better to do this with a BIOS call, but I
  // don't have my BIOS reference handy at the moment.
  out8(0x08, 0x3f2);

#if 0
  if (BootFlags & VolHdr::VF_DEBUG) {
    printf("Press any key to start kernel\n");
    waitkbd();
  }
#endif
  
  StartKernel(KERNBOOTBASE, BOOT2PA(&si, SysInfo *));
}

void
ProbeMachine(SysInfo * /* si */)
{
  for(int dev = 0; dev < SysInfo::N_BIOS_GEOM; dev ++)
    get_diskinfo(dev + 0x80, &si.driveGeom[dev]);

  si.mem.low = memsize(0);
  si.mem.ext = memsize(1);

  gateA20();

  printf("BIOS System Address Map:\n");

#if 0
  uint32_t nextContinuation = 0;
  do {
    printf("press to fetch map info\n");
    waitkbd();

    BiosMemInfo bmi;

    bmi.continuationValue = nextContinuation;

    nextContinuation = QueryAddressMap(bmi);

    const char *addrType = "unknown ";
    if (bmi.Type == AddrRangeMemory)
      addrType = "memory  ";
    else if (bmi.Type == AddrRangeMemory)
      addrType = "reserved";

    printf("  %s 0x%lx (len 0x%lx)\n",
	   addrType, bmi.BaseAddrLow, bmi.LengthLow);
  } while (nextContinuation);
#else
  uint32_t nextContinuation = 0;
  do {
    UrukMemInfo bmi;

    nextContinuation = get_mem_map(&bmi, nextContinuation);

    if (bmi.len > 0) {
      const char *addrType = "UNK";
      if (bmi.Type == AddrRangeMemory)
	addrType = "ARM";
      else if (bmi.Type == AddrRangeReserved)
	addrType = "ARR";

      printf("  %s base=0x%08lx len=0x%08lx (%ld Kbytes)\n",
	     addrType, bmi.BaseAddrLow, bmi.LengthLow,
	     bmi.LengthLow/1024u);

      /* If the BIOS Address Map stuff works, then the machine is
	 relatively new, and the old BIOS calls to fetch extended
	 memory size may or may not report a believable number (-1 is
	 a popular bad answer).  If present, the BIOS Address Map
	 should be believed in preference to the earlier BIOS call to
	 determine how much high memory we actually have. */

      if (bmi.Type == AddrRangeMemory &&
	  bmi.BaseAddrLow == 0x100000u) {
	/* The funny calculation is to ensure that upper memory comes
	   out as a multiple of page size, which the kernel expects. */
	si.mem.ext = bmi.LengthLow / EROS_PAGE_SIZE;
	si.mem.ext *= (EROS_PAGE_SIZE/1024u);
	printf(" *** Revise upper bound to %lu Kbytes\n", si.mem.ext);
      }
    }
  } while (nextContinuation);
#endif

  if (si.mem.ext < 128) {	// I'm not even going to try in < 1M.
    printf("Insufficient memory to run EROS.  It's worth the upgrade!\n");
    halt();
  }
  
  printf("\n>> EROS BOOT @ 0x%x: %d/%d k of memory\n",
	 (unsigned) BOOT2_ADDR, (int) si.mem.low, (int) si.mem.ext);

  printf("   Booting from %s%d  C/H/S=%d/%d/%d, "
	 "geometry C/H/S=%d/%d/%d\n",
	 ((si.boot_drive & 0xf0u) ? "hd" : "fd") ,
	 (int) (si.boot_drive & 0xfu),
	 (int) si.boot.cyl,
	 (int) si.boot.head,
	 (int) si.boot.sec,
	 (int) si.bootGeom.cyls,
	 (int) si.bootGeom.heads,
	 (int) si.bootGeom.secs);

  int vidmode = GetDisplayMode();
  printf("   Video mode is 0x%x\n", vidmode);
  
#if 0
  printf("Press any key to continue\n");
  waitkbd();
#endif
}

void
Interact(SysInfo * /* si */)
{
  DEBUG(unimpl) printf("Interact() not implemented\n");
}




