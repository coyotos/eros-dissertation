/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/LowVolume.hxx>
#include <SysInfo.hxx>
#include "boot.h"
#include <zlib/zlib.h>

extern "C" {
#if 0
  void ProbeMachine(SysInfo *);
  void Interact(SysInfo *);
  void LoadRamDisk(SysInfo *);
  void LoadKernel(SysInfo *);
  void StartKernel(uint32_t pa, SysInfo *);
#endif
  unsigned long crc32(unsigned long crc, const unsigned char *buf,
		      unsigned int len);
  extern void ppcpy(void *from, void *to, size_t len);
  extern void read_sectors(unsigned char drive, unsigned long startSec,
			   unsigned long nsec, void *buf);
  void LoadRamDisk(SysInfo *);
  void printf(const char*, ...);
  void halt();
  extern void *malloc(size_t);
  extern void waitkbd(void);
}

void
LoadRamDisk(SysInfo * si)
{
  if (si->mem.Extended() < (VolSectors * EROS_SECTOR_SIZE)) {
    printf("Insufficient memory to load ramdisk.\n");
    halt();
  }

  uint32_t outSectors = 0;
  
  uint32_t memSectors = VolSectors;
  memSectors += (EROS_PAGE_SECTORS - 1);
  memSectors -= (memSectors % EROS_PAGE_SECTORS);
  
  unsigned memtop = EXT_MEM_START + si->mem.Extended();
  unsigned pdest = memtop - (memSectors * EROS_SECTOR_SIZE);

  uint32_t remainingExt = pdest - EXT_MEM_START;
  remainingExt /= 1024;
  
  // Grow the ramdisk to a multiple of a page:
  si->ramdiskSz = memSectors;
  si->ramdiskAddress = pdest;
  
  if (BootFlags & VolHdr::VF_COMPRESSED) {
#ifdef SUPPORT_COMPRESSED
#define IN_BUF_SECS 24
#define OUT_BUF_SECS 8
#define IN_BUF_SZ (IN_BUF_SECS * EROS_SECTOR_SIZE)
#define OUT_BUF_SZ (OUT_BUF_SECS * EROS_SECTOR_SIZE)

    unsigned char *in_buf = (unsigned char *) malloc(IN_BUF_SZ);
    unsigned char *out_buf = (unsigned char *) malloc(OUT_BUF_SZ);

#if 0
    printf("in_buf: 0x%x out_buf: 0x%x\n", (unsigned) in_buf,
	   (unsigned) out_buf);
#endif
    
    uint32_t in_sectors = ZipLen + EROS_SECTOR_SIZE - 1;
    in_sectors /= EROS_SECTOR_SIZE;
    printf(">> Loading %ld compressed (%ld decompressed) sectors\n"
	   "      into ramdisk at 0x%x... ",
	   in_sectors, VolSectors, pdest);

    /* I originally thought to just copy the in-memory bootstrap into
       high memory, but this would not mirror the bits on the disk
       correctly because by this point the state of the .data and .bss
       sections has changed.  At this time that would not impact
       anything much, but in the future I want to be able to warm boot
       the system, and it seems like a bad plan to have a modified
       ramdisk image.  I cannot point to anything specific, but the
       cost of reading the extra 32k to 64k is small enough that I
       don't care about it. */
       
    read_sectors(si->boot_drive, si->boot.startSec,
		 DISK_BOOTSTRAP_SECTORS, (void *) pdest); 

    /* Turn off the compressed image bit in the ramdisk bootstrap
       header... */
    VolHdr *rd_vh = PA2BOOT(pdest, VolHdr *);
    rd_vh->BootFlags &= ~VolHdr::VF_COMPRESSED;
    
    pdest += EROS_SECTOR_SIZE * DISK_BOOTSTRAP_SECTORS;

    if (BootSectors != DISK_BOOTSTRAP_SECTORS) {
      printf("BootSectors and DISK_BOOTSTRAP_SECTORS do not match!\n");
      halt();
    }
    
    /* Decompress the volume */
    int err;
    int mode = Z_NO_FLUSH;
    z_stream z;

    z.total_in = 0;
    z.avail_in = 0;
    z.next_out = out_buf;
    z.total_out = 0;
    z.avail_out = OUT_BUF_SZ;
  
    z.zalloc = (alloc_func)0;
    z.zfree = (free_func)0;
    z.opaque = (voidpf)0;
    
    z.msg = NULL;

    err = inflateInit(&z);

    if (err != Z_OK) {
      printf("Could not initialize decompression library\n");
      halt();
    }

    uint32_t len = ZipLen;
    uint32_t cur_sec = DISK_BOOTSTRAP_SECTORS;

    do {
      mode = Z_NO_FLUSH;

      if (z.avail_in == 0 && len) {
	read_sectors(si->boot_drive, si->boot.startSec + cur_sec, IN_BUF_SECS,
		     BOOT2PA(in_buf, void *));
	z.avail_in = min(len, IN_BUF_SZ);
	z.next_in = in_buf;
	len -= z.avail_in;
	cur_sec += IN_BUF_SECS;
      }

      if (z.avail_in == 0)
	mode = Z_FINISH;

      if (z.avail_out == 0) {
	ppcpy(BOOT2PA(out_buf, void *), (void *) pdest, OUT_BUF_SZ);
	pdest += OUT_BUF_SZ;
	z.avail_out = OUT_BUF_SZ;
	z.next_out = out_buf;

	outSectors += OUT_BUF_SECS;
      }
    } while ((err = inflate(&z, mode)) == Z_OK);

    if (z.avail_out < OUT_BUF_SZ) {
      int len = OUT_BUF_SZ - z.avail_out;
      
      ppcpy(BOOT2PA(out_buf, void *), (void *) pdest, len);
      pdest += len;
      z.avail_out = OUT_BUF_SZ;
      z.next_out = out_buf;

      outSectors += (len / EROS_SECTOR_SIZE);
    }

    inflateEnd(&z);

    if (err != Z_STREAM_END) {
      printf("Compressed floppy image is corrupt (zlib err %d)\n", err);
      halt();
    }

    printf("done \001\n"); /* IBM character set happy face*/

    outSectors += DISK_BOOTSTRAP_SECTORS;
    
    if (outSectors != VolSectors) {
      printf("!! VolSectors %ld, decompressed sectors %ld -- press any key\n",
	     VolSectors, outSectors);
      halt();
    }
#else
    printf("Compressed volume load not yet supported\n");
#endif
  }
  else {
    printf(">> Loading %d sectors into ramdisk at 0x%x... ",
	   (int) VolSectors, pdest);

    read_sectors(si->boot_drive, si->boot.startSec, VolSectors,
		 (void *) pdest);

    printf("done \001\n"); /* IBM character set happy face*/
  }
  

  printf("   Ramdisk loaded. %d/%d k of memory remaining\n",
	 (int) si->mem.low, (int) remainingExt);

  si->load_drive = RAM_DISK_ID;
#if 0
#error "adjust to take buf[] out of boot heap!"
  {
    unsigned char buf[EROS_SECTOR_SIZE];
    
    unsigned int crc = 0;
    uint32_t sec = 0;
  
    for (sec = 0; sec < VolSectors; sec++) {
      ppcpy((void *) (pdest + (sec * EROS_SECTOR_SIZE)),
	    BOOT2PA(buf, void*), EROS_SECTOR_SIZE);

      crc = crc32(crc, buf, EROS_SECTOR_SIZE);
    }

    printf("CRC32 of %ld sec ramdisk is 0x%x\n", VolSectors, crc);
  }
#endif
}

