/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/LowVolume.hxx>
#include <disk/PagePot.hxx>
#include <SysInfo.hxx>
#include "boot.h"
#include "debug.h"

extern "C" {
  extern void *memcpy(void *, const void *, size_t n);
}

extern "C" {
#if 0
  void ProbeMachine(SysInfo *);
  void Interact(SysInfo *);
  void LoadRamDisk(SysInfo *);
  void StartKernel(uint32_t pa, SysInfo *);

  extern void get_diskinfo(unsigned char drive, BiosGeometry *bg);
  unsigned memsize(unsigned loOrHi);
  void gateA20(void);
  unsigned GetDisplayMode();
#endif
  void LoadKernel(SysInfo *);

  unsigned long crc32(unsigned long crc, const unsigned char *buf,
		      unsigned int len);
  void printf(const char*, ...);
  void halt();
  extern void read_sectors(unsigned char drive, unsigned long startSec,
			   unsigned long nsec, void *buf);
  extern void ppcpy(void *from, void *to, size_t len);
}
void *
memcpy(void *to, const void *from, size_t count)
{
  unsigned char *cto = (unsigned char *) to;
  unsigned char *cfrom = (unsigned char *) from;

  while (count--)
    *cto++ = *cfrom++;

  return to;
}

Division
FindKernelDivision(SysInfo * si)
{
  char buf[EROS_PAGE_SIZE];

  uint32_t divSector = DivTable;

  /* If we loaded a ramdisk, don't bias the load address: */
  if (si->load_drive == si->boot_drive)
    divSector += si->boot.startSec;
  
  DEBUG(load) printf("Division table at sector %ld\n", divSector);

  read_sectors(si->load_drive, divSector,
	       EROS_PAGE_SECTORS, BOOT2PA(buf, void*));

  Division* divtab = (Division *) buf;
  
  int div = 0;

  for(div = 0; div < NDIVENT; div++, divtab++) {
    if (divtab->type == dt_Kernel)
      break;
    if (divtab->type == dt_Unused)
      break;
  }

  if (divtab->type == dt_Unused) {
    printf("... not found\n");
    halt();
  }
  else {
    DEBUG(load) printf("kernel at %ld end %ld\n",
		       divtab->start, divtab->end);
  }

  return *divtab;		// calls memcpy!
}

void
LoadKernel(SysInfo * si)
{
  Division kdiv = FindKernelDivision(si);

  uint32_t kpages = kdiv.endOid - kdiv.startOid;
  kpages /= EROS_OBJECTS_PER_FRAME;
    
  uint32_t cursec = kdiv.start;
  if (si->boot_drive == si->load_drive)
    cursec += si->boot.startSec;

  /* Add bias for range header page with ckpt seq number */
  cursec += EROS_PAGE_SECTORS;

  /* Load the kernel into extended memory.  We will shift it when
     we are done with the I/O buffer */
  uint32_t pdest = (uint32_t) BOOT2_HEAP_PTOP;
  
  printf(">> Loading kernel (%ld pages) @ 0x%lx... ", kpages, pdest);

  uint32_t pg = 0;
    
  while (pg < kpages) {
    if ((pg % DATA_PAGES_PER_PAGE_CLUSTER) == 0)
      cursec += EROS_PAGE_SECTORS;

    uint32_t npg = min(kpages - pg, DATA_PAGES_PER_PAGE_CLUSTER);

    read_sectors(si->load_drive, cursec, npg * EROS_PAGE_SECTORS,
		 (void *) pdest);

    pdest += (npg * EROS_PAGE_SIZE);
    cursec += (npg * EROS_PAGE_SECTORS);
    pg += npg;
  }

  printf("done \001\n"); /* IBM character set happy face*/
  /* Now shift the kernel to its final location */
  if ((BOOT2_HEAP_PTOP) != KERNPBASE) {
    ppcpy ((void *) BOOT2_HEAP_PTOP,
	   (void *) KERNPBASE, kpages * EROS_PAGE_SIZE);

    printf("   Relocated kernel to @ 0x%x\n", KERNPBASE);
  }
}
