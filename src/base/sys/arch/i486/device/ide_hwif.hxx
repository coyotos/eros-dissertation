/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

struct ide_group;

struct ide_hwif : public BlockDev {
  friend class ide_drive;
protected:
  int		ndx;

  ide_drive	drive[MAX_DRIVE];
  uint8_t		irq;		// nonzero == assigned.
  uint32_t          ioBase;		// io port at which interface is assigned.

  IDE::chipset	chipset;

  // Do not use bitmaps -- shitty compilation.
  bool		noprobe;	// do not probe for this interface
  bool		present;	// interface is present

  // uint8_t		selected_drive;
  uint8_t		cur_drive;	// which drive is active
  
  ide_group     *group;

#if 0
  void Put8(uint16_t reg, uint8_t value)
  {
    outb(reg + ioBase, value);
  }
#else
  void Put8(uint16_t reg, uint8_t value);
#endif
  
  uint8_t Get8(uint16_t reg)
  {
    return inb(reg + ioBase);
  }
  
#if 0
  void PutCtl8(uint8_t value)
  {
    outb(ctlBase, value);
  }
  
  uint8_t GetCtl8()
  {
    return inb(ctlBase);
  }
#endif
  
  void Put16(uint16_t reg, uint16_t value)
  {
    old_outh(reg + ioBase, value);
  }
  
  uint16_t Get16(uint16_t reg)
  {
    return inh(reg + ioBase);
  }
  
#if 0
  void PutCtl16(uint16_t value)
  {
    old_outh(ctlBase, value);
  }
  
  uint16_t GetCtl16()
  {
    return inh(ctlBase);
  }
#endif

  void SelectDrive(uint32_t ndx);

  static void OnIntr(struct IntAction*);
  // Interrupt handler functions return true when the current phase of
  // the request is over.  If further interrupts are expected for the
  // current phase, they return false.
  bool (ide_hwif::*int_handler)();
  void SetHandler(uint8_t unit, bool (ide_hwif::*handler)());
  
  // Interrupt handlers:
  bool SetMultmodeIntr();
  bool RecalibrateIntr();
  bool SetGeometryIntr();
  bool ReadIntr();
  bool WriteIntr();
  bool MultWriteIntr();
  
  // Issue a simple drive command
  void DoCmd(uint8_t unit, uint8_t cmd, uint8_t nsec, bool (ide_hwif::*handler)());

  void DoReset1(uint8_t unit, bool suppressAtapi);
  void DoReset(uint8_t unit);
  
  // DMA handler fn pointer -- most controllers still don't do onboard
  // DMA.  For the ones that do, this pointer points to the
  // handler/setup function.  It's not clear to me if this should be
  // part of ide_drive or ide_hwif.

  enum dma_cmd {
    dma_read,
    dma_write,
    dma_abort,
    dma_check,
    dma_status_bad,
    dma_transferred,
    dma_begin
  } ;
  
  // Return true on success
  bool (*dma_handler)(dma_cmd, ide_drive*);

public:
  ide_hwif();

  void Probe();

  // EXTERNAL BLOCKDEV PROTOCOL:

  void MountUnit(uint8_t unit);	// called by mount daemon
  void GetUnitInfo(uint8_t unit, BlockUnitInfo&);

  void InsertRequest(Request* req);
  void StartIO();

  // INTERFACE CALLED BY HW GROUP LOGIC:

  // Return true if request is completed, false otherwise.
  bool StartRequest(Request*);
  Request* GetNextRequest();
} ;
