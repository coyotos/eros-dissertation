/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// TODO: This interface won't work with older ST506 or ESDI drives.
// At the moment I have no plans to support such drives.  Ever.  I'm
// probably the only person on the face of the planet who still owns
// any that work :-).  Even the LINUX crew are now compiling those
// drives out by default.

// #define VERBOSE_BUG

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/IoRegion.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/SysTimer.hxx>
#include <kerninc/BlockDev.hxx>
#include <kerninc/IoRequest.hxx>
#ifdef VERBOSE_BUG
#include <arch/i486/kernel/lostart.hxx>
#endif
#include <arch/i486/kernel/SysConfig.hxx>
#include <arch/i486/kernel/CMOS.hxx>

#include "io.h"

#include "ide_ide.hxx"

#include "ide_drive.hxx"
#include "ide_hwif.hxx"
#include "ide_group.hxx"
	      
#define dbg_probe	0x1	/* steps in taking snapshot */

/* Following should be an OR of some of the above */
#define dbg_flags   ( 0u )

#define DBCOND(x) (dbg_##x & dbg_flags)
#define DEBUG(x) if DBCOND(x)
#define DEBUG2(x,y) if ((dbg_##x|dbg_##y) & dbg_flags)

// #define IDE_DEBUG

#define INITIAL_MULT_COUNT 8	/* 4K per xfer max */
#define IS_PROMISE_DRIVE 0	/* for now */

#define FANCY_STATUS_DUMPS

/*
 * Probably not wise to fiddle with these
 */
#define ERROR_MAX	8	/* Max read/write errors per sector */
#define ERROR_RESET	3	/* Reset controller every 4th retry */
#define ERROR_RECAL	1	/* Recalibrate every 2nd retry */

#define DRIVE_SELECT_DELAY 10	// in MILLISECONDS
#define DRIVE_DETECT_DELAY 50

ide_drive::ide_drive()
{
  present = false;
  mounted = false;
  needsMount = false;
  removable = false;
  media = IDE::med_disk;
  ctl = 0x08;
#if 0
  p_chs.heads = 0;
  p_chs.secs = 0;
  p_chs.cyls = 0;
#endif
  l_chs.hd = 0;
  l_chs.sec = 0;
  l_chs.cyl = 0;
  b_chs.hd = 0;
  b_chs.sec = 0;
  b_chs.cyl = 0;
  select.all = 0xa0;
  flags.all = 0;
  flags.b.recal = 1; // seek to track 0
  flags.b.setGeom = 1; // tell it its geometry
  id = 0;
  name[0] = 'h';
  name[1] = 'd';
  name[2] = '?';
  name[3] = '?';
  name[4] = 0;
}

/*
 * lba_capacity_is_ok() performs a sanity check on the claimed "lba_capacity"
 * value for this drive (from its reported identification information).
 *
 * Returns:	true if lba_capacity looks sensible
 *		false otherwise
 */
bool ide_driveid::CheckLbaCapacity()
{
  uint32_t lba_sects   = lba_capacity;
  uint32_t chs_sects   = cyls * heads * sectors;
  uint32_t _10_percent = chs_sects / 10;

  /* perform a rough sanity check on lba_sects:  within 10% is "okay" */
  if ((lba_sects - chs_sects) < _10_percent)
    return true;		/* lba_capacity is good */

  /* some drives have the word order reversed */
  lba_sects = (lba_sects << 16) | (lba_sects >> 16);
  if ((lba_sects - chs_sects) < _10_percent) {
    lba_capacity = lba_sects;	/* fix it */
    return true;		/* lba_capacity is (now) good */
  }

  return false;			/* lba_capacity value is bad */
}

/*
 * current_capacity() returns the capacity (in sectors) of a drive
 * according to its current geometry/LBA settings.
 */
uint32_t
ide_drive::Capacity()
{
  struct ide_driveid *drvid = (ide_driveid*) id;
#if 0
  MsgLog::printf("l_chs: %d/%d/%d\n", l_chs.cyl, l_chs.hd, l_chs.sec);
#endif
  
  uint32_t capacity = l_chs.cyl * l_chs.hd * l_chs.sec;

  if (!present)
    return 0;
  
  if (media != IDE::med_disk)
    return UINT32_MAX;	/* cdrom or tape */

  if (!IS_PROMISE_DRIVE) {
    select.b.lba = 0;
    /* Determine capacity, and use LBA if the drive properly supports it */
    if (drvid != NULL && (drvid->capability & 2)
	&& drvid->CheckLbaCapacity()) {

      if (drvid->lba_capacity >= capacity) {
	capacity = drvid->lba_capacity;
	select.b.lba = 1;
      }
    }
  }

  return capacity;
#if 0
  return (capacity - drive->sect0);
#endif
}

/*
 * This is used for most PIO data transfers *from* the IDE interface
 */
void
ide_drive::InputData(void *buffer, unsigned int wcount)
{
  uint16_t io_base  = hwif->ioBase;
  uint16_t data_reg = io_base+IDE_DATA;

#if 0
  uint8_t io_32bit = io_32bit;

  if (io_32bit) {
#if SUPPORT_VLB_SYNC
    if (io_32bit & 2) {
      cli();
      do_vlb_sync(io_base+IDE_NSECS);
      insw(data_reg, buffer, wcount);
      if (drive->unmask)
	sti();
    } else
#endif /* SUPPORT_VLB_SYNC */
      insw(data_reg, buffer, wcount);
  } else
#endif
    ins16(data_reg, buffer, wcount<<1);
}

/*
 * ide_multwrite() transfer sectors to the drive, being careful not to
 * overrun the drive's multCount limit.
 */
void
ide_drive::MultWrite (Request *req)
{
  uint32_t nsec = req->req_nsec;

  // Can't do more than the block size limit:
  if (nsec > multCount)
    nsec = multCount;

  OutputData((void *) req->req_ioaddr, nsec * EROS_SECTOR_SIZE >> 2);

  req->nsec = nsec;
}

/*
 * This is used for most PIO data transfers *from* the IDE interface
 */
void
ide_drive::OutputData(void *buffer, unsigned int wcount)
{
  uint16_t io_base  = hwif->ioBase;
  uint16_t data_reg = io_base+IDE_DATA;

#if 0
  uint8_t io_32bit = io_32bit;

  if (io_32bit) {
#if SUPPORT_VLB_SYNC
    if (io_32bit & 2) {
      cli();
      do_vlb_sync(io_base+IDE_NSECS);
      insw(data_reg, buffer, wcount);
      if (drive->unmask)
	sti();
    } else
#endif /* SUPPORT_VLB_SYNC */
      insw(data_reg, buffer, wcount);
  } else
#endif

    outs16(data_reg, buffer, wcount<<1);
}


void
ide_fixstring (uint8_t *s, const int bytecount, const int byteswap)
{
  uint8_t *p = s, *end = &s[bytecount & ~1]; /* bytecount must be even */

  if (byteswap) {
    /* convert from big-endian to host byte order */
    for (p = end ; p != s;) {
      unsigned short *pp = (unsigned short *) (p -= 2);
      *pp = Machine::ntohhw(*pp);
    }
  }

  /* strip leading blanks */
  while (s != end && *s == ' ')
    ++s;

  /* compress internal blanks and strip trailing blanks */
  while (s != end && *s) {
    if (*s++ != ' ' || (s != end && *s && *s != ' '))
      *p++ = *(s-1);
  }

  /* wipe out trailing garbage */
  while (p != end)
    *p++ = '\0';
}

void
ide_drive::AtapiIdentify(uint8_t cmd)
{
  int bswap;
  //  uint32_t capacity, check;

  id = (ide_driveid *) new (0) char[EROS_SECTOR_SIZE];
  
  InputData(id, EROS_SECTOR_SIZE >> 2); /* read 512 bytes of id info */

  MsgLog::printf("Got atapi data.\n");
  /*
   * EATA SCSI controllers do a hardware ATA emulation:  ignore them
   */
  if ((id->model[0] == 'P' && id->model[1] == 'M')
      || (id->model[0] == 'S' && id->model[1] == 'K')) {
    MsgLog::printf("%s: EATA SCSI HBA %40s\n", name, id->model);
    present = 0;
    return;
  }

  /*
   *  WIN_IDENTIFY returns little-endian info,
   *  WIN_PIDENTIFY *usually* returns little-endian info.
   */
  bswap = 1;
  if (cmd == WIN_PIDENTIFY) {
    if ((id->model[0] == 'N' && id->model[1] == 'E') /* NEC */
	|| (id->model[0] == 'F' && id->model[1] == 'X') /* Mitsumi */
	|| (id->model[0] == 'P' && id->model[1] == 'i'))/* Pioneer */
      bswap = 0;	/* Vertos drives may still be weird */
  }
  ide_fixstring (id->model,     sizeof(id->model),     bswap);
  ide_fixstring (id->fw_rev,    sizeof(id->fw_rev),    bswap);
  ide_fixstring (id->serial_no, sizeof(id->serial_no), bswap);

  /*
   * Check for an ATAPI device
   */
  if (cmd == WIN_PIDENTIFY) {
    uint8_t type = (id->config >> 8) & 0x1f;
    MsgLog::printf("%s: %s, ATAPI ", name, id->model);
    switch (type) {
    case 0:		/* Early cdrom models used zero */
    case 5:
      MsgLog::printf ("CDROM drive\n");
      media = IDE::med_cdrom;
      present = 0;		// TEMPORARY
      removable = 1;
      return;
    case 1:
#ifdef IDE_TAPE
      MsgLog::printf ("TAPE drive");
      if (idetape_identify_device (drive,id)) {
	media = ide_tape;
	present = 1;
	removeable = 1;
	if (hwif->dmaproc != NULL &&
	    !hwif->dmaproc(ide_dma_check, drive))
	  MsgLog::printf(", DMA");
	MsgLog::printf("\n");
      }
      else {
	present = 0;
	MsgLog::printf ("\nide-tape: the tape is not supported by this version of the driver\n");
      }
      return;
#else
      MsgLog::printf ("TAPE ");
      break;
#endif /* CONFIG_BLK_DEV_IDETAPE */
    default:
      present = 0;
      MsgLog::printf("Type %d - Unknown device\n", type);
      return;
    }
    present = 0;
    MsgLog::printf("- not supported by this kernel\n");
    return;
  }

  /* check for removeable disks (eg. SYQUEST), ignore 'WD' drives */
  if (id->config & (1<<7)) {	/* removeable disk ? */
    if (id->model[0] != 'W' || id->model[1] != 'D')
      removable = 1;
  }

  /* SunDisk drives: treat as non-removeable, force one unit */
  if (id->model[0] == 'S' && id->model[1] == 'u') {
    removable = 0;
    if (select.b.unit) {
      present = 0;
      return;
    }
  }
	
  media = IDE::med_disk;
  /* Extract geometry if we did not already have one for the drive */
  if (!present) {
    present = 1;
    l_chs.cyl  = b_chs.cyl  = id->cyls;
    l_chs.hd   = b_chs.hd   = id->heads;
    l_chs.sec  = b_chs.sec  = id->sectors;
  }

  /* Handle logical geometry translation by the drive */
  if ((id->field_valid & 1) && id->cur_cyls && id->cur_heads
      && (id->cur_heads <= 16) && id->cur_sectors)
    {
      /*
       * Extract the physical drive geometry for our use.
       * Note that we purposely do *not* update the bios info.
       * This way, programs that use it (like fdisk) will
       * still have the same logical view as the BIOS does,
       * which keeps the partition table from being screwed.
       *
       * An exception to this is the cylinder count,
       * which we reexamine later on to correct for 1024 limitations.
       */
      l_chs.cyl  = id->cur_cyls;
      l_chs.hd   = id->cur_heads;
      l_chs.sec  = id->cur_sectors;

      /* check for word-swapped "capacity" field in id information */
      uint32_t capacity = l_chs.cyl * l_chs.hd * l_chs.sec;
      uint32_t check = (id->cur_capacity0 << 16) | id->cur_capacity1;
      if (check == capacity) {	/* was it swapped? */
	/* yes, bring it into little-endian order: */
	id->cur_capacity0 = (capacity >>  0) & 0xffff;
	id->cur_capacity1 = (capacity >> 16) & 0xffff;
      }
    }

  /* Use physical geometry if what we have still makes no sense */
  if ((!l_chs.hd || l_chs.hd > 16) && id->heads && id->heads <= 16) {
    l_chs.cyl  = id->cyls;
    l_chs.hd   = id->heads;
    l_chs.sec  = id->sectors;
  }
  /* Correct the number of cyls if the bios value is too small */
  if (l_chs.sec == b_chs.sec && l_chs.hd == b_chs.hd) {
    if (l_chs.cyl > b_chs.cyl)
      b_chs.cyl = l_chs.cyl;
  }

  (void) Capacity();

  MsgLog::printf ("%s: %40s, %dMB w/%dkB Cache, %sCHS=%d/%d/%d",
	  name, id->model, Capacity()/2048L,
		  id->buf_size/2,
	  select.b.lba ? "LBA, " : "",
	  b_chs.cyl, b_chs.hd, b_chs.sec);

  multCount = 0;
  if (id->max_multsect) {
    multReq = INITIAL_MULT_COUNT;
    if (multReq > id->max_multsect)
      multReq = id->max_multsect;
    if (multReq || ((id->multsect_valid & 1) && id->multsect)) {
      flags.b.setMultMode = 1;
    }
  }

  if (hwif->dma_handler != 0) {	/* hwif supports DMA? */
    if (!(hwif->dma_handler(ide_hwif::dma_check, this)))
      MsgLog::printf(", DMA");
  }

  MsgLog::printf("\n");
}

uint8_t
ide_drive::Identify(uint8_t cmd)
{
  uint32_t irqs = 0;
  uint16_t hd_status = IDE_CTL_ALTSTATUS; // non-intrusive preferred
  uint8_t rc = 1;
  
  if (hwif->irq == 0) {
    IRQ::EndProbe(IRQ::BeginProbe()); // clear pending interrupts
    irqs = IRQ::BeginProbe();	// wait for interrupt
    hwif->Put8(IDE_CTL_ALTSTATUS, ctl); // enable device IRQ
  }

  Machine::SpinWaitMs(DRIVE_DETECT_DELAY);
  
  if ((hwif->Get8(IDE_CTL_ALTSTATUS) ^
       hwif->Get8(IDE_STATUS)) &
      ~INDEX_STAT) {
    MsgLog::printf("%s: probing with STATUS instead of ALTSTATUS\n", name);
    hd_status = IDE_STATUS;	/* use intrusive polling */
  }

  hwif->Put8(IDE_CMD, cmd);
  
  uint64_t waitTill = ((cmd == WIN_IDENTIFY) ? WAIT_WORSTCASE : WAIT_PIDENTIFY);
  waitTill = Machine::MillisecondsToTicks(waitTill);
  waitTill += SysTimer::Now();
  
  do {
    if (SysTimer::Now() > waitTill) {
      if (hwif->irq == 0)
	(void) IRQ::EndProbe(irqs);
      MsgLog::printf("Drive timed out.\n");
      return 1;	/* drive timed-out */
    }

    Machine::SpinWaitMs(DRIVE_DETECT_DELAY);
  } while (hwif->Get8(hd_status) & BUSY_STAT);

  Machine::SpinWaitMs(DRIVE_DETECT_DELAY);	// wait for IRQ and DRQ_STAT
  uint8_t status = hwif->Get8(IDE_STATUS);
  
  if (OK_STAT(status,DRQ_STAT,BAD_R_STAT)) {
    MsgLog::printf("Drive returned ID.  Fetching ATAPI info...\n");
    // Drive returned an ID.  Do an ATAPI identify:
    AtapiIdentify(cmd);

#if 0
    if (present && media != IDE::med_tape) {
      // tune the PIO mode... the BIOS ought to do this these days.
    }
#endif

    rc = 0;			/* drive responded with ID */
  } else {
    MsgLog::printf("Drive refused ID.\n");
    rc = 2;			/* drive refused ID */
  }

  if (hwif->irq == 0) {
    irqs = IRQ::EndProbe(irqs);	/* get irq number */

    if (irqs > 0)
      hwif->irq = irqs;
    else				/* Mmmm.. multiple IRQs */
      MsgLog::printf("%s: IRQ probe failed (%d)\n", name, irqs);
  }

  DEBUG(probe)
    MsgLog::dprintf(true, "Pausing after drive probe...\n");

  return rc;
}

void
ide_hwif::Put8(uint16_t reg, uint8_t value)
{
#if 0
  MsgLog::printf("Writing byte to hwifno %d reg 0x%04x, %d\n",
		 ndx, reg + ioBase, value);
#endif
  old_outb(reg + ioBase, value);
}

uint32_t
ide_drive::DoProbe(uint8_t cmd)
{
  if (present && (media != IDE::med_disk) && (cmd == WIN_IDENTIFY)) {
    MsgLog::fatal("Drive obviously not IDE\n");
    return 4;
  }

  MsgLog::printf("probing for %s: present=%d, media=%d, probetype=%s hwif=0x%08x\n\n",
		 name, present, media,
		 (cmd == WIN_IDENTIFY) ? "ATA" : "ATAPI",
		 hwif);

  // The following OUGHT to be redundant:
  hwif->SelectDrive(ndx);
  Machine::SpinWaitMs(DRIVE_SELECT_DELAY);

  if (hwif->Get8(IDE_DRV_HD) != select.all  && !present) {
    hwif->Put8(IDE_DRV_HD, 0xa0); // select drive 0
    MsgLog::printf("Drive didn't select and not present\n");
    return 3;
  }
    
  uint8_t status = hwif->Get8(IDE_STATUS);
  uint8_t rc;
  
  if (OK_STAT(status,READY_STAT,BUSY_STAT) ||
      present || cmd == WIN_PIDENTIFY) {
    
    // try to identify the drive twice:
    if  ((rc = Identify(cmd)))
      rc = Identify(cmd);

    // Make sure interrupt status is clear:
    uint8_t status = hwif->Get8(IDE_STATUS);

    if (rc)
      MsgLog::printf("%s: no response(status = 0x%02x)\n", name,
		     status);
  }
  else {
    MsgLog::printf("Drive didn't select\n");
    rc = 3;				/* not present or maybe ATAPI */
  }
  
  if (select.b.unit != 0) {	// unit not 0
    MsgLog::printf("Reselect drive 0. ");
    hwif->Put8(IDE_DRV_HD, 0xa0); // select drive 0
    Machine::SpinWaitMs(DRIVE_SELECT_DELAY);

    // Make sure interrupt status is clear:
    uint8_t result=hwif->Get8(IDE_CTL_ALTSTATUS);
    MsgLog::printf("Alt status : 0x%02x\n", result);
    
    (void) hwif->Get8(IDE_STATUS);
  }
  return rc;
}

void
ide_drive::Probe()
{
#if 0
  if (noprobe)			/* skip probing? */
    return;
#endif

  if (DoProbe(WIN_IDENTIFY) >= 2) { /* if !(success||timed-out) */
    (void) DoProbe(WIN_PIDENTIFY); /* look for ATAPI device */
  }

  if (!present)
    return;			/* drive not found */

  needsMount = true;
  
  // Double check the results of the probe:
  
  if (id == NULL) {		/* identification failed? */
    if (media == IDE::med_disk) {
      MsgLog::printf("%s: old drive, CHS=%d/%d/%d\n",
		     name, b_chs.cyl, b_chs.hd, b_chs.sec);
    }
    else if (media == IDE::med_cdrom) {
      MsgLog::printf("%s: ATAPI CDROM?\n", name);
    }
    else {
      present = false;	/* nuke it */
    }
  }
}

void
ide_drive::DoDriveCommand(Request * /* req */)
{
  MsgLog::fatal("Drive commands not implemented\n");
}

/*
 * do_rw_disk() issues READ and WRITE commands to a disk,
 * using LBA if supported, or CHS otherwise, to address sectors.
 * It also takes care of issuing special DRIVE_CMDs.
 */
#if 0
static inline void do_rw_disk (ide_drive_t *drive, struct request *rq, unsigned long block)
#endif
void
ide_drive::DoDiskRequest(Request *req)
{
  // NOTE: Plug requests are handled in ide_hwif::StartRequest()
  
  hwif->Put8(IDE_CTL_ALTSTATUS, ctl);

  // We can do the whole transfer until proven otherwise.
  req->nsec = req->req_nsec;
  
  uint8_t sec, hd;
  uint16_t cyl;
  uint8_t sel = 0;
  
  if (select.b.lba) {
#if defined(IDE_DEBUG)
    MsgLog::printf("%s: %sing: LBAsect=%d, sectors=%d, buffer=0x%08x\n",
	   name, (req->cmd==Request::Read)?"read":"writ",
	   req->req_start, req->nsec, (unsigned long) req->req_ioaddr);
#endif
    sec = req->req_start;
    cyl = req->req_start >> 8;
    hd = (req->req_start >> 24) & 0x0f;
    
  } else {
    uint32_t track = req->req_start / l_chs.sec;
    sec = req->req_start % l_chs.sec;
    
    hd  = track % l_chs.hd;
    cyl  = track / l_chs.hd;

    sec++;

#if defined(IDE_DEBUG)
    MsgLog::printf("%s: %sing: CHS=%d/%d/%d, sectors=%d, buffer=0x%08x\n",
	   name, (req->cmd==Request::Read)?"read":"writ", cyl,
	   hd, sec, req->nsec, (unsigned long) req->req_ioaddr);
#endif
  }
  sel = hd | select.all;

  // IDE can always issue a request for more sectors than we need.  We
  // go ahead and issue the request for the complete read here.
  //
  // FIX: Someday we need a strategy here for error recovery.
  
  hwif->Put8(IDE_NSECS, req->nsec);
  hwif->Put8(IDE_SECTOR, sec);
  hwif->Put8(IDE_CYL_LO, cyl);
  hwif->Put8(IDE_CYL_HI, cyl >> 8);
  hwif->Put8(IDE_DRV_HD, sel);

#if defined(IDE_DEBUG)
  MsgLog::printf("Start %s: start %d nsec %d at 0x%08x\n",
		 ((req->cmd == Request::Read) ? "read" : "write"),
		 req->req_start, req->req_nsec, req->req_ioaddr);
#endif

#if 0
  if (IS_PROMISE_DRIVE) {
    if (hwif->is_promise2 || rq->cmd == READ) {
      do_promise_io (drive, rq);
      return;
    }
  }
#endif

  if (req->cmd == IoCmd::Read) {
#if defined(IDE_DEBUG)
    uint8_t status = hwif->Get8(IDE_STATUS);
    MsgLog::printf("Status prior to initating: 0x%x\n", status);
#endif

    // If we are able to use DMA, do so.
    assert (use_dma == false);
    if (use_dma && hwif->dma_handler(ide_hwif::dma_read, this))
      return;

    hwif->SetHandler(ndx, &ide_hwif::ReadIntr);
    hwif->Put8(IDE_CMD, multCount ? WIN_MULTREAD : WIN_READ);
#if defined(IDE_DEBUG)
    MsgLog::dprintf(false, "Return from DoDiskRequest\n");
#endif
    return;
  }

  if (req->cmd == IoCmd::Write) {
    // Use multwrite whenever possible.

    assert (use_dma == false);
    if (use_dma && hwif->dma_handler(ide_hwif::dma_write, this))
      return;

    hwif->Put8(IDE_CMD, multCount ? WIN_MULTWRITE : WIN_WRITE);

    if ( WaitStatus(DATA_READY, BAD_W_STAT, WAIT_DRQ) ) {
      MsgLog::printf("%s: no DRQ after issuing %s\n", name,
	     multCount ? "MULTWRITE" : "WRITE");
      return;
    }

    if (multCount) {
      hwif->group->curReq = req;

      hwif->SetHandler (ndx, &ide_hwif::MultWriteIntr);
      MultWrite(req);
    } else {
      hwif->SetHandler (ndx, &ide_hwif::WriteIntr);
      OutputData((void*) req->req_ioaddr, EROS_SECTOR_SIZE >> 2);
    }

    return;
  }

#if 0
  MsgLog::printf("%s: bad command: %d\n", drive->name, rq->cmd);
  ide_end_request(0, HWGROUP(drive));
#endif
}

void
ide_drive::EndDriveCmd(uint8_t /* stat */, uint8_t /* err */)
{
  MsgLog::fatal("Drive end command not implemented\n");
}


/*
 * Error reporting, in human readable form (luxurious, but a memory hog).
 */
uint8_t
ide_drive::DumpStatus (const char *msg, uint8_t stat, Request* req)
{
  uint8_t err = 0;

  MsgLog::printf("%s: %s: status=0x%02x", name, msg, stat);
#ifdef FANCY_STATUS_DUMPS
  if (media == IDE::med_disk) {
    MsgLog::printf(" { ");
    if (stat & BUSY_STAT)
      MsgLog::printf("Busy ");
    else {
      if (stat & READY_STAT)	MsgLog::printf("DriveReady ");
      if (stat & WRERR_STAT)	MsgLog::printf("DeviceFault ");
      if (stat & SEEK_STAT)	MsgLog::printf("SeekComplete ");
      if (stat & DRQ_STAT)	MsgLog::printf("DataRequest ");
      if (stat & ECC_STAT)	MsgLog::printf("CorrectedError ");
      if (stat & INDEX_STAT)	MsgLog::printf("Index ");
      if (stat & ERR_STAT)	MsgLog::printf("Error ");
    }
    MsgLog::printf("}");
  }
#endif	/* FANCY_STATUS_DUMPS */
  MsgLog::printf("\n");
  if ((stat & (BUSY_STAT|ERR_STAT)) == ERR_STAT) {
    err = hwif->Get8(IDE_ERROR);

    MsgLog::printf("%s: %s: error=0x%02x", name, msg, err);
#ifdef FANCY_STATUS_DUMPS
    if (media == IDE::med_disk) {
      MsgLog::printf(" { ");
      if (err & BBD_ERR)	MsgLog::printf("BadSector ");
      if (err & ECC_ERR)	MsgLog::printf("UncorrectableError ");
      if (err & ID_ERR)		MsgLog::printf("SectorIdNotFound ");
      if (err & ABRT_ERR)	MsgLog::printf("DriveStatusError ");
      if (err & TRK0_ERR)	MsgLog::printf("TrackZeroNotFound ");
      if (err & MARK_ERR)	MsgLog::printf("AddrMarkNotFound ");
      MsgLog::printf("}");
      if (err & (BBD_ERR|ECC_ERR|ID_ERR|MARK_ERR)) {
	uint8_t cur = hwif->Get8(IDE_DRV_HD);
	if (cur & 0x40) {	/* using LBA? */
	  MsgLog::printf(", LBAsect=%ld", (unsigned long)
		 ((cur&0xf)<<24)
		 |(hwif->Get8(IDE_CYL_HI)<<16)
		 |(hwif->Get8(IDE_CYL_LO)<<8)
		 | hwif->Get8(IDE_SECTOR));
	} else {
	  MsgLog::printf(", CHS=%d/%d/%d",
		 (hwif->Get8(IDE_CYL_HI)<<8) + hwif->Get8(IDE_CYL_LO),
		 cur & 0xf,	// ?? WHY the limit ??
		 hwif->Get8(IDE_SECTOR));
	}
	if (req)
	  MsgLog::printf(", sector=%ld", req->req_start);
      }
    }
#endif	/* FANCY_STATUS_DUMPS */
    MsgLog::printf("\n");
  }
  return err;
}

/*
 * Error() takes action based on the error returned by the controller.
 */
void
ide_drive::Error(const char *msg, uint8_t stat)
{
  Request *req = rq.GetNextRequest();
  uint8_t err;

  err = DumpStatus(msg, stat, req);
  if (req  == NULL)
    return;

  /* retry only "normal" I/O: */
  if (req->cmd == IoCmd::NUM_IOCMD) {
    req->nError = 1;
    EndDriveCmd(stat, err);
    return;
  }

  if (stat & BUSY_STAT) {		/* other bits are useless when BUSY */
    req->nError |= ERROR_RESET;
  }
  else {
    if (media == IDE::med_disk && (stat & ERR_STAT)) {
      /* err has different meaning on cdrom and tape */
      if (err & (BBD_ERR | ECC_ERR))	/* retries won't help these */
	req->nError = ERROR_MAX;
      else if (err & TRK0_ERR)	/* help it find track zero */
	req->nError |= ERROR_RECAL;
    }
    if ((stat & DRQ_STAT) && req->cmd != IoCmd::Write)
      FlushResidue();
  }

  uint8_t status = hwif->Get8(IDE_STATUS);

  if (status & (BUSY_STAT|DRQ_STAT))
    req->nError |= ERROR_RESET;	/* Mmmm.. timing problem */

  if (req->nError >= ERROR_MAX) {
#ifdef CONFIG_BLK_DEV_IDETAPE
    if (drive->media == ide_tape) {
      req->nError = 0;
      idetape_end_request(0, HWGROUP(drive));
    }
    else
#endif /* CONFIG_BLK_DEV_IDETAPE */
      // Terminate the request:
      req->Terminate();
  }
  else {
    if ((req->nError & ERROR_RESET) == ERROR_RESET) {
      ++req->nError;
      hwif->DoReset(ndx);
      return;
    }
    else if ((req->nError & ERROR_RECAL) == ERROR_RECAL)
      flags.b.recal = 1;
    ++req->nError;
  }

  MsgLog::dprintf(true, "A drive error occurred on drive %s\n",
		  name);
}

// FlushResidue() is invoked in response to a drive unexpectedly
// having its DRQ_STAT bit set.  As an alternative to resetting the
// drive, this routine tries to clear the condition by read a sector's
// worth of data from the drive.  Of course, this may not help if the
// drive is *waiting* for data from *us*.  */
void
ide_drive::FlushResidue()
{
  uint32_t i = (multCount ? multCount : 1) * EROS_SECTOR_SIZE / 2; // 16-bit xfers

  while (i > 0) {
    unsigned long buffer[16];
    unsigned int wcount = (i > 16) ? 16 : i;
    i -= wcount;
    InputData(buffer, wcount);
  }
}

/*
 * do_special() is used to issue WIN_SPECIFY, WIN_RESTORE, and WIN_SETMULT
 * commands to a drive.  It used to do much more, but has been scaled back.
 */
void
ide_drive::HandleDriveFlags()
{
#if 0
next:
#endif

  MsgLog::printf("%s: do_special: 0x%02x\n", name, flags.all);

  if (flags.b.setGeom) {
    flags.b.setGeom = 0;
    
    if (media == IDE::med_disk) {
      hwif->Put8(IDE_SECTOR, l_chs.sec);
      hwif->Put8(IDE_CYL_LO, l_chs.cyl);
      hwif->Put8(IDE_CYL_HI, l_chs.cyl >> 8);
      hwif->Put8(IDE_DRV_HD, (l_chs.hd - 1 | select.all) & 0xBFu);

      if (!IS_PROMISE_DRIVE)
	hwif->DoCmd(ndx, WIN_SPECIFY, l_chs.sec, &ide_hwif::SetGeometryIntr);
    }
  }
  else if (flags.b.recal) {
    flags.b.recal = 0;
    if (media == IDE::med_disk && !IS_PROMISE_DRIVE)
      hwif->DoCmd(ndx, WIN_RESTORE, l_chs.sec, &ide_hwif::RecalibrateIntr);
  }
#if 0
  else if (flags.b.set_pio) {
    ide_tuneproc_t *tuneproc = HWIF(drive)->tuneproc;
    flags.b.set_pio = 0;
    if (tuneproc != NULL)
      tuneproc(drive, drive->pio_req);
    goto next;
  }
#endif
  else if (flags.b.setMultMode) {
    flags.b.setMultMode = 0;
    if (media == IDE::med_disk) {
      if (id && multReq > id->max_multsect)
	multReq = id->max_multsect;
      MsgLog::printf("ship a WIN_SETMULT command multReq %d max %d\n",
		     multReq, id->max_multsect);
      if (!IS_PROMISE_DRIVE)
	hwif->DoCmd(ndx, WIN_SETMULT, multReq, &ide_hwif::SetMultmodeIntr);
    }
    else
      multReq = 0;
  }
  else if (flags.all) {
    uint8_t old_flags = flags.all;
    flags.all = 0;
    MsgLog::printf("%s: bad special flag: 0x%02x\n", name, old_flags);
  }
}

// Busy wait for the drive to return the expected status, but not for
// longer than timelimit.  Return true if there was an error.
bool
ide_drive::WaitStatus(uint8_t good, uint8_t bad, uint32_t timelimit)
{
  timelimit = Machine::MillisecondsToTicks(timelimit);
  timelimit += SysTimer::Now();
  
  do {
    // The IDE spec allows the drive 400ns to get it's act together,
    // This is incredibly stupid, as it guarantees at least a 1ms
    // delay whenever the status needs to be checked.  Common case is
    // that we will do the loop only once.
    //
    // Note that this code needs to be redesigned, because the busy
    // wait loop has very bad implications for real-time.
    
#ifdef VERBOSE_BUG
    uint32_t f = GetFlags();
    MsgLog::printf("Before SpinWait: flags 0x%08x timer %s enabled.\n",
		   f,
		   IRQ::IsEnabled(0) ? "is" : "is not");
#endif

    Machine::SpinWaitUs(1);

    uint8_t status = hwif->Get8(IDE_STATUS);
  
    if ( OK_STAT(status,good, bad) )
      return false;

    // If the drive no longer purports to be busy, life is bad:
    if ( (status & BUSY_STAT) == 0 ) {
      Error("status error", status);
      return true;
    }
  } while( timelimit < SysTimer::Now() );

  uint8_t status = hwif->Get8(IDE_STATUS);
  Error("status timeout", status);
  return true;
}

// This routine will not be called unless there is really a request to
// process.
void
ide_drive::DoRequest(Request * req)
{
#if defined(IDE_DEBUG)
  MsgLog::printf("selecting drive %d\n", ndx);
#endif
  
  hwif->SelectDrive(ndx);
  // This causes infinite loop for some reason:
  //  Machine::SpinWaitMs(DRIVE_SELECT_DELAY);

  if ( WaitStatus(READY_STAT, BUSY_STAT|DRQ_STAT, WAIT_READY) ) {
    MsgLog::fatal("Drive not ready\n");
    return;
  }
  
  if (flags.all) {
    HandleDriveFlags();
#if defined(IDE_DEBUG)
    MsgLog::dprintf(false, "Handling driver flags\n");
#endif
    return;
  }

  if ( req->cmd >= IoCmd::NUM_IOCMD )
    DoDriveCommand(req);
  else {
    switch (media) {
    case IDE::med_disk:
      DoDiskRequest(req);
      break;
    default:
      MsgLog::fatal("Unimplemented media!\n");
    }
  }
}
