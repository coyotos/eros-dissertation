/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

struct ide_hwif;
struct ide_driveid;


struct ide_drive {
  friend class ide_hwif;
  
  union {
    uint8_t        all;

    struct {
      uint8_t setGeom : 1;		// issue drive geometry specification
      uint8_t recal : 1;		// seek to cyl 0
      uint8_t setMultMode : 1;	// set multmode count
#if 0
      uint8_t pio : 1;		// set pio mode
#endif
    } b;
  }             flags;

  RequestQueue  rq;		// request queue.

  ide_driveid   *id;		// vendor id info
  bool	        present;	// unit is present
  bool		removable;	// unit is removable
  bool	        needsMount;	// unit wants to be mounted
  bool	        mounted;	// unit is mounted
  bool	        use_dma;	// DMA (if available) can be used

  bool		active;		// an operation is in progress on this
				// drive.
  ide_hwif      *hwif;
  
  uint32_t	multCount;	// max sectors per xfer
  uint32_t	multReq;	// requested mult sector setting
  IDE::media	media;		// cdrom, disk, tape
  // bool		io_32bit
#if 0
  CHS		p_chs;		// physical geometry
#endif
  CHS		l_chs;		// logical geometry
  CHS		b_chs;		// BIOS geometry

  union {
    uint8_t all;
    struct {
      uint8_t head : 4;
      uint8_t unit : 1;
      uint8_t : 1;			// always 1
      uint8_t lba : 1;		// using LBA, not CHS
      uint8_t : 1;			// always 1
    } b;
  }             select;

  uint8_t		ctl;		// control register value
  uint32_t          ndx;

  char          name[5];
protected:
  void FlushResidue();

  uint8_t DumpStatus (const char *msg, uint8_t stat, Request* req);

  void MultWrite(Request *req);
  void OutputData(void *buffer, unsigned int wcount);
  void InputData(void *buffer, unsigned int wcount);
  void AtapiIdentify(uint8_t);
  uint8_t Identify(uint8_t);

  void HandleDriveFlags();

  void Error(const char*, uint8_t);

  void EndDriveCmd(uint8_t stat, uint8_t err);
  void DoDriveCommand(Request*);
  void DoDiskRequest(Request*);

  bool WaitStatus(uint8_t good, uint8_t bad, uint32_t timelimit);
  
public:
  void DoRequest(Request*);

  uint32_t DoProbe(uint8_t cmd);
  void Probe();
  uint32_t Capacity();

  ide_drive();
};
