/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/IoRegion.hxx>
#include <kerninc/IntAction.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/BlockDev.hxx>
#include <kerninc/IoRequest.hxx>
#include <eros/Device.h>
#include <arch/i486/kernel/SysConfig.hxx>
#include <arch/i486/kernel/CMOS.hxx>

static bool Probe(AutoConf *ac);
static bool Attach(AutoConf *ac);

struct Driver ac_ide = {
  "hd", Probe, Attach
} ;

#include "io.h"

// #define IDE_DEBUG

// Driver for IDE interface devices and (also) older ST506 devices.
// According to the ATA Faq, these interfaces are assigned as follows:
// (REF: ATA Faq)
//
//  Interface   CS0-decode   CS1-decode   IRQ
//  1           0x1F0-0x1F7  0x3F6-0x3F7  14
//  2           0x170-0x177  0x376-0x377  15 or 10 -- usually 15
//  3           0x1E8-0x1EF  0x3EE-0x3EF  12 or 11
//  4           0x168-0x16F  0x36E-0x36F  10 or 9
//
// The current implementation does not configure interfaces 3 and 4,
// because there is no real standard for port assignments on
// interfaces 3 and 4, and I am reluctant to deploy what I cannot
// test.

const uint16_t ide_cs0[] = { 0x1F0, 0x170, 0x1E8, 0x168 };
const uint16_t ide_irq[] = { 14, 15, 12, 10 };

#include "ide_ide.hxx"

const char *ide_name[MAX_HWIF] = { "ide0", "ide1" };

#include "ide_drive.hxx"
#include "ide_hwif.hxx"
#include "ide_group.hxx"
	      
#define BIOS_HD_BASE	 0x1F0

#define OK_TO_RESET_CONTROLLER 0

// In theory, these should not be statically allocated, but it
// complexifies things to do otherwise, and they don't really take up
// all that much space.

ide_group group_tbl[MAX_HWIF];
ide_hwif  hwif_tbl[MAX_HWIF];

ide_hwif::ide_hwif()
{
  uint32_t hwifno = this - hwif_tbl;
  
  ndx = hwifno;
  irq = 0;		// probe for this
  ioBase = ide_cs0[hwifno];
  //  ctlBase = ide_cs1[hwifno];
  chipset = IDE::cs_unknown;
  noprobe = (hwifno > 1) ? true : false;
  present = false;

  // Second unit may be present even if first unit isn't, so...
  nUnits = MAX_DRIVE;
  devClass = DEV_DISK_IDE;
  name = "ide";
  
  group = &group_tbl[hwifno];
  group->hwif[0] = this;

#if 0
  // Given the number of bone-headed IDE controller chips out there,
  // and the fact that more are being discovered daily, proceed on the
  // assumption that the IDE interface chip is brain damaged until
  // proven otherwise.
  
  serialized = true;
#endif

  int_handler = 0;
  dma_handler = 0;
  
  for (int drv = 0; drv < MAX_DRIVE; drv++) {
    drive[drv].ndx = drv;
    drive[drv].hwif = this;
    drive[drv].select.all = (drv << 4) | 0xa0;
    drive[drv].name[0] = 'h';
    drive[drv].name[1] = 'd';
    drive[drv].name[2] = 'a' + hwifno;
    drive[drv].name[3] = '0' + drv;
    drive[drv].name[4] = 0;
  }
}

void
ide_hwif::SetHandler(uint8_t unit, bool (ide_hwif::*handler)())
{
  assert (unit == cur_drive);
  assert(int_handler == 0);
  
  int_handler = handler;

#ifdef IDE_DEBUG
  MsgLog::printf("Set int handler to 0x%08x\n", &handler);
#endif
}


// Issue a simple drive command
void
ide_hwif::DoCmd(uint8_t unit, uint8_t cmd, uint8_t nsec, bool (ide_hwif::*handler)())
{
  SetHandler(unit, handler);
  Put8(IDE_CTL_ALTSTATUS, drive[unit].ctl);
  Put8(IDE_NSECS, nsec);
  Put8(IDE_CMD, cmd);
}


/*
 * ide_do_reset() is the entry point to the drive/interface reset code.
 */
void
ide_hwif::DoReset(uint8_t unit)
{
  DoReset1(unit, false);
#ifdef CONFIG_BLK_DEV_IDETAPE
  if (drive->media == ide_tape)
    drive->tape.reset_issued=1;
#endif /* CONFIG_BLK_DEV_IDETAPE */
}


/*
 * do_reset1() attempts to recover a confused drive by resetting it.
 * Unfortunately, resetting a disk drive actually resets all devices on
 * the same interface, so it can really be thought of as resetting the
 * interface rather than resetting the drive.
 *
 * ATAPI devices have their own reset mechanism which allows them to be
 * individually reset without clobbering other devices on the same interface.
 *
 * Unfortunately, the IDE interface does not generate an interrupt to let
 * us know when the reset operation has finished, so we must poll for this.
 * Equally poor, though, is the fact that this may a very long time to complete,
 * (up to 30 seconds worstcase).  So, instead of busy-waiting here for it,
 * we set a timer to poll at 50ms intervals.
 */
void
ide_hwif::DoReset1(uint8_t unit, bool /* suppressAtapi */)
{
#ifdef CONFIG_BLK_DEV_IDEATAPI
  // Interrupts may need to be disabled here...
  
  /* For an ATAPI device, first try an ATAPI SRST. */
  if (drive[unit].media != IDE::med_disk) {
    MsgLog::fatal("Reset1 on non-disk not implemented\n");
    
    if (!suppressAtapi) {
      if (!keep_settings)
	unmask = 0;
      SelectDrive(unit);
      Machine::SpinWait(20);
      Put8(IDE_CMD, WIN_SRST);
#if 0
      hwgroup->poll_timeout = jiffies + WAIT_WORSTCASE;
      ide_set_handler (drive, &atapi_reset_pollfunc, HZ/20);
#endif
      return;
    }
  }
#endif /* CONFIG_BLK_DEV_IDEATAPI */

  /*
   * First, reset any device state data we were maintaining
   * for any of the drives on this interface.
   */
  for (unit = 0; unit < MAX_DRIVE; ++unit) {
    ide_drive *pUnit = &drive[unit];
    pUnit->flags.all = 0;
    pUnit->flags.b.setGeom = 1;
    pUnit->flags.b.recal  = 1;

    if (OK_TO_RESET_CONTROLLER)
      pUnit->multCount = 0;

#if 0
    if (!pUnit->keep_settings) {
      pUnit->mult_req = 0;
      pUnit->unmask = 0;
      if (pUnit->using_dma) {
	pUnit->using_dma = 0;
	MsgLog::printf("%s: disabled DMA\n", pUnit->name);
      }
    }
#endif

#if 0
    if (pUnit->mult_req != pUnit->mult_count)
      pUnit->special.b.set_multmode = 1;
#endif
  }

#if OK_TO_RESET_CONTROLLER
  /*
   * Note that we also set nIEN while resetting the device,
   * to mask unwanted interrupts from the interface during the reset.
   * However, due to the design of PC hardware, this will cause an
   * immediate interrupt due to the edge transition it produces.
   * This single interrupt gives us a "fast poll" for drives that
   * recover from reset very quickly, saving us the first 50ms wait time.
   */
  OUT_BYTE(drive->ctl|6,IDE_CONTROL_REG);	/* set SRST and nIEN */
  udelay(5);			/* more than enough time */
  OUT_BYTE(drive->ctl|2,IDE_CONTROL_REG);	/* clear SRST, leave nIEN */
  hwgroup->poll_timeout = jiffies + WAIT_WORSTCASE;
  ide_set_handler (drive, &reset_pollfunc, HZ/20);
#endif	/* OK_TO_RESET_CONTROLLER */
}

void
ide_hwif::SelectDrive(uint32_t unit)
{
  Put8(IDE_DRV_HD, drive[unit].select.all);
}


//
// FUNCTIONS FOR THE EROS BLOCK DEVICE INTERFACE
//

void
ide_hwif::MountUnit(uint8_t unit)
{
  assert (unit < MAX_DRIVE);
  assert (drive[unit].present);
  assert (drive[unit].needsMount);

  assert (drive[unit].mounted == false);

  if (drive[unit].removable) {
    MsgLog::fatal("Removable unit locking not yet implemented\n");
  }
  
  drive[unit].mounted = true;
  drive[unit].needsMount = false;
  totalMountedUnits++;
}

void
ide_hwif::GetUnitInfo(uint8_t unit, BlockUnitInfo& ui)
{
  assert (unit < MAX_DRIVE);

  if (drive[unit].present == false) {
    ui.isDisk = false;
  }
  else {
    ui.b_geom = drive[unit].b_chs;
    ui.d_geom = drive[unit].l_chs;
    ui.nSecs = drive[unit].Capacity();
    ui.isEros = true;		// if any partition might be EROS
    ui.isDisk = (drive[unit].media == IDE::med_disk) ? true : false;
    ui.isMounted = drive[unit].mounted;
    ui.needsMount = drive[unit].needsMount;
    ui.hasPartitions = true;
    ui.isBoot = false;
    
    // FIX: If we're not on a DOS machine....
    
    if (ioBase == BIOS_HD_BASE && ((unit | 0x80) == unit))
      ui.isBoot = true;
  }
}

void
ide_hwif::InsertRequest(Request *req)
{
#if 0
  MsgLog::printf("Request ior=0x%08x inserted on hwif=0x%08x\n",
		 req, this);
#endif
  
  assert (req->unit < MAX_DRIVE);
  assert (drive[req->unit].mounted == true);
  drive[req->unit].rq.InsertRequest(req);
  StartIO();
}

// INTERRUPT HANDLERS:
bool
ide_hwif::SetMultmodeIntr()
{
  uint8_t status = Get8(IDE_STATUS);
  if (OK_STAT(status, READY_STAT, BAD_STAT)) {
    drive[cur_drive].multCount = drive[cur_drive].multReq;
  }
  else {
    drive[cur_drive].multCount = 0;
    drive[cur_drive].multReq = 0;
    drive[cur_drive].flags.b.recal = 1;
    drive[cur_drive].DumpStatus("Multmode interrupt", status, 0);
  }
  

#ifdef IDE_DEBUG
  MsgLog::printf("SetMultMode: xfer in %d sec blocks\n",
		 drive[cur_drive].multCount);
#endif

  return true;
}

bool
ide_hwif::RecalibrateIntr()
{
  uint8_t status = Get8(IDE_STATUS);

  if (!OK_STAT(status,READY_STAT,BAD_STAT))
    drive[cur_drive].Error("recalibrate interrupt", status);

#ifdef IDE_DEBUG
  MsgLog::printf("RecalIntr(): Set int handler to 0\n");
#endif
  int_handler = 0;

  return true;
}

bool
ide_hwif::SetGeometryIntr()
{
  uint8_t status = Get8(IDE_STATUS);

  if (!OK_STAT(status,READY_STAT,BAD_STAT))
    drive[cur_drive].Error("set geometry intr", status);

#ifdef IDE_DEBUG
  MsgLog::printf("SetGeomIntr(): Set int handler to 0\n");
#endif
  int_handler = 0;
  
  return true;
}

// When using IDE drives, we always issue a request for the total
// desired number of sectors.  If the drive supports READ MULTIPLE we
// use it to reduce the number of generated interrupts, else we live
// with taking one interrupt per sector.
bool
ide_hwif::ReadIntr()
{
  uint8_t status = Get8(IDE_STATUS);

  if (!OK_STAT(status,DATA_READY,BAD_R_STAT)) {
    drive[cur_drive].Error("read intr", status);
    return true;
  }

  uint32_t max_xfer = drive[cur_drive].multCount;

  if (max_xfer == 0) max_xfer = 1;
  
  // What follows is really the same code as in MultWrite, and it
  // isn't clear to me why it is replicated here.
  
  Request *req = group->curReq;
  
  uint32_t nsec = req->nsec;

  if (nsec > max_xfer)
    nsec = max_xfer;
  
#ifdef IDE_DEBUG
  MsgLog::printf("Transfer was 0x%08x: %d sectors at %d nsec %d\n",
		 req->req_ioaddr, req->req_nsec, req->req_start, req->nsec);

  MsgLog::printf("Transferring %d sectors of data\n", nsec);
#endif
  
  drive[cur_drive].InputData((void*) req->req_ioaddr, nsec *
			     EROS_SECTOR_SIZE >> 2);
  
  req->req_start += nsec;
  req->req_ioaddr += (EROS_SECTOR_SIZE * nsec);
  req->req_nsec -= nsec;
  req->nError = 0;
  req->nsec -= nsec;

#ifdef IDE_DEBUG
  MsgLog::printf("Rd Residual at 0x%08x: %d sectors at %d nsec %d\n",
		 req->req_ioaddr, req->req_nsec, req->req_start,
		 req->nsec); 
#endif

  return (req->req_nsec) ? false : true;
}

bool
ide_hwif::WriteIntr()
{
  uint8_t status = Get8(IDE_STATUS);

  if (!OK_STAT(status,DRIVE_READY,BAD_W_STAT)) {
    drive[cur_drive].Error("write intr", status);
    return true;
  }

  Request *req = group->curReq;
  
  uint32_t nsec = req->nsec;
  
  req->req_start += nsec;
  req->req_ioaddr += (EROS_SECTOR_SIZE * nsec);
  req->req_nsec -= nsec;
  req->nError = 0;
  req->nsec -= nsec;

#ifdef IDE_DEBUG
  MsgLog::printf("Wr Residual at 0x%08x: %d sectors at %d nsec %d\n",
		 req->req_ioaddr, req->req_nsec, req->req_start,
		 req->nsec); 
#endif

  // If the drive is ready and there is more to do, ship off the next
  // sector, else we will have to reschedule it later.
  if (req->req_nsec && (status & DRQ_STAT)) {
    assert( status & DRQ_STAT );

    drive[cur_drive].OutputData((void*) req->req_ioaddr,
				EROS_SECTOR_SIZE >> 2);

    return false;		// let the interrupt handler pointer stand
  }

  return true;
}

// This interrupt hits when the disk is ready for the next multisector
// block write.  If we get the block size we want (4096), we won't
// ever be writing more than 4096 bytes, but conceivably a
// user-initiated raw disk I/O might, so we need to be prepared for
// that case.
bool
ide_hwif::MultWriteIntr()
{
  uint8_t status = Get8(IDE_STATUS);

  if (!OK_STAT(status,DRIVE_READY,BAD_W_STAT)) {
    drive[cur_drive].Error("multwrite intr", status);
    return true;
  }

  Request *req = group->curReq;
  
  uint32_t nsec = req->nsec;
  
  req->req_start += nsec;
  req->req_ioaddr += (EROS_SECTOR_SIZE * nsec);
  req->req_nsec -= nsec;
  req->nError = 0;
  req->nsec -= nsec;

#ifdef IDE_DEBUG
  MsgLog::printf("MWr Residual at 0x%08x: %d sectors at %d nsec %d\n",
		 req->req_ioaddr, req->req_nsec, req->req_start,
		 req->nsec); 
#endif
  
  // If the drive is ready and there is more to do, ship off the next
  // block, else we will have to reschedule it later.
  if (req->req_nsec && (status & DRQ_STAT)) {
    assert( status & DRQ_STAT );

    drive[cur_drive].MultWrite(req);

    return false;		// let the interrupt handler pointer stand
  }

  return true;
}

void
ide_hwif::OnIntr(IntAction* ia)
{
  ide_hwif *pHwif = (ide_hwif*) ia->drvr_ptr;

  if (pHwif->int_handler == 0) {
    uint8_t status = pHwif->Get8(IDE_STATUS);
    pHwif->drive[0].DumpStatus("orphaned interrupt", status, 0);
#if 0
    pHwif->drive[0].flags.b.recal = 1;
    pHwif->drive[1].flags.b.recal = 1;
    halt();
#endif
  }

  // Call pointer to member that happens to be member of pHwif
  if ( (pHwif->*(pHwif->int_handler))() ) {
#ifdef IDE_DEBUG
    MsgLog::printf("OnIntr() Set int handler to 0\n");
#endif
    pHwif->int_handler = 0;
    pHwif->next = readyChain;
    readyChain = pHwif;

    ActivateTask();
  }
}

void
ide_hwif::StartIO()
{
  group->StartIO();
}

// FIX: This needs a much more sophisticated selection policy.  Right
// now it does round-robin issuance among all of the HWIF's in a group
// to prevent starvation. On drives with write-back buffers this isn't
// a big problem, but most drives still use write-through logic, and
// on these drives the current policy results in bad rotational
// delays.  At 7200 RPM, the on-paper mean rotational delay is
// 60/(7200*2) or about 4.2 ms.  Since we write blocks sequentially,
// it is damn likely that we will get close to worst case rotational
// delay of 8ms (I need to measure this).
//
// A better policy would be to have the StartIO() routine examine the
// next request on the same drive and see if that request falls on the
// same PHYSICAL cylinder **and track** as the previous request.  If
// so, it should endeavour to issue the request eagerly.
//
// There is no point doing more than a track on a modern drive -- head
// to head delays closely approximate track to track delays, and we
// might as well switch to the next interface.
//
// Even better would be to do request merging, coalescing adjacent I/O
// operations into a single operation using chained DMA.  This would
// make maximal use of the disk cache and the rotational advantage.

void
ide_group::StartIO()
{
  assert(this);
  
  do {
    if (curReq == 0) {
      for (uint32_t i = 0; i < HWIFS_PER_GROUP; i++) {
	uint32_t which_hwif = (cur_hwif + i + 1) % HWIFS_PER_GROUP;

	if (hwif[which_hwif] == 0)
	  continue;

	curReq = hwif[which_hwif]->GetNextRequest();
	if (curReq) {
	  cur_hwif = which_hwif;
	  break;
	}
      }
    }

    if (curReq == 0)
      return;
  
#if defined(IDE_DEBUG)
    MsgLog::dprintf(false, "Start cur_hwif = 0x%08x, req=0x%08x dio=0x%08x\n",
		    hwif[cur_hwif], curReq, curReq->dio);
#endif
  
  } while ( hwif[cur_hwif]->StartRequest(curReq) );
}

Request*
ide_hwif::GetNextRequest()
{
  // The point of this madness is to cycle through the configured
  // drives exactly once, while still doing round-robin initiation.
  
  for (uint32_t i = 0; i < MAX_DRIVE; i++) {
    uint32_t unit = (cur_drive + i + 1) % MAX_DRIVE;

    if ( drive[unit].present && !drive[unit].rq.IsEmpty() ) {
      cur_drive = unit;
      return drive[unit].rq.GetNextRequest();
    }
  }

  return 0;
}

// Return true when request is completed.
bool
ide_hwif::StartRequest(Request* req)
{
  // We don't do split-seek on IDE drives, so just go ahead and commit
  // the request.
  assert(this);
  assert(req);

  if (inDuplexWait)
    return false;
  
  if ( int_handler ) {
#if defined(IDE_DEBUG)
    MsgLog::dprintf(true, "HWIF already active (handler=0x%08x)\n", &int_handler);
#endif
    return false;
  }
  
  if ( ! req->Commit(this, &drive[req->unit]) ) {
    // stalled by another controller
#if defined(IDE_DEBUG)
    MsgLog::dprintf(true, "Stalled by another controller\n");
#endif
    return false;
  }

  if ( req->IsCompleted() ) {
    RequestQueue& rq = drive[req->unit].rq;
    
    assert (req == group->curReq);
    req->Finish();
    assert ( rq.ContainsRequest(req) );
    rq.RemoveRequest(req);
    group->curReq = 0;

#if defined(IDE_DEBUG)
    if (req->cmd == IoCmd::Plug)
      MsgLog::printf("IDE%d: plug passes\n", req->unit);
    MsgLog::dprintf(false, "Finish current request. More? %c\n",
		    rq.IsEmpty() ? 'n' : 'y');
#endif
    return true;
  }
  
#ifdef IDE_DEBUG
  MsgLog::dprintf(false, "Calling DoRequest w/ req=0x%08x...\n", req);
#endif
  drive[req->unit].DoRequest(req);
  return false;
}

void
ide_hwif::Probe()
{
  if (noprobe) {
    MsgLog::printf("Skipping probe on IDE interface %d\n", ndx);
    return;
  }

  if (ioBase == BIOS_HD_BASE) {
    // Check the CMOS configuration settings.  If a CMOS configuration
    // is found, then this is either BIOS drive 0x80 or 0x81, and the
    // interface is register compatible with the prehistoric register
    // interface.  This implies that the drive in question is NOT, for
    // example, a SCSI drive.
    //
    // FIX: On newer BIOS, CMOS appears to store info for four drives.
    // How to get it if present?

    uint8_t cmos_hds = CMOS::cmosByte(0x12);
    if (cmos_hds)
      present = true;		// HW interface present
    
    uint8_t cmos_hd0 = (cmos_hds >> 4) & 0xfu;
    uint8_t cmos_hd1 = cmos_hds & 0xfu;

    // If the drives are listed in the CMOS, extract their geometry
    // from the BIOS tables.
    if (cmos_hd0) {
      /* 
       * form a longword representing all this gunk:
       *       6 bit zero
       *	10 bit cylinder
       *	 8 bit head
       *	 8 bit sector
       */

      drive[0].b_chs.cyl = SysConfig.driveGeom[0].cyls;
      drive[0].b_chs.hd = SysConfig.driveGeom[0].heads;
      drive[0].b_chs.sec = SysConfig.driveGeom[0].secs;

      drive[0].l_chs = drive[0].b_chs;

      MsgLog::printf("ide0: c/h/s=%d/%d/%d\n",
		     drive[0].b_chs.cyl,
		     drive[0].b_chs.hd,
		     drive[0].b_chs.sec);
      
      drive[0].present = true;
    }
    if (cmos_hd1) {
      drive[1].b_chs.cyl = SysConfig.driveGeom[1].cyls;
      drive[1].b_chs.hd = SysConfig.driveGeom[1].heads;
      drive[1].b_chs.sec = SysConfig.driveGeom[1].secs;

      drive[1].l_chs = drive[1].b_chs;
      MsgLog::printf("ide1: c/h/s=%d/%d/%d\n",
		     drive[1].b_chs.cyl,
		     drive[1].b_chs.hd,
		     drive[1].b_chs.sec);
      
      drive[1].present = true;
    }
  }

  if (IoRegion::IsAvailable(ioBase, 8) == false ||
      IoRegion::IsAvailable(ioBase + IDE_CTL_ALTSTATUS, 2) == false) {
    for (uint32_t unit = 0; unit < MAX_DRIVE; unit++) {
      if (drive[unit].present)
	MsgLog::fatal("IDE: hwif %d drive %d present but ports in use\n",
		      ndx, unit);
    }
    MsgLog::printf("IDE hwif %d ports in use - skipping probe\n",
		   ndx);
    present = false;
    return;
  }

  for (uint32_t unit = 0; unit < MAX_DRIVE; unit++) {
    drive[unit].Probe();
    if (drive[unit].present && !present) {
      present = true;
      IoRegion::Allocate(ioBase, 8, "ide");
      IoRegion::Allocate(ioBase + IDE_CTL_ALTSTATUS, 2, "ide");
    }
  }

  if (!present)
    return;

  if (!irq)
    irq = ide_irq[ndx];

  MsgLog::printf("Registering IDE driver (this=0x%08x)...\n", this);

  Register();

  if (!irq) {
    MsgLog::printf("ide%d disabled: no IRQ\n", ndx);
    // FIX: should unregister
    return;
  }

  {
    uint8_t result=Get8(IDE_CTL_ALTSTATUS);
    MsgLog::printf("Before int register. Alt status : 0x%02x\n", result);
  }

  IntAction *ia = new IntAction(irq, ide_name[ndx], ide_hwif::OnIntr);
  ia->drvr_ptr = this;
  
  IRQ::WireExclusive(ia);

  MsgLog::printf("Check pending interrupt: IRQ %d\n", irq);

  IRQ::Enable(irq);
#if 0
  MsgLog::printf("Intentional halt...\n");
  halt();
#endif
}


static bool
Probe(struct AutoConf* /* ac */)
{
  IDE::InitChipsets();

  for (int ctrlr = 0; ctrlr < MAX_HWIF; ctrlr++)
    hwif_tbl[ctrlr].Probe();

#if 0
  //assume first drive is a 1.44
  TheHardDiskCtrlr.unit[0]->mediaInfo  = &MediaParams[4];

  for(int i= 1; i <=HardDiskCtrlr::NUNITS;i++)
    TheHardDiskCtrlr.unit[i] = 0; //assume the other drives do
				//not exist
  
  TheHardDiskCtrlr.Wakeup();
#endif

  return true;
}

static bool
Attach(struct AutoConf* /* ac */)
{
  return true;
}

