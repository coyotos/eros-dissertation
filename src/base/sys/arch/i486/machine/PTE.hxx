#ifndef __PTE_HXX__
#define __PTE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __U
#define __U(x) (x##u)
#endif

#define PTE_DV	 __U(0x001)	/* valid for data (Intel: 'present') */
#define PTE_W    __U(0x002)	/* writable */
#define PTE_USER __U(0x004)	/* user-accessable page */
#define PTE_WT   __U(0x008)	/* write through */
#define PTE_CD   __U(0x010)	/* cache disable */
#define PTE_ACC  __U(0x020)	/* accessed */
#define PTE_DRTY __U(0x040)	/* dirty */
#define PTE_PGSZ __U(0x080)	/* large page  (PDE, >=Pentium only) */
#define PTE_GLBL __U(0x100)	/* global page (PDE,PTE, >= PPro only) */
#define PTE_CV   __U(0x200)	/* capability valid (Intel: sw use) */
#define PTE_CS   __U(0x400)	/* capability strong (!weaken)
				   (Intel: sw use) */
#define PTE_SW2  __U(0x800)	/* SW use */

#define PTE_FRAMEBITS __U(0xfffff000)
#define PTE_INFOBITS  __U(0x00000fff)

#ifndef __ASSEMBLER__

#define PTE_IS(pte, flg)     (((pte).w_value & (flg)) == (flg))
#define PTE_ISNOT(pte, flg)  (((pte).w_value & (flg)) == 0)
#define PTE_SET(pte, flg)    ((pte).w_value |= (flg))
#define PTE_CLR(pte, flg)    ((pte).w_value &= ~(flg))

/* Be very careful about using PTE_CLR -- it is vitally important that
   PteZapped get set in all cases where the authority of a PTE has
   been reduced. */

extern bool PteZapped;

struct PTE {
  // Declared in IPC-vars.cxx:
  static PTE* kern_fstbuf;
  static PTE* kern_ptebuf;

  uint32_t w_value;

  void Invalidate()
    {
      w_value = 0;
      PteZapped = true;
    }

  void WriteProtect()
    {
      PTE_CLR(*this, PTE_W);
      PteZapped = true;
    }

  uint32_t PageFrame()
    {
      return (w_value & PTE_FRAMEBITS);
    }

  void operator=(uint32_t w)
    {
      assert(PteZapped);
      w_value = (w & PTE_FRAMEBITS) | (w_value & PTE_INFOBITS);
    }

  bool operator==(const PTE& other)
    {
      uint32_t result = w_value ^ other.w_value;
      result &= ~(PTE_ACC | PTE_DRTY);
      return (result == 0);
    }

  bool operator!=(const PTE& other)
    {
      uint32_t result = w_value ^ other.w_value;
      result &= ~(PTE_ACC | PTE_DRTY);
      return (result != 0);
    }

  PTE()
  {
    w_value = 0;
  }

  uint32_t AsWord()
  {
    return w_value;
  }

  bool MapsKva(kva_t kva)
  {
    uint32_t w = w_value & ~EROS_PAGE_MASK;;
    uint32_t pw = (VTOP(kva) & ~EROS_PAGE_MASK);
    if (w == pw)
      return true;
    return false;    
  }

  bool CanMergeWith(PTE *pte)
  {
    uint32_t w = ((uint32_t) this) ^ ((uint32_t) pte);
    if (w & ~EROS_PAGE_MASK)
      return false;
    return true;
  }

  static void ZapMappingPage(kva_t pva);

#ifdef DDB
  // Following lives in PageFault.cxx:
  void ddb_dump();
#endif
  
#ifndef NDEBUG
  // Following lives in PageFault.cxx:
  static bool ObIsNotWritable(struct ObjectHeader *pObj);
#endif
};

#define NPTE_PER_PAGE (EROS_PAGE_SIZE / sizeof (PTE))

#if 0
inline void PTE::WriteDisableMappingPage(kva_t pva)
{
  PTE *pte = (PTE*) pva;
  for (uint32_t entry = 0; entry < NPTE_PER_PAGE; entry++) {
    pte->Writable = 0;
    pte->Dirty = 0;
  }
}
#endif

inline void PTE::ZapMappingPage(kva_t pva)
{
  PTE *pte = (PTE*) pva;
  for (uint32_t entry = 0; entry < NPTE_PER_PAGE; entry++)
    pte->Invalidate();
}

// Notes on EROS page map usage:
//
// Page is present if either AuxPresent or Present is set. Present is
// sometimes turned off during mapping page aging as a way of finding
// out if the mapping page is, in fact, being used.  The AuxPresent
// bit provides a mechanism to efficiently restore the Present bit in
// this case.  Setting Present without setting AuxPresent is always a
// bug.
//
// A page that in principle writable should have it's AuxWritable bit
// set to '1'. If the page is not write hazarded, the Writable bit
// will also be set to '1'.  If the page is write hazarded, the
// Writable bit will be set to 0.  The discrepancy between these bits
// is the tipoff that the issue is a hazard.
//
#endif // __ASSEMBLER__ 
#endif // __PTE_HXX__
