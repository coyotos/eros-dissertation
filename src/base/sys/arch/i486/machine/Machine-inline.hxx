/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

inline void
Machine::FlushTLB()
{
  __asm__ __volatile__("mov %%cr3,%%eax;mov %%eax,%%cr3"
		       : /* no output */
		       : /* no input */
		       : "eax");
}

extern "C" {
  extern uint32_t CpuType;
};
  
inline void
Machine::FlushTLB(klva_t lva)
{
#ifdef SUPPORT_386
  if (CpuType > 3)
    __asm__ __volatile__("invlpg  (%0)"
			 : /* no output */
			 : "r" (lva)
			 );
  else
    FlushTLB();
#else
  __asm__ __volatile__("invlpg  (%0)"
		       : /* no output */
		       : "r" (lva)
		       );
#endif
}

inline uint16_t
Machine::htonhw(uint16_t hw)
{
  uint8_t *bhw = (uint8_t *) &hw;
  uint8_t tmp = bhw[0];
  bhw[0] = bhw[1];
  bhw[1] = tmp;

  return hw;
}

inline uint16_t
Machine::ntohhw(uint16_t hw)
{
  uint8_t *bhw = (uint8_t *) &hw;
  uint8_t tmp = bhw[0];
  bhw[0] = bhw[1];
  bhw[1] = tmp;

  return hw;
}

inline uint32_t
Machine::htonw(uint32_t w)
{
  uint8_t *bw = (uint8_t *) &w;
  uint8_t tmp = bw[0];
  bw[0] = bw[3];
  bw[3] = tmp;
  tmp = bw[1];
  bw[1] = bw[2];
  bw[2] = tmp;

  return w;
}

inline uint32_t
Machine::ntohw(uint32_t w)
{
  uint8_t *bw = (uint8_t *) &w;
  uint8_t tmp = bw[0];
  bw[0] = bw[3];
  bw[3] = tmp;
  tmp = bw[1];
  bw[1] = bw[2];
  bw[2] = tmp;

  return w;
}

inline int
Machine::FindFirstZero(uint32_t w)
{
  __asm__("bsfl %1,%0"
	  :"=r" (w)
	  :"r" (~w));
  return w;
}


// On the x86, this is handled by the object cache logic.
inline void
Machine::MarkMappingsForCOW()
{
}
