/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Low-level floppy controller implementation.
// FIX - don't start the motor shutdown timer until the operation
// queue is empty!

#include <kernel/Diag.hxx>
#include "kernel.hxx"
#include "lostart.hxx"
#include <kernel/io.h>
#include "IDT.hxx"
#include "CMOS.hxx"
#include "KernThread.hxx"
#include "CoreObject.hxx"
#include "IoReq.hxx"
#include "DMA.hxx"
#include "UserMem.hxx"
#include "SysConfig.hxx"
#include "SysTimer.hxx"
#include "DiskCtrlr.hxx"
#include "DiskVolume.hxx"

// #define FDCDEBUG
// #define FDCDEBUG2
// #define PARANOID
// #define FDCMOTOR

struct FloppyInfo {
  uint16_t totSectors;
  uint8_t     nSec;
  uint8_t	   nHd;
  uint8_t	   nCyl;
  uint8_t	   doubleStep;
  
  uint8_t	   gap;
  uint8_t	   dataRate;
  uint8_t	   spec1;
  uint8_t	   fmt_gap;
  char *   name;
  uint8_t	   nextToTry;
};

// Formatting parameters for various media in various drives.
// Note that the order of these is chosen so that the first five
// entries match the drive type encodings returned by the CMOS NVRAM.

static FloppyInfo FloppyParams[] = {
    {    0, 0,0, 0,0,0x00,0x00,0x00,0x00,"None", 0 }, /* no testing */
    {  720, 9,2,40,0,0x2A,0x02,0xDF,0x50,"360k", 0 }, /* 360kB PC diskettes */
    // Following entry used to have '6' in its nextToTry slot:
    { 2400,15,2,80,0,0x1B,0x00,0xDF,0x54,"1.2M", 0 }, /* 1.2 MB AT-diskettes */
    { 1440, 9,2,80,0,0x2A,0x02,0xDF,0x50,"720K", 0 }, /* 720kB diskette */
    { 2880,18,2,80,0,0x1B,0x00,0xCF,0x6C,"1.44M", 3 }, /* 1.44MB diskette */
#if 0
    // we do not support these ancient pieces of crap.  Actually, we
    // only support 360 kb and 720 kb because it's easier than
    // adjusting what the BIOS tells us.
    
    {  720, 9,2,40,1,0x2A,0x02,0xDF,0x50,NULL, 0 }, /* 360kB in 720kB drive */
    {  720, 9,2,40,1,0x23,0x01,0xDF,0x50,NULL, 0 }, /* 360kB in 1.2MB drive */
    { 1440, 9,2,80,0,0x23,0x01,0xDF,0x50,NULL, 0 }, /* 720kB in 1.2MB drive */
#endif
};

#define NUM_UNITS 2

class FDC : public DiskCtrlr {
  // Ports on the NEC 765 controller:
  struct NECREG {
    enum {
      DOR    = 0x2,		// digital output register
      TDR    = 0x3,		// tape drive register (not on NEC 765)
      STATUS = 0x4,		// status register
      DATA   = 0x5,		// data register
      DIR    = 0x7,		// digital input register: read only
      DCR    = 0x7,		// Diskette control register: write only
    };
  };

  // bits in the DOR register:
  struct DOR {
    enum {
      Motor3  = 0x80,		// set if indicated motor is spinning
      Motor2  = 0x40,
      Motor1  = 0x20,
      Motor0  = 0x10,
      DmaGate = 0x08,		// set for most commands
      Reset   = 0x04,		// set for most commands
      DrvSel1 = 0x02,
      DrvSel0 = 0x01,
    };
  };

  struct STATUS {
    enum {
      Master    = 0x80,	// host can send if set to 1
      ReqRead   = 0x40,	// 1 indicates read
      NonDMA    = 0x20,	// set in specify command
      CmdBusy   = 0x10,	// 1 if command in progress
      Drv3Busy  = 0x08,
      Drv2Busy  = 0x04,
      Drv1Busy  = 0x02,
      Drv0Busy  = 0x01,
    };
  };

  // NEC Commands that we use:
  struct NECCMD {
    enum {
      VERSION = 0x10,
      CONFIGURE = 0x13,
      SPECIFY = 0x03,
      RECALIBRATE = 0x07,
      SENSEI = 0x08,
      SEEK = 0x0f,

      // Floppy I/O operations
      READ = 0xc6, // with MT, MFM.  0xE6 with skip deleted
      WRITE = 0xc5, // with MT, MFM
      FORMAT = 0x4d, // with MFM
    };
  };

  // Masks and values for Status Register 0:
  struct SR0 {
    enum {
      IC         = 0xC0,	// interrupt code mask
      ICnormal   = 0x00,	// normal completion
      ICabnormal = 0x40,	// failed
      ICbadcmd   = 0x80,	// bad command
      ICpolled   = 0xC0,	// interrupted by a poll operation.

      SeekEnd    = 0x20,	// Seek completed
      EquipChk   = 0x10,	// Equipment Check - 1 if recalibrate
				// did not make it to track 0 or a
				// relative seek overran track 0
      Head       = 0x04,	// Head address mask
      Head0      = 0x00,
      Head1      = 0x04,
      DrvSel     = 0x03,	// Drive select mask. Values are 0..3
    };
  };

  struct SR1 {
    enum {
      CylEnd     = 0x80,	// Hit end of cylinder
      BadCrc     = 0x20,	// CRC on data or ID failed
      Overrun    = 0x10,	// DMA service too slow
      NoData     = 0x04,	// sector may have been deleted
      WriteProt  = 0x02,	// Write Protected
      NoAddr     = 0x01,	// Missing address mark
    };
  };
  
protected:
  uint8_t dor;

  // controller state holders:
  int nresults;			// number of results from last operation
  uint8_t results[8];		// actual result values

  void Reset();
  void OutFDC(uint8_t);
  int GetResults();
  
  void ConfigureFDC();
  FloppyInfo *curParams;	// How FDC is currently programmed

  // UNIT OPERATIONS
  //
  // These are here so that only one new class needs to be defined for
  // a new controller type.  The unit and the controller are
  // incestuously related anyway.
  
  DiskUnit* units[NUM_UNITS];

  DiskUnit& GetUnit(uint8_t whichUnit);
  void StartIO(uint8_t unit);
  void StartMount(uint8_t unit);

  bool CanSchedule(uint8_t unit);
  void OnEvent(Event& event);
  void OnKeyCall();

  void OnEvent(uint8_t unit, Event& event);
  bool Probe(uint8_t unit);
  void Attach(uint8_t unit);
  void OnKeyCall(uint8_t unit);

public:
  FDC(AutoConf::ID id, io_t ioaddr);
  
  virtual void Probe();
  virtual void Attach();
};

FDC fdca(AutoConf::FDCA, (io_t) 0x3f0);

DiskUnit fdca0(AutoConf::FDCA0, &fdca, 0);
DiskUnit fdca1(AutoConf::FDCA1, &fdca, 1);
DiskUnit fdca2(AutoConf::FDCA2, &fdca, 2);
DiskUnit fdca3(AutoConf::FDCA3, &fdca, 3);

#if 0
// G++ is broken - it doesn't support this.
DiskUnit* FDCAunits[4] = {
  { AutoConf::FDCA0, fdca, 0 },
  { AutoConf::FDCA1, fdca, 1 },
  { AutoConf::FDCA2, fdca, 2 },
  { AutoConf::FDCA3, fdca, 3 }
};
#endif


FDC::FDC(AutoConf::ID id, io_t ioaddr)
: DiskCtrlr(id, NUM_UNITS, ioaddr, (kva_t)0, McMem1M)
{
  int i;
  dor = 0;
  
  if (id == AutoConf::FDCA) {
    units[0] = &fdca0;
    units[1] = &fdca1;
#if (NUM_UNITS > 2)
    units[2] = &fdca2;
#endif
#if (NUM_UNITS > 3)
    units[3] = &fdca3;
#endif
  }
}

void
FDC::Probe()
{
  present = CMOS::HaveFDC();
}

void
FDC::Attach()
{
  IDT::RegisterHandler(IntFloppy, this);

  Reset();
}

bool
FDC::Probe(uint8_t unit)
{
  DiskUnit& du = GetUnit(unit);

  uint32_t type = CMOS::fdType(unit);

  du.type = type;

  if (du.type) {
    du.isRemovable = true;
    du.bootAttach = (unit == SysConfig.boot.drive) ? true : false;
    du.multiFormat = true;
    
    Diag::printf("%s: %s\n", du.Name(), FloppyParams[type].name);
  }

  return (du.type != 0);
}

void
FDC::Attach(uint8_t unit)
{
  // FIX: this needs to change!
  if (present)
    attached = true;
}

void
FDC::Reset()
{
  dor = 0;			// Reset FDC, Motors off
  
  outb(NECREG::DOR, dor);
  Stall(100);
  
  state = CsResetWait;

  dor = DOR::Reset|DOR::DmaGate; // re-enable the part
  outb(ioBase + NECREG::DOR, dor);

  while (state == CsResetWait)
    Stall(100);

  assert(state == CsActive);
}

void
FDC::OnEvent(Event& e)
{
  switch(state) {
  case CsActive:
    OnEvent(curUnit, e);
    return;
  case CsResetWait:
    {
      assert(e.kind == Event::Interrupt);
  
      curParams = 0;
      curUnit = 0;

      // Controller comes back in polling mode.  Must run four SENSEI
      // operations to clear this mode.
    
      int i;
  
      for (i = 0; i < NUM_UNITS; i++) {
	DiskUnit& du = GetUnit(i);

	// sense the drive status
	OutFDC(NECCMD::SENSEI);

	if (GetResults() != 2)
	  Diag::fatal(0,"Improper results from FDC reset\n");

	// The unit is now spun down.
	du.ioState = IosSpunDown;
      }
  
      state = CsActive;
  
      KernThread::SetReady(Thread::DiskDaemon);
  
      return;
    }
  }
}

void
FDC::OnEvent(uint8_t unit, Event& e)
{
  DiskUnit& du = GetUnit(unit);
  
  if (e.kind == Event::Interrupt) {
    switch(du.ioState) {
    case IosRecalWait:
      du.ioState = IosRecalDone;
      break;
    case IosSeekWait:
      du.ioState = IosSeekDone;
      break;
    case IosReadWait:
      du.ioState = IosReadDone;
      break;
    case IosWriteWait:
      du.ioState = IosWriteDone;
      break;
    case IosFormatWait:
      du.ioState = IosFormatDone;
      break;
    default:
      Diag::fatal(0, "%s: Unexpected interrupt\n", Name());
    }

    KernThread::SetReady(Thread::DiskDaemon);
  }
  if (e.kind == Event::Timer) {
    switch(du.ioState) {
    case IosSpinUpWait:
      du.ioState = IosNeedRecal;
      KernThread::SetReady(Thread::DiskDaemon);
      break;
    case IosSpinDownWait:
      dor &= ~(DOR::Motor0 << unit);
      outb(NECREG::DOR, dor);
      du.ioState = IosSpunDown;
      break;
    default:
      Diag::fatal(0, "%s: Unexpected watchdog timer\n", Name());
    }
  }
}

bool
FDC::CanSchedule(uint8_t unit)
{
  DiskUnit& du = GetUnit(unit);

  // If we have nothing to do, pass the token. Note that the effect of
  // this is that the token runs just ahead of the unit index in
  // DiskCtrlr::StartIO(). 
  if (!du.GetNextRequest()) {
    curUnit ++;
    if (curUnit == nUnits)
      curUnit -= nUnits;

    return false;
  }
  

  // If we have something to do, we can start it if we have the token
  // or if we need to start the motor.

  if (unit == curUnit)
    return true;

  if (du.ioState == DiskCtrlr::IosSpunDown)
    return true;

  return false;
}

void
FDC::OnKeyCall()
{
  Diag::fatal(0, "FDC::OnKeyCall called\n");
}

void
FDC::OnKeyCall(uint8_t unit)
{
  Diag::fatal(0, "FDC::OnKeyCall(%d) called\n", unit);
}

DiskUnit& FDC::GetUnit(uint8_t whichUnit)
{
  assert(whichUnit < NUM_UNITS);

  return *(units[whichUnit]);
}

void
FDC::OutFDC(uint8_t b)
{
#ifdef FDCIODEBUG
  Diag::printf("FDC::OutFDC(%x)\n", b);
#endif
  uint8_t r;
  for (int i = 0; i < 10000; i++) {
    r = inb(NECREG::STATUS);
    r &= (STATUS::Master | STATUS::ReqRead);
    
    if (r == STATUS::Master) {
      outb(NECREG::DATA, b);
      return;
    }
  }
  
  Diag::fatal(0,"FDC::OutFDC() failed. Status Reg = %x\n", r);
}

int
FDC::GetResults()
{
  int i, n = 0;
  
  for (i = 0 ; i < 1000 ; i++) {
    uint8_t r = inb(NECREG::STATUS);
    r &= (STATUS::Master|STATUS::ReqRead|STATUS::CmdBusy);
    
    if (r == STATUS::Master) {
	  // not busy, ready for commands
      return n;
    }
    if (r == (STATUS::Master|STATUS::ReqRead|STATUS::CmdBusy)) {
      if (n >= 8) {
	Diag::fatal(0,"FDC::GetResults(): too many answers\n");
	break;
      }
      results[n] = inb(NECREG::DATA);
#ifdef FDCIODEBUG
      Diag::printf("results[%d] = 0x%x\n", n, results[n]);
#endif
      n++;
    }
  }
  
  Diag::fatal(0,"FDC::getStatus(): couldn't get status\n");
  return -1;
}


void FDC::StartIO(uint8_t unit)
{
  DiskUnit& du = GetUnit(unit);
  
  uint8_t hd, sec, cyl;
  uint32_t secsPerCyl;
  
  assert(du.curReq);
  
  DiskIoReq* ioReq = du.curReq->ioReq;
    
#ifdef FDCDEBUG
  Diag::printf("FDC::StartIO(%d): enter. status=%d req = %x, cmd == %s)\n",
	       unit, (int) du.ioState, du.curReq, du.curReq->cmdName());
#endif
  
  if (!du.attached)
    Diag::fatal(0,"Attempt to access detached/absent unit %s\n",
		du.Name());

  for (;;) {
    // If we are actually going to do some work, configure the
    // controller appropriately:
    switch (du.ioState) {
    case IosReady:
    case IosNeedRecal:
    case IosRecalDone:
    case IosSeekDone:
      ConfigureFDC();		// set up for I/O

      // Activate the drive control pins:
      dor &= ~3;
      dor |= unit;
      outb(NECREG::DOR, dor);
      Stall(100);
    }
    
    switch (du.ioState) {
    case IosSpunDown:
    case IosSpinUpWait:
#ifdef FDCDEBUG
      Diag::printf("FDC::startCurrentOp(): still spinning up\n");
#endif
      // nothing to do at the moment
      return;
    case IosSpinDownWait:
      du.timer.Cancel();
    
    case IosReady:
      if (!du.curReq) {
	du.ioState = IosSpinDownWait;
	du.timer.SleepFor(MOTOR_SHUT_WAIT);
	return;
      }
      
    case IosNeedRecal:
    case IosRecalWait:
    case IosRecalDone:
    case IosSeekWait:
    case IosSeekDone:
    case IosReadWait:
    case IosReadDone:
    case IosWriteWait:
    case IosWriteDone:
    case IosFormatWait:
    case IosFormatDone:
    }
  }

  
  char cmdstr[9];		// command string to send
  char *cmdbyte = cmdstr;
  
  // Media is present and spinning.  If not calibrated, do so:
  if (!du.calibrated) {
#ifdef FDCDEBUG
    Diag::printf("FDC::startCurrentOp(): Recalibrate\n");
#endif
	// recall the floppy arm to track 0:
    
    lastCmd = CmdRecal;
    *cmdbyte++ = NECCMD::RECALIBRATE;
    *cmdbyte++ = unit;
    goto sendcmd;
  }
  
  // compute the head, cyl, and sec for this operation.  We need
  // to recompute this in every pass because the last operation
  // may have been a partially complete I/O operation that hit the
  // end of a cylinder boundary.
  
  secsPerCyl = curParams->nHd * curParams->nSec;
  sec = start;
  cyl = sec / secsPerCyl;
  sec -= (cyl * secsPerCyl);
  hd = sec/curParams->nSec;
  sec -= hd * curParams->nSec;
  
#ifdef FDCDEBUG2
  Diag::printf("FDC::startCurrentOp(): start %d nsec %d "
	       "[cyl %d hd %d sec %d]\n",
	       start, nsec, cyl, hd, sec);
#endif
  
  // if necessary, seek to the appropriate cylinder.  It appears
  // that if the seek does not actually move the arm, the head value
  // does not get updated, so only check the cylinder, not the head.
  if (du.curcyl != cyl ) {
#ifdef FDCDEBUG
    Diag::printf("FDC::startCurrentOp(): Seek\n");
#endif
    lastCmd = CmdSeek;
    *cmdbyte++ = NECCMD::SEEK;
    *cmdbyte++ = (hd<<2)|unit;
    *cmdbyte++ = cyl;
    goto sendcmd;
  } else {
	// Seek has completed, so any pending errors should be
	// canceled:
    nError = 0;
  }
  
#if 0
  // If the operation was merely a seek command, we should not get
  // this far before the interrupt handler declares it done.
  assert (ioReq->cmd != DiskIoReq::OpSeek);
#endif
  
  // START THE I/O OPERATION:
  
  
  // Compute number of sectors to transfer:
  secsToXfer = min(nsec, curParams->nSec - sec);
  
#ifdef FDCDEBUG
  Diag::printf("FDC::startCurrentOp(): xfer %d secs\n", secsToXfer);
#endif
  assert(iokva);
  
  assert (ioReq->cmd == IoReq::OpObjectRead ||
	  ioReq->cmd == IoReq::OpObjectWrite);
  
  {
    int iswrite = (ioReq->cmd == IoReq::OpObjectWrite);
    
    dmakva = DMA::setup(DMAC::FLOPPY, iokva, secsToXfer *
			EROS_SECTOR_SIZE, iswrite ? DMA::WRITE : DMA::READ);
    
    lastCmd = iswrite ? CmdWrite : CmdRead;
    *cmdbyte++ = iswrite ? NECCMD::WRITE : NECCMD::READ;
    *cmdbyte++ = (hd<<2)|unit;
    *cmdbyte++ = cyl;
    *cmdbyte++ = hd;
    *cmdbyte++ = sec+1;
    *cmdbyte++ = 0x2u;		// 512 byte sector size
    *cmdbyte++ = curParams->nSec; // sectors per track
    *cmdbyte++ = curParams->fmt_gap; // sectors per track
    *cmdbyte++ = 0xff;		   // DTL??
  }
  
 sendcmd:
  int count = cmdbyte - cmdstr;
  int i;
  for (i = 0; i < count; i++)
    OutFDC(cmdstr[i]);
}

#if 0
class FDC : public DiskCtrlr {

  enum {
    NUM_UNITS = 2,		// floppy controller has up to 4
				// units, but for now support only 2
  };
  
#if 0
  enum UnitCommands {
    CmdNone,
    CmdReset,
    CmdRecal,
    CmdSeek,
    CmdRead,
    CmdWrite,
  };
#endif
  
  DiskUnit units[NUM_UNITS];	
  
      // Because the floppy has removable media, the media parameters
      // can change, and may change from use to use.

  FloppyInfo *unitParams[4];	// Current media parameters
  FloppyInfo *curParams;	// How FDC is currently programmed
  
  DiskUnit& GetUnit(uint8_t whichUnit);
  void StartRequest(uint8_t unit);
  
      // COMMANDS
  
  void Reset();

      // CONTROLLER IO
  
  uint8_t dor;
  uint32_t secsToXfer;		// used in read/write operations
  
  int nresults;			// number of results from last operation
  uint8_t results[8];		// actual result values

  void OutFDC(uint8_t);
  int GetResults();
  
  void ConfigureFDC();
  

      // INTERRUPT HANDLING
  
  virtual void OnEvent(Event&);
  
  void ResetIntr(FixRegs *);
  void SeekIntr(FixRegs *);
  void RwIntr(FixRegs *);


      // MOTOR CONTROL
  
  enum {
    MOTOR_SHUT_WAIT = 10000,
    MOTOR_START_WAIT = 40,
  };
  
  virtual void Spinup(uint8_t unit);
  virtual void Spindown(uint8_t unit);

  virtual bool Attach(uint8_t unit); // return true if succeeded
  virtual void Detach(uint8_t unit);

  void OnIdle(uint8_t unit);
  
public:
  FDC(AutoConf::ID id);
  
  virtual void Probe();
  virtual void Attach();

  virtual const char* UnitName(uint8_t unit);
  uint32_t GetCylinder(uint8_t unit, uint32_t sec);
};

FDC TheFDC(AutoConf::FDCA);
static Timer timer(&TheFDC);

DiskUnit& FDC::GetUnit(uint8_t whichUnit)
{
  assert(whichUnit < NUM_UNITS);

  return units[whichUnit];
}

FDC::FDC(AutoConf::ID id)
: DiskCtrlr(id, NUM_UNITS, McMem1M)
{
  int i;
  dor = 0;
  
  for(i = 0; i < NUM_UNITS; i++) {
    units[i].unitNo = i;
    units[i].ctrlr = this;
  }
}

void
FDC::Reset()
{
#ifdef FDCDEBUG
  Diag::printf("FDC::reset(): enter\n");
#endif
  
  ctrlrStatus = CtrlrBusy;
  
  IDT::Disable();		// CRITICAL REGION
  
  lastCmd = CmdReset;
  
  dor = 0;			// Reset FDC, Motors off
  
  outb(NECREG::DOR, dor);
  Stall(100);
  
  dor = DOR::Reset|DOR::DmaGate; // re-enable the part
  outb(NECREG::DOR, dor);

  IDT::Enable();		// END CRITICAL REGION
}

void FDC::Probe()
{
  // FIX: This may or may not actually be present.  A real probe
  // routine is called for!

  present = true;

}

void FDC::Attach()
{
  int i;
  
  ctrlrStatus = CtrlrIdle;

  IDT::RegisterHandler(IntFloppy, this);
//  IDT::EnableIRQ(IRQ6);
  
      // For the moment, we do not support more than 2 floppy drives.
      // Someday I will have to fix this.
  
  for (i = 0; i < NUM_UNITS; i++) {
    uint32_t type = CMOS::fdType(i);
    DiskUnit& du = GetUnit(i);
    du.type = type;
    if (du.type) {
      du.unitState = DiskCtrlr::UsPresent;
      du.isRemovable = true;
      // Floppies only automount if they are the boot drive.
      du.bootAttach = (i == SysConfig.boot.drive) ? true : false;
      du.multiFormat = true;
    }
    
    Diag::printf("%s: %s\n", UnitName(i), FloppyParams[type].name);
  }
  
  Reset();
  
  attached = true;
  Diag::printf("FDC::init() complete\n");
}

// FIX: a lot of this strategy routine is very disk-centric.  When we
// get around to tapes we need to do something about this.

// The generic controller logic has committed us to doing something,
// and has properly initialized the request block.  Start the actual
// operation.  Controller is already marked "busy".

void FDC::StartRequest()
{
  assert(unit < NUM_UNITS);
  
  uint8_t hd, sec, cyl;
  uint32_t secsPerCyl;
  
  assert(req);
  DiskIoReq* ioReq = req->ioReq;
    
  DiskUnit& du = GetUnit(unit);
  
#ifdef FDCDEBUG
  Diag::printf("FDC::startRequest(): enter. unit=%d status=%d req = %x, cmd == %s)\n",
	       unit, (int) du.status, req, req->cmdName());
#endif
  
  if (!du.unitState < DiskCtrlr::UsAttached)
    Diag::fatal(0,"Attempt to access detached/absent unit %s\n", UnitName(unit));

#if 0
  if (!du.mediaPresent)
    Diag::printf("No media in drive %s\n", UnitName(unit));
#endif

  if (du.status == DiskUnit::UnitIdle) {
      // We are racing with the disk motor logic to cancel the
      // spindown. If we win, we end up in the ready state.  If not,
      // the motor logic will place the drive in the Offline state.
      IDT::Disable();

      if (du.spinning)
	du.status = DiskUnit::UnitActive;
      else
	Spinup(unit);

      IDT::Enable();
#ifdef FDCMOTOR
      Diag::printf("FDC::startCurrentOp(): Unit %d Spindown canceled\n",
		   unit);
#endif
  }

  // At this point, drive is either spinning up or we are ready to
  // initiate.

  // OK if unit is still spinning up.
  if (du.status == DiskUnit::UnitSpinUp) {
#ifdef FDCDEBUG
    Diag::printf("FDC::startCurrentOp(): still spinning up\n");
#endif
    return;
  }
  
  ConfigureFDC();		// set up for I/O
  
      // Activate the drive control pins:
  dor &= ~3;
  dor |= unit;
  outb(NECREG::DOR, dor);
  Stall(100);
  
  char cmdstr[9];		// command string to send
  char *cmdbyte = cmdstr;
  
  // Media is present and spinning.  If not calibrated, do so:
  if (!du.calibrated) {
#ifdef FDCDEBUG
    Diag::printf("FDC::startCurrentOp(): Recalibrate\n");
#endif
	// recall the floppy arm to track 0:
    
    lastCmd = CmdRecal;
    *cmdbyte++ = NECCMD::RECALIBRATE;
    *cmdbyte++ = unit;
    goto sendcmd;
  }
  
  // compute the head, cyl, and sec for this operation.  We need
  // to recompute this in every pass because the last operation
  // may have been a partially complete I/O operation that hit the
  // end of a cylinder boundary.
  
  secsPerCyl = curParams->nHd * curParams->nSec;
  sec = start;
  cyl = sec / secsPerCyl;
  sec -= (cyl * secsPerCyl);
  hd = sec/curParams->nSec;
  sec -= hd * curParams->nSec;
  
#ifdef FDCDEBUG2
  Diag::printf("FDC::startCurrentOp(): start %d nsec %d "
	       "[cyl %d hd %d sec %d]\n",
	       start, nsec, cyl, hd, sec);
#endif
  
  // if necessary, seek to the appropriate cylinder.  It appears
  // that if the seek does not actually move the arm, the head value
  // does not get updated, so only check the cylinder, not the head.
  if (du.curcyl != cyl ) {
#ifdef FDCDEBUG
    Diag::printf("FDC::startCurrentOp(): Seek\n");
#endif
    lastCmd = CmdSeek;
    *cmdbyte++ = NECCMD::SEEK;
    *cmdbyte++ = (hd<<2)|unit;
    *cmdbyte++ = cyl;
    goto sendcmd;
  } else {
	// Seek has completed, so any pending errors should be
	// canceled:
    nError = 0;
  }
  
#if 0
  // If the operation was merely a seek command, we should not get
  // this far before the interrupt handler declares it done.
  assert (ioReq->cmd != DiskIoReq::OpSeek);
#endif
  
  // START THE I/O OPERATION:
  
  
  // Compute number of sectors to transfer:
  secsToXfer = min(nsec, curParams->nSec - sec);
  
#ifdef FDCDEBUG
  Diag::printf("FDC::startCurrentOp(): xfer %d secs\n", secsToXfer);
#endif
  assert(iokva);
  
  assert (ioReq->cmd == IoReq::OpObjectRead ||
	  ioReq->cmd == IoReq::OpObjectWrite);
  
  {
    int iswrite = (ioReq->cmd == IoReq::OpObjectWrite);
    
    dmakva = DMA::setup(DMAC::FLOPPY, iokva, secsToXfer *
			EROS_SECTOR_SIZE, iswrite ? DMA::WRITE : DMA::READ);
    
    lastCmd = iswrite ? CmdWrite : CmdRead;
    *cmdbyte++ = iswrite ? NECCMD::WRITE : NECCMD::READ;
    *cmdbyte++ = (hd<<2)|unit;
    *cmdbyte++ = cyl;
    *cmdbyte++ = hd;
    *cmdbyte++ = sec+1;
    *cmdbyte++ = 0x2u;		// 512 byte sector size
    *cmdbyte++ = curParams->nSec; // sectors per track
    *cmdbyte++ = curParams->fmt_gap; // sectors per track
    *cmdbyte++ = 0xff;		   // DTL??
  }
  
 sendcmd:
  int count = cmdbyte - cmdstr;
  int i;
  for (i = 0; i < count; i++)
    OutFDC(cmdstr[i]);
}

void
FDC::ConfigureFDC()
{
  assert(unitParams[unit]);

  if (curParams == unitParams[unit])
    return;
  
  curParams = unitParams[unit];
  
#ifdef FDCDEBUG2
  Diag::printf("Specify drive parameters\n");
#endif
  
  OutFDC(NECCMD::SPECIFY);
  OutFDC(curParams->spec1);
  OutFDC(6);			// head unload time
  
#ifdef FDCDEBUG2
  Diag::printf("And data rate\n");
#endif
  
  outb(NECREG::DCR, curParams->dataRate & 3);
}

const char*
FDC::UnitName(uint8_t unit)
{
  static char *name = "fdXX\0";
  name[2] = ConfigTable[id].name[2];
  name[3] = '0' + unit;
  return name;
}

void
FDC::OnEvent(Event& e)
{
  if (e.kind == Event::Interrupt) {
    // Reset is not really an operation.
    if (lastCmd == CmdReset)
      ResetIntr(e.fr);
    else {
      if (!req)
	Diag::fatal(0,"Unexpected floppy interrupt\n");
  
      assert(req);
  
      switch(lastCmd) {
      case CmdReset:
	Diag::fatal(0,"Fielded FDC reset interrupt in wrong place\n");
	break;
      case CmdRecal:
      case CmdSeek:
	SeekIntr(e.fr);
	break;
      case CmdRead:
      case CmdWrite:
	RwIntr(e.fr);
	break;
      }

      // if command has completed. call the completion routine:
      if (ioReq->cmd == IoReq::OpDone) {
#ifdef FDCDEBUG
	Diag::printf("FDC::do_intr(): command complete. cte= 0x%x\n", ioReq->cte);
#endif

	ctrlrStatus = CtrlrIdle;
    
	KernThread::SetReady(ioReq->ioThread);
      }
      else
	ctrlrStatus = CtrlrMore;

      // retry the current op
      KernThread::SetReady(Thread::DiskDaemon);
    }
  }
  else {			// timer went off
#ifdef FDCMOTOR
    Diag::printf("FDC::OnTimer(): Motor timer went off\n");
#endif
  
    uint8_t u;
  
    for (u = 0; u < NUM_UNITS; u++) {
      DiskUnit& du = GetUnit(u);

      // No need to check if unit present.  If not present, cannot be
      // in spinup state.  If not present, cannot be spinning.
    
      if (du.status == DiskUnit::UnitSpinUp) {
	du.spinning = true;
	du.calibrated = false;
	du.status = DiskUnit::UnitActive;

	assert(du.unitPresent);
      
#ifdef FDCMOTOR
	Diag::printf("FDC::OnTimer(): unit %d spun up\n", u);
#endif
	ctrlrStatus = CtrlrMore;
	KernThread::SetReady(Thread::DiskDaemon);
      }
      else if (du.status == DiskUnit::UnitIdle && du.spinning) {
#ifdef FDCMOTOR
	Diag::printf("FDC::OnTimer(): unit %d spun down\n", u);
#endif
	assert(du.unitPresent);

	Spindown(u);
      }
    }

#if 0
    dor &= ~(DOR::Motor0|DOR::Motor1|DOR::Motor2|DOR::Motor3);
  
    outb(NECREG::DOR, dor);
    
    for (u = 0; u < NUM_UNITS; u++) {
      DiskUnit& du = GetUnit(u);
      du.status = DiskUnit::Offline;
    }    
#endif
  }
}

void
FDC::ResetIntr(FixRegs *fr)
{
  int i;
  
#ifdef FDCDEBUG2
  Diag::printf("FDC::reset_intr(): Run a sensei after RESET\n");
#endif

  for (i = 0; i < NUM_UNITS; i++) {
    DiskUnit& du = GetUnit(i);

    // sense the drive status
    OutFDC(NECCMD::SENSEI);
    if (GetResults() != 2)
      Diag::fatal(0,"Improper results from FDC reset\n");

    // if the unit is present, it is now offline
    if (units[i].unitPresent)
      units[i].spinning = false;

    // set floppy parameters to the default for the particular
    // media type.
    unitParams[i] = &FloppyParams[du.type];
  }
  
  unit = 0;
  curParams = 0;
  ctrlrStatus = CtrlrIdle;
  
  KernThread::SetReady(Thread::DiskDaemon);
  
//  IDT::ResetPIC();
  
  return;
}

void
FDC::SeekIntr(FixRegs *fr)
{
#ifdef FDCDEBUG
  Diag::printf("FDC::seek_intr(): curcmd == %d\n", lastCmd);
#endif
  
  OutFDC(NECCMD::SENSEI);
  
  if (GetResults() != 2) {
      // controller is hosed - resetting it should restart the requests.
    Reset();
    Diag::fatal(0,"FDC::seek_intr(): Floppy not responding");
  }
  
  uint32_t sr0 = results[0];
  
      // We should have completed the seek:
  if (!(sr0 & SR0::SeekEnd))
    nError += 2;
  
      // We should not have aborted or run off the end of the
      // drive. In the event that we did, give it one more try
      // because we might be recalibrating a drive with > 80
      // cylinders someday:
  
  if (sr0 & (SR0::EquipChk | SR0::IC))
    nError += 4;
  
  if (nError > MAX_ERROR) {
    Diag::fatal(0, "Error count exceeded\n");
    req->OnIntrCompletion(IOR::Range);
  }
  
  if (nError)
    return;
    
  DiskUnit& du = GetUnit(unit);
  
  du.curcyl = results[1];
#if 0
  du.curhd = (results[0] & 0x4) ? 1 : 0;
#endif
  
#ifdef FDCDEBUG
  Diag::printf("Post seek: unit %d curcyl=%d\n", unit, du.curcyl);
#endif

  if (du.calibrated == false) {
    assert(du.curcyl == 0);
    du.calibrated = true;
  }
#if 0
  if (ioReq->cmd == DiskIoReq::OpSeek)
    req->Completed();
#endif
}

void
FDC::RwIntr(FixRegs *fr)
{
#ifdef FDCDEBUG
  Diag::printf("FDC::rw_intr(): curcmd == %d\n", lastCmd);
#endif
  
  if (GetResults() != 7) {
    // controller is hosed - resetting it should restart the requests.
    Reset();
    Diag::fatal(0,"FDC::rw_intr(): Floppy not responding");
  }
  
  uint32_t sr0 = results[0];
  
  // We should not have interrupted or run off the end of the
  // drive. We should not have needed to seek.
  
  if ((sr0 & SR0::IC) == 0) {
    // We now need to decide if we are done with the operation or
    // not.  If the operation went over a track boundary, we need
    // to hack track, head, sector, and nsec and start over:
    
    // Bounce buffer copy.  Probably shouldn't be done with interrupts
    // disabled, but until I rebuild the I/O subsystem this is
    // the simplest solution.
    
    if (dmakva != iokva)
      bcopy(dmakva, iokva, nsec * EROS_SECTOR_SIZE);
    
    nsec -= secsToXfer;
    start += secsToXfer;
    iokva += (secsToXfer * EROS_SECTOR_SIZE);
  }
  
      // FIX: Seeking is probably panicworthy.
  if (sr0 & (SR0::EquipChk | SR0::SeekEnd | SR0::IC))
    Diag::fatal(0,"FDC: Unexpected seek\n");
  
  uint8_t sr1 = results[1];
  
      // A CRC error just means a bad sector.  We should retry:
  if (sr1 & (SR1::BadCrc | SR1::NoData))
    nError += 2;		// allow five retries
  
  if (nError > MAX_ERROR) {
    req->OnIntrCompletion(IOR::Range);
    return;
  }
  
      // An overrun or going past end of cylinder is a fatal
      // problem. Something is wrong with the kernel:
  if (DMA::get_residue(DMAC::FLOPPY) ||
      (sr1 & (SR1::Overrun | SR1::CylEnd)))
    Diag::fatal(0,"DMA Overrun by fd%d\n", unit);
  
  if (!nsec) {
#ifdef FDCDEBUG
    Diag::printf("Op completed\n");
#ifdef PARANOID
    uint32_t* puint32_t = (Word *) UserMem.CTEtoPage(req->cte);

    int i;
    for (i = 0; i < 8; i++)
      Diag::printf("0x%x ", *pWord++);
    Diag::printf("\n");
#endif
#endif

    req->OnIntrCompletion(IOR::OK);
    KernThread::SetReady(Thread::DiskDaemon);
  }
}

    
// MOTOR CONTROL
//
// An FDC has some number of associated diskettes, each of which has a
// motor whose state must unfortunately be modeled.
//
// The spin variable has the following values:
//
//    1   Motor is spinning
//    0   Motor is off
//   -1   Motor will be spinning when the timer goes off
//

void
FDC::Spinup(uint8_t unit)
{
  // Because the spinup timer could go off while we are here, this
  // is a critical region:
  
#ifdef FDCMOTOR
  Diag::printf("FDC::SpinUp(%d): enter\n", unit);
#endif
  
  DiskUnit& du = GetUnit(unit);
  
  assert(du.spinning == false);
  
#ifdef FDCMOTOR
  Diag::printf("FDC::SpinUp(): %s: spinning up\n", UnitName(unit));
#endif
  
  du.status = DiskUnit::UnitSpinUp;
  
  dor |= (0x10 << unit);
  
  // Spin control.  Bet you thought only marketing did that.
  outb(NECREG::DOR, dor);
  
  timer.SleepFor(MOTOR_START_WAIT);
}

void
FDC::OnIdle(uint8_t unit)
{
#ifdef FDCMOTOR
  Diag::printf("FDC::OnIdle(): OnIdle called.\n");
#endif
  DiskUnit& du = GetUnit(unit);

  if (du.unitPresent == false)
    Diag::fatal(0, "FDC::OnIdle(): Tried to idle offline unit %d\n", unit);
  
  IDT::Disable();		// CRITICAL REGION

  if (!timer.active)
    timer.SleepFor(MOTOR_SHUT_WAIT);

  du.status = DiskUnit::UnitIdle;

  IDT::Enable();		// END CRITICAL REGION
}

void
FDC::Spindown(uint8_t unit)
{
  DiskUnit& du = GetUnit(unit);

  dor &= ~(DOR::Motor0 << unit);
  outb(NECREG::DOR, dor);
  du.spinning = false;
}

// Since floppies are not partitionable, this is fairly simple.
bool
FDC::Attach(uint8_t unit)
{
  DiskUnit& du = GetUnit(unit);

  // Attaching media that are not present should succeed trivially.

  if (!du.unitPresent) {
    Diag::printf("unit %d is not present\n", unit);
    return true;
  }
  if (!du.mediaPresent) {
    Diag::printf("unit %d - no media\n", unit);
    return true;
  }

  // do not attach redundantly...
  if (du.attached) {
    Diag::printf("unit %d is attached\n", unit);
    return true;
  }

  // Force a read of sector 0, whose side effect will be to do
  // autoformat detection:
  
  Diag::printf("Reading raw page 0 from unit %d\n", du.unitNo);
  CoreObject* bootSector = du.ReadRawPage(0);
  if (!bootSector)
    return false;

  UserMem.releasePage(bootSector);

  Diag::printf("Released raw page 0 from unit %d\n", du.unitNo);

  DiskVolume* pVol = DiskVolume::AllocVolume();

  Diag::printf("DiskVolume structure allocated\n");

  pVol->diskUnit = &units[unit];
  pVol->start = 0;
  pVol->hostPartition = 0;
  pVol->isErosVolume = false;	// until we know otherwise

  if (unit == SysConfig.boot.drive)
    pVol->isBootVolume = true;

  pVol->Announce();

  du.attached = true;
  return true;
}

void
FDC::Detach(uint8_t unit)
{
  Diag::fatal(0, "FDC::detach() not implemented yet\n");
}

uint32_t
FDC::GetCylinder(uint8_t unit, uint32_t sec)
{
  uint32_t secsPerCyl = unitParams[unit]->nHd * unitParams[unit]->nSec;
  return sec/secsPerCyl;
}
#endif
