/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef BOOTSECTOR_H
#undef BOOTSECTOR_H
#define BOOTSECTOR_H NULLHEADER
#endif

struct BootSector {
    char	jmpInstr[3];
    char	vendor[8];	/* vendor name */
    char	sectorSize[2];	/* size of a disk sector */
    char	sectorsPerCluster[1];
    char	bootProgSectors[2]; /* sectors occupied by the boot program */
    char	nFAT[1];
    char	nRootDirEnts[2];
    char	nDiskSector[2];	/* if zero, see 4 byte version below */
    char	mediaDescriptor[1];
    char	sectorsPerFat[2];
    char	sectorsPerTrack[2];
    char	heads[2];
    char	hiddenSectors[2];
    char	reserved[2];
    char	nDiskSector2[4];
    char	physDrvNumber[1];
    char	reserved2[1];
    char	dos4Signatureuint8_t[1];
    char	serialNumber[4];
    char	volumeName[11];
    char	fatType[8];

    /* following are unique to EROS: */
    char	primaryDivTablePage[2];
    char	secondaryDivTablePage[2];
    char	kernelStartPage[2];
    char	kernelPages[2];
} ;
