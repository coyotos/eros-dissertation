/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Node.hxx>
#include <machine/KernTune.hxx>
#include <kerninc/Depend.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/util.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Check.hxx>
#include <eros/Key.h>
#include <kerninc/Process.hxx>
#include <machine/PTE.hxx>
#include "TSS.hxx"
#include <eros/Invoke.h>
#include <eros/ProcessState.h>
#include <eros/SegKeeperInfo.h>
#include <eros/Registers.h>
#include <eros/i486/Registers.h>
#include <kerninc/PhysMem.hxx>
#include <eros/ProcessKey.h>
#include <eros/SysTraceKey.h>
#include <kerninc/Invocation.hxx>
#if 0
#include <machine/RegLayout.hxx>
#endif
#include <disk/DiskLSS.hxx>

#include "gen.REGMOVE.hxx"
// #define MSGDEBUG
// #define RESUMEDEBUG
// #define XLATEDEBUG

Process *Process::ContextCache;
Process *Process::fpuOwner;
#ifdef SMALL_SPACES
PTE *Process::smallSpaces = 0;
#endif

void
Process::AllocUserContexts()
{
  Process::ContextCache = new (0) Process[KTUNE_NCONTEXT];

  assert (KTUNE_NCONTEXT < 1000);

  for (int i = 0; i < KTUNE_NCONTEXT; i++) {
    int ndx = i;
    Process::ContextCache[i].name[0] = 'u';
    Process::ContextCache[i].name[1] = 's';
    Process::ContextCache[i].name[2] = 'e';
    Process::ContextCache[i].name[3] = 'r';
    Process::ContextCache[i].name[4] = '0' + (ndx / 100);
    ndx %= 100;
    Process::ContextCache[i].name[5] = '0' + (ndx / 10);
    ndx %= 10;
    Process::ContextCache[i].name[6] = '0' + (ndx % 10);
    Process::ContextCache[i].name[7] = 0;
  }

  MsgLog::printf("Allocated User Contexts: 0x%x at 0x%08x\n",
		 sizeof(Process[KTUNE_NCONTEXT]),
		 Process::ContextCache);
}

// FIX: It is unfortunate that some of these checks require !NDEBUG.
// Should they?
bool
Check::Contexts(const char *)
{
  bool result = true;
  
  IRQ::DISABLE();

  // It is possible for this to get called from interrupt handlers
  // before the context cache has been allocated.
  if (Process::ContextCache) {
    for (int i = 0; i < KTUNE_NCONTEXT; i++) {
      Process *p = &Process::ContextCache[i];
    
#ifndef NDEBUG
      if (p->kr.IsValid(p) == false) {
	result = false;
	break;
      }
#endif
    
#ifndef NDEBUG
      for (unsigned k = 0; k < EROS_PROCESS_KEYREGS; k++) {
	if (p->keyReg[0].IsValid() == false) {
	  result = false;
	  break;
	}
      }
#endif

      if (p->procRoot && p->procRoot->IsFree()) {
	MsgLog::dprintf(true, "Context 0x%08x has free process root 0x%08x\n",
			p, p->procRoot);
	result = false;
      }

#ifdef ProcGenRegs
      if (p->annexNode && p->annexNode->IsFree()) {
	MsgLog::dprintf(true, "Context 0x%08x has free annex node 0x%08x\n",
			p, p->annexNode);
	result = false;
      }
#endif

      if (p->keysNode && p->keysNode->IsFree()) {
	MsgLog::dprintf(true, "Context 0x%08x has free keys node 0x%08x\n",
			p, p->keysNode);
	result = false;
      }

      if (result == false)
	break;
    }
  }

  IRQ::ENABLE();

  return result;
}

#ifndef NDEBUG
bool
ValidCtxtPtr(const Process *ctxt)
{
  if ( ((uint32_t) ctxt < (uint32_t) Process::ContextCache ) || 
       ((uint32_t) ctxt >= (uint32_t)
	&Process::ContextCache[KTUNE_NCONTEXT]) )
    return false;

  uint32_t offset = ((uint32_t) ctxt) - ((uint32_t) Process::ContextCache);
  offset %= sizeof(Process);

  if (offset == 0)
    return true;
  return false;
}

#ifdef KEYREGS_IN_CONTEXT
bool
Process::ValidKeyReg(const Key *pKey)
{
  if ( ((uint32_t) pKey < (uint32_t) Process::ContextCache ) || 
       ((uint32_t) pKey >= (uint32_t)
	&Process::ContextCache[KTUNE_NCONTEXT]) )
    return false;

  /* Find the containing context: */
  uint32_t ctxt = ((uint32_t) pKey) - ((uint32_t) Process::ContextCache);
  ctxt /= sizeof(Process);

  Process *p = &Process::ContextCache[ctxt];
  
  if ( ((uint32_t) pKey < (uint32_t) &p->keyReg[0] ) || 
       ((uint32_t) pKey >= (uint32_t) &p->keyReg[EROS_PROCESS_KEYREGS]) )
    return false;

  uint32_t offset = ((uint32_t) pKey) - ((uint32_t) &p->keyReg[0]);

  offset %= sizeof(Key);
  if (offset == 0)
    return true;
  
  return false;
}
#endif
#endif

#ifndef NDEBUG
bool
ValidCtxtKeyRingPtr(const KeyRing* kr)
{
  for (uint32_t c = 0; c < KTUNE_NCONTEXT; c++) {
    Process *ctxt = &Process::ContextCache[c];

    if (kr == &ctxt->kr)
      return true;
  }

  return false;
}
#endif

void
Process::FlushAll()
{
  for (uint32_t c = 0; c < KTUNE_NCONTEXT; c++) {
    Process *ctxt = &Process::ContextCache[c];

    // Unload the context structure, as we are going to COW the process
    // root anyway.
    
    ctxt->Unload();
  }
}

extern "C" {
  void resume_from_kernel_interrupt(fixregs_t *);
  void resume_process(Process *);
  void resume_v86_process(Process *);
};

void
Process::DumpFixRegs()
{
  if (saveArea == 0)
    MsgLog::printf("Note: context is NOT runnable\n");
  ::DumpFixRegs(&fixRegs);
}

void
Process::DumpFloatRegs()
{
  MsgLog::printf("fctrl: 0x%04x fstatus: 0x%04x ftag: 0x%04x fopcode 0x%04x\n",
		 fpuRegs.f_ctrl, 
		 fpuRegs.f_status, 
		 fpuRegs.f_tag, 
		 fpuRegs.f_opcode);
  MsgLog::printf("fcs: 0x%04x fip: 0x%08x fds: 0x%04x fdp: 0x%08x\n",
		 fpuRegs.f_cs, 
		 fpuRegs.f_ip,
		 fpuRegs.f_ds,
		 fpuRegs.f_dp);
}
	      
void
DumpFixRegs(const fixregs_t * fx)
{
#if 0
  uint32_t cip;
  __asm__("movl 4(%%ebp),%0"
		: "=g" (cip)
	        : /* no inputs */);
#endif

#if 0
  if ( sa_IsProcess(fx) )
    MsgLog::printf("Process Savearea 0x%08x (cip 0x%08x)\n", fx, cip);
  else
    MsgLog::printf("Kernel Savearea 0x%08x (cip 0x%08x)\n", fx, cip);
#else
  if ( sa_IsProcess(fx) )
    MsgLog::printf("Process Savearea 0x%08x\n", fx);
  else
    MsgLog::printf("Kernel Savearea 0x%08x\n", fx);
#endif
  
  MsgLog::printf(
       "Units  = 0x%08x  ASID  = 0x%08x  EFLAGS = 0x%08x\n"
       "EAX    = 0x%08x  EBX   = 0x%08x  ECX    = 0x%08x\n"
       "EDX    = 0x%08x  EDI   = 0x%08x  ESI    = 0x%08x\n"
       "EBP    = 0x%08x  CS    = 0x%08x  EIP    = 0x%08x\n"
       "ExNo   = 0x%08x  ExAdr = 0x%08x  Error  = 0x%08x\n",

       fx->ReloadUnits, fx->MappingTable, fx->EFLAGS,
       fx->EAX, fx->EBX, fx->ECX,
       fx->EDX, fx->EDI, fx->ESI,
       fx->EBP, fx->CS, fx->EIP,
       fx->ExceptNo, fx->ExceptAddr, fx->Error);

  if ( sa_IsProcess(fx) ) {
    // user save area:
    const fixregs_t* sa = fx;
    
    MsgLog::printf(
		   "invTy = %d invKey = 0x%x sndPtr = 0x%08x sndLen= %5d sndKeys=0x%08x rcvKeys=0x%08x\n",
		   sa->invType, sa->invKey, sa->sndPtr, sa->sndLen,
		   sa->sndKeys, sa->rcvKeys);

    MsgLog::printf(
		   "SS     = 0x%08x  ESP   = 0x%08x\n"
		   "DS   = 0x%04x  ES   = 0x%04x  FS   = 0x%04x"
		   "  GS   = 0x%04x\n",
		   sa->SS, sa->ESP,
		   sa->DS, sa->ES, sa->FS, sa->GS);
  }
  else {
    uint16_t ss;
    uint16_t ds;
    uint16_t es;
    uint16_t fs;
    uint16_t gs;
    
    __asm__ __volatile__ ("mov %%ss, %0"
			  : "=r" (ss)
			  : /* no input */);
    __asm__ __volatile__ ("mov %%ds, %0"
			  : "=r" (ds)
			  : /* no input */);
    __asm__ __volatile__ ("mov %%es, %0"
			  : "=r" (es)
			  : /* no input */);
    __asm__ __volatile__ ("mov %%fs, %0"
			  : "=r" (fs)
			  : /* no input */);
    __asm__ __volatile__ ("mov %%gs, %0"
			  : "=r" (gs)
			  : /* no input */);
    MsgLog::printf(
	 "SS     = 0x%08x  ESP   = 0x%08x\n"
	 "DS   = 0x%04x  ES   = 0x%04x  FS   = 0x%04x  GS   = 0x%04x\n",

	 ss, ((uint32_t) fx) + 60,
	 ds, es, fs, gs);
  }
}

// NEW POLICY FOR USER CONTEXT MANAGEMENT:
//
//   A context goes through several stages before being allowed to
//   run:
//
//   HAZARD    ACTION
//
//   DomRoot     Load the process root registers.
//
//   Annex0      Reprepare the process general registers annex and load
//               it's registers 
//
//   KeyRegs     Reprepare the process key registers annex and load
//               it's registers
//
//   Validate    Verify that all register values are suitable.
//
//   AddrSpace   Reload the master address space pointer.
//
//   FloatUnit   Floating point unit needs to be loaded.
//
// Note that the Annex0 hazard implies that the associated capability
// in the process root is NOT validated.  If any of 'DomRoot' 'Annex0'
// or 'KeyRegs' cannot be cleared the process is malformed.
//
// Since it has been getting me in trouble, I am no longer trying to
// micro-optimize slot reload.

// Simple round-robin policy for now: 
void
Process::Load(Node* procRoot)
{
  assert(procRoot);

  static uint32_t nextClobber = 0;

  Process* pContext = 0;
  
#if 0
  MsgLog::printf("Begin Process::Load\n");
#endif
  
  do {
    pContext = &Process::ContextCache[nextClobber++];
    if (nextClobber >= KTUNE_NCONTEXT)
      nextClobber = 0;

    if (pContext == Thread::Current()->context) {
      pContext = 0;
      continue;
    }

    if (inv.IsActive() && pContext == inv.invokee) {
      pContext = 0;
      continue;
    }
    
#if 0
    if (pContext->pinCount)
      pContext = 0;
#endif
    
    if (pContext->curThread &&
	pContext->curThread->state == Thread::Running)
      pContext = 0;
  } while (pContext == 0);

  Process& cc = (*pContext);
  
  // wipe out current contents, if any
#if 0
  MsgLog::printf("  Unload old context\n");
#endif

  cc.Unload();

#if 0
  MsgLog::printf("  unloaded\n");
#endif
  
  assert(procRoot->obType == ObType::NtProcessRoot);
  
  cc.procRoot = procRoot;
  
#ifndef KEYREGS_IN_CONTEXT
  cc.keyReg = 0;
#endif
#ifdef ProcGenRegs
  cc.annexNode = 0;
#endif
  cc.saveArea = 0;		// make sure not runnable!
  cc.faultCode = FC_NoFault;
  cc.faultInfo = 0;
  cc.processFlags = 0;
  /* FIX: what to do about runState? */

#ifdef SMALL_SPACES
  uint32_t ndx = &cc - Process::ContextCache;

  cc.limit = SMALL_SPACE_PAGES * EROS_PAGE_SIZE;
  cc.bias = UMSGTOP + (ndx * SMALL_SPACE_PAGES * EROS_PAGE_SIZE);
  cc.smallPTE = &smallSpaces[SMALL_SPACE_PAGES * ndx];

#if 0
  MsgLog::dprintf(true, "Loading small space process 0x%X bias 0x%x "
		  "limit 0x%x\n",
		  procRoot->oid, cc.bias, cc.limit);
#endif
  
  for (uint32_t pg = 0; pg < SMALL_SPACE_PAGES; pg++)
    cc.smallPTE[pg].Invalidate();
#endif

  cc.fixRegs.MappingTable = KERNPAGEDIR;
  
  cc.hazards =
    hz::DomRoot | hz::KeyRegs | hz::Schedule | hz::AddrSpace;
#ifdef EROS_HAVE_FPU
  // Must be hazarded by float regs so that we can correctly re-issue
  // floating point exceptions on restart:

  cc.hazards |= hz::FloatRegs;
#endif

  cc.curThread = 0;
  
  procRoot->context = &cc;
  procRoot->obType = ObType::NtProcessRoot;
#if 0
  MsgLog::printf("End Process::Load\n");
#endif
}

// ValidateRegValues() -- runs last to validate that the loaded context
// will not violate protection rules if it is run.  This routine must
// be careful not to overwrite an existing fault condition. To avoid
// this, it must only alter the fault code value if there isn't
// already a fault code.  Basically, think of this as meaning that bad
// register values are the lowest priority fault that will be reported
// to the user.
void
Process::ValidateRegValues()
{
  if (processFlags & PF_Faulted)
    return;
  
  uint32_t code = 0;
  uint32_t info = 0;
  
  if ( fixRegs.CS && ((fixRegs.CS & 0x7) != 0x3) ){
#if 0
    MsgLog::dprintf(true, "Ctxt 0x%08x has bad CS value\n", this);
#endif
    code = FC_RegValue;
    info = fixRegs.CS;
  }
  else if ( fixRegs.DS && ((fixRegs.DS & 0x7) != 0x3) ){
#if 0
    MsgLog::dprintf(true, "Ctxt 0x%08x has bad DS value\n", this);
#endif
    code = FC_RegValue;
    info = fixRegs.DS;
  }
  else if ( fixRegs.ES && ((fixRegs.ES & 0x7) != 0x3) ){
#if 0
    MsgLog::dprintf(true, "Ctxt 0x%08x has bad ES value\n", this);
#endif
    code = FC_RegValue;
    info = fixRegs.ES;
  }
  else if ( fixRegs.FS && ((fixRegs.FS & 0x7) != 0x3) ){
#if 0
    MsgLog::dprintf(true, "Ctxt 0x%08x has bad FS value\n", this);
#endif
    code = FC_RegValue;
    info = fixRegs.FS;
  }
  else if ( fixRegs.GS && ((fixRegs.GS & 0x7) != 0x3) ){
#if 0
    MsgLog::dprintf(true, "Ctxt 0x%08x has bad GS value\n", this);
#endif
    code = FC_RegValue;
    info = fixRegs.GS;
  }
  else if ( fixRegs.SS && ((fixRegs.SS & 0x7) != 0x3) ){
#if 0
    MsgLog::dprintf(true, "Ctxt 0x%08x has bad SS value\n", this);
#endif
    code = FC_RegValue;
    info = fixRegs.SS;
  }
  else if ( (fixRegs.EFLAGS & EFLAGS_Interrupt) == 0 ) {
#if 0
    MsgLog::dprintf(true, "Ctxt 0x%08x has bad EFLAGS value 0x%08x\n", this, fixRegs.EFLAGS);
#endif
    // interrupts must be enabled, various others not:
    code = FC_RegValue;
  }
  else if ( fixRegs.EFLAGS &
	    (EFLAGS_Nested | EFLAGS_IOPL) ) {
#if 0
    MsgLog::dprintf(true, "Ctxt 0x%08x has bad EFLAGS value 0x%08x\n", this, fixRegs.EFLAGS);
    // Some flags must be clear:
#endif
    code = FC_RegValue;
  }

#ifdef EROS_HAVE_FPU
  /* Check for pending floating point exceptions too. If an unmasked
     exception is pending, invoke the process keeper.

     The bits in the control words are 'mask if set', while the bits
     in the status words are 'raise if set'.  The and-not logic is
     therefore what we want: */
  else if (fpuRegs.f_status & ~(fpuRegs.f_ctrl & FPSTATUS_EXCEPTIONS)) {
    code = FC_FloatingPointError;
    info = fpuRegs.f_cs;
  }

  /* The floating point %f_cs, %f_ip, %f_ds, and %f_dp registers
     record the location of the last operation.  They are output only,
     and need not be checked. */
#endif
  
  if (code)
    SetFault(code, info, true);
  
#if 0
  if (faultCode)
    MsgLog::printf("Bad register values\n");
#endif
  return;
}

// Both loads the register values and validates that the process root
// is well-formed.
void
Process::LoadFixRegs()
{
  assert(hazards & hz::DomRoot);

  assert(procRoot);
  procRoot->MakeObjectDirty();

#ifdef ProcAltMsgBuf
#error "Type checks need revision"
#endif
  
#if EROS_NODE_SIZE == 16
  const uint32_t top_numkey = 13;
#elif EROS_NODE_SIZE == 32
  const uint32_t top_numkey = 30;
#else
#error "unknown node size"
#endif

  for (uint32_t k = 5; k <= top_numkey; k++) {
    if ( (*procRoot)[k].IsType(KtNumber) == false ) {
      SetMalformed();
      return;
    }

    assert ( (*procRoot)[k].IsHazard() == false );
  }
  
  stats.pfCount = 0;

#if 1
  uint8_t *rootkey0 = (uint8_t *) & ((*procRoot)[0]);

  LOAD_FIX_REGS;
#else  
  DomRegLayout* pDomRegs = (DomRegLayout*) & ((*procRoot)[0]);

  faultCode = pDomRegs->faultCode;
  faultInfo = pDomRegs->faultInfo;
  runState = pDomRegs->runState;
  processFlags = pDomRegs->flags;
  
  if (faultCode)
    MsgLog::printf("Fault code already set\n");
  
  fixRegs.EDX = pDomRegs->edx;
  fixRegs.EDI = pDomRegs->edi;
  fixRegs.DS = pDomRegs->ds;

  fixRegs.ECX = pDomRegs->ecx;
  fixRegs.ESI = pDomRegs->esi;
  fixRegs.ES = pDomRegs->es;

  fixRegs.EAX = pDomRegs->eax;
  fixRegs.EBX = pDomRegs->ebx;
  fixRegs.SS = pDomRegs->ss;

  fixRegs.EIP = pDomRegs->eip;
  fixRegs.ESP = pDomRegs->esp;
  fixRegs.CS = pDomRegs->cs;

  fixRegs.EFLAGS = pDomRegs->eflags;
  fixRegs.EBP = pDomRegs->ebp;
  fixRegs.FS = pDomRegs->fs;
  fixRegs.GS = pDomRegs->gs;

  fixRegs.sndPtr = pDomRegs->sndPtr;
  fixRegs.sndLen = pDomRegs->sndLen;
  fixRegs.rcvPtr = pDomRegs->rcvPtr;
  fixRegs.invType = pDomRegs->invType;
    
  stats.evtCounter0 = pDomRegs->hiEvtCount;
  stats.evtCounter0 <<= 32;
  stats.evtCounter0 |= pDomRegs->loEvtCount;
  stats.evtCounter1 = 0;
#endif
  
  fixRegs.ReloadUnits = 0;
  
  for (uint32_t k = 5; k <= top_numkey; k++)
    (*procRoot)[k].SetRwHazard();

  hazards &= ~hz::DomRoot;

#if 0
  MsgLog::dprintf(true, "Process root oid=0x%x to ctxt 0x%08x, eip 0x%08x hz 0x%08x\n",
		  (uint32_t) procRoot->oid, this, fixRegs.EIP, hazards);
#endif
}

#if EROS_NODE_SIZE == 16
void
Process::LoadFloatRegs()
{
  assert (hazards & hz::FloatRegs);
  assert (procRoot->slot[ProcGenRegs].IsHazard() == false);

  if (procRoot->slot[ProcGenRegs].IsType(KtNode) == false) {
    SetFault(FC_MalformedProcess, 0);
    return;
  }
  
  procRoot->slot[ProcGenRegs].Prepare();
  Node *anx = (Node *) procRoot->slot[ProcGenRegs].GetObjectPtr();
  assert(anx->GetFlags(OFLG_PIN));

  anx->Unprepare(false);
  if (anx->obType != ObType::NtUnprepared || anx == procRoot) {
    SetFault(FC_MalformedProcess, 0);
    return;
  }

  for (uint32_t k = 0; k < EROS_NODE_SIZE; k++) {
    assert ( (*anx)[k].IsHazard() == false );
    if ((*anx)[k].IsType(KtNumber) == false) {
      SetFault(FC_MalformedProcess, 0);
      return;
    }
  }
  
  // Node is now known to be valid.  Mark it dirty and proceed:
  anx->MakeObjectDirty();
  
  anx->context = this;
  anx->obType = ObType::NtRegAnnex;
  annexNode = anx;
  procRoot->slot[ProcGenRegs].SetWrHazard();

  assert(annexNode);

  for (uint32_t k = 0; k < EROS_NODE_SIZE; k++)
    (*annexNode)[k].SetRwHazard();

  uint8_t *annex14 = (uint8_t *) & ((*annexNode)[0]);
    
  LOAD_FLOAT_REGS;

  hazards &= ~hz::FloatRegs;
}
#elif EROS_NODE_SIZE == 32
/* This version requires no annex node. */
void
Process::LoadFloatRegs()
{
  assert (hazards & hz::FloatRegs);
  assert (procRoot);
  assert (procRoot->IsDirty());
  
  uint8_t *rootkey0 = (uint8_t *) & ((*procRoot)[0]);

  LOAD_FLOAT_REGS;

  hazards &= ~hz::FloatRegs;
}
#endif

void
Process::LoadKeyRegs()
{
  assert (hazards & hz::KeyRegs);
  assert (procRoot->slot[ProcGenKeys].IsHazard() == false);

  if (procRoot->slot[ProcGenKeys].IsType(KtNode) == false) {
    SetMalformed();
    return;
  }
  
#if 0
  {
    Key& k = procRoot->slot[ProcGenKeys];
    MsgLog::printf("Preparing nd oid 0x%08x%08x\n",
		   (uint32_t) (k.GetKeyOid() >> 32),
		   (uint32_t) (k.GetKeyOid()));
  }
#endif
		 
  procRoot->slot[ProcGenKeys].Prepare();
  Node *kn = (Node *) procRoot->slot[ProcGenKeys].GetObjectPtr();
  assertex(kn,kn->IsPinned());
  
  assert ( kn->Validate() );
  kn->Unprepare(false);

  if (kn->obType != ObType::NtUnprepared || kn == procRoot) {
    SetMalformed();
    return;
  }

  kn->MakeObjectDirty();

#ifdef KEYREGS_IN_CONTEXT
  for (uint32_t k = 0; k < EROS_NODE_SIZE; k++) {
    if ( (*kn)[k].IsHazard() )
      MsgLog::dprintf(true, "Key register slot %d is hazarded in node 0x%08x%08x\n",
		      k,
		      (uint32_t) (kn->ob.oid >> 32), (uint32_t) kn->ob.oid);

#ifndef NDEBUG
    // We know that the context structure key registers are unhazarded
    // and unprepared by virtue of the fact that they are unloaded,
    // but check here just in case:
    if ( keyReg[k].IsHazard() )
      MsgLog::dprintf(true, "Key register %d is hazarded in Process 0x%08x\n",
		      k, this);

    if ( keyReg[k].IsUnprepared() == false )
      MsgLog::dprintf(true, "Key register %d is prepared in Process 0x%08x\n",
		      k, this);
#endif

    keyReg[k].NH_Set(kn->slot[k]);
    (*kn)[k].SetRwHazard();
  }
#else
  for (uint32_t k = 0; k < EROS_NODE_SIZE; k++)
    if ( (*kn)[k].IsHazard() )
      MsgLog::dprintf(true, "Key register slot %d is hazarded in node 0x%08x%08x\n",
		    k,
		    (uint32_t) (kn->oid >> 32), (uint32_t) kn->oid);
  
  (*kn)[0].UnsafeUnprepare();
  (*kn)[0].UnsafeZeroKey();
  (*kn)[0].SetWrHazard();

  keyReg = & (*kn)[0];
#endif
  
  // Node is now known to be valid...
  kn->context = this;
  kn->obType = ObType::NtKeyRegs;
  keysNode = kn;

  procRoot->slot[ProcGenKeys].SetWrHazard();

  assert(keysNode);

  hazards &= ~hz::KeyRegs;
}

void
Process::FlushFixRegs()
{
  assert((hazards & hz::DomRoot) == 0);
  
  assert(procRoot);
  assert(procRoot->IsDirty());

#ifdef ProcAltMsgBuf
#error "Type checks need revision"
#endif
  
#if EROS_NODE_SIZE == 16
  const uint32_t top_numkey = 13;
#elif EROS_NODE_SIZE == 32
  const uint32_t top_numkey = 30;
#else
#error "unknown node size"
#endif

  for (uint32_t k = 5; k <= top_numkey; k++)
    assert ( (*procRoot)[k].IsRwHazard() );

#if 1
  uint8_t *rootkey0 = (uint8_t *) & ((*procRoot)[0]);

  UNLOAD_FIX_REGS;
#else
  DomRegLayout* pDomRegs = (DomRegLayout*) & ((*procRoot)[0]);

  pDomRegs->faultCode = faultCode;
  pDomRegs->faultInfo = faultInfo;
  pDomRegs->runState = runState;
  pDomRegs->flags = processFlags;
  
  pDomRegs->edx = fixRegs.EDX;
  pDomRegs->edi = fixRegs.EDI;
  pDomRegs->ds = fixRegs.DS;

  pDomRegs->ecx = fixRegs.ECX;
  pDomRegs->esi = fixRegs.ESI;
  pDomRegs->es = fixRegs.ES;

  pDomRegs->eax = fixRegs.EAX;
  pDomRegs->ebx = fixRegs.EBX;
  pDomRegs->ss = fixRegs.SS;

  pDomRegs->eip = fixRegs.EIP;
  pDomRegs->esp = fixRegs.ESP;
  pDomRegs->cs = fixRegs.CS;

  pDomRegs->eflags = fixRegs.EFLAGS;
  pDomRegs->ebp = fixRegs.EBP;
  pDomRegs->fs = fixRegs.FS;

  pDomRegs->gs = fixRegs.GS;

  pDomRegs->sndPtr = fixRegs.sndPtr;
  pDomRegs->sndLen = fixRegs.sndLen;
  pDomRegs->rcvPtr = fixRegs.rcvPtr;
  pDomRegs->invType = fixRegs.invType;

  pDomRegs->loEvtCount = stats.evtCounter0;
  pDomRegs->hiEvtCount = stats.evtCounter0 >> 32;
#endif
  
  (*procRoot)[ProcSched].UnHazard();

  for (uint32_t k = 5; k <= top_numkey; k++)
    (*procRoot)[k].UnHazard();

  hazards |= (hz::DomRoot | hz::Schedule);
  saveArea = 0;
}

#ifdef EROS_HAVE_FPU
void
Process::SaveFPU()
{
  assert(fpuOwner == this);

  Machine::EnableFPU();

  __asm__ __volatile__("fnsave %0\n\t" : "=m" (fpuRegs));

  fpuOwner = 0;

  Machine::DisableFPU();
}

void
Process::LoadFPU()
{
  // FPU is unloaded, or is owned by some other process.  Unload
  // that, and load the current process's FPU state in it's place.
  if (fpuOwner)
    Process::fpuOwner->SaveFPU();

  assert(fpuOwner == 0);

  Machine::EnableFPU();

  __asm__ __volatile__("frstor %0\n\t"
		       : /* no outputs */
		       : "m" (fpuRegs));

  fpuOwner = this;
  Machine::DisableFPU();

  hazards &= ~hz::NumericsUnit;
}

#endif

#if EROS_NODE_SIZE == 16
void
Process::FlushFloatRegs()
{
  assert ((hazards & hz::FloatRegs) == 0);
  assert (annexNode);
  assert (procRoot);
  assert(annexNode->IsDirty());
  
#if 0
#endif

  for (uint32_t k = 0; k < EROS_NODE_SIZE; k++)
    (*annexNode)[k].SetRwHazard();

  if (Process::fpuOwner == this) {
    Machine::UnloadFPU(Process::fpuOwner);
    Machine::DisableFPU();
  }

#if 1
  uint8_t *annex14 = (uint8_t*) & ((*annexNode)[0]);

  UNLOAD_FLOAT_REGS;
#else
  GenRegLayout* pGenRegs = (GenRegLayout*) & ((*annexNode)[0]);

  pGenRegs->fpCtrlReg     = floatRegs.ctrlReg;
  pGenRegs->fpStatusReg   = floatRegs.statusReg;
  pGenRegs->fpTagReg      = floatRegs.tagWord;
  pGenRegs->fpInstrPtr    = floatRegs.fip;
  pGenRegs->fpInstrPtrSeg = floatRegs.fcs;
  pGenRegs->fpDataPtr     = floatRegs.fdp;
  pGenRegs->fpDataPtrSeg  = floatRegs.fds;
  pGenRegs->fr0 = floatRegs.st[0];
  pGenRegs->fr1 = floatRegs.st[1];
  pGenRegs->fr1 = floatRegs.st[2];
  pGenRegs->fr3 = floatRegs.st[3];
  pGenRegs->fr4 = floatRegs.st[4];
  pGenRegs->fr5 = floatRegs.st[5];
  pGenRegs->fr6 = floatRegs.st[6];
  pGenRegs->fr7 = floatRegs.st[7];
#endif
  
  for (uint32_t k = 0; k < EROS_NODE_SIZE; k++)
    (*annexNode)[k].UnHazard();

  annexNode->context = 0;
  annexNode->obType = ObType::NtUnprepared;
  annexNode = 0;

  procRoot->slot[ProcGenRegs].UnHazard();

  hazards |= hz::FloatRegs;
  hazards &= ~hz::NumericsUnit;
  saveArea = 0;
}
#elif EROS_NODE_SIZE == 32
/* This version requires no annex node. */
void
Process::FlushFloatRegs()
{
  assert ((hazards & hz::FloatRegs) == 0);
  assert (procRoot);
  assert (procRoot->IsDirty());

  uint8_t *rootkey0 = (uint8_t *) & ((*procRoot)[0]);

  UNLOAD_FLOAT_REGS;

  hazards |= hz::FloatRegs;
  saveArea = 0;
}
#endif

void
Process::FlushKeyRegs()
{
  assert (keysNode);
  assert ((hazards & hz::KeyRegs) == 0);
  assert(procRoot);
  assert (keysNode->IsDirty());

  assert ( keysNode->Validate() );

#if 0
  MsgLog::printf("Flushing key regs on ctxt=0x%08x\n", this);
  if (inv.IsActive() && inv.invokee == this)
    MsgLog::dprintf(true,"THAT WAS INVOKEE!\n");
#endif

#ifdef KEYREGS_IN_CONTEXT
  for (uint32_t k = 0; k < EROS_NODE_SIZE; k++) {
    keysNode->slot[k].UnHazard();
    keysNode->slot[k].NH_Set(keyReg[k]);

    /* Not hazarded because key register */
    keyReg[k].NH_ZeroKey();

    // We know that the context structure key registers are unhazarded
    // and unlinked by virtue of the fact that they are unloaded.
  }
#else
  (*keysNode)[0].UnHazard();
  keyReg = 0;
#endif

  keysNode->context = 0;
  keysNode->obType = ObType::NtUnprepared;
  keysNode = 0;

  hazards |= hz::KeyRegs;

  procRoot->slot[ProcGenKeys].UnHazard();
  saveArea = 0;
}

// Rewrite the process key back to our current thread.  Note that
// the thread's process key is not reliable unless this unload has
// been performed.
void
Process::SyncThread()
{
  assert(curThread);
  assert(procRoot);
  assert (curThread->context == this);
  
  Key& procKey = curThread->processKey;

  assert (procKey.IsHazard() == false);

  /* Not hazarded because thread key */
  if (procKey.IsPrepared())
    procKey.NH_Unprepare();

  procKey.InitType(KtProcess);
  procKey.SetUnprepared();
  procKey.subType = 0;
  procKey.keyData = 0;
  procKey.unprep.oid = procRoot->ob.oid;
  procKey.unprep.count = procRoot->ob.allocCount;
}

void
Process::Unload()
{
  // It might already be unloaded:
  if (procRoot == 0)
    return;
  
#if 0
  {
    const char *descrip = "other";
    bool shouldStop = false;
    
    if (this == Thread::Current()->context)
      descrip = "current";
    if (inv.IsActive() && this == inv.invokee) {
      descrip = "invokee";
      shouldStop = true;
    }
   
    MsgLog::dprintf(shouldStop, "Unloading %s ctxt 0x%08x\n", descrip, this);
  }
#endif
    
#if 0
  if (hazards & hz::DomRoot)
    MsgLog::dprintf(false, "Calling Context::Unload() on 0x%08x, eip=???\n", this);
  else
    MsgLog::dprintf(false, "Calling Context::Unload() on 0x%08x, eip=0x%08x\n",
		   this, fixRegs.EIP);
#endif

#if defined(DBG_WILD_PTR)
  if (dbg_wild_ptr)
    if (Check::Contexts("before unload") == false)
      halt('a');
#endif

  if (curThread) {
    SyncThread();
    curThread->ZapContext();
  }

#if defined(DBG_WILD_PTR)
  if (dbg_wild_ptr)
    if (Check::Contexts("after syncthread") == false)
      halt('b');
#endif
  
  fixRegs.MappingTable = 0;
  saveArea = 0;
  curThread = 0;

  if ( (hazards & hz::FloatRegs) == 0)
    FlushFloatRegs();

  if ((hazards & hz::KeyRegs) == 0)
    FlushKeyRegs();
  
  if ((hazards & hz::DomRoot) == 0)
    FlushFixRegs();
  
  if ( (*procRoot)[ProcAddrSpace].IsHazard() ) {
    Depend::InvalidateKey(&(*procRoot)[ProcAddrSpace]);
    (*procRoot)[ProcAddrSpace].UnHazard();
    hazards |= hz::AddrSpace;
  }

  procRoot->context = 0;
  procRoot->obType = ObType::NtUnprepared;

  assert(procRoot);
  
  kr.UnprepareAll();
  hazards = 0;
  procRoot = 0;
  saveArea = 0;

  stallQ.WakeAll();
}

// If the process has an invalid address space slot, it would probably
// be more efficient to simply invoke the keeper right here rather
// than start up a thread we know will fault.  There are three reasons
// not to do so:
//
// 1. It might get fixed before it runs.
// 2. It is more modular to let the pagefault handler do it, since
//    that handler needs to deal with this situation already.
// 3. This code is goddamn well hairy enough as it is.
//
// To load the mapping table pointer, we walk down the address space
// segment looking for a node spanning 2^32 bits (blss=7).  We then
// examine it's products looking for a suitable mapping table.
//
// If we do not find one, we can't just build one on the spot, because
// we must fabricate suitable depend entries for the new mapping
// table.




// This is somewhat misnamed, as loading the address space on the x86
// doesn't really load the address space at all -- it locates and
// loads the page directory, which need not contain any valid entries.
// Further faults will be handled in the page fault path.
//
// In loading the page directory, the LoadAddressSpace logic does not
// need to construct any dependency table entries for the BLSS::bit32
// node.  That node is a producer, and if it is taken out of core or
// its keys are deprepared we can (and must) walk the producer chain
// in any case.
//
// The address space segment slot may contain a segment key to a
// segment whose span is larger than the span of the hardware address
// space.  For example, the segment might span 2^40 bits while the
// hardware address space is only 2^32 bits.  In interpreting such a
// segment, we take the view that the address space of the process
// starts at offset 0 in the larger segment, and is only as big as the
// hardware will support.
//
// In the event that the address space segment is oversized in this
// way, however, LoadAddressSpace() must create depend entries for all
// nodes whose BLSS is > BLSS::bit32.  We commit a slight bit of
// silliness in doing this, which in practice works out okay.  A page
// directory holds 1024 entries.  For nodes whose BLSS is >
// BLSS::bit32, we know that the only key acting in a memory context
// is the key in slot 0.  We therefore claim (lying through our teeth)
// that these nodes span a range of 16 * 1024 == 16k entries in the
// page directory.  We check for out of range invalidation in the
// invalidate logic, so we never get caught by this.  Also, the
// invalidate logic knows the difference between page directories and
// page tables, and never zaps the kernel entries.
//

void
Process::LoadAddressSpace(bool prompt)
{
  assert (hazards & hz::AddrSpace);
  
  // It's possible that we will end up invalidating exactly the
  // entries we are after!
  if ( DoPageFault(fixRegs.EIP, false, false, prompt) ) {
    hazards &= ~hz::AddrSpace;
  }
}

// Fault code is tricky.  It is conceivable that the process does not
// posess a number key in the fault code slot.  In that event we set
// the fault code to FC_MalformedProcess.  Unless the process node
// posesses a number key in the fault code slot, the fault code will
// not be written back to the process root.  Note that it is not
// possible for a process with faultCode = FC_MalformedProcess to
// achieve any other fault code without first resolving that one, so
// it is okay to fail to write FC_MalformedProcess back to the process
// root.
//
// Note the runstate is not loaded here.  If the process root is
// malformed it's runstate is intrinsically undefined.  Such a process
// cannot execute instructions.  It's malformedness is discovered in
// one of two ways:
//
//   1. It was running and somehow became malformed, in which case it
//      invokes it's keeper on it's own behalf.
//   2. It was invoked by a third party, who is made to invoke its
//      keeper on its behalf.
//
// If the process root later becomes well formed we will be able to
// load its run state at that time.
#if 0
void
Process::LoadFaultCode()
{
  assert (procRoot);
  
  if ( procRoot->slot[ProcTrapCode].IsType(KtNumber) ) {
    DomRegLayout* pDomRegs = (DomRegLayout*) & ((*procRoot)[0]);

    faultCode = pDomRegs->faultCode;
    faultInfo = pDomRegs->faultInfo;
  }
  else {
    faultCode = FC_MalformedProcess;
    faultInfo = 0;
  }
  
  hazards &= ~State::DomFaultCode;
}
#endif

// The DoPrepare() logic has changed, now that we have merged the
// process prep logic into it...
void
Process::DoPrepare()
{
  assert(procRoot);
  assert (isUserContext);

  procRoot->TransLock();
#ifdef ProcGenRegs
  if (annexNode)
    annexNode->Pin();
#endif
  if (keysNode)
    keysNode->TransLock();
  
  hazards &= ~hz::Malformed;	// until proven otherwise
  
  bool check_disjoint
    = (hazards & (hz::DomRoot | hz::FloatRegs | hz::KeyRegs)); 

#if 0
  MsgLog::printf("Enter Process::DoPrepare()\n");
#endif
  // The order in which these are tested is important, because
  // sometimes satisfying one condition imposes another (e.g. floating
  // point bit set in the eflags register)

  if (hazards & hz::DomRoot)
    LoadFixRegs();

  if (faultCode == FC_MalformedProcess) {
    assert (processFlags & PF_Faulted);
    return;
  }
  
  if (hazards & hz::FloatRegs)
    LoadFloatRegs();

  if (hazards & hz::NumericsUnit)
    LoadFPU();

  if (hazards & hz::KeyRegs)
    LoadKeyRegs();

  if (check_disjoint) {
    if (
	(procRoot == keysNode)
#ifdef ProcGenRegs
	|| (procRoot == annexNode)
	|| (keysNode == annexNode)
#endif
	) {
      SetMalformed();
    }
  }
  
  if (faultCode == FC_MalformedProcess) {
    assert (processFlags & PF_Faulted);
    return;
  }
  
  if (hazards & hz::Schedule) {
    // FIX: someday deal with schedule keys!
    Key& schedKey = (*procRoot)[ProcSched];
    assert(schedKey.IsHazard() == false);
    
    if (schedKey.IsType(KtSched) == false) {
      SetFault(FC_NoSchedule, faultInfo, false);
      MsgLog::printf("No schedule key!\n");
    }
    else {
      schedKey.SetWrHazard();
      cpuReserve = &CpuReserve::CpuReserveTable[schedKey.subType];
      hazards &= ~hz::Schedule;
    }
  }

#if 0
  // This is wrong.  The original idea was that by prefaulting the EIP
  // address we could avoid an unnecessary kernel reentry.
  // Unfortunately, the prepare logic needs to be callable in order to
  // load the 32 bit register set from InvokeProcessKeeper.  If no
  // valid address space exists for the process, then trying to build the
  // address space here causes a segment fault.  If the relevant
  // segment has no keeper, this in turn yields in order to cause the
  // process keeper (if any) to be invoked.  In that particular
  // sequence of events, however, an infinite loop is created...
  //
  // Rather than try to automatically build the address space here, we
  // go ahead with a zero mapping table and take the extra instruction
  // fault.
  
  if (fixRegs.MappingTable == 0) {
    hazards |= hz::AddrSpace;
  }
  
  if (hazards & hz::AddrSpace)
    LoadAddressSpace(false);
#endif
  
  if (hazards & hz::SingleStep) {
    fixRegs.EFLAGS |= EFLAGS_Trap;
    hazards &= ~hz::SingleStep;
  }

  ValidateRegValues();
  
  // Change: It is now okay for the context to carry a fault code
  // and prepare correctly.

  saveArea = &fixRegs;
  stallQ.WakeAll();
}

// Extracted as macro for clarity of code below, which is already too
// complicated. 
// Merge must be prohibited on mapping root entries, since merging
// those breaks the context cache.
#if 1
#define ADD_DEPEND(pKey) \
    assert (pPTE0); \
    Depend::AddKey(pKey, pPTE0, canMerge); \
    if (pPTE1) \
      Depend::AddKey(pKey, pPTE1, canMerge);
#else
#define ADD_DEPEND(pKey) \
    assert (pPTE0); \
    Depend::AddKey(pKey, pPTE0, (pPTE0 == mappingRoot)?false:true); \
    if (pPTE1) \
      Depend::AddKey(pKey, pPTE1, true);
#endif

/* proc_WalkSeg() is a performance-critical path.  The segment walk
   logic is complex, and it occurs in the middle of page faulting
   activity, which for obvious reasons is performance-critical.

   The walker is also entered in stages to allow successive
   traversals.  This is mainly to keep the code machine-independent; a
   machine-dependent specialization of this routine could avoid a
   couple of procedure calls.

   The /pPTE0/ and /pPTE1/ arguments and the /canMerge/ argument
   should unquestionably be merged into the SegWalkInfo structure.

   The depth of the walk is bounded by MAX_SEG_DEPTH to detect
   possible loops in the segment tree.  Because the routine is
   multiply entered the actual depth limit can be two or three times
   the nominal limit.  The architecture guarantees a minimum traversal
   depth, but does not promise any particular maximum.


   The traversal process must maintain the following variables:

   /pSegKey/   pointer to the node slot containing the key that names
               the segment we are now considering.

   /segBlss/   size of the segment represented by pSegKey.
   
   /pKeptSeg/  pointer to the NODE that was the last red segment we
               went through.  Arguably, we should track two of these:
	       one for keepers and one for background segment logic.
	       At the present time we do not do so.

   /traverseCount/  the number of traversals we have done on this
               path.  At the moment this is not properly tracked if we
	       use the short-circuited fast walk.
   
   /canWrite/  whether the path traversed already conveyed write
               permission.
   /canCall/   whether the path traversed already allowed keeper
               invocation.
   
   /segFault/  the fault code, if any, associated with this
               traversal.

   In addition, the following members of the WalkInfo structure are
   actually arguments passed from above.  They are stored in the
   WalkInfo structure because they do not change across the several
   calls:
   
   /writeAccess/  whether the translation is for writing
   /prompt/       whether we are willing to take a keeper fault to
                  get a translation.


   I have temporarily removed the support for capability pages to get
   one thing right at a time.
*/
#define BLSS_SHIFT(lss) \
  (((lss) - EROS_PAGE_BLSS - 1) * EROS_NODE_LGSIZE + EROS_PAGE_ADDR_BITS)

inline uint64_t
BLSS_MASK64(uint32_t blss)
{
  uint32_t bits_to_shift =
    (blss - EROS_PAGE_BLSS) * EROS_NODE_LGSIZE + EROS_PAGE_ADDR_BITS; 

  uint64_t mask = (1ull << bits_to_shift);
  mask -= 1ull;
  
  return mask;
}

inline uint32_t
BLSS_MASK32(uint32_t blss)
{
  uint32_t bits_to_shift =
    (blss - EROS_PAGE_BLSS) * EROS_NODE_LGSIZE + EROS_PAGE_ADDR_BITS; 

  uint32_t mask = (1ull << bits_to_shift);
  mask -= 1ull;
  
  return mask;
}

/* #define WALK_LOUD  0x1c00 */

// This is the fully general version.  It handles 64 bit offsets, and
// is relatively expensive to execute because of the need for 64-bit
// arithmetic.
bool
proc_WalkSeg64(Process * p, SegWalkInfo& wi, uint32_t stopBlss,
	     PTE* pPTE0, PTE* pPTE1, bool canMerge)
{
  const uint32_t MAX_SEG_DEPTH = 20;
  KernStats.nWalkSeg++;
  Key *pFormatKey = 0;
  Key *pSegKey = 0;

  if (wi.segObj == 0) {
    pSegKey = p->GetSegRoot();
    wi.segBlss = EROS_ADDRESS_BLSS;	// until proven otherwise
    wi.offset = wi.vaddr;
    wi.redSeg = 0;
    wi.redSegOffset = 0;
    wi.redSpanBlss = 0;
    wi.segObjIsRed = false;

    wi.canCall = true;		// until proven otherwise
    wi.canWrite = true;		// until proven otherwise
    wi.weakRef = false;		// until proven otherwise

    goto process_key;
  }
  
  for(;;) {
    if (wi.segBlss <= stopBlss) {
#ifdef WALK_LOUD
      MsgLog::dprintf(false, "ret proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		      "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d segKey 0x%x\n"
		      "ca %d canCall %d canWrite %d weakRef %d\n"
		      "redSeg 0x%x redOffset 0x%X\n",
		      wi.segObj, wi.segBlss, wi.segObjIsRed,
		      wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		      pSegKey,
		      wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef,
		      wi.redSeg, wi.redSegOffset);
#endif
      return true;
    }
      
    KernStats.nWalkLoop++;

    /////////////////////////////////////////////////////
    //
    // BEGIN Traversal step logic:
    //
    /////////////////////////////////////////////////////

    {
#ifdef WALK_LOUD
      MsgLog::dprintf(false, "wlk proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		      "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d segKey 0x%x\n"
		      "ca %d canCall %d canWrite %d weakRef %d\n"
		      "redSeg 0x%x redOffset 0x%X\n",
		      wi.segObj, wi.segBlss, wi.segObjIsRed,
		      wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		      pSegKey,
		      wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef,
		      wi.redSeg, wi.redSegOffset);
#endif

#ifndef NDEBUG
      if (wi.segObj->obType == ObType::NtUnprepared)
	MsgLog::fatal("Some node unprepare did not work at UserContext.cxx:%d\n",
		      __LINE__);
#endif
    
      Node* segNode = (Node *) wi.segObj;
	
      assert (segNode->obType == ObType::NtSegment);
	
      uint32_t initialSlots = EROS_NODE_SLOT_MASK;

      if (wi.segObjIsRed) {
	assert (wi.segObjIsRed);
	pFormatKey = &segNode->slot[RedSegFormat];

	// We know that the format key is valid.  It was checked in
	// the key-related checking above, either during the current
	// pass or when building information about the parent page
	// table.  If the format key has since been modified, then
	// the parent page table entry will have been modified, and
	// we will not have gotten here without rechecking it.
	  
	ADD_DEPEND(pFormatKey);

	initialSlots = REDSEG_GET_INITIAL_SLOTS(pFormatKey->nk);
      }

      wi.traverseCount++;	

      uint32_t shiftAmt = BLSS_SHIFT(wi.segBlss);
      uint32_t ndx = wi.offset >> shiftAmt;

      if (ndx > initialSlots)
	goto invalid_addr;

      wi.offset &= BLSS_MASK64(wi.segBlss - 1);

      pSegKey = &segNode->slot[ndx];

      // FIX: At the moment, a weak key forces read-only status in
      // the next traversal.  This is probably a bug, and the formal
      // work shows that removing this check doesn't compromise
      // anything we care about.  Leave it for now, though.

      wi.canWrite &= !wi.weakRef;
    }
    /////////////////////////////////////////////////////
    //
    // END Traversal step logic:
    //
    /////////////////////////////////////////////////////

    if (wi.traverseCount >= MAX_SEG_DEPTH)
      goto bad_seg_depth;

  process_key:

    /////////////////////////////////////////////////////
    //
    // BEGIN key processing logic:
    //
    /////////////////////////////////////////////////////

    {
#ifdef WALK_LOUD
      if (wi.segObj == 0)
	MsgLog::dprintf(false, "key proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		      "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d segKey 0x%x\n"
		      "ca %d canCall %d canWrite %d weakRef %d\n"
		      "redSeg 0x%x redOffset 0x%X\n",
		      wi.segObj, wi.segBlss, wi.segObjIsRed,
		      wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		      pSegKey,
		      wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef,
		      wi.redSeg, wi.redSegOffset);
#endif
      pSegKey->Prepare();
    
      uint8_t keyType = pSegKey->GetType();
    
      // Process everything but the per-object processing first:
      switch(keyType) {
      case KtMisc:
	if ( (pSegKey->subType != MiscKeyType::TimePage) )
	  goto seg_malformed;

	wi.canWrite = false;
	wi.canCall = false;
	wi.weakRef = true;
	wi.segBlss = EROS_PAGE_BLSS;

	wi.segObj = SysTimer::TimePageHdr;
	wi.segObjIsRed = false;
      
	if (wi.writeAccess)
	  goto access_fault;

	ADD_DEPEND(pSegKey);

	return true;

      case KtNode:
      case KtSegment:
      case KtDataPage:
      case KtCapPage:
	{
	  Node* segNode = 0;

	  uint32_t parentBlss = wi.segBlss;
	      
	  wi.segBlss = pSegKey->GetBlss();
	  if (wi.segBlss > MAX_RED_BLSS)
	    goto seg_malformed;
	    
	  // This is NOT the same as the corresponding /if/ on many
	  // systems -- the code generated by this expression contains
	  // no branch.
	  wi.canWrite &= !pSegKey->IsReadOnly();
    
	  if (wi.capAccess && keyType == KtDataPage)
	    goto invalid_type;
	  
	  if (wi.capAccess == 0 && keyType == KtCapPage)
	    goto invalid_type;
	  
	  if (!wi.canWrite && wi.writeAccess)
	    goto access_fault;
	
	  wi.canCall &= !pSegKey->IsNoCall();
	
	  wi.weakRef |= pSegKey->IsWeak();
    
	  wi.segObj = pSegKey->ok.pObj;
	  wi.segObjIsRed = false;

	  if ( keyType == KtNode || keyType == KtSegment ) {
	    segNode = (Node *) wi.segObj;
	  
	    if (segNode->PrepAsSegment() == false)
	      goto seg_thru_process;

	    if (pSegKey->IsRedSegmentKey()) {
	      pFormatKey = &segNode->slot[RedSegFormat];

	      ADD_DEPEND(pFormatKey);

	      // FIX: Should we do a more general check to make sure that
	      // reserved bits are set properly?  Probably..  grumble.
	  
	      if (pFormatKey->IsType(KtNumber) == false)
		goto seg_malformed;

	      wi.segBlss = REDSEG_GET_BLSS(pFormatKey->nk);
	      if (wi.segBlss > MAX_RED_BLSS)
		goto seg_malformed;

	      // Note that we don't bother to extract the keeper key
	      // location until we need it.
	      wi.segObjIsRed = true;
	      wi.redSeg = segNode;
	      wi.redSegOffset = wi.offset;
	      wi.redSpanBlss = parentBlss - 1;
	    }
	  }

	  ADD_DEPEND(pSegKey);

	  // Loop around again.
	  break;
	}

      case KtNumber:
	wi.segBlss --;
 
	wi.segObj = 0;
	wi.segObjIsRed = false;

	// FIX: When we implement window keys, may need to add depend
	// entries.
	
	switch(pSegKey->nk.value[0] & 3) {
	default:
	  goto invalid_addr;
	}
	break;
	
      default:
	goto seg_malformed;
      }
    }
    /////////////////////////////////////////////////////
    //
    // END key processing logic:
    //
    /////////////////////////////////////////////////////

    // Loop around again.
  }

 seg_thru_process:
  wi.segFault = FC_SegThroughProcess;
  goto fault_exit;

 access_fault:
  wi.segFault =
    wi.capAccess ? FC_CapAccess : FC_DataAccess;
  goto fault_exit;

 invalid_type:
  wi.segFault =
    wi.capAccess ? FC_CapTypeError : FC_DataTypeError;
  goto fault_exit;

 invalid_addr:
  wi.segFault =
    wi.capAccess ? FC_CapInvalidAddr : FC_DataInvalidAddr;
  goto fault_exit;

 bad_seg_depth:
  wi.segFault = FC_SegDepth;
  goto fault_exit;

 seg_malformed:
  wi.segFault = FC_SegMalformed;

 fault_exit:
#ifdef WALK_LOUD
  MsgLog::dprintf(true, "flt proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		  "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d segKey 0x%x\n"
		  "ca %d canCall %d canWrite %d weakRef %d\n"
		  "redSeg 0x%x redOffset 0x%X\n",
		  wi.segObj, wi.segBlss, wi.segObjIsRed,
		  wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		  pSegKey,
		  wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef,
		  wi.redSeg, wi.redSegOffset);
#endif
  if (wi.prompt == false)
    p->InvokeSegmentKeeper(wi.segFault, wi);

  return false;
}


// This version only handles 32 bit offsets and punts to the general
// version when that won't get the job done.
bool
proc_WalkSeg(Process * p, SegWalkInfo& wi, uint32_t stopBlss,
	     PTE* pPTE0, PTE* pPTE1, bool canMerge)
{
  const uint32_t MAX_SEG_DEPTH = 20;
  KernStats.nWalkSeg++;
  Key *pFormatKey = 0;
  Key *pSegKey = 0;

  if (wi.segObj == 0) {
    pSegKey = p->GetSegRoot();
    wi.segBlss = EROS_ADDRESS_BLSS;	// until proven otherwise
    wi.offset = wi.vaddr;
    wi.redSeg = 0;
    wi.redSegOffset = 0;
    wi.redSpanBlss = 0;
    wi.segObjIsRed = false;

    wi.canCall = true;		// until proven otherwise
    wi.canWrite = true;		// until proven otherwise
    wi.weakRef = false;		// until proven otherwise

    goto process_key;
  }
  
  for(;;) {
    if (wi.segBlss <= stopBlss) {
#ifdef WALK_LOUD
      MsgLog::dprintf(false, "ret proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		      "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d segKey 0x%x\n"
		      "ca %d canCall %d canWrite %d weakRef %d\n"
		      "redSeg 0x%x redOffset 0x%X\n",
		      wi.segObj, wi.segBlss, wi.segObjIsRed,
		      wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		      pSegKey,
		      wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef,
		      wi.redSeg, wi.redSegOffset);
#endif
      return true;
    }
      
    if (wi.offset >= 100000000ull)
      return proc_WalkSeg64(p, wi, stopBlss, pPTE0, pPTE1, canMerge);
    
    KernStats.nWalkLoop++;

    /////////////////////////////////////////////////////
    //
    // BEGIN Traversal step logic:
    //
    /////////////////////////////////////////////////////

    {
#ifdef WALK_LOUD
      MsgLog::dprintf(false, "wlk proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		      "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d segKey 0x%x\n"
		      "ca %d canCall %d canWrite %d weakRef %d\n"
		      "redSeg 0x%x redOffset 0x%X\n",
		      wi.segObj, wi.segBlss, wi.segObjIsRed,
		      wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		      pSegKey,
		      wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef,
		      wi.redSeg, wi.redSegOffset);
#endif

#ifndef NDEBUG
      if (wi.segObj->obType == ObType::NtUnprepared)
	MsgLog::fatal("Some node unprepare did not work at UserContext.cxx:%d\n",
		      __LINE__);
#endif
    
      Node* segNode = (Node *) wi.segObj;
	
      assert (segNode->obType == ObType::NtSegment);
	
      uint32_t initialSlots = EROS_NODE_SLOT_MASK;

      if (wi.segObjIsRed) {
	assert (wi.segObjIsRed);
	pFormatKey = &segNode->slot[RedSegFormat];

	// We know that the format key is valid.  It was checked in
	// the key-related checking above, either during the current
	// pass or when building information about the parent page
	// table.  If the format key has since been modified, then
	// the parent page table entry will have been modified, and
	// we will not have gotten here without rechecking it.
	  
	ADD_DEPEND(pFormatKey);

	initialSlots = REDSEG_GET_INITIAL_SLOTS(pFormatKey->nk);
      }

      wi.traverseCount++;	

      uint32_t shiftAmt = BLSS_SHIFT(wi.segBlss);
      uint32_t ndx = ((uint32_t) wi.offset) >> shiftAmt;

      if (ndx > initialSlots)
	goto invalid_addr;

      wi.offset &= BLSS_MASK32(wi.segBlss - 1);

      pSegKey = &segNode->slot[ndx];

      // FIX: At the moment, a weak key forces read-only status in
      // the next traversal.  This is probably a bug, and the formal
      // work shows that removing this check doesn't compromise
      // anything we care about.  Leave it for now, though.

      wi.canWrite &= !wi.weakRef;
    }
    /////////////////////////////////////////////////////
    //
    // END Traversal step logic:
    //
    /////////////////////////////////////////////////////

    if (wi.traverseCount >= MAX_SEG_DEPTH)
      goto bad_seg_depth;

  process_key:

    /////////////////////////////////////////////////////
    //
    // BEGIN key processing logic:
    //
    /////////////////////////////////////////////////////

    {
#ifdef WALK_LOUD
      if (wi.segObj == 0)
	MsgLog::dprintf(false, "key proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		      "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d segKey 0x%x\n"
		      "ca %d canCall %d canWrite %d weakRef %d\n"
		      "redSeg 0x%x redOffset 0x%X\n",
		      wi.segObj, wi.segBlss, wi.segObjIsRed,
		      wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		      pSegKey,
		      wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef,
		      wi.redSeg, wi.redSegOffset);
#endif
      pSegKey->Prepare();
    
      uint8_t keyType = pSegKey->GetType();
    
      // Process everything but the per-object processing first:
      switch(keyType) {
      case KtMisc:
	if ( (pSegKey->subType != MiscKeyType::TimePage) )
	  goto seg_malformed;

	wi.canWrite = false;
	wi.canCall = false;
	wi.weakRef = true;
	wi.segBlss = EROS_PAGE_BLSS;

	wi.segObj = SysTimer::TimePageHdr;
	wi.segObjIsRed = false;
      
	if (wi.writeAccess)
	  goto access_fault;

	ADD_DEPEND(pSegKey);

	return true;

      case KtNode:
      case KtSegment:
      case KtDataPage:
      case KtCapPage:
	{
	  Node* segNode = 0;

	  uint32_t parentBlss = wi.segBlss;
	      
	  wi.segBlss = pSegKey->GetBlss();
	  if (wi.segBlss > MAX_RED_BLSS)
	    goto seg_malformed;
	    
	  // This is NOT the same as the corresponding /if/ on many
	  // systems -- the code generated by this expression contains
	  // no branch.
	  wi.canWrite &= !pSegKey->IsReadOnly();
    
	  if (wi.capAccess && keyType == KtDataPage)
	    goto invalid_type;
	  
	  if (wi.capAccess == 0 && keyType == KtCapPage)
	    goto invalid_type;
	  
	  if (!wi.canWrite && wi.writeAccess)
	    goto access_fault;
	
	  wi.canCall &= !pSegKey->IsNoCall();
	
	  wi.weakRef |= pSegKey->IsWeak();
    
	  wi.segObj = pSegKey->ok.pObj;
	  wi.segObjIsRed = false;

	  if ( keyType == KtNode || keyType == KtSegment ) {
	    segNode = (Node *) wi.segObj;
	  
	    if (segNode->PrepAsSegment() == false)
	      goto seg_thru_process;

	    if (pSegKey->IsRedSegmentKey()) {
	      pFormatKey = &segNode->slot[RedSegFormat];

	      ADD_DEPEND(pFormatKey);

	      // FIX: Should we do a more general check to make sure that
	      // reserved bits are set properly?  Probably..  grumble.
	  
	      if (pFormatKey->IsType(KtNumber) == false)
		goto seg_malformed;

	      wi.segBlss = REDSEG_GET_BLSS(pFormatKey->nk);
	      if (wi.segBlss > MAX_RED_BLSS)
		goto seg_malformed;

	      // Note that we don't bother to extract the keeper key
	      // location until we need it.
	      wi.segObjIsRed = true;
	      wi.redSeg = segNode;
	      wi.redSegOffset = wi.offset;
	      wi.redSpanBlss = parentBlss - 1;
	    }
	  }

	  ADD_DEPEND(pSegKey);

	  // Loop around again.
	  break;
	}

      case KtNumber:
	wi.segBlss --;
 
	wi.segObj = 0;
	wi.segObjIsRed = false;

	switch(pSegKey->nk.value[0] & 3) {
	default:
	  goto invalid_addr;
	}
	break;
	
      default:
	goto seg_malformed;
      }
    }
    /////////////////////////////////////////////////////
    //
    // END key processing logic:
    //
    /////////////////////////////////////////////////////

    // Loop around again.
  }

 seg_thru_process:
  wi.segFault = FC_SegThroughProcess;
  goto fault_exit;

 access_fault:
  wi.segFault =
    wi.capAccess ? FC_CapAccess : FC_DataAccess;
  goto fault_exit;

 invalid_type:
  wi.segFault =
    wi.capAccess ? FC_CapTypeError : FC_DataTypeError;
  goto fault_exit;

 invalid_addr:
  wi.segFault =
    wi.capAccess ? FC_CapInvalidAddr : FC_DataInvalidAddr;
  goto fault_exit;

 bad_seg_depth:
  wi.segFault = FC_SegDepth;
  goto fault_exit;

 seg_malformed:
  wi.segFault = FC_SegMalformed;

 fault_exit:
#ifdef WALK_LOUD
  MsgLog::dprintf(true, "flt proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		  "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d segKey 0x%x\n"
		  "ca %d canCall %d canWrite %d weakRef %d\n"
		  "redSeg 0x%x redOffset 0x%X\n",
		  wi.segObj, wi.segBlss, wi.segObjIsRed,
		  wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		  pSegKey,
		  wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef,
		  wi.redSeg, wi.redSegOffset);
#endif
  if (wi.prompt == false)
    p->InvokeSegmentKeeper(wi.segFault, wi);

  return false;
}
#if 0
// Note that WalkSeg carries essentially no local state because it is
// called in progressive fashion.  All state must therefore reside in
// the SegWalkInfo structure.  This is actually a blessing, as it
// reduces the number of passed arguments.  The pPTE0, pPTE1 and
// canMerge flags should probably be added to the SegWalkInfo
// structure for this reason.
bool
proc_WalkSeg(Process *p, SegWalkInfo& wi, uint32_t stopBlss,
	     PTE* pPTE0, PTE* pPTE1, bool canMerge)
{
  const MAX_SEG_DEPTH = 20;
#if 0
  PTE* mappingRoot = (PTE*) &p->fixRegs.MappingTable;
#endif

  KernStats.nWalkSeg++;

  for(;;) {
    KernStats.nWalkLoop++;
    
#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr)
      Check::Consistency("Top proc_WalkSeg");
#endif
    uint32_t initialSlots = EROS_NODE_SLOT_MASK;
    Key *pFormatKey = 0;	// needed for depend construction

    if (wi.traverseCount >= MAX_SEG_DEPTH) {
      wi.segFault = FC_SegDepth;
      break;
    }

    if ( wi.pSegKey->GetType() == KtNumber ) {
      wi.segBlss = wi.segBlss - 1;
#if 0
      MsgLog::printf("WalkSubSeg %d: processing number key\n",
		     wi.traverseCount); 
#endif
      // Note that the 'segBlss' value used here is from the previous
      // iteration.  If the root key was a number key we won't have
      // passed the test above, so segBLSS is well-defined.
      
      switch (wi.pSegKey->nk.value[0] & 3) {
      case 3:			// background window key
	{
	  if (wi.backgroundKey == 0) {
#if 1
	    MsgLog::printf("  NO BACKGROUND KEY\n");
#endif
	    wi.segFault =
	      wi.capAccess ? FC_CapInvalidAddr : FC_DataInvalidAddr;
	    break;
	  }
	  
	  // Note that we mask out the information bits in the number key:
	  uint64_t bkOffset =
	    ((uint64_t) wi.pSegKey->nk.value[0] & ~EROS_PAGE_MASK) |
	    (uint64_t) wi.pSegKey->nk.value[1];
	    
	  // This number key has an "expected" BLSS which is one less
	  // than it's containing node.  We computed that by
	  // decrementing wi.segBlss above.  The background window
	  // offset must be an integral multiple of the expected BLSS.
	  // Verify this by verifying that the necessary low-order
	  // bits of the offset are zero.

	  // FIX: perhaps off by one?
	  uint64_t offsetMask = LSS::Mask(wi.segBlss-1);

	  uint64_t maskedOffset = bkOffset & offsetMask;

	  if (maskedOffset != 0llu) {
	    wi.segFault =
	      wi.capAccess ? FC_CapInvalidAddr : FC_DataInvalidAddr;
	    break;
	  }
	  
	  // Extract the portion of the current segment offset that
	  // falls within this window, and replace the upper bits with
	  // the background window offset:
	  
	  wi.offset &= offsetMask;
	  wi.offset |= bkOffset;

	  ADD_DEPEND(wi.pSegKey);
	  // Do not bump traverseCount for the BG window key, since
	  // this is 1-1 with the corresponding red segment
	  
	  // Offset is appropriate multiple.  Update offset and
	  // background window key and keep walking...

	  wi.pSegKey = wi.backgroundKey;
	  wi.backgroundKey = 0;
	  wi.backgroundBlss = 0;

#if 0
	  MsgLog::printf("  bk win, addr 0x%08x bkoffset = 0x%08x new offset=0x%08x\n",
			 wi.vaddr, bkOffset.w[0], wi.offset.w[0]);
#endif
	  continue;		// continue the FOR loop!
	}	  
      default:
	wi.segFault = wi.capAccess ? FC_CapInvalidAddr : FC_DataInvalidAddr;
	break;
      }

      if (wi.segFault != FC_NoFault)
	break;
    }
    
    if (wi.pSegKey->IsSegModeType() == false) {
#if 0
      MsgLog::printf(" Not segmode\n");
#endif
      wi.pSegKey->Print();
      wi.segFault = FC_SegMalformed;
      MsgLog::dprintf(true, "seg key not segmode\n");
      break;
    }

    wi.segBlss = wi.pSegKey->GetBlss();

    // Do this with a variable. /wi.canWrite/ can become false below
    // if the key to the containing node has the weak attribute.
    if ( wi.pSegKey->IsReadOnly() )
      wi.canWrite = false;
    
    if ( wi.canWrite == false && wi.writeAccess) {
      wi.segFault =
	wi.capAccess ? FC_CapAccess : FC_DataAccess;
      break;
    }

    if ( wi.pSegKey->IsNoCall() )
      wi.canCall = false;

    if ( wi.pSegKey->IsWeak() ) {
      wi.weakRef = true;
      wi.canCall = false;
    }
    
#if defined(DBG_WILD_PTR)
    assert ( wi.pSegKey->IsValid() );
#endif
    wi.pSegKey->Prepare();
#if defined(DBG_WILD_PTR)
    assert ( wi.pSegKey->IsValid() );
#endif

    
#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr)
      Check::Consistency("proc_WalkSeg after key prep");
#endif

    Node* segNode = 0;
    
    // The key might be a page key or a number key, so we need to
    // check more carefully here:
    
    if ( wi.pSegKey->IsSegKeyType() ) {
      segNode = (Node *) wi.pSegKey->GetObjectPtr();
      
#if defined(DBG_WILD_PTR)
      segNode->Validate();
#endif

      if (segNode->PrepAsSegment() == false) {
	wi.segFault = FC_SegThroughProcess;
	break;
      }

#if defined(DBG_WILD_PTR)
      segNode->Validate();
#endif
    }

    // If this is a kept segment, the true segment BLSS is embedded in
    // the format key, and we must pick up the background key and
    // keeper key as well:
    if ( wi.pSegKey->IsRedSegmentKey() ) {
#if 1
      // This shouldn't happen.  By this point we should be
      // considering only proper segmode keys, properly formed pages
      // and numbers should never have their red segment bit set.
      if (wi.pSegKey->IsSegKeyType() == false)
	MsgLog::fatal(" Malformed page or number key\n");
#endif

      pFormatKey = &(*segNode)[RedSegFormat];

      // Second part of this test is checking for valid keeper
      // invocation convention.
      if ( pFormatKey->IsValidFormatKey() == false ) {
#ifdef DDB
	extern void db_eros_print_key(Key&);
	db_eros_print_key(*pFormatKey);
#endif
	MsgLog::dprintf(true, "Bad format key!\n");

	wi.segFault = FC_SegMalformed;
	break;
      }

      // FIX: following is vulnerable to excessive BLSS values!
      
      wi.segBlss = REDSEG_GET_BLSS(pFormatKey->nk);
      initialSlots = REDSEG_GET_INITIAL_SLOTS(pFormatKey->nk);
#if 0
      MsgLog::dprintf(true,
		      " red segBlss = %d pFormatKey=0x%08x"
		      " oid=0x%x offset=0x%08x\n",
		      wi.segBlss, pFormatKey, (unsigned long)
		      p->procRoot->oid,
		      (uint32_t) wi.offset); 
#endif

      // FIX: What to do if these are not proper keys?
      
      uint32_t keeperSlot = REDSEG_GET_KPR_SLOT(pFormatKey->nk);
      if (wi.canCall && keeperSlot != RedSegFormat) {
	Key *newKprKey = &(*segNode)[keeperSlot];

	if (newKprKey->IsType(KtStart) == false)
	  newKprKey = &Key::ZeroNumberKey;
	
	wi.segKeeper = newKprKey;
	wi.keptSeg = wi.pSegKey;
	wi.keptSegOffset = wi.offset;

#if 0
	wi.keptSegBlss = wi.segBlss;
	wi.keptSegInitialSlots = initialSlots;
#endif
#if defined(DEBUG_SUBSEG)
	MsgLog::dprintf(true, "cs: Keptseg 0x%08x offset 0x%08x kpr 0x%08x\n",
			wi.keptSeg, wi.keptSegOffset.w[0], wi.segKeeper);
#endif
      }
#if 1
      else {
	MsgLog::dprintf(true, "no-call keeper in keptseg at 0x%08x\n",
			wi.keptSeg);
      }
#endif

      uint32_t bgSlot = REDSEG_GET_BG_SLOT(pFormatKey->nk);
      // Check for background key also:
      if (bgSlot != RedSegFormat) {
	Key *newBkKey = &(*segNode)[bgSlot];
	// FIX: Should we allow zero number key?
	if (newBkKey->IsSegModeType() == false) {
	  newBkKey->Print();
	  MsgLog::dprintf(true, "New bk key not segmode\n");
	  wi.segFault = FC_SegMalformed;
	  break;
	}
	
	wi.backgroundKey = newBkKey;
	wi.backgroundBlss = wi.segBlss - 1;

	// There is no need to remember here that the background key
	// is read-only if the red segment key is weak.  /wi.weakRef/
	// is already /true/, which prevents non-weak cap reads in the
	// background segment.  The only way through the background
	// key at this point is to load a background window key from
	// some node within the foreground segment.  When that load is
	// performed, /wi.canWrite/ will be set to /false/, which will
	// prevent writes in the background segment.
#if 0
	MsgLog::printf("Background key is oid ");
	if ( wi.backgroundKey->IsPrepared() )
	  MsgLog::print(wi.backgroundKey->GetKeyOid());
	else
	  MsgLog::print(wi.backgroundKey->unprep.oid);
	MsgLog::printf("\n");
#endif
      }
    }

#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr)
      Check::Consistency("proc_WalkSeg below IsRedSegmentKey body");
#endif

#if defined(DBG_WILD_PTR)
    if (segNode) segNode->Validate();
#endif
    // Only add the depend entry when we are committed to traversing
    // this slot!
    
    ADD_DEPEND(wi.pSegKey);
    wi.traverseCount++;
    if (pFormatKey) {
      ADD_DEPEND(pFormatKey);
      pFormatKey = 0;
    }

#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr)
      Check::Consistency("below format key depend add");
#endif

    // Do not bump traverseCount for the format key, since this is 1-1
    // with the corresponding red segment

#if 1
    uint32_t shiftAmt =
      (wi.segBlss - EROS_PAGE_BLSS - 1) * EROS_NODE_LGSIZE + EROS_PAGE_ADDR_BITS;

    uint32_t ndx = wi.offset >> shiftAmt;
    uint32_t nndx = LSS::SlotNdx(wi.offset, wi.segBlss);

    /* This is a sanity check.  Do not expect them to match if the
       address was out of bounds, because the bignum logic in some
       cases cheats by returning a known out of bound constant */
    if (ndx != nndx && wi.segBlss != EROS_PAGE_BLSS && ndx < EROS_NODE_SIZE)
      MsgLog::dprintf(true,
		      "offset = 0x%08x blss=%d ndx = 0x%x, nndx = 0x%x\n",
		      (uint32_t) wi.offset, wi.segBlss, ndx, nndx);
#else
    uint32_t ndx = blssSlotNdx(wi.offset, wi.segBlss);
#endif

#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr)
      Check::Consistency("Above ndx > initialSlots check");
#endif

    if (ndx > initialSlots) {
#if 0
      if (wi.pSegKey->IsRedSegmentKey())
	MsgLog::printf(" oversized red seg path (ndx=0x%08x)\n", ndx);
      else
	MsgLog::printf(" bad short circuit path (ndx=0x%08x)\n", ndx);
      MsgLog::printf(" address=0x%08x offset=0x%08x oid=", wi.vaddr,
			(uint32_t) wi.offset);
      MsgLog::print(wi.pSegKey->GetKeyOid());
      MsgLog::dprintf(true," blss=%d kblss=%d\n", wi.segBlss,
		     wi.pSegKey->GetBlss());
#endif
      wi.segFault = wi.capAccess ? FC_CapInvalidAddr : FC_DataInvalidAddr;
      break;
    }

    // We may in fact have found the proper subsegment, but only if
    // the offset had zeros in the skipped levels, in which case the
    // ndx test will have succeeded above
    if (wi.segBlss <= stopBlss) {
#ifdef DBG_WILD_PTR
      if (dbg_wild_ptr)
	Check::Consistency("WalkSeg: found");
#endif
      return true;
    }

#if defined(DEBUG_SUBSEG)
    {
      uint32_t shift_bits = (wi.segBlss - EROS_PAGE_BLSS - 1) *
	EROS_NODE_LGSIZE + EROS_PAGE_ADDR_BITS;
      
      MsgLog::printf(" thrd 0x%X walking through ndx=0x%x (0x%x >> %d)\n",
		     p->oid, ndx, (uint32_t) wi.offset, shift_bits);
    }
#endif

    // FIX: perhaps off by one?
    uint64_t offsetMask = LSS::Mask(wi.segBlss-1);
    wi.offset &= offsetMask;
    wi.offset &= offsetMask;

    wi.pSegKey = &(*segNode)[ndx];
    // a weak key forces read-only status in the next traversal:
    if (wi.weakRef)
      wi.canWrite = false;

#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr)
      Check::Consistency("Bottom proc_WalkSeg");
#endif
  }
  
#if 0
  MsgLog::printf("WalkSeg: out: pKey=0x%08x\n", wi.pSegKey);
#endif

  if (wi.prompt == false) {
    p->InvokeSegmentKeeper(wi.segFault, wi);
  }
  
  return false;
}
#endif

void
Process::InvokeSegmentKeeper(uint32_t code, SegWalkInfo& wi)
{
  Key *keeperKey = 0;
  Key *pFormatKey = 0;
  uint32_t keptBlss = 0;
  
  if (wi.redSeg) {
    // We know this key is valid or /wi.redSeg/ would not have been set.
    pFormatKey = &wi.redSeg->slot[RedSegFormat];

    uint32_t keeperSlot = REDSEG_GET_KPR_SLOT(pFormatKey->nk);

    if (wi.canCall && keeperSlot != RedSegFormat) {
      keeperKey = &wi.redSeg->slot[keeperSlot];

      if (keeperKey->IsType(KtStart) == false) {
	// THIS IS VALID, in the sense that it serves to suppress
	// reporting to the process keeper.
	keeperKey = &Key::ZeroNumberKey;
      }
    }


    keptBlss = REDSEG_GET_BLSS(pFormatKey->nk);
  }
  
  // Ensure retry on yield.  All segment faults are fast-path
  // restartable.
  SetFault(code, wi.vaddr, false);

  if (keeperKey == 0) {
    // Yielding here will cause the thread to resume with a non-zero
    // fault code in the scheduler, at which point it will be shunted
    // off to the process keeper.
#ifndef NDEBUG
    MsgLog::dprintf(true, "InvokeMyKeeper(): Thread takes seg flt with no kpr\n");
#endif
    Thread::Current()->Yield();
  }

  assert(keeperKey);

  // If this was a memory fault, clear the PF_Faulted bit.  This
  // allows us to restart the instruction properly without invoking
  // the process keeper in the event that we are forced to yield in the
  // middle of preparing the keeper key or calling InvokeMyKeeper;
  processFlags &= ~PF_Faulted;
  
  // The only policy decision to make here is whether to pass a key
  // argument.
  Key *keyToPass = &Key::ZeroNumberKey;
  
  if (REDSEG_GET_SENDNODE(pFormatKey->nk) == REDSEG_SEND_NODE) {
    // This could be a call driven by SetupExitString(), in which the
    // original invocation was to a red segment key with the node
    // passing option turned on, so do not reuse scratchKey.

#if 1
    inv.redNodeKey.InitType(KtNode);
    inv.redNodeKey.SetPrepared();
    wi.redSeg->TransLock();
    inv.redNodeKey.subType = 0;
    inv.redNodeKey.keyData = BLSS::RedSeg;
    inv.redNodeKey.ok.pObj = wi.redSeg;
    inv.redNodeKey.ok.next = wi.redSeg->kr.next;
    inv.redNodeKey.ok.prev = (KeyRing*) wi.redSeg;
    wi.redSeg->kr.next = (KeyRing *) &inv.redNodeKey;
    inv.redNodeKey.ok.next->prev = (KeyRing*) &inv.redNodeKey;
#else
    inv.redNodeKey.InitType(KtNode);
    inv.redNodeKey.SetUnprepared();
    inv.redNodeKey.subType = 0;
    inv.redNodeKey.keyData = BLSS::RedSeg;
    inv.redNodeKey.unprep.oid = wi.redSeg->ob.oid;
    inv.redNodeKey.unprep.count = wi.redSeg->ob.allocCount;
#endif
    inv.flags |= INV_REDNODEKEY;

    keyToPass = &inv.redNodeKey;
  }

  // NOTE DELICATE ISSUE
  //
  //   If the keeper is a resume key we must mark the containing
  //   segment node dirty.  See the comments in kern_Invoke.cxx for an
  //   explanation.
  //
  //   In practice, if you use a resume key in a keeper slot you are a
  //   bogon anyway, so I'm not real worried about it...
  //
  //   The following may yield, but note that all segment faults are
  //   restartable, so it's okay.  There is no need for a similar
  //   check in InvokeProcessKeeper(), because process root is always
  //   dirty.

  if (keeperKey->IsType(KtResume))
    wi.redSeg->MakeObjectDirty();

#if defined(WALK_LOUD) || 0
  {
    static uint64_t last_offset = 0x0ll;
    if (wi.redSegOffset != last_offset) {
      MsgLog::dprintf(false, "Invoking segment keeper. redseg 0x%x"
		      " base 0x%X last 0x%X\n",
		      wi.redSeg, wi.redSegOffset, last_offset);
      last_offset = wi.redSegOffset;
    }
  }
#endif
  InvokeMyKeeper(OC_SEGFAULT, code,
		 (uint32_t) wi.redSegOffset, (uint32_t) (wi.redSegOffset>>32),
		 keeperKey, keyToPass,
		 0, 0);
  
  //  MsgLog::fatal("Seg keeper should be invoked here!\n");
}

void
Process::InvokeProcessKeeper()
{
  Registers regs;

#if 0
  MsgLog::dprintf(false, "Fetching register values for InvokeProcessKeeper()\n");
#endif
  GetRegs32(regs);

#if 0
  MsgLog::dprintf(false, "  ExNo: 0x%08x, Error: 0x%08x  Info: 0x%08x  FVA 0x%08x\n",
		  fixRegs.ExceptNo,
		  fixRegs.Error,
		  faultInfo,
		  fixRegs.ExceptAddr);
#endif
  // Must be unprepared in case we throw()!
  
  Key processKey;
  processKey.InitType(KtProcess);
  processKey.SetUnprepared();
  processKey.subType = 0;
  processKey.keyData = 0;	// process key always 0
  processKey.unprep.oid = procRoot->ob.oid;
  processKey.unprep.count = procRoot->ob.allocCount;

  // Guarantee that prepared resume key will be in dirty object -- see
  // comment in kern_Invoke.cxx:
  assert (procRoot->IsDirty());
  
  InvokeMyKeeper(OC_PROCFAULT, 0, 0, 0, &procRoot->slot[ProcKeeper],
		 &processKey, (uint8_t *) &regs, sizeof(regs));
}

#ifdef ProcGenRegs
void
#ifdef NDEBUG
Process::FlushAnnexSlot(Node * /* nd */, uint32_t /* whichSlot */)
#else
Process::FlushAnnexSlot(Node *nd, uint32_t /* whichSlot */)
#endif
{
  assert(annexNode);
  assert(annexNode == nd);

  if ( (hazards & hz::FloatRegs) == 0 )
    FlushFloatRegs();

  return;
}
#endif

void
Process::FlushProcessSlot(uint32_t whichKey)
{
  assert(procRoot);
  
  switch (whichKey) {
  case ProcGenKeys:
    assert ((hazards & hz::KeyRegs) == 0);
    FlushKeyRegs();
    break;
#if EROS_NODE_SIZE == 16
  case ProcGenRegs:
    assert ((hazards & hz::FloatRegs) == 0);

    FlushFloatRegs();
    break;
#endif

  case ProcAddrSpace:
    Depend::InvalidateKey(&(*procRoot)[whichKey]);
    (*procRoot)[whichKey].UnHazard();
    hazards |= hz::AddrSpace;
    assert(fixRegs.MappingTable == 0);
    saveArea = 0;

    break;

  default:
#if EROS_NODE_SIZE == 32
    if ( (hazards & hz::FloatRegs) == 0 )
      FlushFloatRegs();
#endif
    
    assert ((hazards & hz::DomRoot) == 0);
    FlushFixRegs();

    break;
  }

  saveArea = 0;
}

void
Process::FlushKeySlot(uint32_t /* whichKey */)
{
  /* for now, do nothing... */
}

#if 0
void
Process::SetFault(uint32_t code, uint32_t info, bool needRevalidate)
{
  faultCode = code;
  faultInfo = info;

  // For FC_MalformedProcess, should use SetMalformed() instead!!
  assert(faultCode != FC_MalformedProcess);
  
#if 1
  if (faultCode)
    processFlags |= PF_Faulted;
  else
    processFlags &= ~PF_Faulted;
#endif
  
#if 0
  MsgLog::printf("Fault code set to %d info=0x%x\n", code, info);
#endif

#if 0
  // It is now sufficient to set the PF_Faulted flag, which has the
  // effect of rendering the process non-runnable without totally
  // zapping the entire universe.
  
  if (needRevalidate)
    NeedRevalidate();
#endif

#if 0
  Debugger();
#endif
}
#endif

// On entry. inv.rcv.code == RC_OK
bool
Process::GetRegs32(struct Registers& regs)
{
  assert (IsRunnable());

  // Following is x86 specific.  On architectures with more annex
  // nodes this would need to check those too:
  
  if (hazards & hz::DomRoot) {
    return false;
  }
  
  regs.arch   = ARCH_I386;
  regs.len    = sizeof(regs);
  regs.pc     = fixRegs.EIP;
  regs.sp     = fixRegs.ESP;
  regs.faultCode = faultCode;
  regs.faultInfo = faultInfo;
  regs.domState = runState;
  regs.domFlags = processFlags;

  regs.EDI    = fixRegs.EDI;
  regs.ESI    = fixRegs.ESI;
  regs.EBP    = fixRegs.EBP;
  regs.EBX    = fixRegs.EBX;
  regs.EDX    = fixRegs.EDX;
  regs.ECX    = fixRegs.ECX;
  regs.EAX    = fixRegs.EAX;
  regs.EFLAGS = fixRegs.EFLAGS;
  regs.CS     = fixRegs.CS;
  regs.SS     = fixRegs.SS;
  regs.ES     = fixRegs.ES;
  regs.DS     = fixRegs.DS;
  regs.FS     = fixRegs.FS;
  regs.GS     = fixRegs.GS;

  return true;
}

// On entry. inv.rcv.code == RC_OK
bool
Process::SetRegs32(Registers& regs)
{
#if 0
  MsgLog::dprintf(true, "ctxt=0x%08x: Call to SetRegs32\n", this);
#endif

  assert (IsRunnable());

  if (hazards & hz::DomRoot) {
    MsgLog::dprintf(true, "ctxt=0x%08x: No root regs\n", this);
    return false;
  }
  
  // Done with len, architecture
  fixRegs.EIP    = regs.pc;
#if 0
  nextPC         = regs.pc;
#endif
  fixRegs.ESP    = regs.sp;
  faultCode      = regs.faultCode;
  faultInfo      = regs.faultInfo;
  runState       = regs.domState;
  processFlags    = regs.domFlags;

  fixRegs.EDI    = regs.EDI;
  fixRegs.ESI    = regs.ESI;
  fixRegs.EBP    = regs.EBP;
  fixRegs.EBX    = regs.EBX;
  fixRegs.EDX    = regs.EDX;
  fixRegs.ECX    = regs.ECX;
  fixRegs.EAX    = regs.EAX;
  fixRegs.EFLAGS = regs.EFLAGS;
  fixRegs.CS     = regs.CS;
  fixRegs.SS     = regs.SS;
  fixRegs.ES     = regs.ES;
  fixRegs.DS     = regs.DS;
  fixRegs.FS     = regs.FS;
  fixRegs.GS     = regs.GS;

#if 0
  MsgLog::dprintf(true, "SetRegs(): ctxt=0x%08x: EFLAGS now 0x%08x\n", this, fixRegs.EFLAGS);
#endif

  NeedRevalidate();
    
  return true;
}
