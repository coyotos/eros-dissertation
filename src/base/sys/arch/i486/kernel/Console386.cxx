/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Console.hxx>
#include <kerninc/WallClock.hxx>
#include "io.h"

// static void ClearScreen();
static void ClearMessageArea();

// This works for VGA modes 2 and 3, which covers both monochrome and
// color situations.  If you have a CGA, too bad.

// Note that we lie about SCREEN_ROWS.  The kernel currently steels
// the very bottom line of the display for status info.

#define SCREEN_START PTOV(0xb8000u)
#define TEXT_ROWS 24
#define SCREEN_ROWS 25
#define SCREEN_COLS 80
#define SCREEN_END (SCREEN_START + 2*SCREEN_COLS*SCREEN_ROWS)
#define MESSAGE_START (SCREEN_START + 2*SCREEN_COLS*TEXT_ROWS)
#define MESSAGE_CHAR_OFFSET 0
#define MESSAGE_TWIDDLE_OFFSET 2
#define MESSAGE_STRING_OFFSET 12

enum AsciiChars {
  Space = 0x20,
};
    
enum VGAColors {
  Black = 0,
  Blue = 1,
  Green = 2,
  Cyan = 3,
  Red = 4,
  Magenta = 5,
  Brown = 6,
  White = 7,
  Gray = 8,
  LightBlue = 9,
  LightGreen = 10,
  LightCyan = 11,
  LightRed = 12,
  LightMagenta = 13,
  LightBrown = 14,	// yellow
  BrightWhite = 15,

  // Combinations used by the console driver:
  WhiteOnBlack = 0x7,
  blank = 0,
};

uint16_t *screen = (uint16_t *) SCREEN_START;

AnsiConsole TheConsole(24, 80);

uint32_t StartAddressReg = 0;	// as seen by the display hardware

void
AnsiConsole::ShowCursorAt(uint32_t pos)
{
  // Show the cursor at the next output position:
  uint32_t cursorAddress = pos;

  cursorAddress += StartAddressReg;
  
  old_outb(0x3d4, 0xe);		// cursor loc hi register addr
  old_outb(0x3d5, (cursorAddress >> 8) & 0xffu);
  old_outb(0x3d4, 0xf);		// cursor loc lo register addr
  old_outb(0x3d5, cursorAddress & 0xffu);
}

void
AnsiConsole::Scroll(uint32_t startPos, uint32_t endPos, int amount)
{
  if (amount > 0) {
    uint32_t gap = amount;
    for (uint32_t p = startPos + gap; p < endPos; p++)
      screen[p] = screen[p - gap];

    for (uint32_t p = startPos; p < startPos + gap; p++)
      screen[p] = (WhiteOnBlack << 8);
  }
  else {
    uint32_t gap = -amount;

    for (uint32_t p = startPos; p < endPos - gap; p++)
      screen[p] = screen[p + gap];

    for (uint32_t p = endPos - gap; p < endPos; p++)
      screen[p] = (WhiteOnBlack << 8);
  }
}

#if 0
static void
ClearScreen()
{
  uint16_t *from = (uint16_t *) SCREEN_START;
  uint16_t *to = from + (TEXT_ROWS*SCREEN_COLS);

  while (from != to)
    *from++ = (WhiteOnBlack << 8);

  UpdateCursor();
}
#endif

static void
ClearMessageArea()
{
  uint16_t *from = screen + (TEXT_ROWS * SCREEN_COLS);

  for (int i = 0; i < SCREEN_COLS; i++)
    *from++ = (WhiteOnBlack << 8);
}

void
Console::Init()
{
  old_outb(0x3d4, 0xc);		// start address hi register addr
  uint8_t hi = inb(0x3d5);
  old_outb(0x3d4, 0xd);		// start address lo register addr
  uint8_t lo = inb(0x3d5);

  StartAddressReg = hi << 8 | lo;

  ClearMessageArea();
  TheConsole.Reset();
  TheConsole.ClearScreen();
  
  MsgLog::RegisterSink(Console::Put);
}

void
Console::ShowMessage(char *msg)
{
  uint16_t *msgLine = (uint16_t*) MESSAGE_START;
  msgLine += MESSAGE_STRING_OFFSET;

  int i;
  for (i = 0; i < (SCREEN_COLS - 10); i++) {
    if (msg[i] == '\0')
      break;
    msgLine[i] = msg[i] | (WhiteOnBlack << 8);
  }

  for (; i < (SCREEN_COLS - 10); i++)
    msgLine[i] = ' ' | (WhiteOnBlack << 8);
}

// We implement 8 twiddlers (basically rotating console LEDS) to test
// the scheduler:

static int tw_pos[8];
static char tw_chars[] = "|/-\\";

void
Console::ResetTwiddle(uint32_t i)
{
  uint16_t *msgLine = (uint16_t*) MESSAGE_START;
  msgLine += MESSAGE_TWIDDLE_OFFSET;

  assert (i < 8);
  uint16_t* twiddle = &msgLine[i];

  *twiddle = ' ' | (WhiteOnBlack << 8);
  tw_pos[i] = 0;
}

void
Console::Twiddle(uint32_t i)
{
  uint16_t *msgLine = (uint16_t*) MESSAGE_START;
  msgLine += MESSAGE_TWIDDLE_OFFSET;

  assert (i < 8);
  uint16_t* twiddle = &msgLine[i];

  *twiddle = tw_chars[tw_pos[i]++] | (WhiteOnBlack << 8);
  tw_pos[i] %= (sizeof(tw_chars) - 1);
}

static uint16_t tw_colors[] = { White, Cyan, Magenta, Blue };

void
Console::ShowTwiddleChar(char c, uint32_t i)
{
  uint16_t *msgLine = (uint16_t*) MESSAGE_START;
  msgLine += MESSAGE_TWIDDLE_OFFSET;

  assert (i < 8);
  if (((unsigned char) msgLine[i]) != (unsigned char) c)
    tw_pos[i] = 0;

  msgLine[i] = c | (tw_colors[tw_pos[i]++] << 8);
  tw_pos[i] %= (sizeof(tw_colors) - 1);
}

void
Console::ShowMsgChar(char c)
{
  uint16_t *msgLine = (uint16_t*) MESSAGE_START;
  msgLine += MESSAGE_CHAR_OFFSET;

  msgLine[0] = c | (WhiteOnBlack << 8);
}

void
Console::Put(uint8_t c)
{
  TheConsole.PutLog(c);
}

uint8_t
Console::Get()
{
  extern int GetCharFromKbd();
  return GetCharFromKbd();
}

void
Console::SetPoll(bool onoff)
{
  extern int kbd_polling;
  if (onoff)
    kbd_polling++;
  else
    kbd_polling--;
}

void
AnsiConsole::SetPosition(uint32_t pos, uint8_t c, Attr::Type attr)
{
  uint32_t vgaAttrs = WhiteOnBlack << 8;

  switch (attr) {
  default:
    break;
  }

  screen[pos] = ((uint16_t) c) | vgaAttrs;
}

void
AnsiConsole::Beep()
{
}
