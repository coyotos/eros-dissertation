#ifndef __GDT_HXX__
#define __GDT_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "Segment.hxx"

const int GDT_ENTRIES = SegEntry::NUM_SEGENTRY;

class GDT {
  static uint32_t GdtTable[GDT_ENTRIES * 2];
  static uint32_t GDTdescriptor[2];
  static void lgdt();
  static void ReloadSegRegs();
public:

  static void Init();
  static void SetupTSS(SegEntry::Name, uint32_t base);
  static void SetupPageSegment(SegEntry::Name entry, uint32_t vbase, uint32_t nPages);
#ifdef DDB
  static void ddb_dump();
#endif
};

inline void GDT::lgdt()
{
#ifdef __ELF__
  __asm__ __volatile__("lgdt _3GDT.GDTdescriptor"
		       : /* no output */
		       : /* no input */
		       : "memory");
#else
  __asm__ __volatile__("lgdt __3GDT$GDTdescriptor"
		       : /* no output */
		       : /* no input */
		       : "memory");
#endif
}
#endif // __GDT_HXX__
