/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Drivers for 386 protection faults

#include <eros/target.h>
#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Debug.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/util.hxx>
#include <kerninc/Process.hxx>
#include "IDT.hxx"

#ifdef DDB
bool continue_user_bpt;
#endif

void
BptTrap(fixregs_t *sa)
{
  sa->EIP -= 0x1;		// correct the PC!

  if ( sa_IsKernel(sa)) {
#ifdef DDB
    extern void kdb_trap(int, int, fixregs_t *);
    kdb_trap(sa->ExceptNo, sa->Error, sa);
    return;
#else
    halt('a');
#endif
  }

#ifdef DDB
  continue_user_bpt = false;
  MsgLog::dprintf(true, "Domain takes BPT trap. error=%d eip=0x%08x\n",
		  sa->Error, sa->EIP);
  if (continue_user_bpt) {
    sa->EIP += 0x1;
    return;
  }
#else
  MsgLog::printf("Domain takes BPT trap. error=%d eip=0x%08x\n",
	      sa->Error, sa->EIP);
  // sa->Dump();
#endif
  ((Process*) Thread::CurContext())->SetFault(FC_BreakPointTrap,
					      sa->EIP, false);
}
