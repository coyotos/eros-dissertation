/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <kerninc/kernel.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/util.hxx>
#include <kerninc/ObjectHeader.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/Invocation.hxx>
#include "lostart.hxx"

// #define THREAD386DEBUG

extern "C" {
  void kernel_thread_yield();
};

void
Thread::DirectedYield(bool verbose)
{
  assert (InvocationCommitted == false);
  
#ifdef DBG_WILD_PTR
  // This is VERY problematic, because kernel threads can now be preempted!
  if (dbg_wild_ptr)
    Check::Consistency("Before Yield");
#endif
  
  assert ( IsUser() == false );
  
  if (verbose)
    MsgLog::printf("Thread 0x%x yields state %d\n", this, state);

  extern Thread *pIdleThread;
  if (Thread::Current() == pIdleThread)
    assert( (Thread::Current()->state == Thread::Running) ||
	    (Thread::Current()->state == Thread::Ready) );
    
  if (Thread::Current() != this)
    MsgLog::fatal("Bad current thread! Thread::Current()=0x%08x, this=0x%08x\n",
	       Thread::Current(), this);
  
  // Cannot verify interrupts disabled here -- might be preemption.
    
#if defined(DDB)
  {
    extern bool ddb_uyield_debug;
    if ( ddb_uyield_debug )
      MsgLog::dprintf(true, "User thread 0x%08x yields\n",
		      Thread::Current()); 
  }
#endif

#if 0
  ObjectHeader::ReleaseThreadResources(this);
  ObjectCache::ReleaseUncommittedIoPageFrames();
#endif

#ifdef THREAD386DEBUG
  MsgLog::printf("Process 0x%08x yields -- longjmp to 0x%08x\n",
		 this, UserThreadRecoveryBlock[0].pc);
#endif

  __asm__("int $0x30");

  if (verbose)
    MsgLog::printf("Thread 0x%x resumes\n", this);
}

void
Thread::HandleYieldEntry()
{
  /* This routine is really another kernel entry point.  When called,
     the current process is giving up the processor, and is most
     likely (but not always) asleep on a stall queue.

     We do a few cleanups that are useful in some or all of the
     various Yield() paths, call Reschedule, and then resume the
     thread that is current following reschedule.

  */     

  assert (Thread::Current());
  
  /* If we yielded from within the IPC path, we better not be yielding
     after the call to COMMIT_POINT(): */
  assert (InvocationCommitted == false);

  extern uint32_t TrapDepth;

  if ( TrapDepth == 0 )
    MsgLog::fatal("Bad call to DoYield!!\n");

  assert (TrapDepth == 1);
  
#ifndef NDEBUG
  extern Thread *pIdleThread;
  if (Thread::Current() == pIdleThread)
    assert( (Thread::Current()->state == Thread::Running) ||
	    (Thread::Current()->state == Thread::Ready) );
#endif

#if defined(DDB)
  {
    extern bool ddb_uyield_debug;
    if ( ddb_uyield_debug )
      MsgLog::dprintf(true, "Thread 0x%08x yields\n",
		      Thread::Current()); 
  }
#endif

#ifndef NDEBUG
  if ( Thread::Current()->IsUser() )
    Thread::Current()->ValidateUserThread();
#endif

  ObjectHeader::ReleasePinnedObjects();
  ObjectCache::ReleaseUncommittedIoPageFrames();

  extern Invocation inv;
  inv.Cleanup();

  /* If we were in a SEND invocation, release the thread: */
  extern Thread *threadToRelease;
  if (threadToRelease)
    threadToRelease->MigrateTo(0);

  Thread::ForceResched();
  if (Thread::Current()->context)
    Thread::CurContext()->runState = RS_Running;

  /* At this time, the thread rescheduler logic thinks it must run
     disabled. I am not convinced that it really needs to, but it is
     simpler not to argue with it here.  Do this check to avoid
     disabling interrupts recursively forever.  Also, this check has
     the right effect whether or not interrupts were enabled in
     OnKeyInvocationTrap(). */
  
  if (IRQ::DISABLE_DEPTH() == 0)
    IRQ::DISABLE();
  
  assert( IRQ::DISABLE_DEPTH() == 1 );

  Thread::Reschedule();
  Thread::Current()->Resume();
}
