#ifndef __IDT_HXX__
#define __IDT_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


// If IDT_ENTRIES is revised, the size of the IDT table in interrupt.S
// must be updated also.
#define IDT_ENTRIES 0x33
#define HW_ENTRIES 0x30

struct IntVec {
  enum Type {
    DivZero 		= 0x0,
    Debug 		= 0x1,
    Nmi 		= 0x2,
    BreakPoint 		= 0x3,
    Overflow 		= 0x4,
    Bounds 		= 0x5,
    BadOpcode 		= 0x6,
    DeviceNotAvail	= 0x7,
    DoubleFault 	= 0x8,
    CoprocessorOverrun 	= 0x9,
    InvalTSS 		= 0xa,
    SegNotPresent 	= 0xb,
    StackSeg 		= 0xc,
    GeneralProtection 	= 0xd,
    PageFault 		= 0xe,
    CoprocError		= 0x10,
    AlignCheck         	= 0x11,
    MachineCheck       	= 0x12,

    IRQ0 		= 0x20,
    IRQ1 		= 0x21,
    IRQ2 		= 0x22,
    IRQ3 		= 0x23,
    IRQ4 		= 0x24,
    IRQ5 		= 0x25,
    IRQ6 		= 0x26,
    IRQ7 		= 0x27,
    IRQ8 		= 0x28,
    IRQ9 		= 0x29,
    IRQ10 		= 0x2a,
    IRQ11 		= 0x2b,
    IRQ12 		= 0x2c,
    IRQ13 		= 0x2d,
    IRQ14 		= 0x2e,
    IRQ15 		= 0x2f,
    Yield		= 0x30,
    InvokeKey		= 0x31,
    EmulPseudoInstr	= 0x32,
  };
};

struct IRQ386 {
  enum Type {
    IRQ0,
    HardClock 		= IRQ0,	// IRQ0
    IRQ1,
    Keyboard 		= IRQ1,	// IRQ1
    IRQ2,
    Cascade 		= IRQ2,	// IRQ2. Should not happen.
    IRQ3,
    Serial1 		= IRQ3,	// IRQ3. Known as comm2 in DOS
    IRQ4,
    Serial0 		= IRQ4,	// IRQ4. Known as comm1 in DOS
    IRQ5,
    Parallel1 		= IRQ5,	// IRQ5. Known as lpt2 in DOS
    IRQ6,
    Floppy		= IRQ6,	// IRQ6
    IRQ7,
    Parallel0 		= IRQ7,	// IRQ7. Known as lpt1 in DOS
    IRQ8,
    WallClock 		= IRQ8,	// IRQ8
    IRQ9,
    IRQ10,
    IRQ11,
    IRQ12,
    IRQ13,
    MathCoproc 		= IRQ13, // IRQ13 if brain dead NPU
    IRQ14,
    HardDisk0 		= IRQ14, // IRQ14
    IRQ15,
    HardDisk1 		= IRQ15, // IRQ14
  };
};
    
typedef struct fixregs_t fixregs_t;

class IDT {
  typedef void (*VecFn)(fixregs_t*);
  static VecFn IntVecEntry[IDT_ENTRIES];
  static uint32_t IDTdescriptor[2];
  static void lidt();
  static void SetupInterruptControllers();
    
  // Following is called by the assembly-level switch routine, even
  // though it is private.
  static void OnTrapOrInterrupt(fixregs_t *) NORETURN;
  static void OnKeyInvocationTrap(fixregs_t *) NORETURN;
  static void OnEmulatedInstr();
  static void SetEntry(int, void (*)(void), bool);
    
#if 0
  static inline bool IsInterruptPending(uint32_t interrupt);
  static inline void ResetPIC();
#endif

#if 0
public:
  static uint8_t pic1_cache;
  static uint8_t pic2_cache;
#endif

public:
  // This is slowly being obsoleted in place of the one in interrupt.S
  // static uint32_t TrapDepth;
    
  static void Init();

  static void WireVector(uint32_t vector, void (*pf)(fixregs_t*));

  static void YieldVector(fixregs_t*);
#if 0
  static void RunInterruptHandlers(fixregs_t*);
#endif
  static void UnboundVector(fixregs_t*);
  static void UnboundInterrupt(fixregs_t*);
  
#if 0
  static inline void SetInterruptMasks();
  static void EnableInterrupt(uint32_t interrupt);
  static void DisableInterrupt(uint32_t interrupt);
  static bool IsInterruptEnabled(uint32_t interrupt);
#endif
};

extern IDT IDT;

inline void IDT::lidt()
{
#ifdef __ELF__
  __asm__ __volatile__("lidt _3IDT.IDTdescriptor"
		       : /* no output */
		       : /* no input */
		       : "memory");
#else
  __asm__ __volatile__("lidt __3IDT$IDTdescriptor"
		       : /* no output */
		       : /* no input */
		       : "memory");
#endif
}

#endif // __IDT_HXX__
