/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Drivers for 386 protection faults

#include <kerninc/kernel.hxx>
#include <eros/ProcessState.h>
#include <eros/memory.h>
#include <kerninc/Check.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Depend.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/Debug.hxx>
#include <kerninc/util.hxx>
#include <eros/SysTraceKey.h>
#include <kerninc/Process.hxx>
#include <machine/Process.hxx>
#include <machine/PTE.hxx>
#include "IDT.hxx"
#include "lostart.hxx"

#define dbg_pgflt	0x1	/* steps in taking snapshot */
#define dbg_capflt	0x2	/* steps in taking snapshot */

/* Following should be an OR of some of the above */
#define dbg_flags   ( 0u )

#define DEBUG(x) if (dbg_##x & dbg_flags)

// Possible outcomes of a user-level page fault:
//
// 1. Fault was due to a not-present page, and address is not valid in
// address space segment.  Domain should be faulted and an attempt
// made to invoke appropriate keeper.
//
// 2. Fault was due to a not-present page, but address IS valid in
// address space segment.  Segment indicates access is valid.
// Construct the PDE and PTE for this mapping. If the object
// referenced is hazarded, construct a write-hazard mapping even if
// the object is in principle writable. Restart the process.
//
// 3. Fault was an access violation due to writing a page that in
// principle was read-only.  Domain should be faulted and an attempt
// made to invoke appropriate keeper.
//
// 4. Fault was an access violation due to writing a page that has a
// write-hazarded mapping.  Two cases: If the hazard still applies, go
// to sleep on the hazarded object.  else validate the mapping for
// writing and restart the operation.
//
// EROS does not provide write-only or execute-only mappings.
//
//
// Meaning of the error code value for a page fault:
//
// Bit  Value  Meaning
// 0    0      Not-present page
// 0    1      Page-level protection violation
// 1    0      Access was a read
// 1    1      Access was a write
// 2    0      Access was supervisor-mode
// 2    1      Access was user-mode

extern "C" {
  extern uint32_t CpuType;
#ifndef NDEBUG
  extern void end();
  extern void start();
#endif
};

bool PteZapped = false;

static ObjectHeader *
proc_MakeNewPageTable(SegWalkInfo& wi, uint32_t ndx); 
static ObjectHeader*
proc_MakeNewPageDirectory(SegWalkInfo& wi); 


#ifdef DDB
void
PTE::ddb_dump()
{
  char attrs[64];
  char *nxtAttr = attrs;
  MsgLog::printf("Pg Frame 0x%08x [", PageFrame());

#define ADDATTR(s) do { const char *sp = (s); *nxtAttr++ = ','; while (*sp) *nxtAttr++ = *sp++; } while (0)
  
  if (PTE_ISNOT(*this, PTE_DV)) *nxtAttr++ = '!';
  *nxtAttr++ = 'D';
  *nxtAttr++ = 'V';

  if (PTE_IS(*this, PTE_CV))
    ADDATTR("CV");
  else
    ADDATTR("!CV");

  if (PTE_IS(*this, PTE_W)) ADDATTR("W");
  if (PTE_IS(*this, PTE_USER))
    ADDATTR("U");
  else
    ADDATTR("S");
  if (PTE_IS(*this, PTE_ACC)) ADDATTR("A");
  if (PTE_IS(*this, PTE_DRTY)) ADDATTR("D");
  if (PTE_IS(*this, PTE_PGSZ)) ADDATTR("L");
  if (PTE_IS(*this, PTE_GLBL)) ADDATTR("G");
  if (PTE_IS(*this, PTE_WT))
    ADDATTR("WT");
  else
    ADDATTR("!WT");
  if (PTE_IS(*this, PTE_CD)) ADDATTR("CD");
  if (PTE_IS(*this, PTE_CS)) ADDATTR("CS");

#undef ADDATTR

  *nxtAttr++ = 0;
  MsgLog::printf("%s]\n", attrs);
}
#endif

void
Depend::InvalidateProduct(ObjectHeader* page)
{
  // InvalidateProduct is always called after the producing Node has
  // been unprepared (and thus zapped).  If this is a page table, we
  // therefore know that all of it's entries are dead.
  //
  // If this is a page directory, however, we must find all of the
  // mapping table pointers that name it and zap those.

#if 0
  MsgLog::dprintf(true, "NAILING product 0x%08x\n", page);
#endif
  
  assert (page->obType == ObType::PtMappingPage);

  kpa_t mp_pa = VTOP(ObjectCache::ObHdrToPage(page));
	       
  // MUST BE CAREFUL -- if this product is the active mapping table
  // we need to reset the kernel map to the native kernel map!
  kpa_t curmap = Machine::GetMappingTable();
  if (mp_pa == curmap) {
#if 0
    MsgLog::dprintf(true, "Nailing active mapping table!\n");
#endif
    Machine::SetMappingTable(KERNPAGEDIR);
  }
  
  if (page->producerNdx == EROS_NODE_LGSIZE) {
    for (uint32_t i = 0; i < KTUNE_NCONTEXT; i++)
      if (Process::ContextCache[i].fixRegs.MappingTable == mp_pa)
	Process::ContextCache[i].fixRegs.MappingTable = 0;
  }
  
  Machine::FlushTLB();
}

#ifndef NDEBUG
bool
PTE::ObIsNotWritable(ObjectHeader *pObj)
{
  bool result = true;
  
  // Start by building a writable PTE for the page:
  uint32_t kvaw = ObjectCache::ObHdrToPage(pObj);

  kvaw |= PTE_W;

#ifdef SMALL_SPACES
  // Check small spaces first:
  uint32_t nFrames = KTUNE_NCONTEXT / 32;
  PTE *pte = Process::smallSpaces;
  
  for (unsigned i = 0; i < nFrames * NPTE_PER_PAGE; i++) {
    if ((pte[i].w_value & (PTE_FRAMEBITS|PTE_W)) == kvaw) {
      MsgLog::dprintf(true,
		      "Checking pobj 0x%x with frame at 0x%x\n"
		      "Pg hdr 0x%x retains writable small PTE at 0x%x\n",
		      pObj, kvaw,
		      pObj, &pte[i]);
      result = false;
    }
  }
#endif

  for (uint32_t pf = 0; pf < ObjectCache::TotalPages(); pf++) {
    ObjectHeader *pHdr = ObjectCache::GetCorePageFrame(pf);

    if (pHdr->obType != ObType::PtMappingPage)
      continue;

    PTE *ptepg = (PTE *) ObjectCache::ObHdrToPage(pHdr);
    
    uint32_t limit = NPTE_PER_PAGE;
    if (pHdr->producerNdx == EROS_NODE_LGSIZE)
      limit = (UMSGTOP >> 22);	// PAGE_ADDR_BITS + PAGE_TABLE_BITS

    for (uint32_t ent = 0; ent < limit; ent++) {
      if ((ptepg[ent].w_value & (PTE_FRAMEBITS|PTE_W)) == kvaw) {
	MsgLog::dprintf(true,
			"Checking pobj 0x%x with frame at 0x%x\n"
			"Page hdr 0x%x retains writable PTE at 0x%x\n",
			pObj, kvaw,
			pObj, &ptepg[ent]);
	result = false;
      }
    }
  }

  return result;
}
#endif /* !NDEBUG */

void
Depend::WriteDisableProduct(ObjectHeader *pObj)
{
  // This is trickier than the case above, since we must not
  // write-disable kernel mappings.  On the other hand, we don't need
  // to worry about blasting the current mapping table either.

  assert (pObj->obType == ObType::PtMappingPage);

  kva_t mp_va = ObjectCache::ObHdrToPage(pObj);

  // Each mapping table holds 1024 entries, but the uppermost portion
  // of the address space is reserved for the kernel and the small
  // spaces.  Kernel pages should not be write disabled, nor should
  // the small space directory entries (small space page table entries 
  // are write disabled as a special case at the page table level).

  uint32_t limit = NPTE_PER_PAGE;
  if (pObj->producerNdx == EROS_NODE_LGSIZE)
    limit = (UMSGTOP >> 22);	// PAGE_ADDR_BITS + PAGE_TABLE_BITS

  PTE *pte = (PTE*) mp_va;
  for (uint32_t entry = 0; entry < limit; entry++)
    pte[entry].WriteProtect();
}

#ifdef INVOKE_TIMING
extern "C" {
  uint64_t rdtsc();
};
#endif

void
PageFault(fixregs_t *sa)
{
#ifdef KERN_TIMING_STATS
  uint64_t top_time = rdtsc();
#ifdef KERN_EVENT_TRACING
  uint64_t top_cnt0 = Machine::ReadCounter(0);
  uint64_t top_cnt1 = Machine::ReadCounter(1);
#endif
#endif

  ula_t la = sa->ExceptAddr;
  uint32_t error = sa->Error;
  bool writeAccess = (error & 2) ? true : false;

  // sa->Dump();

  // If we page faulted from supervisor mode it's trivial:
  if ((error & 4) == 0) {
#if 0
    if ( (sa->EIP == (uint32_t) ipc_probe_start)
	 || (sa->EIP == (uint32_t) ipc_probe_top) ) {
      MsgLog::printf("Send mapping fault 0\n");
      sa->EAX = 1;
      sa->EIP = (uint32_t) ipc_probe_end;
      return;
    }
#endif

#if 0
    sa->Dump();
#endif
    MsgLog::fatal("Kernel page fault\n"
		  " SaveArea      =  0x%08x  EIP           =  0x%08x\n"
		  " Fault address =  0x%08x  Code          =  0x%08x\n",
		  sa, sa->EIP,
		  la, error);
  }

  // Domain page fault.  If we take this from kernel mode we are in
  // deep kimchee.
  Process* ctxt = (Process*) Thread::CurContext();

  assert(& ctxt->fixRegs == sa);

  ctxt->stats.pfCount++;

  KernStats.nPfTraps++;
  if (writeAccess) KernStats.nPfAccess++;

  PteZapped = false;
  ObjectHeader::BeginTransaction();

  (void) ctxt->DoPageFault(la, writeAccess, false, false);

  // We succeeded (wonder of wonders) -- release pinned resources.
  ObjectHeader::ReleasePinnedObjects();
  // No need to release uncommitted I/O page frames -- there should
  // not be any.

  assert(Thread::CurContext());

#ifdef KERN_TIMING_STATS
  {
    extern uint64_t pf_delta_cy;
    uint64_t bot_time = rdtsc();

#ifdef KERN_EVENT_TRACING
    extern uint64_t pf_delta_cnt0;
    extern uint64_t pf_delta_cnt1;

    uint64_t bot_cnt0 = Machine::ReadCounter(0);
    uint64_t bot_cnt1 = Machine::ReadCounter(1);
    pf_delta_cnt0 += (bot_cnt0 - top_cnt0);
    pf_delta_cnt1 += (bot_cnt1 - top_cnt1);
#endif
    pf_delta_cy += (bot_time - top_time);
  }
#endif
}


/* Note that CAP_PAGE_FLAGS does NOT include user mode! */
#define DATA_PAGE_FLAGS  (PTE_ACC|PTE_USER|PTE_DV)
#define CAP_PAGE_FLAGS  (PTE_ACC|PTE_CV|PTE_DV)

#ifdef SMALL_SPACES
bool
Process::DoSmallPageFault(ula_t la, bool isWrite,
			  bool isCap, bool prompt)
{
#if 0
  MsgLog::dprintf(true, "SmallPgFlt w/ la=0x%08x, bias=0x%08x,"
		  " limit=0x%08x\n", la, bias, limit);
#endif

  // Address is a linear address.  Subtract the base and check the
  // bound.
  uva_t va = la - bias;

#if 0
  // This assertion can be false given that the process can run from
  // any address space and the save logic may subsequently save that
  // address space pointer.
  assert ( fixRegs.MappingTable == KERNPAGEDIR );
#endif

  SegWalkInfo wi;
  wi.vaddr = va;
  wi.segObj = 0;
  wi.segFault = FC_NoFault;
  wi.traverseCount = 0;

  wi.prompt = prompt;
  wi.writeAccess = isWrite;
  wi.capAccess = isCap;

  PTE& thePTE = smallPTE[(va >> EROS_PAGE_ADDR_BITS) % SMALL_SPACE_PAGES];

  // If the virtual address falls outside the small range and is
  // valid, this walk will result in depend entries that blast the
  // wrong PTE.  No real harm can come of that, since either the
  // address is bad or we will in that case be switching to a large
  // space anyway.
  
  // Do the traversal...
  if ( !proc_WalkSeg(this, wi, EROS_PAGE_BLSS, &thePTE, 0, false) ) { 
    SetFault(wi.segFault, va, false);
    return false;
  }

  // If we get this far, there is a valid translation at this address,
  // but if the address exceeds the small space limit we need to
  // convert the current address space into a large space.
  //
  // If this is necessary, set smallPTE to zero and YIELD, because we
  // might have been called from unside the IPC code.  This will
  // induce another page fault, which will follow the large spaces
  // path.

  // This should not happen on the invoker side, where the addresses
  // were validated during page probe (causing SS fault or GP fault).
  // On the invokee side, however, it is possible that we forced the
  // invokee to reload, in which case it loaded as a small space, and
  // we then tried to validate the receive address.  In that case,
  // PopulateExitBlock() called us here with an out of bounds virtual
  // address.  We reset the process mapping state and Yield(),
  // allowing the correct computation to be done in the next pass.

  if (va >= limit) {
#if 0
    MsgLog::dprintf(true, "!! la=0x%08x va=0x%08x\n"
		    "Switching process 0x%X to large space\n",
		    la, va,
		    procRoot->ob.oid);
#endif
    SwitchToLargeSpace();
    Thread::Current()->Yield();
  }
     
  kpa_t pageAddr = 0;
  
  uint32_t flags = DATA_PAGE_FLAGS;
  
  if (wi.segObj->obType == ObType::PtCapPage) {
    DEBUG(capflt)
      MsgLog::dprintf(true, "Mapping a capability page!\n");
    flags = CAP_PAGE_FLAGS;
      
    if (wi.weakRef == false)
      flags |= PTE_CS;
  }
    
  ObjectHeader *pPageHdr = wi.segObj;
    
  if (isWrite)
    pPageHdr->MakeObjectDirty();

  pageAddr = VTOP(ObjectCache::ObHdrToPage(pPageHdr));

  assert ((pageAddr & EROS_PAGE_MASK) == 0);
  assert (pageAddr < (kpa_t) start || pageAddr >= (kpa_t) end);
	  
  assert (va < (SMALL_SPACE_PAGES * EROS_PAGE_SIZE));

  bool needInvalidate = false;
  
  if (isWrite && PTE_IS(thePTE, PTE_DV)) {
    // We are doing this because the old PTE had insufficient
    // permission, so we must zap the TLB.
    needInvalidate = true;
  }
  
  thePTE.Invalidate();
  PTE_SET(thePTE, (pageAddr & PTE_FRAMEBITS) | flags);
  if (isWrite)
    PTE_SET(thePTE, PTE_W);
#ifdef WRITE_THROUGH
  if (CpuType >= 5)
    PTE_SET(thePTE, PTE_WT);
#endif

  DEBUG(capflt)
    if (wi.segObj->obType == ObType::PtCapPage)
      MsgLog::dprintf(true, "Built Cap Page PTE for pg 0x%x at 0x%x\n",
		      pageAddr, &thePTE);
#if 0
  MsgLog::dprintf(true, "Built PTE at 0x%08x\n", &thePTE);
#endif
  
  // If cap page, better not be data valid.
  assert ( (wi.segObj->obType != ObType::PtCapPage) ||
	   (PTE_IS(thePTE, PTE_USER) == 0) );
    
  if (needInvalidate)
    Machine::FlushTLB(la);
    
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("End of DoSmallPageFault()");
#endif

  return true;
}
#endif

uint32_t DoPageFault_CallCounter;

// At some point, this logic will need to change to account for
// background windows.  In the event that we encounter a non-local
// background window key we will need to do a complete traversal in
// order to find the background segment, because the background
// segment slot is not cached.
//
// Actually, this is contingent on a design distinction, which is
// whether multiple red segments should be tracked on the way down the
// segment tree.  When we cross a KEPT red segment, we should
// certainly forget any outstanding background segment -- we do not
// want the red segment keeper to be able to fabricate a background
// window key that might reference a segment over which the keeper
// should not have authority.
//
// A case can be made, however, that a kept red segment should be
// permitted to contain a NON-kept red segment that specifies a
// background segment.  The main reason to desire this is to allow
// (e.g.) VCSK to operate on a background segment that contains a
// window.
//
// For the moment, we do not support this, and I am inclined to
// believe that it is unwise to do so until I see a case in which it
// is useful.
//
// Local windows are not yet supported by the SegWalk code, but none
// of this is an issue for local windows,

#if 1
inline uint64_t
BLSS_MASK64(uint32_t blss)
{
  uint32_t bits_to_shift =
    (blss - EROS_PAGE_BLSS) * EROS_NODE_LGSIZE + EROS_PAGE_ADDR_BITS; 

  uint64_t mask = (1ull << bits_to_shift);
  mask -= 1ull;
  
  return mask;
}

// #define WALK_LOUD
#define FAST_TRAVERSAL
bool
Process::DoPageFault(ula_t la, bool isWrite, bool isCap, bool prompt)
{
#if EROS_NODE_SIZE == 16
  const int walk_root_blss = 7;
  const int walk_top_blss = 5;
  const int walk_mid_blss = 4;
#elif EROS_NODE_SIZE == 32
  const int walk_root_blss = 6;
  const int walk_top_blss = 4;
#else
#error "Unhandled BLSS value in DoPageFault()."
#endif
  
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top of DoPageFault()");
#endif

  DoPageFault_CallCounter++;
  
  DEBUG(pgflt) {
    MsgLog::printf("DoPageFault: ctxt=0x%08x EIP 0x%08x la=0x%08x, isWrite=%c prompt=%c\n",
		   this,
		   fixRegs.EIP,
		   la,
		   isWrite ? 't' : 'f',
		   prompt ? 't' : 'f');
  }
  
#ifdef SMALL_SPACES
  uva_t va = la - bias;
#else
  uva_t va = la;
#endif
  
  // If LA is simply out of range, then forget the whole thing:
  if ( la >= KVA ) {
    MsgLog::dprintf(true, "Domain accessed kernel or small space la\n");
    SetFault(isCap ? FC_CapInvalidAddr : FC_DataInvalidAddr, va, false);
    return false;
  }

#ifdef SMALL_SPACES
  if (smallPTE)
    return DoSmallPageFault(la, isWrite, isCap, prompt);
#endif
			    
  // If LA is simply out of range, then forget the whole thing:
  if ( la >= UMSGTOP ) {
    MsgLog::dprintf(true, "Large domain accessed small space la\n");
    SetFault(isCap ? FC_CapInvalidAddr : FC_DataInvalidAddr, va, false);
    return false;
  }

  // If we discover on the way to load the process that it's mapping
  // table register was voided, we substituted KERNPAGEDIR.  Notice
  // that here:
  if ( fixRegs.MappingTable == KERNPAGEDIR )
    fixRegs.MappingTable = 0;

  // Set up a WalkInfo structure and start building the necessary
  // mapping table, PDE, and PTE entries.

  SegWalkInfo wi;
  wi.segFault = FC_NoFault;
  wi.traverseCount = 0;
  wi.vaddr = va;
  wi.segObj = 0;
  wi.writeAccess = isWrite;
  wi.capAccess = isCap;
  wi.prompt = prompt;
  wi.weakRef = false;

  PTE *pTable = (PTE *) (PTOV(fixRegs.MappingTable) & ~EROS_PAGE_MASK);

#ifdef WALK_LOUD
  MsgLog::dprintf(false, "pTable is 0x%x\n", pTable);
#endif
  
  if (pTable == 0)
    goto need_pgdir;
  
  {
    ObjectHeader *pTableHdr = ObjectCache::PageToObHdr(PTOV(pTable));

    if (isWrite && !pTableHdr->rwProduct) {
      MsgLog::dprintf(true, "DoPageFault(): isWrite && !pTableHdr->rwProduct hdr 0x%x\n", pTableHdr);
      goto access_fault;
    }

    wi.canCall = pTableHdr->caProduct;
    
#ifdef FAST_TRAVERSAL
    /* We have a page directory conveying suitable access rights from
       the top.  See if the PDE has the necessary permissions: */
    {
      uint32_t pdeNdx = (la >> 22) & 0x3ffu;
      PTE& thePDE = pTable[pdeNdx];

      if ( PTE_IS(thePDE, PTE_DV|PTE_USER) ) {

	// We could short-circuit the walk in this case by remembering
	// the status of /wi.canWrite/ in a spare bit in the PTE, but
	// at the moment we do not do so because in most cases the
	// write restriction appears lower down in the segment tree.

	if ( PTE_IS(thePDE, PTE_W) || !isWrite ) {
	  // We have a valid PDE with the necessary permissions!

	  pTable = (PTE *) thePDE.PageFrame();
	  pTableHdr = ObjectCache::PageToObHdr(PTOV(pTable));
	  
	  wi.offset = wi.vaddr & ((1u << 22) - 1u);
	  wi.segBlss = pTableHdr->producerBlss;
	  wi.segObj = pTableHdr->producer;
	  wi.redSeg = pTableHdr->mp.redSeg;
	  if (wi.redSeg) {
	    wi.redSpanBlss = pTableHdr->mp.redSpanBlss;
	    wi.redSegOffset =
	      ((uint64_t) wi.vaddr) & BLSS_MASK64(wi.redSpanBlss);
	  }
	  wi.segObjIsRed = pTableHdr->mp.redProducer;
	  wi.canWrite = pTableHdr->rwProduct ? true : false;
	  wi.canCall &= pTableHdr->caProduct;

#if 0
	  if (wi.redSeg && wi.offset != wi.redSegOffset) {
      MsgLog::dprintf(false, "pdr proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		      "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d segKey 0x%x\n"
		      "ca %d canCall %d canWrite %d weakRef %d\n"
		      "redSeg 0x%x redOffset 0x%X\n",
		      wi.segObj, wi.segBlss, wi.segObjIsRed,
		      wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		      0x0,
		      wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef,
		      wi.redSeg, wi.redSegOffset);

	    MsgLog::dprintf(true, "Found pg dir. Offset 0x%X RedSegOffset 0x%X spanBlss %d\n",
			    wi.offset, wi.redSegOffset, wi.redSpanBlss);
	  }
#endif
	  
	  // NOTE: This is doing the wrong thing when the red segment
	  // needs to be grown upwards, because the segBlss in that
	  // case is less than the *potential* span of the red
	  // segment.
#ifdef WALK_LOUD
	  MsgLog::dprintf(false, "have_good_pde\n");
#endif
	  goto have_good_pde;
	}
      }
    }
#endif
    
    wi.offset = wi.vaddr;
    wi.segBlss = pTableHdr->producerBlss;
    wi.segObj = pTableHdr->producer;
    wi.redSeg = pTableHdr->mp.redSeg;
    if (wi.redSeg) {
      wi.redSpanBlss = pTableHdr->mp.redSpanBlss;
      wi.redSegOffset =
	((uint64_t) wi.vaddr) & BLSS_MASK64(wi.redSpanBlss);
    }
    wi.segObjIsRed = pTableHdr->mp.redProducer;
    wi.canWrite = pTableHdr->rwProduct ? true : false;

    // FIX: THIS IS WRONG!!
    wi.weakRef = false;
    
#ifdef WALK_LOUD
    MsgLog::dprintf(false, "have_pgdir\n");
#endif
    goto have_pgdir;
  }
  
 need_pgdir:
  
  // Begin the traversal...
  if ( !proc_WalkSeg(this, wi, walk_root_blss,
		     (PTE *)&fixRegs.MappingTable, 0, false) ) { 
    SetFault(wi.segFault, va, false);
    return false;
  }
  
  assert (pTable == 0);
  if (pTable == 0) {
    // See if a mapping table has already been built for this address
    // space.  If so, just use it.  Using wi.segBlss is okay here
    // because the mapping table pointer will be zapped if anything
    // above this point gets changes, whereupon the gunk the the page
    // directory will no longer matter.
    ObjectHeader *pTableHdr =
      wi.segObj->FindProduct(wi, EROS_NODE_LGSIZE /* ndx */,
			     wi.canWrite, wi.canCall);
    
    if (pTableHdr == 0)
      pTableHdr = proc_MakeNewPageDirectory(wi);

    pTable = (PTE *) ObjectCache::ObHdrToPage(pTableHdr);

    fixRegs.MappingTable = VTOP(pTable);
  }

 have_pgdir:
  
#ifndef NDEBUG
  {
    ObjectHeader *pTableHdr = ObjectCache::PageToObHdr(PTOV(pTable));

    assert(wi.segBlss == pTableHdr->producerBlss);
    assert(wi.segObj == pTableHdr->producer);
    assert(wi.redSeg == pTableHdr->mp.redSeg);
    assert(wi.canWrite == (pTableHdr->rwProduct ? true : false));
  }
#endif

  {
    // Start building the PDE entry:
    uint32_t pdeNdx = (la >> 22) & 0x3ffu;
    PTE& thePDE = pTable[pdeNdx];

    // Translate the top 8 (10) bits of the address:
    if ( !proc_WalkSeg(this, wi, walk_top_blss, &thePDE, 0, true) )
      return false;

    // If we get this far, we need the page table to proceed further.
    // See if we need to build a new page table:

    if (PTE_IS(thePDE, PTE_DV)) {
      pTable = (PTE *) PTOV(thePDE.PageFrame());

      if (wi.canWrite && !PTE_IS(thePDE, PTE_W)) {
	// We could only have taken this fault because writing was not
	// enabled at the directory level, which means that we need to
	// flush the PDE from the hardware TLB.
	thePDE.Invalidate();
      }
    }
    else {
      uint32_t productNdx = 0;

      // Level 0 product need never be a read-only product.  We use
      // the write permission bit at the PDE level.
      
      ObjectHeader *pTableHdr =
	wi.segObj->FindProduct(wi, productNdx, true, true);

      if (pTableHdr == 0)
	pTableHdr = proc_MakeNewPageTable(wi, productNdx);

      assert(wi.segBlss == pTableHdr->producerBlss);
      assert(wi.segObj == pTableHdr->producer);
      assert(wi.redSeg == pTableHdr->mp.redSeg);
      assert(wi.canWrite == (pTableHdr->rwProduct ? true : false));

      pTable = (PTE *) ObjectCache::ObHdrToPage(pTableHdr);
    }

    // The level 0 page table is still contentless - there is no
    // need to build depend table entries covering it's contents.
    // We simply need to fill in the page directory entry:

    PTE_SET(thePDE, (VTOP((kva_t)pTable) & PTE_FRAMEBITS));
    PTE_SET(thePDE, PTE_ACC|PTE_USER|PTE_DV|PTE_CV|PTE_CS);
    
    // Using /canWrite/ instead of /isWrite/ reduces the number of
    // cases in which we need to rebuild the PDE without altering the
    // actual permissions, and does not require us to dirty a page.
    // This is legal only because on this architecture the node tree
    // and the page tables are congruent at this level.
    if (wi.canWrite)
      PTE_SET(thePDE, PTE_W);
#if 0
    if (isCap)
      PTE_SET(thePDE, PTE_CV|PTE_CS);
#endif
  }

#ifdef FAST_TRAVERSAL
 have_good_pde:
#endif
  
  {
    uint32_t pteNdx = (la >> 12) & 0x3ffu;
    PTE& thePTE = pTable[pteNdx];
  
#if EROS_NODE_SIZE == 16
    // Translate the next 4 bits of the address, after which we will
    // have translated the top 10 bits of the LA, and can set the
    // Writable bit of the PDE accordingly if this is a write request.
  
    if ( !proc_WalkSeg(this, wi, walk_mid_blss, &thePDE, &thePTE) )
      return false;
#endif

    // Translate the remaining bits of the address:
    if ( !proc_WalkSeg(this, wi, EROS_PAGE_BLSS, &thePTE, 0, true) )
      return false;

    kpa_t pageAddr = 0;

    uint32_t flags = DATA_PAGE_FLAGS;
  
    if (wi.segObj->obType == ObType::PtCapPage) {
      DEBUG(capflt)
	MsgLog::dprintf(true, "Mapping a capability page!\n");
      flags = CAP_PAGE_FLAGS;
      
      if (wi.weakRef == false)
	flags |= PTE_CS;
    }

    if (isWrite)
      wi.segObj->MakeObjectDirty();

    pageAddr = VTOP(ObjectCache::ObHdrToPage(wi.segObj));

    if (pageAddr == 0)
      MsgLog::dprintf(true, "pPageHdr 0x%08x at addr 0x%08x!!\n",
		      wi.segObj, pageAddr);

    assert ((pageAddr & EROS_PAGE_MASK) == 0);
    assert (pageAddr < (kpa_t) start || pageAddr >= (kpa_t) end);
	  
    if (isWrite && PTE_IS(thePTE, PTE_DV)) {
      // We are doing this because the old PTE had insufficient
      // permission, so we must zap the TLB.  There is no need to
      // invalidate in the PTE_CV case, because we are going to
      // interpret that in software.
      thePTE.Invalidate();
    }
  
    PTE_SET(thePTE, (pageAddr & PTE_FRAMEBITS) );
    PTE_SET(thePTE, flags);
    if (isWrite)
      PTE_SET(thePTE, PTE_W);
#ifdef WRITE_THROUGH
    if (CpuType >= 5)
      PTE_SET(thePTE, PTE_W);
#endif

    DEBUG(capflt)
      if (wi.segObj->obType == ObType::PtCapPage)
	MsgLog::dprintf(true, "Built Cap Page PTE for pg 0x%x at 0x%x\n",
			pageAddr, &thePTE);

    // If cap page, better not be data valid.
    assert ( (wi.segObj->obType != ObType::PtCapPage) ||
	     (PTE_IS(thePTE, PTE_USER) == false) );
  }
    
  if (PteZapped)
    Machine::FlushTLB();
    
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("End of DoPageFault()");
#endif

  return true;

 access_fault:
  wi.segFault =
    wi.capAccess ? FC_CapAccess : FC_DataAccess;
  goto fault_exit;

 fault_exit:
#ifdef WALK_LOUD
    MsgLog::dprintf(true, "flt proc_WalkSeg: wi.producer 0x%x, wi.prodBlss %d wi.isRed %d\n"
		    "wi.vaddr 0x%x wi.offset 0x%X flt %d  wa %d\n"
		    "ca %d canCall %d canWrite %d weakRef %d\n",
		    wi.segObj, wi.segBlss, wi.segObjIsRed,
		    wi.vaddr, wi.offset, wi.segFault, wi.writeAccess,
		    wi.capAccess, wi.canCall, wi.canWrite, wi.weakRef);
#endif
  if (wi.prompt == false)
    InvokeSegmentKeeper(wi.segFault, wi);

  return false;
}
#else
bool
Process::DoPageFault(ula_t la, bool isWrite, bool isCap, bool prompt)
{
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top of DoPageFault()");
#endif

  DoPageFault_CallCounter++;
  
  DEBUG(pgflt) {
    MsgLog::printf("DoPageFault: ctxt=0x%08x EIP 0x%08x la=0x%08x, isWrite=%c prompt=%c\n",
		   this,
		   fixRegs.EIP,
		   la,
		   isWrite ? 't' : 'f',
		   prompt ? 't' : 'f');
  }
  
#ifdef SMALL_SPACES
  uva_t va = la - bias;
#else
  uva_t va = la;
#endif
  
  // If LA is simply out of range, then forget the whole thing:
  if ( la >= KVA ) {
    MsgLog::dprintf(true, "Domain accessed kernel or small space la\n");
    SetFault(isCap ? FC_CapInvalidAddr : FC_DataInvalidAddr, va);
    return false;
  }

#ifdef SMALL_SPACES
  if (smallPTE)
    return DoSmallPageFault(la, isWrite, isCap, prompt);
#endif
			    
  // If LA is simply out of range, then forget the whole thing:
  if ( la >= UMSGTOP ) {
    MsgLog::dprintf(true, "Large domain accessed small space la\n");
    SetFault(isCap ? FC_CapInvalidAddr : FC_DataInvalidAddr, va);
    return false;
  }

  // If we discover on the way to load the process that it's mapping
  // table register was voided, we substituted KERNPAGEDIR.  Notice
  // that here:
  if ( fixRegs.MappingTable == KERNPAGEDIR )
    fixRegs.MappingTable = 0;

  // Set up a WalkInfo structure and start building the necessary
  // mapping table, PDE, and PTE entries.

  SegWalkInfo wi;
  wi.vaddr = va;
  wi.segObj = 0;
  wi.writeAccess = isWrite;
  wi.capAccess = isCap;
  wi.prompt = prompt;

#if EROS_NODE_SIZE == 16
  const walk_root_blss = 7;
  const walk_top_blss = 5;
  const walk_mid_blss = 4;
#elif EROS_NODE_SIZE == 32
  const walk_root_blss = 6;
  const walk_top_blss = 4;
#else
#error "Unhandled BLSS value in DoPageFault()."
#endif
  
  // Begin the traversal...
  if ( !proc_WalkSeg(this, wi, walk_root_blss,
		     (PTE *)&fixRegs.MappingTable, 0, false) ) { 
    SetFault(wi.segFault, va);
    return false;
  }
  
  if (fixRegs.MappingTable == 0) {
    // See if a mapping table has already been built for this address
    // space.  If so, just use it.  Using wi.segBlss is okay here
    // because the mapping table pointer will be zapped if anything
    // above this point gets changes, whereupon the gunk the the page
    // directory will no longer matter.
    ObjectHeader *pTable;
    
    {
      ObjectHeader *pObjHdr = wi.segObj;
      
      pTable =
	pObjHdr->FindProduct(wi, EROS_NODE_LGSIZE /* ndx */, isWrite);
    }
    
    bool newTable = false;
    if (pTable == 0) {
      pTable = proc_MakeNewPageDirectory(wi, isWrite);
      newTable = true;
    }

    kva_t tableAddr = ObjectCache::ObHdrToPage(pTable);

    fixRegs.MappingTable = VTOP(tableAddr);
  }


  // Start building the PDE entry:
  uint32_t pdeNdx = (la >> 22) & 0x3ffu;
  PTE& thePDE = ((PTE*) PTOV(fixRegs.MappingTable) )[pdeNdx];

  // Translate the top 8 (10) bits of the address:
  if ( !proc_WalkSeg(this, wi, walk_top_blss, &thePDE, 0, true) )
    return false;

  // If we get this far, we need the page table to proceed further.
  // See if we need to build a new page table:

  bool needInvalidate = false;

  if ( PTE_ISNOT(thePDE, PTE_DV) || (PTE_ISNOT(thePDE,PTE_W) && isWrite) ) {
    uint32_t productNdx = 0;
  
    if (PTE_IS(thePDE, PTE_DV)) {
      // We are doing this because the old PDE had insufficient
      // permission, so we must zap the TLB.
      needInvalidate = true;
    }
    
#if EROS_NODE_SIZE == 16
    // If we actually ended up at a BLSS=5 node, that node produces 4
    // distinct page tables, and we must now find the proper one.
    // Permissions below the BLSS::bit24 layer will be managed within
    // the page tables, so we do not worry about those here.  It is
    // possible that we ended up at something smaller than a
    // BLSS::bit24 node, in which case only one page table is
    // produced.  Note that the productNdx computation will in that
    // case generate '0', which is precisely what we want.

    if (wi.segBlss == walk_top_blss)
      productNdx = (la >> 22) & 0x3;
#endif

    // Level 0 product need never be a read-only product.  We use
    // the write permission bit at the PDE level.
      
    ObjectHeader *pTable;
    
    {
      ObjectHeader *pObjHdr = wi.segObj;
      
      pTable =
	pObjHdr->FindProduct(wi, productNdx, true);
    }

    if (pTable == 0)
      pTable = proc_MakeNewPageTable(wi, productNdx);

    PTE *tableAddr = (PTE *) ObjectCache::ObHdrToPage(pTable);

    // The level 0 page table is still contentless - there is no
    // need to build depend table entries covering it's contents.
    // We simply need to fill in the page directory entry:

    thePDE.Invalidate();
    PTE_SET(thePDE, (VTOP((kva_t)tableAddr) & PTE_FRAMEBITS));
    PTE_SET(thePDE, PTE_ACC|PTE_USER|PTE_DV);
  }
  
  uint32_t pteNdx = (la >> 12) & 0x3ffu;
  kpa_t physPageTableAddr = thePDE.PageFrame();
  
  PTE* pageTable = (PTE*) PTOV(physPageTableAddr);
  PTE& thePTE = pageTable[pteNdx];
  
#if EROS_NODE_SIZE == 16
  // Translate the next 4 bits of the address, after which we will
  // have translated the top 10 bits of the LA, and can set the
  // Writable bit of the PDE accordingly if this is a write request.
  
  if ( !proc_WalkSeg(this, wi, walk_mid_blss, &thePDE, &thePTE) )
    return false;
#endif

  // Translate the remaining bits of the address:
  if ( !proc_WalkSeg(this, wi, EROS_PAGE_BLSS, &thePTE, 0, true) )
    return false;

  if (isWrite)
    PTE_SET(thePDE, PTE_W);
  if (isCap)
    PTE_SET(thePDE, PTE_CV|PTE_CS);

  kpa_t pageAddr = 0;

  uint32_t flags = DATA_PAGE_FLAGS;
  
  if (wi.segObj->obType == ObType::PtCapPage) {
    flags = CAP_PAGE_FLAGS;
      
    if (wi.weakRef == false)
      flags |= PTE_CS;
  }

  ObjectHeader *pPageHdr = wi.segObj;
  
  if (isWrite)
    pPageHdr->MakeObjectDirty();

  pageAddr = VTOP(ObjectCache::ObHdrToPage(pPageHdr));

  if (pageAddr == 0)
    MsgLog::dprintf(true, "pPageHdr 0x%08x at addr 0x%08x!!\n",
		    pPageHdr, pageAddr);

  assert ((pageAddr & EROS_PAGE_MASK) == 0);
  assert (pageAddr < (kpa_t) start || pageAddr >= (kpa_t) end);
	  
  if (isWrite && PTE_IS(thePTE, PTE_DV)) {
    // We are doing this because the old PTE had insufficient
    // permission, so we must zap the TLB.  There is no need to
    // invalidate in the PTE_CV case, because we are going to
    // interpret that in software.
    needInvalidate = true;
  }
  
  thePTE.Invalidate();
  PTE_SET(thePTE, (pageAddr & PTE_FRAMEBITS) );
  PTE_SET(thePTE, flags);
  if (isWrite)
    PTE_SET(thePTE, PTE_W);
#ifdef WRITE_THROUGH
  if (CpuType >= 5)
    PTE_SET(thePTE, PTE_W);
#endif

  // If cap page, better not be data valid.
  assert ( (wi.segObj->obType != ObType::PtCapPage) ||
	   (PTE_IS(thePTE, PTE_DV) == false) );
    
  if (needInvalidate)
    Machine::FlushTLB(la);
    
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("End of DoPageFault()");
#endif

  return true;
}
#endif

static ObjectHeader*
proc_MakeNewPageDirectory(SegWalkInfo& wi)
{
  ObjectHeader *pTable = ObjectCache::GrabPageFrame();
  assert (pTable->kr.IsValid(pTable));
  pTable->obType = ObType::PtMappingPage;
  pTable->producerNdx = EROS_NODE_LGSIZE;
  pTable->producerBlss = wi.segBlss;

  pTable->mp.redSeg = wi.redSeg;
  pTable->mp.redProducer = wi.segObjIsRed;
  pTable->mp.redSpanBlss = wi.redSpanBlss;
  pTable->rwProduct = wi.canWrite ? 1 : 0;
  pTable->caProduct = wi.canCall ? 1 : 0;
  pTable->SetDirtyFlag();

  kva_t tableAddr = ObjectCache::ObHdrToPage(pTable);

  bzero((void *) tableAddr, EROS_PAGE_SIZE);

  {
    uint32_t *kpgdir = (uint32_t *) PTOV(KERNPAGEDIR);
    uint32_t *upgdir = (uint32_t *) tableAddr;
    
    for (uint32_t i = (UMSGTOP >> 22); i < NPTE_PER_PAGE; i++)
      upgdir[i] = kpgdir[i];
  }    

  wi.segObj->AddProduct(pTable);

  return pTable;
}

static ObjectHeader*
proc_MakeNewPageTable(SegWalkInfo& wi, uint32_t ndx)
{
  // Need to make a new mapping table:

  ObjectHeader *pTable = ObjectCache::GrabPageFrame();
  assert (pTable->kr.IsValid(pTable));
  pTable->obType = ObType::PtMappingPage;
  pTable->producerNdx = ndx;
  pTable->producerBlss = wi.segBlss;
  
  pTable->mp.redSeg = wi.redSeg;
  pTable->mp.redProducer = wi.segObjIsRed;
  pTable->mp.redSpanBlss = wi.redSpanBlss;
  pTable->rwProduct = 1;
  pTable->caProduct = 1;	// we use spare bit in PTE
  pTable->SetDirtyFlag();

  kva_t tableAddr = ObjectCache::ObHdrToPage(pTable);

  bzero((void *)tableAddr, EROS_PAGE_SIZE);

#if 0
  MsgLog::printf("0x%08x->MkPgTbl(blss=%d,ndx=%d,rw=%c,ca=%c,"
		 "producerTy=%d) => 0x%08x\n",
		 wi.segObj,
		 wi.segBlss, ndx, 'y', 'y', wi.segObj->obType,
		 pTable);
#endif

  wi.segObj->AddProduct(pTable);

  return pTable;
}
