#ifndef __SEGMENT_HXX__
#define __SEGMENT_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

struct SegEntry {
  enum Name {
    Null = 0,
    KernelCode = 0x1,
    KernelData = 0x2,
    DomainTSS  = 0x3,
    DomainCode = 0x4,
    DomainData = 0x5,

    KProcCode  = 0x6,
    KProcData  = 0x7,

    ApmCode32  = 0x8,
    ApmCode16  = 0x9,
    ApmData    = 0x10,

    NUM_SEGENTRY
  };
};

struct Selector {
  enum Name {
    Null = 0x0,
    KernelCode = 0x8,		// entry 1, rpl=0
    KernelData = 0x10,		// entry 2, rpl=0
    DomainTSS  = 0x18,		// entry 3, rpl=3
    DomainCode = 0x23,		// entry 4, rpl=3
    DomainData = 0x2b,		// entry 5, rpl=3

    KProcCode  = 0x31,		// entry 6, rpl=1
    KProcData  = 0x39,		// entry 7, rpl=1

    ApmCode32  = 0x48,		// entry 9, rpl=0
    ApmCode16  = 0x50,		// entry 10, rpl=0
    ApmData    = 0x58,		// entry 11, rpl=0

    // Descriptor aliases for use in BIOS32:
    KernelBios32 = KernelCode,
    KProcBios32 = KProcCode,
  } ;
}; 

struct SegDescriptor {
  uint32_t loLimit     : 16;
  uint32_t loBase      : 16;
  uint32_t midBase     : 8;
  uint32_t type        : 4;
  uint32_t system      : 1;
  uint32_t dpl         : 2;
  uint32_t present     : 1;
  uint32_t hiLimit     : 4;
  uint32_t avl         : 1;
  uint32_t zero        : 1;
  uint32_t dflt        : 1;
  uint32_t granularity : 1;
  uint32_t hiBase      : 8;
};

struct GateDescriptor {
  uint32_t loOffset : 16;
  uint32_t selector : 16;
  uint32_t zero : 8;
  uint32_t type : 4;
  uint32_t system : 1;
  uint32_t dpl : 2;
  uint32_t present : 1;
  uint32_t hiOffset : 16;
};

#endif // __SEGMENT_HXX__
