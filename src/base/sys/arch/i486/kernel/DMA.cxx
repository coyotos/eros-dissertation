/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/DMA.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/IRQ.hxx>
#include "io.h"
#include <kerninc/util.hxx>
#include <kerninc/PhysMem.hxx>

// #define DMADEBUG

// PC DMA channels do not span the entirety of available memory.  I
// toyed with various complex memory allocation strategies to manage
// this, and they ultimately created more harm than good.  Ultimately,
// I concluded that the best solution was to make the DMA system look
// like it worked right.
//
// If a driver performs DMA operations longer than a page, it is the
// driver's responsibility to ensure that the addresses are reachable
// by the DMA channel.  In practice, this restricts the DMA problem to
// things like sound cards that need large buffers, and are likely to
// require fairly complex management strategies for dealing with those
// buffers.
//
// If the I/O size is less than a page, and falls outside the range
// reachable by the DMA processor, then the DMA setup routine will
// perform the operation to a bounce buffer instead.  The address
// returned by the DMA setup routine is the KERNEL VIRTUAL ADDRESS of
// the bounce buffer.
//
// Note that we do NOT check for crossing a 64K boundary.  If you are
// doing sector I/O, align it properly or it won't work.

const int NDMA=8;		// PC's have 8 channels.

Set32 allocatedChannels;

struct ChanInfo {
  kva_t addr;			// as supplied by user
  kva_t ioaddr;			// where we really did it :-)
  kva_t bounceBuf;		// ptr to bounce area, if any
  bool needFlush;		// whether copyout is needed
  uint32_t count;			// how many bytes

  ThreadPile sleepQueue;	// FIX: not sure this is right
};

static ChanInfo chanInfo[NDMA];

static uint8_t pagereg[8] = {
  0x80, 0x83, 0x81, 0x82, 0x8f, 0x8b, 0x89, 0x8a};

static const uint32_t dmaRange[8] = {
  0x10000u, 0x10000u, 0x10000u, 0x10000u,
  0x1000000u, 0x1000000u, 0x1000000u, 0x1000000u
};

struct DMAC {
  enum {
    FLOPPY   = 2,		// floppy DMA on channel 2
  };
};

struct DMA1 {
  enum {
    CMD      = 0x08,
    STATUS   = 0x08,
    MASK     = 0x0A,
    MODE     = 0x0B,
    CLEAR_FF = 0x0C,	// 0 for LSB, 1 for MSB
    RESET    = 0x0D,	// write to rest


    IOBASE   = 0x00,	// 8 bit slave DMA, channels 0..3
  };
};

struct DMA2 {
  enum {
    CMD      = 0xD0,
    STATUS   = 0xD0,
    MASK     = 0xD4,
    MODE     = 0xD6,
    CLEAR_FF = 0xD8,	// 0 for LSB, 1 for MSB
    RESET    = 0xDA,	// write to rest

    IOBASE   = 0xC0,	// 16 bit master DMA, channels 4..7
  };
};

struct MODE {
  enum mode_t {
    READ     = 0x44u,	// I/O to memory, increment, single mode
    WRITE    = 0x48u,	// memory to I/O, increment, single mode
    CASCADE  = 0x40u,	// cascade mode - DMA2 only!!
  };
};

// HELPER FUNCTIONS FIRST, so the declarations are visible below.

static void
clear_ff(uint8_t channel)
{
  if (channel<=3)
    out8(0, DMA1::CLEAR_FF);
  else
    out8(0, DMA2::CLEAR_FF);
}

static void
set_mode(uint8_t channel, MODE::mode_t mode)
{
  if (channel<=3)
    old_outb(DMA1::MODE, mode | channel);
  else
    old_outb(DMA2::MODE, MODE::CASCADE | mode |
	 (channel&3));
}

static void
set_page(uint8_t channel, uint8_t pageval)
{
  old_outb(pagereg[channel], pageval);
}

static void
set_addr(uint8_t channel, uint32_t wa)
{
  set_page(channel, wa>>16);
  uint16_t port;
  
  if (channel <= 3) {
    port = DMA1::IOBASE + ((channel & 3) << 1);
  } else {
    port = DMA2::IOBASE + ((channel & 3) << 2);
    wa >>= 1;
  }
  
  old_outb(port, wa & 0xff);
  old_outb(port, (wa >> 8) & 0xff);
}

static void
set_count(uint8_t channel, uint32_t count)
{
  uint16_t dc;
  uint16_t port;
  if (channel <= 3) {
    port = DMA1::IOBASE + ((channel & 3) << 1);
  } else {
    port = DMA2::IOBASE + ((channel & 3) << 2);
  }
  port += 1;
  
  if (channel>3) count >>=1; // word count
  /* Transfer size is one less than the count: */
  dc = count - 1;
  
  old_outb(port, dc & 0xff);
  old_outb(port, (dc>>8) & 0xff);
}

static void disable(uint8_t channel)
{
  if (channel <= 3)
    old_outb(DMA1::MASK, channel | 4);
  else
    old_outb(DMA2::MASK, (channel & 3) | 4);
}

static void enable(uint8_t channel)
{
  if (channel <= 3)
    old_outb(DMA1::MASK, channel);
  else
    old_outb(DMA2::MASK, (channel & 3));
}

static void
setup(DMA::chan_t chan, kva_t where, uint32_t count, MODE::mode_t mode)
//move all below into setup
{
  uint32_t pwhere = VTOP(where);
  
  disable(chan);
  
  clear_ff(chan);               // reset the flip flop

  set_mode(chan, mode);
  set_addr(chan, pwhere);
  set_count(chan, count);
  
  enable(chan);
}

DMA::chan_t DMA::alloc(chan_t chan)
{
  IRQ::DISABLE();

  if ( allocatedChannels.isSet(chan) ) {
    Thread::Current()->SleepOn(chanInfo[chan].sleepQueue);
    MsgLog::dprintf(true, "call to yield() within disable from DMA::alloc()\n");
    Thread::Current()->Yield();
  }

  allocatedChannels.set(chan);

  IRQ::ENABLE();

  return chan;
}

DMA::chan_t DMA::alloc(const chanset_t& channels)
{
  IRQ::DISABLE();

  Set32 avail = channels - allocatedChannels;
  
  if ( avail.isEmpty() ) {
    Thread::Current()->SleepOn(chanInfo[channels.firstSetBit()].sleepQueue);
    MsgLog::dprintf(true, "call to yield() within disable from DMA::alloc()\n");
    Thread::Current()->Yield();
  }

  chan_t chan = avail.firstSetBit();

  allocatedChannels.set(chan);

  IRQ::ENABLE();

  return chan;
}

void DMA::release(chan_t chan)
{
  IRQ::DISABLE();

  // Shap is paranoid. Tell me something I didn't know. 
  if (allocatedChannels.isSet(chan) == false)
    MsgLog::fatal("Releasing unset channel\n");

  allocatedChannels.unset(chan);
  // Wake up the next driver in line for this dma channel:
  chanInfo[chan].sleepQueue.WakeNext();

  IRQ::ENABLE();
}

uint32_t DMA::read(chan_t chan, kva_t buf, uint32_t count)
{
  kva_t addr = buf;
  kva_t ioaddr = buf;
  
  if (addr + count >= dmaRange[chan]) {
    chanInfo[chan].needFlush = true;

    ioaddr = (kva_t) chanInfo[chan].bounceBuf;
    // Don't overflow bounce buffer:
    if (count > EROS_PAGE_SIZE)
      count = EROS_PAGE_SIZE;
  }
  else {
    chanInfo[chan].needFlush = false;
  }

  chanInfo[chan].addr = addr;
  chanInfo[chan].ioaddr = ioaddr;
  chanInfo[chan].count = count;
  
  setup(chan, ioaddr, count, MODE::READ);

  return count;
}

uint32_t DMA::write(chan_t chan, kva_t buf, uint32_t count)
{
  kva_t addr = buf;
  kva_t ioaddr = buf;
  
  if (addr + count >= dmaRange[chan]) {
    ioaddr = chanInfo[chan].bounceBuf;

    // Don't overflow bounce buffer:
    if (count > EROS_PAGE_SIZE)
      count = EROS_PAGE_SIZE;

    bcopy((const void *) addr, (void *) ioaddr, count);
  }

  chanInfo[chan].addr = addr;
  chanInfo[chan].ioaddr = ioaddr;
  chanInfo[chan].count = count;
  chanInfo[chan].needFlush = false;
  
  setup(chan, ioaddr, count, MODE::WRITE);

  return count;
}
 
void DMA::flush(chan_t chan)
{
  if (!chanInfo[chan].needFlush)
    return;

  bcopy((const void *) chanInfo[chan].ioaddr, (void *) chanInfo[chan].addr, chanInfo[chan].count);

  // Don't flush twice - there's a drought!
  chanInfo[chan].needFlush = false;
}

void DMA::Init()
{
  // FIX: check for EISA/PCI someday!

  // allocate bounce buffers...
  chanInfo[0].bounceBuf = (kva_t) PhysMem::Alloc(EROS_PAGE_SIZE, McMem1M);
  chanInfo[1].bounceBuf = (kva_t) PhysMem::Alloc(EROS_PAGE_SIZE, McMem1M);
  chanInfo[2].bounceBuf = (kva_t) PhysMem::Alloc(EROS_PAGE_SIZE, McMem1M);
  chanInfo[3].bounceBuf = (kva_t) PhysMem::Alloc(EROS_PAGE_SIZE, McMem1M);

  // Only need bounce buffers for channels 4..7 if there is more than
  // 16M of memory.  Allocate these from the 16M region to maximize
  // the amount of available low memory:

  chanInfo[4].bounceBuf = 0;
  chanInfo[5].bounceBuf = 0;
  chanInfo[6].bounceBuf = 0;
  chanInfo[7].bounceBuf = 0;
  
  if (PhysMem::AvailPages(McMemHi)) {
    chanInfo[4].bounceBuf = (kva_t) PhysMem::Alloc(EROS_PAGE_SIZE, McMem16M);
    chanInfo[5].bounceBuf = (kva_t) PhysMem::Alloc(EROS_PAGE_SIZE, McMem16M);
    chanInfo[6].bounceBuf = (kva_t) PhysMem::Alloc(EROS_PAGE_SIZE, McMem16M);
    chanInfo[7].bounceBuf = (kva_t) PhysMem::Alloc(EROS_PAGE_SIZE, McMem16M);
  }
}
