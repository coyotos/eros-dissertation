/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/MsgLog.hxx>
#include "IDT.hxx"
#include "io.h"

uint8_t pic1_mask = 0xffu;
uint8_t pic2_mask = 0xffu;

// #define INTRDEBUG

// The X86 PIC uses disable mask rather than enable mask.  We use
// enable logic in the kernel because it is easier to initialize
// things that way, so...
uint32_t
IRQ::GetEnableMaskFromPIC()
{
  uint32_t disableMask = pic2_mask;
  disableMask <<= 8;
  disableMask |= pic1_mask;

  return ~disableMask;
}

void
IRQ::EnablePIC(uint32_t irq)
{
  IRQ::DISABLE();

  if (irq >= 8) {
    pic2_mask &= ~(1u << (irq-8));
    old_outb(0xa1, pic2_mask);
    // Make sure that the cascade entry on PIC1 is enabled as well (I
    // got caught by this at one point)
    if (pic1_mask & (1u << IRQ386::Cascade))
      irq = IRQ386::Cascade;
  }
  
  if (irq < 8) {
    pic1_mask &= ~(1u << irq);
    old_outb(0x21, pic1_mask);
  }

#ifdef INTRDEBUG
  if (irq == 3) MsgLog::printf("Enable IRQ line %d\n", irq);
#endif

  IRQ::ENABLE();
}

void
IRQ::DisablePIC(uint32_t irq)
{
  IRQ::DISABLE();

  if (irq < 8) {
    pic1_mask |= (1u << irq);
    old_outb(0x21, pic1_mask);
  }
  else {
    pic1_mask |= (1u << (irq-8));
    old_outb(0xa1, pic1_mask);
  }

#ifdef INTRDEBUG
  if (irq == 3) MsgLog::printf("Disable IRQ line %d\n", irq);
#endif

  IRQ::ENABLE();
}
