/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/DMA.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/util.hxx>
#include <kerninc/PhysMem.hxx>
#include <kerninc/Console.hxx>
#include <kerninc/SysTimer.hxx>
#include <kerninc/PCI.hxx>
#include <eros/TimeOfDay.h>
#include <eros/memory.h>
#include <eros/SysTraceKey.h>
#include "SysConfig.hxx"
#include "Log386.hxx"
#include <machine/PTE.hxx>
#include "CpuFeatures.hxx"
/* #include <machine/RegLayout.hxx> */
#include <kerninc/Process.hxx>
#include "CMOS.hxx"
#include "lostart.hxx"

#include "GDT.hxx"
#include "IDT.hxx"
#include "TSS.hxx"

#include <disk/LowVolume.hxx>	// for VolFlags

extern "C" {
  SysInfo* SysInfoPtr;

  extern void etext();
  extern void end();
  extern void start();
}

static void MapKernel();

// Machine::BootInit() -- first routine called by main() if we
// came into the kernel via the bootstrap code.
//
// On entry, we have a stack (created in lostart.S) and interrupts
// are disabled.  Static constructors have been run.
//
// Otherwise, the state of the machine is whatever environment
// the bootstrap code gave us (typically not much).  In particular,
// controllers and devices may be in arbitrarily inconsistent states
// unless the static constructors have ensured otherwise.
//
// On exit from this routine, the kernel should be running in
// an appropriate virtual map, and should have performed enough
// initialization to have a minimal interrupt table installed.
// Interrupts are disabled on entry, and should be enabled on exit.
// Note that on exit from this procedure the controller entry points
// have not yet been established (that will happen next).
//
// The implicit assumption here is that interrupt handling happens
// in two tiers, and that the procesor interrupts can be enabled
// without enabling all device interrupts.
//
// It is customary for this code to hand-initialize the console, so
// that boot-related diagnostics can be seen.

void Machine::BootInit()
{
  // copy the system BIOS information into a safe place before we
  // start dorking with the memory map:
  
  bcopy(SysInfoPtr, &SysConfig, sizeof(SysInfo));

  // Set up the boot console by hand so that we can do kernel
  // diagnostics during startup.  Note that the boot console is output
  // only, so it does not interact at all with the autoconfiguration
  // mechanism. 
  Console::Init();
  
  // On the 386, this can be done before enabling VM, which
  // is helpful.
  PhysMem::Init();
  
  // The kernel address space must, however, be constructed and
  // enabled before the GDT, IDT, and TSS descriptors are loaded,
  // because these descriptors reference linear addresses that change
  // when the mapping is updated.
  
  MapKernel();
  
  (void) Machine::SetMappingTable(KERNPAGEDIR); // Well known address!
  (void) Machine::EnableVirtualMapping();

  GDT::Init();
  
  //    MsgLog::printf("main(): loaded GDT\n");
  // Commented out to avoid missing symbol complaints
  
  IDT::Init();
  
//    MsgLog::printf("main(): loaded IDT\n");
  
  TSS::Init();
  
//    MsgLog::printf("main(): loaded TSS\n");

#if 0
  // Enable the following when you dork the process structure to
  // recompute the important offsets.
  MsgLog::printf("PROCESS_FIXREGS_OFFSET = %d\n",
		 offsetof(Process, fixRegs));
  MsgLog::printf("PROCESS_MAPTABLE_OFFSET = %d\n",
		 offsetof(Process, fixRegs.MappingTable));
  MsgLog::printf("PROCESS_V86_FIXREGS_TOP = %d\n",
		 offsetof(Process, fixRegs) + offsetof(fixregs_t, sndPtr));
  MsgLog::fatal("PROCESS_FIXREGS_TOP = %d\n",
		 offsetof(Process, fixRegs) + offsetof(fixregs_t, ES));
#endif
  
#if 0
  // Before enabling interrupts, convert the main procedure's stack
  // into a proper thread so that interrupting it will work correctly.
  Machine::BecomeFirstKernelThread();
#endif

  switch(BusArchitecture()) {
  case bt_Unknown:
    MsgLog::printf("Unknown bus type!\n");
    halt('u');
  case bt_ISA:
    MsgLog::printf("ISA bus\n");
    break;
  case bt_MCA:
    MsgLog::printf("MCA bus -- get a real machine!\n");
    break;
  case bt_PCI:
    MsgLog::printf("PCI bus\n");
    break;
  }

  DMA::Init();
  
  MsgLog::printf("Initializing PCI...\n");
  PCI::Init();
  
  assert(sizeof(uint16_t) == 2);
  assert(sizeof(uint32_t) == 4);
  assert(sizeof(void *) == 4);
  //  assert(sizeof(Key) == 12);
  assert(sizeof(fixregs_t) == EROS_SAVE_AREA_SIZE);

#if 0
  assert(sizeof(GenRegLayout) == EROS_NODE_SIZE * sizeof(Key));
  assert(sizeof(DomRegLayout) == EROS_NODE_SIZE * sizeof(Key));
#endif

  // Verify the queue key representation pun:
  assert(sizeof(ThreadPile) == 2 * sizeof(uint32_t));
  
  //  MsgLog::printf("Kernel is mapped. CR0 = 0x%x\n", flags);

  //  MsgLog::printf("Pre enable: intdepth=%d\n", IDT::intdepth);

  // We enable interrupts on the processor here.  Note that at this
  // point all of the hardware interrupts are disabled.  We need to
  // enable processor interrupts before we start autoconfiguring,
  // since some of the IRQ detection code will proceed by inducing an
  // interrupt to detect the configured IRQ.
  //
  // The down side to enabling here is that we have not yet set up the
  // exception handlers for processor generated exceptions.  Those
  // will get initialized very early in the autoconfiguration process.
  // The alternative would be a small redesign that would allow us to
  // initialize them by hand via a call to the AutoConfig logic here.
  //
  // In principle, the kernel ought to be completely free of
  // processor-generated exceptions, so the current design is probably
  // just fine.  This note is here mostly as a guideline for other
  // processors that may require different solutions.
  
#ifdef KERN_PROFILE
  // This must be done before the timer interrupt is enabled!!
  extern void InitKernelProfiler();
  InitKernelProfiler();
#endif

  if (SysConfig.ramdiskSz) {
    MsgLog::printf("Machine::init(): %d sector ramdisk found at 0x%x\n",
		 SysConfig.ramdiskSz,
		 SysConfig.ramdiskAddress);
  }
  else {
    MsgLog::printf("Machine::init(): no ramdisk\n",
		 SysConfig.ramdiskSz,
		 SysConfig.ramdiskAddress);
  }

#ifdef EROS_HAVE_FPU
  Machine::InitializeFPU();
#endif
  
  IRQ::ENABLE();
  
  Machine::InitHardClock();
  
  MsgLog::printf("Motherboard interrupts initialized\n");
}

uint32_t
Machine::GetRamDiskSize()
{
  return SysConfig.ramdiskSz;
}

uint32_t
Machine::GetRamDiskAddress()
{
  return PTOV(SysConfig.ramdiskAddress);
}

#ifdef SMALL_SPACES
#include <kerninc/Invocation.hxx>
void
Process::WriteDisableSmallSpaces()
{
  uint32_t nFrames = KTUNE_NCONTEXT / 32;
  PTE *pte = Process::smallSpaces;
  
  for (unsigned i = 0; i < nFrames * NPTE_PER_PAGE; i++)
    pte[i].WriteProtect();
}

static void
MakeSmallSpaces()
{
  assert (KTUNE_NCONTEXT % 32 == 0);

  /* Allocate the page *tables* for the small spaces. */
  
  uint32_t nFrames = KTUNE_NCONTEXT / 32;
  
  Process::smallSpaces = 
    (PTE *) PhysMem::Alloc(nFrames * EROS_PAGE_SIZE, McMemHi);

  bzero(Process::smallSpaces, nFrames * EROS_PAGE_SIZE);

  assert (((uint32_t)Process::smallSpaces & EROS_PAGE_MASK) == 0);

  PTE *pageTab = Process::smallSpaces;
  
  /* Insert those page tables into the master kernel map, from which
     they will be copied to all other spaces. */
  
  uint32_t vaddr = UMSGTOP;
  uint32_t dirndx = (vaddr >> 22);

  PTE *pageDir = (PTE*) PTOV(KERNPAGEDIR); // kernel page directory

  for (uint32_t i = 0; i < nFrames; i++) {
    PTE_SET(pageDir[dirndx], (VTOP(pageTab) & PTE_FRAMEBITS) );
    PTE_SET(pageDir[dirndx], PTE_W|PTE_DV|PTE_CV|PTE_CS|PTE_ACC|PTE_USER);

    pageTab += NPTE_PER_PAGE;
    dirndx++;
  }
}
#endif
  
/* Build a kernel mapping table.  In the initial construction, we
   create TWO mappings for the kernel.  The first begins at VA=0x0,
   and is used until we get a chance to set up a new global descriptor
   table.  The second begins at KVA, and goes into effect after we set
   up a global descriptor table for the kernel.

   Since the page table entries for the two mappings are identical,
   the duplication of mapping has essentially no cost.  There is also
   no need to undo the duplication, as all of the mappings in question
   are supervisor-only.
   */
static void
MapKernel()
{
  PTE *pageDir = (PTE*) PTOV(KERNPAGEDIR); // kernel page directory
  bzero(pageDir, EROS_PAGE_SIZE);

  PTE *pageTab = 0;

  // The kernel mapping table must include all of the physical pages,
  // including the ramdisk if any:
  uint32_t physPages = PhysMem::PhysicalPages();

  //  MsgLog::printf("pageDir: 0x%x\n", pageDir);

  // Computation of the virtual address must user paddr + KVA
  // because we are using segmentation tricks to make kernel segment
  // offsets be the same be the same as physical addresses
  
  uint32_t increment = 1;		// increment in pages by default

  uint32_t globalPage = 0;

#ifndef NO_GLOBAL_PAGES
  if (CpuIdHi > 1 && CpuFeatures & CPUFEAT_PGE)
    globalPage = PTE_GLBL;
#endif
  
  bool supports_large_pages = false;
#ifndef NO_LARGE_PAGES
  if (CpuIdHi > 1 && CpuFeatures & CPUFEAT_PSE) {
    supports_large_pages = true;
  }
#endif
  
#ifndef NO_LARGE_PAGES
  if (supports_large_pages) {
    // Enable the page size extensions:
    
    __asm__ __volatile__ ("\tmov %%cr4,%%eax\n"
			  "\torl $0x10,%%eax\n"
			  "\tmov %%eax,%%cr4\n"
			  : /* no outputs */
			  : /* no inputs */
			  : "ax" /* smash any convenient register */);

    increment = 1024;		// increment in large pages
  }
#endif

#ifndef NO_GLOBAL_PAGES
  if (globalPage) {
    // Enable the page size extensions:
    
    __asm__ __volatile__ ("\tmov %%cr4,%%eax\n"
			  "\torl $0x80,%%eax\n"
			  "\tmov %%eax,%%cr4\n"
			  : /* no outputs */
			  : /* no inputs */
			  : "ax" /* smash any convenient register */);

    increment = 1024;		// increment in large pages
  }
#endif

  for (uint32_t i = 0 ; i < physPages; i += increment) {
    uint32_t paddr = i * EROS_PAGE_SIZE;
    uint32_t vaddr = KVTOL(PTOV(paddr));
    uint32_t tabndx = (vaddr >> 12) & 0x3ffu;
    uint32_t dirndx = (vaddr >> 22);
    uint32_t pdirndx = (paddr >> 22);
      
#ifndef NO_LARGE_PAGES
    if (supports_large_pages) {
      assert (tabndx == 0);
      // Pentium or higher.  Use large pages:
      PTE_SET(pageDir[dirndx], (paddr & PTE_FRAMEBITS) );
      PTE_SET(pageDir[dirndx], PTE_W|PTE_DV|PTE_PGSZ|globalPage);
#ifdef WRITE_THROUGH
      PTE_SET(pageDir[dirndx], PTE_WT);
#endif
      pageDir[pdirndx] = pageDir[dirndx];
      continue;
    }
#endif

    if (tabndx == 0) {
      // Allocate a new page table. Allocating here also has the
      // property that all of the pages above those allocated for use
      // by the kernel will be contiguous in virtual space.
      pageTab = (PTE *) PhysMem::Alloc(EROS_PAGE_SIZE, McMemHi);
      bzero(pageTab, EROS_PAGE_SIZE);

      assert (((uint32_t)pageTab & EROS_PAGE_MASK) == 0);
      
      // pageTab = ::new PTE[NPTE_PER_PAGE];

#if 0
      MsgLog::printf("Allocated new page table at 0x%x, dirndx 0x%x\n",
		     pageTab, dirndx);
#endif
    
      PTE_SET(pageDir[dirndx], (VTOP(pageTab) & PTE_FRAMEBITS) );
      PTE_SET(pageDir[dirndx], PTE_W|PTE_DV|globalPage);
      pageDir[pdirndx] = pageDir[dirndx];
    }

    assert((paddr & EROS_PAGE_MASK) == 0);
    
    PTE_SET(pageTab[tabndx], (paddr & PTE_FRAMEBITS) );
    PTE_SET(pageTab[tabndx], PTE_DV|globalPage);

    // Note that the processor won't honor the writable bit in
    // supervisor mode, so there really isn't any point to setting it.
    // We could enable the WP bit in CR0, but then the DMA code would
    // be forced to check before flushing on inbound I/O.  Better,
    // IMHO, to trust the kernel from the start.
    
    if (paddr < (uint32_t) start ||
	paddr >= ((uint32_t)etext & PTE_FRAMEBITS)){
      PTE_SET(pageTab[tabndx], PTE_W);
#ifdef WRITE_THROUGH
      PTE_SET(pageTab[tabndx], PTE_WT);
#endif
      // Hand-protect kernel page zero, which holds the kernel page
      // directory, but is never referenced by virtual addresses once
      // we load the new mapping table pointer:
      if (vaddr == 0) 
	PTE_CLR(pageTab[tabndx], PTE_W|PTE_DV);
    }
  }

  /* BELOW THIS POINT we do not bother to duplicate mappings, since by
     the time any of these are needed we have already loaded the
     segment structures.

     ALSO, below this point we do not use the /globalPage/ bit, as
     these mappings are per-IPC and MUST get flushed when a context
     switch occurs.
  */
  
  // Reserve directory entries for the fast send buffer
  PTE::kern_fstbuf = &pageDir[KVTOL(KVA_FSTBUF) >> 22];

  // Set up mapping slots for the receive buffer page:
  pageTab = (PTE *) PhysMem::Alloc(EROS_PAGE_SIZE, McMemHi);
  assert (((uint32_t)pageTab & EROS_PAGE_MASK) == 0);
  bzero(pageTab, EROS_PAGE_SIZE);

  /* I AM NO LONGER CONVINCED THAT THIS IS NECESSARY in the contiguous 
     string case. Copying the user PDEs should be sufficient in that
     situation, and if we really needed to copy the PTEs were weren't
     going to see and TLB locality in any case. */
  PTE_SET(pageDir[KVTOL(KVA_PTEBUF) >> 22], (VTOP(pageTab) & PTE_FRAMEBITS) );
  PTE_SET(pageDir[KVTOL(KVA_PTEBUF) >> 22], PTE_W|PTE_DV );
  
  // Following is harmless on pre-pentium:
  PTE_CLR(pageDir[KVTOL(KVA_PTEBUF) >> 22], PTE_PGSZ );

  PTE *pte = &pageTab[(KVTOL(KVA_PTEBUF) >> 12) & 0x3ffu];

  PTE::kern_ptebuf = pte;

  // 64K message limit = 16 pages + 1 for unaligned.
  for (int j = 0; j < 17; j++) {
    PTE_SET(*pte, PTE_W|PTE_DV|PTE_DRTY|PTE_ACC);

#ifdef WRITE_THROUGH
    if (cpuType >= 5)
      PTE_SET(*pte, PTE_WT);
#endif
  
    pte++;
  }
  
#ifdef SMALL_SPACES
  MakeSmallSpaces();
#endif
  
#if 0
  MsgLog::printf("Built Kernel Page Map!\n");
#endif
}


static inline
uint32_t BcdToBin(uint32_t val)
{
  return ((val)=((val)&15) + ((val)>>4)*10);
}

inline bool IsLeapYear(uint32_t yr)
{
  if (yr % 400 == 0)
    return true;
  if (yr % 100 == 0)
    return false;
  if (yr % 4 == 0)
    return true;
  return false;
}

inline static uint32_t yeartoday(unsigned year)
{
  return (IsLeapYear(year) ? 366 : 365);
}

void
Machine::GetHardwareTimeOfDay(TimeOfDay& tod)
{
  static uint32_t month_length[12] = {
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
  };
  
  tod.sec = CMOS::cmosByte(0x0);
  tod.min = CMOS::cmosByte(0x2);
  tod.hr = CMOS::cmosByte(0x4);
  tod.dayOfWeek = CMOS::cmosByte(0x6);
  tod.dayOfMonth = CMOS::cmosByte(0x7);
  tod.month = CMOS::cmosByte(0x8);
  tod.year = CMOS::cmosByte(0x9);
  
  tod.sec = BcdToBin(tod.sec);
  tod.min = BcdToBin(tod.min);
  tod.hr = BcdToBin(tod.hr);
  tod.dayOfMonth = BcdToBin(tod.dayOfMonth);
  tod.dayOfWeek = BcdToBin(tod.dayOfWeek);
  tod.month = BcdToBin(tod.month);
  tod.year = BcdToBin(tod.year);
  if (tod.year < 70)		// correct for y2k rollover
    tod.year += 100;
  
  tod.year += 1900;		// correct for century.
  
  tod.dayOfYear = 0;
  for (uint32_t i = 0; i < tod.month; i++)
    tod.dayOfYear += month_length[i];

  tod.dayOfYear += tod.dayOfMonth;

  if (tod.month > 1 && IsLeapYear(tod.year))
    tod.dayOfYear++;

  /* Compute coordinated universal time: */
  tod.utcDay = 0;
  for (uint32_t yr = 1970; yr < tod.year; yr++)
    tod.utcDay += yeartoday(yr);
  tod.utcDay += tod.dayOfYear;
}

// This cannot be run until the kernel has it's own map -- the gift
// map we get from the bootstrap code at the moment is too small.
uint32_t
Machine::BusArchitecture()
{
  static uint32_t busType = bt_Unknown;

  if (busType != bt_Unknown)
    return busType;
  
  busType = bt_ISA;

  if (memcmp((char*)0x0FFFD9, "EISA", 4) == 0)
    busType = bt_EISA;

  if (PciBios::Present())
    busType = bt_PCI;

  return busType;
}

void
Machine::SpinWaitMs(uint32_t ms)
{
  uint64_t ticks = MillisecondsToTicks(ms);

  uint64_t start = SysTimer::Now();
  uint64_t end = start + ticks + 1;

  while (SysTimer::Now() < end)
    ;
}

#if 0
// Map the passed physical pages starting at the designated kernel
// virtual address:
void
Machine::MapBuffer(kva_t va, kpa_t p0, kpa_t p1)
{
  const uint32_t ndx0 = (KVTOL(va) >> 22) & 0x3ffu;
  const uint32_t ndx1 = (KVTOL(va) >> 12) & 0x3ffu;

  kpa_t maptbl_paddr;
  
  __asm__ __volatile__("movl %%cr3, %0"
		       : "=r" (maptbl_paddr)
		       : /* no inputs */);

  PTE *pageTbl = (PTE*) PTOV(maptbl_paddr);
  pageTbl = (PTE*) PTOV( (pageTbl[ndx0].AsWord() & ~EROS_PAGE_MASK) );

#ifdef MSGDEBUG
  MsgLog::printf("MapBuffer: pg tb = 0x%08x p0=0x%08x\n",
		 pageTbl, p0);
#endif
  
  PTE *pte = pageTbl + ndx1;

  // These PTE's are already marked present, writable, etc. etc. by
  // construction in the kernel mapping table - just update the
  // frames.
  (*pte) = p0;
  pte++;
  if (p1) {
    (*pte) = p1;
    PTE_SET(*pte, PTE_DV);
  }
  else
    PTE_CLR(*pte, PTE_DV);

  if (CpuType > 3) {
    Machine::FlushTLB(KVTOL(va));
    Machine::FlushTLB(KVTOL(va + 4096));
  }
  else
    Machine::FlushTLB();
}
#endif

// non-static because compiler cannot detect ASM usage and complains
uint32_t BogusIDTDescriptor[2] = { 0, 0 };

void
Machine::HardReset()
{
  // Load an IDT with no valid entries, which will force a machine
  // check when the next interrupt occurs:
  
  __asm__ __volatile__("lidt BogusIDTDescriptor"
		       : /* no output */
		       : /* no input */
		       : "memory");

  // now force an interrupt:
  __asm__ ("int $0x30");
}

// Note that the RETURN from this procedure flushes the I prefetch
// cache, which is why it is not inline.
void
Machine::SetMappingTable(kpa_t pAddr)
{
  __asm__ __volatile__("movl %0,%%cr3"
		       : /* no outputs */
		       : "r" (pAddr) );
}

kpa_t
Machine::GetMappingTable()
{
  kpa_t result;
  
  __asm__ __volatile__("movl %%cr3,%0"
		       : "=r" (result));
  return result;
}

void
Machine::EnableVirtualMapping()
{
  __asm__ __volatile__("movl %%cr0,%%eax\n\t"
		       "orl $0x80010000,%%eax\n\t"
		       "movl %%eax,%%cr0\n\t"	/* Turn on PG,WP bits. */
		       : /* no outputs */
		       : /* no inputs */
		       : "ax" /* eax smashed */);
}

#ifdef EROS_HAVE_FPU
void
Machine::InitializeFPU()
{
  __asm__ __volatile__("fninit\n\t"
		       "smsw %%ax\n\t"
		       "orw $0x1a,%%eax\n\t"
		       "lmsw %%ax\n\t"	/* Turn on TS,MP bits. */
		       : /* no outputs */
		       : /* no inputs */
		       : "ax" /* eax smashed */);
}

void
Machine::DisableFPU()
{
  __asm__ __volatile__("smsw %%ax\n\t"
		       "orw $0xa,%%eax\n\t"
		       "lmsw %%ax\n\t"	/* Turn on TS,MP bits. */
		       : /* no outputs */
		       : /* no inputs */
		       : "ax" /* eax smashed */);
}

void
Machine::EnableFPU()
{
  __asm__ __volatile__("smsw %%ax\n\t"
		       "andl $0xfff1,%%eax\n\t"
		       "lmsw %%ax\n\t"	/* Turn off TS,EM,MP bits. */
		       : /* no outputs */
		       : /* no inputs */
		       : "ax" /* eax smashed */);
}
#endif

bool
Machine::IsDebugBoot()
{
#ifdef DBG_WILD_PTR
  return true;
#else
  if (SysConfig.bootFlags & VolHdr::VF_DEBUG)
    return true;
  return false;
#endif
}

extern "C" {
  extern uint32_t CpuType;
  extern const char CpuVendor[];
	   }

const char *
Machine::GetCpuVendor()
{
  return CpuVendor;
}

uint32_t
Machine::GetCpuType()
{
  return CpuType;
}

// Following probably would be easier to do in assembler, but updating
// the mode table is a lot easier to do in C++.  Second argument says
// whether we wish to count cycles (1) or events (2).  Generally we
// will want events.

extern "C" {
  extern void Pentium_SetCounterMode(uint32_t mode, uint32_t wantCy);
  extern void PentiumPro_SetCounterMode(uint32_t mode, uint32_t wantCy);
	   }

static const char * ModeNames[SysTrace_Mode_NumCommonMode] = {
  "Cycles",
  "Instrs",
  "DTLB",
  "ITLB",
  "Dmiss",
  "Imiss",
  "Dwrtbk",
  "Dfetch",
  "Ifetch",
  "Branch",
  "TkBrnch",
};
static uint32_t PentiumModes[SysTrace_Mode_NumCommonMode] = {
  0x0,				/* SysTrace_Mode_Cycles	*/
  0x16,				/* SysTrace_Mode_Instrs	*/
  0x02,				/* SysTrace_Mode_DTLB	*/
  0x0d,				/* SysTrace_Mode_ITLB	*/
  0x29,				/* SysTrace_Mode_Dmiss	*/
  0x0e,				/* SysTrace_Mode_Imiss	*/
  0x06,				/* SysTrace_Mode_Dwrtbk	*/
  0x28,				/* SysTrace_Mode_Dfetch	*/
  0x0c,				/* SysTrace_Mode_Ifetch	*/
  0x12,				/* SysTrace_Mode_Branches */
  0x14,				/* SysTrace_Mode_BrTaken */
};
static uint32_t PentiumProModes[SysTrace_Mode_NumCommonMode] = {
  0x79,				/* SysTrace_Mode_Cycles	*/
  0xc0,				/* SysTrace_Mode_Instrs	*/
  0x0,		/* no analog */	/* SysTrace_Mode_DTLB	*/
  0x85,				/* SysTrace_Mode_ITLB	*/
  0x45,				/* SysTrace_Mode_Dmiss	*/
  0x81,				/* SysTrace_Mode_Imiss	*/
  0x0,          /* no analog */	/* SysTrace_Mode_Dwrtbk	*/
  0x43,				/* SysTrace_Mode_Dfetch	*/
  0x80,				/* SysTrace_Mode_Ifetch	*/
  0xc4,				/* SysTrace_Mode_Branches */
  0xc9,				/* SysTrace_Mode_BrTaken */
};

/* Other possible modes of interest:

   Pentium   Ppro    What
   0x17      0x0     V-pipe instrs
   0x19      0x04    WB-full stalls
   0x1a      ??      mem read stalls   
   0x13      0xca    btb hits (ppro: retired taken mispredicted branches)
   0x0       0x65    Dcache reads (ppro: burst read transactions)
   0x1f      ??      Agen interlocks */

bool
Machine::SetCounterMode(uint32_t mode)
{
  uint32_t wantcy = (mode == SysTrace_Mode_Cycles) ? 1 : 0;
  if (mode >= SysTrace_Mode_NumCommonMode)
    return false;

  if (CpuType == 5) {
    Pentium_SetCounterMode(PentiumModes[mode], wantcy);
  }
  else if (CpuType == 6) {
    PentiumPro_SetCounterMode(PentiumProModes[mode], wantcy);
  }

  return true;
}

const char *
Machine::ModeName(uint32_t mode)
{
  if (mode >= SysTrace_Mode_NumCommonMode)
    return "???";

  return ModeNames[mode];
}

