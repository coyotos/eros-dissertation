/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Drivers for 386 protection faults

#include <eros/target.h>
#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/Debug.hxx>
#include <kerninc/util.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/ObjectCache.hxx>
#include <machine/Process.hxx>
#include "IDT.hxx"
#include "lostart.hxx"

#define pi_copy_cap  0		/* handled in fast path */
#define pi_xchg_cap  1		/* handled in fast path */
#define pi_load_cap  2		/* handled here */
#define pi_store_cap 3		/* handled here */

#define dbg_capstore	0x1
#define dbg_capload	0x2

/* Following should be an OR of some of the above */
#define dbg_flags	( 0u )

#define DEBUG(x) if (dbg_##x & dbg_flags)

extern void Desensitize(Key *k);
extern bool PteZapped;

// STORE_KEY:
// Known properties of *to:
//
//   1. it is not prepared because it is in a capability page.
//   2. it is not hazarded for the same reason.
//   3. it is a KVA!!!
//
// Known properties of *keyReg:
//
//   1. it is not hazarded because it is in a capability register.

// LOAD_KEY:
// Known properties of *from:
//
//   1. it is not prepared because it is in a capability page.
//   2. it is not hazarded for the same reason.
//   3. it is a KVA!!!
//
// Known properties of *keyReg:
//
//   1. it is not hazarded because it is in a capability register.

void
PseudoInstrException(fixregs_t *sa)
{
  if ( sa_IsKernel(sa) ) {
    halt('p');
  }

  Process* ctxt = (Process*) Thread::CurContext();

  // Because this path calls the page fault handler, we must establish
  // a thread recovery block here.

  assert(& ctxt->fixRegs == sa);

  PteZapped = false;
  ObjectHeader::BeginTransaction();

#ifdef SMALL_SPACES
      uint32_t bias = ctxt->bias;
#else
      uint32_t bias = 0;
#endif

  switch (sa->EAX) {
  case pi_load_cap:
    {
      ula_t ula = sa->ECX + bias;
      if (ula % sizeof(Key)) {
	((Process*) Thread::CurContext())->SetFault(FC_Alignment,
						    sa->EIP, false);
	break;
      }
	
      if (sa->EBX >= EROS_NODE_SIZE) {
	((Process*) Thread::CurContext())->SetFault(FC_BadOpcode,
						    sa->EIP, false);
	break;
      }
      
      DEBUG(capload)
	MsgLog::dprintf(true, "Load cap from ula 0x%x ula 0x%x\n",
			sa->ECX, ula);
      
      PTE *pte0 = proc_TranslatePage(ctxt, ula, PTE_CV, false);
      if (pte0 == 0) {
	uint32_t retry;
	for (retry = 0; pte0 == 0 && retry < 4; retry++) {
	  if (ctxt->DoPageFault(ula, false, true, false) == false)
	    assert(false);

	  pte0 = proc_TranslatePage(ctxt, ula, PTE_CV, false);
#if 0
	  DEBUG(capload)
	    MsgLog::dprintf(true, "Resulting PTE* is 0x%08x\n", pte);
#endif
	}
      }
      if (pte0 == 0)
	MsgLog::fatal("No translation\n");

      PTE *pte1 = proc_TranslatePage(ctxt, ula, PTE_CV|PTE_CS, false);

      uint32_t addr = (uint32_t) sa->ECX + bias + KUVA;

      DEBUG(capload)
	MsgLog::dprintf(true, "Load cap from ula 0x%x kva is 0x%x\n",
			ula, addr);

      Key *kr = &ctxt->keyReg[sa->EBX];
      Key *km = (Key *)addr;

      assert(km->IsUnprepared());

      kr->NH_Set(*km);
      if (!pte1)
	Desensitize(kr);

      break;
    }
    
  case pi_store_cap:
    {
#ifdef SMALL_SPACES
      ula_t ula = sa->ECX + bias;
#else
      ula_t ula = sa->ECX;
#endif
      if (ula % sizeof(Key)) {
	((Process*) Thread::CurContext())->SetFault(FC_Alignment,
						    sa->EIP, false);
	break;
      }
	
      if (sa->EBX >= EROS_NODE_SIZE) {
	((Process*) Thread::CurContext())->SetFault(FC_BadOpcode,
						    sa->EIP, false);
	break;
      }
      
      DEBUG(capstore)
	MsgLog::dprintf(true, "Store cap to ula 0x%x ula 0x%x\n",
			sa->ECX, ula);
      
      PTE *pte0 = proc_TranslatePage(ctxt, ula, PTE_CV|PTE_W, false);
      if (pte0 == 0) {
	uint32_t retry;
	for (retry = 0; pte0 == 0 && retry < 4; retry++) {
	  if (ctxt->DoPageFault(ula, true, true, false) == false)
	    assert(false);

	  pte0 = proc_TranslatePage(ctxt, ula, PTE_CV|PTE_W, true);
#if 0
	  DEBUG(capstore)
	    MsgLog::dprintf(true, "Resulting PTE* is 0x%08x\n", pte);
#endif
	}
      }
      if (pte0 == 0)
	MsgLog::fatal("No translation\n");

      uint32_t addr = (uint32_t) sa->ECX + bias + KUVA;
      Key *kr = &ctxt->keyReg[sa->EBX];
      Key *km = (Key *)addr;

      DEBUG(capstore)
	MsgLog::dprintf(true, "store cap to ula 0x%x kva is 0x%x\n",
			ula, addr);

      assert(km->IsUnprepared());

      km->NH_Set(*kr);
      if (km->IsPreparedObjectKey())
	km->NH_Unprepare();

      assert(km->IsUnprepared());

      DEBUG(capstore)
	MsgLog::dprintf(true, "store cap completed to kva is 0x%x\n",
			addr);


      break;
    }
    
  default:
    ((Process*) Thread::CurContext())->SetFault(FC_BadOpcode, sa->EIP, 
						false);
    break;
  }
}
