/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Drivers for 386 protection faults

#include <eros/target.h>
#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/Debug.hxx>
#include <kerninc/util.hxx>
#include <kerninc/Process.hxx>
#include "IDT.hxx"
#include "lostart.hxx"

void
GPFault(fixregs_t *sa)
{
  if ( sa_IsKernel(sa) ) {
    MsgLog::printf("Kernel GP fault. curctxt=0x%08x error=0x%x eip=0x%08x\n",
		   Thread::CurContext(),
		   sa->Error, sa->EIP);
#if 0
    MsgLog::printf("domRoot=0x%08x, keyRegs=0x%08x\n",
    		   Thread::CurContext()
		   ? ((ArchContext*)Thread::CurContext())->procRoot
		   : 0,
    		   Thread::CurContext()
		   ? ((ArchContext*)Thread::CurContext())->keyRegs
		   : 0);
    MsgLog::printf("0x%x Ctxt hzrd=0x%08x 0x%x hzrd rlu=0x%08x\n",
		   sa->EDX,
		   ((ArchContext*) sa->EDX)->hazards,
		   sa->EDI,
		   ((ArchContext*) sa->EDI)->hazards);
    MsgLog::printf("0x%x Ctxt rlu=0x%08x 0x%x ctxt rlu=0x%08x\n",
		   sa->EDX,
		   ((ArchContext*) sa->EDX)->fixRegs.ReloadUnits,
		   sa->EDI,
		   ((ArchContext*) sa->EDI)->fixRegs.ReloadUnits);
    MsgLog::printf("0x%x Ctxt keys=0x%08x 0x%x ctxt keys=0x%08x\n",
		   sa->EDX,
		   ((ArchContext*) sa->EDX)->keyRegs,
		   sa->EDI,
		   ((ArchContext*) sa->EDI)->keyRegs);
    MsgLog::printf("0x%x Ctxt EDX=0x%08x (offset %u)\n",
		   sa->EDX,
		   ((ArchContext*) sa->EDX)->fixRegs.EDX,
		   & ((ArchContext*) 0)->fixRegs.EDX );
#endif
#if 1
    DumpFixRegs(sa);
#endif
#if 0
    Debug::Backtrace();
#endif
    halt('t');
  }

  // If the general protection fault is on a domain marked as a small
  // space domain, upgrade the domain to a large space domain and let
  // it retry the reference before concluding that we should take this
  // fault seriously.
#ifdef SMALL_SPACES
  if (Thread::CurContext()->smallPTE) {
#if 0
    MsgLog::dprintf(true, "Small Space domain takes GP fault\n");
#endif
    Thread::CurContext()->SwitchToLargeSpace();
    return;
  }
#endif
  
  Node *domRoot = ((Process*) Thread::CurContext())->procRoot;
  MsgLog::printf("Domain ");
  MsgLog::print(domRoot->ob.oid);
  MsgLog::printf(" takes GP fault. error=0x%x eip=0x%08x\n",
	      sa->Error, sa->EIP);

#if 0
  sa->Dump();
#endif
  if (Thread::CurContext())
    ((Process*) Thread::CurContext())->SetFault(FC_GenProtection,
						sa->EIP, false);
}
