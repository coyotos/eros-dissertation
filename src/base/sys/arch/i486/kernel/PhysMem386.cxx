/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/util.hxx>
// #include "CMOS.hxx"
#include <kerninc/PhysMem.hxx>
#include "SysConfig.hxx"
     
// #define ALLOCDEBUG

// Implementation of Memory Allocation
     
const uint32_t BASIC_MEMORY_START = PTOV(0x1000u); // 1K - BIOS parameter page
const uint32_t BASIC_MEMORY_END   = PTOV(0xA0000u); // video start

const uint32_t MID_MEMORY_START   = PTOV(0x100000u); // 1 Mbyte
const uint32_t MID_MEMORY_END     = PTOV(0x1000000u); // 16Mbyte
const uint32_t HI_MEMORY_START    = PTOV(0x1000000u); // 16 Mbyte

extern "C" {
  extern int end;
}

void
PhysMem::Init()
{
  * ((uint32_t *) PTOV(0x000b8000)) = 0x074D0750;	// "PM"
  * ((uint32_t *) PTOV(0x000b8004)) = 0x07200720;	// "  "

  assert( MID_MEMORY_START == PTOV(KERNPAGEDIR) );

  /*
   * kernel loads into low memory, so first region starts just
   * above 'end'.  Mid region must avoid kernel page directory.
   */
#if KERNPBASE < BASIC_MEMORY_END
  memClass[McMem1M].base = (uint32_t) &end;
  memClass[McMem16M].base = MID_MEMORY_START + 4096u;
  memClass[McMemHi].base = HI_MEMORY_START;

  /*
   * I'm not aware of any guarantee that 'end' is word aligned, so
   * be paranoid:
   */
  
  memClass[McMem1M].base += 3u;
  memClass[McMem1M].base &= ~0x3u;
#else
  memClass[McMem1M].base = BASIC_MEMORY_START;
  memClass[McMem16M].base = (uint32_t) &end;
  memClass[McMemHi].base = HI_MEMORY_START;

  /*
   * I'm not aware of any guarantee that 'end' is word aligned, so
   * be paranoid:
   */
  
  memClass[McMem16M].base += 3;
  memClass[McMem16M].base &= ~0x3u;
#endif
  memClass[McMem1M].orig_base = memClass[McMem1M].base;
  memClass[McMem16M].orig_base = memClass[McMem16M].base;
  memClass[McMemHi].orig_base = memClass[McMemHi].base;

  memClass[McMem1M].top = BASIC_MEMORY_END;
  memClass[McMem16M].top = memClass[McMem16M].base;
  memClass[McMemHi].top = memClass[McMemHi].base;

  physMemTop = SysConfig.mem.Extended() + MID_MEMORY_START;
  if (SysConfig.ramdiskSz)
    kernMemTop = PTOV(SysConfig.ramdiskAddress);
  else
    kernMemTop = physMemTop;

  assert(physMemTop % EROS_PAGE_SIZE == 0);
  assert(kernMemTop % EROS_PAGE_SIZE == 0);

  if (kernMemTop > memClass[McMem16M].top)
    memClass[McMem16M].top = min(kernMemTop, MID_MEMORY_END);
  if (kernMemTop > memClass[McMemHi].top)
    memClass[McMemHi].top = kernMemTop;
    
#ifdef ALLOCDEBUG
  MsgLog::printf("PhysMemTop: 0x%08x KernMemTop: 0x%08x\n", physMemTop,
	      kernMemTop); 
  MsgLog::printf("Available 1M memory:  base 0x%x  top 0x%x\n",
	       memClass[McMem1M].base, memClass[McMem1M].top);
  MsgLog::printf("Available 16M memory:  base 0x%x  top 0x%x\n",
	       memClass[McMem16M].base, memClass[McMem16M].top);
  MsgLog::printf("Available Hi memory:  base 0x%x  top 0x%x\n",
	       memClass[McMemHi].base, memClass[McMemHi].top);
#else
  MsgLog::printf("Available memory: %dK low %dK mid %dK hi\n",
	       (memClass[McMem1M].top - memClass[McMem1M].base) / 1024,
	       (memClass[McMem16M].top - memClass[McMem16M].base) / 1024,
	       (memClass[McMemHi].top - memClass[McMemHi].base) / 1024);
#endif
}

