#ifndef __SYSINFO_HXX__
#define __SYSINFO_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>

/*
 * SysInfo structure is used to transfer information from the boot
 * block to the kernel regarding system configuration.
 *
 * A geometry description containing only zeros indicates a drive that
 * the BIOS did not perceive.
 */

struct BiosGeometry {
  unsigned char  drive;			// as reported by BIOS.
  unsigned char  secs;
  unsigned short cyls;
  unsigned char  heads;
  unsigned long  spcyl;
} ;

struct SysInfo {

  BiosGeometry bootGeom;

  struct {			// boot partition start description
    uint32_t head;
    uint32_t cyl;
    uint32_t sec;
    uint32_t startSec;
  } boot;

  uint8_t  boot_drive;
  uint8_t  load_drive;

  enum { N_BIOS_GEOM = 8 };

  BiosGeometry driveGeom[N_BIOS_GEOM];	// BIOS info on drive geometries

  struct {			// BIOS info on memory configuration
    uint32_t low;
    uint32_t ext;

    uint32_t Extended()
    {
      return ext * 1024;
    }
  } mem;

  uint32_t bootFlags;
  uint32_t ramdiskSz;		// size of ramdisk, if any, in sectors
  uint32_t ramdiskAddress;		// where ramdisk can be found
} ;

#endif /* __SYSINFO_HXX__ */
