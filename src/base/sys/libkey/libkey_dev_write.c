/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/DeviceKey.h>
#include <eros/memory.h>

uint32_t
device_write(uint32_t krDevice, uint32_t startSec, uint32_t nSec, uint8_t *buf)
{
  Message msg;
  bzero(&msg, sizeof(msg));

  msg.snd_w1 = startSec;
  msg.snd_w2 = nSec;

  msg.snd_w3 = 0;

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;

  msg.snd_len = nSec * EROS_SECTOR_SIZE;
  msg.snd_data = buf;
  
  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;
  msg.rcv_len = 0;

  msg.snd_invKey = krDevice;
  msg.snd_code = OC_Device_Write;
  
  return CALL(&msg);
}
