#ifndef __LOWVOLUME_HXX__
#define __LOWVOLUME_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <disk/DiskKey.hxx>

// Low-level portion of the EROS volume structure.
// This header file is included by things like boot code.  Statically
// allocated objects in this header or anything it includes are a
// profoundly bad idea.
//
// EROS volume structure.  EROS volumes contain:
//
//	1. A Boot Page		(Page 0: must be error free)
//	2. A Division Table	(Page 1: must be error free)
//	3. A spare sector region
//	3. Other divisions as defined by the user

// In principle, these classes probably ought to live in distinct
// header files.  In practice, there is essentially nothing you can do
// with a volume unless you have the division table and the bad map
// table.  Rather than keep the relationships in multiple files, it
// seemed better to put them all in one place.

enum DivType {

#define __DIVDECL(x) dt_##x,
#include <disk/DivTypes.hxx>
#undef __DIVDECL

  dt_NUMDIV,			// highest numbered division type
};

#ifdef __KERNEL__
#define BOOT_DISK_DIVISION 0xffff
#endif

// Division Table Entry:
class Division {
  static const char* DivTypeNames[dt_NUMDIV];
public:
  uint32_t start;
  uint32_t end;
  
  OID startOid;
  OID endOid;

  uint8_t type;		// see division type enum, above
  uint8_t flags;
  
  Division()
    { flags = 0; type = dt_Unused; start = 0; end = 0; }

  const char* TypeName() const
    { return DivTypeNames[type]; }

  static const char* TypeName(int dtype)
    { return DivTypeNames[dtype]; }
  
  void operator=(const Division& that)
    {
      flags = that.flags;
      type = that.type;
      start = that.start;
      end = that.end;
      startOid = that.startOid;
      endOid = that.endOid;
    }

  bool Contains(const OID&) const;
} ;

// Bad sector mapping info.
struct BadEnt {
  unsigned long badSec;	/* the sector being replaced */
  unsigned long goodSec;	/* the sector we replaced it with */

  BadEnt()
  { badSec = 0; goodSec = 0; }
} ;

enum {
  NDIVENT = 64,
  NBADENT = ((EROS_PAGE_SIZE - (64*sizeof(Division))) /
	     sizeof(BadEnt))
};

#if defined(i386) || defined(i486)
#define VOLHDR_VERSION 1

struct VolHdr {
  enum Flags {
    VF_BOOT       = 0x1,	// load volume image as ramdisk
    VF_RAMDISK    = 0x2,	// load volume image as ramdisk
    VF_IPL        = 0x4,	// This is an IPL volume.
    VF_COMPRESSED = 0x8,	// Volume is compressed.
    VF_DEBUG      = 0x80000000,	// Debugging boot
  };

  char        code[8];		// leading jump instr
  uint32_t    HdrVersion;
  uint32_t    PageSize;
  uint32_t    DivTable;
  uint32_t    AltDivTable;
  uint32_t    BootFlags;
  uint32_t    BootSectors;		// Boot block size
  uint32_t    VolSectors;		// number of sectors actually written
				// to this volume by the formatter.
  uint32_t    zipLen;		// length of zipped portion of volume
  DiskKey     iplKey;		// domain key of IPL domain
  uint64_t    iplSysId;		// Unique system identifier

  uint8_t     signature[4];	// 'E' 'R' 'O' 'S'
} ;
#endif

#endif // __VOLUME_HXX__
