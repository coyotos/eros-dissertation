#ifndef __LSS_HXX__
#define __LSS_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
// must NOT include memory.h, as this is sometimes included from cross
// tools!

struct LSS {
#ifndef __KERNEL__
  static uint32_t BiasedLSS(uint64_t);
#endif
  static uint32_t SlotNdx(uint64_t offset, uint32_t blss);
  static uint64_t Mask(uint32_t blss);
};

#endif // __LSS_HXX__
