#ifndef __DISKNODE_HXX__
#define __DISKNODE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// DiskNode.hxx: Declaration of a the Disk format of a Node.
//
// A DiskNode is the form that a Node takes when it appears on disk.
// DiskNodes are collected into pages known as Node Pots.  When a Node
// needs to be brought in from the disk, the containing Node Pot is
// brought in and the Node is read out of that.  DiskNodes ONLY appear
// within NodePots.
//
// Something about the number of Nodes per node pot ought to go
// here.
//
// DiskNodes are VERY simple objects.  They are just smart enough to
// convert themselves to/from ordinary Nodes.


#include "DiskKey.hxx"

#ifdef __KERNEL__
class Node;
#endif

class DiskNode {
#ifdef __KERNEL__
  friend class Node;
#endif
public:
  ObCount allocCount;
  ObCount callCount;
  OID oid;

  // Following is private solely to conform to the usage in Node.hxx,
  // where hazards are a possibility.
private:
  DiskKey slot[EROS_NODE_SIZE];

public:
  

  inline DiskNode()
  {
  }

  inline DiskKey& operator[](int n)
  {
    return slot[n];
  }
  inline const DiskKey& operator[](int n) const
  {
    return slot[n];
  }

  // Copy from another DiskNode
  DiskNode& operator=(const DiskNode&);

#ifdef __KERNEL__
  // Copy from an in-core Node:
  void operator=(Node&);
#endif
} ;

#define DISK_NODES_PER_PAGE (EROS_PAGE_SIZE / sizeof(DiskNode))

#endif // __DISKNODE_HXX__
