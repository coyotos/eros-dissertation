/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/DiskNode.hxx>

#if 0
DiskNode::~DiskNode()
{
}
#endif

// Used by the migrator:
DiskNode&
DiskNode::operator=(const DiskNode& other)
{
  unsigned int i;
  for (i = 0; i < EROS_NODE_SIZE; i++)
    slot[i] = other[i];
  
  allocCount = other.allocCount;
  callCount = other.callCount;
  oid = other.oid;
  
  return *this;
}

// Implementation of DiskNode& DiskNode::operator=(Node& other) can be
// found in kerninc/kern_Node.cxx 
