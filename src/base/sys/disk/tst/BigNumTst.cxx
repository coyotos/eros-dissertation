/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <memory.h>

#include <disk/BigNum.hxx>

extern "C" {
    extern int	printf(const char *, ...);
};

void
print(BigNum& bn)
{
  printf("{ ");
  for (uint32_t i = BigNum::NWORDS-1; i > 0; i--)
    printf("%lu, ", bn.w[i]);
  printf("%lu }", bn.w[0]);
}

void
printx(BigNum& bn)
{
  printf("{ ");
  for (uint32_t i = BigNum::NWORDS-1; i > 0; i--)
    printf("%lx, ", bn.w[i]);
  printf("%lx }", bn.w[0]);
}

main()
{
    BigNum bn = 5;

    bn = bn + 2;
    printf("bn(5) + 2 = ");
    print(bn); printf("\n");

    bn = WORD_MAX;
    printf("bn(WORD_MAX) = ");
    print(bn); printf("\n");

    bn = WORD_MAX;
    bn += 1;

    printf("bn(WORD_MAX) + 1 = ");
    print(bn); printf("\n");

    bn = WORD_MAX;
    bn -= 1;

    printf("bn(WORD_MAX) - 1 = ");
    print(bn); printf("\n");

    bn = BigNum(0);
    bn.w[1] = 1;
    bn -= 1;

    printf("bn(1,0) - 1 = ");
    print(bn); printf("\n");

    bn = BigNum(0);
    bn.w[2] = 1;
    bn -= 1;

    printf("bn(1,0,0) - 1 = ");
    print(bn); printf("\n");

    bn = BigNum(WORD_MAX) + BigNum(WORD_MAX);

    printf("bn(WORD_MAX) + BigNum(WORD_MAX) = ");
    print(bn); printf("\n");

    bn = BigNum(0);
    bn.w[1] = 0xfedcba98;
    bn.w[0] = 0x76543000;

    printx(bn);
    printf(".blssSlotNdx(%d) = %lx\n", 3, bn.blssSlotNdx(3));
    printx(bn);
    printf(".blssSlotNdx(%d) = %lx\n", 8, bn.blssSlotNdx(8));
    printx(bn);
    printf(".blssSlotNdx(%d) = %lx\n", 11, bn.blssSlotNdx(11));
    printx(bn);
    printf(".blssSlotNdx(%d) = %lx\n", 15, bn.blssSlotNdx(15));
    printx(bn);
    printf(".blssSlotNdx(%d) = %lx\n", 17, bn.blssSlotNdx(17));

    bn = BigNum::blssMask(3);
    printf("blss mask 3 = "); printx(bn); printf("\n"); 
    bn = BigNum::blssMask(8);
    printf("blss mask 8 = "); printx(bn); printf("\n"); 
    bn = BigNum::blssMask(9);
    printf("blss mask 9 = "); printx(bn); printf("\n"); 
    bn = BigNum::blssMask(11);
    printf("blss mask 11 = "); printx(bn); printf("\n"); 
    bn = BigNum::blssMask(15);
    printf("blss mask 15 = "); printx(bn); printf("\n"); 
    bn = BigNum::blssMask(17);
    printf("blss mask 17 = "); printx(bn); printf("\n"); 
}

