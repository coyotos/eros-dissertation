/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <disk/LowVolume.hxx>
#include <disk/PagePot.hxx>
#include <disk/DiskNode.hxx>
     
#if 0
OID
Division::EndOID() const
{
  OID startoid(oid);

  int nPages = (end - start) / EROS_PAGE_SECTORS;

  if (type == dt_Node) {
    uint32_t count = nPages * DISK_NODES_PER_PAGE;
    OID end = startoid + count;
    return end;
  }

  if (type != dt_Page)
    return 0;

  int nClusters = nPages / (PAGES_PER_PAGE_CLUSTER);
  if (nPages % PAGES_PER_PAGE_CLUSTER)
    nClusters++;

  // number of clusters == number of page pots.
  nPages -= nClusters;
  OID end = startoid + nPages;
  return end;
}
#endif

bool
Division::Contains(const OID& oid) const
{
  if (startOid > oid)
    return false;
  
  if (endOid <= oid)
    return false;

  return true;
}
