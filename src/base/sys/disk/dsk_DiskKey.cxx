/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/DiskKey.hxx>

#if 0
DiskKey::DiskKey(KeyType kt, uint16_t keySubType)
{
  InitType(kt);
  SetUnprepared();
  subType = keySubType;
  keyData = 0;
  
  nk.value[2] = 0;
  nk.value[1] = 0;
  nk.value[0] = 0;
}

// Start/Resume keys
DiskKey::DiskKey(KeyType kt, uint16_t _keyData, OID oid)
{
  InitType(kt);
  SetUnprepared();
  subType = 0;
  keyData = _keyData;
  
  unprep.oid = oid;
  unprep.count = 0;
}

// Segment/Page keys
DiskKey::DiskKey(KeyType kt, OID oid, uint32_t blss,
		   bool readOnly)
{
  InitType(kt);
  SetUnprepared();
  subType = 0;
  keyData = blss;
  if (readOnly)
    SetReadOnly();
  
  unprep.oid = oid;
  unprep.count = 0;
}
#endif

DiskKey::DiskKey(uint32_t hi, uint32_t mid, uint32_t lo)
{
  InitType(KtNumber);
  SetUnprepared();
  subType = 0;
  keyData = EROS_PAGE_BLSS;
  SetReadOnly();

  nk.value[2] = hi;
  nk.value[1] = mid;
  nk.value[0] = lo;

#if 0
  // There is NO reason for the cross tools to be generating
  // pre-optimized number keys.  Let prepare do that, numnuts!
  
  // If zero number key:
  if (hi == 0 && mid == 0 && lo == 0)
    subType = 1;
#endif
}

DiskKey::DiskKey()
{
  KS_ZeroInitKey();
}

#if 0
DiskKey::DiskKey(KeyType kt, OID lo, OID hi)
{
  InitType(kt);
  SetUnprepared();
  subType = 0;
  keyData = 0;

  rk.oid = lo;
  rk.count = hi - lo;
}
#endif

DiskKey&
DiskKey::operator=(const DiskKey& that)
{
  // this is dirty!!!
  *((uint32_t *)this) = *((uint32_t *) &that);
  nk.value[0] = that.nk.value[0];
  nk.value[1] = that.nk.value[1];
  nk.value[2] = that.nk.value[2];

  return *this;
}
