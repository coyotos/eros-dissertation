#ifndef __DISKKEY_HXX__
#define __DISKKEY_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// DiskKey.hxx: Declaration of a the Disk format of a Key.
//
// A DiskKey is the form that a Key takes when it appears on disk.
// DiskKeys live in DiskNodes in the same way that Keys live in Nodes.
//
// DiskKeys are VERY simple objects.  They are just smart enough to
// convert themselves to/from ordinary Keys, and to assign from each
// other.


#include <eros/target.h>
#include <disk/KeyType.hxx>

#ifdef __KERNEL__
class Key;
#endif

class DiskKey : public KeyBits {
#if 0
protected:
  // Misc and Schedule keys:
  DiskKey(KeyType kt, uint16_t keySubType = 0);

  DiskKey(KeyType kt, OID oid, uint32_t blss, bool readOnly);
  // DiskKey(KeyType kt, OID oid, Memuint8_t mb);

public:
  // Start/Resume/Domain/Node/Page/Segment/Fetch keys:
  // public so that Parse::MatchKey can do simple construction
  DiskKey(KeyType kt, uint16_t _keyData, OID oid);
#endif
  
protected:
  // number keys
  DiskKey(uint32_t hi, uint32_t mid, uint32_t lo);

#if 0

  // Range keys
  DiskKey(KeyType kt, OID lo, OID hi);
#endif

public:
  // Default constructor: (zero number key)
  DiskKey();
  ~DiskKey()
  {
  }
    
  inline void UnsafeSet(KeyBits& kb)
  {
    KS_Set(kb);
  }

  inline void ZeroInitKey()
  {
    // This is called from InitNodePot, where there may remain garbage
    // bits in the frame.  Use the zeroinit logic instead of the
    // ZeroKey logic for that reason -- a disk node should never be
    // prepared anyway.
    KS_ZeroInitKey();
  }
  
  DiskKey& operator=(const DiskKey& that);
#ifdef __KERNEL__
  // Copy from an in-core Node:
  void operator=(const Key& that);
#endif
} ;

// FOLLOWING CLASS STUBS EXIST SOLELY FOR KEYTYPE CHECKING ON
// CONSTRUCTION
class SchedKey : public DiskKey {
public:
  SchedKey(uint16_t prio = Prio::Normal)
  {
    InitType(KtSched);
    SetUnprepared();
    subType = prio;
    keyData = 0;
  
    nk.value[2] = 0;
    nk.value[1] = 0;
    nk.value[0] = 0;
  }
};

class SegmentKey : public DiskKey {
public:
  SegmentKey(OID oid, uint32_t blss, bool readOnly = false, bool noCall = false)
  {
    InitType(KtSegment);
    SetUnprepared();
    subType = 0;
    keyData = blss;
    if (readOnly)
      SetReadOnly();
  
    unprep.oid = oid;
    unprep.count = 0;

    if (noCall)
      SetNoCall();
  }
};

class DataPageKey : public DiskKey {
public:
  DataPageKey(OID oid, bool readOnly = false/* , bool zeroPage = false */)
  {
    InitType(KtDataPage);
    SetUnprepared();
    subType = 0;
    keyData = EROS_PAGE_BLSS;
    if (readOnly)
      SetReadOnly();
  
    unprep.oid = oid;
    unprep.count = 0;
  }
};

class CapPageKey : public DiskKey {
public:
  CapPageKey(OID oid, bool readOnly = false/* , bool zeroPage = false */)
  {
    InitType(KtCapPage);
    SetUnprepared();
    subType = 0;
    keyData = EROS_PAGE_BLSS;
    if (readOnly)
      SetReadOnly();
  
    unprep.oid = oid;
    unprep.count = 0;
  }
};

class StartKey : public DiskKey {
public:
  StartKey(OID oid, uint16_t _keyData = 0)
  {
    InitType(KtStart);
    SetUnprepared();
    subType = 0;
    keyData = _keyData;
  
    unprep.oid = oid;
    unprep.count = 0;
  }
};

class ResumeKey : public DiskKey {
public:
  ResumeKey(OID oid)
  {
    InitType(KtResume);
    SetUnprepared();
    subType = 0;
    keyData = 0;
  
    unprep.oid = oid;
    unprep.count = 0;
  }
};

class ProcessKey : public DiskKey {
public:
  ProcessKey(OID oid)
  {
    InitType(KtProcess);
    SetUnprepared();
    subType = 0;
    keyData = 0;
  
    unprep.oid = oid;
    unprep.count = 0;
  }
};

class NodeKey : public DiskKey {
public:
  NodeKey(OID oid, uint16_t _keyData = 0)
  {
    InitType(KtNode);
    SetUnprepared();
    subType = 0;
    keyData = _keyData;
  
    unprep.oid = oid;
    unprep.count = 0;
  }
};

class RangeKey : public DiskKey {
public:
  RangeKey(OID oidlo, OID oidhi)
  {
    InitType(KtRange);
    SetUnprepared();
    subType = 0;
    keyData = 0;

    rk.oid = oidlo;
    rk.count = oidhi - oidlo;
  }
};

class NumberKey : public DiskKey {
public:
  NumberKey(uint32_t w)
    : DiskKey(0, 0, w)
  {
  }
  NumberKey(uint32_t hi, uint32_t mid, uint32_t lo)
    : DiskKey(hi, mid, lo)
  {
  }
};

class MiscKey : public DiskKey {
public:
  MiscKey(uint16_t ty)
  {
    InitType(KtMisc);
    SetUnprepared();
    subType = ty;
    if (ty == MiscKeyType::TimePage)
      keyData = (EROS_PAGE_BLSS | SEGMODE_RO);
    else
      keyData = 0;
  
    nk.value[2] = 0;
    nk.value[1] = 0;
    nk.value[0] = 0;
  }
};

#endif // __DISKKEY_HXX__
