#ifndef __KEYTYPE_HXX__
#define __KEYTYPE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <disk/ErosTypes.h>
#include <disk/MiscKeyType.hxx>
#include <disk/KeyRing.hxx>
#include <eros/Key.h>

//
// KeyType - the major key types supported in EROS. Keys also have a subtype
// For example, the Process Tool is KtMisc, KsProcTool.
//
#define PRIMARY_KEY_TYPES 32
#define PRIMARY_KEY_TYPE_BITS 5

// If these numbers change, the dispatch table in kerninc/Invoke.cxx
// must be changed as well.  Key preparation is a very frequent
// operation in the kernel, so these numbers are ordered in such a way
// as to take advantage of a representation pun.  Key types having a
// prepared form appear starting at zero.  In the actual key data
// structure, the first byte of the key is layed out as:
//
//   (bit 7)   hazard[2] :: prepared :: keytype[5]   (bit 0)
//
// Both the hazard field and the prepared field are 0 on an unprepared
// key.  This allows a combined test on the byte value to determine
// which keys need to be prepared and which keys need to be
// validated.  If the byte value is <= LAST_OBJECT_KEYTYPE, then the
// key is NOT prepared and needs to be.  If the key is prepared, it
// needs to be validated. 

// WARNING!!! Any changes to the key types must be kept in sync with
// the invocation dispatch table.
enum KeyType {
  // gate keys clustered to simplify assembly code:
  KtStart,
  KtResume,			// must be odd because of
				// representation pun. see
				// ObjectHeader.hxx

  LAST_GATE_KEYTYPE = KtResume,

  KtDataPage,
  KtCapPage,
  KtNode,
  KtSegment,

  KtProcess,
  // End of Node Keys

  LAST_OBJECT_KEYTYPE = KtProcess,
  
  KtDevice,

  //  LAST_PREPARED_KEYTYPE = KtDevice,
  
  KtNumber,
  KtTimer,			// placeholder - implementation undefined
  KtSched,			// schedule for a process
  KtRange,
  KtMisc,

  LAST_KEYTYPE = KtMisc
} ;

enum KeySubType {
  // Subtypes of resume key:
  KstResume = 0,		// can convey a message
  KstFault = 1,			// cannot convey a message
};

struct Prio {
  enum Priority {
    Inactive = -2,
    KernIdle = -1,
    UserIdle = 0,
    Normal = 8,
    High = 16
  };
} ;

struct BLSS {
  enum {
    bit32 = 7,
    bit28 = 6,
    bit24 = 5,
    bit20 = 4,
    bit16 = 3,
    bit12 = 2,

    RedSeg = 1,
  };
};


/*************************************************************
 *
 * NEW KEY TYPE BYTE ORGANIZATION (STILL EXPERIMENTAL):
 *
 *     7 6         2  1   0
 *    +-+-----------+---+---+
 *    |P|   type    |RHz|Whz|
 *    +-+-----------+---+---+
 *
 * P == 1 ==> prepared in this design, only object keys are
 * prepared.
 *
 * RATIONALE:
 *
 * 1. Key type comparison requires a mask operation in most
 *    cases anyway, but since the comparison is to a constant
 *    the shift operation should be resolvable at compile time.
 *
 * 2. Key types requiring preparation (object types) can be
 *    tested for with a single less-than operation.
 *
 * 3. Key types requiring pin can be tested for with a simple
 *    bit test.
 *
 * This rearrangement is in fact a precursor to the pin logic
 * change.
 *
 * One problem with this rearrangement is that it destroys the
 * bit pun in the keyring.  That doesn't appear to be critical,
 * though.
 *************************************************************/

// The ObjectHeader pointer declaration is unused in the on-disk
// key format, but including it here is harmless and simplifies
// the declarations in Key.hxx.

#define KHAZARD_WRITE     0x1u
#define KHAZARD_READ      0x2u
#define KHAZARD_READWRITE 0x2u

class ObjectTable;

#define PREPARED_KT_BIT    __U(0x80)
#define KHAZARD_BITS       __U(0x3)
#define KEYTYPE_BITS       __U(0x7c)

		
#define KEYTYPE_NEEDS_PREPARE(x) (x < ((LAST_OBJECT_KEYTYPE+1)<<2))
#define KEYTYPE_IS_PREPPED(x)    (x >= ((LAST_OBJECT_KEYTYPE+1)<<2))
#define KEYTYPE_NEEDS_PIN(x)     (x >= (((LAST_GATE_KEYTYPE+1)<<2)|PREPARED_KT_BIT))
  
// #define PREPARABLE_TYPE(x) ( ((x) & KEYTYPE_BITS) <= LAST_PREPARED_KEYTYPE )

#define UNPREPARED_KT(x) ((uint8_t)(x) & ~PREPARED_KT_BIT)
#define PREPARED_KT(x)   ((uint8_t)(x) | PREPARED_KT_BIT)

// #define PREPPED_TYPE(ty)  ((ty <= LAST_PREPARED_KEYTYPE) ? (ty | 0x2u) : ty)

struct KeyBits {
#ifdef BITFIELD_PACK_LOW
  uint8_t x_ktByte;
  uint8_t subType;
  uint16_t keyData;
#else
#error "verify bitfield layout" 
#endif

  // ACCESSORS AND MUTATORS RELATING TO SEGMODE KEYS:
  uint32_t GetBlss() const
  {
    return keyData & 0x1fffu;
  }
  void SetBlss(uint32_t blss)
  {
    keyData = (keyData & ~SEGMODE_BLSS_MASK) | blss;
  }
  
  bool IsHazard() const
  {
    return (x_ktByte & KHAZARD_BITS);
  }
  
  void UnHazard()
  {
     x_ktByte &= ~KHAZARD_BITS;
  }

  void SetPrepared()
  {
     x_ktByte = PREPARED_KT(x_ktByte);
  }
  
  void SetUnprepared()
  {
     x_ktByte = UNPREPARED_KT(x_ktByte);
  }
  
  void SetRdHazard()
  {
     x_ktByte |= KHAZARD_READ;
  }
  
  void SetWrHazard()
  {
     x_ktByte |= KHAZARD_WRITE;
  }
  
  void SetRwHazard()
  {
     x_ktByte |= KHAZARD_READWRITE;
  }

  bool IsRdHazard()
  {
    return (x_ktByte & KHAZARD_READ);
  }
  
  bool IsWrHazard()
  {
    return (x_ktByte & KHAZARD_WRITE);
  }
  
  bool IsRwHazard()
  {
    return (x_ktByte & KHAZARD_BITS);
  }
  
  KeyType GetType() const
  {
    return (KeyType) ((x_ktByte & KEYTYPE_BITS) >> 2);
  }
  
  void InitType(uint8_t t)
  {
    x_ktByte = UNPREPARED_KT((unsigned)(t<<2)); // not hazard, not prepared
  }
  
  void SetType(KeyType kt)
  {
    x_ktByte &= ~KEYTYPE_BITS;
    x_ktByte |= ((kt<<2) & KEYTYPE_BITS);
  }
  
  bool IsType(uint8_t t) const
  {
    return (GetType() == t);
  }

  bool IsNoCall() const
  {
    return (keyData & (SEGMODE_NC|SEGMODE_WEAK)) ? true : false;
  }
  
  void SetNoCall()
  {
    keyData |= SEGMODE_NC;
  }
  
  bool IsReadOnly() const
  {
    return (keyData & SEGMODE_RO) ? true : false;
  }
  
  bool IsWeak() const
  {
    return (keyData & SEGMODE_WEAK) ? true : false;
  }
  
  void SetWeak()
  {
    keyData |= SEGMODE_WEAK;
  }
  
  bool IsPrepared() const
    {
      if (KEYTYPE_IS_PREPPED(x_ktByte))
	return true;
      return false;
    }

  bool IsUnprepared() const
    {
      if ((x_ktByte & PREPARED_KT_BIT) == 0)
	return true;
      return false;
    }

  bool NeedsPrepare() const
    {
      return KEYTYPE_NEEDS_PREPARE(x_ktByte) ? true : false;
    }
  
  bool NeedsPin() const
    {
      return KEYTYPE_NEEDS_PIN(x_ktByte) ? true : false;
    }
  
  bool IsPreparedObjectKey() const
    {
      return (x_ktByte & PREPARED_KT_BIT) ? true : false;
    }
      
  bool IsPreparedResumeKey() const
  {
    // Resume keys are never hazarded...
    if ((x_ktByte & ~KHAZARD_BITS) == PREPARED_KT(KtResume<<2))
      return true;
    return false;
  }
  
  void SetReadOnly()
  {
    keyData |= SEGMODE_RO;
  }
  
  bool IsRedSegmentKey() const
  {
    return ((keyData & SEGMODE_BLSS_MASK) == BLSS::RedSeg) ? true : false;
  }
  
  bool IsGateKey() const
  {
    return (GetType() <= LAST_GATE_KEYTYPE) ? true : false;
  }
  
  bool IsObjectKey() const
  {
    return (GetType() <= LAST_OBJECT_KEYTYPE) ? true : false;
  }

  bool IsNodeKeyType() const;

  bool IsSegKeyType() const
  {
    return (IsType(KtNode) ||
	    IsType(KtSegment));
  }

  bool IsDataPageType() const
  {
    return (IsType(KtDataPage) ||
	    IsType(KtMisc) && subType == MiscKeyType::TimePage);
  }

  bool IsSegModeType() const
  {
    return (IsSegKeyType() || IsDataPageType());
  }

  // EVERYTHING ABOVE HERE IS GENERIC TO ALL KEYS.  Structures below
  // this point should describe the layout of a particular key type
  // and should occupy exactly three words.
  
  union {
    struct {
      ObCount count;
      OID     oid;
    } unprep;

    // Changes to this substructure must be coordinated with
    // KeyRing.hxx!
    struct {
      KeyRing *next;
      KeyRing *prev;
      struct ObjectHeader *pObj;
    } ok;
    struct {
      KeyRing *next;
      KeyRing *prev;
      struct Process *pContext;
    } gk;

    struct {			// NUMBER KEYS
      uint32_t value[3];
    } nk;

    // Priority keys use the keyData field, but no other fields.
    
    // Miscellaneous Keys have no substructure, but use the 'subType'
    // field.

    // Device Keys (currently) have no substructure, but use the
    // 'subType' field. 

    // It is currently true that oidLo and oidHi in range keys and
    // object keys overlap, but not important that they do so.

    struct {			// RANGE KEYS
      uint32_t    count;
      OID     oid;
    } rk;      
    
    struct {			// DEVICE KEYS
      uint16_t devClass;
      uint16_t devNdx;
      uint32_t     devUnit;
    } dk;

    struct {			// QUEUE MISC KEY
      uint32_t releaseCount;
      uint32_t value[2];

      // This is a horrible representation pun, user here solely to
      // avoid the need to publish the link structure outside the kernel
      class ThreadPile& AsThreadPile()
      {
	return * ((ThreadPile *) &value[0]);
      }
    } semaMiscKey;
  } ;

  bool IsZeroKey() const
  {
    if ( IsType(KtNumber) && (subType == 1) )
      return true;
    
    return (IsType(KtNumber) &&
	    nk.value[2] == 0 &&
	    nk.value[1] == 0 &&
	    nk.value[0] == 0);
  }

#ifdef NEW_SEGMENT_LOGIC
  bool IsValidFormatKey() const
  {
    if ( ( IsType(KtNumber)) &&
	 ( subType == 0 ) && 
	 ( REDSEG_GET_INITIAL_SLOTS(nk) <= REDSEG_GET_RESERVED_SLOTS(nk) ) &&
	 ( REDSEG_GET_INITIAL_SLOTS(nk) < EROS_NODE_SLOT_MASK ) &&
	 ( REDSEG_GET_RESERVED_SLOTS(nk) <= EROS_NODE_SLOT_MASK ) )
      return true;
    return false;
  }
#else
  bool IsValidFormatKey() const
  {
    if ( ( IsType(KtNumber)) &&
	 ( subType == 0 ) && 
#if EROS_NODE_SIZE == 16
	 ( (nk.value[0] & 0xe0) == 0 ) &&
#elif EROS_NODE_SIZE == 32
	 ( (nk.value[0] & 0xc0) == 0 ) &&
#endif
	 ( REDSEG_GET_INITIAL_SLOTS(nk) < EROS_NODE_SLOT_MASK ) &&
	 ( REDSEG_GET_RESERVED_SLOTS(nk) <= EROS_NODE_SLOT_MASK ) )
      return true;
    return false;
  }
#endif
  
#ifdef __KERNEL__
  inline void KeyBits::IKS_Unchain();
#endif
  
  // These two functions are identical, but must be inlined
  // selectively so that improving the gate key case does not damage
  // the kernel key case.
  inline void KS_ZeroInitKey();
  void KS_ZeroKey();
  
  void KS_Set(KeyBits& kb);
#ifdef __KERNEL__
  inline void IKS_ZeroKey();
  inline void IKS_Set(KeyBits& kb);
#endif
} ;

#ifdef __KERNEL__
inline void
KeyBits::IKS_Unchain()
{
  if ( IsPreparedObjectKey() ) {
    ok.next->prev = ok.prev;
    ok.prev->next = ok.next;
  }
}
#endif

inline void
KeyBits::KS_ZeroInitKey()
{
  // Zero key without first checking anything at all!
  
  InitType(KtNumber);
  SetUnprepared();
  subType = 1;		// new zero key subtype
  keyData = EROS_PAGE_BLSS;
  SetReadOnly();
  nk.value[0] = 0;
  nk.value[1] = 0;
  nk.value[2] = 0;
}

#ifdef __KERNEL__
inline void
KeyBits::IKS_ZeroKey()
{
#ifdef __KERNEL__
  IKS_Unchain();
#endif

  KS_ZeroInitKey();
}

inline void
KeyBits::IKS_Set(KeyBits& kb)
{
  // Skip copy if src == dest (surprisingly often!)
  if (&kb == (KeyBits*) this)
    return;
    
#ifdef __KERNEL__
  IKS_Unchain();
#endif

  // this is unclean!!!
  *((uint32_t *)this) = ( *((uint32_t *) &kb) & (~KHAZARD_BITS) );
  nk.value[0] = kb.nk.value[0];
  nk.value[1] = kb.nk.value[1];
  nk.value[2] = kb.nk.value[2];

  // Update the linkages if destination is now prepared:
  if ( IsPreparedObjectKey() ) {
    ok.prev->next = (KeyRing*) this;
    ok.next = (KeyRing*) &kb;
    kb.ok.prev = (KeyRing*) this;
  }
}
#endif

#endif // __KEYTYPE_HXX__
