/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <disk/KeyType.hxx>

bool
KeyBits::IsNodeKeyType() const
{
  switch (GetType()) {
  case KtNode:
  case KtSegment:
  case KtProcess:
  case KtStart:
  case KtResume:
    return true;
  default:
    return false;
  }
}

void
KeyBits::KS_ZeroKey()
{
#ifdef __KERNEL__
  IKS_Unchain();
#endif

  InitType(KtNumber);
  SetUnprepared();
  subType = 1;		// new zero key subtype
  keyData = EROS_PAGE_BLSS;
  SetReadOnly();
  nk.value[0] = 0;
  nk.value[1] = 0;
  nk.value[2] = 0;
}

void
KeyBits::KS_Set(KeyBits& kb)
{
  // Skip copy if src == dest (surprisingly often!)
  if (&kb == (KeyBits*) this)
    return;
    
#ifdef __KERNEL__
  IKS_Unchain();
#endif

  // this is unclean!!!
  *((uint32_t *)this) = ( *((uint32_t *) &kb) & (~KHAZARD_BITS) );
  nk.value[0] = kb.nk.value[0];
  nk.value[1] = kb.nk.value[1];
  nk.value[2] = kb.nk.value[2];

  // Update the linkages if destination is now prepared:
  if ( IsPreparedObjectKey() ) {
    ok.prev->next = (KeyRing*) this;
    ok.next = (KeyRing*) &kb;
    kb.ok.prev = (KeyRing*) this;
  }
}
