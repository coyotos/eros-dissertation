#ifndef __KEYRING_HXX__
#define __KEYRING_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// KeyRing is an overlay on both the Key structure and the
// ObjectHeader structure.  It can be unioned with a one-word object
// to avoid wasting the space of the first field.
struct KeyRing {
  uint32_t : 32;			// filler
  KeyRing *next;
  KeyRing *prev;

#ifdef __KERNEL__
  // Resume key zap does not require mustUnprepare, because the kernel
  // guarantees that prepared resume keys only reside in dirty objects.
  void RescindAll(bool mustUnprepare);
  void ZapResumeKeys();
  void UnprepareAll();
  bool HasResumeKeys() const;
  void ObjectMoved(struct ObjectHeader *);

#ifndef NDEBUG
  bool IsValid(const void *) const;
#endif

#endif /* __KERNEL__ */
  
  bool IsEmpty() const
  {
    return (next == this) ? true : false;
  }
  
  void ResetRing()
  {
    next = this;
    prev = this;
  }
};

#endif __KEYRING_HXX__
