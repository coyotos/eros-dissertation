#ifndef __STDKEYTYPE_H__
#define __STDKEYTYPE_H__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/* StdKeyType: definitions of the alleged key type values for kernel
   objects and standard domains.  Note that this file is in some cases
   included by assembler code.
   */

/* Local Variables: */
/* comment-column:34 */

#define AKT_MISC(x)	(0x00010000 + (x))
#define AKT_DEVICE(x)	(0x00020000 + (x))
#define AKT_DOMAIN(x)	(0x00030000 + (x))

/* Segment keys:

   Value     Meaning

   [0..0xff]      ordinary segment, value is blss.
   [0x100..0x1ff] rd-only segment, low byte is blss
   [0x200]        malformed red segment

   */

#define AKT_IsSeg(x) ((x) >= 0 && (x) <= 0xff)
#define AKT_IsRoSeg(x) ((x) >= 0x100 && (x) <= 0x1ff)

#if 0
#define AKT_AddressSpace(blss)   (blss)
#define AKT_RoAddressSpace(blss) ((blss)|0x100)
#define AKT_Page                 AKT_AddressSpace(2)
#define AKT_RoPage               AKT_RoAddressSpace(2)
#endif

#define AKT_DataPage             0x2
#define AKT_CapPage              0x3
#define AKT_MalformedRedSeg      0x200
#define AKT_Node                 0x301
#define AKT_Space                0x302
#define AKT_Process              0x304
#define AKT_Range                0x305
#define AKT_Number               ((KT)+1)

#define AKT_Device		 0x400

#define AKT_PCC                  AKT_DOMAIN(0)
#define AKT_MetaConstructor      AKT_DOMAIN(1)
#define AKT_SpaceBank            AKT_DOMAIN(2)
#define AKT_DomCre               AKT_DOMAIN(8)
#define AKT_VcskSeg              AKT_DOMAIN(9)
#define AKT_FreshSeg             AKT_DOMAIN(10)

#define AKT_ConstructorBuilder   AKT_DOMAIN(11)
#define AKT_ConstructorRequestor AKT_DOMAIN(12)

#define AKT_Directory            AKT_DOMAIN(64)
#define AKT_File                 AKT_DOMAIN(65)

#define AKT_SigMux               AKT_DOMAIN(72)
#endif /* __STDKEYTYPE_H__ */
