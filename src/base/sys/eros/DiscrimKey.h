#ifndef __DISCRIM_H__
#define __DISCRIM_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/*
 * This file resides in eros/ because the kernel and the invocation
 * library various must agree on the values.
 */

/* Local Variables: */
/* comment-column:34 */

/* ORDER and RESULT code values: */

#define OC_Discrim_Classify          1
#define OC_Discrim_Verify            2
#define OC_Discrim_Compare           3

#define Discrim_Class_Number  0
#define Discrim_Class_Resume  1
#define Discrim_Class_Memory  2
#define Discrim_Class_Sched   3
#define Discrim_Class_Other   255

uint32_t discrim_classify(uint32_t krDiscrim, uint32_t krObject);
uint32_t discrim_verify(uint32_t krDiscrim, uint32_t krObject);
uint32_t discrim_compare(uint32_t krDiscrim, uint32_t krOb1, uint32_t krOb2);

#endif /* __DISCRIM_H__ */
