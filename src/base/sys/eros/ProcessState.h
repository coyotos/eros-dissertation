#ifndef __PROCESSSTATE_H__
#define __PROCESSSTATE_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/* This file resides in eros/ because the kernel and various sorts of
   keepers must agree on the values.

   Note that this file is also loaded by low-level assembly code, so
   any C structures must be suitably ifdef'd
   */

/* Local Variables: */
/* comment-column:34 */

/* We have departed from the KeyKOS design in our handling of fault
   codes.  In KeyKOS, the process fault code space and the segment
   error code space were separate namespaces.  Since the segment
   keeper can pass faults to the process keeper, we have unified the
   name spaces into a single fault code space.  This unification
   somewhat simplifies keeper invocation, which is a good thing.  See
   the "Keeper" concepts section of the object reference manual.

   After going back and forth for a while, I decided to just add all
   of the weird processor-specific faults here where no other fault
   code would do.  StackSegFault will never be generated on any
   non-x86 machine, but it's easier to have it in the enum than not.

   Segment error codes from the GNOSIS reference:

   1 write protect
   2 invalid key {other than data}
   3 invalid address {too big}
   4 path passes thru node that belongs to the process
   5 invalid data key
   6 tree too deep
   7 slot 15 of red segment node not a data key
   8 bad format for slot 15 {format key} of red segment node
   9 ssc out of range
   10 window key offset not a multiple of slot size
   11 hardware page damage
   12 hardware node damage
   13 background window key was used but there is no background key

   */

/* Odd codes mean that PC points to next instruction */

/* FAULT CODES */
#define FC_NoFault	      0	  /* process is not faulted */

/* SEGMENT FAULT CODES */
#define FC_CapInvalidAddr     1  /* cap ref to undefined address */
#define FC_DataInvalidAddr    2  /* data ref to undefined address */
#define FC_CapTypeError       3  /* cap ref to data address */
#define FC_DataTypeError      4  /* data ref to data address */
#define FC_CapAccess	      5  /* cap write on RO cap subseg */
#define FC_DataAccess	      6  /* data write on RO data subseg */

#define FC_SegDepth           16  /* segment tree is too deep */
#define FC_SegMalformed       17  /* format key of red seg not a number key */

/* Not sure about keeping the following: */
#define FC_SegThroughProcess  18  /* segment node is part of active process */

#define FC_SegHwPageDamage    19  /* page is broken */
#define FC_SegHwNodeDamage    20  /* node is broken */
#define FC_SegBadSCC          21  /* ???? */

/* PROCESS FAULT CODES */
#define FC_NoAddrSpace	      32  /* process has no address space */
#define FC_MalformedProcess   33  /* process malformed */
#define FC_NoSchedule	      34  /* process lacks a schedule key */
#define FC_BadGenRegs	      35  /* gen regs holds non-number key */
#define FC_RegValue	      36  /* reg values inappropriate */
#define FC_BreakPointFault    37  /* BPT with PC at bpt instr */
#define FC_BreakPointTrap     38  /* BPT with PC after bpt instr */
#define FC_BadOpcode	      39  /* bad or undefined opcode */
#define FC_DivZero	      40  /* divide by zero exception */
#define FC_ForeignInvocation  41  /* Process has no key registers */
#define FC_BadEntryBlock      42  /* Invocation had bad entry block */
#define FC_BadExitBlock       43  /* Invocation had bad entry block */
#define FC_ParmLack           44  /* Some part of exit data is block missing */
#define FC_BadSegReg          45  /* Segment register holds invalid value */
#define FC_NoFPU              46  /* Floating point unit not present */
#define FC_FloatingPointError 47  /* Floating point exception or fault */
#define FC_Alignment          48  /* Alignment error */
    
/* MACHINE SPECIFIC FAULTS FOR X86 */

#define FC_GenProtection      128 /* hardware protection violation */
#define FC_StackSeg           129 /* hardware protection violation */
#define FC_Overflow           130 /* intel-specific overflow error */
#define FC_Bounds             131 /* intel-specific bounds error */
#define FC_SegNotPresent      132 /* intel-specific bounds error */

/* RUN STATES -- it is desirable if RS_Waiting == KT_REPLY and
   RS_Available == KT_CALL to improve the hand-coded IPC path. */
#define RS_Available 0
#define RS_Waiting   1
#define RS_Running   2

/* PROCESS FLAGS */
#define PF_Faulted   0x1u

/* Fault classifications used by the kernel when reporting faults: */
#define OC_SEGFAULT   0
#define OC_PROCFAULT   1

#endif /* __PROCESSSTATE_H__ */
