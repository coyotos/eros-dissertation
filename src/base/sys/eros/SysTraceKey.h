#ifndef __SYSTRACE_H__
#define __SYSTRACE_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/*
 * This file resides in eros/ because the kernel and the invocation
 * library various must agree on the values.
 */

/* Local Variables: */
/* comment-column:34 */

/* Tracing modes that are common enough to warrant machine-independent
   codes: */
#define SysTrace_Mode_Cycles	0x0 /* cycles executed */
#define SysTrace_Mode_Instrs	0x1 /* instrs executed */
#define SysTrace_Mode_DTLB	0x2 /* data TLB misses (r+w) */
#define SysTrace_Mode_ITLB	0x3 /* instr TLB misses (r) */
#define SysTrace_Mode_Dmiss	0x4 /* data cache misses */
#define SysTrace_Mode_Imiss	0x5 /* instr cache misses */
#define SysTrace_Mode_Dwrtbk	0x6 /* data cache misses */
#define SysTrace_Mode_Dfetch	0x7 /* data fetches */
#define SysTrace_Mode_Ifetch	0x8 /* instr fetches */
#define SysTrace_Mode_Branches	0x9 /* branches */
#define SysTrace_Mode_BrTaken	0xa /* taken branches */

#define SysTrace_Mode_NumCommonMode 0xb

#if 0
#define SysTrace_586_vpipe	xxx
#define SysTrace_586_btbhit	xxx
#define SysTrace_586_dcreads	xxx
#define SysTrace_586_agenlks	xxx
#define SysTrace_586_memstall	xxx
#define SysTrace_586_wbstall	xxx
#endif

/* ORDER and RESULT code values: */

#define OC_SysTrace_StartCounter	1
#define OC_SysTrace_ReportCounter	2
#define OC_SysTrace_StopCounter		3
#define OC_SysTrace_StopCounterVerbose	4

#define OC_SysTrace_ClearKstats		16

#define OC_SysTrace_GetCycle		32

#ifndef ASSEMBLER
struct SysTrace {
  uint32_t mode;
  uint64_t cycles;
  uint64_t count0;
  uint64_t count1;
};

/* Container for various kernel statistics: */

#ifdef KERN_STATS

struct KernStats_s {
  uint64_t nDepend;
  uint64_t nDepMerge;
  uint64_t nDepInval;
  uint64_t nDepZap;

  uint64_t nInvoke;
  uint64_t nInvKpr;
  uint64_t nInter;
  uint64_t nKeyPrep;

  uint64_t nWalkSeg;
  uint64_t nWalkLoop;

  uint64_t nPfTraps;
  uint64_t nPfAccess;

  uint64_t nGateJmp;
  uint64_t nInvRetry;

  uint64_t nRetag;
};
extern struct KernStats_s KernStats;

#endif


uint32_t systrace_start(uint32_t krSysTrace, uint32_t mode);
uint32_t systrace_report(uint32_t krSysTrace, struct SysTrace *);
uint32_t systrace_stop(uint32_t krSysTrace);
uint32_t systrace_stopverbose(uint32_t krSysTrace);
uint32_t systrace_getcycle(uint32_t krSysTrace, uint64_t* cy);

uint32_t systrace_clear_kstats(uint32_t krSysTrace);
#endif

#endif /* __SYSTRACE_H__ */
