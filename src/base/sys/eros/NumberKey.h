#ifndef __NUMBERKEY_H__
#define __NUMBERKEY_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/*
 * This file resides in eros/ because the kernel and the invocation
 * library various must agree on the values.
 */

/* Local Variables: */
/* comment-column:34 */

/* ORDER and RESULT code values for number keys: */

#define OC_Number_Value		1

/* Number keys store 96 bits */				  

#ifndef __ASSEMBLER__

typedef struct nk_value_s {
  uint32_t value[3];
} nk_value;

/* Mnemonics for various number fields */
#define NKV_BG_WINDOW   3
#define NKV_LOCAL_WINDOW   2

#define nkv_is_bg_window(nkv) \
     ( ((nkv).value[0] & 0xf) == NKV_BG_WINDOW )
#define nkv_is_local_window(nkv) \
     ( ((nkv).value[0] & 0xf) == NKV_LOCAL_WINDOW )
#define nkv_is_zero(nkv) \
     ( !( (nkv).value[0] | (nkv).value[1] | (nkv).value[2] ) )

uint32_t number_get_value(uint32_t krNumber, nk_value *nkv);
uint32_t number_get_word(uint32_t krNumber, uint32_t *pW);
uint32_t number_get_dblword(uint32_t krNumber, uint64_t *pDW);

/* Some useful converters: */
uint64_t number_to_dblword(nk_value);
uint64_t number_to_word(nk_value);
#endif /* __ASSEMBLER__ */

#endif /* __NUMBERKEY_H__ */
