#ifndef __INVOKE_H__
#define __INVOKE_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/*
 * This file resides in eros/ because the kernel and the invocation
 * library must agree on the values.
 *
 * Changes to the Message structure must be reflected in the assembler
 * stubs in lib/domain/ARCH/...S
 */

/* Local Variables: */
/* comment-column:34 */

#if !defined(__ASSEMBLER__) /* && !defined(__KERNEL__) */

#ifndef BITFIELD_PACK_LOW
# error "Check bitfield packing"
#endif

typedef struct Message {
  fixreg_t invType;
  fixreg_t snd_invKey;		  /* key to be invoked */

  fixreg_t snd_len;
  const void *snd_data;
  
  uint8_t snd_key0;
  uint8_t snd_key1;
  uint8_t snd_key2;
  uint8_t snd_key3;

  fixreg_t rcv_len;
  void *rcv_data;
  
  uint8_t rcv_key0;
  uint8_t rcv_key1;
  uint8_t rcv_key2;
  uint8_t rcv_key3;

  fixreg_t snd_code;		  /* called this for compatibility */
  fixreg_t snd_w1;
  fixreg_t snd_w2;
  fixreg_t snd_w3;


  fixreg_t rcv_code;			  /* called this for compatibility */
  fixreg_t rcv_w1;
  fixreg_t rcv_w2;
  fixreg_t rcv_w3;

  uint16_t rcv_keyInfo;
} Message;

#ifdef __cplusplus
extern "C" {
#endif

extern fixreg_t CALL(Message*);
extern fixreg_t RETURN(Message*);
extern fixreg_t SEND(Message*);

#ifdef __cplusplus
}
#endif

#endif /* !ASSEMBLER */

/* INVOCATION TYPES */

#define IT_Reply 0
#define IT_Call  1
#define IT_Send  2

/* ORDER and RESULT code values: */

#define KT      (EROS_SREG_MIN)

#define OC_KeyType          ((fixreg_t)KT)
#define OC_Destroy          (KT+1)

/* see ObRef introduction */ 
#define RC_OK   	    0u
#define RC_UnknownRequest   (KT+2)
#define RC_RequestError     (KT+3)
#define RC_NoAccess         (KT+4)
#define RC_NoMoreNodes      (KT+5)
#define RC_NoMorePages      (KT+6)

#define KR_ZERO             0

#ifndef __ASSEMBLER__
fixreg_t key_kt(uint32_t krKey);
fixreg_t key_extended_kt(uint32_t krKey, fixreg_t *w1);
fixreg_t key_destroy(uint32_t krKey);
#endif

#endif /* __INVOKE_H__ */
