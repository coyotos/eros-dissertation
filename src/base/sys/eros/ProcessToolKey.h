#ifndef __PROCESSTOOLKEY_H__
#define __PROCESSTOOLKEY_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/*
 * This file resides in eros/ because the kernel and the invocation
 * library various must agree on the values.
 */

/* Local Variables: */
/* comment-column:34 */

/* ORDER and RESULT code values: */

#define OC_ProcTool_MkProcKey	           0
#define OC_ProcTool_IdentGateKey           1
#define OC_ProcTool_IdentProcessKey        2
#define OC_ProcTool_IdentSegGateKey        3
#define OC_ProcTool_CompareProcessOrigins  4

#define RC_ProcTool_Fail		    ((uint32_t) -1)

#ifndef ASSEMBLER
uint32_t pt_make_node_key(uint32_t krNode, uint16_t keyData, uint32_t krTo);
uint32_t pt_make_process_key(uint32_t krProcTool, uint32_t krNode, uint32_t krProcess);
int pt_same_origins(uint32_t krProcTool, uint32_t krProc0, uint32_t krProc1);
uint32_t pt_canopener(uint32_t krProcTool, uint32_t krGate, uint32_t krBrand, uint32_t krNode);
uint32_t pt_identsegkpr(uint32_t krProcTool, uint32_t krSegkey, uint32_t krBrand, uint32_t krNode);
#endif

#endif /* __PROCESSTOOLKEY_H__ */
