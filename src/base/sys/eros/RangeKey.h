#ifndef __RANGEKEY_H__
#define __RANGEKEY_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/*
 * This file resides in eros/ because the kernel and the invocation
 * library various must agree on the values.
 */

/* Local Variables: */
/* comment-column:34 */

/* Object type values: */
#define OT_DataPage	0
#define OT_CapPage	1
#define OT_Node		2
#ifdef EROS_PROCS_PER_FRAME
#define OT_Process	3
#define OT_NUM_TYPES    4
#else
#define OT_NUM_TYPES    3
#endif
#define OT_INVALID      EROS_REG_MAX

/* ORDER and RESULT code values: */

#define OC_Range_Query     	 1
#define OC_Range_Identify  	 2
#define OC_Range_Rescind   	 3
#define OC_Range_GetObKey  	 4
#define OC_Range_WaitObKey 	 5
#define OC_Range_Compare	 6

#define RC_Range_RangeErr	 1
#define RC_Range_NotMounted	 2
#define RC_Range_IoErr		 3

#ifndef __ASSEMBLER__
typedef uint64_t range_off_t;	/* range key offset type */

fixreg_t range_query(uint32_t krRange, uint64_t *length);

fixreg_t range_identify(uint32_t krRange,
		    uint32_t krKey,
		    fixreg_t *obType,
		    uint64_t *offset);

fixreg_t range_rescind(uint32_t krRange, uint32_t krKey);

fixreg_t range_getobjectkey(uint32_t krRange,
			fixreg_t type,
			uint64_t offset,
			uint32_t krObj);

fixreg_t range_waitobjectkey(uint32_t krRange,
			 fixreg_t type,
			 uint64_t offset,
			 uint32_t krObj);

fixreg_t range_compare(uint32_t krRange,
		   uint32_t krCmpRange,
		   int *sign,
		   uint64_t *offset);
/*   returns 1 if /krRange/ and /krCmpRange/ do not overlap, or
 * /krCmpRange/ is not a range key.
 *
 *   returns RC_OK if the ranges overlap, and:
 *
 *    if krCmpRange.start > krRange.start,
 *       *sign =  1 and *offset = krCmpRange's start in krRange
 *    if krCmpRange.start = krRange.start,
 *       *sign =  0 and *offset = 0
 *    if krCmpRange.start < krRange.start,
 *       *sign = -1 and *offset = krRange's start in krCmpRange
 */

#endif /* __ASSEMBLER __ */

#endif /* __RANGEKEY_H__ */







