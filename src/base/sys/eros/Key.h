#ifndef __KEY_H__
#define __KEY_H__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/*
   The 'keyData' field of the key is used for a number of purposes:
  
     o In start keys, it provides the process with an indicator of
       which key was invoked.
  
     o In segmode keys, it contains the biased log segment size and
       the permissions, organized as follows:
  
          bit position: 15  14  13:5 4:0
                        RO  NC   0   blss
  
       bits 12:5 are reserved.  RO and NC stand for "read only" and
       "don't call" respectively.  TY indicates whether a segment key
       is a red or black segment key. 
  
     o In schedule keys, it holds the priority.
  
     o In the old key format, it was used to hold the key subtype in
       device and miscellaneous keys.  In the new format, this
       information is now stored in the 'subType' field.
*/
/* User-visible attributes of a key: */
#define SEGMODE_RO		__U(0x8000)
#define SEGMODE_NC		__U(0x4000)
#define SEGMODE_WEAK		__U(0x2000)
#define SEGMODE_BLSS_MASK	__U(0x1fff)
#define SEGMODE_ATTRIBUTE_MASK  __U(0xe000)

/* Slots of a Process:
   0: Schedule
   1: Keeper Key
   2: I-Space (I+D if not split I+D architecture)
   3: D-Space	(Null if not split I+D architecture)
   4: Brand
   5: Trap Code
   6: Fallback message buffer
   7: Register values for SP, PC
   8: Receive Block.  Register values for RcvStrPtr, RcvStrBlock, MsgCtlLo
      numLo:  RcvStrBlock:  rcvLen(16) ik3 ik2 ik1 ik0
      numMid: RcvStrPtr
      NumHi:  MsgCtlLo: osc(2), isc(2), invk(4)
   9: Send Blcok.  Register values for SndStrPtr, SndStrBlock, MsgCtlHi
      numLo:  SndStrBlock:  sndLen(16) ok3 ok2 ok1 ok0
      numMid: SndStrPtr
      numHi:  MsgCtlHi: 0
   10: Register value for Order/Return code, PSW
   11-12: Misc Number Keys
   13: Hook
   14: genRegs
   15: genKeys

   */

/* Slots of a Red Segment:

   12: By CONVENTION where the red seg keeper puts its space bank
   13: Keeper Key (variable - determined by format key)
   14: Background Key (variable - determined by format key)
   15: Format Key
   */

#define ProcSched             0
#define ProcKeeper            1
#define ProcAddrSpace         2
#define ProcSymSpace          3
#define ProcBrand             4
#define ProcTrapCode          5
#define ProcRcvBlock          6
#define ProcSndBlock          7
#define ProcOrderAndInvoke    8
#define ProcPCandSP           9

#if EROS_NODE_SIZE == 16

/* #define ProcAltMsgBuf         13 */
#define ProcGenRegs           14
#define ProcGenKeys           15

#elif EROS_NODE_SIZE == 32

/* #define ProcAltMsgBuf         6 */
#define ProcGenKeys           31

#else
#error "EROS_NODE_SIZE is undefined"
#endif

/* #define NEW_SEGMENT_LOGIC */

#define REDSEG_SEND_NODE      1
#define REDSEG_SEND_UNALTERED 0

#ifndef NEW_SEGMENT_LOGIC
/* #define RedSegBank           12 */
#if EROS_NODE_SIZE == 16
#define RedSegBackground     13
#define RedSegKeeper         14
#define RedSegFormat         15
#else
#define RedSegBackground     29
#define RedSegKeeper         30
#define RedSegFormat         31
#endif

#define REDSEG_SET_INITIAL_SLOTS(nkv, i) \
  do { \
    (nkv).value[0] &= ~EROS_NODE_SLOT_MASK; \
    (nkv).value[0] |= (i); \
  } while (0) 
#define REDSEG_GET_INITIAL_SLOTS(nkv) ( (nkv).value[0] & EROS_NODE_SLOT_MASK )

#define REDSEG_SET_RESERVED_SLOTS(nkv, r) ( (void) 0 )
#define REDSEG_GET_RESERVED_SLOTS(nkv) EROS_NODE_SLOT_MASK

#define REDSEG_SET_KPR_SLOT(nkv, s) \
  do { \
    (nkv).value[0] &= ~(EROS_NODE_SLOT_MASK << (2*EROS_NODE_LGSIZE)); \
    (nkv).value[0] |= ( (uint32_t)(s)<<(2*EROS_NODE_LGSIZE) ); \
  } while (0) 
#define REDSEG_GET_KPR_SLOT(nkv) ( ((nkv).value[0] >> (2*EROS_NODE_LGSIZE)) & EROS_NODE_SLOT_MASK )

#define REDSEG_SET_BG_SLOT(nkv, s) \
  do { \
    (nkv).value[0] &= ~(EROS_NODE_SLOT_MASK << (3*EROS_NODE_LGSIZE)); \
    (nkv).value[0] |= ( (uint32_t)(s)<<(3*EROS_NODE_LGSIZE) ); \
  } while (0) 
#define REDSEG_GET_BG_SLOT(nkv) ( ((nkv).value[0] >> (3*EROS_NODE_LGSIZE)) & EROS_NODE_SLOT_MASK )

#define REDSEG_SET_BLSS(nkv, blss) \
  do { \
    (nkv).value[0] &= ((1u << (4*EROS_NODE_LGSIZE)) - 1u); \
    (nkv).value[0] |= ((uint32_t)(blss) << (4*EROS_NODE_LGSIZE)); \
  } while (0) 
#define REDSEG_GET_BLSS(nkv) \
      ( (nkv).value[0] >> (4*EROS_NODE_LGSIZE) )

#define REDSEG_SET_SENDNODE(nkv, val) \
  do { \
    (nkv).value[0] &= ~(EROS_NODE_SLOT_MASK << (EROS_NODE_LGSIZE)); \
    (nkv).value[0] |= ( (uint32_t)(val)<<(EROS_NODE_LGSIZE) ); \
  } while (0) 
#define REDSEG_GET_SENDNODE(nkv) ( ((nkv).value[0] >> (EROS_NODE_LGSIZE)) & EROS_NODE_SLOT_MASK )

#else
#if EROS_NODE_SIZE == 16
#define nRedSegWorkingSet     12
#define nRedSegBackground     13
#define nRedSegKeeper         14
#define nRedSegFormat         15
#else
#error "Macros not debugged yet for 32 slots"
#define nRedSegWorkingSet     28
#define nRedSegBackground     29
#define nRedSegKeeper         30
#define nRedSegFormat         31
#endif

    /* NOTE that the following are not defined in terms of node size,
       and therefore do not require adjustment... */

#define REDSEG_SET_INITIAL_SLOTS(nkv, i) \
  do { \
    (nkv).value[0] &= ~0xffu; \
    (nkv).value[0] |= (i); \
  } while (0) 

#define REDSEG_GET_INITIAL_SLOTS(nkv) ( (nkv).value[0] & 0xffu )

#define REDSEG_SET_RESERVED_SLOTS(nkv, r) \
  do { \
    (nkv).value[0] &= ~0xff00u; \
    (nkv).value[0] |= ( (uint32_t)(r) << 8 ); \
  } while (0) 

#define REDSEG_GET_RESERVED_SLOTS(nkv) ( ((nkv).value[0] >>8 ) & 0xffu )

#define REDSEG_SET_KPR_SLOT(nkv, s) \
  do { \
    assert (s == nRedSegKeeper); \
    if (REDSEG_GET_RESERVED_SLOTS(nkv) > s) \
      REDSEG_SET_RESERVED_SLOTS(nkv, s) \
  } while (0) 
#define REDSEG_GET_KPR_SLOT(nkv) ( nRedSegKeeper )

#define REDSEG_SET_BG_SLOT(nkv, s) \
  do { \
    assert (s == nRedSegBackground); \
    if (REDSEG_GET_RESERVED_SLOTS(nkv) > s) \
      REDSEG_SET_RESERVED_SLOTS(nkv, s) \
  } while (0) 
#define REDSEG_GET_BG_SLOT(nkv) ( nRedSegBackground )

#define REDSEG_SET_WS_SLOT(nkv, s) \
  do { \
    assert (s == nRedSegWorkingSet); \
    if (REDSEG_GET_RESERVED_SLOTS(nkv) > s) \
      REDSEG_SET_RESERVED_SLOTS(nkv, s) \
  } while (0) 
#define REDSEG_GET_BG_SLOT(nkv) ( nRedSegWorkingSet )

#define REDSEG_SET_BLSS(nkv, blss) \
  do { \
    (nkv).value[0] &= 0x8000fffffu; \
    (nkv).value[0] |= ((uint32_t)(blss) << 16); \
  } while (0) 
#define REDSEG_GET_BLSS(nkv) ( ((nkv).value[0] >> 16) & 0x7fffu )

#define REDSEG_SET_SENDNODE(nkv, val) \
  do { \
    (nkv).value[0] &= ~0x80000000u; \
    (nkv).value[0] |= ((uint32_t)(val) << 31); \
  } while (0) 
#define REDSEG_GET_SENDNODE(nkv) ( ((nkv).value[0] >> 31 ) & 0x1u )

#endif

#define RedSegBLSS	     1

#endif /* __KEY_H__ */
