#ifndef __NETINTERFACE_H__
#define __NETINTERFACE_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

#define	OC_NI_Read			1
#define	OC_NI_Write			2
#define OC_NI_MultiRead			3
#define OC_NI_MultiWrite		4

// #define	OC_NI_WriteIEEE			3

#define	OC_NI_GetInfo			8
#define	OC_NI_GetStats			9
#define	OC_NI_Reset			10
#if 0
#define	OC_STAT				9
#define	OC_PAR				19
#define	OC_CHK_INVOKE			255
#endif

#define RC_PARTIAL			1

#if 0
#define	RC_RX_ERROR			17	// Tentative 0001 0001
#define	RC_NO_PACKET_RX			18	// Tentative 0001 0010
#define	RC_TX_ERROR			33	// Tentative 0010 0001
#define	RC_TX_INCORRECT_SRC_ADDR	34	// Tentative 0010 0010
#define	RC_TX_INCORRECT_PKT_LEN		35	// Tentative 0010 0011
#define	RC_FUNC_NOT_SUPPORTED_BY_MODEL	49	// Tentative 0011 0001
#define	RC_INCORRECT_RAM_SIZE		50	// Tentative 0011 0010

#define	MAX_FIFO_PKT_LEN		0X0700
#endif

#ifndef __ASSEMBLER__

/* Network device statistics structure */
typedef struct ni_stats {
  uint32_t     txWatchdog;	// buggy driver
  uint32_t     txCarrierErrors;
  uint32_t     txHeartbeatErrors;
  uint32_t     collisions;
  uint32_t     txWindowErrors;	// not sure this is general
  uint32_t     rxFifoErrors;	// not sure this is general
  uint32_t     txAbort;
  uint64_t  txCpuCycles;
  uint64_t  rxCpuCycles;
  uint64_t  rxDropped;
  uint64_t  rxCrcErrors;
  uint64_t  rxFrameErrors;
  uint64_t  rxLengthErrors;
  uint64_t  rxOverrun;
  uint64_t  rxPackets;
  uint64_t  txPackets;
  uint64_t  rxMore;
  uint64_t  rdStall;
  uint64_t  wrStall;
} ni_stats_t;

typedef struct ni_info {
  char	   name[8];
  uint32_t	   niStatus;
  uint32_t     niAddrLen;
  uint8_t     niAddr[32];		/* link-layer address -- this is
				   intended to be big enough not to
				   run into problems anytime soon. */
} ni_info_t;

/* Bits in the niStatus field */
#define NI_ATTACHED	0x01
#define NI_ENABLED	0x02
#define NI_STATION	0x10
#define NI_BROADCAST	0x20
#define NI_MULTICAST	0x40

/* Structures for the multiread/multiwrite operation. Invocation send
   buffer points to array of these whose length is derived by the
   kernel from the array itself via dubious means.

   For MultiRead, entry and exit iovecs must be the same length.  The
   first iovec entry (entry [0]) contains the offset in the RECEIVE
   buffer of the exit IoVec and the length in bytes of BOTH IoVecs.
   All other entries contain offsets and limits in the receive buffer
   for any packets returned.  On exit, the exit block will contain
   actual lengths for any packets read successfully, zeros for all
   other packets.

   For the write operation, the IoVec structure identifies offsets and
   bounds of the SEND buffer, and entry 0 describes the SEND IoVec
   length. The kernel will bypass any zero length IoVec entries, which
   allows the caller to make the send and receive IoVec entries identical
   and hand the multiwrite buffer to the kernel several times.  Note that
   there is NO guarantee that all of the requested writes will be
   performed by the kernel.  If any are skipped, a result code of
   RC_PARTIAL will be returned.

   The IoVec may hold no more than 16 entries.

   At the moment, the kernel stops reading with the first packet
   it is unable to accept.  This will not always be the case.

   If ANY requested range would fall outside of the receive buffer, a
   return code of RC_RequestError is generated. */
struct IoVec {
  uint32_t offset;			/* into receive buffer  */
  uint32_t len;			/* within receive buffer */
};

#define MAX_IOVEC 16

#if 0
struct Partition
{
	enum
	{
		ThreeToFive,
		OneToThree,
		OneToOne
	} txRXRatio;
};
#endif

uint32_t ni_get_info(uint32_t krNI, ni_info_t* pNetInfo);
uint32_t ni_read_packet(uint32_t krNI, uint8_t *buf, uint32_t *pLimit);
uint32_t ni_write_packet(uint32_t krNI, uint8_t *buf, uint32_t len);
uint32_t ni_multiread_packet(uint32_t krNI, uint8_t *iovec,
			 uint32_t nvec, uint8_t *rcvBuf, uint32_t *pLimit);
uint32_t ni_multiwrite_packet(uint32_t krNI, uint8_t *buf, uint8_t *rcvBuf,
			  uint32_t len);

#endif /* __ASSEMBLER__ */

#endif /*  __NETINTERFACE_H__ */
