#ifndef __KEYBITSKEY_H__
#define __KEYBITSKEY_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/*
 * This file resides in eros/ because the kernel and the invocation
 * library various must agree on the values.
 */

/* Local Variables: */
/* comment-column:34 */

/* ORDER and RESULT code values for number keys: */

#define OC_KeyBits_Translate	1

#define EROS_KEYBITS_VERSION 1

typedef struct Bits {
  uint32_t w[4];
} Bits;

/* Returns nonzero if valid key */
int GetKeyBits(uint32_t kbr, uint32_t kr, uint32_t *version, Bits *bits);

#endif /* __KEYBITSKEY_H__ */
