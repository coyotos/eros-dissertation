/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/IoRequest.hxx>

// #define REQUEST_DEBUG

void
RequestQueue::InsertRequest(Request* req)
{
  IRQ::DISABLE();

  // Insert on the basis of true start sector, NOT on the basis of
  // current start sector.
  if (opq == 0 || req->req_start < opq->req_start) {
    assert (insertBase == 0);		// if no opq, no insertBase
    req->next = opq;
    opq = req;
  }
  else if (req->cmd == IoCmd::Plug) {
    // Always goes at the end of the queue.
    for (Request *pos = (insertBase ? insertBase : opq); ; pos = pos->next) {
      Request *next = pos->next;

      if (next == 0) {
	pos->next = req;
	req->next = next;
	insertBase = req;
	break;
      }
    }

  }
  else {
    for (Request *pos = (insertBase ? insertBase : opq); ; pos = pos->next) {
      Request *next = pos->next;

      if (next == 0 || req->req_start < next->req_start) {
	req->next = next;
	pos->next = req;
	break;
      }
    }
  }
    
  IRQ::ENABLE();
}

void
RequestQueue::RemoveRequest(Request* req)
{
  IRQ::DISABLE();
  
  assert(opq);
  
#ifdef REQUEST_DEBUG
  MsgLog::dprintf(true, "request 0x%08x is deleted\n", req);
#endif
  
  if (curReq == req)
    curReq = req->next;

  // If current top insertBase, forget top insertBase
  if (insertBase == req)
    insertBase = 0;

  if (opq == req) {
      opq = opq->next;
  }
  else {
    Request *pos = opq;
    while (pos->next != req)
      pos = pos->next;
    pos->next = req->next;
  }

  delete req;
    
  IRQ::ENABLE();
}

#ifndef NDEBUG
bool
RequestQueue::ContainsRequest(Request* req)
{
  bool result = false;
  Request *pos = opq;
  
  IRQ::DISABLE();
  
  while (pos && pos != req)
    pos = pos->next;

  if (pos == req)
    result = true;
  
  IRQ::ENABLE();

  return result;
}
#endif

Request *
RequestQueue::GetNextRequest()
{
  if (curReq == 0)
    curReq = opq;

  if (curReq->cmd == IoCmd::Plug && curReq != opq)
    curReq = opq;
  
  return curReq;
}
