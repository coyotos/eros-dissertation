/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <machine/KernTune.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/NetInterface.hxx>

static NetInterface netif[KTUNE_NNETDEV];

NetInterface::NetInterface()
{
  devClass = 0;			// this will pass for unknown.
  isAlloc = false;
  name = "???";
}

NetInterface*
NetInterface::Alloc()
{
  for (uint32_t i = 0; i < KTUNE_NNETDEV; i++) {
    if (netif[i].isAlloc == false) {
      netif[i].isAlloc = true;
      bzero(&netif[i].stats, sizeof(netif[i].stats));
      bzero(&netif[i].info, sizeof(netif[i].info));
      strcpy(netif[i].info.name, "eth0");
      netif[i].info.name[3] += i; // increment eth #
      return &netif[i];
    }
  }

  MsgLog::fatal("Net device limit exceeded\n");
  return 0;
}

NetInterface*
NetInterface::Get(uint32_t n)
{
  if (n >= KTUNE_NNETDEV)
    return 0;
  
  if (netif[n].isAlloc == false)
    return 0;

  return &netif[n];
}

