/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/util.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/Machine.hxx>
#include <disk/LowVolume.hxx>
#include <kerninc/Partition.hxx>
#include <kerninc/BlockDev.hxx>
#include <disk/DiskKey.hxx>
#include <machine/KernTune.hxx>

static uint32_t nextPartitionID = 0;
DiskKey Partition::BootstrapKey;
bool Partition::ValidBootstrapOid;
uint64_t Partition::BootSysID;
static Partition PartitionPool[KTUNE_NPARTITION];

void *
Partition::operator new(size_t /* sz */)
{
  for (uint32_t i = 0; i < KTUNE_NPARTITION; i++) {
    if (PartitionPool[i].ctrlr == 0) {
      // Hack alert -- fixed by constructor.
      PartitionPool[i].ctrlr = (BlockDev*) 0x1;
      return &PartitionPool[i];
    }
  }

  assert(false);
  return 0;
}

void
Partition::operator delete(void * vp)
{
  Partition *p = (Partition*) vp;

  p->ctrlr = 0;
}

Partition::Partition(BlockDev *_ctrlr, uint8_t _unit, uint8_t _ndx)
{
  uniqueID = nextPartitionID++;
  nSecs = 0;
  ctrlr = _ctrlr;
  unit = _unit;
  ndx = _ndx;
}

void
Partition::MountAll()
{
  for (uint32_t i = 0; i < KTUNE_NPARTITION; i++) {
    if (PartitionPool[i].ctrlr && PartitionPool[i].isEros)
      PartitionPool[i].Mount();
  }
}

void
Partition::Mount()
{
  MsgLog::printf("Mounting partition %s%d-%d\n",
		 ctrlr->name, unit, ndx);

  ObjectHeader *ioPage = ObjectCache::GrabPageFrame();
  
  MsgLog::printf("Grabbed frame 0x%08x\n", ioPage);
  
  DoDeviceRead(ioPage, 0, EROS_PAGE_SECTORS);

  kva_t addr = ObjectCache::ObHdrToPage(ioPage);
  VolHdr volHdr;

  bcopy((const void *) addr, (void *) &volHdr, sizeof(volHdr));

  MsgLog::printf("VolHdr:DivTable: %d\n", volHdr.DivTable);
  MsgLog::printf("VolHdr:AltDivTable: %d\n", volHdr.AltDivTable);
  MsgLog::printf("VolHdr:BootFlags: 0x%x\n", volHdr.BootFlags);

  DoDeviceRead(ioPage, volHdr.DivTable, EROS_PAGE_SECTORS);

  Division *divTable = (Division*) addr;

  for (int i = 0; i < NDIVENT; i++) {
    Division& dte = divTable[i];
    
    if (dte.type != dt_Unused)
      Persist::RegisterDivision(this, dte);
  }

  if (isBoot && !ValidBootstrapOid) {
    BootstrapKey = volHdr.iplKey;
    BootSysID = volHdr.iplSysId;
    if (volHdr.BootFlags & VolHdr::VF_IPL)
      ValidBootstrapOid = true;

    MsgLog::printf("%s%d-%d: Bootstrap domain is ",
		   ctrlr->name, unit, ndx);
    MsgLog::print(BootstrapKey.unprep.oid);
    MsgLog::printf(" alloc=");
    MsgLog::print(BootstrapKey.unprep.count);
    MsgLog::printf("\n");
    // Machine::SpinWait(4000);
  }

  MsgLog::printf("Releasing frame 0x%08x\n", ioPage);

  ObjectCache::ReleasePageFrame(ioPage);

  isMounted = true;
  // Machine::SpinWait(10000);
}

