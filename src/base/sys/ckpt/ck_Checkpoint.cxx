/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/DiskNode.hxx>
#include <disk/PagePot.hxx>
#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Checkpoint.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/Depend.hxx>
#include <kerninc/BlockDev.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/Console.hxx>

#include <machine/PTE.hxx>

#define dbg_ckpt	0x1	/* steps in taking snapshot */
#define dbg_ckage	0x2	/* migration state machine */
#define dbg_mig		0x4	/* migration state machine */
#define dbg_ckdir	0x8	/* steps in ckdir reconstruction */
#define dbg_reservation	0x10	/* reservation logic */
#define dbg_wrckdir	0x20	/* writing ckdir */
#define dbg_dirent	0x40	/* dirent manipulation */
#define dbg_stabilize	0x80	/* stabilization */
#define dbg_mignode	0x100	/* actual object migration */
#define dbg_migpage	0x200	/* actual object migration */
#define dbg_migfinish	0x400	/* actual object migration */

/* Following should be an OR of some of the above */
#define dbg_flags   ( dbg_migfinish | 0u )

#define DBCOND(x) (dbg_##x & dbg_flags)
#define DEBUG(x) if DBCOND(x)
#define DEBUG2(x,y) if ((dbg_##x|dbg_##y) & dbg_flags)

bool 		 Checkpoint::enabled = false;
CoreGeneration   Checkpoint::coreGeneration[nGeneration];

DiskCheckpoint  *Checkpoint::lastCkptHdr = 0;
ObjectHeader    *Checkpoint::lastCkptObHdr = 0;
lid_t            Checkpoint::lastCkptHdrLid = 0;

DiskCheckpoint  *Checkpoint::nextCkptHdr = 0;
ObjectHeader    *Checkpoint::nextCkptObHdr = 0;
lid_t            Checkpoint::nextCkptHdrLid = 0;

ObjectHeader    *Checkpoint::diskDirPageHdr = 0;

ObjectHeader    *Checkpoint::threadDirHdr[nThreadDirPage];
ThreadDirPage   *Checkpoint::threadDirPg[nThreadDirPage];
lid_t            Checkpoint::lastThreadDirLid[nThreadDirPage];
lid_t            Checkpoint::nextThreadDirLid[nThreadDirPage];
ObjectHeader    *Checkpoint::reserveDirHdr[nReserveDirPage];
ReserveDirPage  *Checkpoint::reserveDirPg[nReserveDirPage];
lid_t            Checkpoint::lastReserveDirLid[nReserveDirPage];
lid_t            Checkpoint::nextReserveDirLid[nReserveDirPage];
ObjectHeader    *Checkpoint::nextDirPageHdr;
CkptDirPage     *Checkpoint::nextDirPage;

AllocMap         Checkpoint::allocMap;
logframe_t       Checkpoint::LastAllocatedFrame = 0;
uint32_t         Checkpoint::totLogFrame = 0;
uint32_t         Checkpoint::nAvailLogFrame = 0;
uint32_t         Checkpoint::nReservedLogFrame = 0;
uint32_t         Checkpoint::nAllocatedLogFrame = 0;
uint32_t         Checkpoint::nMasterPage = 0;
Checkpoint::MigrationStatus
                 Checkpoint::migrationStatus;

uint32_t         Checkpoint::nCheckpointsCompleted = 0;
uint32_t         Checkpoint::ckGenLimit;


void
Checkpoint::SwapCheckpointHeaders()
{
  ObjectHeader *oldLastCkptObHdr = lastCkptObHdr;
  DiskCheckpoint *oldLastCkptHdr = lastCkptHdr;
  lid_t oldLastCkptHdrLid = lastCkptHdrLid;
  
  lastCkptObHdr = nextCkptObHdr;
  lastCkptHdr = nextCkptHdr;
  lastCkptHdrLid = nextCkptHdrLid;
  
  nextCkptObHdr = oldLastCkptObHdr;
  nextCkptHdr = oldLastCkptHdr;
  nextCkptHdrLid = oldLastCkptHdrLid;
}

void
Checkpoint::SwapThreadDirs()
{
  for (uint32_t i = 0; i < nThreadDirPage; i++) {
    lid_t oldLid = lastThreadDirLid[i];
    lastThreadDirLid[i] = nextThreadDirLid[i];
    nextThreadDirLid[i] = oldLid;
  }
  
  for (uint32_t i = 0; i < nReserveDirPage; i++) {
    lid_t oldLid = lastReserveDirLid[i];
    lastReserveDirLid[i] = nextReserveDirLid[i];
    nextReserveDirLid[i] = oldLid;
  }
}

// Unpreparing the dirty nodes will have the effect of flushing all of
// the context caches.
void
Checkpoint::MarkDirtyNodesForCkpt()
{
  for (uint32_t nd = 0; nd < ObjectCache::TotalNodes(); nd++) {
    Node *pNode = ObjectCache::GetCoreNodeFrame(nd);

    if (pNode->IsDirty()) {
      assert (pNode->GetFlags(OFLG_CKPT) == false);

      for (unsigned i = 0; i < EROS_NODE_SIZE; i++)
	if (pNode->slot[i].IsRdHazard())
	  pNode->ClearHazard(i);

      pNode->SetFlags(OFLG_CKPT);
    }
  }
}

// Mark dirty pages for COW and ckpt, and disable write permissions on
// all outstanding PTE's to such pages.
void
Checkpoint::MarkDirtyPagesForCkpt()
{
#ifdef SMALL_SPACES
  Process::WriteDisableSmallSpaces();
#endif
  
  for (uint32_t pg = 0; pg < ObjectCache::TotalPages(); pg++) {
    ObjectHeader *pObj = ObjectCache::GetCorePageFrame(pg);

    switch(pObj->obType) {
    case ObType::PtDataPage:
    case ObType::PtCapPage:
      if (pObj->GetFlags(OFLG_REDIRTY) && !pObj->GetFlags(OFLG_DIRTY))
	MsgLog::dprintf(true, "Checkpointing redirtied page!!\n");
      
      if (pObj->IsDirty()) {
	assert (pObj->GetFlags(OFLG_CKPT) == 0);
	pObj->SetFlags(OFLG_CKPT);
      }

      break;

#ifdef USES_MAPPING_PAGES
    case ObType::PtMappingPage:
      Depend::WriteDisableProduct(pObj);
      break;
#endif
    }
  }

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("After marking pgs for ckpt");
#endif
}

// Initialize all fields of the soon-to-be-written checkpoint header
// that we can.  We will update nDirPage, nThreadPage, and maxLogLoc
// as we construct and write out the various directories.
  
void
Checkpoint::InitNextCkptHdr()
{
  nextCkptHdr->nThreadPage = 0;
  nextCkptHdr->nRsrvPage = 0;
  nextCkptHdr->nDirPage = 0;
  nextCkptHdr->hasMigrated = false;
  nextCkptHdr->sequenceNumber = lastCkptHdr->sequenceNumber + 1;
  nextCkptHdr->maxLogLid = 2 * EROS_OBJECTS_PER_FRAME;
  nextCkptObHdr->SetDirtyFlag();
}

void
Checkpoint::SnapshotReserves()
{
  uint32_t r = 0;
  coreGeneration[current].nReserve = 0;

  while (r < MAX_CPU_RESERVE) {
    ReserveDirPage* rdp = reserveDirPg[r / ReserveDirPage::maxDirEnt];

    reserveDirHdr[r / ReserveDirPage::maxDirEnt]->SetDirtyFlag();

    uint32_t count = 0;
    for (; count < ReserveDirPage::maxDirEnt && r < MAX_CPU_RESERVE;
	 count++, r++) {
      CpuReserve& rsrv = CpuReserve::CpuReserveTable[r];
      
      CpuReserveInfo& ckrsrv = rdp->entry[count];

      ckrsrv.index = r;
      ckrsrv.quanta = rsrv.quanta;
      ckrsrv.duration = rsrv.duration;
      ckrsrv.period = rsrv.period;
      ckrsrv.start = rsrv.start;
      ckrsrv.normPrio = rsrv.normPrio;
      ckrsrv.rsrvPrio = rsrv.rsrvPrio;

      coreGeneration[current].nReserve++;
    }

    rdp->nDirent = count;
    assert(nextReserveDirLid[nextCkptHdr->nRsrvPage]);
    nextCkptHdr->dirPage[nextCkptHdr->nRsrvPage] =
      nextReserveDirLid[nextCkptHdr->nRsrvPage];
    nextCkptHdr->nRsrvPage++;
  }
}

void
Checkpoint::SnapshotThreads()
{
  uint32_t t = 0;
  coreGeneration[current].nThread = 0;

  while (t < KTUNE_NTHREAD) {
    ThreadDirPage* tdp = threadDirPg[t / ThreadDirPage::maxDirEnt];

    threadDirHdr[t / ThreadDirPage::maxDirEnt]->SetDirtyFlag();

    uint32_t count = 0;
      
    for (; count < ThreadDirPage::maxDirEnt && t < KTUNE_NTHREAD;
	 t++) {
      Thread& thrd = Thread::ThreadTable[t];
      
      thrd.pageWriteCount = 0;

      if (thrd.state == Thread::Free)
	continue;

      if (thrd.state != Thread::Free &&
	  thrd.processKey.IsObjectKey() == false) {
	// This thread contained a key which was rescinded.  It has
	// been scheduled to run, and will die at that time.  Do not
	// bother to copy it.

	assert (thrd.state == Thread::Ready);
	assert (thrd.processKey.IsType(KtNumber));
#if 0
	MsgLog::dprintf(true, "Non-free thread 0x%08x with non-object key!\n",
			&thrd);
#endif
	continue;
      }
      
#if 0
      // Now that we are allowing mapping tables to persist in
      // read-only form, the thread no longer gets deprepared.
      
      // Thread must be in deprepared form:
      assert (thrd.context == 0);
#endif

#if 0
      // Can the embodied domain key be prepared? -- I think probably so.
      assert (thrd.domainKey.IsPrepared() == false);
#endif
      
      tdp->entry[count].oid = thrd.processKey.GetKeyOid();
      tdp->entry[count].allocCount = thrd.processKey.GetAllocCount();
      tdp->entry[count].schedNdx =
	thrd.cpuReserve - CpuReserve::CpuReserveTable; 

      coreGeneration[current].nThread++;
      count++;

      DEBUG(ckdir)
	MsgLog::dprintf(false, "Add snap thrd 0x%08x%08x ac=0x%08x\n",
			(uint32_t) (tdp->entry[count].oid >> 32),
			(uint32_t) tdp->entry[count].oid,
			tdp->entry[count].allocCount);
    }

    tdp->nDirent = count;
    assert(nextThreadDirLid[nextCkptHdr->nThreadPage]);
    nextCkptHdr->dirPage[nextCkptHdr->nThreadPage + nextCkptHdr->nRsrvPage] =
      nextThreadDirLid[nextCkptHdr->nThreadPage];
    nextCkptHdr->nThreadPage++;
  }
}

void
Checkpoint::AgeGenerations()
{
  DEBUG(ckage) MsgLog::dprintf(true, "Enter AgeGenerations\n");

  // First, forget the oldest generation present.
  CoreGeneration *cg = &coreGeneration[nGeneration-1];
  assert (cg->canReclaim);
  
  DEBUG(ckage) MsgLog::dprintf(true, "Removing object entries\n");
  cg->Reclaim();

  assert(cg->alloc.nFrames == cg->rsrv.nFrames);
  assert(cg->rsrv.nDirent == cg->alloc.nCoreDirent);
  
  for (int g = last_ckpt; g < nGeneration; g++)
    coreGeneration[g].CheckConsistency(true);

  DEBUG(ckage) MsgLog::dprintf(true, "Shift Generations\n");
  // Next, age everything remaining one generation.
  for (int g = nGeneration - 2; g >= 0; g--) {
    bcopy(&coreGeneration[g], &coreGeneration[g+1], sizeof(CoreGeneration));
    coreGeneration[g+1].index = g+1;
  }

  coreGeneration[current].Init();

  DEBUG(ckage) MsgLog::dprintf(true, "Exit AgeGenerations\n");
}

void
Checkpoint::TakeCheckpoint()
{
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top TakeCheckpoint()");
#endif
  if (migrationStatus != mg_Idle) {
    ProcessMigration();
    Thread::Current()->Yield();
  }
  
  MsgLog::dprintf(false,"!");
  
  DEBUG(ckpt) MsgLog::dprintf(true, "Enter TakeCheckpoint()\n");
  MsgLog::dprintf(true, "Checkpoint is declared\n");
  MarkDirtyNodesForCkpt();
  MarkDirtyPagesForCkpt();

  DEBUG(ckpt) MsgLog::dprintf(true, "Everything is now marked for checkpoint\n");

  // Ensure that all the invalidated PTE's stay that way:
  Machine::FlushTLB();

  DEBUG(ckpt) MsgLog::dprintf(true, "TLB is flushed\n");

  InitNextCkptHdr();

  DEBUG(ckpt) MsgLog::dprintf(true, "Next ckpt hdr valid\n");

  SnapshotReserves();

  DEBUG(ckpt) MsgLog::dprintf(true, "Rsrvs are snapshotted\n");

  SnapshotThreads();

  DEBUG(ckpt) MsgLog::dprintf(true, "Threads are snapshotted\n");

  AgeGenerations();

  DEBUG(ckpt) MsgLog::dprintf(true, "Generations have been aged\n");

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Bottom TakeCheckpoint()");
#endif

  DEBUG2(ckpt,migfinish) {
	MsgLog::dprintf(false, "About to start migration()\n");
        //Check::Pages();
  }
  StartMigration(true);

  // Must yield, since current thread has been deprepared by the fact
  // of having taken a checkpoint!
  Thread::Current()->Yield();
}

// Variables associated with stabilization and migration:
static uint32_t mg_nextCorePage;
static uint32_t mg_nextCoreNode;
static CoreDirent *mg_nextDirent;
static uint32_t mg_nextThreadDirPg;
static uint32_t mg_nextReserveDirPg;
static OID mg_objectPotOID;
static OID mg_tagPotOID;
#ifndef NDEBUG
static bool  mg_SanityChecked;
#endif

const char *
Checkpoint::MigStateName()
{
  static char *names[Checkpoint::mg_nState] = {
    "Idle",
    "StartCheckpoint",
    "StabilizeNodes",
    "StabilizePages",
    "WriteDir",
    "DrainCkpt",
    "WriteHeader",
    "StartMigration",
    "MigrateObjects",
    "DrainMigration",
    "UpdateRangeHeaders",
  };

  return names[migrationStatus];
}
			  
char
Checkpoint::MigStateAbbrev()
{
  static char letters[Checkpoint::mg_nState] = {
    ' ',			// idle shows blank
    'C',			// start checkpoitn
    'N',			// stabilize nodes
    'P',			// stabilize pages
    'D',			// writing directory
    'W',			// drain wait
    'H',			// write header
    'm',			// start migration
    'o',			// migrate objects
    'w',			// migrate drain wait
    'r',			// update range headers
  };

  return letters[migrationStatus];
}

void
Checkpoint::StartMigration(bool needStabilize)
{
  mg_nextCorePage = 0;
  mg_nextCoreNode = 0;
  mg_nextDirent = coreGeneration[last_ckpt].FirstDirEntry();
  mg_nextThreadDirPg = 0;
  mg_nextReserveDirPg = 0;
  mg_objectPotOID = 0;
  mg_tagPotOID = 0;
#ifndef NDEBUG
  mg_SanityChecked = false;
#endif

  migrationStatus = needStabilize ? mg_StartCheckpoint : mg_StartMigration;
  
  DEBUG(mig) MsgLog::printf("Starting migration in state %s\n",
			    MigStateName());
}

void
Checkpoint::StabilizeNodes()
{
  assert (Thread::Current());
  
  while (mg_nextCoreNode < ObjectCache::TotalNodes()) {
    if (Thread::Current()->pageWriteCount >= Thread::FairMigrateWrites) {
      Thread::Current()->pageWriteCount = 0;
      return;
    }
  
    Node *pNode = ObjectCache::GetCoreNodeFrame(mg_nextCoreNode);

    assert (pNode->GetFlags(OFLG_IO) == 0);
      
    if (pNode->GetFlags(OFLG_CKPT) && pNode->IsDirty()) {
      assert (pNode->IsFree() == false);
      WriteNodeToLog(pNode);
    }

    mg_nextCoreNode++;
  }

  if (Thread::Current()->pageWriteCount >= Thread::FairMigrateWrites) {
    Thread::Current()->pageWriteCount = 0;
    return;
  }
  
  // At this point, we know that everything is marked dirty that is
  // going to be:
  coreGeneration[last_ckpt].FinishActiveNodeFrame();

  mg_nextCoreNode = 0;
  migrationStatus = mg_StabilizePages;
}

// Note that StabilizePages also writes the log frames that were
// modified during node stabilization!
void
Checkpoint::StabilizePages()
{
  while (mg_nextCorePage < ObjectCache::TotalPages()) {
    if (Thread::Current()->pageWriteCount >= Thread::FairMigrateWrites) {
      Thread::Current()->pageWriteCount = 0;
      return;
    }
  
    ObjectHeader *pObj = ObjectCache::GetCorePageFrame(mg_nextCorePage);

    if (pObj->IsDirty()) {
      if (pObj->obType == ObType::PtLogPage ||
	  (pObj->GetFlags(OFLG_CKPT) &&
	   (pObj->obType == ObType::PtDataPage ||
	    pObj->obType == ObType::PtCapPage))) {
	assert (pObj->IsFree() == false);
	if (DBCOND(stabilize)) {
	  MsgLog::printf("stabilizing pObj=0x%08x oid=0x%08x%08x cur: %c chk: %c drt: %c%c\n",
			 pObj,
			 (uint32_t) (pObj->ob.oid >> 32),
			 (uint32_t) pObj->ob.oid,
			 pObj->GetFlags(OFLG_CURRENT) ? 'y' : 'n',
			 pObj->GetFlags(OFLG_CKPT) ? 'y' : 'n',
			 pObj->GetFlags(OFLG_DIRTY) ? 'y' : 'n',
			 pObj->GetFlags(OFLG_REDIRTY) ? 'y' : 'n'
			 );
	  MsgLog::printf("    pObj=0x%08x calccheck=0x%08x\n",
			 pObj,
#ifdef OB_MOD_CHECK
			 pObj->CalcCheck()
#else
			 0
#endif
			 );
	}
	assert ( PTE::ObIsNotWritable(pObj) );
	Persist::WritePage(pObj);

      }
    }
      
    mg_nextCorePage++;
  }

  mg_nextCorePage = 0;
  migrationStatus = mg_WriteDir;
}

void
Checkpoint::DrainDirentPage()
{
  if ( !nextDirPageHdr->IsDirty() ) {
    nextDirPage->nDirent = 0;
    nextDirPageHdr->ob.oid = UNDEF_LID;
    return;
  }

  // This needs to block, because we only posess a single in-core
  // directory entry page.  A regrettable consequence is that writing
  // the directory is a sequential operation across all threads
  // desiring to dirty pages.  I should look into doing this in lazy
  // fashion -- pre-reserving the directory page may simply be
  // unnecessary; I did it to ensure that I wouldn't deadlock stuck
  // while writing out directory pages because I didn't have time to
  // think through the relevant possible bugs.
  
  if (nextDirPageHdr->GetFlags(OFLG_IO) == 0) {
    // Add an entry in the master header for this directory page,
    // and write it to the disk.
    lid_t lid = nextDirPageHdr->ob.oid;
    
    if (lid == UNDEF_LID) {
      lid = coreGeneration[last_ckpt].AllocateLid();
      coreGeneration[last_ckpt].alloc.nDirFrame++;
      nextDirPageHdr->ob.oid = lid;

      uint32_t firstDirEntry =
	nextCkptHdr->nRsrvPage + nextCkptHdr->nThreadPage; 
      uint32_t dirEntry = firstDirEntry + nextCkptHdr->nDirPage++;

      nextDirPageHdr->ob.oid = lid;

      nextCkptHdr->dirPage[dirEntry] = lid;
      nextCkptObHdr->SetDirtyFlag();
    }
    Persist::WritePageToLog(nextDirPageHdr, lid, true);
  }
  else {
    Thread::Current()->SleepOn(nextDirPageHdr->ObjectSleepQueue());
    Thread::Current()->Yield();
  }
}

void
Checkpoint::AppendDiskDirent(CoreDirent* cde)
{
  if (nextDirPage->nDirent == CkptDirPage::maxDirEnt) {
    DEBUG(stabilize)
      MsgLog::dprintf(true, "Draining full disk dirent page\n");
		    
    DrainDirentPage();
  }

#ifdef PARANOID_CKPT
  if (nextDirPage->nDirent >= CkptDirPage::maxDirEnt)
    Debugger();
#endif
      
  CkptDirent &cd = nextDirPage->entry[nextDirPage->nDirent];
  cd.oid = cde->oid;
  cd.count = cde->count;
  cd.lid = cde->lid;
  cd.type = cde->type;
  nextDirPageHdr->SetDirtyFlag();
  nextDirPage->nDirent++;
}

void
Checkpoint::WriteCkptDirectory()
{
#ifndef NDEBUG
  // Sanity check:  If all pages and nodes have gone out, there should
  // exist no entries in the respective core directories with an
  // UNDEF_LOGLOC entry.
  if (mg_SanityChecked == false) {
    while (mg_nextDirent != CkNIL) {
      assert ( mg_nextDirent->lid != UNDEF_LID );
      assert ( mg_nextDirent->lid != DEAD_LID );
      mg_nextDirent = mg_nextDirent->Successor();
    }
    
    mg_nextDirent = coreGeneration[last_ckpt].FirstDirEntry();
    mg_SanityChecked = true;
  }
#endif

  while (mg_nextDirent != CkNIL) {
    if (Thread::Current()->pageWriteCount >= Thread::FairMigrateWrites) {
      Thread::Current()->pageWriteCount = 0;
      return;
    }

    AppendDiskDirent(mg_nextDirent);
    
    mg_nextDirent = mg_nextDirent->Successor();
  }

  if (Thread::Current()->pageWriteCount >= Thread::FairMigrateWrites) {
    Thread::Current()->pageWriteCount = 0;
    return;
  }

  // This must not be conditionalized on dirtyness, or the nDirent
  // field will not be reset properly!
  DrainDirentPage();

  DEBUG(wrckdir)
    MsgLog::dprintf(true, "Last dirent page had %d entries\n",
		    nextDirPage->nDirent); 

#define NUM_CONTAINERS(x, y) (((x) + (y) - 1) / (y))
  uint32_t nThreadPage =
    NUM_CONTAINERS(coreGeneration[last_ckpt].nThread, ThreadDirPage::maxDirEnt);
  uint32_t nRsrvPage =
    NUM_CONTAINERS(coreGeneration[last_ckpt].nReserve, ReserveDirPage::maxDirEnt);

  assert (nRsrvPage == nextCkptHdr->nRsrvPage);
  assert (nThreadPage == nextCkptHdr->nThreadPage);

  while (mg_nextReserveDirPg < nRsrvPage) {
    if (Thread::Current()->pageWriteCount >= Thread::FairMigrateWrites) {
      Thread::Current()->pageWriteCount = 0;
      return;
    }

    DEBUG(wrckdir) {
      MsgLog::dprintf(true, "Writing rsrv dir pg %d to 0x%08x\n",
		      mg_nextReserveDirPg,
		      nextReserveDirLid[mg_nextReserveDirPg]);
      if (reserveDirHdr[mg_nextReserveDirPg]->IsDirty() == false) {
	MsgLog::dprintf(true, "Reserve dir pg %d not dirty!\n",
			mg_nextReserveDirPg);
	reserveDirHdr[mg_nextReserveDirPg]->SetDirtyFlag();
      }
    }
    Persist::WritePageToLog(reserveDirHdr[mg_nextReserveDirPg],
			    nextReserveDirLid[mg_nextReserveDirPg]);

    mg_nextReserveDirPg++;
  }

  while (mg_nextThreadDirPg < nThreadPage) {
    if (Thread::Current()->pageWriteCount >= Thread::FairMigrateWrites) {
      Thread::Current()->pageWriteCount = 0;
      return;
    }

    DEBUG(wrckdir) {
      MsgLog::dprintf(true, "Writing thread dir pg %d to 0x%08x\n",
		      mg_nextThreadDirPg, nextThreadDirLid[mg_nextThreadDirPg]);
      if (threadDirHdr[mg_nextThreadDirPg]->IsDirty() == false) {
	MsgLog::dprintf(true, "Thread dir pg %d not dirty!\n",
			mg_nextThreadDirPg);
	threadDirHdr[mg_nextThreadDirPg]->SetDirtyFlag();
      }
    }
    Persist::WritePageToLog(threadDirHdr[mg_nextThreadDirPg],
			    nextThreadDirLid[mg_nextThreadDirPg]);

    mg_nextThreadDirPg++;
  }

  mg_nextDirent = coreGeneration[last_ckpt].FirstDirEntry();
  mg_nextThreadDirPg = 0;
  mg_nextReserveDirPg = 0;

#ifndef NDEBUG
  mg_SanityChecked = false;
#endif
  
  nextCkptObHdr->SetDirtyFlag();

  migrationStatus = mg_DrainCheckpoint;
}

void
Checkpoint::WriteCkptHeader()
{
  if (!nextCkptObHdr->IsDirty()) {

    // Up to this point, we have been writing down the 'next'
    // checkpoint.  Once the checkpoint header has been written, the
    // 'next' checkpoint becomes the 'last' checkpoint.
    //
    // At this point in the logic, we also logically discard all of the
    // remaining directory pages of the OLD last checkpoint.  In
    // practice, the on-disk object directories were discarded when
    // migration completed, and the thread and reserve directory pages
    // will be recycled when the time comes to write the NEW next
    // checkpoint.

    DEBUG2(ckage,mig) MsgLog::dprintf(true, "About to release Aged Storage\n");

    CoreGeneration &fm = coreGeneration[first_migrated];
    
    // Release the storage associated with the checkpoint directory in
    // what used to be the last checkpoint generation:
    uint32_t entry = lastCkptHdr->nRsrvPage + lastCkptHdr->nThreadPage;

    for (uint32_t i = 0; i < lastCkptHdr->nDirPage; i++, entry++) {
      lid_t dirLoc = lastCkptHdr->dirPage[entry];
      Checkpoint::DeallocateLid(dirLoc);
      fm.release.nDirFrame++;
      fm.release.nFrames++;
    }

    assert (fm.alloc.nDirFrame == fm.release.nDirFrame);
    lastCkptHdr->nDirPage = 0;
    
    // Now that a new checkpoint has been stabilized, we can permit
    // storage from the previous checkpoint to be reclaimed, and we
    // can free the directory pages associated with that checkpoint.
    fm.canReclaim = true;

    SwapCheckpointHeaders();
    SwapThreadDirs();
    InitNextCkptHdr();		// probably unnecessary - done in
				// TakeCheckpoint() 

    nCheckpointsCompleted++;
    migrationStatus = mg_MigrateObjects;
#ifdef TESTING_CKPT
    Debugger();
#endif
    return;
  }

  // This needs to block, because we only posess a single in-core
  // directory entry page.  A regrettable consequence is that writing
  // the directory is a sequential operation across all threads
  // desiring to dirty pages.  I should look into doing this in lazy
  // fashion -- pre-reserving the directory page may simply be
  // unnecessary; I did it to ensure that I wouldn't deadlock stuck
  // while writing out directory pages because I didn't have time to
  // think through the relevant possible bugs.
  
  if (nextCkptObHdr->GetFlags(OFLG_IO) == 0) {
    nextCkptObHdr->ob.oid = nextCkptHdrLid;
      
    DEBUG(ckdir)
      MsgLog::dprintf(true, "Writing CkHdr seq=0x%08x to 0x%08x, dirpg %d\n",
		      (uint32_t) nextCkptHdr->sequenceNumber,
		      nextCkptHdrLid, nextCkptHdr->nDirPage);

    Persist::WritePageToLog(nextCkptObHdr, nextCkptHdrLid, true);
  }
  else {
    Thread::Current()->SleepOn(nextCkptObHdr->ObjectSleepQueue());
    Thread::Current()->Yield();
  }
}

void
Checkpoint::DrainLastObjectPot()
{
  ObjectHeader *pPot = ObjectHeader::Lookup(ObType::PtObjectPot, mg_objectPotOID);
  if (pPot && (pPot->IsDirty()))
    Persist::WritePage(pPot);
}

ObjectHeader *
Checkpoint::GetObjectPot(OID oid)
{
  FrameInfo fi(oid);
  if (fi.cd == 0)
    MsgLog::fatal("Migrating to unknown range!\n");
  
  // Bring in the object pot from the home location:
  ObjectHeader *pObjectPot = Persist::GetObjectPot(fi);
  pObjectPot->TransLock();

  // If this object is going to a new pot, and the old pot is still in
  // core and is dirty, start the old pot to the disk:
  if (pObjectPot->ob.oid != mg_objectPotOID) {
    DrainLastObjectPot();
  }

  mg_objectPotOID = pObjectPot->ob.oid;

  return pObjectPot;
}

// Returns true iff tag changed.
bool
Checkpoint::UpdateTagPot(CoreDirent *cd, ObCount count)
{
  bool result = false;
  
  FrameInfo fi(cd->oid);
  if (fi.cd  == 0)
    MsgLog::fatal("Bad oid 0x%08x%08x to UpdateTagPot()\n",
		  (cd->oid >> 32), cd->oid);

  ObjectHeader *pTagHdr = Persist::GetTagPot(fi);
  pTagHdr->TransLock();

  // If this object is going to a new tag pot, and the old tag pot is
  // still in core and is dirty, start it to the disk:
  if (pTagHdr->ob.oid != mg_tagPotOID)
    DrainLastTagPot();

  mg_tagPotOID = pTagHdr->ob.oid;

#if 0
  MsgLog::printf("pTagHdr 0x%08x entry %d\n", pTagHdr, fi.tagEntry);
#endif

  PagePot* pPagePot = (PagePot *) ObjectCache::ObHdrToPage(pTagHdr);

  // If the tag is incorrect, we may need to set all the allocation
  // counts on the objects themselves.  This is a bloody great mess.
  // We don't need to do it for objects that occupy the full frame, as
  // in such cases we have the correct answer in hand already.
  //
  // Note that we are careful to ensure always that the tag pot
  // allocation count is ALWAYS the max of the member counts.  This
  // eliminates the need to make an additional type-dependent pass
  // over the frame here.
  
  uint8_t tagType = cd->type;
  if (cd->type == FRM_TYPE_ZNODE)
    tagType = FRM_TYPE_NODE;
  
  if (pPagePot->type[fi.tagEntry] != tagType) {
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("In retag()");
#endif

    pPagePot->type[fi.tagEntry] = tagType;
    pTagHdr->SetDirtyFlag();
#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr)
      Check::Consistency("Post-In retag()");
#endif
    result = true;
  }
  
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Post retag()");
#endif

  // Count is *usually* the allocation count, but for processes it is
  // the greater of the allocation count and the call count.

  if (pPagePot->count[fi.tagEntry] < count) {
    pPagePot->count[fi.tagEntry] = count;
    pTagHdr->SetDirtyFlag();
  }

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Exit UpdateTagPot()");
#endif

  return result;
}

// The first time a node migrates to a pot that is not currently a node
// pot, we need to reformat that pot.  It is guaranteed that all nodes
// within the pot are present in the ckpt generation, except those
// superceded in the current generation.  We need, however, to make
// sure that the content of the pot is validly formatted with nodes so
// that the RetagFrame logic will compute correct allocation counts.
// Since the ckpt/current nodes are guaranteed to have higher counts,
// we simply set all alloc/call counts to ZERO here.
void
Checkpoint::InitNodePot(DiskNode *pDiskNode, OID oid)
{
  for (int nd = 0; nd < EROS_NODES_PER_FRAME; nd++, oid++) {
    pDiskNode[nd].oid = oid;
    pDiskNode[nd].allocCount = 0llu;
    pDiskNode[nd].callCount = 0llu;
    for (unsigned int k = 0; k < EROS_NODE_SIZE; k++)
      pDiskNode[nd][k].ZeroInitKey();
  }
}

void
Checkpoint::MigrateNode(CoreDirent *cd)
{
#if defined(DBG_WILD_PTR)
  if (dbg_wild_ptr)
    Check::Consistency("Top MigrateNode()");
#endif
  
  assert (cd->type == FRM_TYPE_NODE || cd->type == FRM_TYPE_ZNODE);

  DEBUG(mignode)
    MsgLog::printf("Migrating node 0x%08x%08x, ty=%d lid=0x%x\n",
		   (uint32_t) (cd->oid >> 32),
		   (uint32_t) (cd->oid),
		   cd->type, cd->lid);
  
  // If the node is still in core, it is the checkpoint version,
  // because we will not be asked to migrate it unless it is the
  // latest one.  If so, take it from that version:
  Node *pNode = ObjectHeader::LookupNode(cd->oid);
  if (pNode)
    pNode->TransLock();
  
#if 0
  // This test is bogus.  Under extreme pressure, the object may be
  // aged to the disk and reloaded between the time it is stabilized
  // and the time we migrate it.  It need not be modified when
  // reloaded, but the ckpt flag will not be set if that event
  // sequence has occurred.  It's much more likely to happen with
  // nodes than with pages, since node pots age out relatively slowly.
  assert (pNode == 0 || pNode->flags.ckpt);
#endif
  assert (pNode == 0 || pNode->IsDirty() == false);

  ObjectHeader *pPot = GetObjectPot(cd->oid);
  pPot->TransLock();
  
  assert (pPot->obType == ObType::PtObjectPot);
  
  uint32_t offset = (uint32_t) (cd->oid - pPot->ob.oid);
  assert( offset < DISK_NODES_PER_PAGE );

  DiskNode *pDiskNode = (DiskNode *) ObjectCache::ObHdrToPage(pPot);

  // The pot we are writing must be marked dirty, but no log space
  // should be allocated for it, so just do it by hand:
  pPot->SetDirtyFlag();

  if (pNode) {
    // Call count gets bumped whenever allocation count gets bumped,
    // so just use that:
    assert (pNode->callCount >= pNode->ob.allocCount);
    if ( UpdateTagPot(cd, pNode->callCount) )
      InitNodePot(pDiskNode, pPot->ob.oid);
    
#if defined(DBG_WILD_PTR)
    if (dbg_wild_ptr)
      Check::Consistency("pNode nonzero, post UpdateTagPot()");
#endif

    pDiskNode[offset] = *pNode;
  }
  else if (cd->type == FRM_TYPE_ZNODE) {
    // Could use LoadCurrentNode above, but that would force an
    // allocation of a node frame for a node that isn't really active:
    
    assert(cd->lid == ZERO_LID);

    if ( UpdateTagPot(cd, cd->count) )
      InitNodePot(pDiskNode, pPot->ob.oid);

#if defined(DBG_WILD_PTR)
    if (dbg_wild_ptr)
      Check::Consistency("pNode nonzero, post UpdateTagPot()");
#endif

    pDiskNode[offset].oid = cd->oid;
    pDiskNode[offset].allocCount = cd->count;
    pDiskNode[offset].callCount = cd->count;
    for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
      /* not hazarded because disk key */
      pDiskNode[offset][i].ZeroInitKey();
    }
  }
  else {
    assert(CONTENT_LID(cd->lid));

    // Otherwise, we need to bring in the associated log pot if not
    // already present:
    ObjectHeader *pLogPot = Persist::GetCkFrame(cd->lid);
    pLogPot->TransLock();
    assert (pLogPot->IsDirty() == false);
    
    assert( pLogPot );

    DiskNode *which = (DiskNode*) ObjectCache::ObHdrToPage(pLogPot);
    which += (cd->lid % EROS_OBJECTS_PER_FRAME);

    // Call count gets bumped whenever allocation count gets bumped,
    // so just use that:
    assert (which->callCount >= which->allocCount);
    if ( UpdateTagPot(cd, which->callCount) )
      InitNodePot(pDiskNode, pPot->ob.oid);
    
#if defined(DBG_WILD_PTR)
    if (dbg_wild_ptr)
      Check::Consistency("pNode zero, post UpdateTagPot()");
#endif

    pDiskNode[offset] = *which;
  }

#if defined(DBG_WILD_PTR)
  if (dbg_wild_ptr)
    Check::Consistency("After node migration");
#endif
}

void
Checkpoint::MigratePage(CoreDirent *cd)
{
  assert (cd->type == FRM_TYPE_ZDPAGE ||
	  cd->type == FRM_TYPE_DPAGE ||
	  cd->type == FRM_TYPE_ZCPAGE ||
	  cd->type == FRM_TYPE_CPAGE);
  
  DEBUG(migpage)
    MsgLog::printf("Migrating page 0x%08x%08x, ty=%d lid=0x%x\n",
		   (uint32_t) (cd->oid >> 32),
		   (uint32_t) (cd->oid),
		   cd->type, cd->lid);
  
  // If the page is still in core, it MAY still be the checkpoint
  // version.  If so, take it from that version:
  ObjectHeader *pPage = ObjectHeader::Lookup(ObType::PtDataPage, cd->oid);
  pPage->TransLock();
  
#if 0
  // This test is bogus.  Under extreme pressure, the object may be
  // aged to the disk and reloaded between the time it is stabilized
  // and the time we migrate it.  It need not be modified when
  // reloaded, but the ckpt flag will not be set if that event
  // sequence has occurred.  It's much more likely to happen with
  // nodes than with pages, since node pots age out relatively slowly.
  // We shouldn't be migrating if it's been dirtied since anyway.
  assert (pPage == 0 || pPage->flags.ckpt);
#endif
  assert (pPage == 0 || pPage->IsDirty() == false);

  UpdateTagPot(cd, cd->count);

  if (cd->type == FRM_TYPE_DPAGE ||
      cd->type == FRM_TYPE_CPAGE) {

    assert(CONTENT_LID(cd->lid));
    // If it wasn't in core, we need it now:
    if (pPage == 0)
      pPage = Persist::GetCkFrame(cd->lid);

    assert(pPage);
    assert (pPage->IsDirty() == false);

    assert ( PTE::ObIsNotWritable(pPage) );
    Persist::WritePageToHome(pPage, cd->oid);
  }
  else {
    assert (cd->type == FRM_TYPE_ZDPAGE ||
	    cd->type == FRM_TYPE_ZCPAGE);
    assert(cd->lid == ZERO_LID);
  }
}

void
Checkpoint::MigrateObjects()
{
  assert (Thread::Current());

  for ( ; mg_nextDirent != CkNIL;
	mg_nextDirent = mg_nextDirent->Successor() ) {

    if (Thread::Current()->pageWriteCount >= Thread::FairMigrateWrites) {
      Thread::Current()->pageWriteCount = 0;
      return;
    }

    // If object has been redirtied, no need to migrate it:
    if (coreGeneration[current].FindObject(mg_nextDirent->oid) != CkNIL)
      continue;
					   
    switch(mg_nextDirent->type) {
    case FRM_TYPE_ZDPAGE:
    case FRM_TYPE_DPAGE:
    case FRM_TYPE_ZCPAGE:
    case FRM_TYPE_CPAGE:
      MigratePage(mg_nextDirent);
      break;
    case FRM_TYPE_NODE:
    case FRM_TYPE_ZNODE:
      MigrateNode(mg_nextDirent);
      break;
    default:
      MsgLog::fatal("Attempt to migrate unknown type %d\n",
		    mg_nextDirent->type);
    }
  }
    
  DrainLastObjectPot();
  
  DrainLastTagPot();
  
  mg_objectPotOID = 0;
  mg_nextDirent = coreGeneration[last_ckpt].FirstDirEntry();
  migrationStatus = mg_DrainMigration;
}


void
Checkpoint::DrainLastTagPot()
{
  ObjectHeader *pPot = ObjectHeader::Lookup(ObType::PtAllocPot,
					    mg_tagPotOID); 
  if (pPot && (pPot->IsDirty()))
    Persist::WritePage(pPot);
}

void
Checkpoint::DrainMigrationCallback(DuplexedIO *)
{
  DEBUG(mig) MsgLog::dprintf(true, "Migration drainage callback happened\n");
  migrationStatus = mg_UpdateRangeHeaders;
}

void
Checkpoint::DrainMigration()
{
  BlockDev::PlugAllBlockDevices(DrainMigrationCallback);
}

void
Checkpoint::UpdateRangeHeaders()
{
  // We should be able to simply scribble the last checkpoint header
  // at the front of every range...
  Persist::WriteRangeHeaders(lastCkptObHdr);

  migrationStatus = mg_Idle;
}

void
Checkpoint::DrainCheckpointCallback(DuplexedIO *)
{
  DEBUG(mig) MsgLog::dprintf(true, "Checkpoint drainage callback happened\n");
  migrationStatus = mg_WriteHeader;
}

void
Checkpoint::DrainCheckpoint()
{
  BlockDev::PlugAllBlockDevices(DrainCheckpointCallback);
}

bool
Checkpoint::ProcessMigration()
{
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top ProcessMigration()");
#endif
  
#ifndef NDEBUG
  Checkpoint::CheckConsistency("Top ProcessMigration", false);
#endif

  if (migrationStatus == mg_Idle) {
    return true;
  }

  Console::ShowTwiddleChar(MigStateAbbrev(),1);

  DEBUG(mig)
    MsgLog::dprintf(true, "Checkpoint::ProcessMigration(): enter in state %s\n",
		    MigStateName());

  // In the current design, we no longer need to wait for I/O to
  // stabilize, because if outbound I/O is in progress on the object
  // then it is on the way to the log, and the attempt to write it
  // again will block while allocating the IoRequest structure.
  
  if (migrationStatus == mg_StartCheckpoint)
    migrationStatus = mg_StabilizeNodes;

  if (migrationStatus == mg_StabilizeNodes)
    StabilizeNodes();

  if (migrationStatus == mg_StabilizePages)
    StabilizePages();

  if (migrationStatus == mg_WriteDir)
    WriteCkptDirectory();

  if (migrationStatus == mg_DrainCheckpoint)
    DrainCheckpoint();
  
  if (migrationStatus == mg_WriteHeader)
    WriteCkptHeader();

  if (migrationStatus == mg_StartMigration)
    migrationStatus = mg_MigrateObjects;
  
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Before MigrateObjects()");
#endif

  if (migrationStatus == mg_MigrateObjects)
    MigrateObjects();

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("After MigrateObjects()");
#endif

  if (migrationStatus == mg_DrainMigration)
    DrainMigration();
  
  if (migrationStatus == mg_UpdateRangeHeaders)
    UpdateRangeHeaders();
  
  DEBUG(mig) MsgLog::dprintf(true, "Checkpoint::ProcessMigration(): exit in state %s\n",
		  MigStateName());

  DEBUG(migfinish)
    if (migrationStatus == mg_Idle) {
      MsgLog::dprintf(false, "Migration completed\n");
      //Check::Pages();
    }
    
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Bottom ProcessMigration()");
#endif
  
  return false;
}

bool
#ifndef NDEBUG
Checkpoint::CheckConsistency(const char *msg, bool allAllocated)
#else
Checkpoint::CheckConsistency(const char *, bool allAllocated)
#endif
{
  bool ckresult = true;
  
  IRQ::DISABLE();
  
#ifndef NDEBUG
  REQUIRE( CoreDirent::CheckConsistency(msg) );
#endif
  
  if (! coreGeneration[0].CheckConsistency(false) )
    ckresult = false;
  
  if ( !coreGeneration[1].CheckConsistency(allAllocated) )
    ckresult = false;

  REQUIRE (nAllocatedLogFrame <= nReservedLogFrame);
  REQUIRE (nReservedLogFrame <= nAvailLogFrame);

  uint32_t totRsrv = 0;
  uint32_t totAlloc = 0;
  uint32_t totRelease = 0;

  for (uint32_t i = 0; i < nGeneration; i++) {
    totRsrv += coreGeneration[i].rsrv.nFrames;
    totAlloc += coreGeneration[i].alloc.nFrames;
    totRelease += coreGeneration[i].release.nFrames;
  }
  
  REQUIRE ( nReservedLogFrame == totRsrv - totRelease);
  REQUIRE ( nAllocatedLogFrame == totAlloc - totRelease);

  REQUIRE (nAllocatedLogFrame + nMasterPage == allocMap.NumAllocated());

  IRQ::ENABLE();
  
  return ckresult;
}

#ifdef DDB
void
Checkpoint::ddb_dump_mig_status()
{
  extern void db_printf(const char *fmt, ...);

  db_printf("Migration state: %s nCheckpointsCompleted: %d\n",
	    MigStateName(), nCheckpointsCompleted);

  db_printf("nxtCrPg      %6d nxtCrNd      %6d nxtCrDirent 0x%x\n",
	    mg_nextCorePage, mg_nextCoreNode, mg_nextDirent);
  db_printf("nxtThrdDirPg %6d nxtRsrvDirPg %6d\n",
	    mg_nextThreadDirPg, mg_nextReserveDirPg);
  db_printf("obPotOid 0x%08x%08x tagPotOid 0x%08x%08x\n",
	    (uint32_t) (mg_objectPotOID >> 32),
	    (uint32_t) (mg_objectPotOID),
	    (uint32_t) (mg_tagPotOID >> 32),
	    (uint32_t) (mg_tagPotOID));
}
#endif
