/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/DiskNode.hxx>
#include <disk/PagePot.hxx>
#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Checkpoint.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/Machine.hxx>

#define dbg_ckdir	0x1	/* steps in ckdir reconstruction */
#define dbg_reload	0x2	/* steps in reload */
#define dbg_ckinit	0x4	/* steps in taking snapshot */

#if 0
#define dbg_ckpt	0x1	/* steps in taking snapshot */
#define dbg_ckage	0x2	/* migration state machine */
#define dbg_mig		0x4	/* migration state machine */
#define dbg_reservation	0x40	/* reservation logic */
#define dbg_wrckdir	0x80	/* writing ckdir */
#define dbg_dirent	0x100	/* dirent manipulation */
#define dbg_stabilize	0x200	/* stabilization */
#define dbg_retag	0x400	/* stabilization */
#endif

/* Following should be an OR of some of the above */
#define dbg_flags   (0u)

#define DBCOND(x) (dbg_##x & dbg_flags)
#define DEBUG(x) if DBCOND(x)
#define DEBUG2(x,y) if ((dbg_##x|dbg_##y) & dbg_flags)

static ThreadPile CheckpointedThreads;

void
Checkpoint::Init()
{
  assert (EROS_NODES_PER_FRAME == DISK_NODES_PER_PAGE);
  
  enabled = false;		// until we know otherwise
  
  for (uint32_t i = 0; i < nThreadDirPage; i++) {
    threadDirHdr[i] = 0;
    threadDirPg[i] = 0;
    lastThreadDirLid[i] = 0;
    nextThreadDirLid[i] = 0;
  }
  
  for (uint32_t i = 0; i < nReserveDirPage; i++) {
    reserveDirHdr[i] = 0;
    reserveDirPg[i] = 0;
    lastReserveDirLid[i] = 0;
    nextReserveDirLid[i] = 0;
  }

  for (uint32_t i = 0; i < nGeneration; i++) {
    coreGeneration[i].Init();
    coreGeneration[i].index = i;
    if (i > last_ckpt)
      coreGeneration[i].canReclaim = true;
  }

  lastCkptHdr = 0;
  nextCkptHdr = 0;
  migrationStatus = mg_Idle;
  nCheckpointsCompleted = 0;
  
  CheckConsistency("Bottom Chekpoint::Init()", true);
}

void
Checkpoint::LoadDirectoryHeaders()
{
  // These must be allocated by hand in order to get the correct log
  // locations allocated.  The master checkpoint headers do not appear
  // within the page allocations of the disk checkpoint areas.  They
  // are not considered "available" for allocation.
  AllocateMasterLid((lid_t)0);
  AllocateMasterLid((lid_t)(1 * EROS_OBJECTS_PER_FRAME));

  DEBUG(ckdir) MsgLog::printf("reading ckhdr 0\n");

  lastCkptHdrLid = 0;
  lastCkptObHdr = Persist::KernGetCkFrame(lastCkptHdrLid);

  // FIX: need to check for IO error.  Not sure how yet.

  assert(lastCkptObHdr);

#if 1
  lastCkptObHdr->Unintern();
  lastCkptObHdr->obType = ObType::PtDriverPage; // FIX: Race condition?
#else
  lastCkptObHdr->KernPin();	// forever...
#endif

  lastCkptHdr = (DiskCheckpoint*) ObjectCache::ObHdrToPage(lastCkptObHdr);

  DEBUG(ckdir) MsgLog::printf("0: pObj 0x%08x ppg 0x%08x, seqno 0x%x nThreadPg %d nDirPg %d\n",
		 lastCkptObHdr,
		 lastCkptHdr,
		 (uint32_t) lastCkptHdr->sequenceNumber,
		 (uint32_t) lastCkptHdr->nThreadPage,
		 (uint32_t) lastCkptHdr->nDirPage);

  // In case ckhdr 1 is the current one, read that too:
  
  DEBUG(ckdir) MsgLog::printf("reading ckhdr 1\n");

  nextCkptHdrLid = (lid_t)(1 * EROS_OBJECTS_PER_FRAME);
  nextCkptObHdr = Persist::KernGetCkFrame(nextCkptHdrLid);
  assert(nextCkptObHdr);
  // FIX: need to check for IO error.  Not sure how yet.

#if 1
  nextCkptObHdr->Unintern();
  nextCkptObHdr->obType = ObType::PtDriverPage; // FIX: Race condition?
#else
  nextCkptObHdr->KernPin();	// forever...
#endif

  nextCkptHdr = (DiskCheckpoint*) ObjectCache::ObHdrToPage(nextCkptObHdr);

  DEBUG(ckdir) MsgLog::printf("1: pObj 0x%08x ppg 0x%08x, seqno 0x%x nThreadPg %d nDirPg %d\n",
		 nextCkptObHdr,
		 nextCkptHdr,
		 (uint32_t) nextCkptHdr->sequenceNumber,
		 (uint32_t) nextCkptHdr->nThreadPage,
		 (uint32_t) nextCkptHdr->nDirPage);

  // Might have the headers swapped:

  if (nextCkptHdr->sequenceNumber == lastCkptHdr->sequenceNumber)
    MsgLog::fatal("Cannot start: Checkpoint headers corrupted\n");

  if (nextCkptHdr->sequenceNumber > lastCkptHdr->sequenceNumber)
    SwapCheckpointHeaders();
  
  CheckConsistency("bot Checkpoint::LoadDirHdrs", true);
}

// Reserve pages are allocated from the checkpoint log just like
// everything else, but they are allocated as master locations rather
// than per-checkpoint locations.
void
Checkpoint::ReloadSavedReserves()
{
  DEBUG(reload) MsgLog::dprintf(true, "Reading %d saved reserve pages...\n",
		 lastCkptHdr->nRsrvPage);

  uint32_t entry = 0;
  
  for (uint32_t i = 0; i < lastCkptHdr->nRsrvPage; i++, entry++) {
    lid_t dirLid = lastCkptHdr->dirPage[entry];

    AllocateMasterLid(dirLid);
    lastReserveDirLid[i] = dirLid;
    
    ObjectHeader *pObj = Persist::KernGetCkFrame(dirLid);

    assert(pObj);

#if 1
    pObj->Unintern();
    pObj->obType = ObType::PtDriverPage; // FIX: Race condition?
#else
    pObj->KernPin();	// forever...
#endif

    reserveDirHdr[i] = pObj;

    ReserveDirPage *rdp = (ReserveDirPage *) ObjectCache::ObHdrToPage(pObj);
    reserveDirPg[i] = rdp;

    for (uint32_t ndx = 0; ndx < rdp->nDirent; ndx++) {
      coreGeneration[last_ckpt].nReserve++;

      CpuReserveInfo& ckrsrv = rdp->entry[ndx];
      CpuReserve& rsrv = CpuReserve::CpuReserveTable[ckrsrv.index];

      DEBUG(ckdir) if (ckrsrv.index < 5)
	MsgLog::dprintf(true, "[%3d], dur 0x%08x%08x per 0x%08x%08x\n"
			"quant 0x%08x%08x normPrio %d rsrvPrio %d\n",
			ckrsrv.index,
			(uint32_t) (ckrsrv.duration>>32),
			(uint32_t) ckrsrv.duration,
			(uint32_t) (ckrsrv.period>>32),
			(uint32_t) ckrsrv.period,
			(uint32_t) (ckrsrv.quanta>>32),
			(uint32_t) ckrsrv.quanta,
			ckrsrv.normPrio,
			ckrsrv.rsrvPrio);

      rsrv.residQuanta = 0;
      rsrv.expired = true;
      rsrv.residDuration = 0;
      rsrv.active = false;
      rsrv.quanta = ckrsrv.quanta;
      rsrv.duration = ckrsrv.duration;
      rsrv.period = ckrsrv.period;
      rsrv.start = ckrsrv.start;
      rsrv.normPrio = ckrsrv.normPrio;
      rsrv.rsrvPrio = ckrsrv.rsrvPrio;

      rsrv.Reset();
    }
  }

  DEBUG(reload) MsgLog::dprintf(true, "Done reading reserves...\n");

  CheckConsistency("bot Ckpt::ReloadSavedResources", true);
}

void
Checkpoint::ReloadSavedThreads()
{
  DEBUG(reload) MsgLog::dprintf(true, "Reading %d saved thread pages...\n",
		 lastCkptHdr->nThreadPage);

  uint32_t entry = lastCkptHdr->nRsrvPage;

  for (uint32_t i = 0; i < lastCkptHdr->nThreadPage; i++, entry++) {
    lid_t dirLid = lastCkptHdr->dirPage[entry];
    
    AllocateMasterLid(dirLid);
    lastThreadDirLid[i] = dirLid;
    
    ObjectHeader *pObj = Persist::KernGetCkFrame(dirLid);
    assert(pObj);

#if 1
    pObj->Unintern();
    pObj->obType = ObType::PtDriverPage; // FIX: Race condition?
#else
    pObj->KernPin();	// forever...
#endif

    threadDirHdr[i] = pObj;
    
    ThreadDirPage *tdp = (ThreadDirPage *) ObjectCache::ObHdrToPage(pObj);
    threadDirPg[i] = tdp;

    for (uint32_t ndx = 0; ndx < tdp->nDirent; ndx++) {
      // Fabricate a thread for each entry:

      coreGeneration[last_ckpt].nThread++;

      ThreadDirent& tde = tdp->entry[ndx];

      DEBUG(ckdir) MsgLog::dprintf(true, "Adding thread for OID 0x%08x%08x ac=%d\n",
		      (uint32_t) (tde.oid>>32),
		      (uint32_t) tde.oid,
		      tde.allocCount);

      // FIX: What forged priority should we use (probably HIGH) --
      // need to make sure that preparing a thread updates the
      // priority field properly.
      
      Thread* thread = new Thread;
      assert(thread);

      assert (thread->processKey.IsUnprepared());
      assert( thread->processKey.IsHazard() == false );

      // Forge a domain key for this thread:
      Key& k = thread->processKey;
      k.InitType(KtProcess);
      k.SetUnprepared();
      k.unprep.oid = tde.oid;
      k.unprep.count = tde.allocCount;
      k.subType = 0;
      k.keyData = 0;

      CpuReserve::CpuReserveTable[tde.schedNdx].AddUserThread(thread);

      thread->SleepOn(CheckpointedThreads);
    }
  }

  DEBUG(reload) MsgLog::dprintf(true, "All threads now on wakeup stall q\n");
  
  CheckConsistency("bot Ckpt::ReloadSavedThreads()", true);
}

void
Checkpoint::AddCkDirent(CkptDirent& de, uint8_t generation)
{
  assert (de.type != FRM_TYPE_INVALID);
  assert (de.lid != UNDEF_LID);
      
  assert (de.lid <= CkptDirent::MaxLidValue);
	
  CoreDirent *cld = new CoreDirent;
  assert(cld);
      
  cld->oid = de.oid;
  cld->count = de.count;
  cld->lid = de.lid;
  cld->type = de.type;
  cld->color = CoreDirent::red;
      
  coreGeneration[generation].rsrv.nDirent++;
  coreGeneration[generation].alloc.nCoreDirent++;

  DEBUG(ckdir) {
    const char* cty = "??";
    switch(de.type) {
    case FRM_TYPE_ZDPAGE:
      cty = "zp";
      break;
    case FRM_TYPE_DPAGE:
      cty = "dp";
      break;
    case FRM_TYPE_ZCPAGE:
      cty = "zc";
      break;
    case FRM_TYPE_CPAGE:
      cty = "cp";
      break;
    case FRM_TYPE_NODE:
      cty = "nd";
      break;
    case FRM_TYPE_ZNODE:
      cty = "zn";
      break;
    }
  
    MsgLog::dprintf(true, "Adding %s 0x%08x%08x lid=0x%08x\n",
		    cty,
		    (uint32_t) (de.oid >> 32),
		    (uint32_t) de.oid,
		    de.lid);
  }
  
  assert (de.type == FRM_TYPE_ZDPAGE ||
	  de.type == FRM_TYPE_ZCPAGE ||
	  de.type == FRM_TYPE_ZNODE ||
	  CONTENT_LID(de.lid));
  
  uint32_t nFrames = 0;
  
  if (de.lid != ZERO_LID) {
    bool needFrame = FrameIsEmpty(de.lid);
    
    if (needFrame) {
      coreGeneration[generation].ReserveFrame();
      nFrames = 1;
    }

    // Nodes cause multiple allocations per frame, which is CORRECT.
    coreGeneration[generation].AllocateLid(de.lid);

    // Allocating using AllocateLid won't grab a frame implicitly
    // unless the passed lid is undefined.  Manage the alloc.nFrames
    // count expliticly:
    if (needFrame)
      coreGeneration[generation].alloc.nFrames++;
  }

  coreGeneration[generation].AddToOidMap(cld);

  switch (de.type) {
  case FRM_TYPE_NODE:
    assert(de.lid != ZERO_LID);
    coreGeneration[generation].rsrv.nNode++;
    coreGeneration[generation].rsrv.nNodeFrame += nFrames;
    coreGeneration[generation].alloc.nNode++;
    coreGeneration[generation].alloc.nNodeFrame += nFrames;
    break;
  case FRM_TYPE_DPAGE:
    assert(de.lid != ZERO_LID);
    coreGeneration[generation].rsrv.nPage++;
    coreGeneration[generation].alloc.nPage++;
    break;
  case FRM_TYPE_CPAGE:
    assert(de.lid != ZERO_LID);
    coreGeneration[generation].rsrv.nCapPage++;
    coreGeneration[generation].alloc.nCapPage++;
    break;
  case FRM_TYPE_ZNODE:
  case FRM_TYPE_ZDPAGE:
  case FRM_TYPE_ZCPAGE:
    assert(de.lid == ZERO_LID);
    break;
  }
}

void
Checkpoint::ReloadSavedDirectory()
{
  // Validate all of the content locations to make sure they are
  // present:
  
  DEBUG(reload) MsgLog::dprintf(true, "Validating content pages...\n");

  if (lastCkptHdr->hasMigrated == true) {
    MsgLog::printf("   checkpoint has migrated\n");
    return;
  }
  
  uint32_t entry = lastCkptHdr->nRsrvPage + lastCkptHdr->nThreadPage;

  for (uint32_t i = 0; i < lastCkptHdr->nDirPage; i++, entry++) {
    lid_t dirLid = lastCkptHdr->dirPage[entry];

    coreGeneration[last_ckpt].ReserveFrame();
    coreGeneration[last_ckpt].AllocateLid(dirLid);
    coreGeneration[last_ckpt].rsrv.nDirFrame++;
    coreGeneration[last_ckpt].alloc.nDirFrame++;

    DEBUG(ckdir) MsgLog::printf("reading dirpg 0x%x\n", dirLid);
    ObjectHeader *pObj = Persist::KernGetCkFrame(dirLid);
    assert(pObj);

    pObj->TransLock();

    CkptDirPage *cdp = (CkptDirPage *) ObjectCache::ObHdrToPage(pObj);

    DEBUG(ckdir) MsgLog::printf("  nDirEnt = %d\n", cdp->nDirent);

    // Scan all the directories to make sure we have all of the
    // corresponding object locations in the checkpoint area attached:
    
    for (uint32_t obent = 0; obent < cdp->nDirent; obent++) {
      lid_t lid = cdp->entry[obent].lid;
      
      if (!Persist::FindDivision(dt_Log, lid))
	MsgLog::fatal("Checkpoint page 0x%08x [dirent %d] not mounted\n",
		      lid, obent); 

      AddCkDirent(cdp->entry[obent], last_ckpt);
    }

    pObj->TransUnlock();
  }

  CheckConsistency("bot Ckpt::ReloadSavedDir()", true);
}

void
Checkpoint::ReserveCoreDirPages()
{
  // Now that we have determined where everything is and what the free
  // map looks like, and it is safe to call ReserveLogPageFrame(),
  // preallocate the log frames for the thread list.  Also, allocate
  // any necessary page frames to ensure that we can rewrite the
  // entire thread directory:
  
  DEBUG(reload) MsgLog::dprintf(true, "Allocating locations for reserve and thread dirs\n");

  for (uint32_t t = 0; t < nThreadDirPage; t++) {
    if (threadDirHdr[t] == 0) {
      ObjectHeader *pObj = ObjectCache::GrabPageFrame();
      threadDirHdr[t] = pObj;
      ThreadDirPage *tdp = (ThreadDirPage *) ObjectCache::ObHdrToPage(pObj);
      threadDirPg[t] = tdp;
    }
    
    if (lastThreadDirLid[t] == ZERO_LID) {
      lastThreadDirLid[t] = FindFreeFrame();
      assert(lastThreadDirLid[t] != 0);
      AllocateMasterLid(lastThreadDirLid[t]);
    }

    if (nextThreadDirLid[t] == ZERO_LID) {
      nextThreadDirLid[t] = FindFreeFrame();
      assert(nextThreadDirLid[t] != 0);
      AllocateMasterLid(nextThreadDirLid[t]);
    }
  }
  
  for (uint32_t r = 0; r < nReserveDirPage; r++) {
    if (reserveDirHdr[r] == 0) {
      ObjectHeader *pObj = ObjectCache::GrabPageFrame();
      reserveDirHdr[r] = pObj;
      ReserveDirPage *rdp = (ReserveDirPage *) ObjectCache::ObHdrToPage(pObj);
      reserveDirPg[r] = rdp;
    }
    if (lastReserveDirLid[r] == ZERO_LID) {
      lastReserveDirLid[r] = FindFreeFrame();
      assert( lastReserveDirLid[r] );
      AllocateMasterLid(lastReserveDirLid[r]);
    }

    if (nextReserveDirLid[r] == ZERO_LID) {
      nextReserveDirLid[r] = FindFreeFrame();
      assert( nextReserveDirLid[r] );
      AllocateMasterLid(nextReserveDirLid[r]);
    }
#if 0
    MsgLog::dprintf(true, "ThreadDirPage[%d] = 0x%08x\n", t,
		    ObjectCache::ObHdrToPage(threadDirHdr[t]));
#endif
  }

  nextDirPageHdr = ObjectCache::GrabPageFrame();
  nextDirPageHdr->ob.oid = UNDEF_LID;
  assert(nextDirPageHdr);
  nextDirPage = (CkptDirPage *) ObjectCache::ObHdrToPage(nextDirPageHdr);

  CheckConsistency("bot Ckpt::RsrvCrDirPgs()", true);
}

void
Checkpoint::LoadDirectory()
{
  // If no ckpt range found, don't bother.
  if (totLogFrame == 0)
    return;

  CoreDirent::InitFreeList(totLogFrame);
  
  if ( !allocMap.IsFree((logframe_t) 0) || !allocMap.IsFree((logframe_t) 1) )
    MsgLog::fatal("Checkpoint header range not found\n");

  LoadDirectoryHeaders();
  
  // Last successful checkpoint record is now pointed to by
  // lastCkptHdr.  Mark all of the directory pages allocated,
  // including the reserve and thread directory pages.

  if (diskDirPageHdr == 0)
      diskDirPageHdr = ObjectCache::GrabPageFrame();

  ReloadSavedReserves();  
  ReloadSavedThreads();  
  ReloadSavedDirectory();  
  ReserveCoreDirPages();

  DEBUG(ckinit) MsgLog::dprintf(true, "Checkpoint fully reloaded\n");
  
  if (lastCkptHdr->hasMigrated == false)
    StartMigration(false);

#if 0
  MsgLog::printf("Tot: %d, Avail: %d curLogLoc %d chkLogLoc %d hdrLogLoc %d\n",
		 totLogFrame,
		 nAvailLogLoc,
		 nCurLogLoc,
		 nChkLogLoc,
		 nHdrLogLoc);
  
  MsgLog::printf("chkDirPg %d chkNdPot %d chkPg %d chkZpg %d chkNd %d chkDirent %d\n",
		 nChkDirPg,
		 nChkNodePot,
		 nChkPage,
		 nChkZeroPage,
		 nChkNode,
		 nChkDirent);
#endif

#if 0
  Debugger();
#endif
  
  enabled = true;

  CheckConsistency("bot Ckpt::LoadDir", true);
}

void
Checkpoint::AttachRange(lid_t lolid, lid_t hilid)
{
  MsgLog::printf("Attaching ckpt range [0x%x,0x%x)\n", lolid, hilid);
  
  logframe_t lo = lolid / EROS_OBJECTS_PER_FRAME;
  logframe_t hi = hilid / EROS_OBJECTS_PER_FRAME;
  
  for (logframe_t ll = lo; ll < hi; ll++)
    allocMap.MarkFree(ll);

  nAvailLogFrame += (hi - lo);
  totLogFrame += (hi - lo);

  // FIX: This is not right for very small checkpoint areas, because
  // it does not account properly for the thread/reserve directory
  // pages!
  ckGenLimit = (nAvailLogFrame * 6) / 10;
}

void
Checkpoint::StartThreads()
{
#ifdef DDB
  if ( Machine::IsDebugBoot() ) {
    MsgLog::printf("Stopping before waking up threads on restart\n");
    Debugger();
  }
#endif

  CheckpointedThreads.WakeAll();
}
