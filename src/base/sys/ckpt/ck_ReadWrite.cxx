/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/DiskNode.hxx>
#include <disk/PagePot.hxx>
#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Checkpoint.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/Depend.hxx>
#include <kerninc/BlockDev.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Machine.hxx>

#define dbg_ckage	0x1	/* migration state machine */
#define dbg_reservation	0x2	/* reservation logic */
#define dbg_dirent	0x4	/* dirent manipulation */
#define dbg_ndload      0x8

/* Following should be an OR of some of the above */
#define dbg_flags   ( 0u )

#define DBCOND(x) (dbg_##x & dbg_flags)
#define DEBUG(x) if DBCOND(x)
#define DEBUG2(x,y) if ((dbg_##x|dbg_##y) & dbg_flags)


void
Checkpoint::AllocateMasterLid(lid_t ll)
{
  logframe_t lf = (ll / EROS_OBJECTS_PER_FRAME);
  allocMap.Allocate(lf);
  nAvailLogFrame --;
  nMasterPage ++;
}

void
Checkpoint::WriteNodeToLog(Node *pNode)
{
#ifdef DBG_WILD_PTR
  Check::Consistency("Top WriteNodeToLog()");
#endif

  assert(pNode->obType <= ObType::NtLAST_NODE_TYPE);
  assert (pNode->IsDirty());

  uint32_t gen = pNode->GetFlags(OFLG_CKPT) ? last_ckpt : current;

  CoreGeneration *pGen = &coreGeneration[gen];

  DEBUG(ckage)
    MsgLog::dprintf(false, "Writing node 0x%08x%08x to log, generation %d\n",
		    (uint32_t) (pNode->ob.oid > 32),
		    (uint32_t) pNode->ob.oid,
		    gen);

#ifdef DBG_WILD_PTR
  if (pNode->Validate() == false) {
    MsgLog::printf("Node no good prior to hazard clearance\n");
    Debugger();
  }
#endif

  bool isZero = true;
  for (uint32_t k = 0; k < EROS_NODE_SIZE; k++) {
    if ( (*pNode)[k].IsRdHazard() )
      pNode->ClearHazard(k);
    isZero = isZero && pNode->slot[k].IsZeroKey();
  }
  
#ifndef NDEBUG
  if (pNode->Validate() == false) {
    MsgLog::printf("Node no good post hazard clearance\n");
    Debugger();
  }
#endif

  CoreDirent *cde = FindObject(pNode->ob.oid, ObType::NtUnprepared, gen);
  if (cde == 0 || cde == CkNIL)
    MsgLog::dprintf(true, "Cleaning nd 0x%08x%08x -- cde=0x%08x not in cur dir\n",
		    (uint32_t) (pNode->ob.oid >> 32),
		    (uint32_t) pNode->ob.oid,
		    cde);
		    
  assert(cde && cde != CkNIL);
  // No ZNODE's here, as they should have been converted in
  // RegisterDirtyObject() 
  assert (cde->type == FRM_TYPE_NODE);
  
  ObjectHeader * pLogPageHdr = 0;

  cde->count = pNode->ob.allocCount;

#ifdef DBG_WILD_PTR
  CheckConsistency(false);
#endif

  // It would be nice if we could assume that /cde->lid == UNDEF_LID/.
  // Regrettably, we can allocate a lid here in the call to
  // pGen->Allocate() and then yield trying to fetch in that frame.
  // When we subsequently try to rewrite the node, we will discover
  // the previously allocated lid.  Note that we do not try to
  // decommit the previously assigned location -- in all likelihood it
  // was assigned very recently, and we do not want the effort to
  // bring in the frame to be wasted.
  //
  // Just to make life interesting, however, it is possible that some
  // other operation has zeroed the node in the interim.  In that
  // case, we really do need to release the allocated storage.
  
  if (isZero) {
    pGen->Release(cde);
    cde->type = FRM_TYPE_ZNODE;
    cde->lid = ZERO_LID;
    DEBUG(ckage)
      MsgLog::dprintf(false, "    at lid=0x%08x\n", cde->lid);

#ifdef DBG_WILD_PTR
    CheckConsistency(false);
#endif
  }
  else {
    pGen->Allocate(cde);
    DEBUG(ckage)
      MsgLog::dprintf(false, "    at lid=0x%08x\n", cde->lid);
  
#ifdef DBG_WILD_PTR
    CheckConsistency(false);
#endif

    pLogPageHdr = Persist::GetCkFrame(cde->lid);
    assert(pLogPageHdr);
    pLogPageHdr->TransLock();

    assert( pLogPageHdr->GetFlags(OFLG_CKPT) == 0 );
#ifdef DBG_CLEAN
    MsgLog::printf("Object ty %d oid=0x%08x%08x marked dirty\n",
		   pLogPageHdr->obType,
		   (uint32_t) (pLogPageHdr->oid >> 32),
		   (uint32_t) pLogPageHdr->oid);
#endif
    pLogPageHdr->SetDirtyFlag();
    pLogPageHdr->age = Age::NewBorn;

#ifdef DBG_WILD_PTR
    CheckConsistency(false);
#endif

    DiskNode *potBase = (DiskNode *) ObjectCache::ObHdrToPage(pLogPageHdr);
    DiskNode *pDiskNode = potBase + (cde->lid % EROS_OBJECTS_PER_FRAME);

    DEBUG(ckage)
      MsgLog::printf("WrNdToLg: lid=0x%x potBase=0x%x "
		     "potHdr=0x%x pDiskNode=0x%x\n",
		     cde->lid, potBase, pLogPageHdr, pDiskNode);
    
    // Copy the node to the log pot:
    *pDiskNode = *pNode;

#ifdef DBG_WILD_PTR
    CheckConsistency(false);
#endif
  }
  
#ifdef OB_MOD_CHECK
  pNode->ob.check = pNode->CalcCheck();
#endif
    
  pNode->ClearFlags(OFLG_DIRTY|OFLG_REDIRTY);

#ifdef DBG_CLEAN
  MsgLog::printf("Object 0x%08x ty %d oid=0x%08x%08x cleaned to node pot\n",
		 pNode,
		 pNode->obType,
		 (uint32_t) (pNode->oid >> 32),
		 (uint32_t) pNode->oid);
#endif

#ifdef DBG_WILD_PTR
  CheckConsistency(false);
  
  if (dbg_wild_ptr)
    Check::Nodes();
#endif
  
#if defined(DBG_WILD_PTR)
  if (dbg_wild_ptr)
    Check::Consistency("Bottom WriteNodeToLog()");
#endif
}

lid_t
Checkpoint::FindFreeFrame()
{
  logframe_t stop = LastAllocatedFrame;
  logframe_t loc = LastAllocatedFrame + 1;

  for (;loc != stop; loc++) {
    if (loc == totLogFrame)
      loc = 0;

    if ( allocMap.IsFree(loc) ) {
      LastAllocatedFrame = loc;
      break;
    }
  }

  if (loc == stop) {
    MsgLog::fatal("loc: %d, lastAlloc: %d, totFrame: %d\n",
		  loc, LastAllocatedFrame, totLogFrame);
  }

#if 0
  MsgLog::dprintf(true, "Allocated log loc=%d\n", loc);
#endif

  // If the log page exists in core, blow it away:
  lid_t ll = loc * EROS_OBJECTS_PER_FRAME;
  
#ifndef NDEBUG
  ObjectHeader * pLogPageHdr =
    ObjectHeader::Lookup(ObType::PtLogPage, ll);
#endif

  assert (pLogPageHdr == 0);

#if 0
  if (pLogPageHdr) {
    // It should not be involved in I/O -- if so, then it was not really
    // free because migration was not done with it.
    assert (pLogPageHdr->GetFlags(OFLG_IO) == 0);
    
    ObjectCache::ReleasePageFrame(pLogPageHdr);
  }
#endif

  return ll;
}

lid_t
Checkpoint::AllocateLid(lid_t ll)
{
  logframe_t frm = ll / EROS_OBJECTS_PER_FRAME;
  
  assert (nReservedLogFrame >= nAllocatedLogFrame);

  assert (CONTENT_LID(ll));
  
  if (allocMap.IsFree(frm)) {
    nAllocatedLogFrame++;
    assert (nReservedLogFrame >= nAllocatedLogFrame);
  }
  allocMap.Allocate(frm);
  
  return ll;
}

// return true if log frame is now empty.
bool
Checkpoint::DeallocateLid(lid_t ll)
{
  logframe_t frm = ll / EROS_OBJECTS_PER_FRAME;

  assert (nReservedLogFrame >= nAllocatedLogFrame);
  assert (CONTENT_LID(ll));
  
  allocMap.Deallocate(frm);
  
  if (allocMap.IsFree(frm)) {
    ll &= ~0xffu;
    
    ObjectHeader * pLogPageHdr =
      ObjectHeader::Lookup(ObType::PtLogPage, ll);
    if (pLogPageHdr) {
      assert(pLogPageHdr->IsDirty() == false);
      pLogPageHdr->Unintern();
      ObjectCache::ReleasePageFrame(pLogPageHdr);
    }

    nAllocatedLogFrame--;
    nReservedLogFrame--;
    return true;
  }
  
  return false;
}

bool
Checkpoint::ReserveLogFrame()
{
  if (nReservedLogFrame == nAvailLogFrame) {
    for (uint32_t g = nGeneration - 1; g >= first_migrated; g--) {
      DEBUG(reservation)
	MsgLog::printf("Reclaiming log frame from generation %d\n", g);
      if (coreGeneration[g].ReclaimLogPage())
	break;
    }
  }
  
  if (nReservedLogFrame == nAvailLogFrame)
    return false;

  assert (nReservedLogFrame <= nAvailLogFrame);
  nReservedLogFrame++;
  return true;
}

// We are making an object dirty.  If it is already dirty and non-zero
// in the current checkpoint, do nothing and return.
//
// If space is not already reserved for this object, See if we can
// reserve space for the object in the log .  If so, reserve the
// space.
//
// If no directory entry exists, attempt to reserve a disk directory
// entry for the object.  If that fails, declare a checkpoint.
//
// If no directory entry exists, attempt to allocate and populate a
// core directory entry for the object.  If that fails, declare a
// checkpoint.

void
Checkpoint::RegisterDirtyObject(ObjectHeader *pObj)
{
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top RegisterDirtyObject()");
#endif

  if (enabled == false)
    return;

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Nodes();
#endif

  bool haveSpace = true;

  if (migrationStatus != mg_Idle)
    ProcessMigration();

  // RegisterDirtyObject() is called to tell the checkpoint logic that
  // the object should be added to the checkpoint directory.
  //
  // It is possible that the object we are trying to make dirty was in
  // core, and was just started on it's way to home location by the
  // migrator.  If so, we must wait for the I/O to complete:
  if (pObj->GetFlags(OFLG_IO)) {
#if 0
    if (pObj->GetFlags(OFLG_CKPT) == 0)
      MsgLog::dprintf(true, "Object hdr 0x%x OFLG_IO but not OFLG_CKPT!\n",
		      pObj);
#endif
    
    Thread::Current()->SleepOn(pObj->ObjectSleepQueue());
    Thread::Current()->Yield();
  }
      
  assert (!pObj->IsDirty());
  
  bool isNode = (pObj->obType <= ObType::NtLAST_NODE_TYPE);
  bool isDataPage = (pObj->obType == ObType::PtDataPage);
  
  CoreDirent* cde = coreGeneration[current].FindObject(pObj->ob.oid);

  DEBUG(reservation)
    MsgLog::dprintf(false, "Reserving space for %s 0x%08x%08x\n",
		    isNode ? "Node" : (isDataPage ? "dpage" : "cpage"),
		    (uint32_t) (pObj->ob.oid >> 32),
		    (uint32_t) pObj->ob.oid);

#ifdef DBG_WILD_PTR
  Checkpoint::CheckConsistency(false);
#endif

  bool neededDirent = false;
  
  // Allocate the directory entry first, as this is easier to
  // deallocate if we aren't able to get space for the actual object.
  if (cde == CkNIL) {
    neededDirent = true;

    // MUST grab the core dirent first, as allocating it might kick in
    // the ageing mechanism, during which the reservation counts for
    // core and disk dirents would disagree if we reserve the disk
    // dirent first.  This in turn violates the consistency checker.
    
    cde = new CoreDirent;
    if (cde == 0 || cde == CkNIL) // FIX: not sure
      goto take_checkpoint;
    
    haveSpace = coreGeneration[current].ReserveDirent();
    if (haveSpace == false) {
      MsgLog::dprintf(true, "Could not reserve disk dirent\n");
      goto release_core_dirent;
    }
    
    cde->oid = pObj->ob.oid;
    cde->color = CoreDirent::red;
    cde->count = pObj->ob.allocCount;
    // Note that we TEMPORARILY use VIRGIN_LID so that the Release()
    // logic will know not to release any existing storage.
    cde->lid = VIRGIN_LID;
    cde->type = isNode ? FRM_TYPE_NODE : (isDataPage ? FRM_TYPE_DPAGE
					  : FRM_TYPE_CPAGE);

    assert(cde->left == CkNIL);
    assert(cde->right == CkNIL);
    assert(cde->parent == CkNIL);

    coreGeneration[current].alloc.nCoreDirent++;
  }

  // Now reserve space for the actual object:
  if (isNode) {
    if (! coreGeneration[current].ReserveNode() ) {
      MsgLog::dprintf(true, "Could not reserve log space for node\n");
      goto release_dirent;
    }
    coreGeneration[current].Release(cde);
    cde->lid = UNDEF_LID;
    cde->type = FRM_TYPE_NODE;
  }
  else if (isDataPage) {
    if (! coreGeneration[current].ReserveDataPage() ) {
      MsgLog::dprintf(true, "Could not reserve log space for page\n");
      goto release_dirent;
    }
    coreGeneration[current].Release(cde);
    cde->lid = UNDEF_LID;
    cde->type = FRM_TYPE_DPAGE;
  }
  else {
    if (! coreGeneration[current].ReserveCapPage() ) {
      MsgLog::dprintf(true, "Could not reserve log space for CapPage\n");
      goto release_dirent;
    }
    coreGeneration[current].Release(cde);
    cde->lid = UNDEF_LID;
    cde->type = FRM_TYPE_CPAGE;
  }
  
  // This must be deferred until we are past the possibility of a
  // checkpoint.
  if (neededDirent)
    coreGeneration[current].AddToOidMap(cde);

#ifdef DBG_WILD_PTR
  Checkpoint::CheckConsistency(false);
  if (dbg_wild_ptr) {
    Check::Consistency("Bottom RegisterDirtyObject()");
  
    Check::Nodes();
  }
#endif

  pObj->SetDirtyFlag();
  pObj->ClearFlags(OFLG_CKPT);
  
  return;

release_dirent:
  if (!neededDirent)
    goto take_checkpoint;
  
  DEBUG(dirent)
    MsgLog::dprintf(false, "Releasing disk dirent.\n");
  coreGeneration[current].UnreserveDirent();
  
  coreGeneration[current].alloc.nCoreDirent--;

release_core_dirent:
  assert (neededDirent && cde != 0 && cde != CkNIL);
  DEBUG(dirent)
    MsgLog::dprintf(false, "Releasing core dirent.\n");
  delete cde;

take_checkpoint:
#ifdef DBG_WILD_PTR
  Checkpoint::CheckConsistency(false);
#endif
  
  DEBUG(dirent)
    MsgLog::dprintf(false, "Taking checkpoint.\n");
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("RegisterDirtyObject() before TakeCheckpoint()");
#endif
  TakeCheckpoint();
}

lid_t
Checkpoint::GetLidForPage(ObjectHeader *pObj)
{
  assert(pObj->obType == ObType::PtDataPage ||
	 pObj->obType == ObType::PtCapPage);

#ifdef DBG_WILD_PTR
  Checkpoint::CheckConsistency(false);
#endif

  uint32_t gen = pObj->GetFlags(OFLG_CKPT) ? last_ckpt : current;

  DEBUG(reservation)
    MsgLog::dprintf(false, "Assigning log loc for 0x%08x%08x, generation %d\n",
		    (uint32_t) (pObj->ob.oid >> 32),
		    (uint32_t) pObj->ob.oid,
		    gen);
  
  CoreGeneration *pGen = &coreGeneration[gen];
  CoreDirent *cde = FindObject(pObj->ob.oid, (ObType::Type) pObj->obType, gen);

  assert (cde);			// type should be valid
  
#ifndef NDEBUG
  if (cde == CkNIL) {
    MsgLog::fatal("GetLDEforPage(): Looking for pg 0x%08x%08x gen=%d, not found\n",
		  (uint32_t) (pObj->ob.oid >> 32),
		  (uint32_t) pObj->ob.oid,
		  gen);
  }
#endif
  
  assert(cde != CkNIL);
  assert ( (EROS_PAGE_SIZE % sizeof(uint32_t)) == 0 );

  // GetLogDirentForPage is only called when writing a dirty page to
  // the checkpoint area.  If page is dirty, it will not have a
  // directory entry indicating a zero page.
  assert (cde->type != FRM_TYPE_ZDPAGE);
  assert (cde->type != FRM_TYPE_ZCPAGE);

  bool isZero = true;
  bool isDataPage = (pObj->obType == ObType::PtDataPage);

  if (isDataPage) {
    uint32_t *pData = (uint32_t *) ObjectCache::ObHdrToPage(pObj);

    for (uint32_t i = 0; i < (EROS_PAGE_SIZE/sizeof(uint32_t)); i++) {
      if (pData[i]) {
	isZero = false;
	break;
      }
    }
  }
  else {
    Key *kData = (Key *) ObjectCache::ObHdrToPage(pObj);
    for (uint32_t i = 0; i < (EROS_PAGE_SIZE/sizeof(Key)); i++) {
      if (kData[i].IsZeroKey() == false) {
	isZero = false;
	break;
      }
    }
  }
  
  // It would be nice if we could assume that /cde->lid == UNDEF_LID/.
  // Regrettably, we can allocate a lid here in the call to
  // pGen->Allocate() and then yield trying to obtain an IoRequest
  // structure with which to actually write the frame.  When we
  // subsequently try to rewrite the page, we will discover the
  // previously allocated lid.  Note that we do not try to decommit
  // the previously assigned location -- in all likelihood it was
  // assigned very recently, and we do not want the effort to bring in
  // the frame to be wasted.
  //
  // Just to make life interesting, however, it is possible that some
  // other operation has zeroed the page in the interim.  In that
  // case, we need to release the allocated storage.
  
  cde->count = pObj->ob.allocCount;

  if (isZero) {
    pGen->Release(cde);
    cde->lid = ZERO_LID;
    cde->type = isDataPage ? FRM_TYPE_ZDPAGE : FRM_TYPE_ZCPAGE;
  }
  else {
    pGen->Allocate(cde);
    assert ( cde->type == FRM_TYPE_DPAGE ||
	     cde->type == FRM_TYPE_CPAGE);
  }

#ifdef DBG_WILD_PTR
  Checkpoint::CheckConsistency(false);
#endif
  return cde->lid;
}

Node *
Checkpoint::LoadCurrentNode(CoreDirent *cde)
{
  assert (cde);
  assert (cde != CkNIL);
  assert (cde->type == FRM_TYPE_NODE || cde->type == FRM_TYPE_ZNODE);
  
  Node *pNode;

  if (cde->type == FRM_TYPE_ZNODE) {
    assert (cde->lid == ZERO_LID);
    DEBUG(ndload)
      MsgLog::printf("Loading zero node 0x%08x%08x\n",
		     (uint32_t) (cde->oid>>32),
		     (uint32_t) (cde->oid));
    pNode = ObjectCache::GrabNodeFrame();
    assert (pNode);
    assert(ObjectCache::ValidNodePtr(pNode));
    assert (pNode->kr.IsEmpty());
    assert (pNode->obType == ObType::NtUnprepared);

    pNode->ob.allocCount = cde->count;
    pNode->callCount = cde->count;
    pNode->ob.oid = cde->oid;
    for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
      assert (pNode->slot[i].IsUnprepared());
      /* not hazarded because newly loaded node */
      pNode->slot[i].NH_ZeroKey();
    }
  }
  else {
    assert (cde->type == FRM_TYPE_NODE);
    assert (cde->lid != ZERO_LID);
  
    DEBUG(ndload)
      MsgLog::printf("Loading non-zero node 0x%08x%08x lid=0x%08x\n",
		     (uint32_t) (cde->oid>>32),
		     (uint32_t) (cde->oid),
		     cde->lid);

    ObjectHeader *pObj = Persist::GetCkFrame(cde->lid);
    assert(pObj);

    if (pObj->IsDirty())
      Persist::WritePage(pObj, true);

    pObj->TransLock();

    DiskNode *which = (DiskNode*) ObjectCache::ObHdrToPage(pObj);
    which += (cde->lid % EROS_OBJECTS_PER_FRAME);
    
    pNode = ObjectCache::GrabNodeFrame();
    assert(pNode);
    assert(ObjectCache::ValidNodePtr(pNode));
    assert (pNode->kr.IsEmpty());
    assert (pNode->obType == ObType::NtUnprepared);

    (*pNode) = *which;

    pObj->TransUnlock();
  }

#ifdef OB_MOD_CHECK
  pNode->ob.check = pNode->CalcCheck();
#endif

  pNode->SetFlags(OFLG_CURRENT|OFLG_DISKCAPS);
  assert (pNode->GetFlags(OFLG_CKPT|OFLG_DIRTY|OFLG_REDIRTY|OFLG_IO) == 0);

#if 0
  MsgLog::printf("Interning node (hdr=0x%08x)...\n", pNode);
#endif
  pNode->ResetKeyRing();
  pNode->Intern();
  
  assert (pNode->Validate());
  
  return pNode;
}

ObjectHeader *
Checkpoint::LoadCurrentPage(CoreDirent * cde)
{
  assert (cde != CkNIL);
  assert (cde->type == FRM_TYPE_DPAGE ||
	  cde->type == FRM_TYPE_ZDPAGE ||
	  cde->type == FRM_TYPE_CPAGE ||
	  cde->type == FRM_TYPE_ZCPAGE);
  
  ObjectHeader *pPage;

  bool isDataPage = (cde->type == FRM_TYPE_DPAGE ||
		     cde->type == FRM_TYPE_ZDPAGE);
  
#if 1
  if (cde->oid == 0x2f000)
    MsgLog::dprintf(true, "Loading pg 0x2f000 from ckpt\n");
#endif
  
  if (cde->type == FRM_TYPE_ZDPAGE) {
    pPage = ObjectCache::GrabPageFrame();
    uint8_t *pbuf = (uint8_t *) ObjectCache::ObHdrToPage(pPage);
    bzero( (void *) pbuf, EROS_PAGE_SIZE );
  }
  else if (cde->type == FRM_TYPE_ZCPAGE) {
    pPage = ObjectCache::GrabPageFrame();
    DiskKey* kbuf = (DiskKey *) ObjectCache::ObHdrToPage(pPage);
    // Following because placement vector new simply isn't working.
    for (unsigned i = 0; i < EROS_PAGE_SIZE/sizeof(DiskKey); i++)
      kbuf[i] = DiskKey();
  }
  else {
    pPage = Persist::GetCkFrame(cde->lid);
    if (pPage->IsDirty())
      Persist::WritePage(pPage, true);
    
    pPage->Unintern();
  }
  
#if 0
  MsgLog::printf("In-core log hdr 0x%08x\n", pPage);
#endif
  
  pPage->age = Age::NewBorn;
  pPage->ob.oid = cde->oid;
  pPage->ob.allocCount = cde->count;
  pPage->SetFlags(OFLG_CURRENT|OFLG_DISKCAPS);
  assert (pPage->GetFlags(OFLG_CKPT|OFLG_DIRTY|OFLG_REDIRTY|OFLG_IO) == 0);
  pPage->ob.ioCount = 0;
  pPage->obType = isDataPage ? ObType::PtDataPage : ObType::PtCapPage;
#ifdef OB_MOD_CHECK
  pPage->ob.check = pPage->CalcCheck();
#endif
  pPage->ResetKeyRing();
  pPage->Intern();

  return pPage;
}

// Note on the FindXxx() family.  We are looking for an object of
// given type in each generation.  If we don't find it, but we find
// some other object *in the same frame* whose type is not the same as
// the object type we are hunting for, then the frame has been
// retagged and the object we are after does not exist.
CoreDirent*
Checkpoint::FindObject(OID oid, ObType::Type oty, uint8_t generation)
{
  // If no ckpt range found, don't bother.
  if (totLogFrame == 0)
    return 0;

  for (uint8_t g = generation; g < nGeneration; g++) {
    CoreDirent* cde = coreGeneration[g].FindObject(oid);

    if (cde != CkNIL) {
      switch((unsigned)oty) {	// suppress incomplete switch warnings...
      case ObType::PtDataPage:
	if (cde->type == FRM_TYPE_DPAGE || cde->type == FRM_TYPE_ZDPAGE) 
	  return cde;
	break;
      case ObType::PtCapPage:
	if (cde->type == FRM_TYPE_CPAGE || cde->type == FRM_TYPE_ZCPAGE) 
	  return cde;
	break;
      case ObType::NtUnprepared:
	if (cde->type == FRM_TYPE_NODE || cde->type == FRM_TYPE_ZNODE)
	  return cde;
	break;
      }

      return 0;
    }
    else {
      cde = coreGeneration[g].FindFrame(oid);
      if (cde == CkNIL)
	continue;
      
      switch((unsigned)oty) {	// suppress incomplete switch warnings
      case ObType::PtDataPage:
	if (cde->type != FRM_TYPE_DPAGE && cde->type != FRM_TYPE_ZDPAGE) 
	  return 0;
	break;
      case ObType::PtCapPage:
	if (cde->type != FRM_TYPE_CPAGE && cde->type != FRM_TYPE_ZCPAGE) 
	  return 0;
	break;
      case ObType::NtUnprepared:
	if (cde->type != FRM_TYPE_NODE && cde->type != FRM_TYPE_ZNODE)
	  return 0;
	break;
      }
    }
  }

  return CkNIL;
}

#ifdef DDB
void
Checkpoint::ddb_showhdrloc()
{
  extern void db_printf(const char *fmt, ...);

  db_printf("Checkpoint headers at %d and %d\n", 0, 1);

  db_printf("Next thread locs at");
  for (int i = 0; i < nThreadDirPage; i++)
    db_printf(" 0x%08x", (uint32_t) nextThreadDirLid[i]);
  db_printf("\n");

  db_printf("Last thread locs at");
  for (int i = 0; i < nThreadDirPage; i++)
    db_printf(" 0x%08x", (uint32_t) lastThreadDirLid[i]);
  db_printf("\n");

  db_printf("Next reserce locs at");
  for (int i = 0; i < nReserveDirPage; i++)
    db_printf(" 0x%08x", (uint32_t) nextReserveDirLid[i]);
  db_printf("\n");

  db_printf("Last reserce locs at");
  for (int i = 0; i < nReserveDirPage; i++)
    db_printf(" 0x%08x", (uint32_t) lastReserveDirLid[i]);
  db_printf("\n");

  db_printf("Next ckpt dir pgs at");
  for (uint32_t i = 0; i < nextCkptHdr->nDirPage; i++)
    db_printf(" 0x%08x", (uint32_t) nextCkptHdr->dirPage[i]);
  db_printf("\n");

  db_printf("Last ckpt dir pgs at");
  for (uint32_t i = 0; i < lastCkptHdr->nDirPage; i++)
    db_printf(" 0x%08x", (uint32_t) lastCkptHdr->dirPage[i]);
  db_printf("\n");
}

void
Checkpoint::ddb_showalloc()
{
  extern void db_printf(const char *fmt, ...);

  db_printf("HDR: TotLog: %d Avail: %d Reserved: %d Allocated: %d\n",
	    totLogFrame, nAvailLogFrame, nReservedLogFrame,
	    nAllocatedLogFrame, allocMap.NumAllocated());
  db_printf("HDR: masterLoc: %d TrueAlloc: %d Migrator: %s nChk %d\n",
	    nMasterPage, allocMap.NumAllocated(),
	    MigStateName(), nCheckpointsCompleted);

  CoreDirent::ddb_showalloc();

  for (uint32_t gen = 0; gen < nGeneration; gen++)
    coreGeneration[gen].ddb_showalloc();
}

void
Checkpoint::ddb_dump_pgtree(uint32_t g)
{
  extern void db_printf(const char *fmt, ...);

  if (g >= nGeneration)
    db_printf("Only %d generations\n", nGeneration);
  else
    coreGeneration[g].oidTree->DumpTree(0);
}

void
Checkpoint::ddb_dump_ndtree(uint32_t g)
{
  extern void db_printf(const char *fmt, ...);

  if (g >= nGeneration)
    db_printf("Only %d generations\n", nGeneration);
  else
    coreGeneration[g].oidTree->DumpTree(0);
}

void
Checkpoint::ddb_dumpdir(uint32_t generation)
{
  uint32_t count;
  
  extern void db_printf(const char *fmt, ...);

  if ( coreGeneration[generation].oidTree == CkNIL ) {
    db_printf("ckpt directory is empty\n");
    return;
  }
  
  count = 0;
  
  if (coreGeneration[generation].oidTree != CkNIL) {
    CoreDirent *cde = coreGeneration[generation].oidTree->Minimum();

    while (cde != CkNIL) {
      const char *cty = "??";
      switch(cde->type) {
      case FRM_TYPE_ZDPAGE:
	cty = "zd";
	break;
      case FRM_TYPE_DPAGE:
	cty = "dp";
	break;
      case FRM_TYPE_ZCPAGE:
	cty = "zc";
	break;
      case FRM_TYPE_CPAGE:
	cty = "cp";
	break;
      case FRM_TYPE_NODE:
	cty = "nd";
	break;
      case FRM_TYPE_ZNODE:
	cty = "zn";
	break;
      }

      db_printf("[%3d] %s 0x%08x%08x ac=0x%08x lid=0x%08x gen: %d\n",
		count, cty,
		(uint32_t) (cde->oid>>32), (uint32_t) cde->oid,
		cde->count, cde->lid,
		generation);
      count++;
      cde = cde->Successor();
    }
  }
}
#endif

