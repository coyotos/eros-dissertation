/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Console.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/SysTimer.hxx>
#include <kerninc/Task.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/Machine.hxx>
#include <eros/TimeOfDay.h>
#include <kerninc/util.hxx>
#include <kerninc/WallClock.hxx>

static bool Probe(AutoConf*);
static bool Attach(AutoConf*);

struct Driver ac_todclock = {
  "todclock", Probe, Attach
} ;

// WallClock does not handle any interrupts, but will eventually have
// an associated user key:

WallClock TheWallClock;

inline
uint32_t BcdToBin(uint32_t val)
{
  return ((val)=((val)&15) + ((val)>>4)*10);
}


#if 1
// The wall clock is always present.
static bool
Probe(AutoConf* /* ac */)
{
  return true;
}

static Timer wallClockTimer;
static void run_wallclock(Timer *);

// The wall clock is always present.
static bool
Attach(AutoConf* ac)
{
  TheWallClock.Start();
  
  MsgLog::printf("%s (task)\n", ac->name);
  return true;
}

static void
run_wallclock(Timer * /* pt */)
{
  TheWallClock.Start();
}

WallClock::WallClock()
{
}

void
WallClock::Start()
{
  static bool initialized = false;
  static uint64_t secondsSinceStart = 0;
  static uint64_t wakeTime = 0;		// ms since start

  static uint32_t second = 0;
  static uint32_t start_second = 0;
  static uint32_t minute = 0;
  static uint32_t start_minute = 0;
  static uint32_t hour = 0;
  static uint32_t start_hour = 0;

  TimeOfDay tod;
  
  if (initialized == false) {
    initialized = true;
    wakeTime = 0;

    Machine::GetHardwareTimeOfDay(tod);
  
    start_second = second = tod.sec;
    start_minute = minute = tod.min;
    start_hour = hour = tod.hr;

    wallClockTimer.WakeupAt(wakeTime, run_wallclock);
    return;
  }
  else {
    wakeTime += 1000;
    
    secondsSinceStart++;
    
    // screw that - it doesn't work anyway.
    secondsSinceStart = wakeTime / 1000;
    
    if (++second == 60) {
      second = 0;
      if (++minute == 60) {
	minute = 0;
	if (++hour == 24) {
	  hour = 0;
	}
      }
    }
  }

  IRQ::DISABLE();

  Machine::GetHardwareTimeOfDay(tod);
  uint32_t cmosSecond = tod.sec;
  uint32_t cmosMinute = tod.min;
  uint32_t cmosHour = tod.hr;

  uint32_t upHr = secondsSinceStart / 3600;
  uint32_t upMin = (secondsSinceStart % 3600) / 60;
  uint32_t upSec = (secondsSinceStart % 60);
      
  IRQ::ENABLE();

  char *msg = "EROS=xx:xx:xx  CMOS=xx:xx:xx  START=xx:xx:xx  UP=xxx:xx:xx";
  msg[5] = hour/10 + '0';
  msg[6] = hour % 10 + '0';
  msg[8] = minute/10 + '0';
  msg[9] = minute % 10 + '0';
  msg[11] = second/10 + '0';
  msg[12] = second % 10 + '0';

  msg[20] = cmosHour/10 + '0';
  msg[21] = cmosHour % 10 + '0';
  msg[23] = cmosMinute/10 + '0';
  msg[24] = cmosMinute % 10 + '0';
  msg[26] = cmosSecond/10 + '0';
  msg[27] = cmosSecond % 10 + '0';

  msg[36] = start_hour / 10 + '0';
  msg[37] = start_hour % 10 + '0';
  msg[39] = start_minute / 10 + '0';
  msg[40] = start_minute % 10 + '0';
  msg[42] = start_second / 10 + '0';
  msg[43] = start_second % 10 + '0';
  
  msg[49] = upHr / 100 + '0';
  msg[50] = ((upHr % 100) / 10) + '0';
  msg[51] = (upHr % 10) + '0';
  msg[53] = upMin/10 + '0';
  msg[54] = upMin%10 + '0';
  msg[56] = upSec/10 + '0';
  msg[57] = upSec%10 + '0';
    
  Console::ShowMessage(msg);

  wallClockTimer.WakeupAt(wakeTime, run_wallclock);
}


#else
const uint32_t StackSize = 1024;
static uint32_t WallClockStack[StackSize];

WallClock::WallClock()
  : KernThread("WallClock", Prio::High,
	       (void*) WallClock::Start,
	       WallClockStack, &WallClockStack[StackSize])
{
  procContext.InitStack();
}

// The wall clock is always present.
static void
Probe(AutoConf* ac)
{
  ac->present = true;
}

// The wall clock is always present.
static void
Attach(AutoConf* ac)
{
  TheWallClock.Wakeup();	// let it initialize itself...
  
  MsgLog::printf("%s (no vector)\n", ac->name);
}

// There is a minor piece of trickery here.  We don't want to use
// relative sleeps, because we would slowly lose clock accuracy.
// Instead, snapshot the current tick at startup and explicitly
// request a particular millisecond for startup.
void WallClock::Start()
{
  int stack;
  MsgLog::printf("Start WallClock (thread 0x%x,context 0x%x,stack 0x%x)\n",
	       this, context, &stack);

  TimeOfDay tod;
  
  Machine::GetHardwareTimeOfDay(tod);
  
  static uint64_t secondsSinceStart = 0;

  second = tod.sec;
  minute = tod.min;
  hour = tod.hr;

  uint64_t startTick = SysTimer::Now();

  uint64_t ms = 1000ll;

  for(;;) {
    uint64_t wakeTick = startTick + Machine::MillisecondsToTicks(ms);
    WakeUpAtTick(wakeTick);
    SleepOn(IdlePile,Thread::Stall);
    DirectedYield();

    secondsSinceStart++;
    
    if (++second == 60) {
      second = 0;
      if (++minute == 60) {
	minute = 0;
	if (++hour == 24) {
	  hour = 0;
	}
      }
    }

    ms += 1000ll;

    int cpl = IRQ::splhigh();

    Machine::GetHardwareTimeOfDay(tod);
    uint32_t cmosSecond = tod.sec;
    uint32_t cmosMinute = tod.min;
    uint32_t cmosHour = tod.hr;

    uint32_t upHr = secondsSinceStart / 3600;
    uint32_t upMin = (secondsSinceStart % 3600) / 60;
    uint32_t upSec = (secondsSinceStart % 60);
      
    IRQ::splx(cpl);

    char *msg = "EROS=xx:xx:xx  CMOS=xx:xx:xx  UP=xxx:xx:xx";
    msg[5] = hour/10 + '0';
    msg[6] = hour % 10 + '0';
    msg[8] = minute/10 + '0';
    msg[9] = minute % 10 + '0';
    msg[11] = second/10 + '0';
    msg[12] = second % 10 + '0';

    msg[20] = cmosHour/10 + '0';
    msg[21] = cmosHour % 10 + '0';
    msg[23] = cmosMinute/10 + '0';
    msg[24] = cmosMinute % 10 + '0';
    msg[26] = cmosSecond/10 + '0';
    msg[27] = cmosSecond % 10 + '0';

    msg[33] = upHr / 100 + '0';
    msg[34] = ((upHr % 100) / 10) + '0';
    msg[35] = (upHr % 10) + '0';
    msg[37] = upMin/10 + '0';
    msg[38] = upMin%10 + '0';
    msg[40] = upSec/10 + '0';
    msg[41] = upSec%10 + '0';
    
    Console::ShowMessage(msg);
  }
}
#endif
