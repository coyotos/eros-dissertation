/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
     
#include <kerninc/kernel.hxx>
#include <disk/DiskKey.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/util.hxx>
#include <kerninc/Depend.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/StackTester.hxx>
#include <eros/SysTraceKey.h>

#define dbg_prepare	0x1	/* steps in taking snapshot */

/* Following should be an OR of some of the above */
#define dbg_flags   (0)

#define DBCOND(x) (dbg_##x & dbg_flags)
#define DEBUG(x) if DBCOND(x)
#define DEBUG2(x,y) if ((dbg_##x|dbg_##y) & dbg_flags)

ObjectHeader *
Key::GetObjectPtr() const
{
  assert( IsPreparedObjectKey() );
  if (IsGateKey())
    return gk.pContext->procRoot;
  else
    return ok.pObj;
}

OID
Key::GetKeyOid() const
{
  assert (IsObjectKey());
  
  if ( IsPreparedObjectKey() )
    return GetObjectPtr()->ob.oid;
  return unprep.oid;
}

uint32_t
Key::GetAllocCount() const
{
  assert (GetType() > LAST_GATE_KEYTYPE);
  assert (GetType() <= LAST_OBJECT_KEYTYPE);
  
  if ( IsPreparedObjectKey() )
    return GetObjectPtr()->ob.allocCount;
  return unprep.count;
}

Key Key::ZeroNumberKey;	// default constructor

// NOTE: if we are running OB_MOD_CHECK, the key prepare logic does an
// incremental recomputation on the check field in the containing object.
void
Key::DoPrepare()
{
  assert( IsUnprepared() );
  
  assert ( NeedsPrepare() );

  KernStats.nKeyPrep++;
  
#ifdef TEST_STACK
  StackTester st;
#endif
 
#if 0
  if (ktByte > UNPREPARED(LAST_PREPARED_KEYTYPE))
    return;
#endif

#ifdef MEM_OB_CHECK
  uint32_t ck = CalcCheck();
#endif
    
  ObjectHeader *pObj = 0;
  
  switch(GetType()) {		// not prepared, so not hazarded!
  case KtResume:
  case KtStart:
    {
      // Gate keys are linked onto the context structure.  First, we
      // need to validate the key:

      Process * context = 0;
      Node *pNode =
	Persist::GetNode(unprep.oid, unprep.count,
			 IsType(KtResume) ? false : true);

      if (IsType(KtResume) && pNode->callCount != unprep.count)
	pNode = 0;

      if (pNode == 0) {
	DEBUG(prepare)
	  MsgLog::printf("Zeroing invalid gate key\n");
	assert ( IsHazard() == false );
	assert ( IsUnprepared() );
	/* key was not prepared, so cannot be hazarded */
	KS_ZeroKey();
	return;
      }
	
      assertex(pNode, ObjectCache::ValidNodePtr(pNode));
      
      pNode->TransLock();
      
      context = pNode->GetDomainContext();
      if (! context )		// malformed domain
	MsgLog::fatal("Preparing gate key to malformed domain\n");

      // Okay, we prepared successfully.
      gk.pContext = context;

      // Link into context chain on left or right according to key
      // type.
      if ( IsType(KtResume) ) {
	gk.prev = context->kr.prev;
	gk.prev->next = (KeyRing *) this;
	context->kr.prev = (KeyRing *) this;
	gk.next = (KeyRing*) &context->kr;
      }
      else {
	gk.next = context->kr.next;
	gk.next->prev = (KeyRing *) this;
	context->kr.next = (KeyRing *) this;
	gk.prev = (KeyRing*) &context->kr;
      }

      SetPrepared();

#if 0
      MsgLog::printf("Prepared key ");
      Print();
#endif
  
#ifdef MEM_OB_CHECK
      assert(ck == CalcCheck());
#endif
    
#ifdef DBG_WILD_PTR
      if (dbg_wild_ptr)
	Check::Consistency("In Key::DoPrepare()");
#endif
#ifdef TEST_STACK
      st.check();
#endif
      return;
    }

  case KtDataPage:
  case KtCapPage:
    {
      ObType::Type oty =
	(GetType() == KtDataPage) ? ObType::PtDataPage : ObType::PtCapPage;
      pObj = Persist::GetPage(unprep.oid, unprep.count, oty, true);
      if (pObj)
	assertex(this, ObjectCache::ValidPagePtr(pObj));
      break;
    }
    
  case KtNode:
  case KtSegment:
  case KtProcess:
    pObj = Persist::GetNode(unprep.oid, unprep.count, true);
    break;

#if 0
  case KtDevice:
    // Do nothing for now!
    SetPrepared();
    return;
  case KtMisc:
    assert (nk.value[0] == 0);
    assert (nk.value[1] == 0);
    assert (nk.value[2] == 0);
    SetPrepared();
    return;
  case KtNumber:
    // Once prepared, a special-case zero number key should be
    // properly layed out.
    if (subType == 1) {
      nk.value[0] = 0;
      nk.value[1] = 0;
      nk.value[2] = 0;
      subType = 0;
    }
  default:
    SetPrepared();
    return;
#else
  default:
    assertex(this,false);
#endif
  }
  
  if (pObj == 0) {
    DEBUG(prepare)
      MsgLog::dprintf(true, "Zeroing invalid key\n");

    assert ( IsHazard() == false );
    assert ( IsUnprepared() );
    NH_ZeroKey();
#ifdef TEST_STACK
    st.check();
#endif
    return;
  }

  // It's definitely an object key.  Pin the object it names.
  pObj->TransLock();
  
  // Link as next key after object
  ok.pObj = pObj;
  
  ok.next = pObj->kr.next;
  ok.prev = (KeyRing *) pObj;
  
  pObj->kr.next = (KeyRing*) this;
  ok.next->prev = (KeyRing*) this;

#ifdef MEM_OB_CHECK
  assert(ck == CalcCheck());
#endif
    
  SetPrepared();
#if 0
  MsgLog::printf("Prepared key ");
  Print();
#endif
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("End Key::DoPrepare()");
#endif
#ifdef TEST_STACK
  st.check();
#endif
  return;
}

#ifndef NDEBUG
void
Key::Prepare()
{
  extern bool InvocationCommitted;
  assert (InvocationCommitted == false);

  if (NeedsPrepare())
    DoPrepare();
      
  if ( NeedsPin() )
    ok.pObj->TransLock();
}
#endif

bool
Key::Prepare(KeyType ty)
{
#ifndef NDEBUG
  extern bool InvocationCommitted;
#endif
  assert (InvocationCommitted == false);

  // MsgLog::printf("Key::Prepare(ty) -- want ty %d\n", ty);

  if (IsType(ty) == false) {
    Print();
    MsgLog::fatal("Key::Prepare(kt) -- wrong keytype 0x%x (wanted 0x%x)\n",
		  GetType(), ty);
    return false;
  }

  if (NeedsPrepare())
    Prepare();

  if (NeedsPin())
    ok.pObj->TransLock();
  
  if (IsType(KtNumber) && (ty != KtNumber)) {
    MsgLog::printf("Key::Prepare(ty) -- number key. Key rescinded?\n");
    return false;
  }

  return true;
}

#define CAP_REGMOVE
#ifdef CAP_REGMOVE
// save_key(from, to) -- given a key address /from/ that is within a
// capability register, copy that key to the key address /to/, which
// is a slot in a capability page.
//
// Note that all functions called by save_key() are prompt and do not
// yield.  This is important, as we are running on an interrupt stack.
extern "C" {
  void copy_key(Key *fromKeyReg, Key *toKeyReg);
  void xchg_key(Key *kr0, Key *kr1);
}

void
copy_key(Key *from, Key *to)
{
  if (to == &Thread::CurContext()->keyReg[0])
    return;
  
  // Do not bother to deprepare dest key, as it isn't going to disk as
  // a result of this operation.
  to->NH_Set(*from);
}

void
xchg_key(Key *cr0, Key *cr1)
{
  Key tmp;
  tmp.NH_Set(*cr0);

  if (cr0 != &Thread::CurContext()->keyReg[0]) {
    cr0->NH_Set(*cr1);
  }
  if (cr1 != &Thread::CurContext()->keyReg[0]) {
    cr1->NH_Set(tmp);
  }
  
  tmp.NH_Unchain();
}

#endif /* CAP_REGMOVE */

void
Key::NH_Unprepare()
{
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top Key::NH_Unprepare()");
#endif

  // MsgLog::fatal("Unprepare() called\n");
  assert(IsHazard() == false);

  if ( IsUnprepared() )
    return;

  if ( IsObjectKey() ) {
    ObjectHeader *pObj = ok.pObj;
  
    if ( IsGateKey() ) {
#ifndef NDEBUG
      extern bool ValidCtxtPtr(const Process *);
      if (ValidCtxtPtr(gk.pContext) == false)
	MsgLog::fatal("Key 0x%08x Kt %d, 0x%08x not valid ctxt ptr\n",
		      this, GetType(), gk.pContext);
#endif
      pObj = gk.pContext->procRoot;
    }

#ifndef NDEBUG
    if ( IsType(KtDataPage) || IsType(KtCapPage) ) {
      if ( ObjectCache::ValidPagePtr(pObj) == false )
	MsgLog::fatal("Key 0x%08x Kt %d, 0x%08x not valid page ptr\n",
		      this, GetType(), pObj);
    }
    else {
      if ( ObjectCache::ValidNodePtr((Node *)pObj) == false )
	MsgLog::fatal("Key 0x%08x Kt %d, 0x%08x not valid node ptr\n",
		      this, GetType(), pObj);
    }
#endif

    NH_Unchain();

    pObj->SetFlags(OFLG_DISKCAPS);
    unprep.oid = pObj->ob.oid;

    if ( IsType(KtResume) )
      unprep.count = ((Node *) pObj)->callCount;
    else
      unprep.count = pObj->ob.allocCount;
  }

#ifndef NDEBUG
  if ( IsType(KtMisc) ) {
    assert (nk.value[0] == 0);
    assert (nk.value[1] == 0);
    assert (nk.value[2] == 0);
  }
#endif

  SetUnprepared();

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("End Key::NH_Unprepare()");
#endif

#if 0
  MsgLog::printf("Deprepared key ");
  Print();
#endif
}

void
DiskKey::operator=(const Key& k)
{
  assert ( k.IsValid() );

  if ( k.IsPreparedObjectKey() ) {
    ObjectHeader *pObj = k.ok.pObj;
  
    if ( k.IsType(KtResume) || k.IsType(KtStart) ) {
#ifndef NDEBUG
      extern bool ValidCtxtPtr(const Process *);
      if (ValidCtxtPtr(k.gk.pContext) == false)
	MsgLog::fatal("Kt %d, 0x%08x not valid ctxt ptr\n",
		      k.GetType(),
		      k.gk.pContext);
#endif
      pObj = k.gk.pContext->procRoot;
    }

#ifndef NDEBUG
    if ( k.IsType(KtDataPage) || k.IsType(KtCapPage) ) {
      if ( ObjectCache::ValidPagePtr(pObj) == false )
	MsgLog::fatal("Kt %d, 0x%08x not valid page ptr\n", k.GetType(),
		      pObj);
    }
    else {
      if ( ObjectCache::ValidNodePtr((Node *)pObj) == false )
	MsgLog::fatal("Kt %d, 0x%08x not valid node ptr\n", k.GetType(),
		      pObj);
    }
#endif

    *((uint32_t *)this) = ( *((uint32_t *) &k) & (~KHAZARD_BITS) );
    SetUnprepared();
    
    if ( k.IsType(KtResume) )
      unprep.count = ((Node *) pObj)->callCount;
    else
      unprep.count = pObj->ob.allocCount;

    unprep.oid = pObj->ob.oid;

    pObj->SetFlags(OFLG_DISKCAPS);
  }
  else {
    // this is dirty!!!
    *((uint32_t *)this) = ( *((uint32_t *) &k) & (~KHAZARD_BITS) );
    SetUnprepared();
    nk.value[0] = k.nk.value[0];
    nk.value[1] = k.nk.value[1];
    nk.value[2] = k.nk.value[2];
  }
}

void
Key::Print() const
{
  uint32_t * pWKey = (uint32_t *) this;

  if ( IsPreparedObjectKey() ) {
    ObjectHeader * pObj = GetObjectPtr();
    
    uint32_t * pOID = (uint32_t *) &pObj->ob.oid;

    if (IsType(KtResume)) {
#if 0
      MsgLog::printf("rsm 0x%08x 0x%08x 0x%08x 0x%08x (obj=0x%08x)\n",
		     pWKey[0], ((Node *)pObj)->callCount,
		     pOID[0], pOID[1], pObj);
      
#else
      MsgLog::printf("0x%08x rsm 0x%08x 0x%08x 0x%08x 0x%08x\n",
		     this,
		     pWKey[0], ((Node *)pObj)->callCount,
		     pOID[0], pOID[1]);
#endif
    }
    else {
#if 0
      MsgLog::printf("pob 0x%08x 0x%08x 0x%08x 0x%08x (obj=0x%08x)\n",
		     pWKey[0], ok.pObj->allocCount,
		     pOID[0], pOID[1], pObj);
#else
      MsgLog::printf("0x%08x pob 0x%08x 0x%08x 0x%08x 0x%08x\n",
		     this,
		     pWKey[0], ((Node *)pObj)->ob.allocCount,
		     pOID[0], pOID[1]);
#endif
    }
  }
  else {
#if 0
    MsgLog::printf("ukt 0x%08x 0x%08x 0x%08x 0x%08x\n",
		   pWKey[0], pWKey[1], pWKey[2], pWKey[3]);
#else
    MsgLog::printf("0x%08x ukt 0x%08x 0x%08x 0x%08x 0x%08x\n",
		   this,
		   pWKey[0], pWKey[1], pWKey[2], pWKey[3]);
#endif
  }
}

#ifdef OB_MOD_CHECK
uint32_t
Key::CalcCheck()
{
  uint32_t ck = 0;

  assert(this);

  uint32_t *pWKey = (uint32_t *) this;

  // mask out prepared, hazard bits!
  ck ^= pWKey[0] & ~(KHAZARD_BITS|PREPARED_KT_BIT);

  if ( IsPreparedObjectKey() ) {
    if (IsType(KtResume)) {
      // Inject the checksum for a zero number key instead so that
      // zapping them won't change the checksum:

      ck = ZeroNumberKey.CalcCheck();
    }
    else if (IsType(KtStart)) {
      // This pointer hack is simply so that I don't have to remember
      // machine specific layout conventions for long long
    
      Node *pDomain = gk.pContext->procRoot;
      
      ck ^= pDomain->ob.allocCount;

      uint32_t * pOID = (uint32_t *) &pDomain->ob.oid;
      ck ^= pOID[0];
      ck ^= pOID[1];
    }
    else {
      // This pointer hack is simply so that I don't have to remember
      // machine specific layout conventions for long long
    
      ck ^= ok.pObj->ob.allocCount;

      uint32_t * pOID = (uint32_t *) &ok.pObj->ob.oid;
      ck ^= pOID[0];
      ck ^= pOID[1];
    }
  }
  else {
    ck ^= pWKey[1];
    ck ^= pWKey[2];
    ck ^= pWKey[3];
  }
  
  return ck;
}
#endif

// New Key -- use KS_ZeroKey, since in a couple of cases we do this on
// the stack and there is no telling what garbage bits are sitting there.
Key::Key()
{
  KS_ZeroInitKey();
}

#ifndef NDEBUG
bool
Key::IsValid() const
{
  if ( IsType(KtMisc) ) {
    if (nk.value[0] || nk.value[1] || nk.value[2])
      return false;
  }

#if defined(DBG_WILD_PTR)
  // Following is a debugging-only check.
  if (IsObjectKey() && GetKeyOid() > 0x100000000llu) {
    OID oid = GetKeyOid();
    
    MsgLog::printf("Key 0x%08x has invalid OID 0x%08x%08x\n",
		   this, (uint32_t) (oid>>32), (uint32_t) oid);
  }
#endif
      
  if ( IsType(KtMisc) ) {
    assert(nk.value[0] == 0);
    assert(nk.value[1] == 0);
    assert(nk.value[2] == 0);
  }

  if ( IsPreparedObjectKey() ) {
    extern bool ValidCtxtPtr(const Process *);
    extern bool ValidCtxtKeyRingPtr(const KeyRing *);
	
#ifndef NDEBUG
    if ( IsGateKey() ) {
      Process *ctxt = gk.pContext;
      if (ValidCtxtPtr(ctxt) == false)
	return false;
    }
    else if ( IsType(KtDataPage) || IsType(KtCapPage) ) {
      ObjectHeader *pObject = ok.pObj;
      if ( ObjectCache::ValidPagePtr(pObject) == false ) {
	Print();
	MsgLog::printf("Key 0x%08x has invalid pObject 0x%08x\n",
		       this, pObject);
	return false;
      }
      if ( pObject->IsFree() ) {
	Print();
	MsgLog::printf("Prepared key 0x%08x names free pObject 0x%08x\n",
		       this, pObject);
	return false;
      }
    }
    else {
      assertex (this, IsObjectKey() );
      Node *pNode = (Node *) ok.pObj;
      if ( ObjectCache::ValidNodePtr(pNode) == false ) {
	MsgLog::printf("0x%x is not a valid node ptr\n", pNode);
	return false;
      }
      if ( pNode->IsFree() ) {
	Print();
	MsgLog::printf("Prepared key 0x%08x names free pObject 0x%08x\n",
		       this, pNode);
	return false;
      }
    }
#endif

    if ( IsObjectKey() ) {
      // KeyRing pointers must either point to key slots or to
      // object root.
#ifndef NDEBUG
      KeyRing * krn = ok.next;
      KeyRing * krp = ok.prev;

      if ( ! ( ObjectCache::ValidKeyPtr((Key *) krn) ||
	       ObjectCache::ValidPagePtr((ObjectHeader*) krn) ||
	       ObjectCache::ValidNodePtr((Node *) krn) ||
	       ValidCtxtKeyRingPtr(krn) ) ) {
	MsgLog::printf("key 0x%x nxt key 0x%x bogus\n", this, krn);
	return false;
      }
      
      if ( ! ( ObjectCache::ValidKeyPtr((Key *) krp) ||
	       ObjectCache::ValidPagePtr((ObjectHeader *) krp) ||
	       ObjectCache::ValidNodePtr((Node *) krp) ||
	       ValidCtxtKeyRingPtr(krp) ) ) {
	MsgLog::printf("key 0x%x prv key 0x%x bogus\n", this, krp);
	return false;
      }

      // Prev and next pointers must be linked properly:
      if (krp->next != (KeyRing *) this) {
	MsgLog::printf("Prepared key 0x%08x bad keyring links to prev\n",
		       this);
	return false;
      }
      if (krn->prev != (KeyRing *) this) {
	MsgLog::printf("Prepared key 0x%08x bad keyring links to next\n",
		       this);
	return false;
      }
#endif
    }
  }
  return true;
}
#endif
