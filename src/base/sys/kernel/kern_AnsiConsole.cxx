/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// ANSI console support.  Provides a standard implementation for all
// platforms that implement some sort of memory-mapped console buffer
// (nearly every machine we know about).   All the machine specific
// code has to do is to implement
//
//    AnsiConsole::SetPosition(uint32_t pos, char c, Attribute a);
//    void ScrollConsole(int nLines);
//
// and have it's console::Put() routine pass the buck to
// AnsiConsole::put.
//
// Note that AnsiConsole is stateful.  This allows the machine
// dependent code to implement multiple virtual terminals if it
// desires to do so.

#include <kerninc/Console.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/util.hxx>

struct ASCII {
  enum  {
    NUL = 0x00,
    SOH = 0x01,
    STX = 0x02,
    ETX = 0x03,
    EOT = 0x04,
    ENQ = 0x05,
    ACK = 0x06,
    BEL = 0x07,
    BS  = 0x08,
    TAB = 0x09,
    LF  = 0x0a,
    VT  = 0x0b,
    FF  = 0x0c,
    CR  = 0x0d,
    SO  = 0x0e,
    SI  = 0x0f,
    DLE = 0x10,
    DC1 = 0x11,
    DC2 = 0x12,
    DC3 = 0x13,
    DC4 = 0x14,
    NAK = 0x15,
    SYN = 0x16,
    ETB = 0x17,
    CAN = 0x18,
    EM  = 0x19,
    SUB = 0x1a,
    ESC = 0x1b,
    FS  = 0x1c,
    GS  = 0x1d,
    RS  = 0x1e,
    US  = 0x1f,
    DEL = 0x7f,
  };
};

enum State {
  NotInit,
  WantChar,			// expecting a normal character
  SawEsc,			// saw ESC
  WantParam,			// Grabbing parameters
};

AnsiConsole::AnsiConsole(uint32_t _rows, uint32_t _cols)
{
  rows = _rows;
  cols = _cols;
  state = NotInit;
}

void
AnsiConsole::ClearScreen()
{
  for (uint32_t wpos = 0; wpos < rows * cols; wpos++)
    SetPosition(wpos, ' ', Attr::Normal);
}

void
AnsiConsole::Reset()
{
  state = WantChar;
  pos = 0;
  curAttr = Attr::Normal;
  scroll = true;
}

void
AnsiConsole::ProcessChar(uint8_t b)
{
  const int TABSTOP = 8;
  
  if (IsPrint(b)) {
    SetPosition(pos, b, curAttr);
    pos++;
  }
  else {
    // Handle the non-printing characters:
    switch(b) {
    case ASCII::BEL:
      Beep();
      break;
    case ASCII::BS:		// backspace is NONDESTRUCTIVE
      if ( pos % cols )
	pos--;
      break;
    case ASCII::TAB:		// NONDESTRUCTIVE until we know how
      while (pos % TABSTOP) {
	SetPosition(pos, ' ', curAttr);
	pos++;
      }
      break;
    case ASCII::LF:
      pos += cols;
      break;
    case ASCII::VT:		// reverse line feed
      if (pos > cols)
	pos -= cols;
      break;
#if 0
    case ASCII::FF:		// reverse line feed
      pos = 0;
      ClearScreen();
      break;
#endif
    case ASCII::CR:
      pos -= (pos % cols);
      break;
    }
  }
    
  if (pos >= rows * cols) {
    if (scroll) {
      Scroll(0, rows * cols, - (int) cols);
      pos -= cols;
    }
    else
      pos = (pos % cols);
  }

  assert (pos < rows * cols);
  ShowCursorAt(pos);
  return;
}

void
AnsiConsole::ProcessEsc(uint8_t b)
{
  uint32_t posRow = pos / cols;
  uint32_t posCol = pos % cols;

#if 0
  // I don't yet wish to support this.
  
  // The ESC [ N 'm' sequence is the only one that has a default
  // parameter value of zero.  Handle that one specially so we can do
  // common logic on the rest:

  if (b == 'm') {
    if (nParams != 1)
      return;
    switch(param[0]) {
    case 0:
    case 7:
      break;
    }
  }
#endif
  
  // Do default handling:
  for (uint32_t p = 0; p < MaxParam; p++) {
    if (param[p] == 0)
      param[p] = 1;
  }
  
  switch(b) {
  case '@':			// insert N characters
    {
      int distance = min(param[0], cols - posCol);

      Scroll(pos, pos + (cols - posCol), distance);
      break;
    }
  case 'A':			// Cursor Up N
    {
      uint32_t count = min(param[0], posRow);
      pos -= (cols * count);
      break;
    }
  case 'B':			// Cursor Down N
    {
      uint32_t count = min(param[0], rows - posRow - 1);
      pos += (cols * count);
      break;
    }
  case 'C':			// Cursor Forward N
    {
      uint32_t count = min(param[0], cols - posCol - 1);
      pos += count;
      break;
    }
  case 'D':			// Cursor Back N
    {
      uint32_t count = min(param[0], posCol);
      pos -= count;
      break;
    }
  case 'E':			// Cursor Start of N Lines Down (N)
    {
      uint32_t count = min(param[0], cols - posCol - 1);
      pos += count;
      pos -= (pos % cols);
      break;
    }
  case 'f':			// Cursor position (row, col)
  case 'H':			// Cursor position (row, col)
    {
      uint32_t therow = min(param[0], rows);
      uint32_t thecol = min(param[1], cols);
      therow--;
      thecol--;

      pos = (therow * cols) + thecol;
      break;
    }
  case 'J':			// Erase to End of Screen (none)
    {
      Scroll (pos, rows * cols, (rows * cols) - pos);
      break;
    }
  case 'K':			// Erase to End of Line (none)
    {
      Scroll (pos, pos + (cols - posCol), (cols - posCol));
      break;
    }
  case 'L':			// Insert N Lines above current
    {
      uint32_t nRows = min(param[0], rows - posRow);
      Scroll (posRow * cols, rows * cols, nRows * cols);
      break;
    }
  case 'M':			// Delete N Lines from current
    {
      uint32_t nRows = min(param[0], rows - posRow);
      Scroll (posRow * cols, rows * cols, - ((int) nRows * cols));
      break;
    }
  case 'P':			// Delete N Characters
    {
      int distance = min(param[0], cols - posCol);

      Scroll(pos, pos + (cols - posCol), -distance);
      break;
    }
#if 0
  case 'm':			// select graphic rendition
  case 'p':			// Set black on white mode
  case 'q':			// Set white on black mode
#endif
  case 's':			// RESET (none)
    {
      pos = 0;
      curAttr = Attr::Normal;
      state = WantChar;
      break;
    }
  default:
    break;
  }

  state = WantChar;
}

void
AnsiConsole::PutLog(uint8_t c)
{
  uint32_t posCol = pos % cols;

  // On newline, clear to EOL:
  if (c == ASCII::CR)
    if (pos % cols) Scroll (pos, pos + (cols - posCol), (cols - posCol));

  Put(c);
}

void
AnsiConsole::Put(uint8_t c)
{
  if (state == NotInit)
    Reset();
  
  switch (state) {
  case WantChar:
    {
      if (c == ASCII::ESC) {
	state = SawEsc;
	return;
      }
      
      ProcessChar(c);
      break;
    }
  case SawEsc:
    {
      if (c == '[') {
	state = WantParam;

	nParams = 0;
	for (int i = 0; i < MaxParam; i++)
	  param[i] = 0;
	
	return;
      }

      // Escape sequence error -- pretend we never saw it:
      state = WantChar;
      break;
    }
  case WantParam:
    {
      if (nParams < 2 && c >= '0' && c <= '9') {
	param[nParams] *= 10;
	param[nParams] += (c - '0');
      }
      else if (nParams < 2 && c == ';')
	nParams++;
      else if (nParams == 2)
	state = WantChar;
      else
	ProcessEsc(c);
    }
    break;
  }

  ShowCursorAt(pos);
}

