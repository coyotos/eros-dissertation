/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Persist.hxx>
#include <machine/KernTune.hxx>
#include <eros/memory.h>

#include <disk/DiskNode.hxx>
#include <disk/PagePot.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/util.hxx>
#include <kerninc/Partition.hxx>
#include <kerninc/Checkpoint.hxx>
#include <kerninc/Persist.hxx>
#include <eros/Invoke.h>

#include <machine/PTE.hxx>

#define dbg_io     0x1		/* io requests */
#define dbg_div    0x2		/* div tbl */
#define dbg_req    0x4		/* requests (high level) */
#define dbg_obread 0x8		/* low-level object reads */
/* Following should be an OR of some of the above */
#define dbg_flags   ( 0u )

#define DEBUG(x) if (dbg_##x & dbg_flags)

#ifndef NDEBUG
extern bool InvocationCommitted;
#endif

// static to ensure it ends up in BSS:
static CoreDivision coreDivTbl[KTUNE_NCOREDIV];

void *
CoreDivision::operator new(size_t /* sz */)
{
  int i;
  for (i = 0; i < KTUNE_NCOREDIV; i++) {
    CoreDivision& cd = coreDivTbl[i];
    if (cd.partition == 0) {
      cd.partition = (Partition*) 0x1; // HACK!! corrected in
				      // constructor 
      return &cd;
    }
  }

  return 0;
}

CoreDivision::CoreDivision(Partition* part, const Division& d)
{
  *((Division*)this) = d;
  
  partition = part;
  
#if 0
  nPages = (end - start) / EROS_PAGE_SECTORS;
  if (type == dt_Page)
    nPages = endOid - startOid;
#endif
}


void
Persist::RegisterDivision(Partition *part, const Division& d)
{
  switch(d.type) {
  case dt_Unused:
  case dt_Boot:
  case dt_DivTbl:
  case dt_Spare:
	// These are silently ignored
#if 1
    MsgLog::printf("Divsn on %s%d-%d, secs [%d,%d) (%s) skipped\n",
		   part->ctrlr->name,
		   part->unit, part->ndx,
		   d.start, d.end,
		   d.TypeName() );
#endif
    return;
  case dt_Kernel:
    break;
  }
  
  // FIX: Verify that ranges do not overlap improperly
  
  bool isDuplex = false;

  for (uint32_t i = 0; i < KTUNE_NCOREDIV; i++) {
    if (coreDivTbl[i].type == d.type &&
	coreDivTbl[i].startOid == d.startOid &&
	coreDivTbl[i].endOid == d.endOid) {
      isDuplex = true;
      MsgLog::printf("It's a duplex\n");
    }
  }

  CoreDivision *cd = new CoreDivision(part, d);

  // Kernel division is mounted as a page division:
  if (cd->type == dt_Kernel)
    cd->type = dt_Object;

  if (cd->type == dt_Log && isDuplex == false)
    Checkpoint::AttachRange(d.startOid, d.endOid);
    
  if (!cd)
    MsgLog::fatal("Out of core divisions\n");
  
#if 1
  MsgLog::printf("Divsn on %s%d-%d, secs [%d,%d)"
		 " oid [",
		 part->ctrlr->name,
		 part->unit, part->ndx,
		 cd->start, cd->end);
  MsgLog::print(cd->startOid);
  MsgLog::printf(",");
  MsgLog::print(cd->endOid);
  MsgLog::printf(") (%s)\n", d.TypeName());
#endif
  
  return;
}

#ifdef DDB
void
Persist::ddb_DumpDivisions()
{
  extern void db_printf(const char *, ...);
  
  for (int i = 0; i < KTUNE_NCOREDIV; i++) {
    if (coreDivTbl[i].type != dt_Unused) {
      CoreDivision* cd = &coreDivTbl[i];
      db_printf("Divsn on %s%d-%d, secs [%d,%d)"
		     " oid [",
		     cd->partition->ctrlr->name,
		     cd->partition->unit, cd->partition->ndx,
		     cd->start, cd->end);
      db_printf("0x%08x%08x", (uint32_t) (cd->startOid >> 32),
		(uint32_t) cd->startOid);
      db_printf(",");
      db_printf("0x%08x%08x", (uint32_t) (cd->endOid >> 32),
		(uint32_t) cd->endOid);
      db_printf(") (%s)\n", cd->TypeName());
    }
  }
}
#endif

void
Persist::UnregisterDivisions(Partition* part)
{
  int i;
  for (i = 0; i < KTUNE_NCOREDIV; i++) {
    if (coreDivTbl[i].type != dt_Unused &&
	coreDivTbl[i].partition == part)
      delete &coreDivTbl[i];
  }
}

bool
Persist::HaveCkptLog()
{
  int i;
  for (i = 0; i < KTUNE_NCOREDIV; i++) {
    if (coreDivTbl[i].type == dt_Log)
      return true;
  }
  return false;
}

// In order to proceed, we must reserve an object page frame for the
// inbound I/O (which will be grabbed later), we must verify that
// there are a sufficiency of Request structures, and we must lay our
// hands on the appropriate DuplexedIO structure for the OID we are
// trying to load.  Having the DuplexedIO structures be keyed by OID
// prevents redundant I/O on the same object.  Ensuring space to begin
// with guarantees that a read will never preclude a write on a
// hash-colliding OID, but we may later want to make read and write
// DuplexedIO structures independent since the two operations
// fundamentally cannot collide.
//
// If a user thread fails to satisfy any of the conditions, it will
// Yield() while having reserved an IO page frame.  The Yield code
// checks for and takes care of this case, but note that kernel
// threads and user threads call CommitIoPageFrame in different places
// because kernel threads do not unwind completely.  We will
// definitely need to reconsider what to do when tasks start
// initiating I/O.  That issue in and of itself may be a sufficient
// reason to retain kernel threads.
//
// Finally, note that when invoked on log pages, this routine is
// handed a LogLoc, not an OID.
void
Persist::ReadPageFrame( CoreDivision *pcd, uint32_t relativePage,
			ObType::Type obty, OID oid,
			ObCount count)
{
  DEBUG(io) MsgLog::dprintf(true, "Reserving I/O frame...\n");

  // First make sure the I/O won't block:
  ObjectCache::ReserveIoPageFrame();
  
  DEBUG(io) MsgLog::dprintf(true, "Reserving Requests...\n");
  Request::Require(KTUNE_MAXDUPLEX);

  DEBUG(io) MsgLog::dprintf(true, "Grabbing DIO...\n");
  DuplexedIO *dio = DuplexedIO::Grab(oid, IoCmd::Read);

  ObjectCache::CommitIoPageFrame();

  DEBUG(io) MsgLog::dprintf(true, "Setting up request (dt=%s)...\n",
			    pcd->TypeName());

  dio->allocCount = count;
  dio->obType = obty;
  dio->isObjectRead = true;
  
  // I/O is free to complete while this loop is still running (happens
  // mostly with ramdisk), so we need to go to sleep before queueing
  // any of the requests.  Nothing below this point in this function
  // will sleep, but we MUST avoid preemption until we yield
  // voluntarily!

  Thread::Current()->SleepOn(dio->stallQ);

  uint32_t sectorOffset = relativePage * EROS_PAGE_SECTORS;
  
  for (uint32_t i = 0; i < KTUNE_NCOREDIV; i++) {
    CoreDivision *cd = &coreDivTbl[i];
    
    DEBUG(div)
      if (cd->type != dt_Unused)
	MsgLog::dprintf(true, "Check cd %d (type %s) start=0x%x, end=0x%x\n",
			i,
			cd->TypeName(),
			(uint32_t) cd->startOid,
			(uint32_t) cd->endOid);

    if (cd->type != pcd->type)
      continue;
      
    if ( cd->startOid != pcd->startOid )
      continue;
	
    // Found a replicate of the right range. Queue the I/O on the
    // device: 
    Partition *part = cd->partition;
    assert(part);

    Request *req = new Request(part->unit,
			       IoCmd::Read, sectorOffset + cd->start, EROS_PAGE_SECTORS);
    dio->AddRequest(req);

    assert(req);

    DEBUG(io) MsgLog::dprintf(true, "Insert request on %s%d-%d\n",
			      part->ctrlr->name,
			      part->unit,
			      part->ndx);

    part->InsertRequest(req);
  }

  DEBUG(io) MsgLog::dprintf(true, "Post-setup DIO release\n");

  dio->Release();
  
  Thread::Current()->Yield(/* true */);
}

void
Persist::ReadLogFrame(lid_t lid)
{
  assert ((lid % EROS_OBJECTS_PER_FRAME) == 0);

  CoreDivision * pcd = FindDivision(dt_Log, lid);

  uint64_t relativeOid = lid - pcd->startOid;
  uint32_t relativePage = relativeOid / EROS_OBJECTS_PER_FRAME;

  ReadPageFrame( pcd, relativePage, ObType::PtLogPage, lid, 0 );
}

FrameInfo::FrameInfo(OID argOid)
{
  oid = argOid;
  
  cd = Persist::FindDivision(dt_Object, oid);
  if (cd == 0)
    return;

  assert ((cd->startOid % EROS_OBJECTS_PER_FRAME) == 0);

  uint64_t relativeOid = (oid - cd->startOid);

  // No, some of this calculation is NOT the most straightforward way,
  // but I am trying to avoid divisions involving 64-bit quantities
  // where the divisor is not a power of two.  Also, GCC seems to be
  // barfing on some 64 bit x 32 bit multiplies in certain cases.
  
  obFrameNdx = oid % EROS_OBJECTS_PER_FRAME;
  obFrameOid = oid - obFrameNdx;
  relObFrame = relativeOid / EROS_OBJECTS_PER_FRAME;
  clusterNo = relObFrame / DATA_PAGES_PER_PAGE_CLUSTER;
  tagEntry = relObFrame % DATA_PAGES_PER_PAGE_CLUSTER;

  tagFrameOid = relObFrame - tagEntry;
  tagFrameOid *= EROS_OBJECTS_PER_FRAME;
  tagFrameOid += cd->startOid;

  relObFrame += clusterNo;
  relObFrame++;		// add one for cluster pot
  relObFrame++;		// add one for seq no
  
  relTagFrame = clusterNo * PAGES_PER_PAGE_CLUSTER;
  relTagFrame++;		// add one for seq no

#if 0
  MsgLog::dprintf(false, "   OID=0x%08x%08x frmOid=0x%08x%08x tagOid=0x%08x%08x\n"
                  " relFrm 0x%08x clusNo 0x%08x tagEnt 0x%08x tagFrame 0x%08x\n",
                  (uint32_t) (oid >> 32),
                  (uint32_t) (oid),
                  (uint32_t) (obFrameOid >> 32),
                  (uint32_t) (obFrameOid),
                  (uint32_t) (tagFrameOid >> 32),
                  (uint32_t) (tagFrameOid),
                  relObFrame,
                  clusterNo,
                  tagEntry,
                  relTagFrame);
#endif

  assert( tagEntry < DATA_PAGES_PER_PAGE_CLUSTER );
  assert( obFrameNdx < EROS_OBJECTS_PER_FRAME );
}

CoreDivision*
Persist::FindDivision(DivType dt, OID oid)
{
  for (uint32_t i = 0; i < KTUNE_NCOREDIV; i++) {
    CoreDivision *cd = &coreDivTbl[i];
    
    if (cd->type != dt)
      continue;
      
    OID divOID = cd->startOid;

    if (divOID > oid || cd->endOid <= oid)
      continue;

    return cd;
  }

  return 0;
}

ObjectHeader *
Persist::GetObjectPot(FrameInfo& fi)
{
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top GetObjectPot()");
#endif

  if (fi.cd == 0)
    MsgLog::fatal("Bad oid 0x%08x%08x to GetObjectPot()\n",
		  (fi.oid >> 32), fi.oid);
  
  ObjectHeader *obPotHdr =
    ObjectHeader::Lookup(ObType::PtObjectPot, fi.obFrameOid);

  if (obPotHdr) {
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("End GetObjectPot()");
#endif
    return obPotHdr;
  }

  // Must load node pot:
  ReadPageFrame(fi.cd, fi.relObFrame, ObType::PtObjectPot,
		fi.obFrameOid, 0);
  assert (false);
  return 0;
}

ObjectHeader *
Persist::GetTagPot(FrameInfo& fi)
{
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top GetTagPot()");
#endif

  if (fi.cd == 0)
    MsgLog::fatal("Bad oid 0x%08x%08x to GetTagPot()\n",
		  (fi.oid >> 32), fi.oid);
  
  ObjectHeader *tagPotHdr =
    ObjectHeader::Lookup(ObType::PtAllocPot, fi.tagFrameOid);

  if (tagPotHdr) {
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("End GetTagPot()");
#endif
    return tagPotHdr;
  }

  // Must load node pot:
  ReadPageFrame(fi.cd, fi.relTagFrame, ObType::PtAllocPot,
		fi.tagFrameOid, 0);
  assert (false);
  return 0;
}

Node *
Persist::CopyDiskNode(FrameInfo &fi, DiskNode *nodePot)
{
  if (nodePot[fi.obFrameNdx].oid != fi.oid)
    MsgLog::dprintf(false, "oid=0x%08x%08x, potOid=0x%08x%08x ndOid=0x%08x%08x\n",
		    (uint32_t) (fi.oid >> 32),
		    (uint32_t) fi.oid,
		    (uint32_t) (fi.obFrameOid >> 32),
		    (uint32_t) fi.obFrameOid,
		    (uint32_t) (nodePot[fi.obFrameNdx].oid >> 32),
		    (uint32_t) (nodePot[fi.obFrameNdx].oid) );

  assert (nodePot[fi.obFrameNdx].oid == fi.oid);

  Node *pNode = ObjectCache::GrabNodeFrame();
  assert(Thread::Current()->state == Thread::Running);
  assert(ObjectCache::ValidNodePtr(pNode));
  assert (pNode->kr.IsEmpty());
  assert (pNode->obType == ObType::NtUnprepared);
    
  (*pNode) = nodePot[fi.obFrameNdx];

#ifdef OB_MOD_CHECK
  pNode->ob.check = pNode->CalcCheck();
#endif
  pNode->SetFlags(OFLG_CURRENT|OFLG_DISKCAPS);
#ifdef OFLG_PIN
  assert (pNode->GetFlags(OFLG_PIN) == 0);
#endif
  pNode->ClearFlags(OFLG_CKPT|OFLG_DIRTY|OFLG_REDIRTY|OFLG_IO);
#ifdef DBG_CLEAN
  MsgLog::printf("Object ty %d oid=0x%08x%08x loaded clean\n",
		 pNode->obType,
		 (uint32_t) (pNode->oid >> 32),
		 (uint32_t) pNode->oid);
#endif
  
  pNode->ResetKeyRing();
  pNode->Intern();

  assert (pNode->Validate());

  return pNode;
}

Node *
Persist::GetNode(OID oid, ObCount count, bool useCount)
{
  Node *pNode;

  pNode = ObjectHeader::LookupNode(oid);

  if (pNode) {
#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr)
      Check::Consistency("End GetNode()");
#endif

    assertex(pNode, pNode->Validate());

    if (useCount && pNode->ob.allocCount != count)
      return 0;
      
    return pNode;
  }


  FrameInfo fi(oid);
  if (fi.cd == 0) {
    MsgLog::fatal("nd oid= 0x%08x%08x.  Should sleep on mountwait queue here\n",
		  (uint32_t)(oid>>32), (uint32_t) oid);
    return 0;
  }

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top GetNode()");
#endif

  DEBUG(req) MsgLog::printf("Doing lookup on node 0x%08x%08x\n",
			    (uint32_t) (oid >> 32),
			    (uint32_t) oid);

  assert (InvocationCommitted == false);
  
  DEBUG(req) MsgLog::printf("Loading node from ckpt area\n");

  CoreDirent* cde = Checkpoint::FindObject(oid, ObType::NtUnprepared,
					   Checkpoint::current);
  if (cde == 0)
    return 0;
  
  if (cde != CkNIL) {
    if (cde->type != FRM_TYPE_NODE && cde->type != FRM_TYPE_ZNODE)
      return 0;

    if (useCount && cde->count != count)
      return 0;

    return Checkpoint::LoadCurrentNode(cde);
  }
    
  ObjectHeader *pPot = GetTagPot(fi);
  assert (pPot);
  PagePot *pp = (PagePot *) ObjectCache::ObHdrToPage(pPot);

  if (pp->type[fi.tagEntry] != FRM_TYPE_NODE)
    return 0;
    
  DEBUG(req) MsgLog::printf("Loading node from home loc area\n");

  ObjectHeader *nodePotHdr = GetObjectPot(fi);
  assert(nodePotHdr);

  nodePotHdr->TransLock();
    
  DiskNode* nodePot = (DiskNode*) ObjectCache::ObHdrToPage(nodePotHdr);

  pNode = CopyDiskNode(fi, nodePot);

  nodePotHdr->TransUnlock();

  if (useCount && pNode->ob.allocCount != count)
    return 0;
      
  assert (pNode->Validate());

  return pNode;
}

ObjectHeader *
Persist::GetPage(OID oid, ObCount count, ObType::Type pty, bool useCount)
{
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top GetPage()");
#endif

  bool stop=false;
    
  DEBUG(req)
    MsgLog::dprintf(stop, "Doing lookup on %cpage oid=0x%08x%08x\n",
		    (pty == ObType::PtDataPage ? 'd' : 'c'),
		    (uint32_t) (oid >> 32), (uint32_t) oid);

  ObjectHeader *pPage;

  pPage = ObjectHeader::Lookup(pty, oid);
  if (pPage) {
#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr)
      Check::Consistency("End GetPage()");
#endif

    if (useCount && pPage->ob.allocCount != count)
      return 0;
      
    return pPage;
  }

  FrameInfo fi(oid);
  if (fi.cd == 0) {
    MsgLog::fatal("pg oid= 0x%08x%08x.  Should sleep on mountwait queue here\n",
		  (uint32_t)(oid>>32), (uint32_t) oid);
    return 0;
  }

  assert (InvocationCommitted == false);
  
  DEBUG(req)
    MsgLog::dprintf(stop, "Page lookup failed\n");

  CoreDirent* cde = Checkpoint::FindObject(oid, pty, Checkpoint::current);
  if (cde == 0) {
    DEBUG(req)
      MsgLog::dprintf(true, "Wrong tag type\n");
    return 0;
  }
  
  if (cde != CkNIL) {
    if (useCount && cde->count != count)
      return 0;

    return Checkpoint::LoadCurrentPage(cde);
  }

  DEBUG(req)
    MsgLog::dprintf(stop, "Loading home location of page\n");

  ObjectHeader *pPot = GetTagPot(fi);
  assert (pPot);
  PagePot *pp = (PagePot *) ObjectCache::ObHdrToPage(pPot);

  DEBUG(req)
    MsgLog::dprintf(stop, "rel tag frame %d gives tag %d\n",
		    fi.relTagFrame, pp->type[fi.tagEntry]);
    
  switch (pp->type[fi.tagEntry]) {
  case FRM_TYPE_ZDPAGE:
  case FRM_TYPE_ZCPAGE:
    {
      ObjectHeader* pPage = ObjectCache::GrabPageFrame();

      if (pp->type[fi.tagEntry] == FRM_TYPE_ZDPAGE) {
	if (pty != ObType::PtDataPage)
	  return 0;
	
	uint8_t * pbuf = (uint8_t *) ObjectCache::ObHdrToPage(pPage);
	bzero( (void*) pbuf, EROS_PAGE_SIZE);
      }
      else {
	if (pty != ObType::PtCapPage)
	  return 0;

	// Use DiskKey instead of Key to suppress unchain behavior --
	// this is a virgin page.
	DiskKey* kbuf = (DiskKey *) ObjectCache::ObHdrToPage(pPage);

	// Following because placement vector new simply isn't working.
	for (unsigned i = 0; i < EROS_PAGE_SIZE/sizeof(DiskKey); i++)
	  kbuf[i] = DiskKey();
      }

      pPage->ob.oid = oid;
      pPage->ob.allocCount = pp->count[fi.tagEntry];
      pPage->SetFlags(OFLG_CURRENT|OFLG_DISKCAPS);
#ifdef OFLG_PIN
      assert (pPage->GetFlags(OFLG_PIN) == 0);
#endif
      pPage->ClearFlags(OFLG_CKPT|OFLG_DIRTY|OFLG_REDIRTY|OFLG_IO);
#ifdef DBG_CLEAN
      MsgLog::printf("Object ty %d oid=0x%08x%08x loaded clean\n",
		     pPage->obType,
		     (uint32_t) (pPage->oid >> 32),
		     (uint32_t) pPage->oid);
#endif
      pPage->ob.ioCount = 0;
      pPage->obType = pty;
      pPage->products = 0;
#ifdef OB_MOD_CHECK
      pPage->ob.check = pPage->CalcCheck();
#endif
      pPage->ResetKeyRing();
      pPage->Intern();

#ifndef NDEBUG
      ObjectHeader *npPage = ObjectHeader::Lookup(pty,oid);
      assert(npPage == pPage);
#endif

      return pPage;
    }
  case FRM_TYPE_DPAGE:
  case FRM_TYPE_CPAGE:
      if (pp->type[fi.tagEntry] == FRM_TYPE_DPAGE) {
	if (pty != ObType::PtDataPage)
	  return 0;
      }
      else {
	if (pty != ObType::PtCapPage)
	  return 0;
      }

    ReadPageFrame(fi.cd, fi.relObFrame, pty,
		  fi.obFrameOid, pp->count[fi.tagEntry]); 
    assert(false);
    break;
  default:
    return 0;
  }

  // If all of the above fail, page is for an out of range OID.

  MsgLog::printf("Out of range page OID ");
  MsgLog::print(oid);
  MsgLog::fatal("\n");
  return 0;
}

// Wrapper for GetCkFrame used in checkpoint load logic.  Catches the
// yield thrown by the checkpoint logic, issues a DirectedYield(), and
// retries on wakeup.
ObjectHeader *
Persist::KernGetCkFrame(lid_t lid)
{
#ifndef NDEBUG
  extern uint32_t TrapDepth;
  assert ( Thread::Current()->IsKernel() && TrapDepth == 0);
#endif

  Process *p = Thread::CurContext();

  /* hand-construct a misc logpage key in slot 1: */
  Key& k = p->keyReg[1];
  k.NH_ZeroKey();		// restore slot to well-known state
  k.SetType(KtMisc);
  k.subType = MiscKeyType::LogFrame;
  k.nk.value[0] = lid;
  k.nk.value[1] = 0;
  k.nk.value[2] = 0;

  Message msg;
  bzero(&msg, sizeof(msg));
  msg.invType = IT_Call;
  msg.snd_invKey = 1;
  msg.snd_code = KT;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;
  
  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  
  CALL(&msg);
  
  ObjectHeader * pLogPageHdr =
    ObjectHeader::Lookup(ObType::PtLogPage, lid);

  assert(pLogPageHdr);
  
  DEBUG(req) MsgLog::printf("GetCkPage: pLogPageHdr = 0x%08x\n",
			    pLogPageHdr);

  assert (pLogPageHdr);
  return pLogPageHdr;
}

ObjectHeader *
Persist::ZeroCkFrame(lid_t lid)
{
  ObjectHeader *pObj = ObjectCache::GrabPageFrame();
  bzero( (void*) ObjectCache::ObHdrToPage(pObj), EROS_PAGE_SIZE );

  pObj->ob.oid = lid;
  pObj->ob.allocCount = 0;
  pObj->SetFlags(OFLG_CURRENT|OFLG_DISKCAPS);
#ifdef OFLG_PIN
  assert (pObj->GetFlags(OFLG_PIN) == 0);
#endif
  pObj->ClearFlags(OFLG_CKPT|OFLG_IO);
  pObj->SetFlags(OFLG_DIRTY|OFLG_REDIRTY);
#ifdef DBG_CLEAN
  MsgLog::printf("Object ty %d oid=0x%08x%08x loaded clean\n",
		 pObj->obType,
		 (uint32_t) (pObj->oid >> 32),
		 (uint32_t) pObj->oid);
#endif
  pObj->ob.ioCount = 0;
  pObj->obType = ObType::PtLogPage;
  pObj->products = 0;

#ifdef OB_MOD_CHECK
  pObj->ob.check = pObj->CalcCheck();
#endif

  pObj->ResetKeyRing();
  pObj->Intern();

  return pObj;
}

ObjectHeader *
Persist::GetCkFrame(lid_t lid)
{
  lid = EROS_FRAME_FROM_OID(lid);

  DEBUG(req) MsgLog::printf("GetCkPage: lid = 0x%x\n", lid);
  
  CoreDivision *pcd = FindDivision(dt_Log, lid);

  if (!pcd)
    return 0;

  ObjectHeader * pLogPageHdr =
    ObjectHeader::Lookup(ObType::PtLogPage, lid);

  DEBUG(req) MsgLog::printf("GetCkPage: pLogPageHdr = 0x%08x\n",
			    pLogPageHdr);

  if (pLogPageHdr)
    return pLogPageHdr;
  
  assert (InvocationCommitted == false);
  
  DEBUG(req) MsgLog::printf("GetCkPage: Reading page...\n");

  // If an I/O is already pending, wait for it.  If there are live
  // objects in the frame, fetch it. Otherwise, allocate a new zero
  // frame. 

  DuplexedIO *pDio = DuplexedIO::FindPendingIO(lid, ObType::PtLogPage);
  if (pDio) {
    DEBUG(req) MsgLog::printf("Thread 0x%08x Sleeps because I/O is "
			      "already in progress\n"); 
    DEBUG(req) MsgLog::dprintf(true, "DIO [0x%08x]: status %d nReq %d pObHdr=0x%08x\n",
			       pDio, pDio->status, pDio->nRequest, pDio->pObHdr);
    Thread::Current()->SleepOn(pDio->stallQ);
    Thread::Current()->Yield();
  }
    
  if (Checkpoint::FrameIsEmpty(lid)) {
    ObjectHeader *pObj = ZeroCkFrame(lid);
    assert ( PTE::ObIsNotWritable(pObj) );
    pObj->ClearFlags(OFLG_DIRTY|OFLG_REDIRTY);
    
    return pObj;
  }
  else {
    ReadLogFrame(lid);
    return 0;			// suppress GCC warning
  }
}

// Return true if the passed division is a duplex of 'this', including
// if it *is* this.
bool
CoreDivision::IsDuplexOf(const CoreDivision *cd)
{
  DEBUG(div)
    if (cd->type != dt_Unused)
      MsgLog::dprintf(true, "Check cd %d (type %s) start=0x%x, end=0x%x\n",
		      cd - coreDivTbl,
		      cd->TypeName(),
		      (uint32_t) cd->startOid,
		      (uint32_t) cd->endOid);

  if (cd->type != type)
    return false;
      
  if ( cd->startOid != startOid )
    return false;

  return true;
}

// Write a page to the first sector of all object ranges...
void
Persist::WriteRangeHeaders(struct ObjectHeader *pObj)
{
  Request::Require(KTUNE_NCOREDIV);

  // This must not be done lazily:
  DuplexedIO *dio = DuplexedIO::Grab(0, IoCmd::Write); 

  DEBUG(io) MsgLog::dprintf(true, "Setting up request (dt=ALL)...\n");

  dio->pObHdr = pObj;
  dio->completionCallBack = 0;
  
  pObj->SetFlags(OFLG_IO);
  pObj->ClearFlags(OFLG_REDIRTY);
  pObj->ob.ioCount = 1;

  dio->ioaddr = ObjectCache::ObHdrToPage(pObj);
    
  for (uint32_t i = 0; i < KTUNE_NCOREDIV; i++) {
    CoreDivision *cd = &coreDivTbl[i];

    if (cd->type != dt_Object)
      continue;
    
    // Found an object range. Queue the I/O on the device:
    Partition *part = cd->partition;
    assert(part);

    Request *req = new Request(part->unit,
			       IoCmd::Write, cd->start,
			       EROS_PAGE_SECTORS);
    assert(req);
    dio->AddRequest(req);

    assert(req);

    DEBUG(io) MsgLog::dprintf(true, "Insert request on %s%d-%d\n",
			      part->ctrlr->name,
			      part->unit,
			      part->ndx);
    part->InsertRequest(req);
  }

#if 0
  // There is never any need to wait for this I/O.
  if (synchronous)
    Thread::Current()->SleepOn(dio.stallQ);
#endif

#if 0
  MsgLog::dprintf(true, "Write dt=%s DIO 0x%08x pOb 0x%08x data 0x%08x ready to run\n",
		  pcd->TypeName(), &dio, dio.pObHdr, dio.ioaddr);
#endif

  dio->Release();

  assert(Thread::Current());
  Thread::Current()->pageWriteCount++;

#if 0
  if (synchronous)
    Thread::Current()->Yield();
#endif
}

// Write a page to a core division and all of it's duplexes, without
// regard to the type of the page -- this is the bottom level write
// implementation. 
void
Persist::WritePageTo(struct ObjectHeader *pObj, CoreDivision *pcd,
		     uint32_t atSector, bool synchronous,
		     void (*callBack)(DuplexedIO*))
{
  assert (pObj);
  assert (pcd);
  assert (pObj->obType > ObType::NtLAST_NODE_TYPE);

#if defined(OB_MOD_CHECK) && 0
  if (pObj->IsDirty())
    pObj->check = pObj->CalcCheck();
#endif
  
  if (pObj->GetFlags(OFLG_IO)) {
    Thread::Current()->SleepOn(pObj->ObjectSleepQueue());
    Thread::Current()->Yield();
  }
    
  assert ( PTE::ObIsNotWritable(pObj) );

  Request::Require(KTUNE_MAXDUPLEX);

  DuplexedIO *dio = DuplexedIO::Grab(pObj->ob.oid, IoCmd::Write);

  DEBUG(io) MsgLog::dprintf(true, "Setting up request (dt=%s)...\n",
			    pcd->TypeName(pcd->type));

  dio->pObHdr = pObj;
  dio->completionCallBack = callBack;
  
  pObj->SetFlags(OFLG_IO);
  pObj->ClearFlags(OFLG_REDIRTY);
  pObj->ob.ioCount = 1;

  dio->ioaddr = ObjectCache::ObHdrToPage(pObj);
   
  for (uint32_t i = 0; i < KTUNE_NCOREDIV; i++) {
    CoreDivision *cd = &coreDivTbl[i];

    if (! cd->IsDuplexOf(pcd))
      continue;
    
    // Found a replicate of the right range. Queue the I/O on the
    // device: 
    Partition *part = cd->partition;
    assert(part);

    Request *req = new Request(part->unit,
			       IoCmd::Write, atSector + cd->start,
			       EROS_PAGE_SECTORS);
    assert(req);
    dio->AddRequest(req);

    assert(req);

    DEBUG(io) MsgLog::dprintf(true, "Insert request on %s%d-%d\n",
			      part->ctrlr->name,
			      part->unit,
			      part->ndx);
    part->InsertRequest(req);
  }

  if (synchronous)
    Thread::Current()->SleepOn(dio->stallQ);

#if 0
  MsgLog::dprintf(true, "Write dt=%s DIO 0x%08x pOb 0x%08x data 0x%08x ready to run\n",
		  pcd->TypeName(), dio, dio->pObHdr, dio->ioaddr);
#endif
  dio->Release();

  assert(Thread::Current());
  Thread::Current()->pageWriteCount++;

  if (synchronous)
    Thread::Current()->Yield();
}

void
Persist::WritePageToHome(struct ObjectHeader *pObj, OID oid)
{
  assert (pObj->obType == ObType::PtLogPage ||
	  pObj->obType == ObType::PtDataPage ||
	  pObj->obType == ObType::PtCapPage);

  // assert(pObj->IsDirty());

  FrameInfo fi(oid);
  if (fi.cd == 0)
    MsgLog::fatal("Writing object to nonexistent range\n");

  WritePageTo(pObj, fi.cd, fi.relObFrame * EROS_PAGE_SECTORS);
}

void
Persist::WritePageToLog(struct ObjectHeader *pObj, lid_t lid,
			bool synchronous,
			void (*callBack)(DuplexedIO*))
{
  assert (pObj->obType == ObType::PtLogPage
	  || pObj->obType == ObType::PtDriverPage);
  
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top WritePageToLog()");
#endif

#ifndef NDEBUG
  if ( pObj->obType == ObType::PtLogPage && pObj->IsDirty() == false )
    MsgLog::dprintf(true, "Object (hdr=0x%08x) is not dirty in WritePageToLog()\n",
		    pObj);
  assert(pObj->obType == ObType::PtDriverPage || pObj->IsDirty());
#endif

  ObjectHeader * pLogPageHdr =
    ObjectHeader::Lookup(ObType::PtLogPage, lid);
  if (pLogPageHdr) {
    assert (pLogPageHdr != pObj);
    ObjectCache::ReleasePageFrame(pLogPageHdr);
  }

  CoreDivision * pcd = FindDivision(dt_Log, lid);

  // Do not deal with dismounted home ranges yet.
  assert (pcd);

  uint64_t relativeOid = lid - pcd->startOid;

  uint32_t relativePage = relativeOid / EROS_OBJECTS_PER_FRAME;

  WritePageTo(pObj, pcd, relativePage * EROS_PAGE_SECTORS,
	      synchronous, callBack);
}


// WritePage() is called by the ageing logic to force pages to disk.
// To my initial surprise, deciding where to put them is not
// difficult:
//
//   LogPage      written to the log
//   UserPage     written to the log
//   NodePot      written to home location -- node pots are ONLY
//                dirtied by the migrator, so their content is safe.
//   AllocPot     written to home location, as with NodePot.
//
// Rewriting dirty pages back to home locations might seem a bit
// tricky, but turns out to be very simple.  The page isn't migrated
// unless it is current.  If it *is* current, the migrator brings it
// in to core and then uses Persist::WritePageToHome() to force it to
// go out to it's home location.
//
// In general, the I/O subsystem allows a dirty page to be remodified
// if it is in an outbound I/O queue.  Note that since it was current,
// the user page going to home location CANNOT be dirty.  Attempts to
// mutate it will therefore generate a fault, at which point we will
// notice that this page is the checkpoint version and perform COW.

// FIX: if page being written is ckpt and dirty, should retag as log frame.
void
Persist::WritePage(struct ObjectHeader *pObj, bool synchronous)
{
  uint32_t relativePage = 0;
  CoreDivision *cd = 0;

  assert(pObj->IsDirty());

  switch(pObj->obType) {
  case ObType::PtAllocPot:
    {
      // Going to home location.  OID == oid of first covered entry,
      // so we don't need to round that.  We do need to adjust the
      // offset by the overhead of intervening clusters, if any:
      
      FrameInfo fi(pObj->ob.oid);
      if (fi.cd == 0)
	MsgLog::fatal("Writing object to nonexistent range\n");

      relativePage = fi.relTagFrame;
      cd = fi.cd;
      break;
    }

  case ObType::PtObjectPot: 
    {
      // Going to home location.  OID == oid of first covered entry,
      // so we don't need to round that, but the computation of
      // relative page needs to be scaled by the cluster size:
      
      FrameInfo fi(pObj->ob.oid);
      if (fi.cd == 0)
	MsgLog::fatal("Writing object to nonexistent range\n");

      relativePage = fi.relObFrame;
      cd = fi.cd;
      break;
    }

  case ObType::PtDataPage:
  case ObType::PtCapPage:
    {
      lid_t where = Checkpoint::GetLidForPage(pObj);
#if 1
      if (where == ZERO_LID) {
#ifdef OB_MOD_CHECK
	pObj->ob.check = pObj->CalcCheck();
	assert(pObj->ob.check == 0);
#endif

	pObj->ClearFlags(OFLG_IO|OFLG_DIRTY|OFLG_REDIRTY);
#ifdef DBG_CLEAN
	MsgLog::printf("Object ty %d oid=0x%08x%08x written zero\n",
		       pObj->obType,
		       (uint32_t) (pObj->oid >> 32),
		       (uint32_t) pObj->oid);
#endif

	pObj->ObjectSleepQueue().WakeAll();
	return;
      }
#endif

#if 0
      // FIX: this seems to be causing trouble, and it isn't
      // necessary.
      //
      //
      // If this is no longer the current version of the page, retag
      // this object as a copy of the log frame so that the migrator
      // will be able to find it later if it has not been aged out.
      // Note that if we fail to get the proper I/O request structure,
      // this write will recur properly, because StabilizePages()
      // stabilizes dirty log frames as well.
      if (pObj->flags.current == 0) {
	assert (pObj->flags.ckpt);
	pObj->flags.ckpt = 0;
	pObj->obType = ObType::PtLogPage;
	pObj->oid = where;
	pObj->Intern();
      }
#endif
      
      cd = FindDivision(dt_Log, where);
      relativePage = where - (uint32_t) cd->startOid;
      relativePage /= EROS_OBJECTS_PER_FRAME;
      break;
    }

  case ObType::PtLogPage:
    {
      lid_t where = pObj->ob.oid;
      cd = FindDivision(dt_Log, where);

      relativePage = where - (uint32_t) cd->startOid;
      relativePage /= EROS_OBJECTS_PER_FRAME;
      break;
    }
  default:
    MsgLog::fatal("Don't know how to write obtype=%d\n", pObj->obType);
    break;
  }

  WritePageTo(pObj, cd, relativePage * EROS_PAGE_SECTORS, synchronous);
}
