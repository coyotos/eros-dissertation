/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Console.hxx>
#include <kerninc/Machine.hxx>

static bool Probe(AutoConf *);
static bool Attach(AutoConf *);
struct Driver ac_idler = {
  "idler", Probe, Attach
} ;

const uint32_t StackSize = 256;
static uint32_t Stack[StackSize];

// I wonder
// if I shouldn't hand-code this thread in assembler for the sake of
// reducing it's stack, though.

class IdleThread : public KernThread {
public:
  static void Start();

  IdleThread()
    :KernThread("Idler", 
		&IdleThread::Start,
		Stack, &Stack[StackSize])
  {
    procContext.InitStack();
  }
};

IdleThread TheIdleThread;
Thread *pIdleThread = &TheIdleThread;

static bool
Probe(AutoConf*)
{
  CpuReserve::KernIdleCpuReserve.AddKernelThread(&TheIdleThread);

  return true;
}

static bool
Attach(AutoConf* ac)
{
  TheIdleThread.Wakeup();	// let it initialize itself...
  
  MsgLog::printf("%s (no vector)\n", ac->name);
  return true;
}

// Unlike all other threads, this one runs once and then exits (or at
// least sleeps forever).
void
IdleThread::Start()
{
  int stack;
  MsgLog::printf("Start IdleThread (thread 0x%x,context 0x%x,stack 0x%x)\n",
	       &TheIdleThread, TheIdleThread.context, &stack);

  for(;;) {
    // On machines with high privilege-crossing latency, it is NOT a
    // good idea for the idle thread to enter and leave the kernel
    // aggressively.  On the x86, for example, this can easily render
    // the processor non-interruptable for 300-400 cycles PER YIELD,
    // which (among other problems) has the effect of quantizing
    // interrupt response time. 
    //    Machine::SpinWaitUs(50);
    TheIdleThread.DirectedYield();
#if 0
    Console::Twiddle(0);
#endif
  }

    // We will never wake up again...
  TheIdleThread.SleepOn(IdlePile);
  TheIdleThread.DirectedYield();
}
