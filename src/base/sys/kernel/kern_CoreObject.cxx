/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/MsgLog.hxx>
#include <kerninc/kernel.hxx>
#include <kerninc/util.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/ObjectHeader.hxx>
#include <kerninc/Machine.hxx>
// #include "Blast.hxx"
#include <kerninc/Node.hxx>
// #include "IDT.hxx"

//#define LOCKDEBUG
     
CoreObject::CoreObject()
{
  bzero(this, sizeof(CoreObject));
}

void
CoreObject::reset()
{
#ifdef LOCKDEBUG
  MsgLog::printf("Reset CTE 0x%x\n", this);
#endif
  bzero(this, sizeof(CoreObject));
}

// NOTE ON CORE OBJECT LOCKING
//
// A core object can be "locked" in three senses:
//
// The 'lock' flag can be set, which is a temporary state used as a
// correctness check in the segment translation logic.
//
// The object may have a nonzero pin count, in which case it cannot be
// removed from memory
//
// The object may be hazarded by I/O, in which case the pPendingReq
// field is nonzero.  We no longer have an I/O count field within the
// CoreObject structure, because we now keep duplexing counts in the
// IoRequest structure.  In the current architecture, we impose the
// restriction that an object may only have at most one I/O request
// pending on it at a time.  This has implications for buffered I/O
// that may force me to reconsider.
//
// OLD ARCHITECTURE:
// The object may have a nonzero ioCount, indicating that one or more
// I/O operations are pending against this object.  If the ioCount is
// nonzero, it is unwise to change the state of the object.
//
// ABOUT THE LOCK FLAG
//
// In an MP architecture, the lock bit will need to be replaced with a
// processor pointer, so we can distinguish between these cases.

int
CoreObject::isLocked()
{
  int r = ((flags & FlLocked) != 0);
#ifdef LOCKDEBUG
  MsgLog::printf("CTE 0x%x is locked.\n", this);
#endif
  return r;
}

// This should ONLY be called by CoreObjectLocker, to ensure that the
// lock will be released if the holder calls Resched()!

bool
CoreObject::lock()
{
  int cpl = Machine::splhigh();

  bool result;
  
  if (flags & FlLocked) {
    result = false;
  }
  else {
    flags |= FlLocked;
    result = true;
  }
  
  Machine::splx(cpl);
#ifdef LOCKDEBUG
  MsgLog::printf("Locked CTE 0x%x\n", this);
#endif

  return result;
}

void
CoreObject::unlock()
{
  int cpl = Machine::splhigh();
  
  flags &= ~FlLocked;
  
  Machine::splx(cpl);
#ifdef LOCKDEBUG
  MsgLog::printf("Unlocked CTE 0x%x\n", this);
#endif
}

void
CoreObject::ioLock()
{
  int cpl = Machine::splhigh();
  
  // cant fail with any plausible number of simultaneous IO
  // operations, so dont even check.
  ioCount++;
  
  Machine::splx(cpl);
}

void
CoreObject::ioUnLock()
{
  assert(ioCount);
  
  int cpl = Machine::splhigh();
  
  // cant fail with any plausible number of simultaneous IO
  // operations, so dont even check.
  ioCount--;
  
  if (!ioCount)
    flags &= ~FlRhazard;
  
  Machine::splx(cpl);
}

void
CoreObject::setType(ObType ty)
{
  int cpl = Machine::splhigh();
  obType = ty;
  Machine::splx(cpl);
}
