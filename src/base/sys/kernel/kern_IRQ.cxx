/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/IntAction.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/Task.hxx>

// #define TIMING_DEBUG
// #define PROBE_DEBUG

uint32_t IRQ::DisableDepth = 1;	// interrupts disabled on kernel entry
uint32_t IRQ::enableMask = 0;
volatile uint32_t IRQ::PendingInterrupts = 0;

IntAction *IRQ::irqActions[NUM_HW_INTERRUPT];
static bool wiredExclusive[NUM_HW_INTERRUPT]; // inits to false



// Two independent things must be managed by the spl* functions:
//
//   Whether interrupts are enabled (at all)
//   Which interrupts may be taken.
//

bool
IRQ::WireExclusive(IntAction* pia)
{
  uint32_t interrupt = pia->irq;

  assert(pia->wired == false);
  
  if (irqActions[interrupt] != 0)
    return false;
  
  wiredExclusive[interrupt] = true;
  irqActions[interrupt] = pia;
  pia->next = 0;
  return true;
}

bool
IRQ::WireShared(IntAction* pia)
{
  uint32_t interrupt = pia->irq;

  assert(pia->wired == false);

  if (wiredExclusive[interrupt] == true)
    return false;
  
  pia->next = irqActions[interrupt];
  irqActions[interrupt] = pia;
  
  return true;
}

void
IRQ::Unwire(IntAction* pia)
{
  uint32_t interrupt = pia->irq;
  assert(pia->wired == true);

  if ( pia == irqActions[interrupt] ) {
    irqActions[interrupt] = pia->next;
  }
  else {
    IntAction *iq = irqActions[interrupt];

    while (iq) {
      if (iq->next == pia) {
	iq->next = pia->next;
	break;
      }
      iq = iq->next;
    }
  }

  pia->next = 0;

  if (irqActions[interrupt] == 0)
    wiredExclusive[interrupt] = false;
}

uint32_t
IRQ::BeginProbe()
{
  uint32_t irqs = 0;


  /* Enable all unassigned interrupts: */
  for (int i = (NUM_HW_INTERRUPT - 1); i >= 0; i--) {
    if (irqActions[i] == 0) {
      IRQ::Enable(i);
      irqs |= (1 << i);
    }
  }

#ifdef PROBE_DEBUG
  MsgLog::printf("IRQ::BeginProbe: irqs=0x%04x %d EN=0x%04x PICEN=0x%04x\n",
		 irqs, IRQ::DisableDepth, enableMask,
		 GetEnableMaskFromPIC() );
#endif

  // Is a settling delay required?  We already do the spurious
  // interrupt check in the trap handler, so probably not.
  
  return irqs;
}

int
IRQ::EndProbe(uint32_t irqs)
{
  int i;

  // Asssuming the driver was able to generate an interrupt, the
  // interrupt came in on a previously unassigned line, was detected
  // as unassigned by the interrupt handler, and disabled.  Thus, we
  // ought now to find that a single unassigned interrupt has been
  // disabled.

#ifdef PROBE_DEBUG
  MsgLog::printf("IRQ::EndProbe: irqs=0x%04x %d EN=0x%04x PICEN-0x%04x\n",
		 irqs, IRQ::DisableDepth, enableMask,
		 GetEnableMaskFromPIC() );
#endif

  irqs &= enableMask;

  if (!irqs)
    return 0;

  i = Machine::FindFirstZero(irqs);

  if (irqs != (irqs & (1 << i)))
    i = -i;

  return i;
}

bool
IRQ::RunActions(uint32_t interrupt)
{
  if (IRQ::irqActions[interrupt]) {
    IntAction *ia = irqActions[interrupt];
    while (ia) {
      assert(ia->irq == interrupt);
      ia->fn(ia);
      ia = ia->next;
    }
    return true;
  }
  else {
    MsgLog::printf("STRAY INTERRUPT %d\n", interrupt);
    IRQ::Disable(interrupt);
  }
  return false;
}

// If an interrupt is pending, it is masked on the PIC.  An explicit
// enable will re-enable the interrupt on the PIC.  Unless the
// interrupt is explicitly re-enabled, it is disabled on the PIC while
// it is pending -- this is to avoid taking a redundant interrupt when
// interrupts are level triggered.  The interrupt remains pending
// until after the handler runs.
//
// This must be called with interrupts disabled!

void
IRQ::RunInterruptHandlers()
{
  while (PendingInterrupts) {
    uint32_t irq  = Machine::FindFirstZero(~IRQ::PendingInterrupts);
    uint32_t irqbit = (1u << irq);

#if 0
    if (irq == 3)
      MsgLog::printf("pending after remove 3: 0x%08x\n", PendingInterrupts);
#endif

    ENABLE();
    RunActions(irq);
    DISABLE();

    PendingInterrupts &= ~irqbit;
    // If the interrupt is still enabled, uninhibit the PIC now that
    // we have handled it -- interrupt handlers are not reentrant!
    if (enableMask & irqbit)
      EnablePIC(irq);
    
    if (Task::activeTasks) {
      IRQ::ENABLE();
      Task::RunActiveTasks();
      IRQ::DISABLE();
    }
  }
}

