/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Driver for generic ramdisk:

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/util.hxx>
#include <kerninc/BlockDev.hxx>
#include <kerninc/IoRequest.hxx>
#include <kerninc/Partition.hxx>
#include <eros/Device.h>

#define dbg_ramio	0x1	/* migration state machine */

/* Following should be an OR of some of the above */
#define dbg_flags   (0)

#define DEBUG(x) if (dbg_##x & dbg_flags)

static bool Probe(AutoConf *);
static bool Attach(AutoConf *);
struct Driver ac_ramdisk = {
  "ramdisk", Probe, Attach
} ;

class RamDiskCtrlr : public BlockDev {
  RequestQueue rq;
  bool mounted;
  uint32_t nSecs;			// number of sectors in Ramdisk
  kva_t diskAddr;		// address where disk is found

public:
  void MountUnit(uint8_t unit);	// called by mount daemon
  void GetUnitInfo(uint8_t unit, BlockUnitInfo&);

  void InsertRequest(Request* req);
  void StartIO();

  RamDiskCtrlr(kva_t where, uint32_t nsecs);
} ;

static bool
Probe(struct AutoConf*)
{
  if (Machine::GetRamDiskSize())
    return true;

  else
    MsgLog::printf("No ramdisk found\n");

  return false;
}

static bool
Attach(struct AutoConf*)
{
  RamDiskCtrlr *rdc =
    new (0) RamDiskCtrlr(
			 Machine::GetRamDiskAddress(),
			 Machine::GetRamDiskSize()
			 );
  rdc->Register();
  return true;
}

RamDiskCtrlr::RamDiskCtrlr(kva_t where, uint32_t nsecs)
{
  name = "ram";
  devClass = DEV_DISK_RAM;
  nUnits = 1;
  mounted = false;
  diskAddr = where;
  nSecs = nsecs;
}

void 
RamDiskCtrlr::MountUnit(uint8_t unit)
{
  // There is nothing much to do for the ramdisk:
  assert(unit == 0);
  assert (mounted == false);
  mounted = true;
  totalMountedUnits++;
}

void
RamDiskCtrlr::GetUnitInfo(uint8_t unit, BlockUnitInfo& ui)
{
  assert(unit == 0);

  ui.b_geom.hd = 1;
  ui.b_geom.cyl = 1;
  ui.b_geom.sec = nSecs;
  
  ui.d_geom = ui.b_geom;
  ui.isEros = true;
  ui.isBoot = true;
  ui.isDisk = true;
  ui.isMounted = mounted;
  ui.nSecs = nSecs;
  ui.needsMount = !mounted;
  ui.hasPartitions = false;
}

void 
RamDiskCtrlr::InsertRequest(Request* req)
{
  assert(req->unit == 0);
  rq.InsertRequest(req);
  StartIO();
}

void 
RamDiskCtrlr::StartIO()
{
  if (inDuplexWait)
    return;

  while ( ! rq.IsEmpty() ) {
    Request *r = rq.GetNextRequest();

    DEBUG(ramio)
      MsgLog::printf("Ramdisk found request: start %d nsec %d\n",
		     r->req_start, r->req_nsec);

    if (r->Commit(this, this) == false) {
      return;
    }

    kva_t secAddr = diskAddr + r->req_start * EROS_SECTOR_SIZE;

#if 0
    IOR::IoResult result = IOR::OK;
#endif
      
    if ((r->req_start + r->req_nsec) > nSecs)
      MsgLog::fatal("IO Range error - start %d nsec %d\n",
		    r->req_start, r->req_nsec);

    switch (r->cmd) {
    case IoCmd::Read:
      if ( ! r->IsCompleted() ) {
	DEBUG(ramio)
	  MsgLog::printf("Ramdisk: READ %d bytes from sector %d to addr 0x%x\n",
			 r->req_nsec * EROS_SECTOR_SIZE,
			 r->req_start, r->req_ioaddr);

	bcopy((const void *) secAddr, (void *) r->req_ioaddr, r->req_nsec * EROS_SECTOR_SIZE);
      }
      break;
    case IoCmd::Write:

      DEBUG(ramio)
	MsgLog::printf("Ramdisk: WRITE %d bytes to sector %d from addr 0x%x\n",
		       r->req_nsec * EROS_SECTOR_SIZE,
		       r->req_start, r->req_ioaddr);

      bcopy((const void *)r->req_ioaddr, (void *) secAddr, r->req_nsec * EROS_SECTOR_SIZE);
      break;
    case IoCmd::Plug:
      DEBUG(ramio)
	MsgLog::printf("Ramdisk: PLUG passes\n");

      break;
    }

    r->Finish();
    rq.RemoveRequest(r);
  }
}
