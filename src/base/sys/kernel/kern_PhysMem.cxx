/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/util.hxx>
#include <kerninc/PhysMem.hxx>
     
// #define ALLOCDEBUG

// Implementation of Memory Allocation
     
PhysMem::MemInfo PhysMem::memClass[Mc_NUM_MEM_CLASS];
uint32_t PhysMem::kernMemTop;
uint32_t PhysMem::physMemTop;

uint32_t
PhysMem::AvailPages()
{
  uint32_t nPages = 0;
    
  // The computation rounds down deliberately!
  for (int mc = 0; mc < Mc_NUM_MEM_CLASS; mc++)
    nPages += AvailPages((MemClass) mc);

  return nPages;
}

uint32_t
PhysMem::AvailBytes()
{
  uint32_t nBytes = 0;
    
  // The computation rounds down deliberately!
  for (int mc = 0; mc < Mc_NUM_MEM_CLASS; mc++)
    nBytes += AvailBytes((MemClass) mc);

  return nBytes;
}

void *
PhysMem::Alloc(size_t nbytes, MemClass mc, uint32_t alignment)
{
  uint32_t where;

  if ((nbytes % EROS_PAGE_SIZE) == 0)
    alignment = EROS_PAGE_SIZE;
    
  // Okay to allocate in a lower memory class, but not a higher one:
    
  int imc = mc;
    
  for (;imc >= 0; imc--) {
    where = align(memClass[imc].base, alignment);
    assert(where >= memClass[imc].base);
    if (memClass[imc].top - where >= nbytes) {
      memClass[imc].base = where + nbytes;
#if 0
      MsgLog::printf("Found memclass %d at 0x%x\n", imc, where);
      Stall(1000000);
#endif
      bzero((void *) where, nbytes);
      return (void *) where;
    }
  }

  MsgLog::fatal("Memory exhausted!!!\n");
  return 0;
}

uint32_t
PhysMem::PhysicalPages()
{
  uint32_t availBytes = VTOP(physMemTop);

  // The following rounds down deliberately!
  return availBytes / EROS_PAGE_SIZE;
}

uint32_t
PhysMem::KernelPages()
{
  uint32_t availBytes = VTOP(kernMemTop);

  // The following rounds down deliberately!
  return availBytes / EROS_PAGE_SIZE;
}

void
PhysMem::PrintStatus()
{
  for (int i = 0; i < Mc_NUM_MEM_CLASS; i++)
    MsgLog::printf("Memory class %d: %d bytes available\n", i,
		 memClass[i].top - memClass[i].base);
}

#define NEW_STOP false
void *
operator new[](size_t sz, void *place)
{
  void *v = 0;
  
  if (place == 0)
    v = PhysMem::Alloc(sz, McMemHi, 4);
  else
    MsgLog::fatal("unknown placement\n");
    
  MsgLog::dprintf(NEW_STOP, "placement vec new grabs "
		  "0x%x (%d) at 0x%08x\n", sz,
		  sz, v);

  return v;
}

void *
operator new(size_t sz, void * place)
{
  void *v = 0;
  
  if (place == 0)
    v = PhysMem::Alloc(sz, McMemHi, 4);
  else
    MsgLog::fatal("unknown placement\n");
    
  MsgLog::dprintf(NEW_STOP, "placement new grabs "
		  "0x%x (%d) at 0x%08x\n", sz,
		  sz, v);

  return v;
}

void *
operator new(size_t sz)
{
  MsgLog::fatal("Inappropriate call to non-placement operator new\n");
#if 1
  return (void *) PhysMem::Alloc(sz, McMemHi, 4);
#else
  return 0;
#endif
}

void *
operator new [](size_t sz)
{
  MsgLog::fatal("Inappropriate call to non-placement operator vector new\n");
#if 1
  return (void *) PhysMem::Alloc(sz, McMemHi, 4);
#else
  return 0;
#endif
}
