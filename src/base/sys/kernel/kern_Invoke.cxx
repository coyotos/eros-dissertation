/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Driver for key invocation

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Debug.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/util.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/Depend.hxx>
#include <kerninc/StackTester.hxx>
#include <eros/SysTraceKey.h>
#include <machine/Process.hxx>
#include <eros/Invoke.h>
#ifdef DDB
#include <eros/StdKeyType.h>
#endif

#include <kerninc/Machine.hxx>
#if 0
#include <machine/PTE.hxx>
#endif

// #define GATEDEBUG 5
// #define KPRDEBUG

// #define TESTING_INVOCATION

#ifdef TESTING_INVOCATION
const dbg_wild_ptr = 1;
#endif

#ifdef DDB
bool ddb_inv_message = false;
bool ddb_gate_message = false;
bool ddb_return_message = false;
bool ddb_keeper_message = false;
bool ddb_keyerr_message = false;
#endif

#define  KEYTYPE(x)  void x##Key (Invocation& msg)
KEYTYPE(Number);
KEYTYPE(Gate);
KEYTYPE(DataPage);
KEYTYPE(CapPage);
KEYTYPE(Node);
KEYTYPE(Segment);
KEYTYPE(Sense);
KEYTYPE(Process);
KEYTYPE(Sched);
KEYTYPE(Range);
KEYTYPE(Device);
KEYTYPE(Misc);

static void
UnknownKey(Invocation& inv)
{
  inv.key->Print();
  MsgLog::fatal("Invocation of unknown primary key type 0x%x\n",
		inv.key->GetType());
}

static void
UnimplementedKey(Invocation& inv)
{
  MsgLog::fatal("Invocation of unimplemented primary key type 0x%x\n",
	     inv.key->GetType());
}

KeyHandler keyHandler[PRIMARY_KEY_TYPES] = {
  GateKey,			// 0 == KtStart
  GateKey,			// 1 == KtResume
  DataPageKey,			// 2 == KtDataPage
  CapPageKey,			// 3 == KtCapPage
  NodeKey,			// 4 == KtNode
  SegmentKey,			// 5 == KtSegment
  ProcessKey,			// 6 == KtProcess
  DeviceKey,			// 7 == KtDevice
  NumberKey,			// 8 == KtNumber
  UnimplementedKey,		// 9 == KtTimer
  SchedKey,			// 10 == KtSched
  RangeKey,			// 11 == KtRange
  MiscKey,			// 12 == KtMisc
  UnknownKey,			// 13 (unassigned)
  UnknownKey,			// 14 (unassigned)
  UnknownKey,			// 15 (unassigned)

  UnknownKey,			// 16 (unassigned)
  UnknownKey,			// 17 (unassigned)
  UnknownKey,			// 18 (unassigned)
  UnknownKey,			// 19 (unassigned)
  UnknownKey,			// 20 (unassigned)
  UnknownKey,			// 21 (unassigned)
  UnknownKey,			// 22 (unassigned)
  UnknownKey,			// 23 (unassigned)
  UnknownKey,			// 24 (unassigned)
  UnknownKey,			// 25 (unassigned)
  UnknownKey,			// 26 (unassigned)
  UnknownKey,			// 27 (unassigned)
  UnknownKey,			// 28 (unassigned)
  UnknownKey,			// 29 (unassigned)
  UnknownKey,			// 30 (unassigned)
  UnknownKey,			// 31 (unassigned)
};

#if defined(KERN_TIMING_STATS)
uint64_t Invocation::KeyHandlerCycles[PRIMARY_KEY_TYPES][3];
uint64_t Invocation::KeyHandlerCounts[PRIMARY_KEY_TYPES][3];
#endif

#if defined(KERN_TIMING_STATS)
void
Invocation::ZeroStats()
{
  for (int i = 0; i < PRIMARY_KEY_TYPES; i++) {
    KeyHandlerCycles[i][IT_Call] = 0;
    KeyHandlerCycles[i][IT_Reply] = 0;
    KeyHandlerCycles[i][IT_Send] = 0;
    KeyHandlerCounts[i][IT_Call] = 0;
    KeyHandlerCounts[i][IT_Reply] = 0;
    KeyHandlerCounts[i][IT_Send] = 0;
  }
}
#endif

void
Invocation::BootInit()
{
#if defined(KERN_TIMING_STATS)
  ZeroStats();
#endif
}

// KEY INVOCATION
//
// Logic in Principle: Entry, Action, Exit
//
// Entry: 
//
//    1. Validate that the invocation is well-formed, and that the
//       data argument if any is present and validly mapped.  
//
//    2. Validate all of the information associated with the
//       invocation to a place of safety against the possibility that
//       entry and exit arguments overlap.
//
//    3. Determine who we will be exiting to (the invokee) -- this is
//       often the invoker.  It can also be the domain named by the
//       invoked gate key.  It can also be the domain named by the key
//       in slot 4 if this is a kernel key (YUCK).  It can also be
//       NULL if the key in slot 4 proves not to be a resume key.
//
//       Invoked      Invocation    Slot 4     Invokee
//       Key Type     Type          Key Type
//
//       Gate Key     ANY           *          Named by gate key
//       Kernel Key   CALL          *          Same as invoker
//       Kernel Key   FORK,RETURN   Resume     Named by resume key
//       Kernel Key   FORK,RETURN   !Resume    NONE
//
//       Note that the case of CALL on a kernel key is in some sense
//       the same as FORK/RETURN on a kernel key -- the resume key was
//       generated by the call.  I may in fact implement the first
//       version this way.
//
//    4. Determine if the invokee will receive a resume key to the
//       invoker.  (yes if the invocation is a CALL).
//
//    5. Determine if a resume key will be consumed by the invocation
//       (yes if the invokee was named by a resume key, including the
//       case of CALL on kernel key).
//
//    6. Determine if the invokee is in the proper state:
//
//       Invoked      Invocation    Invokee
//       Key Type     Type          State
//
//       Start Key    ANY           AVAILABLE
//       Resume Key   ANY           WAITING
//       Kernel Key   CALL          RUNNING (really waiting per note above)
//       Kernel Key   FORK,RETURN   WAITING
//
//    7. Determine if a new thread is to be created.
//
//    8. Construct a receive buffer mapping, splicing in the
//       kernel-reserved void page where no mapping is present.
//
//    NONE OF THE ABOVE CAN MODIFY THE PROGRAM as the action may put
//    the program to sleep, causing us to need to restart the entire
//    invocation.
//
// Action:
//    Whatever the key is specified to do.  This may require stalling
//    the application.  The interesting case is the gate key
//    implementation.  Given that the above actions have already been
//    performed, the gate key action
//
// Exit:
//    If a resume key is to be copied
//      If already consumed copy a null key
//      else construct resume key
//    Migrate the outbound thread to the invokee.
//
//
// NOTE DELICATE ISSUE
//
//   Red seg invocation is the only place where a resume key can
//   become prepared without it's containing object being dirty (the
//   other case -- key registers -- guarantees that the object is
//   already dirty.  Preparing a resume key without guaranteeing dirty
//   object causes resume key rescind to violate the checkpoint
//   constraint by creating a situation in which the resume key may
//   get turned into DK(0) before the containing node is stabilized.
//   The code here is careful to check -- if the keeper gate key is a
//   resume key, we mark the containing node dirty.
//
//   In practice, if you use a resume key in a keeper slot you are a
//   bogon anyway, so I'm not real worried about it...

Invocation inv;
  
#ifndef NDEBUG
bool InvocationCommitted = false;
#endif
  
void
Invocation::Commit()
{
  MaybeDecommit();
  
#ifndef NDEBUG
  InvocationCommitted = true;
#endif

  if (Thread::Current()->context) {
    // This is bogus, because this gets called from GateKey(), which
    // can in turn be called from InvokeMyKeeper, within which
    // CurContext is decidedly NOT runnable.
    // assert ( Thread::CurContext()->IsRunnable() );
    ((Process *) Thread::Current()->context)->SetPC(nextPC);
  }
}

bool
Invocation::IsInvocationKey(const Key *pKey)
{
  /* because this gets set up early in the keeper invocation
   * path, /inv.key/ may not be set yet, so check this early.
   */
  if (pKey == &redNodeKey)
    return true;
  
  if (key == 0)
    return false;
  
  if (pKey == key)
    return true;
  
#if 0
  if (pKey == &resumeKey)
    return true;
#endif
  
  if (pKey == &scratchKey)
    return true;
  
#if 0
  if (pKey == &exit.key[0] ||
      pKey == &exit.key[1] ||
      pKey == &exit.key[2] ||
      pKey == &exit.key[3])
    return true;
#endif
    
  return false;
}

Invocation::Invocation()
: entry(), exit()
{
  entry.key[0] = 0;
  entry.key[1] = 0;
  entry.key[2] = 0;
  entry.key[3] = 0;

  exit.len = 0;
  exit.w1 = 0;
  exit.w2 = 0;
  exit.w3 = 0;
  // exit.code is always set explicitly, so don't bother.

  key = 0;

#ifndef NDEBUG
  InvocationCommitted = false;
#endif

  invokee = 0;
  flags = 0;
}

void
Invocation::Cleanup()
{
  key = 0;
  
#ifndef NDEBUG
  InvocationCommitted = false;
#endif
  
  // exit register values are set up in PopulateExitBlock, where they
  // are only done on one path.  Don't change them here.
  
#ifndef NDEBUG
  // Leaving the key pointers dangling causes no harm
  entry.key[0] = 0;
  entry.key[1] = 0;
  entry.key[2] = 0;
  entry.key[3] = 0;
#endif

  /* NH_Unprepare is expensive.  Avoid it if possible: */
  if (flags & INV_SCRATCHKEY)
    scratchKey.NH_ZeroKey();
  if (flags & INV_REDNODEKEY)
    redNodeKey.NH_ZeroKey();
#if 0
  if (flags & INV_RESUMEKEY)
    resumeKey.NH_ZeroKey();
#endif
#if 0
  if (flags & INV_EXITKEY0)
    exit.key[0].NH_ZeroKey();
  if (flags & INV_EXITKEY3)
    exit.key[3].INH_ZeroKey();
  if (flags & INV_EXITKEYOTHER) {
    exit.key[1].NH_ZeroKey();
    exit.key[2].NH_ZeroKey();
  }
#endif
  
  flags = 0;
}

void
Invocation::RetryInvocation()
{
  Cleanup();
  Thread::CurContext()->runState = RS_Running;
  Thread::Current()->Yield();
}

#ifndef NDEBUG
bool
Invocation::IsCorrupted()
{
  for (int i = 0; i < 4; i++) {
    if (entry.key[i])
      return true;
#if 0
    if (exit.key[i].IsPrepared())
      return true;
#endif
  }
  return false;
}
#endif

void
Process::BuildResumeKey(Key& resumeKey)
{
  // Fabricate a prepared resume key.  The mere existence of this key is
  // technically an error, but we will blow it away at the end of the
  // invocation, and nothing we call from here will notice it's
  // presence.

#ifndef NDEBUG
  if (!kr.IsValid(this))
    MsgLog::dprintf(true, "Key ring screwed up\n");
#endif

  /* not hazarded because it is an invocation key */
  resumeKey.NH_Unprepare();
  
  resumeKey.InitType(KtResume);
  resumeKey.SetPrepared();
  resumeKey.subType = KstResume;
  resumeKey.keyData = 0;	// ???
  resumeKey.gk.pContext = this;
  resumeKey.gk.next = &kr;
  resumeKey.gk.prev = kr.prev;
  kr.prev = (KeyRing*) &resumeKey;
  resumeKey.gk.prev->next = (KeyRing *) &resumeKey;
#if 0
  MsgLog::printf("Dumping key ring (context=0x%08x, resumeKey=0x%08x):\n"
		 "kr.prev=0x%08x kr.next=0x%08x\n",
		 this, &resumeKey, kr.prev, kr.next);
  KeyRing      *pkr=kr.prev;
  int		count=0;
  while (pkr!=&kr)
    {
      MsgLog::printf("pkr->prev=0x%08x pkr->next=0x%08x\n",
		     pkr->prev, pkr->next);
      ((Key *)pkr)->Print();
      pkr=pkr->prev;
      count++;
    }
  if (count>1)
    MsgLog::dprintf(true, "Multiple keys in key ring\n");
#endif

#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Crafted resume key\n");
#endif
}

extern "C" {
  uint64_t rdtsc();
};

// This is a carefully coded routine, probably less than clear.  The
// version that handles all of the necessary cases is
// DoGeneralKeyInvocation; this version is trying to cherry pick the
// high flyers.
//
// The common cases are, in approximate order:
//
//  1. CALL   on a gate key
//  2. RETURN on a gate key    (someday: via returner)
//  3. CALL   on a kernel key
//  4. Anything else
//
// This version also assumes that the string arguments are valid, and
// does only the fast version of the string test.

void
Process::DoKeyInvocation()
{
#ifdef KERN_TIMING_STATS
  uint64_t pre_handler;
  uint64_t top_time = rdtsc();
#ifdef KERN_EVENT_TRACING
  uint64_t top_cnt0 = Machine::ReadCounter(0);
  uint64_t top_cnt1 = Machine::ReadCounter(1);
#endif
#endif

  KernStats.nInvoke++;

#ifndef NDEBUG
  InvocationCommitted = false;
#endif

  inv.nextPC = CalcPostInvocationPC();  // in case we must restart
  AdjustInvocationPC();

  inv.suppressXfer = false;  // suppress compiler bitching
  
  // If preparation causes a depend entry to get zapped, it may be US
  // that gets zapped.  Set up to check for that...
  PteZapped = false;
  ObjectHeader::BeginTransaction();
  
  SetupEntryBlock(inv);

#ifdef DDB
  if ( ddb_inv_message || ddb_gate_message || ddb_keeper_message)
    goto general_path;
#endif

  // Send is a pain in the butt.  Fuck 'em if they can't take a joke.
  if (inv.invType == IT_Send)
    goto general_path;

  // If it's a segment key, it might be a red segment key.  Take the
  // long way:
  if (inv.keyType == KtSegment)
    goto general_path;

  // Handle the return via returner case efficiently:
  if (inv.key->subType == MiscKeyType::Returner && inv.keyType == KtMisc) {
    inv.key = inv.entry.key[3];
    inv.keyType = inv.key->GetType();
    if (inv.keyType != KtResume) {
      /* NOTE: this also captures key being unprepared!!  */
      goto general_path;
    }
    inv.entry.key[3] = &Key::ZeroNumberKey;
  }
    
  if (inv.keyType <= KtResume) {
    inv.invokee = inv.key->gk.pContext;
    if (inv.invokee->IsWellFormed() == false)
      goto general_path;

    // If this is a resume key, we know the process is in the
    // RS_Waiting state.  If it is a start key, the process might be
    // in any state.  Do not solve the problem here to avoid loss of
    // I-cache locality in this case; we are going to sleep anyway.
    if (inv.keyType == KtStart && inv.invokee->runState != RS_Available)
      goto general_path;
  }
  else {
    if (inv.invType != IT_Call)
      goto general_path;
    
    inv.invokee = this;
  }
  
#ifdef PURE_ENTRY_STRINGS
  if (inv.entry.len != 0)
    SetupEntryString(inv);
#endif

  if (inv.invokee->IsRunnable() == false)
    goto general_path;
  
  if (inv.keyType == KtResume && inv.key->subType != KstResume)
    inv.suppressXfer = true;

  // Following is a truly disgusting piece of numerology, which is
  // done deliberately:
  assert (IT_Call == RS_Waiting);
  assert (IT_Reply == RS_Available);
  runState = inv.invType;

  // It does not matter if the invokee fails to prepare!
  // Note that we may be calling this with 'this == 0'
  inv.invokee->SetupExitBlock(inv);
#ifdef PURE_EXIT_STRINGS
  if (inv.validLen != 0)
    inv.invokee->SetupExitString(inv, inv.validLen);
#endif

  {
#if defined(DDB) && !defined(NDEBUG)
    bool invoked_gate_key = inv.key->IsGateKey();

#if defined(DBG_WILD_PTR) || defined(TESTING_INVOCATION)
    if (dbg_wild_ptr)
      Check::Consistency("DoKeyInvocation() before invoking handler\n");
#endif

    // suppressXfer only gets set if this was a fault key, in which case 
    // this is likely a re-invocation of the process by the keeper.
    if ( ddb_inv_message ||
	 (ddb_gate_message && invoked_gate_key) ||
	 (ddb_keeper_message && inv.suppressXfer) )
      MsgLog::dprintf(true, "About to invoke key handler (inv.ty=%d) ic=%d\n",
		      inv.keyType, KernStats.nInvoke);
#endif

#if defined(KERN_TIMING_STATS)
    pre_handler = rdtsc();
#endif
    keyHandler[inv.keyType](inv);
#if defined(KERN_TIMING_STATS)
    {
      extern uint32_t inv_delta_reset;
      if (inv_delta_reset == 0) {
	extern uint64_t inv_handler_cy;
	uint64_t post_handler = rdtsc();
	inv_handler_cy += (post_handler - pre_handler);
	Invocation::KeyHandlerCycles[inv.keyType][inv.invType] +=
	  (post_handler - pre_handler);
	Invocation::KeyHandlerCounts[inv.keyType][inv.invType] ++;
      }
    }  
#endif

#if defined(DBG_WILD_PTR) || defined(TESTING_INVOCATION)
    if (dbg_wild_ptr)
      Check::Consistency("DoKeyInvocation() after invoking handler\n");
#endif

    assert (InvocationCommitted);
#ifndef NDEBUG
    InvocationCommitted = false;    
#endif

#if defined(DDB) && !defined(NDEBUG)
    // inv.suppressXfer only gets set if this was a fault key, in which
    // case this is likely a re-invocation of the process by the keeper.
    // FIX: This is no longer true
    if ( ddb_inv_message ||
	 ( ddb_gate_message && invoked_gate_key ) ||
	 ( ddb_keeper_message && inv.suppressXfer) ||
	 ( ddb_return_message && inv.invType == IT_Reply ) ||
	 ( ddb_keyerr_message &&
	   !invoked_gate_key &&
	   ((inv.key->GetType() != KtMisc) ||
	    (inv.key->subType != MiscKeyType::Returner)) &&
	   (inv.exit.code != RC_OK &&
	    inv.entry.code != OC_KeyType &&
	    inv.exit.code != (uint32_t)AKT_Number) ) )
      MsgLog::dprintf(true, "Before DeliverResult() (invokee=0x%08x)\n",
		      inv.invokee); 
#endif
  }

  // Now for the tricky part.  It's possible that the proces did an
  // invocation whose effect was to blow the invokee apart.  I know of
  // no way to speed these checks:

  if (inv.invokee->IsNotRunnable()) {
    if (inv.invokee->procRoot == 0)
      goto invokee_died;

    inv.invokee->Prepare();

    if (inv.invokee->IsNotRunnable())
      goto invokee_died;
  }

  inv.invokee->runState = RS_Running;
  if (!inv.suppressXfer) {
    inv.invokee->DeliverResult(inv);
#if defined(DBG_WILD_PTR) || defined(TESTING_INVOCATION)
    if (dbg_wild_ptr)
      Check::Consistency("DoKeyInvocation() after DeliverResult()\n");
#endif
  }

  if (inv.invokee != this) {
    // Following is only safe because we handle non-call on primary
    // key in the general path.
    if (inv.keyType == KtResume)
      inv.invokee->kr.ZapResumeKeys();

    Thread::Current()->MigrateTo(inv.invokee);
  }
  
  if (runState == RS_Available)
    stallQ.WakeAll();
    
  inv.Cleanup();
  inv.invokee = 0;

#ifdef KERN_TIMING_STATS
  {
    extern uint32_t inv_delta_reset;

    if (inv_delta_reset == 0) {
      extern uint64_t inv_delta_cy;

      uint64_t bot_time = rdtsc();
#ifdef KERN_EVENT_TRACING
      extern uint64_t inv_delta_cnt0;
      extern uint64_t inv_delta_cnt1;

      uint64_t bot_cnt0 = Machine::ReadCounter(0);
      uint64_t bot_cnt1 = Machine::ReadCounter(1);
      inv_delta_cnt0 += (bot_cnt0 - top_cnt0);
      inv_delta_cnt1 += (bot_cnt1 - top_cnt1);
#endif
      inv_delta_cy += (bot_time - top_time);
    }
    else
      inv_delta_reset = 0;
  }
#endif

#if defined(DBG_WILD_PTR) || defined(TESTING_INVOCATION)
  if (dbg_wild_ptr)
    Check::Consistency("bottom DoKeyInvocation()");
#endif
  
  return;

 general_path:
  runState = RS_Running;
  // This path has its own, entirely separate recovery logic....
  DoGeneralKeyInvocation();
  return;
  
 invokee_died:
  inv.invokee = 0;
  inv.Cleanup();
  Thread::Current()->MigrateTo(0);
}

Thread *threadToRelease = 0;
  
void
Process::DoGeneralKeyInvocation()
{
#ifdef KERN_TIMING_STATS
  uint64_t top_time = rdtsc();
  uint64_t pre_handler;
#ifdef KERN_EVENT_TRACING
  uint64_t top_cnt0 = Machine::ReadCounter(0);
  uint64_t top_cnt1 = Machine::ReadCounter(1);
#endif
#endif

  threadToRelease = 0;
  
  // Set up the entry block, faulting in any necessary data pages and
  // constructing an appropriate kernel mapping:
  SetupEntryBlock(inv);

#ifdef PURE_ENTRY_STRINGS
  if (inv.entry.len != 0)
    SetupEntryString(inv);
#endif

#ifdef GATEDEBUG
  MsgLog::dprintf(true, "Populated entry block\n");
#endif

  assert(inv.key->IsPrepared());
  
  // There are two cases where the actual invocation may proceed on a
  // key other than the invoked key:
  //
  //   Invocation of kept red segment key proceeds as invocation on
  //     the keeper, AND observes the slot 2 convention of the format
  //     key!!!   Because this must overwrite slot 2, it must occur
  //     following the entry block preparation.
  //
  //   Gate key to malformed domain proceeds as invocation on DK(0)
  //
  // The red seg test is done first because the extracted gate key (if
  // any) needs to pass the well-formed test too.
  
  if ( inv.key->IsType(KtSegment)
       && inv.key->IsRedSegmentKey()
       && inv.key->IsReadOnly() == false
       && inv.key->IsNoCall() == false
       && inv.key->IsWeak() == false ) {
    //    MsgLog::dprintf(false, "It's a red segment key\n");

    Node *redSegNode = (Node *) inv.key->GetObjectPtr();
    Key& fmtKey = redSegNode->slot[RedSegFormat];

    // Only pass through if red segment is well-formed and has gate
    // key as keeper!
    
    if ( fmtKey.IsType(KtNumber) ) {
      uint32_t kprSlot = REDSEG_GET_KPR_SLOT(fmtKey.nk);

      if ( kprSlot != EROS_NODE_SLOT_MASK
	   && redSegNode->slot[kprSlot].IsGateKey() ) {

	// See if we need to clobber slot 2 with a node key to the red
	// segment.  Need to do this BEFORE we bash inv.key... :-)
	if ( REDSEG_GET_SENDNODE(fmtKey.nk) == REDSEG_SEND_NODE ) {
	  /* Not hazarded because invocation key */
	  inv.scratchKey.NH_Set(*inv.key);
	  inv.scratchKey.SetType(KtNode);
	  inv.entry.key[2] = &inv.scratchKey;
	  inv.flags |= INV_SCRATCHKEY;
	}

	/* Not hazarded because invocation key */
	inv.key = &(redSegNode->slot[kprSlot]);
	inv.keyType = inv.key->GetType();

	// Prepared resume keys can only reside in dirty objects!
	if (inv.keyType == KtResume)
	  redSegNode->MakeObjectDirty();
	    
	inv.key->Prepare();	// MAY YIELD!!!
	inv.entry.w1 = fmtKey.nk.value[1];
      }
      else
	MsgLog::dprintf(true, "Keeper not gate key or no keeper\n");
    }
    else
      MsgLog::dprintf(true, "Format key is bad\n");
  }
  
  assert(inv.key->IsPrepared());

  inv.invokee = 0;		// until proven otherwise
  
  // Right now a corner case here is buggered because we have not yet
  // updated the caller's runstate according to the call type.  As a
  // result, a return on a start key to yourself won't work in this
  // code.
  
  if ( inv.key->IsGateKey() ) {
    assert (inv.key->IsPrepared());
    // Make a local copy (subvert alias analysis pessimism)
    Process *p = inv.key->gk.pContext;
    inv.invokee = p;
    p->Prepare();		// may yield

    // This is now checked in pk_GateKey.cxx
    if (inv.keyType == KtResume && inv.key->subType != KstResume)
      inv.suppressXfer = true;

    if (p->IsWellFormed() == false) {
      /* Not hazarded because invocation key */
      inv.key = &Key::ZeroNumberKey;
      inv.keyType = KtNumber;
      inv.invokee = this;
      inv.entry.key[3] = &Key::ZeroNumberKey;
#ifndef NDEBUG
      MsgLog::printf("Jumpee malformed\n");
#endif
    }
#ifndef NDEBUG
    else if (inv.keyType == KtResume &&
	     p->runState != RS_Waiting) {
      MsgLog::fatal("Resume key to wrong-state context\n");
    }
#endif
    else if (inv.keyType == KtStart && p->runState != RS_Available) {
      Thread::Current()->SleepOn(p->stallQ);
      Thread::Current()->Yield();
    }
#if 0
    else if ( inv.invType == IT_Call ) {
      BuildResumeKey(inv.resumeKey);
      inv.entry.key[3] = &inv.resumeKey;
      inv.flags |= INV_RESUMEKEY;
    }
#endif
  }
  else if (inv.invType == IT_Call) {
    // Call on non-gate key always returns to caller and requires no
    // resume key.
    inv.invokee = this;
    inv.entry.key[3] = &Key::ZeroNumberKey;
  }
  else {
    // Kernel key invoked with REPLY or SEND.  Key in slot 4 must be a
    // resume key, and if so the process must be waiting, else we will
    // not return to anyone.

    Key& rk = *inv.entry.key[3];
    rk.Prepare();

    // Kernel keys return as though via the returner, so the key in
    // slot four must be a resume key to a party in the right state.

    assert(rk.IsHazard() == false);
    
    if (rk.IsPreparedResumeKey()) {
      Process *p = rk.gk.pContext;
      p->Prepare();

      assert(p->runState == RS_Waiting);
      inv.invokee = p;

      // it can, however, be a fault key.  Since we are not going via
      // GateKey(), request xfer suppression here.
      if (rk.subType != KstResume)
	inv.suppressXfer = true;
    }
    else
      assert (inv.invokee == 0);
  }

  // Pointer to the domain root (if any) whose sleepers we should
  // awaken on successful completion:
  Process *wakeRoot = 0;

  // Revise the invoker runState to what it will be when this is all
  // over.  We'll fix it above if we yield.
  static uint8_t newState[3] = {
    RS_Available,		// IT_Reply
    RS_Waiting,			// IT_Call
    RS_Running,			// IT_Send
  };

  runState = newState[inv.invType];
  if (runState == RS_Available)
    // Ensure that we awaken the sleeping threads (if any).
    wakeRoot = this;
  
  ////////////////////////////////////////////////////////////////////////
  // AT THIS POINT we know that the invocation will complete in
  // principle. It is still possible that the invoker will block while
  // some part of the invokee gets paged in or while waiting for an
  // available Thread structure.  The latter is a problem, and needs
  // to be dealt with.  Note that the finiteness of the thread pool
  // isn't the source of the problem -- the real problem is that we
  // might not be able to fit all of the running domains in the swap
  // area. Eventually we shall need to implement a decongester to deal
  // with this, but that can wait.
  ////////////////////////////////////////////////////////////////////////


#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>3, "Checked for well-formed recipient\n");
#endif

  if (inv.invokee && inv.invokee->IsWellFormed() == false)
    inv.invokee = 0;

  assert(inv.key->IsPrepared());
  
#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Invokee now valid\n");
#endif

  assert(inv.invokee == 0 || inv.invokee->IsRunnable());
  
  // It does not matter if the invokee fails to prepare!
  // Note that we may be calling this with 'this == 0'
  inv.invokee->SetupExitBlock(inv);
#ifdef PURE_EXIT_STRINGS
  if (inv.validLen != 0)
    inv.invokee->SetupExitString(inv, inv.validLen);
#endif

#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Populated exit block\n");
#endif
  
#ifdef GATEDEBUG
  if (inv.suppressXfer)
    MsgLog::dprintf(true, "xfer is suppressed\n");
#endif
  
  // Identify the thread that will migrate to the recipient.  Normally
  // it's the current thread.  If this is a FORK invocation, it's a
  // new thread.  Populate this thread consistent with the invokee.
  
  Thread *threadToMigrate = Thread::Current();
  
  if (inv.invType == IT_Send) {
    // This is wrong -- we need a new thread even if it is not a gate
    // key.  Consider send on kernel key passing resume key in slot
    // 3.  For real fun, consider send on domain key saying 'start
    // this process' with resume key in slot 3
    if ( inv.key->IsGateKey() ) {
      threadToMigrate = new Thread;
      threadToRelease = threadToMigrate;
      threadToMigrate->state = Thread::Ready;
#ifdef GATEDEBUG
      MsgLog::dprintf(true, "Built new thread for fork\n");
#endif
    }
    else
      threadToMigrate = 0;
  }
  
#if defined(DDB) && !defined(NDEBUG)
  bool invoked_gate_key = inv.key->IsGateKey();
  
#if defined(DBG_WILD_PTR) || defined(TESTING_INVOCATION)
  if (dbg_wild_ptr)
    Check::Consistency("DoKeyInvocation() before invoking handler\n");
#endif

  // suppressXfer only gets set if this was a fault key, in which case 
  // this is likely a re-invocation of the process by the keeper.
  if ( ddb_inv_message ||
       (ddb_gate_message && invoked_gate_key) ||
       (ddb_keeper_message && inv.suppressXfer) )
    MsgLog::dprintf(true, "About to invoke key handler (inv.ty=%d) ic=%d\n",
		    inv.keyType, KernStats.nInvoke);
#endif

#if defined(KERN_TIMING_STATS)
  pre_handler = rdtsc();
#endif
  keyHandler[inv.keyType](inv);
#if defined(KERN_TIMING_STATS)
  {
    extern uint32_t inv_delta_reset;
    if (inv_delta_reset == 0) {
      extern uint64_t inv_handler_cy;
      uint64_t post_handler = rdtsc();
      inv_handler_cy += (post_handler - pre_handler);
      Invocation::KeyHandlerCycles[inv.keyType][inv.invType] +=
	(post_handler - pre_handler);
      Invocation::KeyHandlerCounts[inv.keyType][inv.invType] ++;
    }
  }  
#endif

#if defined(DBG_WILD_PTR) || defined(TESTING_INVOCATION)
  if (dbg_wild_ptr)
    Check::Consistency("DoKeyInvocation() after invoking handler\n");
#endif

  assert (InvocationCommitted);
#ifndef NDEBUG
  InvocationCommitted = false;    
#endif
  
#if defined(DDB) && !defined(NDEBUG)
  // inv.suppressXfer only gets set if this was a fault key, in which
  // case this is likely a re-invocation of the process by the keeper.
  // FIX: This is no longer true
  if ( ddb_inv_message ||
       ( ddb_gate_message && invoked_gate_key ) ||
       ( ddb_keeper_message && inv.suppressXfer) ||
       ( ddb_return_message && inv.invType == IT_Reply ) ||
       ( ddb_keyerr_message &&
	 !invoked_gate_key &&
	 ((inv.key->GetType() != KtMisc) ||
	  (inv.key->subType != MiscKeyType::Returner)) &&
	 (inv.exit.code != RC_OK &&
	  inv.entry.code != OC_KeyType &&
	  inv.exit.code != (uint32_t)AKT_Number) ) )
    MsgLog::dprintf(true, "Before DeliverResult() (invokee=0x%08x)\n",
		    inv.invokee); 
#endif
  
  // Check the sanity of the receiving process in various ways:
  
  if (inv.invokee) {
    if (inv.invokee->IsNotRunnable()) {
      if (inv.invokee->procRoot == 0) {
	inv.invokee = 0;
	goto bad_invokee;
      }
    
      inv.invokee->Prepare();

      if (inv.invokee->IsNotRunnable()) {
	inv.invokee = 0;
	goto bad_invokee;
      }
    }

    // Invokee is okay.  Deliver the result:
    inv.invokee->runState = RS_Running;
    if (!inv.suppressXfer) {
      inv.invokee->DeliverResult(inv);
#if defined(DBG_WILD_PTR) || defined(TESTING_INVOCATION)
      if (dbg_wild_ptr)
	Check::Consistency("DoKeyInvocation() after DeliverResult()\n");
#endif
    }

    // If we are returning to ourselves, the resume key was never
    // generated.
    if (inv.invokee != this)
      inv.invokee->kr.ZapResumeKeys();
  }
 bad_invokee:
  
  /* ONCE DELIVERRESULT IS CALLED, NONE OF THE INPUT CAPABILITIES
     REMAINS ALIVE!!! */
  
  // Clean up the invocation block:
  inv.Cleanup();
#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Cleaned up invocation\n");
#endif

#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Updated invokee runstate\n");
#endif
  
  if (threadToMigrate) {
    threadToMigrate->MigrateTo(inv.invokee);

    if (inv.invType == IT_Send && inv.invokee) {
      threadToMigrate->Wakeup();
#ifdef GATEDEBUG
      MsgLog::dprintf(true, "Woke up forkee\n");
#endif
    }
  }
  
  if (wakeRoot) {
#if 0
    MsgLog::dprintf(false, "Wake up all of the losers sleeping on dr=0x%08x\n", wakeRoot);
#endif
    wakeRoot->stallQ.WakeAll();
  }
  
#ifdef DBG_WILD_PTR
  {
    extern void ValidateAllThreads();
    ValidateAllThreads();
  }
#endif

#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Migrated the thread\n");
#else
#if 0
  if ( invoked_gate_key )
    MsgLog::dprintf(true, "Migrated the thread\n");
#endif
#endif

  inv.invokee = 0;
  threadToRelease = 0;
  
#ifdef KERN_TIMING_STATS
  {
    extern uint32_t inv_delta_reset;

    if (inv_delta_reset == 0) {
      extern uint64_t inv_delta_cy;

      uint64_t bot_time = rdtsc();
#ifdef KERN_EVENT_TRACING
      extern uint64_t inv_delta_cnt0;
      extern uint64_t inv_delta_cnt1;

      uint64_t bot_cnt0 = Machine::ReadCounter(0);
      uint64_t bot_cnt1 = Machine::ReadCounter(1);
      inv_delta_cnt0 += (bot_cnt0 - top_cnt0);
      inv_delta_cnt1 += (bot_cnt1 - top_cnt1);
#endif
      inv_delta_cy += (bot_time - top_time);
    }
    else
      inv_delta_reset = 0;
  }
#endif
}

#if 0
void
Process::DoKeyInvocation()
{
#ifdef INVOKE_TIMING
  uint64_t top_time = rdtsc();
#if 0
  uint64_t top_cnt0 = Machine::ReadCounter(0);
  uint64_t top_cnt1 = Machine::ReadCounter(1);
#endif
#endif

  KernStats.nInvoke++;

  inv.suppressXfer = false;  // suppress compiler bitching
  
  // If preparation causes a depend entry to get zapped, it may be US
  // that gets zapped.  Set up to check for that...
  PteZapped = false;
  ObjectHeader::BeginTransaction();
  
#ifndef NDEBUG
  assert (InvocationCommitted == false);
  InvocationCommitted = false;
  assert (inv.IsCorrupted() == false);
#endif
  inv.nextPC = CalcPostInvocationPC();  // in case we must restart
  AdjustInvocationPC();

#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Reset gate PC\n");
#endif
  
  threadToRelease = 0;
  
#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>3, "Built recovery block\n");
#endif

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Begin DoKeyInvocation()");
#endif
  
  // Set up the entry block, faulting in any necessary data pages and
  // constructing an appropriate kernel mapping:
  
  SetupEntryBlock(inv);

#ifdef GATEDEBUG
  MsgLog::dprintf(true, "Populated entry block\n");
#endif

  assert(inv.key->IsPrepared());
  
  // There are two cases where the actual invocation may proceed on a
  // key other than the invoked key:
  //
  //   Invocation of kept red segment key proceeds as invocation on
  //     the keeper, AND observes the slot 2 convention of the format
  //     key!!!   Because this must overwrite slot 2, it must occur
  //     following the entry block preparation.
  //
  //   Gate key to malformed domain proceeds as invocation on DK(0)
  //
  // The red seg test is done first because the extracted gate key (if
  // any) needs to pass the well-formed test too.
  
  if ( inv.key->IsType(KtSegment)
       && inv.key->IsRedSegmentKey()
       && inv.key->IsReadOnly() == false
       && inv.key->IsNoCall() == false
       && inv.key->IsWeak() == false ) {
    //    MsgLog::dprintf(false, "It's a red segment key\n");

    Node *redSegNode = (Node *) inv.key->GetObjectPtr();
    Key& fmtKey = redSegNode->slot[RedSegFormat];

    // Only pass through if red segment is well-formed and has gate
    // key as keeper!
    
    if ( fmtKey.IsType(KtNumber) ) {
      uint32_t kprSlot = REDSEG_GET_KPR_SLOT(fmtKey.nk);

      if ( kprSlot != EROS_NODE_SLOT_MASK
	   && redSegNode->slot[kprSlot].IsGateKey() ) {

	// See if we need to clobber slot 2 with a node key to the red
	// segment.  Need to do this BEFORE we bash inv.key... :-)
	if ( REDSEG_GET_SENDNODE(fmtKey.nk) == REDSEG_SEND_NODE ) {
	  /* Not hazarded because invocation key */
	  inv.redScratchKey.NH_Set(*inv.key);
	  inv.redScratchKey.SetType(KtNode);
	  inv.entry.key[2] = &inv.redScratchKey;
	  inv.flags |= INV_REDSEGKEY;
	}

	/* Not hazarded because invocation key */
	inv.key = &(redSegNode->slot[kprSlot]);
	inv.keyType = inv.key->GetType();

	// Prepared resume keys can only reside in dirty objects!
	if (inv.keyType == KtResume)
	  redSegNode->MakeObjectDirty();
	    
	inv.key->Prepare();	// MAY YIELD!!!
	inv.entry.w1 = fmtKey.nk.value[1];
	inv.entry.w2 = fmtKey.nk.value[2];
      }
      else
	MsgLog::dprintf(true, "Keeper not gate key or no keeper\n");
    }
    else
      MsgLog::dprintf(true, "Format key is bad\n");
  }
  
  assert(inv.key->IsPrepared());

  inv.invokee = 0;		// until proven otherwise
  
  // Right now a corner case here is buggered because we have not yet
  // updated the caller's runstate according to the call type.  As a
  // result, a return on a start key to yourself won't work in this
  // code.
  
  if ( inv.key->IsGateKey() ) {
    assert (inv.key->IsPrepared());
    // Make a local copy (subvert alias analysis pessimism)
    Process *p = inv.key->gk.pContext;
    inv.invokee = p;
    p->Prepare();		// may yield

#if 0
    // This is now checked in pk_GateKey.cxx
    if (inv.keyType == KtResume && inv.key->subType != KstResume)
      inv.suppressXfer = true;
#endif

    if (p->IsWellFormed() == false) {
      /* Not hazarded because invocation key */
      inv.key = &Key::ZeroNumberKey;
      inv.keyType = KtNumber;
      inv.invokee = this;
      inv.entry.key[3] = &Key::ZeroNumberKey;
#ifndef NDEBUG
      MsgLog::printf("Jumpee malformed\n");
#endif
    }
#ifndef NDEBUG
    else if (inv.keyType == KtResume &&
	     p->runState != RS_Waiting) {
      MsgLog::fatal("Resume key to wrong-state context\n");
    }
#endif
    else if (inv.keyType == KtStart &&
	     p->runState != RS_Available) {
      Thread::Current()->SleepOn(p->stallQ);
      Thread::Current()->Yield();
    }
#if 0
    else if ( inv.invType == IT_Call ) {
      BuildResumeKey(inv.resumeKey);
      inv.entry.key[3] = &inv.resumeKey;
      inv.flags |= INV_RESUMEKEY;
    }
#endif
  }
  else if (inv.invType == IT_Call) {
    // Call on non-gate key always returns to caller and requires no
    // resume key.
    inv.invokee = this;
    inv.entry.key[3] = &Key::ZeroNumberKey;
  }
  else {
    // Kernel key invoked with REPLY or SEND.  Key in slot 4 must be a
    // resume key, and if so the process must be waiting, else we will
    // not return to anyone.

    Key& rk = *inv.entry.key[3];
    rk.Prepare();

    // Kernel keys return as though via the returner, so the key in
    // slot four must be a resume key to a party in the right state.

    if (rk.ktByte == KtResume) {
      Process *p = rk.gk.pContext;
      p->Prepare();

      assert(p->runState == RS_Waiting);
      inv.invokee = p;

      // it can, however, be a fault key.  Since we are not going via
      // GateKey(), request xfer suppression here.
      if (rk.subType != KstResume)
	inv.suppressXfer = true;
    }
  }

  // Pointer to the domain root (if any) whose sleepers we should
  // awaken on successful completion:
  Process *wakeRoot = 0;

  // Revise the invoker runState to what it will be when this is all
  // over.  We'll fix it above if we yield.
  static newState[3] = {
    RS_Available,		// IT_Reply
    RS_Waiting,			// IT_Call
    RS_Running,			// IT_Send
  };

  runState = newState[inv.invType];
  if (runState == RS_Available)
    // Ensure that we awaken the sleeping threads (if any).
    wakeRoot = this;
  
  ////////////////////////////////////////////////////////////////////////
  // AT THIS POINT we know that the invocation will complete in
  // principle. It is still possible that the invoker will block while
  // some part of the invokee gets paged in or while waiting for an
  // available Thread structure.  The latter is a problem, and needs
  // to be dealt with.  Note that the finiteness of the thread pool
  // isn't the source of the problem -- the real problem is that we
  // might not be able to fit all of the running domains in the swap
  // area. Eventually we shall need to implement a decongester to deal
  // with this, but that can wait.
  ////////////////////////////////////////////////////////////////////////


#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>3, "Checked for well-formed recipient\n");
#endif

  if (inv.invokee && inv.invokee->IsWellFormed() == false)
    inv.invokee = 0;

  assert(inv.key->IsPrepared());
  
#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Invokee now valid\n");
#endif

  assert(inv.invokee == 0 || inv.invokee->IsRunnable());
  
  // It does not matter if the invokee fails to prepare!
  // Note that we may be calling this with 'this == 0'
  inv.invokee->SetupExitBlock(inv);

#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Populated exit block\n");
#endif
  
#ifdef GATEDEBUG
  if (inv.suppressXfer)
    MsgLog::dprintf(true, "xfer is suppressed\n");
#endif
  
  // Identify the thread that will migrate to the recipient.  Normally
  // it's the current thread.  If this is a FORK invocation, it's a
  // new thread.  Populate this thread consistent with the invokee.
  
  Thread *threadToMigrate = Thread::Current();
  
  if (inv.invType == IT_Send) {
    if ( inv.key->IsGateKey() ) {
      threadToMigrate = new Thread;
      threadToRelease = threadToMigrate;
      threadToMigrate->state = Thread::Ready;
#ifdef GATEDEBUG
      MsgLog::dprintf(true, "Built new thread for fork\n");
#endif
    }
    else
      threadToMigrate = 0;
  }
  
#if defined(DDB) && !defined(NDEBUG)
  bool invoked_gate_key = inv.key->IsGateKey();
  
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("DoKeyInvocation() before invoking handler\n");
#endif
  
  // suppressXfer only gets set if this was a fault key, in which case 
  // this is likely a re-invocation of the process by the keeper.
  if ( ddb_inv_message ||
       (ddb_gate_message && invoked_gate_key) ||
       (ddb_keeper_message && inv.suppressXfer) )
    MsgLog::dprintf(true, "About to invoke key handler (inv.ty=%d)\n", inv.keyType);
#endif

  keyHandler[inv.keyType](inv);

  assert (InvocationCommitted);
#ifndef NDEBUG
  InvocationCommitted = false;    
#endif
  
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("DoKeyInvocation() before invoking handler\n");
#endif
#if defined(DDB) && !defined(NDEBUG)
  // inv.suppressXfer only gets set if this was a fault key, in which
  // case this is likely a re-invocation of the process by the keeper.
  // FIX: This is no longer true
  if ( ddb_inv_message ||
       ( ddb_gate_message && invoked_gate_key ) ||
       ( ddb_keeper_message && inv.suppressXfer) ||
       ( ddb_return_message && inv.invType == IT_Reply ) ||
       ( ddb_keyerr_message &&
	 !invoked_gate_key &&
	 ((inv.key->GetType() != KtMisc) ||
	  (inv.key->subType != MiscKeyType::Returner)) &&
	 (inv.exit.code != RC_OK &&
	  inv.entry.code != OC_KeyType &&
	  inv.exit.code != (uint32_t)AKT_Number) ) )
    MsgLog::dprintf(true, "Before DeliverResult() (invokee=0x%08x)\n",
		    inv.invokee); 
#endif
  
  // Check the sanity of the receiving process in various ways:
  
  if (inv.invokee) {
    if (inv.invokee->IsNotRunnable()) {
      if (inv.invokee->procRoot == 0) {
	inv.invokee = 0;
	goto bad_invokee;
      }
    
      inv.invokee->Prepare();

      if (inv.invokee->IsNotRunnable()) {
	inv.invokee = 0;
	goto bad_invokee;
      }
    }

    // Invokee is okay.  Deliver the result:
    inv.invokee->runState = RS_Running;
    if (!inv.suppressXfer)
      inv.invokee->DeliverResult(inv);

    // If we are returning to ourselves, the resume key was never
    // generated.
    if (inv.invokee != this)
      inv.invokee->kr.ZapResumeKeys();
  }
 bad_invokee:
  
  /* ONCE DELIVERRESULT IS CALLED, NONE OF THE INPUT CAPABILITIES
     REMAINS ALIVE!!! */
  
  // Clean up the invocation block:
  inv.Cleanup();
#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Cleaned up invocation\n");
#endif

#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Updated invokee runstate\n");
#endif
  
  if (threadToMigrate) {
    threadToMigrate->MigrateTo(inv.invokee);

    if (inv.invType == IT_Send && inv.invokee) {
      threadToMigrate->Wakeup();
#ifdef GATEDEBUG
      MsgLog::dprintf(true, "Woke up forkee\n");
#endif
    }
  }
  
  if (wakeRoot) {
#if 0
    MsgLog::dprintf(false, "Wake up all of the losers sleeping on dr=0x%08x\n", wakeRoot);
#endif
    wakeRoot->stallQ.WakeAll();
  }
  
#ifdef DBG_WILD_PTR
  {
    extern void ValidateAllThreads();
    ValidateAllThreads();
  }
#endif

#ifdef GATEDEBUG
  MsgLog::dprintf(GATEDEBUG>2, "Migrated the thread\n");
#else
#if 0
  if ( invoked_gate_key )
    MsgLog::dprintf(true, "Migrated the thread\n");
#endif
#endif

  inv.invokee = 0;
  threadToRelease = 0;
  
#ifdef INVOKE_TIMING
  {
    extern uint32_t inv_delta_reset;

    if (inv_delta_reset == 0) {
      extern uint64_t inv_delta_cy;

      uint64_t bot_time = rdtsc();
#if 0
      extern uint64_t inv_delta_cnt0;
      extern uint64_t inv_delta_cnt1;

      uint64_t bot_cnt0 = Machine::ReadCounter(0);
      uint64_t bot_cnt1 = Machine::ReadCounter(1);
      inv_delta_cnt0 += (bot_cnt0 - top_cnt0);
      inv_delta_cnt1 += (bot_cnt1 - top_cnt1);
#endif
      inv_delta_cy += (bot_time - top_time);
    }
    else
      inv_delta_reset = 0;
  }
#endif
}
#endif

// KEEPER INVOCATION -- this looks a lot like key invocation, and the
// code for the two should probably be merged.  The difficulty is that
// the keeper invocation code is able to make a variety of useful
// assumptions about abandonment that the general path cannot make.
void
Process::InvokeMyKeeper(uint32_t oc,
			uint32_t warg1,
			uint32_t warg2,
			uint32_t warg3,
			Key *keeperKey,
			Key* keyArg2, uint8_t *data, uint32_t len)
{
#ifdef KERN_TIMING_STATS
  uint64_t top_time = rdtsc();
#ifdef KERN_EVENT_TRACING
  uint64_t top_cnt0 = Machine::ReadCounter(0);
  uint64_t top_cnt1 = Machine::ReadCounter(1);
#endif
#endif

  inv.suppressXfer = false;
  KernStats.nInvoke++;
  KernStats.nInvKpr++;
  
#ifdef KPRDEBUG
  MsgLog::dprintf(true, "Enter InvokeMyKeeper\n");
#endif
  bool canInvoke = true;
  
  // If preparation causes a depend entry to get zapped, it may be US
  // that gets zapped.  Set up to check for that...
  PteZapped = false;
  
  // Do not call BeginTransaction here -- the only way we can be here
  // is if we are already in some transaction, and it's okay to let
  // that transaction prevail.
  
  keeperKey->Prepare();

  if (keeperKey->IsGateKey() == false)
    canInvoke = false;
  
#ifdef KPRDEBUG
  MsgLog::dprintf(true, "Kpr key is gate key? '%c'\n", canInvoke ? 'y' : 'n');
#endif

  // A recovery context has already been established, either by a call
  // to PageFault handler or by Thread::Resched().

#ifndef NDEBUG
  InvocationCommitted = false;
#endif
  /* Not hazarded because invocation key */
  inv.key = keeperKey;
  inv.invType = IT_Call;
  inv.keyType = keeperKey->GetType();
  inv.nextPC = GetPC();

#ifdef KPRDEBUG
  MsgLog::dprintf(true, "Populated invocation block\n");
#endif

  if (keeperKey->IsType(KtResume) && keeperKey->subType != KstResume) {
    inv.suppressXfer = true;
    MsgLog::printf("Suppress xfer -- key is restart key\n");
  }

  Process * const invokee = canInvoke ? keeperKey->gk.pContext : 0;

  if (invokee) {
    inv.invokee = invokee;
    
#ifdef KPRDEBUG
    MsgLog::dprintf(true, "Prepare invokee\n");
#endif

    // Make sure invokee is valid
    invokee->Prepare();
  
#ifdef KPRDEBUG
    MsgLog::dprintf(true, "Invokee prepare completed\n");
#endif

    if ( invokee->IsWellFormed() == false )
      canInvoke = false;

#ifdef KPRDEBUG
    MsgLog::dprintf(true, "Have good keeper, ctxt=0x%08x\n", invokee);
#endif

#ifndef NDEBUG
    if (keeperKey->IsType(KtResume) && invokee->runState != RS_Waiting)
      // Bad resume key!
      canInvoke = false;
#endif

    if (keeperKey->IsType(KtStart) && invokee->runState != RS_Available) {
      Thread::Current()->SleepOn(invokee->stallQ);
      Thread::Current()->Yield();
    }

    if (keeperKey->IsType(KtResume) && keeperKey->subType != KstResume)
      inv.suppressXfer = true;
  }

  if (canInvoke) {
#ifdef KPRDEBUG
    MsgLog::dprintf(true, "Keeper in right state\n");
#endif

    // Now have valid invokee in proper state.  Build exit block:
    invokee->SetupExitBlock(inv);
    if (inv.validLen != 0)
      invokee->SetupExitString(inv, len);

#ifdef KPRDEBUG
    MsgLog::dprintf(true, "Populated exit block\n");
#endif

    COMMIT_POINT();
    
    runState = RS_Waiting;
  
    assert (InvocationCommitted);

    if (!inv.suppressXfer) {
      inv.CopyOut(len, data);
    
      // This is weird, because we are using DeliverGateResult():
      
      inv.entry.code = oc;
      inv.entry.w1 = warg1;
      inv.entry.w2 = warg2;
      inv.entry.w3 = warg3;

      inv.entry.key[0] = &Key::ZeroNumberKey;
      inv.entry.key[1] = &Key::ZeroNumberKey;
      inv.entry.key[2] = keyArg2;
      inv.entry.key[3] = &Key::ZeroNumberKey;
      
      invokee->DeliverGateResult(inv, true);
    
#if defined(DBG_WILD_PTR) || defined(TESTING_INVOCATION)
      if (dbg_wild_ptr)
	Check::Consistency("DoKeyInvocation() after invoking keeper\n");
#endif
    }
    
#ifndef NDEBUG
    InvocationCommitted = false;    
#endif

    invokee->runState = RS_Running;

    if (keeperKey->IsType(KtResume))
      invokee->kr.ZapResumeKeys();

    // Clean up the invocation block:
    inv.Cleanup();

#ifdef GATEDEBUG
    MsgLog::dprintf(GATEDEBUG>2, "Cleaned up invocation\n");
#endif

    Thread::Current()->MigrateTo(invokee);

#ifdef KPRDEBUG
    MsgLog::dprintf(true, "Thread %s has migrated\n", Thread::Current()->Name());
#endif
    inv.invokee = 0;
  }
  else {
#ifndef NDEBUG
    OID oid = Thread::CurContext()->procRoot->ob.oid;
    MsgLog::printf("No keeper for OID 0x%08x%08x, FC %d FINFO 0x%08x\n",
		   (uint32_t) (oid >> 32),
		   (uint32_t) oid,
		   ((Process *)Thread::CurContext())->faultCode, 
		   ((Process *)Thread::CurContext())->faultInfo);
    MsgLog::dprintf(true,"Dead context was 0x%08x\n", Thread::CurContext());
#endif
    // Just retire the thread, leaving the domain in the running
    // state.

    Thread::Current()->MigrateTo(0);
#if 0
    Thread::Current()->SleepOn(procRoot->ObjectSleepQueue());
    Thread::Current()->Yield();
#endif
  }

#ifdef KERN_TIMING_STATS
  {
    extern uint64_t kpr_delta_cy;

    uint64_t bot_time = rdtsc();
#ifdef KERN_EVENT_TRACING
    extern uint64_t kpr_delta_cnt0;
    extern uint64_t kpr_delta_cnt1;
    
    uint64_t bot_cnt0 = Machine::ReadCounter(0);
    uint64_t bot_cnt1 = Machine::ReadCounter(1);
    kpr_delta_cnt0 += (bot_cnt0 - top_cnt0);
    kpr_delta_cnt1 += (bot_cnt1 - top_cnt1);
#endif
    kpr_delta_cy += (bot_time - top_time);
  }
#endif
}
