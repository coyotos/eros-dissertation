/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/SysTimer.hxx>
#include <kerninc/Task.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/IRQ.hxx>

TaskVec Task::activeTasks;
static TaskVec allocatedTasks;
Task Task::TaskTable[Task::MaxTask];

void *
Task::operator new(size_t)
{
  for (uint32_t tsk = 0; tsk < MaxTask; tsk++) {
    TaskVec tskbit = (1 << tsk);

    if ((allocatedTasks & tskbit)  == 0) {
      allocatedTasks |= tskbit;
      return &TaskTable[tsk];
    }
  }

  MsgLog::fatal("Task table exhausted\n");

  return 0;
}

void
Task::operator delete(void *vtp)
{
  Task * tp = (Task *) vtp;
  uint32_t tsk = tp - TaskTable;

  allocatedTasks &= ~(1 << tsk);
}

void
Task::RunActiveTasks()
{
  // The reason that this needs to sit in an infamous loop is that
  // running a task may cause other tasks to be enabled, and
  // interrupts may arrive that enable tasks while we are within the
  // loop.
    
  while (activeTasks) {
    for (uint32_t tsk = 0; tsk < MaxTask; tsk++) {
      TaskVec tskbit = (1u << tsk);

      if (activeTasks & tskbit) {
	activeTasks &= ~tskbit;
	TaskTable[tsk].fn(&TaskTable[tsk]);
      }
    }
  }
}

