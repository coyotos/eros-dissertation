/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// main.cxx - initialization and startup for EROS.
//
// One of the design rules for EROS was that no global class should
// require construction.  It is assumed that any classes that have
// global instances will supply an init() routine that can be directly
// called from the EROS main() routine.  On entry to main(),
// interrupts are disabled.

// currently, no arguments to the EROS kernel:

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Console.hxx>
// #include "lostart.hxx"
#include <kerninc/Machine.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/util.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/Checkpoint.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/Debug.hxx>
#include <kerninc/BlockDev.hxx>
#include <kerninc/Partition.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/Invocation.hxx>
#include <disk/DiskKey.hxx>

extern "C" {
    int main(int);
    extern uint32_t StartupStack;
    extern uint32_t StartupStackTop;
}

void main_thread_entry();

// Need an initial reserve setup to keep main() running past first
// interrupt: 
KernThread InitialThread("Initial",
			 main_thread_entry,
			 &StartupStack, &StartupStackTop);

int
main(int /* isboot */)
{
  // Initialize the stuff we don't go anywhere without:
  Thread::curThread = &InitialThread;
  CpuReserve::Current = &CpuReserve::KernThreadCpuReserve;
  CpuReserve::Current->AddKernelThread(&InitialThread);
  Thread::curThread->state = Thread::Running;

  Machine::BootInit();

  SysTimer::BootInit();
  
  Invocation::BootInit();
  
#ifdef DDB
  extern void ddb_init();
  ddb_init();
#endif

#if 0
  Debugger();
#endif

  IRQ::DISABLE();
  
  InitialThread.context->Resume();
}

void
main_thread_entry()
{
  MsgLog::printf("Continuing from main thread...\n");
	
#if 0
  Debugger();
#endif

  // Set up the standard system tasks:
  BlockDev::Init();
  
  // Initialize the EROS internal data structures that don't
  // want to come out of nodes and pages:

#if 0
  IoRequest::Init();
  UnitIoReq::Init();

  MsgLog::printf("Request blocks configured...\n");

#endif
  
  // Configure the machine's hardware:
  AutoConf::ArchConfigure();
  AutoConf::Configure();
  
  MsgLog::printf("After AutoConfigure()\n");
  
  // Allocate all of the boot-time allocated structures:
  CpuReserve::AllocUserCpuReserves();

  Process::AllocUserContexts(); // machine dependent!

  Thread::AllocUserThreads();

  extern void AllocReturnerBuffer();
  AllocReturnerBuffer();
  
  // Initialize the in-core nodes and pages, and the CoreTable:
  ObjectCache::Init();
  
  MsgLog::printf("Object cache initialized...\n");

  MsgLog::printf("main() thread is 0x%x\n", &InitialThread);

#if 0
  MsgLog::dprintf(true, "main() thread: about to call RunMountDaemon...\n");
#endif

  // Following should be done before any possibility of a time key
  // getting prepared:
  SysTimer::InitTimePage();

  // FROM THIS POINT ON IT IS LEGAL TO PERFORM I/O AND TO ENQUEUE THE
  // CURRENT THREAD
  
  // Mount all of the units we can find:
  BlockDev::RunMountDaemon();

#if 0
  MsgLog::dprintf(true, "Mount daemon ran to completion\n");
#endif

  // halt();
  
  Checkpoint::Init();

  MsgLog::printf("Checkpoint area inited OK\n");
#if 0
  Debugger();
#endif

  Partition::MountAll();
  MsgLog::printf("Partition::MountAll() returns\n");
#if 0
  Debugger();
#endif
  
  Checkpoint::LoadDirectory();
  MsgLog::printf("Checkpoint directory is loaded\n");
#if 0
  Debugger();
#endif

  // At this point, all of the ranges we are going to automount have
  // been mounted.  See if they include any checkpoint log ranges, in
  // which case we should read the thread list from the checkpoint
  // log:
  if ( Persist::HaveCkptLog() ) {
    Checkpoint::StartThreads();
  }
  else if ( Partition::ValidBootstrapOid ) {
    Thread* startThread = new Thread();
    CpuReserve::CpuReserveTable[Prio::Normal].AddUserThread(startThread);
    assert(startThread);

    // Forge a domain key for this thread:
    startThread->processKey.NH_Set(Partition::BootstrapKey);
    assert( startThread->processKey.IsHazard() == false );
    
    // Prepare the domain root key to verify that we will be able to
    // start this domain:
    
    if (startThread->processKey.Prepare(KtProcess) == false)
      MsgLog::fatal("Unable to prepare root node of startup thread\n");

    assert( startThread->processKey.IsPrepared() );

    // Explicit preparation won't hurt any, and will let us debug the
    // prepare logic.
    // startThread->Prepare();
    
    startThread->Wakeup();
  }
  else
    MsgLog::fatal("Unable to identify bootstrap thread.\n");
  
#if 1
  MsgLog::printf(
       "\n"
       "- The rapidly moving twiddle is caused by the idle thread, which\n"
       "  is in an infinite loop calling Yield().\n"
       "- The slower twiddle is driven by the twiddler every 250ms.\n"
       "- The timer is obvious.\n"
       "- The slowest twiddle is me ticking every 10 seconds or so.\n");

  MsgLog::printf("\nYour cpu type is %d (%s)\n",
		 Machine::GetCpuType(),
		 Machine::GetCpuVendor());

  uint64_t startTick = SysTimer::Now();
  
  uint64_t ms = 0ll;
  uint32_t tick = 0;

  for(;;) {
    ms += 10000ll;
    tick++;
    
    uint64_t wakeTick = startTick + Machine::MillisecondsToTicks(ms);
    IRQ::DISABLE();
    Thread::Current()->WakeUpAtTick(wakeTick);
    Thread::Current()->SleepOn(IdlePile);
    IRQ::ENABLE();
    Thread::Current()->DirectedYield();
    // MsgLog::printf("StartupThread ticks %d\n", tick);
    Console::Twiddle(3);
  }

#endif
  
  // We will never wake up again...
  Thread::Current()->SleepOn(IdlePile);
  Thread::Current()->Yield();
  
  
  pause();
  
  /* NOTREACHED */
  
  // At this point, there are no threads running, and we are running
  // off of the kernel interrupt stack.  We now set those threads in
  // motion in no particular order.  Most of them will quickly go to
  // sleep after doing some brief initializaeion.  The one we care
  // about particularly is the mount daemon, which will mount all of
  // the disks and set all threads in motion.

  // In the early test code, the only thread that actually gets woken
  // up here is the wall clock thread.

  MsgLog::printf("Somehow halted\n");

  halt('m');
  
  MsgLog::printf("main(): exit\n");
}
