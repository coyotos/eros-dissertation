/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Console.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/SysTimer.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/util.hxx>

static bool Probe(AutoConf *);
static bool Attach(AutoConf *);
struct Driver ac_twiddler = {
  "twiddler", Probe, Attach
} ;

// Twiddler does not handle any interrupts, but will eventually have
// an associated user key:

//const uint32_t StackSize = sizeof(KernSaveArea) / 4 + 3;
const uint32_t StackSize = 1024;
static uint32_t TwStack[StackSize];

class Twiddler : public KernThread {
public:
  static void Start();

  Twiddler()
    :KernThread("Twiddler",
		&Twiddler::Start,
		TwStack, &TwStack[StackSize])
  {
    procContext.InitStack();
  }
};

static Twiddler TheTwiddler;

// The twiddler is always present.
static bool
Probe(AutoConf*)
{
  CpuReserve::KernThreadCpuReserve.AddKernelThread(&TheTwiddler);
  return true;
}

static bool
Attach(AutoConf* ac)
{
  TheTwiddler.Wakeup();	// let it initialize itself...
  
  MsgLog::printf("%s (no vector)\n", ac->name);
  return true;
}

// There is a minor piece of trickery here.  We don't want to use
// relative sleeps, because we would slowly lose clock accuracy.
// Instead, snapshot the current tick at startup and explicitly
// request a particular millisecond for startup.
void Twiddler::Start()
{
  int stack;
  MsgLog::printf("Start Twiddler (thread 0x%x,context 0x%x,stack 0x%x)\n",
	       &TheTwiddler, TheTwiddler.context, &stack);

  assert(IRQ::INTERRUPTS_ENABLED());

  uint64_t startTick = SysTimer::Now();

  uint64_t ms = 0ll;

  for(;;) {
    ms += 250ll;
    Console::Twiddle(4);
    
  assert(IRQ::INTERRUPTS_ENABLED());
    uint64_t wakeTick = startTick + Machine::MillisecondsToTicks(ms);
    IRQ::DISABLE();
    TheTwiddler.WakeUpAtTick(wakeTick);
    TheTwiddler.SleepOn(IdlePile);
    IRQ::ENABLE();
    assert(IRQ::INTERRUPTS_ENABLED());
    TheTwiddler.DirectedYield();
  }
}
