/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Provides diagnostics management.  Kernel diagnostics go to a ring
// buffer.  The kernel writes diagnostics to the ring buffer, simply
// appending each to the end of the buffer.  Whatever agent has agreed
// to display such diagnostics reads the ring buffer, attempting to
// keep up with the kernel.
//
// The display agent registers a callback with the Diag manager.  When
// the kernel writes into the buffer it invokes this callback.  The
// callback must be prompt.  If going to the console, the display
// manager may simply drive the characters through the console tty
// driver immediately.  If going to a window, the display manager may
// simply wish to schedule the appropriate domain to be run and
// return. This runs the risk of kernel diagnostics being "lost", but
// we know of no way to avoid this without doing dynamic allocation,
// which seems like the worse of the two evils.
//
// There is also a kernel panic mechanism.  The kernel panic mechanism
// unconditionally trashes the console bitmap and displays on the
// console, halting the machine without an automatic reboot.  This is
// to ensure that the diagnostic is noticed.  This does not cause
// problems for checkpoint, as if we call kpanic, we aren't coming
// back, so no checkpoint will wrongfully copy the state in the
// display memory.

#include <kerninc/kernel.hxx>
#include <kerninc/Debug.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/util.hxx>
#include <kerninc/IRQ.hxx>
     
#define DIAG_BUF_PAGES	1u

bool MsgLog::SuppressDebug = false;

static char diagbuf[DIAG_BUF_PAGES * EROS_PAGE_SIZE];

static char *const logbuf = diagbuf;
static char *nextin = diagbuf;
static char *nextout = diagbuf;
static char *const logtop = diagbuf + (DIAG_BUF_PAGES * EROS_PAGE_SIZE);

static void (*output_char)(uint8_t);

static void do_putc(uint8_t);
static void doprint(const char *, void *);
#if 0
static void puthex(uint64_t, uint32_t digits); // for OID's and allocation counts
#endif



extern "C" {
  void Stall(uint32_t);
}

static void
Flush()
{
  if (output_char == 0)
    return;

  while (nextout != nextin) {
    // Wrap if we need to.
    if (nextout == logtop)
      nextout = logbuf;

    output_char(*nextout++);
  }

  Stall(100000);		// let me read!
  // Stall(4000000);		// let me read!
}

static void
do_putc(uint8_t c)
{
  if ((unsigned)(nextin - logbuf) > (DIAG_BUF_PAGES * EROS_PAGE_SIZE))
    halt('p');
  
  if (nextin == logtop)
    nextin = logbuf;
  
  *nextin++ = c;
}

void
MsgLog::RegisterSink(void (*fun)(uint8_t))
{
  output_char = fun;

  Flush();
}

static void
putc(uint8_t c)
{
  if (IsPrint(c) || c == '\t')
    do_putc(c);
  else if (c == '\n') {
    do_putc('\r');
    do_putc('\n');
  }
  else {
    do_putc('\\');
    do_putc('x');
    do_putc(hexdigit((c >> 4) & 0xfu));
    do_putc(hexdigit(c & 0xfu));
  }
}

void
MsgLog::nlputc(char c)
{
  if (c == '\n')
    do_putc('\r');

  do_putc(c);

  Flush();
}

void
MsgLog::putc(char c)
{
  ::putc(c);
  Flush();
}

void
MsgLog::putbuf(uint8_t *s, uint32_t len)
{
  for (uint32_t i = 0; i < len; i++) {
    uint8_t c = s[i];
    ::putc(c);
  }
  
  Flush();
}

void
MsgLog::print(OID oid)
{
  MsgLog::printf("0x%04x%08x", (uint32_t) (oid >> 32), (uint32_t) oid);
}

void
MsgLog::print(ObCount count)
{
  MsgLog::printf("0x%08x", count);
}

// This is a VERY simplified version of printf.  It does not handle
// any fancy justification or formatting.  All hex values are printed
// as 8 digits, leading zeros, all upcase.  No octal for now.
//
// Further, this version of doprint is, shall we say, somewhat
// dangerous.  It depends on the fact that the compiler does not make
// va_list a builtin type, which a true ANSI compiler is permitted to do.
//
static void
doprint(const char *fmt, void *vap)
{
  va_list ap = (va_list) vap;
  bool rightAdjust = true;
  char fillchar = ' ';
  uint32_t width = 0;
  char sign = 0;
  char buf[20];
  char *pend = &buf[20];
  char *p = pend;

  if (fmt == 0) {		// bogus input specifier
    ::putc('?');
    ::putc('f');
    ::putc('m');
    ::putc('t');
    ::putc('?');

#if 0
    Stall(100);			// let things settle.
#endif
    Flush();
    halt('a');
  }

  for( ; *fmt; fmt++) {
    if (*fmt != '%') {
      ::putc(*fmt);
      continue;
    }

    fmt++;

    // check for left adjust..
    rightAdjust = true;
    if (*fmt == '-') {
      rightAdjust = false;
      fmt++;
    }
      
    fillchar = ' ';
    
    // we just saw a format character.  See if what follows
    // is a width specifier:
    width = 0;

    if (*fmt == '0')
      fillchar = '0';

    while (*fmt && *fmt >= '0' && *fmt <= '9') {
      width *= 10;
      width += (*fmt - '0');
      fmt++;
    }
    
    if (*fmt == 0) {		// bogus input specifier
      ::putc('%');
      ::putc('N');
      ::putc('?');
#if 0
      Stall(100);			// let things settle.
#endif
      Flush();
      halt('b');
    }
    
    sign = 0;
    
    // largest thing we might convert fits in 20 digits (unsigned long
    // long as decimal)
    pend = &buf[20];
    p = pend;
    
    switch (*fmt) {
    default:
      {
	// If we cannot interpret, we cannot go on
	::putc('%');
	::putc('?');
#if 0
	Stall(100);			// let things settle.
#endif
	Flush();
	halt('c');
      }
    case 'c':
      {
	long c;
	c = va_arg(ap, long);
	*(--p) = (char) c;
	break;
      }	    
    case 'd':
      {
	long l;
	unsigned long ul;

	l = va_arg(ap, long);
	      
	if (l == 0) {
	  *(--p) = '0';
	}
	else {
	  if (l < 0)
	    sign = '-';

	  ul = (l < 0) ? (unsigned) -l : (unsigned) l;

	  if (l == TARGET_LONG_MIN)
	    ul = ((unsigned long) TARGET_LONG_MAX) + 1ul;

	  while(ul) {
	    *(--p) = '0' + (ul % 10);
	    ul = ul / 10;
	  }
	}
	break;
      }
    case 'u':
      {
	unsigned long ul;

	ul = va_arg(ap, unsigned long);
	      
	if (ul == 0) {
	  *(--p) = '0';
	}
	else {
	  while(ul) {
	    *(--p) = '0' + (ul % 10);
	    ul = ul / 10;
	  }
	}
	break;
      }
    case 'U':
      {
	unsigned long long ull;

	ull = va_arg(ap, unsigned long long);
	      
	if (ull == 0) {
	  *(--p) = '0';
	}
	else {
	  while(ull) {
	    *(--p) = '0' + (ull % 10);
	    ull = ull / 10;
	  }
	}
	break;
      }
    case 't':
      {		// for 2-digit time values
	long l;

	l = va_arg(ap, long);
	      
	*(--p) = (l / 10) + '0';
	*(--p) = (l % 10) + '0';
	break;
      }
    case 'x':
      {
	unsigned long ul;
	    
	ul = va_arg(ap, unsigned long);
	      
	if (ul == 0) {
	  *(--p) = '0';
	}
	else {
	  while(ul) {
	    *(--p) = hexdigit(ul & 0xfu);
	    ul = ul / 16;
	  }
	}

	break;
      }
    case 'X':
      {
	unsigned long long ull;
	    
	ull = va_arg(ap, unsigned long long);
	      
	if (ull == 0) {
	  *(--p) = '0';
	}
	else {
	  while(ull) {
	    *(--p) = hexdigit(ull & 0xfu);
	    ull = ull / 16;
	  }
	}

	break;
      }
    case 's':
      {
	p = pend = va_arg(ap, char *);
	      
	while (*pend)
	  pend++;
	break;
      }
    case '%':
      {
	*(--p) = '%';
	break;
      }
    }

    uint32_t len = pend - p;
    if (sign)
      len++;

    // do padding with initial spaces for right justification:
    if (width && rightAdjust && len < width) {
      while (len < width) {
	::putc(fillchar);
	width--;
      }
    }

    if (sign)
      ::putc('-');
    
    // output the text
    while (p != pend)
      ::putc(*p++);
    
    // do padding with initial spaces for left justification:
    if (width && rightAdjust == false && len < width) {
      while (len < width) {
	::putc(fillchar);
	width--;
      }
    }
  }

  Flush();
}

void
MsgLog::printf(const char * fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
    
  IRQ::DISABLE();
    
  doprint(fmt, ap);
    
  // If calls to printf come too fast, we get spurious interrupts on
  // IRQ7 due to the timer not being serviced.  I have no idea how
  // that comes about, but simply letting the callback be invoked by
  // the timer seems to solve it.

  IRQ::ENABLE();

  va_end(ap);

#if 0
  Stall(100);			// let things settle.
  //  Stall(4000000);		// let me read!
#endif
}

void
MsgLog::dprintf(bool shouldStop, const char * fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
    
  Flush();
  
  IRQ::DISABLE();
    
  doprint(fmt, ap);
    
  // If calls to printf come too fast, we get spurious interrupts on
  // IRQ7 due to the timer not being serviced.  I have no idea how
  // that comes about, but simply letting the callback be invoked by
  // the timer seems to solve it.

  IRQ::ENABLE();

  va_end(ap);

#if 0
  Stall(100);			// let things settle.
  //  Stall(4000000);		// let me read!
#endif
  if(shouldStop && !SuppressDebug)
    Debugger();
}

void
MsgLog::fatal(const char * fmt, ...) 
{
  va_list ap;

  va_start(ap, fmt);
    
  Flush();
  
  // We ignore the error code in kernel printing!
  IRQ::DISABLE();
    
  doprint(fmt, ap);
    
  IRQ::ENABLE();

  va_end(ap);

#if 0
  Stall(200);	// let things settle
#endif

#ifdef DDB
  Debugger();
#else
  Debug::Backtrace(0, false);
  
  // This routine will never return. I personally guarantee it!
  Flush();
  halt('d');
#endif
}

extern "C" void __pure_virtual(void);
void
__pure_virtual(void)
{
  MsgLog::fatal("pure virtual function called\n");
}
