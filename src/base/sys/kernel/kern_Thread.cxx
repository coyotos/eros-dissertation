/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <eros/Key.h>

#include <kerninc/Thread.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/SysTimer.hxx>
#include <machine/KernTune.hxx>
#include <kerninc/util.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/PhysMem.hxx>

const int SCHED_QUANTA = 10;

#define PREEMPTION
// #define THREADDEBUG

const char *Thread::stateNames[Thread::NUM_STATES] = {
    "Free",
    "Ready",
    "Running",
    "Stalled",
    "IoCompleted",
};

#if 0
// These definitions were moved to arch/XXX/kernel/IPC-vars.cxx for cache
// line locality
Thread* Thread::curThread;
bool Thread::threadShouldYield;
bool Thread::cannotPreempt;

jmp_buf Thread::ThreadRecoveryBlock;
#endif

Thread *Thread::ThreadTable;
Thread *Thread::NextFreeThread;

#if 0
static ThreadPile freeList;
#endif

ThreadPile ProcessorQueue;
ThreadPile IdlePile;
uint32_t LowestRunningPriority;

// This constructor used when fabricating new threads in IT_Send.
// Reserve field will get populated when thread migrates to receiving
// context. 
Thread::Thread()
{
  context = 0;
  errCode = 0;
  ioCount = 0;
  state = Thread::Stall;
  nRun = 0;
  cpuReserve = 0;
}

// Called only for kernel threads
Thread::Thread(Process* pContext)
{
  context = pContext;
  errCode = 0;
  ioCount = 0;
  state = Thread::Stall;
  nRun = 0;
  cpuReserve = 0;
  // CAREFUL -- reserve cannot be defined here, because of constructor
  // order!!!
}

// Note -- at one point I tried to make all threads have domain keys,
// and inoperative threads have domain keys with bad values.  This
// DOES NOT WORK, because if the root of the process is rescinded with
// a prepared key in an outstanding thread that key will get zeroed.
// Yuck!.
void
Thread::AllocUserThreads()
{
  // Must NOT use the overloaded new() operator!!!!!
  ThreadTable = new (0) Thread[KTUNE_NTHREAD];
  NextFreeThread = 0;

  for (int i = 0; i < KTUNE_NTHREAD; i++) {
    Thread *t = &ThreadTable[i];

    t->state = Thread::Free;
    t->processKey.IKS_ZeroKey();
    
    delete t;
  }

  MsgLog::printf("Allocated User Threads: 0x%x at 0x%08x\n",
		 sizeof(Thread[KTUNE_NTHREAD]), ThreadTable);
}

void *
Thread::operator new(size_t /* sz */)
{
  if (NextFreeThread) {
    Thread *t = NextFreeThread;
    NextFreeThread = (Thread *) NextFreeThread->next;
    t->next = 0;

    return t;
  }
  
  MsgLog::fatal("Threads exhausted\n");
  return 0;
}

void
Thread::operator delete(void *v)
{
  Thread *t = (Thread *) v;
  // MsgLog::dprintf(true, "Deleting thread 0x%08x\n", t);
  /* not hazarded because thread key */
  t->processKey.NH_ZeroKey();
  t->reserveLinkage.Unlink();
  t->state = Thread::Free;
  if (curThread == t) {
#if 0
    MsgLog::printf("curthread 0x%08x is deleted\n", t);
#endif
    curThread = 0;
  }

  // This must be done after the unlink, since we are using the field
  // in a way that the Link structure doesn't know about.
  t->next = NextFreeThread;
  NextFreeThread = t;
}

#ifndef NDEBUG
bool
Thread::ValidThreadKey(const Key* pKey)
{
  int i;
  for (i = 0; i < KTUNE_NTHREAD; i++) {
    if ( &ThreadTable[i].processKey == pKey )
      return true;
  }

  return false;
}
#endif

void
Thread::SleepOn(ThreadPile& p)
{
  IRQ::DISABLE();

#if defined(DBG_WILD_PTR)
  if (dbg_wild_ptr && 0)
    Check::Consistency("In Thread::SleepOn()");
#endif

  if (state != Running && prev)
    MsgLog::fatal("Thread 0x%08x (%s) REqueues q=0x%08x lastq=0x%08x state=%d\n",
		  this, Name(), &p, lastq, state);

  Thread::DO_NOT_PREEMPT();

  lastq = &p;
  
  assert(next == 0);
  assert(prev == 0);

  Link* ql = &p;

  next = ql->next;
  prev = ql;
  ql->next = this;
  if (next)
    next->prev = this;

  state = Thread::Stall;

#ifdef DDB
  {
    extern bool ddb_thread_uqueue_debug;
    if (IsUser() && ddb_thread_uqueue_debug)
      MsgLog::dprintf(true, "Thread 0x%08x sleeps on queue 0x%08x\n",
		      this, &p);
  }
#endif
  IRQ::ENABLE();
}

void
Thread::Delay(uint64_t ms)
{
  assert(curThread == this);
  IRQ::DISABLE();
  WakeUpIn(ms);
  SleepOn(IdlePile);
  IRQ::ENABLE();
  Yield();
}

// Threads only use the timer system when they are about to yield,
// sleep, so do not preempt them once they set a timer.
void
Thread::WakeUpIn(uint64_t ms)
{
  Thread::DO_NOT_PREEMPT();

  assert(Thread::state == Thread::Running);

  wakeTime = SysTimer::Now() + Machine::MillisecondsToTicks(ms);

  assert(Thread::state == Thread::Running);

  SysTimer::AddSleeper(*this);

  assert(Thread::state == Thread::Running);
}

void
Thread::WakeUpAtTick(uint64_t tick)
{
  Thread::DO_NOT_PREEMPT();

  wakeTime = tick;
  SysTimer::AddSleeper(*this);
}

void
Thread::Wakeup()
{
  IRQ::DISABLE();
  
  int priority = cpuReserve->curPrio;
  
  Unlink();
  if (wakeTime) {
    assert ( state != Running );
    SysTimer::CancelAlarm(*this);
    wakeTime = 0;
  }
  

  Link* ql = &ProcessorQueue;

  Thread *nextThread = (Thread *) ql->next;
  while (nextThread && nextThread->cpuReserve->curPrio >= priority) {
#ifdef DDB
    {
      extern bool ddb_thread_uqueue_debug;
      if (IsUser() && ddb_thread_uqueue_debug)
	MsgLog::dprintf(true, "Thread 0x%08x sleeps on queue 0x%08x (ProcessorQueue)\n",
			this, &ProcessorQueue);
    }
#endif
    ql = (Link *) nextThread;
    nextThread = (Thread *) ql->next;
  }

  // Either there are no more threads, or we should be inserted
  // after ql:

  next = ql->next;
  prev = ql;
  ql->next = this;
  if (next)
    next->prev = this;

  state = Thread::Ready;
  lastq = &ProcessorQueue;

  // Do not set threadShouldYield until the first thread runs!
  // There may not be a curthread if we are being woken up because
  // the previous thread died.
  // assert(curThread);
  if (curThread && cpuReserve->curPrio > curThread->cpuReserve->curPrio) {
#if 0
    MsgLog::printf("Wake 0x%08x Cur thread should yield. canPreempt=%c\n",
		   this, canPreempt ? 'y' : 'n');
#endif
    curThread->Preempt();
  }

  IRQ::ENABLE();

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("In Thread::Wakeup()");
#endif
}


#if 0
extern "C" {
  void resume_from_savearea(SaveArea*);
}
#endif

void
Thread::Expired(Timer*)
{
  Preempt();
  
#if 0
  if (!CAN_PREEMPT()) {
    MsgLog::printf("0x%08x Quanta expires in NOPREEMPT, state %d.\n",
		   curThread, curThread->state); 
  }
#endif
}

#ifdef PREEMPTION
// static Timer QuantaTimer;
#endif

inline void
Thread::ChooseNewCurrentThread()
{
  assert ( ProcessorQueue.IsEmpty() == false );
    
  assert( IRQ::DISABLE_DEPTH() == 1 );

  IRQ::DISABLE();

  curThread = (Thread*) ProcessorQueue.next;

  assert(curThread);
    
  curThread->Unlink();
  curThread->state = Thread::Running;

  IRQ::ENABLE();

  assert( IRQ::DISABLE_DEPTH() == 1 );
}



// DoReschedule() is called for a number of reasons:
//
//     1. No current thread
//     2. Current thread preempted
//     3. Current thread not prepared
//     4. Current thread's context not prepared
//     5. Current thread has fault code (keeper invocation needed)
//
// In the old logic, only thread prepare could Yield().  In the new
// logic, the keeper invocation may also yield.  For this reason, both
// the prepare logic and the keeper invocation logic are in separate
// functions for now.  I am seriously contemplating integrating them
// into the main code body and setting up a recovery block here.

// FIX: Somewhere in here the context pins are not getting updated
// correctly.  It's not important until we do SMP.
void
Thread::DoReschedule()
{
  assert( IRQ::DISABLE_DEPTH() == 1 );

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr && 0)
    Check::Consistency("In DoReschedule()");
#endif

  // DoReschedule may be called from the timer interrupt when kernel
  // interrupts are enabled.  If so, suppress if we are in a
  // no-preempt state, as in this event the thread will shortly yield
  // in any case:
  
  if (yieldState == (ys_ShouldYield | ys_NoPreempt)) {
    MsgLog::printf("0x%08x Preempt suppressed, state=%d\n",
		   curThread, curThread->state);
    return;
  }

  // On the way out of an invocation trap there may be no current
  // thread, in which case we may need to choose a new one:
  if (curThread == 0) {
    ChooseNewCurrentThread();
    yieldState = 0;
  }

  if (yieldState == ys_ShouldYield) {
    // Current thread may be stalled or dead; if so, don't stick it
    // back on the run list!
    if ( curThread->state == Thread::Running ) {
      curThread->Wakeup();	// perverse, but correct.
#ifdef THREADDEBUG
      if ( curThread->IsUser() )
	MsgLog::printf("Active thread goes to end of run queue\n");
#endif
    }

    yieldState = 0;
    ChooseNewCurrentThread();
  }
  
  do {
    ObjectHeader::BeginTransaction();
    
#ifdef DBG_WILD_PTR
    if (dbg_wild_ptr && 0)
      Check::Consistency("In DoReschedule() loop");
#endif
#if 0
    MsgLog::printf("schedloop: curThread 0x%08x ctxt 0x%08x fc 0x%x\n",
		   curThread, curThread->context,
		   curThread->context ? curThread->context->faultCode
		   : 0);
#endif

    assert (curThread);
    
    // If thread cannot be successfully prepared, it cannot (ever) run,
    // and should be returned to the free thread list.  Do this even if
    // we are rescheduling, since we want the thread entry back promptly
    // and it doesn't take that long to test.
    if ( curThread && curThread->Prepare() == false ) {
      assert( curThread->IsUser() );
    
      // We shouldn't be having this happen YET
      MsgLog::fatal("Current thread no longer runnable\n");

      delete curThread;
      assert (curThread == 0);
      ChooseNewCurrentThread();
    }

    assert (curThread);

    // Thread might have gone to sleep as a result of context being prepared.
    if (curThread->state != Thread::Running)
      ChooseNewCurrentThread();

    if (curThread->context
	&& curThread->context->processFlags & PF_Faulted) 
      curThread->InvokeMyKeeper();

    // Thread can go away as a consequence of keeper invocation.
    if (curThread == 0 || curThread->state != Thread::Running)
      ChooseNewCurrentThread();

    assert (curThread);

#if 0
    static count = 0;
    count++;
    if (count % 100 == 0) {
      count = 0;
      MsgLog::printf("schedloop: curThread 0x%08x ctxt 0x%08x (%s) run?"
		     " %c st %d fc 0x%x\n",
		     curThread,
		     curThread->context,
		     curThread->context->Name(),
		     curThread->context->IsRunnable() ? 'y' : 'n',
		     curThread->state,
		     curThread->context->faultCode);
    }
#endif

  } while (curThread->IsRunnable() == false);

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Bottom DoReschedule()");
#endif

  assert (curThread);

  assert (curThread->context);

  CpuReserve *cpuReserve = curThread->cpuReserve;
  assert (cpuReserve == CurContext()->cpuReserve);

#ifdef PREEMPTION
  // MsgLog::printf("Disarming expiration timer\n");
  //       QuantaTimer.Disarm();
  assert( IRQ::DISABLE_DEPTH() == 1 );
  yieldState = 0;		// until proven otherwise

  // MsgLog::printf("Setting expiration timer\n");
  //      QuantaTimer.WakeupIn(SCHED_QUANTA, Expired);

  // SysTimer::SetQuanta(Machine::MillisecondsToTicks(SCHED_QUANTA));
  //      MsgLog::dprintf(true,"Set quanta to %d\n", (uint32_t) (cpuReserve->residQuanta));
  CpuReserve::Current = cpuReserve;

  assert( IRQ::DISABLE_DEPTH() == 1 );
  // MsgLog::printf("It's set...\n");
#endif

  assert(Thread::Current() == curThread);
  // Thread::Current()->SetStackLimit();
  Thread::Current()->nRun++;
}

void
ThreadPile::WakeNext()
{
  IRQ::DISABLE();

  if (next)
    ((struct Thread *) next)->Wakeup();

  IRQ::ENABLE();
}

void
ThreadPile::WakeAll(uint32_t errCode, bool /* verbose */)
{
  IRQ::DISABLE();

  while (next) {
#ifdef DDB
    Thread *thread = (struct Thread *) next;
    
    {
	extern bool ddb_thread_uqueue_debug;
	if (ddb_thread_uqueue_debug && thread->IsUser() )
	  MsgLog::dprintf(true, "Waking up thread 0x%08x (%s)\n",
			  thread, thread->Name());
    }
#endif

#if 0
    if (verbose)
      MsgLog::printf("Waking up thread 0x%08x (%s)\n",
		     next, ((struct Thread *) next)->Name());
#endif

    ((struct Thread *) next)->errCode = errCode;
    ((struct Thread *) next)->Wakeup();
  }

  IRQ::ENABLE();
}

void
ThreadPile::IoWakeAll(uint32_t errCode, bool /* verbose */)
{
  IRQ::DISABLE();

  while (next) {
#ifdef DDB
    Thread *thread = (struct Thread *) next;
    
    {
	extern bool ddb_thread_uqueue_debug;
	if (ddb_thread_uqueue_debug && thread->IsUser() )
	  MsgLog::dprintf(true, "Waking up thread 0x%08x (%s)\n",
			  thread, thread->Name());
    }
#endif

#if 0
    if (verbose)
      MsgLog::printf("Waking up thread 0x%08x (%s)\n",
		     next, ((struct Thread *) next)->Name());
#endif

    ((struct Thread *) next)->errCode = errCode;
    ((struct Thread *) next)->state = Thread::IoCompleted;
    ((struct Thread *) next)->Wakeup();
  }

  IRQ::ENABLE();
}

bool
ThreadPile::IsEmpty()
{
  bool result = false;
  
  IRQ::DISABLE();

  if (next == 0)
    result = true;
  
  IRQ::ENABLE();

  return result;
}

#ifndef NDEBUG
void
ValidateAllThreads()
{
  for (int i = 0; i < KTUNE_NTHREAD; i++) {
    if ( Thread::ThreadTable[i].state != Thread::Free )
      Thread::ThreadTable[i].ValidateUserThread();
  }
}

void
Thread::ValidateUserThread()
{
  uint32_t wthis = (uint32_t) this;
  uint32_t wtbl = (uint32_t) ThreadTable;
  
  if (wthis < wtbl)
    MsgLog::fatal("Thread 'this' pointer too low\n");

  wthis -= wtbl;

  if (wthis % sizeof(Thread))
    MsgLog::fatal("Thread 'this' pointer not valid.\n");

  if ((wthis / sizeof(Thread)) >= KTUNE_NTHREAD)
    MsgLog::fatal("Thread 'this' pointer too high.\n");

  if (!context && processKey.GetType() != KtProcess  &&
      processKey.GetType() != KtNumber)
    MsgLog::fatal("Thread 0x%08x has bad key type.\n", this);
}
#endif

const char*
Thread::Name()
{
  if ( !IsUser() )
    return context->Name();
  
  static char * userThreadName = "userXXX\0";
  uint32_t ndx = this - &ThreadTable[0];

  if (ndx >= 100) {
    userThreadName[4] = (ndx / 100) + '0';
    ndx = ndx % 100;
    userThreadName[5] = (ndx / 10) + '0';
    ndx = ndx % 10;
    userThreadName[6] = ndx + '0';
    userThreadName[7] = 0;
  }
  else if (ndx >= 10) {
    userThreadName[4] = (ndx / 10) + '0';
    ndx = ndx % 10;
    userThreadName[5] = ndx + '0';
    userThreadName[6] = 0;
  }
  else {
    userThreadName[4] = ndx + '0';
    userThreadName[5] = 0;
  }

  return userThreadName;
}


// ALERT
//
// There are four places to which a user thread Yield() can return:
//
//    Thread::Prepare() -- called from the scheduler
//    Thread::InvokeMyKeeper() -- called from the scheduler
//    The page fault handler
//    The gate jump path.
//
// We would prefer a design in which there was only one recovery
// point, but if Thread::Prepare() is being called, it is possible
// likely that the thread's Context entry is gone, and even possible
// that the domain root has gone out of memory.
//
// Note that a thread's prepare routine must always be called by that
// thread, and not by any other.  Threads prepare themselves.
// Contexts, by contrast, can be prepared by anyone.
//
// That said, here's the good news: it is not possible for the uses to
// conflict.  Any action taken as a result of a gate jump that causes
// the thread to Yield() will have put the thread to sleep, in which
// case it's not being prepared.

bool
Thread::Prepare()
{
  static uint32_t count;
  
#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr && 0)
    Check::Consistency("Before ThrdPrepare()");
#endif

  // In the absence of the PTE zap case, which is now handled
  // separately, it is not clear that we still need the for(;;)
  // loop...
  
  for (count = 0; count < 20;count++) {
    // If we have a context, the thread ain't dead yet!
    if ( context && context->IsRunnable() ) {
      assert(context->cpuReserve);
      assert (cpuReserve == context->cpuReserve);
      
      return true;
    }
  
    assert ( Thread::state != Thread::Free );
    
    // Probably not necessary, but it doesn't do any harm:
    assert( Thread::Current() == this );
  
    // Try to prepare this thread to run:
#ifdef THREADDEBUG
    MsgLog::printf("Preparing user thread\n");
#endif
  
    if ( context == 0 ) {
      // Domain root may have been rescinded....

#if 0
      MsgLog::printf("prepping dom key ");
      processKey.Print();
#endif
    
      processKey.Prepare();

      if (processKey.IsType(KtProcess) == false) {
	MsgLog::fatal("Rescinded thread!\n");
	return false;
      }
  
      assert( processKey.IsHazard() == false );
      assert( processKey.IsPrepared() );

      context = ((Node *) processKey.ok.pObj)->GetDomainContext();
      if (!context)
	return false;
    
      context->SetThread(this);
    }

#ifdef THREADDEBUG
    MsgLog::printf("Preparing context 0x%08x..\n", context);
#endif

    context->Prepare();

    if (state == Thread::IoCompleted) {
      state = Thread::Running;
      context->SetPC(context->CalcPostInvocationPC());
    }
    
    if ( context->IsRunnable() ) {
      assert(context->cpuReserve);
      assert (cpuReserve == context->cpuReserve);
#ifdef DBG_WILD_PTR
      if (dbg_wild_ptr && 0)
	Check::Consistency("After ThrdPrepare()");
#endif
      return true;
    }

    // The context could not be prepared.

#ifdef OLD_DORESCHED
    // Either context could not be prepared or the resulting context had
    // a fault of some kind.  Invoke the domain keeper:
  
#if 1
    MsgLog::printf("Invoking domain keeper on Thread 0x%08x pfcount=%d\n",
		   this, ((DomainContext*)context)->pfCount);
#endif

    ((ArchContext *)context)->InvokeDomainKeeper();

#ifdef THREADDEBUG
    MsgLog::dprintf(true, "After kpr invoke, ctxt=0x%08x, svArea=0x%08x\n",
		    context, context->saveArea);
#endif

#if 0
    MsgLog::printf("User thread is prepared!\n");
    // startThread->Wakeup();
#endif
  
    // Invoking the domain keeper either stuck us on a sleep queue, in
    // which case we Yielded() to the setjmp() above, or migrated the
    // current thread to a new domain.  In the latter case, go back to
    // the top of this mess to prepare the new context.
#endif
  }

  MsgLog::dprintf(true, "Thread start loop exceeded\n");
  return false;
}

void
Thread::InvokeMyKeeper()
{
  ((Process *)context)->InvokeProcessKeeper();
    
  // Invoking the domain keeper either stuck us on a sleep queue, in
  // which case we Yielded() to the setjmp() above, or migrated the
  // current thread to a new domain, in which case we will keep
  // trying in the Thread::DoReschedule() loop.
}

void
Thread::Unprepare()
{
  if (context) {
    context->SyncThread();
    context->Unthread();
    context = 0;
    cpuReserve = 0;
  }

  /* not hazarded because thread key */
  processKey.NH_Unprepare();
}
