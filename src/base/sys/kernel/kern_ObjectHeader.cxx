/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/ObjectHeader.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Checkpoint.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Depend.hxx>
#include <kerninc/Persist.hxx>
#include <eros/memory.h>

#define dbg_rescind	0x1	/* steps in taking snapshot */

/* Following should be an OR of some of the above */
#define dbg_flags   (0)

#define DBCOND(x) (dbg_##x & dbg_flags)
#define DEBUG(x) if DBCOND(x)
#define DEBUG2(x,y) if ((dbg_##x|dbg_##y) & dbg_flags)


// If MAX_PIN is changed, may need to check for overflow on user
// thread invocation of Pin().
uint8_t ObjectHeader::CurrentTransaction = 1; // guarantee nonzero!

#ifdef DDB
const char *ObType::ddb_name(uint8_t t)
{
  const char * names[] = {
    "NtUnprep  ",
    "NtSegment ",
    "NtDomRoot ",
    "NtKeyRegs ",
    "NtRegAnnex",
    "NtFreeFrm ",
    "PtDataPage",
    "PtCapPage ",
    "PtAllocPot",
    "PtDrvrPage",
    "PtObjPot  ",
    "PtLogPage ",
    "PtRawPage ",
#ifdef USES_MAPPING_PAGES
    "PtMapPage ",
#endif
    "PtFreeFrm ",
    0
  };

  return names[t];
}
#endif

void
ObjectHeader::KernPin()
{
  assert(kernPin < BYTE_MAX);
  kernPin++;

#ifdef OFLG_PIN
  SetFlags(OFLG_PIN);
#endif
}

void
ObjectHeader::KernUnpin()
{
  assert(kernPin);
  kernPin--;

#ifdef OFLG_PIN
  if (kernPin == 0 && userPin == 0)
    ClearFlags(OFLG_PIN);
#endif
}

// Walk the node looking for an acceptable product:
ObjectHeader *
ObjectHeader::FindProduct(SegWalkInfo& wi, uint32_t ndx, bool rw, bool ca)
{
  uint32_t blss = wi.segBlss;

#if 0
  MsgLog::printf("Search for product blss=%d ndx=%d, rw=%c producerTy=%d\n",
	       blss, ndx, rw ? 'y' : 'n', obType);
#endif
  
  // #define FINDPRODUCT_VERBOSE

  ObjectHeader *product = 0;
  
  for (product = products; product; product = product->next) {
    if ((uint32_t) product->producerBlss != blss) {
#ifdef FINDPRODUCT_VERBOSE
      MsgLog::printf("Producer BLSS not match\n");
#endif
      continue;
    }
    if (product->mp.redSeg != wi.redSeg) {
#ifdef FINDPRODUCT_VERBOSE
      MsgLog::printf("Red seg not match\n");
#endif
      continue;
    }
    if (product->mp.redSeg) {
      if (product->mp.redProducer != wi.segObjIsRed) {
#ifdef FINDPRODUCT_VERBOSE
	MsgLog::printf("redProducer not match\n"); 
#endif
	continue;
      }
      if (product->mp.redSpanBlss != wi.redSpanBlss) {
#ifdef FINDPRODUCT_VERBOSE
	MsgLog::printf("redSpanBlss not match: prod %d wi %d\n",
		       product->mp.redSpanBlss, wi.redSpanBlss);
#endif
	continue;
      }
    }
    if ((uint32_t) product->producerNdx != ndx) {
#ifdef FINDPRODUCT_VERBOSE
      MsgLog::printf("producerNdx not match\n");
#endif
      continue;
    }
    if (product->rwProduct != (rw ? 1 : 0)) {
#ifdef FINDPRODUCT_VERBOSE
      MsgLog::printf("rwProduct not match\n");
#endif
      continue;
    }
    if (product->caProduct != (ca ? 1 : 0)) {
#ifdef FINDPRODUCT_VERBOSE
      MsgLog::printf("caProduct not match\n");
#endif
      continue;
    }

    // WE WIN!
    break;
  }

  if (product) {
    assert(product->producer == this);
    assert(product->obType == ObType::PtMappingPage);

  }

#if 0
  if (wi.segBlss != wi.pSegKey->GetBlss())
    MsgLog::dprintf(true, "Found product 0x%x segBlss %d prodKey 0x%x keyBlss %d\n",
		    product, wi.segBlss, wi.pSegKey, wi.pSegKey->GetBlss());
#endif

#ifdef FINDPRODUCT_VERBOSE
  MsgLog::printf("0x%08x->FindProduct(blss=%d,ndx=%d,rw=%c,ca=%c,"
		 "producerTy=%d) => 0x%08x\n",
		 this,
		 blss, ndx, rw ? 'y' : 'n', ca ? 'y' : 'n', obType,
		 product);
#endif

  return product;
}

void
ObjectHeader::AddProduct(ObjectHeader* product)
{
  product->next = products;
  product->producer = this;
  products = product;
}

void
ObjectHeader::DelProduct(ObjectHeader* product)
{
  assert (product->producer == this);
  
  if (products == product) {
    products = product->next;
  }
  else {
    ObjectHeader *curProd = products;
    while (curProd->next) {
      if (curProd->next == product) {
	curProd->next = product->next;
	break;
      }
      curProd = curProd->next;
    }
  }

  product->next = 0;
  product->producer = 0;
}

#if 0
void
ObjectHeader::DoCopyOnWrite()
{
  assert (obType > ObType::NtLAST_NODE_TYPE);
#if 0
  MsgLog::dprintf(true,
		  "Trying copy on write, ty %d oid 0x%08x%08x "
		  "hdr 0x%08x\n",
		  obType, (uint32_t) (oid >> 32), (uint32_t) (oid), this);
#endif

  assert(GetFlags(OFLG_CKPT) && IsDirty());
  
  ObjectHeader *pObj = ObjectCache::GrabPageFrame();

  assert (pObj->kr.IsEmpty());

  kva_t from = ObjectCache::ObHdrToPage(this);
  kva_t to = ObjectCache::ObHdrToPage(pObj);
    
  // Copy the page data
  bcopy((const void *)from, (void *)to, EROS_PAGE_SIZE);

  // FIX -- the header needs to be copied with care -- perhaps this
  // should be expanded in more explicit form?
  
  // And the object header:
  bcopy(this, pObj, sizeof(ObjectHeader));

  // The key ring needs to be reset following the header bcopy
  pObj->kr.ResetRing();
  
  // Because the original may already be queued up for I/O, the copy
  // has to become the new version.  This poses a problem: we may have
  // gotten here trying to mark an object dirty to satisfy a write
  // fault, in which event there are very likely outstanding prepared
  // capabilities to this object sitting on the stack somewhere.  In
  // all such cases the object being copied will be pinned.  If the
  // object being copied is pinned we Yield(), which will force the
  // whole chain of events to be re-executed, this time arriving at
  // the correct object.
  
  // NOTE About the 'dirty' bit -- which I have 'fixed' twice now to
  // my regret.  It really should be zero.  We are only running this
  // code if the object was marked ckpt.  In that event, the
  // checkpointed version of the object is dirty until flushed, but
  // the COW'd version of the object is not dirty w.r.t the next
  // checkpoint until something happens along to try and dirty it.  We
  // are here because someone is trying to do that, but we must let
  // MakeObjectDirty() handle the marking rather than do it here.  The
  // prolem is that we haven't reserved a directory entry for the
  // object.  This can (and probably should) be resolved by calling
  // RegisterDirtyObject() from here to avoid the extra Yield(), but
  // for now be lazy, since I know that will actually work.
  
  assert(kernPin == 0);
  
  ClearFlags(OFLG_CURRENT);
  pObj->SetFlags(OFLG_CURRENT);
  pObj->ClearFlags(OFLG_CKPT|OFLG_IO|OFLG_DIRTY|OFLG_REDIRTY|OFLG_PIN);
#ifdef DBG_CLEAN
  MsgLog::printf("Object 0x%08x ty %d oid=0x%08x%08x COW copy cleaned\n",
		 pObj, pObj->obType,
		 (uint32_t) (pObj->oid >> 32),
		 (uint32_t) pObj->oid);
#endif
  pObj->SetFlags(GetFlags(OFLG_DISKCAPS));
  pObj->ioCount = 0;
  pObj->userPin = 0;
  pObj->prstPin = 0;
#ifdef OB_MOD_CHECK
  pObj->check = pObj->CalcCheck();
#endif


  // Switch the keyring to the new object, and update all of the keys
  // to point to the copy:

  kr.ObjectMoved(pObj);

  Unintern();			// take us out of the hash chain
  pObj->Intern();		// and put the copy in in our place.
  
  // we must now re-insert the old page as a log page, because the new
  // page might conceivably get aged out before the old page, at which
  // point we would find the wrong one.
  

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Bottom DoCopyOnWrite()");
#endif

  // Since we may have come here by way of the page fault code, we are
  // now forced to Yield(), because there are almost certainly
  // outstanding pointers to this object on the stack:
  Thread::Current()->Yield();
}
#endif

void
ObjectHeader::FlushIfCkpt()
{
  // If this page is involved in checkpointing, run the COW logic.
  if (GetFlags(OFLG_CKPT) && IsDirty()) {
    assert (IsFree() == false);
    if (obType <= ObType::NtLAST_NODE_TYPE) {
      Checkpoint::WriteNodeToLog((Node *) this);
      assert (!IsDirty());
    }
    else  
      Persist::WritePage(this, true);
  }
}

void
ObjectHeader::MakeObjectDirty()
{
  assertex(this, IsPinned());
  assertex(this, GetFlags(OFLG_CURRENT));

  if ( IsDirty() && (GetFlags(OFLG_CKPT|OFLG_IO) == 0) )
    return;
  
#ifndef NDEBUG
  extern bool InvocationCommitted;
#endif
  assert (InvocationCommitted == false);

  if (obType == ObType::PtDataPage ||
      obType == ObType::PtCapPage ||
      obType <= ObType::NtLAST_NODE_TYPE)
    assert(IsPinned());
  
  FlushIfCkpt();
  
  age = Age::NewBorn;
  
#ifdef OB_MOD_CHECK
  if (IsDirty() == 0 && ob.check != CalcCheck())
    MsgLog::fatal("MakeObjectDirty(0x%08x): not dirty and bad checksum!\n",
		  this);
#endif

#if 0
  // This was correct only because we were not reassigning new
  // locations every time an object was dirtied.  Now that we are
  // doing reassignment, we must reregister.
  
  if (IsDirty()) {
    // in case a write is in progress, mark reDirty, but must not do
    // this before registration unless we know the object is already
    // dirty.  Note that we already know this object to be current!
    SetFlags(OFLG_REDIRTY);
    return;
  }
#endif
  
  Checkpoint::RegisterDirtyObject(this);

#if 0
  // FIX: Why should we ever do this here?
  ClearFlags(OFLG_REDIRTY);
#endif

#ifdef DBG_CLEAN
  MsgLog::dprintf(true,
		  "Marked pObj=0x%08x oid=0x%08x%08x dirty. dirty: %c chk: %c\n",
		  this,
		  (uint32_t) (oid >> 32), (uint32_t) (oid),
		  GetFlags(OFLG_DIRTY) ? 'y' : 'n',
		  GetFlags(OFLG_CKPT) ? 'y' : 'n');
#endif

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Top RegisterDirtyObject()");
#endif

#if 0  
  uint32_t ocpl = IRQ::splhigh()
  MsgLog::printf("** Object ");
  MsgLog::print(oid);
  MsgLog::printf(" marked dirty.\n");
  IRQ::splx(ocpl);
#endif
}

void
ObjectHeader::Rescind()
{
  DEBUG(rescind)
    MsgLog::dprintf(true, "Rescinding ot=%d oid=0x%08x%08x\n",
		    obType, (uint32_t) (ob.oid >> 32), (uint32_t) ob.oid);
  FlushIfCkpt();
  
  DEBUG(rescind)
    MsgLog::dprintf(true, "After 'FlushIfCkpt()'\n");

  assert (IsDirty() && GetFlags(OFLG_IO|OFLG_CKPT) == 0);
  assert (GetFlags(OFLG_CURRENT) == OFLG_CURRENT);
  
  DEBUG(rescind)
    MsgLog::dprintf(true, "After 'MakeObjectDirty()'\n");
#ifndef NDEBUG
  if (!kr.IsValid(this))
    MsgLog::dprintf(true, "Keyring of oid 0x%08x%08x invalid!\n",
		    (uint32_t)(ob.oid>>32), (uint32_t)ob.oid);
#endif

  bool hasCaps = GetFlags(OFLG_DISKCAPS) ? true : false;
  
  kr.RescindAll(hasCaps);

  DEBUG(rescind)
    MsgLog::dprintf(true, "After 'RescindAll()'\n");

  // If object has on-disk keys, must dirty the new object to ensure
  // that it gets written.
  if (hasCaps) {
    ob.allocCount++;
    if (obType <= ObType::NtLAST_NODE_TYPE)
      ((Node *) this)->callCount++;

    ClearFlags(OFLG_DISKCAPS);
    DEBUG(rescind)
      MsgLog::dprintf(true, "After bump alloc count\n");
  }

  // FIX: Explicitly zeroing defeats the sever operation.

  if (obType <= ObType::NtLAST_NODE_TYPE) {
    // zeroing unprepares and invalidates products too
    ((Node *) this)->DoZeroThisNode();
    assert ( obType == ObType::NtUnprepared );
  }
  else if (obType == ObType::PtDataPage) {
    InvalidateProducts();

    kva_t pPage = ObjectCache::ObHdrToPage(this);
    bzero((void*)pPage, EROS_PAGE_SIZE);
  }
  else if (obType == ObType::PtCapPage) {
    InvalidateProducts();

    DiskKey* kbuf = (DiskKey *) ObjectCache::ObHdrToPage(this);
    // Following because placement vector new simply isn't working.
    for (unsigned i = 0; i < EROS_PAGE_SIZE/sizeof(DiskKey); i++)
      kbuf[i].ZeroInitKey();
  }
  else
    MsgLog::fatal("Rescind of non-object!\n");

  DEBUG(rescind)
    MsgLog::dprintf(true, "After zero object\n");
}

void
ObjectHeader::ZapResumeKeys()
{
  kr.ZapResumeKeys();
}

#ifdef OB_MOD_CHECK
uint32_t
ObjectHeader::CalcCheck() const
{
  uint32_t ck = 0;
  
#ifndef NDEBUG
  uint8_t oflags = flags;
#endif
#if 0
  MsgLog::printf("Calculating cksum for 0x%08x\n", this);
  MsgLog::printf("OID is 0x%08x%08x, ty %d\n", (uint32_t) (oid>>32),
		 (uint32_t) oid, obType);
#endif
  
  if (obType <= ObType::NtLAST_NODE_TYPE) {
    assert (ObjectCache::ValidNodePtr((Node *) this));
    // Object is a node - compute XOR including allocation count, call
    // counts, and key slots.

    Node *pNode = (Node *) this;
#if 0
    ck ^= ((uint32_t *) &allocCount)[0];
    ck ^= ((uint32_t *) &allocCount)[1];
    ck ^= ((uint32_t *) &(pNode->callCount))[0];
    ck ^= ((uint32_t *) &(pNode->callCount))[1];
#else
    ck ^= ob.allocCount;
    ck ^= pNode->callCount;
#endif

    for (uint32_t i = 0; i < EROS_NODE_SIZE; i++)
      ck ^= (*pNode)[i].CalcCheck();
  }
  else {
    assert (ObjectCache::ValidPagePtr(this));
    uint32_t *pageData = (uint32_t *) ObjectCache::ObHdrToPage(this);

    for (uint32_t w = 0; w < EROS_PAGE_SIZE/sizeof(uint32_t); w++)
      ck ^= pageData[w];
  }

  assert(flags == oflags);

  return ck;
}
#endif

void
ObjectHeader::InvalidateProducts()
{
  if (obType == ObType::PtDataPage ||
      obType == ObType::PtCapPage ||
      obType == ObType::NtSegment) {
    // We need to zap the product chain (MAJOR bummer!)
    while (products) {
      ObjectHeader *pProd = products;
      assert( pProd->obType == ObType::PtMappingPage );
      products = products->next;

      Depend::InvalidateProduct(pProd);
      ObjectCache::ReleasePageFrame(pProd);
    }
    products = 0;
  }
}

#ifdef DDB
void
ObjectHeader::ddb_dump()
{
  extern void db_printf(const char *fmt, ...);

#ifdef OB_MOD_CHECK
  db_printf("Object Header 0x%08x (%s) calcCheck 0x%08x:\n", this,
	    ObType::ddb_name(obType),
	    CalcCheck());
  db_printf("    oid=0x%08x%08x ac=0x%08x check=0x%08x\n",
	    (uint32_t) (ob.oid >> 32), (uint32_t) ob.oid,
	    ob.allocCount, ob.check);
#else
  db_printf("Object Header 0x%08x (%s) oid=0x%016X ac=0x%08x\n", this,
	    ObType::ddb_name(obType),
	    ob.oid, ob.allocCount);
#endif
  db_printf("    ioCount=0x%08x next=0x%08x flags=0x%02x obType=0x%02x age=0x%02x\n",
	    ob.ioCount, next, flags, obType, age);
  db_printf("    prodNdx=%d prodBlss=%d rwProd=%c usrPin=%d kernPin=%d\n",
	    producerNdx, producerBlss, rwProduct ? 'y' : 'n', userPin,
	    kernPin);

  switch(obType) {
  case ObType::PtMappingPage:
    db_printf("    producer=0x%08x\n", producer);
    break;
  case ObType::PtCapPage:
  case ObType::PtDataPage:
  case ObType::NtSegment:
    {
      ObjectHeader *oh = products;
      db_printf("    products= ", products);
      while (oh) {
	db_printf(" 0x%08x", oh);
	oh = oh->next;
      }
      db_printf("\n", products);
      break;
    }
  case ObType::NtProcessRoot:
  case ObType::NtKeyRegs:
  case ObType::NtRegAnnex:
    db_printf("    context=0x%08x\n", context);
    break;
  }
}
#endif
