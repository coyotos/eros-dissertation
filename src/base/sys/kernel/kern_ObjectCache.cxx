/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/Depend.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Invocation.hxx>
//#include <kerninc/Persist.hxx>
#include <kerninc/Checkpoint.hxx>
#ifdef USES_MAPPING_PAGES
#include <machine/PTE.hxx>
#endif
#include <kerninc/util.hxx>
#include <eros/memory.h>
#include <machine/KernTune.hxx>
// #include "Blast.hxx"
// #include <kerninc/Depend.hxx>
#include <kerninc/PhysMem.hxx>
#include <kerninc/Persist.hxx>

#define dbg_cachealloc	0x1	/* steps in taking snapshot */
#define dbg_ckpt	0x2	/* migration state machine */
#define dbg_map		0x4	/* migration state machine */
#define dbg_ndalloc	0x8	/* node allocation */

/* Following should be an OR of some of the above */
#define dbg_flags   ( 0u )

#define DEBUG(x) if (dbg_##x & dbg_flags)

struct PageInfo {
  uint32_t nPages;
  uint32_t baseva;
  ObjectHeader *firstObHdr;
};
static PageInfo pageInfo[Mc_NUM_MEM_CLASS];

uint32_t ObjectCache::nNodes;
uint32_t ObjectCache::nFreeNodeFrames;
Node *ObjectCache::nodeTable;
Node *ObjectCache::firstFreeNode;
ObjectHeader *ObjectCache::firstFreePage;

uint32_t ObjectCache::nPages;
uint32_t ObjectCache::nFreePageFrames;
uint32_t ObjectCache::nReservedIoPageFrames = 0;
uint32_t ObjectCache::nCommittedIoPageFrames = 0;
ObjectHeader *ObjectCache::coreTable;
kva_t ObjectCache::messageScratchPageKva = 0;

// Now that CachedDomains is a tunable, domain cache and depend
// entries are allocated well before we get here.  The new logic only
// needs to worry about allocating nodes, pages, and core table
// entries.  Nodes and pages are allocated in equal proportions, with
// one core table entry per page.

void
ObjectCache::Init()
{
  uint32_t availBytes = PhysMem::AvailBytes();
    
  DEBUG(cachealloc)
    MsgLog::printf("%d bytes of available storage.\n", availBytes);

  uint32_t allocQuanta =
    sizeof(Node) + EROS_PAGE_SIZE + sizeof(ObjectHeader);
    
  availBytes = PhysMem::AvailBytes();

  nNodes = availBytes / allocQuanta;
    
#ifdef TESTING_AGEING
  nNodes = 90;			// This is one less than logtst requires.
#endif
  
  nodeTable = ::new (0) Node[nNodes];

  MsgLog::printf("Allocated Nodes: 0x%x at 0x%08x\n",
		 sizeof(Node[nNodes]), nodeTable);

  // Drop all the nodes on the free node list:
  for (uint32_t i = 0; i < nNodes; i++) {
    // object type is set in constructor...
    assert (nodeTable[i].obType == ObType::NtFreeFrame);
    nodeTable[i].next = &nodeTable[i+1];
  }
  
  nodeTable[nNodes - 1].next = 0;
  firstFreeNode = &nodeTable[0];

  nFreeNodeFrames = nNodes;

  Depend::InitKeyDependTable(nNodes);

  DEBUG(cachealloc)
    MsgLog::printf("%d bytes of available storage after key dep tbl alloc.\n", availBytes);

  AllocateUserPages();

  MsgLog::printf("%d cached domains, %d nodes, %d pages\n",
	       KTUNE_NCONTEXT,
	       nNodes, nPages);

  nFreePageFrames = nPages;

  messageScratchPageKva = ObHdrToPage(GrabPageFrame());
}

Node *
ObjectCache::ContainingNode(void *vp)
{
  uint8_t *bp = (uint8_t *) vp;
  uint8_t *nt = (uint8_t *) nodeTable;
  int nuint8_ts = bp - nt;

  Node *nnt = (Node *) nodeTable;

  return &nnt[nuint8_ts/sizeof(Node)];
}

void
ObjectCache::AllocateUserPages()
{
  // When we get here, we are allocating the last of the core
  // memory. take it all.
    
#if 0
  nPages = PhysMem::AvailPages();

  Depend::InitPageDependTable(nPages);
#endif

  // That took some, so recalculate:

  nPages = PhysMem::AvailPages();

  coreTable = new (0) ObjectHeader[nPages];
  assert(coreTable);

  MsgLog::printf("Allocated Page Headers: 0x%x at 0x%08x\n",
		 sizeof(ObjectHeader[nPages]), coreTable);

  // Allocating the core table ate some of the available pages.
  // As a result, there will be a small number of core table entries
  // that are never used.

  nPages = PhysMem::AvailPages();

#if 0
  MsgLog::printf("alleged nPages = %d (0x%x)\n", nPages, nPages);
#endif
  // Block the pages by class, allocate them, and recompute nPages.
  // Link all pages onto the appropriate free list:
    
  nPages = 0;

  for (int mc = 0; mc < Mc_NUM_MEM_CLASS; mc++) {
    // On the way through this loop, nPages holds the total number
    // of pages in all previous classes until the very end.
	
    uint32_t np = PhysMem::AvailPages((MemClass)mc);
#ifdef TESTING_AGEING
    np = min (np, 50);
#endif
    pageInfo[mc].nPages = np;
    pageInfo[mc].baseva =
      (uint32_t) PhysMem::Alloc(EROS_PAGE_SIZE * np, (MemClass)mc);
    pageInfo[mc].firstObHdr = &coreTable[nPages];

#if 0
    MsgLog::dprintf(true, "0x%08x: core pages: %d (0x%x)\n",
		    pageInfo[mc].baseva, 
		    pageInfo[mc].nPages,
		    pageInfo[mc].nPages);
#endif
    nPages += np;
  }

#if 0
  MsgLog::printf("nPages = %d (0x%x)\n", nPages, nPages);

  halt();
#endif

  // Populate all of the page address pointers in the core table
  // entries:

  {
#if 0
    ObjectHeader *pObHdr = coreTable;
#endif
    
    for (int mc = 0; mc < Mc_NUM_MEM_CLASS; mc++) {
      kva_t frameKva = pageInfo[mc].baseva;
      ObjectHeader *pObHdr = pageInfo[mc].firstObHdr;
      
      for (uint32_t pg = 0; pg < pageInfo[mc].nPages; pg++) {
	pObHdr->pageAddr = frameKva;
	frameKva += EROS_PAGE_SIZE;
	pObHdr++;
      }
    }
  }

  // Link all of the resulting core table entries onto the free page list:
  
  for (uint32_t i = 0; i < nPages; i++) {
    coreTable[i].obType = ObType::PtFreeFrame;
    coreTable[i].next = &coreTable[i+1];
  }

  coreTable[nPages - 1].next = 0;
  firstFreePage = &coreTable[0];
}

ObjectHeader*
ObjectCache::OIDtoObHdr(uint32_t /*cdaLo*/, uint16_t /*cdaHi*/)
{
  // FIX: implement me
  MsgLog::fatal("OIDtoObHdr unimplemented!\n");
  return 0;
}

ObjectHeader *
ObjectCache::GetCorePageFrame(uint32_t ndx)
{
  for (int mc = 0; mc < Mc_NUM_MEM_CLASS; mc++) {
    if (ndx < pageInfo[mc].nPages)
      return &pageInfo[mc].firstObHdr[ndx];
    ndx -= pageInfo[mc].nPages;
  }

  return 0;
}

Node *
ObjectCache::GetCoreNodeFrame(uint32_t ndx)
{
  return &nodeTable[ndx];
}

ObjectHeader *
ObjectCache::PageToObHdr(kva_t pageva)
{
  ObjectHeader *pHdr = 0;
    
  for (int mc = 0; mc < Mc_NUM_MEM_CLASS; mc++) {
    uint32_t pageNo = (pageva - pageInfo[mc].baseva) / EROS_PAGE_SIZE;
    if (pageNo < pageInfo[mc].nPages) {
      pHdr = &pageInfo[mc].firstObHdr[pageNo];
#if 1
      if (pageva == 0xA0000)
	MsgLog::fatal("addr=0xA0000 obhdr=0x%08x (%d) mc=%d npg=%d\n",
		      pHdr, pHdr - pageInfo[0].firstObHdr,
		      mc, pageInfo[mc].nPages);
#endif
      break;
    }
  }

  return pHdr;
}

#ifndef NDEBUG
bool
ObjectCache::ValidPagePtr(const ObjectHeader *pObj)
{
  uint32_t wobj = (uint32_t) pObj;
  
  for (int mc = 0; mc < Mc_NUM_MEM_CLASS; mc++) {
    uint32_t wbase = (uint32_t) pageInfo[mc].firstObHdr;
    uint32_t top = wbase + pageInfo[mc].nPages * sizeof(ObjectHeader);

    if (wobj < wbase)
      continue;
    if (wobj >= top)
      continue;

    uint32_t delta = wobj - wbase;
    if (delta % sizeof(ObjectHeader))
      return false;
    return true;
  }

  return false;
}
#endif

#ifndef NDEBUG
bool
ObjectCache::ValidNodePtr(const Node *pObj)
{
  uint32_t wobj = (uint32_t) pObj;
  uint32_t wbase = (uint32_t) nodeTable;
  uint32_t wtop = wbase + nNodes * sizeof(Node);

  if (wobj < wbase)
    return false;
  if (wobj >= wtop)
    return false;

  uint32_t delta = wobj - wbase;
  if (delta % sizeof(Node))
    return false;

  return true;
}
#endif

#ifndef NDEBUG
bool
ObjectCache::ValidKeyPtr(const Key *pKey)
{
  uint32_t wobj = (uint32_t) pKey;
  uint32_t wbase = (uint32_t) nodeTable;
  uint32_t wtop = wbase + nNodes * sizeof(Node);

  if (inv.IsInvocationKey(pKey))
    return true;

#ifdef KEYREGS_IN_CONTEXT
  if ( Process::ValidKeyReg(pKey) )
    return true;
#endif

  if ( Thread::ValidThreadKey(pKey) )
    return true;

  if (wobj < wbase)
    return false;
  if (wobj >= wtop)
    return false;

  // It's in the right range to be a pointer into a node.  See if it's
  // at a valid slot:
  
  uint32_t delta = wobj - wbase;

  delta /= sizeof(Node);

  Node *pNode = nodeTable + delta;

  for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
    if ( & ((*pNode)[i]) == pKey )
      return true;
  }

  return false;
}
#endif

#ifdef DDB
void
ObjectCache::ddb_dump_pinned_objects()
{
  extern void db_printf(const char *fmt, ...);
  uint32_t userPins = 0;

  for (uint32_t nd = 0; nd < nNodes; nd++) {
    Node *pObj = GetCoreNodeFrame(nd);
    if (pObj->IsPinned()) {
      if (pObj->userPin)
	userPins++;
      db_printf("node 0x%08x%08x\n",
		(uint32_t) (pObj->ob.oid >> 32),
		(uint32_t) pObj->ob.oid);
    }
  }

  for (uint32_t pg = 0; pg < nPages; pg++) {
    ObjectHeader *pObj = GetCorePageFrame(pg);
    if (pObj->IsPinned()) {
      if (pObj->userPin)
	userPins++;
      db_printf("page 0x%08x%08x\n",
		(uint32_t) (pObj->ob.oid >> 32),
		(uint32_t) pObj->ob.oid);
    }
  }

#ifdef OLD_PIN
  db_printf("User pins found: %d official count: %d\n", userPins,
	    ObjectHeader::PinnedObjectCount);
#else
  db_printf("User pins found: %d \n", userPins);
#endif
}

void
ObjectCache::ddb_dump_pages()
{
  uint32_t nFree = 0;
  
  extern void db_printf(const char *fmt, ...);

  for (uint32_t pg = 0; pg < nPages; pg++) {
    ObjectHeader *pObj = GetCorePageFrame(pg);

    if (pObj->IsFree()) {
      nFree++;
      continue;
    }

    char producerType = 'p';
      
    if (pObj->obType == ObType::PtMappingPage) {
      if (pObj->producer == 0) {
	producerType = '?';
      }
      else if (pObj->producer->obType == ObType::PtDataPage) {
	producerType = 'p';
      }
      else if (pObj->producer->obType == ObType::PtCapPage) {
	producerType = 'c';
      }
      else {
	producerType = 'n';
      }
    }
    
#ifdef OB_MOD_CHECK
    char goodSum = (pObj->ob.check == pObj->CalcCheck()) ? 'y' : 'n';
#else
    char goodSum = '?';
#endif
    db_printf("%02d: %s oid %c0x%08x%08x pn:%c cr:%c ck:%c drt:%c%c io:%c sm:%c dc:%c\n",
	      pg,
	      ObType::ddb_name(pObj->obType),
	      producerType,
	      (uint32_t) (pObj->ob.oid >> 32),
	      (uint32_t) (pObj->ob.oid),
	      pObj->IsPinned() ? 'y' : 'n',
	      pObj->GetFlags(OFLG_CURRENT) ? 'y' : 'n',
	      pObj->GetFlags(OFLG_CKPT) ? 'y' : 'n',
	      pObj->GetFlags(OFLG_DIRTY) ? 'y' : 'n',
	      pObj->GetFlags(OFLG_REDIRTY) ? 'y' : 'n',
	      pObj->GetFlags(OFLG_IO) ? 'y' : 'n',
	      goodSum,
	      pObj->GetFlags(OFLG_DISKCAPS) ? 'y' : 'n');
  }

  db_printf("Total of %d pages, of which %d are free\n", nPages, nFree);
}

void
ObjectCache::ddb_dump_nodes()
{
  uint32_t nFree = 0;
  
  extern void db_printf(const char *fmt, ...);

  for (uint32_t nd = 0; nd < nNodes; nd++) {
    ObjectHeader *pObj = GetCoreNodeFrame(nd);

    if (pObj->IsFree()) {
      nFree++;
      continue;
    }

    if (pObj->obType > ObType::NtLAST_NODE_TYPE)
      MsgLog::fatal("Node @0x%08x: object type %d is broken\n", pObj,
		    pObj->obType); 
    
#ifdef OB_MOD_CHECK
    char goodSum = (pObj->ob.check == pObj->CalcCheck()) ? 'y' : 'n';
#else
    char goodSum = '?';
#endif
    db_printf("%02d: %s oid 0x%08x%08x pn:%c cr:%c ck:%c drt:%c%c io:%c sm:%d dc:%c\n",
	      nd,
	      ObType::ddb_name(pObj->obType),
	      (uint32_t) (pObj->ob.oid >> 32),
	      (uint32_t) (pObj->ob.oid),
	      pObj->IsPinned() ? 'y' : 'n',
	      pObj->GetFlags(OFLG_CURRENT) ? 'y' : 'n',
	      pObj->GetFlags(OFLG_CKPT) ? 'y' : 'n',
	      pObj->GetFlags(OFLG_DIRTY) ? 'y' : 'n',
	      pObj->GetFlags(OFLG_REDIRTY) ? 'y' : 'n',
	      pObj->GetFlags(OFLG_IO) ? 'y' : 'n',
	      goodSum,
	      pObj->GetFlags(OFLG_DISKCAPS) ? 'y' : 'n');
  }

  db_printf("Total of %d nodes, of which %d are free\n", nNodes, nFree);
}
#endif

// Queue for threads that are waiting for available page frames:
static ThreadPile PageAvailableQueue;

void
ObjectCache::AgeNodeFrames()
{
  static uint32_t curNode = 0;
  uint32_t nStuck = 0;
  uint32_t nPinned = 0;

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Before AgeNodeFrames()");
#endif

  for (int pass = 0; pass <= Age::PageOut; pass++) {
    nPinned = 0;
    nStuck = 0;
    for (uint32_t count = 0; count < nNodes; count++, curNode++) {
      if (curNode >= nNodes)
	curNode = 0;

      Node *pObj = GetCoreNodeFrame(curNode);
    
      assert(pObj->age <= Age::PageOut);
    
      assert (pObj->GetFlags(OFLG_IO) == 0);
    
      if (pObj->IsPinned()) {
	nPinned++;
	nStuck++;
	continue;
      }
    
      if (pObj->IsFree())
	continue;
#if 0
      // DO NOT AGE OUT THE CURRENT PROCESS!!!
    
      if (pObj->obType == ObType::NtDomainRoot ||
	  pObj->obType == ObType::NtRegAnnex ||
	  pObj->obType == ObType::NtKeyRegs) {

	// FIX: on MP, this needs to check an active bit in the context
	// structure.
	if ( pObj->context && pObj->context->curThread ==
	     Thread::Current() ) {
	  nStuck++;
	  pObj->age = 1;
	}
      }
#endif
      // DO NOT AGE OUT CONSTITUENTS OF AN ACTIVE CONTEXT
      //
      // If a node has an active context structure, it is not a
      // candidate for ageing.  This pins a (relatively) small number
      // of nodes in memory, but guarantees that the invoker and the
      // invokee in a given invocation will not get aged out.
      // Simultaneously, it guarantees that on invocation of a process
      // capability the process context will not go out due to ageing.
      //
      // The reason this is an issue is because we short-circuit the
      // pin of the constituents in Process::Prepare() by testing for
      // a valid saveArea.
      //
      // In SMP implementations we will need to pin contexts in the
      // same way that we pin nodes, in order to ensure that one
      // processor does not remove a context that is active on
      // another.
      //
      // One unfortunate aspect of this design rule is that it becomes
      // substantially harder to test the ageing mechanism.  This rule
      // has the effect of increasing the minimum number of nodes
      // required to successfully operate the system.  This issue can
      // be eliminated once we start pinning process structures.
    
      if (pObj->obType == ObType::NtProcessRoot ||
	  pObj->obType == ObType::NtRegAnnex ||
	  pObj->obType == ObType::NtKeyRegs) {

	if (pObj->context &&
	    ((pObj->context == Thread::Current()->context) ||
	     (inv.IsActive() && (inv.invokee == pObj->context)))) {
	  nStuck++;
	  pObj->age = 1;
	}
      }
      
      // THIS ALGORITHM IS NOT THE SAME AS THE PAGE AGEING ALGORITHM!!!
      //
      // While nodes are promptly cleanable (just write them to a log
      // pot and let the page cleaner work on them), there is still no
      // sense tying up log I/O bandwidth writing the ones that are
      // highly active.  We therefore invalidate them, but we don't try
      // to write them until they hit the ageout age.
    
      if (pObj->age == Age::Invalidate) {
	DEBUG(ckpt)
	  MsgLog::dprintf(false, "Invalidating node=0x%08x oty=%d dirty=%c oid=0x%08x%08x\n",
			  pObj, pObj->obType,
			  (pObj->IsDirty() ? 'y' : 'n'),
			  (uint32_t) (pObj->ob.oid >> 32),
			  (uint32_t) (pObj->ob.oid));

	// Deprepare all keys to this node:
	pObj->kr.UnprepareAll();

	// Also decache any caches of the node:
	pObj->Unprepare(false);

#ifdef TESTING_AGEING
	// And the keys within the node:
	for (unsigned i = 0; i < EROS_NODE_SIZE; i++) {
	  if ( (*pObj)[i].IsHazard() )
	    pObj->ClearHazard(i);
	  (*pObj)[i].NH_Unprepare();
	}
	//  MsgLog::dprintf(true, "After Unprepare()\n");
#endif
      
      }

      if (pObj->age < Age::PageOut) {
	pObj->age++;

	continue;
      }
    
      DEBUG(ckpt)
	MsgLog::dprintf(false, "Ageing out node=0x%08x oty=%d dirty=%c oid=0x%08x%08x\n",
			pObj, pObj->obType,
			(pObj->IsDirty() ? 'y' : 'n'),
			(uint32_t) (pObj->ob.oid >> 32),
			(uint32_t) (pObj->ob.oid));

      pObj->kr.UnprepareAll();
      pObj->Unprepare(false);

      for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
	if ( (*pObj)[i].IsHazard() )
	  pObj->ClearHazard(i);
	(*pObj)[i].NH_Unprepare();
      }

      // Make sure that the next process that wants a frame is
      // unlikely to choose the same node frame:
      curNode++;

      if (pObj->IsDirty()) {
	// This may block if we need to clear out a page frame to
	// accomplish it, in which case we will recurse into the page
	// ager and go to sleep there.
      
	Checkpoint::WriteNodeToLog(pObj);
      }

      assert (!pObj->IsDirty());
    
      // Remove this page from the cache and return it to the free page
      // list:
    
      ReleaseNodeFrame(pObj);

#if defined(TESTING_AGEING) && 0
      MsgLog::dprintf(true, "AgeNodeFrame(): Object evicted\n");
#endif
#ifdef DBG_WILD_PTR
      if (dbg_wild_ptr)
	Check::Consistency("After AgeNodeFrames()");
#endif

      return;
    }
  }

  MsgLog::fatal("%d stuck nodes of which %d are pinned\n",
		nStuck, nPinned);
}

void
ObjectCache::CleanPageFrame(ObjectHeader *pObj)
{
  // Clean up the object we are reclaiming so we can free it:
#ifdef USES_MAPPING_PAGES
  if (pObj->obType == ObType::PtMappingPage) {
    ObjectHeader * pProducer = pObj->producer;

    assert(pProducer);
    assert ( pProducer->kr.IsValid(pProducer) );

    assert (pProducer->IsPinned() == false);
    
    // Zapping the key ring will help if producer was a page or
    // was of perfect height -- ensures that the PTE in the next
    // higher level of the table gets zapped.
    pProducer->kr.UnprepareAll();

    if (pProducer->obType == ObType::NtSegment) {
      // FIX: What follows is perhaps a bit too strong:
      //
      // Unpreparing the producer will have invalidated ALL of it's
      // products, including this one.  We should probably just be
      // disassociating THIS product from the producer.
      //
      // While this is overkill, it definitely works...
      ((Node*)pProducer)->Unprepare(false);
    }

    if (pObj->obType == ObType::PtMappingPage) {
      kva_t pgva = ObjectCache::ObHdrToPage(pObj);
      DEBUG(map)
	MsgLog::printf("Blasting mapping page at 0x%08x\n", pgva);
#if defined(SMALL_SPACES) && 0
      if (pObj->producerNdx == EROS_NODE_LGSIZE)
	MsgLog::printf("Blasting pg dir 0x%08x\n", pgva);
#endif
      PTE::ZapMappingPage(pgva);
    }

    // This product (and all it's siblings) are now on the free
    // list.  The possibility exists, however, that we contrived
    // to invalidate some address associated with the current
    // thread by yanking this mapping table, so we need to do a
    // Yield() here to force the current process to retry:
    Thread::Current()->Wakeup();
    Thread::Current()->Yield();
  }
#endif

  pObj->kr.UnprepareAll();	// This zaps any PTE's as a side effect.
  pObj->InvalidateProducts();

  // Page must be paged out if dirty:
  if (pObj->IsDirty()) {
    // If the object got rescued, it won't have hit ageout age, so
    // the only way it should still be dirty is if the write has not
    // completed:

    DEBUG(ckpt)
      MsgLog::dprintf(true, "ty %d oid 0x%08x%08x slq=0x%08x\n",
		      pObj->obType,
		      (uint32_t) (pObj->ob.oid >> 32),
		      (uint32_t) pObj->ob.oid,
		      &pObj->ObjectSleepQueue());
    
    // FIX: This shouldn't be synchronous -- we should be
    // initiating opportunistic writes here.  It can't simply be
    // asynchronous, as when we are running on a RamDisk the write
    // completes instantly and we end up on the wakeup() list,
    // after which we fail to queue again properly.  The correct
    // fix is to arrange for DIO::Grab() to fail properly on an
    // asynchronous request, and have WritePage...() return a
    // boolean result indicating success/failure.
	
    assert ( PTE::ObIsNotWritable(pObj) );
    Persist::WritePage(pObj, true);
  }
}


// The page frame ager is one of the places where a bunch of sticky
// issues all collide.  Some design principles that we would LIKE to
// satisfy:
//
//  1. Pages should be aged without regard to cleanliness.  The issue
//     is that ageing which favors reclaiming clean pages will tend to
//     reclaim code pages.
//
//  2. The CPU should not sit idle when frames are reclaimable.  This
//     conflicts with principle (1), since any policy that satisfies
//     (1) implies stalling somewhere.
//
//  3. Real-time and non-real-time processes should not have resource
//     conflicts imposed by the ager.  This means both on the frames
//     themselves (easy to solve by coloring) and on the I/O Request
//     structures (which is much harder).
//
// I can see no way to satisfy all of these constraints at once.  For
// that matter, I can't see any solution that always satisfies (1) and
// (2) simultaneously.  The problem is that cleaning takes time which
// is many orders of magnitude longer than a context switch.
//
// The best solution I have come up with is to try to ameliorate
// matters by impedance matching.  Under normal circumstances, the
// ager is the only generator of writes in the system.  During
// stabilization and migration, any write it may do is more likely to
// help than hurt.  These plus the journaling logic are ALL of the
// sources of writes in the system, and we know that object reads and
// writes can never conflict (if the object is already in core to be
// written, then we won't be reading it from the disk).
//
// This leaves two concerns:
//
//   1. Ensuring that there is no contention on the I/O request pool.
//   2. Ensuring that there is limited contention for disk bandwidth.
//
// Since reads and writes do not conflict, (1) can be resolved by
// splitting the I/O request pools by read/write (not currently
// implemented).  If this split is implemented, (2) can be
// accomplished by the simple expedient of restricting the relative
// I/O request pool sizes.
//
// In an attempt to limit the impact of delays due to dirty objects,
// the ager attempts to write objects long before they are candidates
// for reclamation (i.e. we run a unified ageing and cleaning policy
// -- this may want to be revisited in the future, as if the outbound
// I/O bandwidth is available we might as well use it).
//
// The ageing policy proceeds as follows: when ageing is invoked, run
// the ager until one of the following happens:
//
//   1. We find a page to reclaim
//   2. We find a dirty page due to be written.
//
// If (2) occurs, initiate the write and attempt to find a bounded
// number (currently 5) of additional writes to initiate.

void
ObjectCache::AgePageFrames()
{
  static uint32_t curPage = 0;
  uint32_t nStuck = 0;
  uint32_t nPasses = 200;	// arbitrary - catches kernel bugs and
				// dickhead kernel hackers (like me)

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("Before AgePageFrames()");
#endif

  ObjectHeader *reclaimedObject = 0;

  do {
    for (uint32_t count = 0; count < nPages; count++, curPage++) {
      if (curPage >= nPages)
	curPage = 0;

      ObjectHeader *pObj = GetCorePageFrame(curPage);
      
      // Some page types do not get aged:
      if (pObj->obType == ObType::PtDriverPage ||
	  pObj->obType == ObType::PtRawPage) {
	nStuck++;
	continue;
      }
    
      if (pObj->IsFree())
	  continue;
	  
      // Some pages cannot be aged because they are active or pinned:
      if (pObj->IsPinned())
	continue;
    
#ifdef USES_MAPPING_PAGES
      // Or because their producer is pinned -- these cannot go out
      // because they are likely to be involved in page translation.
      if (pObj->obType == ObType::PtMappingPage) {
	ObjectHeader * pProducer = pObj->producer;
	if (pProducer->IsPinned())
	  continue;
      }
#endif

      if (pObj->age == Age::PageOut) {
	if (pObj->GetFlags(OFLG_IO)) {
	  // If this object is due to go out and actively involved in I/O,
	  // then we are still waiting for the effects of the last call to
	  // complete, and we should put the current thread to sleep on
	  // this object:
	  Thread::Current()->SleepOn(pObj->ObjectSleepQueue());
	  Thread::Current()->Yield();
	  assert (false);
	}

	reclaimedObject = pObj;
	break;
      }
      
      pObj->age++;

      if (pObj->age == Age::Invalidate) {
#ifdef USES_MAPPING_PAGES
	// It's a lot cheaper to regenerate a mapping page than to
	// read some other page back in from the disk...
	if (pObj->obType == ObType::PtMappingPage)
	  break;
#endif
	
	DEBUG(ckpt)
	  MsgLog::dprintf(false,
			  "Invalidating page=0x%08x oty=%d dirty=%c oid=0x%08x%08x\n",
			  pObj, pObj->obType,
			  (pObj->IsDirty() ? 'y' : 'n'),
			  (uint32_t) (pObj->ob.oid >> 32),
			  (uint32_t) (pObj->ob.oid));

	assert ( pObj->kr.IsValid(pObj) );
	pObj->kr.UnprepareAll();
	assert ( pObj->kr.IsValid(pObj) );

	// If it's a page, the only products it should have are page tables.
	pObj->InvalidateProducts();
	
	// Unlike nodes, the decision about where to write the page
	// depends on the page type.  Punt it back to the persist
	// logic.  Note that this I/O is done asynchronously if
	// possible.
	if (pObj->IsDirty()) {
	  assert ( PTE::ObIsNotWritable(pObj) );
	  Persist::WritePage(pObj);
	}
      }
    }
  } while (!reclaimedObject && --nPasses);

  if (reclaimedObject == 0)
    MsgLog::fatal("Page frame ageing failed - kernel halts\n");
  
  // Make sure that the next request will not zap the same page --
  // this is especially important if it happens that we are clobbering
  // a mapping page.  If the mapping page is actively in use, it will
  // promptly get rebuilt in the same spot, and we will infinite loop
  // clearing it out.  I know because it happened.
  
  curPage++;
  
  CleanPageFrame(reclaimedObject);
    
  assert (!reclaimedObject->IsDirty());

  // Remove this page from the cache and return it to the free page
  // list:
  
  ReleasePageFrame(reclaimedObject);

#ifdef DBG_WILD_PTR
  if (dbg_wild_ptr)
    Check::Consistency("After AgePageFrames()");
#endif
}

void
ObjectCache::WaitForAvailablePageFrame()
{
  assert (nFreePageFrames >= nReservedIoPageFrames);
  
  if (nFreePageFrames == nReservedIoPageFrames)
    AgePageFrames();

  assert (nFreePageFrames > nReservedIoPageFrames);
}

ObjectHeader *
ObjectCache::DoGrabPageFrame()
{
  assert (nFreePageFrames > 0);

  nFreePageFrames--;
  
  assert(firstFreePage);
  ObjectHeader *pObHdr = firstFreePage;

  assert(pObHdr->kr.IsEmpty());

  firstFreePage = firstFreePage->next;
  pObHdr->next = 0;

  kva_t kva;
  kva = pObHdr->pageAddr;
  bzero(pObHdr, sizeof(*pObHdr));
  pObHdr->pageAddr = kva;

  pObHdr->age = Age::NewBorn;
  pObHdr->obType = ObType::PtDriverPage; // until further notice
  pObHdr->ClearFlags(OFLG_CKPT);
  pObHdr->ResetKeyRing();
  
  assert ( PTE::ObIsNotWritable(pObHdr) );

  return pObHdr;
}

ObjectHeader *
ObjectCache::IoCommitGrabPageFrame()
{
  assert(firstFreePage);
  assert(nReservedIoPageFrames);
  assert(nCommittedIoPageFrames >= nReservedIoPageFrames);
  assert(nFreePageFrames);

  ObjectHeader *pObHdr = DoGrabPageFrame();
  nReservedIoPageFrames--;
  nCommittedIoPageFrames--;

  return pObHdr;
}

ObjectHeader *
ObjectCache::GrabPageFrame()
{
  WaitForAvailablePageFrame();

  ObjectHeader *pObHdr = DoGrabPageFrame();
  return pObHdr;
}

void
ObjectCache::ReleasePageFrame(ObjectHeader *pObHdr)
{
  assert(pObHdr);
  
  assert ( PTE::ObIsNotWritable(pObHdr) );

  assert(pObHdr->kr.IsEmpty());

  pObHdr->Unintern();
    
  if (pObHdr->GetFlags(OFLG_CKPT))
    assert (!pObHdr->IsDirty());
  
  pObHdr->next = firstFreePage;
  pObHdr->obType = ObType::PtFreeFrame;

  firstFreePage = pObHdr;
  
  nFreePageFrames++;
  PageAvailableQueue.WakeAll();
}

void
ObjectCache::RequireNodeFrames(uint32_t n)
{
  while (nFreeNodeFrames < n)
    AgeNodeFrames();
}

void
ObjectCache::RequirePageFrames(uint32_t n)
{
  while (nFreeNodeFrames < n)
    AgePageFrames();
}

Node *
ObjectCache::GrabNodeFrame(/* MemClass mc */)
{
  Node *pNode = 0;

  if (firstFreeNode == 0)
    AgeNodeFrames();
  
  assert(firstFreeNode);
  assert(nFreeNodeFrames);
  
  if (firstFreeNode) {
    pNode = firstFreeNode;
    firstFreeNode = (Node *) firstFreeNode->next;
    pNode->next = 0;
    nFreeNodeFrames--;
  }

  if (pNode) {
    if (pNode->IsFree() == false)
      MsgLog::dprintf(true, "Allocating non-free node 0x%08x\n",
		      pNode);
    
    // Rip it off the hash chain, if need be:
    pNode->Unintern();
    bzero(pNode, sizeof(ObjectHeader));
    pNode->age = Age::NewBorn;
    pNode->obType = ObType::NtUnprepared;
    pNode->ResetKeyRing();
    
#ifndef NDEBUG
    for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
      if (pNode->slot[i].IsUnprepared() == false)
	MsgLog::dprintf(true, "Virgin node 0x%08x had prepared slot %d\n",
			pNode, i);
    }
#endif
  }

  DEBUG(ndalloc)
    MsgLog::printf("Allocated node=0x%08x nfree=%d\n", pNode, nFreeNodeFrames);

  return pNode;
}

void
ObjectCache::ReleaseNodeFrame(Node *pNode)
{
  assert(pNode);
  assert((uint32_t)pNode < (uint32_t)&pNode[nNodes]);
  
  if ( pNode->IsFree() )
    MsgLog::dprintf(true, "Freeing free node frame 0x%08x!\n", pNode);
  
#ifndef NDEBUG
  {
    for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
      if (pNode->slot[i].IsUnprepared() == false)
	MsgLog::dprintf(true, "Released node 0x%08x had prepared slot %d\n",
			pNode, i);
    }
  }
#endif

  pNode->Unintern();
  pNode->obType = ObType::NtFreeFrame;
  
  for (uint32_t i = 0; i < EROS_NODE_SIZE; i++)
    assert (pNode->slot[i].IsUnprepared());
  
  pNode->next = firstFreeNode;
  firstFreeNode = pNode;
  nFreeNodeFrames++;

  DEBUG(ndalloc)
    MsgLog::printf("Freed node=0x%08x (oid 0x%08x%08x) nfree=%d\n",
		   pNode,
		   (uint32_t)(pNode->ob.oid >> 32), (uint32_t)(pNode->ob.oid),
		   nFreeNodeFrames);
}
