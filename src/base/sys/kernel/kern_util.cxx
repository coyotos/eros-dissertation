/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>

int
__assert(const char *expression, const char *file, int line)
{
  MsgLog::fatal("%s:%d: failed assertion `%s'\n", file, line, expression);
  return 0;
}

int
__assertex(const void *ptr, const char *expression, const char *file, int line)
{
  MsgLog::printf("Ptr 0x%08x\n", ptr);
  MsgLog::fatal("%s:%d: failed assertion `%s'\n", file, line, expression);
  return 0;
}

void
__require(const char *expression, const char *file, int line)
{
  MsgLog::dprintf(true, "%s:%d: failed rqt `%s'\n", file, line, expression);
}

static char hexdigits[16] = {
  '0', '1', '2', '3', '4', '5', '6', '7',
  '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};

char
hexdigit(uint8_t b)
{
  return hexdigits[b];
}

bool
IsPrint(uint8_t c)
{
  if (c > 127)
    return false;
  
  static uint8_t isPrint[16] = {	// really a bitmask!
    0x00,			// BEL ACK ENQ EOT ETX STX SOH NUL
    0x00,			// SI  SO  CR  FF  VT  LF  TAB BS
    0x00,			// ETB SYN NAK DC4 DC3 DC2 DC1 DLE
    0x00,			// US  RS  GS  FS  ESC SUB EM  CAN
    0xff,			// '   &   %   $   #   "   !   SPC
    0xff,			// /   .   -   ,   +   *   )   (
    0xff,			// 7   6   5   4   3   2   1   0
    0xff,			// ?   >   =   <   ;   :   9   8
    0xff,			// G   F   E   D   C   B   A   @
    0xff,			// O   N   M   L   K   J   I   H
    0xff,			// W   V   U   T   S   R   Q   P
    0xff,			// _   ^   ]   \   [   Z   Y   X
    0xff,			// g   f   e   d   c   b   a   `
    0xff,			// o   n   m   l   k   j   i   h
    0xff,			// w   v   u   t   s   r   q   p
    0x7f,			// DEL ~   }   |   {   z   y   x
  } ;

  uint8_t mask = isPrint[(c >> 3)];
  c &= 0x7u;
  if (c)
    mask >>= c;
  if (mask & 1)
    return true;
  return false;
}

int
strcmp(const char *p1, const char *p2)
{
  while (*p1 && *p2) {
    if (*p1 < *p2)
      return -1;
    if (*p2 > *p1)
      return 1;

    p2++;
    p1++;
  }

  if (*p1 == *p2)
    return 0;
  if (*p1 == 0)
    return -1;
  return 1;
}

void
strcpy(char *to, const char *from)
{
  char c;
  
  do {
    c = *from++;
    *to++ = c;
  } while(c);
}

uint32_t
align(uint32_t w, uint32_t alignment)
{
    w += alignment - 1;
    w &= ~(alignment - 1);

    return w;
}

