/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Low priority background thread to do consistency checking on kernel
// data structures.  Runs through the object cache, the thread list,
// etc. making sure that things look kosher.

#include <kerninc/kernel.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/AutoConf.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/Depend.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Console.hxx>
#include <kerninc/IRQ.hxx>

static bool Probe(AutoConf *);
static bool Attach(AutoConf *);
struct Driver ac_checker = {
  "checker", Probe, Attach
} ;

#ifdef STRESS_TEST
#define DELAY 2ll
#define INVAL_MODULUS 1
#else
#define DELAY 7000ll
#define INVAL_MODULUS 4
#endif

// Once main() exits and threads are started, this thread drives the
// completion of the bootup process.

const uint32_t StackSize = 1024;
static uint32_t Stack[StackSize];

class CheckThread : public KernThread {
  static void CheckMappingPage(ObjectHeader *);
  static void CheckPage(uint32_t frame);
  static void CheckNode(uint32_t frame);
public:
  static void Start();
  static void UnprepareObjects();

  CheckThread()
    :KernThread("Checker",
		&CheckThread::Start,
		Stack, &Stack[StackSize])
  {
    procContext.InitStack();
  }
};

CheckThread TheCheckThread;

static bool
Probe(AutoConf *)
{
  CpuReserve::KernIdleCpuReserve.AddKernelThread(&TheCheckThread);

  return true;
}

static bool
Attach(AutoConf* ac)
{
  TheCheckThread.Wakeup();	// let it initialize itself...
  
  MsgLog::printf("%s (no vector)\n", ac->name);
  return true;
}

static uint32_t pass = 0;

void UnprepareObjects();

// Unlike all other threads, this one runs once and then exits (or at
// least sleeps forever).
void
CheckThread::Start()
{
  int stack;
  MsgLog::printf("Start CheckThread (thread 0x%x,context 0x%x,stack 0x%x)\n",
	       &TheCheckThread, TheCheckThread.context, &stack);

  for(;;) {
    pass++;
    
    assert(TheCheckThread.state == Thread::Running);

    Check::Consistency("chkthrd");
  
    assert(TheCheckThread.state == Thread::Running);
    
    UnprepareObjects();
    
    assert(TheCheckThread.state == Thread::Running);
    assert( Thread::CAN_PREEMPT() );

    IRQ::DISABLE();
    TheCheckThread.WakeUpIn(DELAY);

    // MsgLog::printf("Checkthread pass %d state %d\n", pass, Thread::state);
    assert(TheCheckThread.state == Thread::Running);

    TheCheckThread.SleepOn(IdlePile);
    IRQ::ENABLE();
    
    TheCheckThread.DirectedYield();

    Console::Twiddle(2);
  }

    // We will never wake up again...
  TheCheckThread.SleepOn(IdlePile);
  TheCheckThread.DirectedYield();
}

void
CheckThread::UnprepareObjects()
{
#if 0
  if ((pass % 2) == 0) {
    for (uint32_t pg = 0; pg < ObjectCache::NumCorePageFrames(); pg++) {
      ObjectHeader *pPage = ObjectCache::GetCorePageFrame(pg);
      if (pPage->flags.free)
	continue;

      pPage->DiscardObTableEntry(false);
      TheCheckThread.DirectedYield();
    }
  }
#endif
  if ((pass % 2) == 1) {
    for (uint32_t nd = 0; nd < ObjectCache::NumCoreNodeFrames(); nd++) {
      Node *pNode = ObjectCache::GetCoreNodeFrame(nd);
      if (pNode->IsFree())
	continue;
#if 0
      if (pNode->obType <= ObType::NtSegment)
	pNode->DiscardObTableEntry(false);
#endif
      TheCheckThread.DirectedYield();
    }
  }
}

