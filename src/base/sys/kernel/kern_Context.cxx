/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/ObjectHeader.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/util.hxx>

// Note protected constructor!!
Process::Process()
{
  kr.ResetRing();
  isUserContext = true;		// until proven otherwise
  cpuReserve = 0;
  faultCode = FC_NoFault;
  faultInfo = 0;
  processFlags = 0;
  saveArea = 0;
  hazards = 0u;	// deriver should change this!
  curThread = 0;

  for (int i = 0; i < 8; i++)
    name[i] = '?';
  name[7] = 0;
}
