/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <disk/MiscKeyType.hxx>	// for MiscKey enum

void StaleMiscKey(Invocation& inv)
{
  extern void NumberKey(Invocation&);
  
  assert (!inv.key->IsHazard());
  inv.key->NH_ZeroKey();

  /* Above may have been a fast ZeroKey operation.  Now actually fill
     in the zeroed values: */
  inv.key->nk.value[0] = 0;
  inv.key->nk.value[1] = 0;
  inv.key->nk.value[2] = 0;
  
  NumberKey(inv);
}

#define DEF_MISCKEY(name) extern void name##Key(Invocation&);
#define OLD_MISCKEY(name)
#include <disk/MiscKey.def>

KeyHandler MiscKeyTable[MiscKeyType::NKEYS] = {
#define DEF_MISCKEY(name) name##Key,
#define OLD_MISCKEY(name) StaleMiscKey,
#include <disk/MiscKey.def>
};

void
MiscKey(Invocation& inv)
{
  uint32_t miscKeyType = inv.key->subType;

  assert (miscKeyType < MiscKeyType::NKEYS);

  /* FIX: These assertions will in some cases be false!  In
     particular, mk_PhysPage keys don't honor this. */

  if (inv.key->subType != MiscKeyType::LogFrame)
    assert (inv.key->nk.value[0] == 0);
  assert (inv.key->nk.value[1] == 0);
  assert (inv.key->nk.value[2] == 0);
  
  MiscKeyTable[miscKeyType](inv);

  assert (InvocationCommitted);

  if (inv.key->subType != MiscKeyType::LogFrame)
    assert (inv.key->nk.value[0] == 0);
  assert (inv.key->nk.value[1] == 0);
  assert (inv.key->nk.value[2] == 0);
}
