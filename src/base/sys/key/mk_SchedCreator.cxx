/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <eros/Invoke.h>
#include <eros/Reserve.h>
#include <eros/SchedCreKey.h>
#include <kerninc/Invocation.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/Thread.hxx>
#include <eros/StdKeyType.h>

void
SchedCreatorKey(Invocation& inv)
{
#ifndef PURE_EXIT_STRINGS
  if (inv.entry.code == OC_SchedCre_Get)
    inv.invokee->SetupExitString(inv, sizeof(CpuReserveInfo));
#endif
#ifndef PURE_ENTRY_STRINGS
  if (inv.entry.code == OC_SchedCre_Set)
    Thread::CurContext()->SetupEntryString(inv);
#endif

  COMMIT_POINT();
      
  switch(inv.entry.code) {
  case OC_SchedCre_GetLimit:
    {
      inv.exit.w1 = MAX_CPU_RESERVE;
      break;
    }
  case OC_SchedCre_Get:
    {
      uint32_t ndx;
      
      ndx = inv.entry.w1;
      if (ndx < STD_CPU_RESERVE || ndx >= MAX_CPU_RESERVE) {
	inv.exit.code = RC_RequestError;
	return;
      }
      
      CpuReserveInfo rsrvinfo;
      CpuReserve& rsrv = CpuReserve::CpuReserveTable[ndx];

      rsrvinfo.index = ndx;
      rsrvinfo.period = Machine::TicksToMilliseconds(rsrv.period);
      rsrvinfo.duration = Machine::TicksToMilliseconds(rsrv.duration);
      rsrvinfo.quanta = Machine::TicksToMilliseconds(rsrv.quanta);
      rsrvinfo.start = Machine::TicksToMilliseconds(rsrv.start);
      rsrvinfo.rsrvPrio = rsrv.rsrvPrio;
      rsrvinfo.normPrio = rsrv.normPrio;

      inv.CopyOut(sizeof(rsrvinfo), &rsrvinfo);
      break;
    }
  case OC_SchedCre_Set:
    {
      if (inv.entry.len < sizeof(CpuReserveInfo)) {
	inv.exit.code = RC_RequestError;
	return;
      }

      CpuReserveInfo ri;

      inv.CopyIn(sizeof(CpuReserveInfo), &ri);
      
      if (ri.index < STD_CPU_RESERVE || ri.index >= MAX_CPU_RESERVE) {
	inv.exit.code = RC_RequestError;
	return;
      }
      
      CpuReserve& rsrv = CpuReserve::CpuReserveTable[ri.index];
      rsrv.reserveTimer.Disarm();
      
      rsrv.period = Machine::MillisecondsToTicks(ri.period);
      rsrv.duration = Machine::MillisecondsToTicks(ri.duration);
      rsrv.quanta = Machine::MillisecondsToTicks(ri.quanta);
      rsrv.start = Machine::MillisecondsToTicks(ri.start);
      rsrv.rsrvPrio = ri.rsrvPrio;
      rsrv.normPrio = ri.normPrio;
      
      rsrv.residQuanta = rsrv.quanta;
      rsrv.residDuration = rsrv.duration;
      if (rsrv.residDuration)
	rsrv.residDuration -= rsrv.quanta;

      rsrv.Reset();
	  
      if (inv.exit.pKey[0]) {
	inv.exit.pKey[0]->InitType(KtSched);
	inv.exit.pKey[0]->SetUnprepared();
	inv.exit.pKey[0]->subType = ri.index;
      }
      // Rest of zero number key is right for this call.
      break;
    }
  case KT:
    {
      inv.exit.code = AKT_MISC(inv.key->subType);
      break;
    }
  default:
    inv.exit.code = RC_UnknownRequest;
    break;
  }
  return;
}
