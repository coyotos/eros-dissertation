/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <eros/Device.h>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <kerninc/Thread.hxx>
#include <kerninc/NetInterface.hxx>
#include <kerninc/BlockDev.hxx>

// FIX -- device key needs a kosher index field, and should use
// subtype for the class.
void
DeviceKey(Invocation& inv)
{
  // Note that DEV_GET_CLASS retrieves the CLASS from the device.
  
  uint32_t devclass = DEV_GET_CLASS(inv.key->dk.devClass);

  if (inv.entry.code == (uint32_t) KT) {
    COMMIT_POINT();
      
    inv.exit.code = AKT_Device;
    inv.exit.w1 = devclass;
    inv.exit.w2 = DEV_GET_SUBCLASS(inv.key->dk.devClass);
    return;
  }

  switch (devclass) {
  case DEV_GET_CLASS(DEV_NET_ENET): // network devices
    {
      uint32_t ndx = inv.key->dk.devNdx;
      NetInterface *ni = NetInterface::Get(ndx);
      if (ni == 0) {
#if 0
	// FIX: Should a key to a nonexistent interface evaporate?  If
	// so, solution is to Walk the caller key regs and do the zap.
	// Fortunately, this is a low frequency case.  COMMENTED OUT
	// BECAUSE MAY NOT BE RIGHT THING

	ArchContext self = (ArchContext *) Thread::CurContext();

	for (i = 1; i < 15; i++) {
	  Key& key = self->keyRegs[i];
	  if (key.GetType() == KtDevice &&
	      devClass == DEV_GET_CLASS(inv.key->dk.devClass) &&
	      inv.key->dk.devNdx == ndx) {
	    key.UnsafeUnprepare()
	    key.ZeroKey();
	  }
	}
#endif

	COMMIT_POINT();
      
	inv.exit.code = RC_UnknownRequest;
      }
      else {
	ni->Invoke(ni, inv);
	assert (InvocationCommitted);
      }

      return;
    }
  case DEV_GET_CLASS(DEV_DISK_SCSI):	// disk devices
    {
      uint32_t ndx = inv.key->dk.devNdx;
      BlockDev *bd = BlockDev::Get(ndx);
      if (bd == 0) {
	COMMIT_POINT();
      
	inv.exit.code = RC_UnknownRequest;
      }
      else {
	bd->Invoke(inv);
	assert (InvocationCommitted);
      }
	
      MsgLog::fatal("Disk keys not yet implemented\n");

      return;
    }
    break;

  default:
    COMMIT_POINT();
      
    inv.exit.code = RC_UnknownRequest;
    return;
  }
}
