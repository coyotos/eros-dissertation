/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/IRQ.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/SleepKey.h>

void
SleepKey(Invocation& inv)
{
  COMMIT_POINT();
      
  switch (inv.entry.code) {
  case OC_Sleep_Wakeup:
    {
      /* This is NOT a no-op.  The wakeup logic hacks wakeup by
	 resetting the order code to this one */
      break;
    }
  case OC_Sleep_Sleep:
  case OC_Sleep_SleepTill:
    {
      uint64_t ms = 00l;

      ms = (((uint64_t) inv.entry.w2) << 32) | ((uint64_t) inv.entry.w1);

      /* FIX: call DeliverResult() here to update result regs! */
      
      IRQ::DISABLE();
      if (inv.entry.code == OC_Sleep_Sleep)
	Thread::Current()->WakeUpIn(ms);
      else
	Thread::Current()->WakeUpAtTick(Machine::MillisecondsToTicks(ms));

      // The following is an ugly lie.  We have committed the
      // invocation, and therefore advanced the PC.  The sleep call,
      // however, does not return in the usual way, but rather yields
      // here.  To make sure that this does not trip up the various
      // consistency checks, we set InvocationCommitted to FALSE here.

#ifndef NDEBUG
      InvocationCommitted = false;
#endif
      
      Thread::Current()->SleepOn(IdlePile);
      IRQ::ENABLE();
      Thread::Current()->Yield();
      return;
    }
    
  case KT:
    inv.exit.code = AKT_MISC(inv.key->subType);
    return;
  default:
    inv.exit.code = RC_UnknownRequest;
    return;
  }
}
