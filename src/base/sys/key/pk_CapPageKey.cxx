/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/PageKey.h>
#include <eros/memory.h>

void
CapPageKey(Invocation& inv)
{
  Key * pageAddress = (Key *)
    ObjectCache::ObHdrToPage(inv.key->GetObjectPtr());

  switch(inv.entry.code) {
  case KT:
    COMMIT_POINT();

    inv.exit.code = AKT_CapPage;
    inv.exit.w1 = inv.key->keyData;
    return;

  case OC_Page_MakeReadOnly:	// Make RO cappage key
    COMMIT_POINT();

    // No problem with overwriting original key, since in that event
    // we would be overwriting it with itself anyway.
    inv.SetExitKey(0, *inv.key);
    if (inv.exit.pKey[0])
      inv.exit.pKey[0]->SetReadOnly();
    
    inv.exit.code = RC_OK;
    return;

  case OC_Page_Zero:		// zero page
    {
      if (inv.key->IsReadOnly()) {
	inv.exit.code = RC_NoAccess;
	return;
      }

      // Mark the object dirty.
      inv.key->GetObjectPtr()->MakeObjectDirty();

      COMMIT_POINT();

      Key* kbuf = (Key *) pageAddress;
      // Following because placement vector new simply isn't working.
      for (unsigned i = 0; i < EROS_PAGE_SIZE/sizeof(Key); i++)
	kbuf[i].KS_ZeroInitKey();

      inv.exit.code = RC_OK;
      return;
    }
  case OC_Page_Clone:
    {
      // copy content of page key in arg0 to current page

      if (inv.key->IsReadOnly()) {
	COMMIT_POINT();
	inv.exit.code = RC_NoAccess;
	return;
      }

      // FIX: This is now wrong: phys pages, time page
      if (inv.entry.key[0]->IsType(KtCapPage) == false) {
	COMMIT_POINT();
	inv.exit.code = RC_RequestError;
	return;
      }

      // Mark the object dirty.
      inv.key->GetObjectPtr()->MakeObjectDirty();

      inv.entry.key[0]->Prepare();
      assert(inv.key->IsPrepared());

      COMMIT_POINT();

      kva_t invokedPage = ObjectCache::ObHdrToPage(inv.key->GetObjectPtr());
      kva_t copiedPage = ObjectCache::ObHdrToPage(inv.entry.key[0]->GetObjectPtr());

      bcopy((void *) copiedPage, (void *) invokedPage, EROS_PAGE_SIZE);
						    
      inv.exit.code = RC_OK;
      return;
    }

  default:
    COMMIT_POINT();

    break;
  }

  inv.exit.code = RC_UnknownRequest;
  return;
}
