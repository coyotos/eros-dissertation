/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/CpuReserve.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/ProcessKey.h>
#include <eros/machine/Registers.h>

// #define DEBUG

void
MaybeRepairCpuReserveLinkages(Node *theNode, CpuReserve* oldReserve,
			      const Key &newKey) 
{
  // We may have just changed the reserve of a domain.
  // Unfortunately, reserve pointers are kept in the thread
  // structure.  We only need to check if the previous key was a
  // schedule key.  If so, the thread we want (if any) is
  // somewhere on the thread chain of the old reserve, else it
  // doesn't exist.

  Thread *pThrd = 0;

#ifdef DEBUG	
  MsgLog::printf("Hunting down threads for cpuReserve %d (0x%08x)\n",
		 oldKey.subType, oldReserve);
#endif
  Link *tlnk = oldReserve->threadChain.next;
  while (tlnk) {
    Thread *lThrd = Thread::ThreadFromCpuReserveLinkage(tlnk);

#ifdef DEBUG
    MsgLog::printf("Examine thread 0x%08x (tlnk 0x%08x)\n", lThrd, tlnk);
#endif
    
    if ( lThrd->IsUser() ) {
      if (lThrd->context) {
	if ( ((Process *) lThrd->context)->procRoot == theNode ) {
	  pThrd = lThrd;
	  break;
	}
      }
      else if (lThrd->processKey.GetKeyOid() == theNode->ob.oid) {
	pThrd = lThrd;
	break;
      }
    }

    tlnk = tlnk->next;
  }

#ifdef DEBUG
  MsgLog::printf("Found thread 0x%08x\n", pThrd);
#endif
  if (pThrd) {
    if ( newKey.IsType(KtSched) ) {
      CpuReserve& newCpuReserve = CpuReserve::CpuReserveTable[newKey.subType];

      newCpuReserve.AddUserThread(pThrd);
    }
    else {
      pThrd->Unlink();		// in case sleeping somewhere
      pThrd->MigrateTo(0);
    }
  }
}

void
ProcessKey(Invocation& inv)
{
  Node *theNode = (Node *) inv.key->GetObjectPtr();

  if (inv.invokee && theNode == inv.invokee->procRoot
      && OC_Process_Swap(0) <= inv.entry.code
      && inv.entry.code != OC_Process_Swap(2)
      && inv.entry.code <= OC_Process_Swap(63))
    MsgLog::dprintf(true, "Modifying invokee domain root\n");

  if (inv.invokee && theNode == inv.invokee->keysNode
      && OC_Process_Swap(0) <= inv.entry.code
      && inv.entry.code <= OC_Process_Swap(63))
    MsgLog::dprintf(true, "Modifying invokee keys node\n");

  switch (inv.entry.code) {
  case KT:
    {
      COMMIT_POINT();

      inv.exit.code = AKT_Process;
      return;
    }
  case OC_Process_Copy(4):
  case OC_Process_Swap(4):
    {
      COMMIT_POINT();

      inv.exit.code = RC_RequestError;
      return;
    }
  
  case OC_Process_Copy(0):
  case OC_Process_Copy(1):
  case OC_Process_Copy(2):
  case OC_Process_Copy(3):
    // slot 4 not valid
  case OC_Process_Copy(5):
  case OC_Process_Copy(6):
  case OC_Process_Copy(7):
  case OC_Process_Copy(8):
  case OC_Process_Copy(9):
  case OC_Process_Copy(10):
  case OC_Process_Copy(11):
  case OC_Process_Copy(12):
  case OC_Process_Copy(13):
  case OC_Process_Copy(14):
  case OC_Process_Copy(15):
#if EROS_NODE_SIZE == 32
  case OC_Process_Copy(16):
  case OC_Process_Copy(17):
  case OC_Process_Copy(18):
  case OC_Process_Copy(19):
  case OC_Process_Copy(20):
  case OC_Process_Copy(21):
  case OC_Process_Copy(22):
  case OC_Process_Copy(23):
  case OC_Process_Copy(24):
  case OC_Process_Copy(25):
  case OC_Process_Copy(26):
  case OC_Process_Copy(27):
  case OC_Process_Copy(28):
  case OC_Process_Copy(29):
  case OC_Process_Copy(30):
  case OC_Process_Copy(31):
#endif
    {
      uint32_t slot = inv.entry.code - OC_Process_Copy(0);

      COMMIT_POINT();

      // All of these complete ok.
      inv.exit.code = RC_OK;
      
      if ((*theNode)[slot].IsRdHazard())
	theNode->ClearHazard(slot);
      inv.SetExitKey(0, theNode->slot[slot]);

      return;
    }      
  case OC_Process_Swap(0):
  case OC_Process_Swap(1):
  case OC_Process_Swap(2):
  case OC_Process_Swap(3):
  case OC_Process_Swap(5):
  case OC_Process_Swap(6):
  case OC_Process_Swap(7):
  case OC_Process_Swap(8):
  case OC_Process_Swap(9):
  case OC_Process_Swap(10):
  case OC_Process_Swap(11):
  case OC_Process_Swap(12):
  case OC_Process_Swap(13):
  case OC_Process_Swap(14):
  case OC_Process_Swap(15):
#if EROS_NODE_SIZE == 32
  case OC_Process_Swap(16):
  case OC_Process_Swap(17):
  case OC_Process_Swap(18):
  case OC_Process_Swap(19):
  case OC_Process_Swap(20):
  case OC_Process_Swap(21):
  case OC_Process_Swap(22):
  case OC_Process_Swap(23):
  case OC_Process_Swap(24):
  case OC_Process_Swap(25):
  case OC_Process_Swap(26):
  case OC_Process_Swap(27):
  case OC_Process_Swap(28):
  case OC_Process_Swap(29):
  case OC_Process_Swap(30):
  case OC_Process_Swap(31):
#endif
    {
      if (theNode == Thread::CurContext()->keysNode)
	MsgLog::dprintf(true, "Swap involving sender keys\n");

      uint32_t slot = inv.entry.code - OC_Process_Swap(0);

      // All of these complete ok.
      inv.exit.code = RC_OK;
      
      theNode->MakeObjectDirty();

      COMMIT_POINT();
      
      theNode->ClearHazard(slot);

      CpuReserve *oldReserve = 0;
      if ( slot == ProcSched && theNode->slot[slot].IsType(KtSched) )
	oldReserve =
	  &CpuReserve::CpuReserveTable[theNode->slot[slot].subType];
	
      {
	Key k;			// temporary in case send and receive
				// slots are the same.

	k.IKS_Set(theNode->slot[slot]);
	
	(*theNode)[slot].NH_Set(*inv.entry.key[0]);
	inv.SetExitKey(0, k);

	// Unchain, but do not unprepare -- the objects do not have
	// on-disk keys. 
	k.NH_Unchain();
      }

      if (oldReserve)
	MaybeRepairCpuReserveLinkages(theNode, oldReserve,
				      theNode->slot[slot]);
      
      Thread::Current()->Prepare();
      inv.nextPC = ((Process *) Thread::CurContext())->GetPC();
      
      return;
    }      
#if EROS_NODE_SIZE == 16
  case OC_Process_Copy(16):
  case OC_Process_Copy(17):
  case OC_Process_Copy(18):
  case OC_Process_Copy(19):
  case OC_Process_Copy(20):
  case OC_Process_Copy(21):
  case OC_Process_Copy(22):
  case OC_Process_Copy(23):
  case OC_Process_Copy(24):
  case OC_Process_Copy(25):
  case OC_Process_Copy(26):
  case OC_Process_Copy(27):
  case OC_Process_Copy(28):
  case OC_Process_Copy(29):
  case OC_Process_Copy(30):
  case OC_Process_Copy(31):
#elif EROS_NODE_SIZE == 32
  case OC_Process_Copy(32):
  case OC_Process_Copy(33):
  case OC_Process_Copy(34):
  case OC_Process_Copy(35):
  case OC_Process_Copy(36):
  case OC_Process_Copy(37):
  case OC_Process_Copy(38):
  case OC_Process_Copy(39):
  case OC_Process_Copy(40):
  case OC_Process_Copy(41):
  case OC_Process_Copy(42):
  case OC_Process_Copy(43):
  case OC_Process_Copy(44):
  case OC_Process_Copy(45):
  case OC_Process_Copy(46):
  case OC_Process_Copy(47):
  case OC_Process_Copy(48):
  case OC_Process_Copy(49):
  case OC_Process_Copy(50):
  case OC_Process_Copy(51):
  case OC_Process_Copy(52):
  case OC_Process_Copy(53):
  case OC_Process_Copy(54):
  case OC_Process_Copy(55):
  case OC_Process_Copy(56):
  case OC_Process_Copy(57):
  case OC_Process_Copy(58):
  case OC_Process_Copy(59):
  case OC_Process_Copy(60):
  case OC_Process_Copy(61):
  case OC_Process_Copy(62):
  case OC_Process_Copy(63):
#else
#error "Bad node size"
#endif
    {
#ifdef KEYS_IN_CONTEXT
      // GetRegs32 length is machine specific, so it does its own copyout.
      inv.exit.code = RC_OK;

      Process* ac = theNode->GetDomainContext();
      ac->Prepare();

      COMMIT_POINT();

      uint32_t slot = inv.entry.code - OC_Process_Copy(EROS_NODE_SIZE);

      inv.exit.key[0].UnsafeSet(ac->keyReg[slot]);
      inv.exit.code = RC_OK;
#else
      uint32_t slot = inv.entry.code - OC_Process_Copy(EROS_NODE_SIZE);

      // Just because it is a domain key does not mean it is well
      // formed!
      
      COMMIT_POINT();
      
      // IF the domain has a key registers subnode, get it:
      Node *domRoot = (Node *) inv.key->GetObjectPtr();
      if ( (*domRoot)[ProcGenKeys].IsZeroKey() ) {
	inv.SetExitKey(0, Key::ZeroNumberKey);
	inv.exit.code = RC_Process_NoKeys;
	return;
      }

      if ( (*domRoot)[ProcGenKeys].IsType(KtNode) == false ) {
	inv.SetExitKey(0, Key::ZeroNumberKey);
	inv.exit.code = RC_Process_Malformed;
	return;
      }
	
      (*domRoot)[ProcGenKeys].Prepare();
      
      // All of these complete ok.
      inv.exit.code = RC_OK;
      
      Node *keysNode = (Node *) (*theNode)[ProcGenKeys].ok.pObj;
      if ((*keysNode)[slot].IsRdHazard())
	keysNode->ClearHazard(slot);
      inv.SetExitKey(0, keysNode->slot[slot]);

      Thread::Current()->Prepare();
#endif      
      return;
    }

#if EROS_NODE_SIZE == 16
  case OC_Process_Swap(16):
  case OC_Process_Swap(17):
  case OC_Process_Swap(18):
  case OC_Process_Swap(19):
  case OC_Process_Swap(20):
  case OC_Process_Swap(21):
  case OC_Process_Swap(22):
  case OC_Process_Swap(23):
  case OC_Process_Swap(24):
  case OC_Process_Swap(25):
  case OC_Process_Swap(26):
  case OC_Process_Swap(27):
  case OC_Process_Swap(28):
  case OC_Process_Swap(29):
  case OC_Process_Swap(30):
  case OC_Process_Swap(31):
#elif EROS_NODE_SIZE == 32
  case OC_Process_Swap(32):
  case OC_Process_Swap(33):
  case OC_Process_Swap(34):
  case OC_Process_Swap(35):
  case OC_Process_Swap(36):
  case OC_Process_Swap(37):
  case OC_Process_Swap(38):
  case OC_Process_Swap(39):
  case OC_Process_Swap(40):
  case OC_Process_Swap(41):
  case OC_Process_Swap(42):
  case OC_Process_Swap(43):
  case OC_Process_Swap(44):
  case OC_Process_Swap(45):
  case OC_Process_Swap(46):
  case OC_Process_Swap(47):
  case OC_Process_Swap(48):
  case OC_Process_Swap(49):
  case OC_Process_Swap(50):
  case OC_Process_Swap(51):
  case OC_Process_Swap(52):
  case OC_Process_Swap(53):
  case OC_Process_Swap(54):
  case OC_Process_Swap(55):
  case OC_Process_Swap(56):
  case OC_Process_Swap(57):
  case OC_Process_Swap(58):
  case OC_Process_Swap(59):
  case OC_Process_Swap(60):
  case OC_Process_Swap(61):
  case OC_Process_Swap(62):
  case OC_Process_Swap(63):
#else
#error "Bad node size"
#endif
    {
#ifdef KEYS_IN_CONTEXT
      if (theNode == Thread::CurContext()->keysNode)
	MsgLog::dprintf(true, "Swap involving sender keys\n");

      // GetRegs32 length is machine specific, so it does its own copyout.
      inv.exit.code = RC_OK;

      Process* ac = theNode->GetDomainContext();
      ac->Prepare();

      COMMIT_POINT();

      uint32_t slot = inv.entry.code - OC_Process_Swap(EROS_NODE_SIZE);

      inv.exit.key[0].UnsafeSet(ac->keyReg[slot]);

      if (slot != 0) {
	// FIX: verify that the damn thing HAD key registers??
	ac->keyReg[slot].UnsafeSet(inv.entry.key[0]);

#if 0
	MsgLog::printf("set key reg slot %d to \n", slot);
	inv.entry.key[0].Print();
#endif
      }

      inv.exit.code = RC_OK;
#else
      uint32_t slot = inv.entry.code - OC_Process_Swap(EROS_NODE_SIZE);

      // Just because it is a domain key does not mean it is well
      // formed!
      
      // IF the domain has a key registers subnode, get it:
      Node *domRoot = (Node *) inv.key->GetObjectPtr();

      if ( (*domRoot)[ProcGenKeys].IsZeroKey() ) {
	MsgLog::dprintf(true, "dom_swap_key(%d): no keys regs\n",
			slot);
	COMMIT_POINT();

	inv.SetExitKey(0, Key::ZeroNumberKey);
	inv.exit.code = RC_Process_NoKeys;

      
	return;
      }

      if ( (*domRoot)[ProcGenKeys].IsType(KtNode) == false ) {
	MsgLog::dprintf(true, "dom_swap_key(%d): keys slot not node key\n",
			slot);
	COMMIT_POINT();
      
	inv.SetExitKey(0, Key::ZeroNumberKey);
	inv.exit.code = RC_Process_Malformed;

	return;
      }
	
      // All of these complete ok.
      inv.exit.code = RC_OK;

      (*domRoot)[ProcGenKeys].Prepare();
      Node *theNode = (Node *) (*domRoot)[ProcGenKeys].ok.pObj;

      // Can't assume about hazards, since no guarantee that domain is
      // prepared, and the constituent node may be under another
      // contract if the user was sufficiently perverse.  In spite of
      // this, check for read hazard so don't unhazard key reg 0 if it
      // happens that this domain IS prepared (yuck-o).
      
      if ( theNode->slot[slot].IsRdHazard() )
	theNode->ClearHazard(slot);

      if (slot != 0) {
	if ( theNode->slot[slot].IsWrHazard() )
	  theNode->ClearHazard(slot);
	theNode->MakeObjectDirty();
      }      

      COMMIT_POINT();
      
      {
	Key k;

	k.IKS_Set(theNode->slot[slot]);

	if (slot != 0) {
	  /* hazard cleared above */
	  (*theNode)[slot].NH_Set(*inv.entry.key[0]);
#if 0
	  MsgLog::printf("set key reg slot %d to \n", slot);
	  inv.entry.key[0].Print();
#endif
	}

	inv.SetExitKey(0, k);

	// Unchain, but do not unprepare -- the objects do not have
	// on-disk keys. 
	k.NH_Unchain();
      }
	
      // NOTE: We know that all of the constituents are in-core, so we
      // know that this will not yield.
      Thread::Current()->Prepare();
#endif
      return;
    }
#if 0
  case OC_Process_GetCtrlInfo32:
    {
      DomCtlInfo32_s info;

      Node *domRoot = (Node *) inv.key->GetObjectPtr();
      
      inv.exit.code = RC_OK;

      ArchContext* ac = domRoot->GetDomainContext();
      ac->Prepare();
      
      COMMIT_POINT();
      
      if (ac->GetControlInfo32(info) == false)
	inv.exit.code = RC_Process_Malformed;

      inv.CopyOut(sizeof(info), &info);
      return;
    }
#endif    
  case OC_Process_GetRegs32:
    {
      Registers regs;
      
      // GetRegs32 length is machine specific, so it does its own copyout.
      inv.exit.code = RC_OK;

      assert( inv.invokee->IsRunnable() );
#if 0
      MsgLog::printf("GetRegs32: invokee is 0x%08x, IsActive? %c\n",
		     inv.invokee, inv.IsActive() ? 'y' : 'n');
#endif

      Process* ac = theNode->GetDomainContext();
      ac->Prepare();

#ifndef PURE_EXIT_STRINGS
      inv.invokee->SetupExitString(inv, sizeof(regs));
#endif

      COMMIT_POINT();
      
      if (ac->GetRegs32(regs) == false)
	inv.exit.code = RC_Process_Malformed;

      inv.CopyOut(sizeof(regs), &regs);
      return;
    }
#if 0    
  case OC_Process_SetCtrlInfo32:
    {
      DomCtlInfo32_s info;

      if ( inv.entry.len != sizeof(info) ) {
	inv.exit.code = RC_RequestError;
	COMMIT_POINT();
      
	return;
      }
      
      inv.exit.code = RC_OK;
      ArchContext* ac = theNode->GetDomainContext();
      ac->Prepare();
      
#ifndef PURE_ENTRY_STRINGS
      Thread::CurContext()->SetupEntryString(inv);
#endif

      COMMIT_POINT();

      inv.CopyIn(sizeof(info), &info);

      if (ac->SetControlInfo32(info) == false)
	inv.exit.code = RC_Process_Malformed;

      return;
    }
#endif
  
  case OC_Process_SetRegs32:
    {
      Registers regs;
      
      if ( inv.entry.len != sizeof(regs) ) {
	inv.exit.code = RC_RequestError;
	COMMIT_POINT();
      
	return;
      }
      
      // FIX: check embedded length and arch type!
      
      // GetRegs32 length is machine specific, so it does its own copyout.
      inv.exit.code = RC_OK;

      Process* ac = theNode->GetDomainContext();
      ac->Prepare();

#ifndef PURE_ENTRY_STRINGS
      Thread::CurContext()->SetupEntryString(inv);
#endif

      COMMIT_POINT();

      inv.CopyIn(sizeof(regs), &regs);

      if (ac->SetRegs32(regs) == false)
	inv.exit.code = RC_Process_Malformed;

      return;
    }

  case OC_Process_SwapMemory32:
    {
      inv.exit.w1 = inv.entry.w1;
      inv.exit.w2 = inv.entry.w2;
      inv.exit.w3 = inv.entry.w3;
      
      // All of these complete ok.
      inv.exit.code = RC_OK;
      
      Process* ac = theNode->GetDomainContext();
      ac->Prepare();

      COMMIT_POINT();

      ac->SetPC(inv.entry.w1);
      
      theNode->ClearHazard(ProcAddrSpace);

      {
	Key k;

	k.IKS_Set(theNode->slot[ProcAddrSpace]);
	
	(*theNode)[ProcAddrSpace].NH_Set(*inv.entry.key[0]);
	inv.SetExitKey(0, k);

	// Unchain, but do not unprepare -- the objects do not have
	// on-disk keys. 
	k.NH_Unchain();
      }

      inv.nextPC = ((Process *) Thread::CurContext())->GetPC();

      return;
    }

  case OC_Process_MkStartKey:
    {
      Process* p = theNode->GetDomainContext();
      p->Prepare();

      COMMIT_POINT();
      
      uint32_t keyData = inv.entry.w1;
      
      if ( keyData > EROS_KEYDATA_MAX ) {
	inv.exit.code = RC_RequestError;
	MsgLog::dprintf(true, "Value 0x%08x is out of range\n",	keyData);
      
	return;
      }

      // All of these complete ok.
      inv.exit.code = RC_OK;
      
      // AAARRRGGGGGHHHH!!!!! This used to fabricate an unprepared
      // key, which was making things REALLY slow.  We do need to be
      // careful, as gate keys do not go on the same key chain as
      // domain keys.  Still, it was just never THAT hard to get it
      // right.  Grumble.
      
      if (inv.exit.pKey[0]) {
	Key &k = *inv.exit.pKey[0];

	k.IKS_Unchain();
	k.SetType(KtStart);
	k.subType = 0;
	k.keyData = keyData;
	k.gk.pContext = p;

	k.gk.next = p->kr.next;
	k.gk.next->prev = (KeyRing *) &k;
	p->kr.next = (KeyRing *) &k;
	k.gk.prev = &p->kr;

	k.SetPrepared();
      }

      return;
    }

  // I'm holding off on these because they require killing the
  // active thread, and I want to make sure I do that when I'm awake.
  case OC_Process_MkProcessAvailable:
  case OC_Process_MkProcessWaiting:
    COMMIT_POINT();

    inv.exit.code = RC_UnknownRequest;
    return;

  case OC_Process_MkResumeKey:
  case OC_Process_MkFaultKey:
    COMMIT_POINT();
      
    inv.SetExitKey(0, *inv.key);
    if (inv.exit.pKey[0]) {
      inv.exit.pKey[0]->NH_Unprepare();
      inv.exit.pKey[0]->SetType(KtResume);
      inv.exit.pKey[0]->keyData = 0;
      inv.exit.pKey[0]->subType =
	(inv.entry.code == OC_Process_MkResumeKey) ? 0 : 1;
    }

    inv.exit.code = RC_OK;

    return;

  case OC_Process_GetFloatRegs:
  case OC_Process_SetFloatRegs:
    COMMIT_POINT();
      
    MsgLog::fatal("Domain key order %d ot yet implemented\n",
		  inv.entry.code);
    
  default:
    COMMIT_POINT();
      
    inv.exit.code = RC_UnknownRequest;
    return;
  }
}
