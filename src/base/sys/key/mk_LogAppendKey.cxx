/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>

void
LogAppendKey(Invocation& inv)
{
#ifndef PURE_ENTRY_STRINGS
  Thread::CurContext()->SetupEntryString(inv);
#endif
  
  COMMIT_POINT();
      
  switch (inv.entry.code) {
  case 1:
    {
      if (inv.entry.len > 1024) {
	inv.exit.code = RC_RequestError;
	return;
      }

#if 0
      MsgLog::printf("Copying %d characters from 0x%08x\n",
		   inv.entry.len, inv.entry.data);
#endif
  
      if (inv.entry.len) {
      MsgLog::putbuf(inv.entry.data, inv.entry.len);

	if (inv.entry.data[inv.entry.len-1] != '\n')
	MsgLog::printf("\n");
      }
  
      inv.exit.code = RC_OK;
      break;
    }
  case KT:
    inv.exit.code = AKT_MISC(inv.key->subType);
    break;

  default:
    inv.exit.code = RC_UnknownRequest;
    break;
  }
  return;
}
