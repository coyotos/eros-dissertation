/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/DiscrimKey.h>
#include <kerninc/Persist.hxx>

void
LogFrameKey(Invocation& inv)
{
  lid_t lid = inv.key->nk.value[0];

  Persist::GetCkFrame(lid);

  COMMIT_POINT();

  if (inv.entry.code == (unsigned) KT) {
    inv.exit.code = RC_OK;
  }
  else {
    inv.exit.code = AKT_MISC(inv.key->subType);
  }

  return;
}
