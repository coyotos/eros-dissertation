/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Machine.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/DevCreKey.h>
#include <kerninc/NetInterface.hxx>

// #define DEBUG

void
NetDevCreatorKey(Invocation& inv)
{
  COMMIT_POINT();
      
  switch (inv.entry.code) {
  case OC_DevCre_ClassInstances:
    {
      uint32_t top = KTUNE_NNETDEV;
      inv.exit.w1 = top;
      inv.exit.code = RC_OK;
      return;
    }
    
  case OC_DevCre_CreateInstanceKey:
    {
      Key *key = inv.exit.pKey[0];
      inv.flags |= INV_EXITKEY0;

      uint32_t ndx;
    
      ndx = inv.entry.w1;

      NetInterface *ni = NetInterface::Get(ndx);
      if (ni == 0) {
	inv.exit.code =  RC_DevCre_NoSuchInstance;
	return;
      }
      
      if (key) {
	assert( key->IsUnprepared() );
	/* key is an exit block key, so not hazarded */
	key->NH_ZeroKey();
	key->InitType(KtDevice);
	key->SetUnprepared();
	key->dk.devClass = ni->devClass;
	key->dk.devNdx   = ndx;
      }

      // FIX: device keys require an allocation count

      inv.exit.code = RC_OK;
      return;
    }
  case KT:
    inv.exit.code = AKT_MISC(inv.key->subType);
    return;
  default:
    inv.exit.code = RC_UnknownRequest;
    return;
  }
}
