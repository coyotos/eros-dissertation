/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/SegmentKey.h>

void
SegmentKey(Invocation& inv)
{
  // FIX: what should KT return from a red seg sans keeper?
    
  COMMIT_POINT();

  if ( inv.key->IsRedSegmentKey() ) {
    // Sadly, we need to verify that the segment is well formed...
    Node *segNode = (Node *) inv.key->GetObjectPtr();
    
    Key &fmtKey = (*segNode)[RedSegFormat];
    if ( fmtKey.IsValidFormatKey() == false ) {
      inv.exit.code = AKT_MalformedRedSeg;	// malformed segment
      inv.exit.w1 = inv.key->keyData;
      return;
    }

    uint32_t slot = REDSEG_GET_KPR_SLOT(fmtKey.nk);
    if ( slot != EROS_NODE_SLOT_MASK &&
	 (*segNode)[slot].IsGateKey() == false &&
	 (*segNode)[slot].IsZeroKey() == false) {
      inv.exit.code = AKT_MalformedRedSeg;	// malformed segment
      inv.exit.w1 = inv.key->keyData;
      return;
    }
	 
  }
  
  inv.exit.code = RC_OK;

  switch (inv.entry.code) {
  case OC_Seg_MakeSpaceKey:
    inv.SetExitKey(0, *inv.key);
    if (inv.exit.pKey[0])
      inv.exit.pKey[0]->keyData |= (inv.entry.w1 & SEGMODE_ATTRIBUTE_MASK);
    return;
  case KT:
    {
      inv.exit.code = AKT_Space;
      inv.exit.w1 = inv.key->keyData;

      return;
    }
  default:
    inv.exit.code = RC_UnknownRequest;
    return;
  }
}
