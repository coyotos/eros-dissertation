/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/DiscrimKey.h>

void
DiscrimKey(Invocation& inv)
{
  switch(inv.entry.code) {

  case OC_Discrim_Classify:
    inv.entry.key[0]->Prepare();

    COMMIT_POINT();
  
    switch(inv.entry.key[0]->GetType()) {
    case KtNumber:
      inv.exit.code = Discrim_Class_Number;
      break;
    case KtResume:
      inv.exit.code = Discrim_Class_Resume;
      break;
    case KtDataPage:
    case KtCapPage:
    case KtNode:
    case KtSegment:
      inv.exit.code = Discrim_Class_Memory;
      break;
    case KtSched:
      inv.exit.code = Discrim_Class_Sched;
      break;
    default:
      inv.exit.code = Discrim_Class_Other;
      break;
    }

    return;

  case OC_Discrim_Verify:
    inv.entry.key[0]->Prepare();

    COMMIT_POINT();
  
    switch(inv.entry.key[0]->GetType()) {
    case KtDataPage:
      if (inv.entry.key[0]->IsReadOnly())
	inv.exit.code = 0;
      break;
    case KtCapPage:
    case KtSegment:
    case KtNode:
      if (inv.entry.key[0]->IsReadOnly() &&
	  inv.entry.key[0]->IsNoCall() &&
	  inv.entry.key[0]->IsWeak())	  
	inv.exit.code = 0;
      break;
    case KtNumber:
      inv.exit.code = 0;
      break;
    case KtMisc:
      if (inv.entry.key[0]->subType == MiscKeyType::Discrim ||
	  inv.entry.key[0]->subType == MiscKeyType::Returner) 
	inv.exit.code = 0;
      break;
    default:
      inv.exit.code = 1;		// not discrete
      break;
    }
    
    return;

  case OC_Discrim_Compare:
    inv.exit.code = 1;

    inv.entry.key[0]->Prepare();
    inv.entry.key[1]->Prepare();
    
    COMMIT_POINT();
  
    // Note that because both keys are prepared, this does the right
    // thing in the case of a special zero number key!
    
    if (inv.entry.key[0]->GetType() != inv.entry.key[1]->GetType())
      inv.exit.code = 0;
    else if ( inv.entry.key[0]->IsObjectKey() ) {
      if ( inv.entry.key[0]->ok.pObj != inv.entry.key[1]->ok.pObj )
	inv.exit.code = 0;
    }
    else {
      if (inv.entry.key[0]->nk.value[0] != inv.entry.key[0]->nk.value[0])
	inv.exit.code = 0;
      if (inv.entry.key[0]->nk.value[1] != inv.entry.key[0]->nk.value[1])
	inv.exit.code = 0;
      if (inv.entry.key[0]->nk.value[2] != inv.entry.key[0]->nk.value[2])
	inv.exit.code = 0;
    }
	     
    if (inv.entry.key[0]->subType != inv.entry.key[1]->subType)
      inv.exit.code = 0;
    if (inv.entry.key[0]->keyData != inv.entry.key[1]->keyData)
      inv.exit.code = 0;

    return;
    
  case KT:
    COMMIT_POINT();
  
    inv.exit.code = AKT_MISC(inv.key->subType);
    return;
  default:
    COMMIT_POINT();
  
    break;
  }

  inv.exit.code = RC_UnknownRequest;
  return;
}
