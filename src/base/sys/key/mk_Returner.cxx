/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/PhysMem.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/ReturnerKey.h>

static uint8_t *ReturnerBuffer;

void AllocReturnerBuffer()
{
  ReturnerBuffer = ::new (0) uint8_t[EROS_MESSAGE_LIMIT];
  MsgLog::printf("Allocated returner buffer: 0x%x at 0x%08x\n",
		 sizeof(uint8_t[EROS_MESSAGE_LIMIT]), ReturnerBuffer);
}

void
ReturnerKey(Invocation& inv)
{
  // CAREFUL! Strings may overlap.  The following strategy works
  // for overlapped strings:
      
  uint32_t len = min (inv.validLen, inv.entry.len);
    
#ifndef PURE_ENTRY_STRINGS
  Thread::CurContext()->SetupEntryString(inv);
#endif
#ifndef PURE_EXIT_STRINGS
  inv.invokee->SetupExitString(inv, inv.entry.len);
#endif

  COMMIT_POINT();

  for (int i = 0; i < 3; i++)
    inv.SetExitKey(i, *inv.entry.key[i]);

  // The returner is used in two situations: invokee == invoker and
  // invokee != invoker.  In the former case, it may be getting used
  // for a permute operation, and we need to make a temporary copy.
  // In the latter case, we can do the copy directly.

  if (inv.invokee == Thread::CurContext()) {
    inv.CopyIn(len, ReturnerBuffer);
    inv.CopyOut(len, ReturnerBuffer);
  }
  else
    inv.CopyOut(len, inv.entry.data);
  
  inv.exit.code = inv.entry.code;
}
