/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/Thread.hxx>
#include <machine/Process.hxx>
#include <eros/Invoke.h>
#include <eros/SysTraceKey.h>
#include <eros/StdKeyType.h>

#if 0
// #define GK_DEBUG
void
GateKey(Invocation& inv)
{
#ifdef GK_DEBUG
  MsgLog::printf("Enter GateKey(), invokedKey=0x%08x\n", inv.key);
#endif

  Process *invokee = inv.invokee;
  
  if (inv.key->IsType(KtResume))
    assert (invokee->runState == RS_Waiting);

#ifdef GK_DEBUG
  MsgLog::printf("Gate: Copying keys\n");
#endif
  
#ifndef PURE_ENTRY_STRINGS
  Thread::CurContext()->SetupEntryString(inv);
#endif
#ifndef PURE_EXIT_STRINGS
  inv.invokee->SetupExitString(inv);
#endif

  COMMIT_POINT();

  KernStats.nGateJmp++;

  // Transfer the keys:
  for (uint32_t i = 0; i < 4; i++) {
    assert(inv.exit.key[i].IsHazard() == false);
    assert(inv.exit.key[i].IsPrepared() == false);
    assert(inv.entry.key[i]->IsRdHazard() == false);

    inv.SetExitKey(i, *inv.entry.key[i]);
  }
  
  // Transfer the data:
#ifdef GK_DEBUG
  MsgLog::printf("Send %d bytes, Rcv %d bytes\n", inv.entry.len, inv.exit.len);
#endif
  inv.CopyOut(inv.entry.len, inv.entry.data);

  {
    extern uint64_t bytes_moved;
    bytes_moved += inv.exit.len;
  }

  // Transfer the order code:
  inv.exit.code = inv.entry.code;
  inv.exit.w1 = inv.entry.w1;
  inv.exit.w2 = inv.entry.w2;
  inv.exit.w3 = inv.entry.w3;

  if (inv.key->IsType(KtResume) &&
      inv.key->subType == KstFault) {
    if (inv.entry.code)
      invokee->SetFault(invokee->faultCode, invokee->faultInfo);
    else
      invokee->SetFault(FC_NoFault, 0);
  }
  return;
}
#endif

void
GateKey(Invocation& inv)
{
#ifdef GK_DEBUG
  MsgLog::printf("Enter GateKey(), invokedKey=0x%08x\n", inv.key);
#endif

  Process *invokee = inv.invokee;
  
  if (inv.key->IsType(KtResume)) {
    if (inv.key->subType != KstResume) {
      inv.suppressXfer = true;

      COMMIT_POINT();
    
      if (inv.entry.code) {
	// Regardless of what the fault code may be, there is no
	// action that a keeper can take via the fault key that can
	// require an uncleared fault demanding slow-path validation
	// is already required for some other reason.  It is therefore 
	// safe to use 'false' here.
	invokee->SetFault(invokee->faultCode, invokee->faultInfo,
			  false);
      }
      else
	invokee->SetFault(FC_NoFault, 0, false);
      return;
    }
    
    assert (invokee->runState == RS_Waiting);
  }

#ifdef GK_DEBUG
  MsgLog::printf("Gate: Copying keys\n");
#endif
  
#ifndef PURE_ENTRY_STRINGS
  Thread::CurContext()->SetupEntryString(inv);
#endif
#ifndef PURE_EXIT_STRINGS
  inv.invokee->SetupExitString(inv, inv.entry.len);
#endif

  COMMIT_POINT();
  KernStats.nGateJmp++;

  // Transfer the data:
#ifdef GK_DEBUG
  MsgLog::printf("Send %d bytes, Rcv %d bytes\n", inv.entry.len, inv.exit.len);
#endif

  inv.CopyOut(inv.entry.len, inv.entry.data);

  {
    extern uint64_t bytes_moved;
    bytes_moved += inv.exit.len;
  }

  // Note that the following will NOT work in the returning to self
  // case, which is presently messed up anyway!!!

  invokee->DeliverGateResult(inv, false);
  
  return;
}
