/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/Thread.hxx>
#include <eros/Key.h>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/ProcessToolKey.h>
#include <eros/KeyBitsKey.h>

static bool
CompareBrand(Invocation& inv, Key* pDomKey, Key* pBrand)
{
  // It was some sort of identify operation -- see if we can satisfy
  // it.  Must not do this operation on prepared key, since the link
  // pointers mess things up rather badly.  Since we know that these
  // are argument keys, the deprepare will not damage any state in the
  // client key registers.
  //
  // The brand slot never needs to be prepared anyway.

  // WE NEED TO MAKE A TEMPORARY COPY OF THESE KEYS in case one of
  // them proves to be the output key.

  assert (pDomKey->IsPrepared());
  pBrand->Prepare();

  Node *domNode = (Node *) pDomKey->GetObjectPtr();
  Key& otherBrand = (*domNode)[ProcBrand];

  otherBrand.Prepare();

  COMMIT_POINT();
  
#if 0
  pBrand->Print();
  domNode->slot[ProcBrand].Print();
#endif

  // Expensive comparison, primarily because we do not want to
  // unprepare the keys (unpreparing them would bump allocation counts
  // unnecessarily).

  if ( pBrand->GetType() != otherBrand.GetType() 
       || pBrand->subType != otherBrand.subType
       || pBrand->keyData != otherBrand.keyData
       || pBrand->keyData != otherBrand.keyData )
    return false;

  // If they *did* prepare, and they are object keys, then the object
  // pointers will be the same.
  switch (pBrand->GetType()) {
  case KtStart:
  case KtResume:
  case KtDataPage:
  case KtCapPage:
  case KtNode:
  case KtSegment:
  case KtProcess:
    // Following takes advantage of representation pun in key
    // structure.  Remember that both keys are prepared.
    assert (offsetof(KeyBits, ok.pObj) == offsetof(KeyBits, gk.pContext));
    if (pBrand->ok.pObj != otherBrand.ok.pObj)
      return false;
    break;

  case KtDevice:
  case KtNumber:
  case KtTimer:
  case KtSched:
  case KtRange:
  case KtMisc:
    if ( pBrand->nk.value[0] != otherBrand.nk.value[0]
	 || pBrand->nk.value[1] != otherBrand.nk.value[1]
	 || pBrand->nk.value[2] != otherBrand.nk.value[2] )
      return false;
    break;
  }
  
  inv.exit.code = pDomKey->keyData;
  
  // Temporary keys of this form MUST NOT BE BUILT until after the
  // commit point!!!
  {
    // Unchain, but do not unprepare -- the objects do not have on-disk
    // keys. 
    Process *pContext = 0;
    if (pDomKey->IsGateKey())
      pContext = pDomKey->gk.pContext;

    inv.SetExitKey(0, *pDomKey);

    if (inv.exit.pKey[0]) {
      assert (inv.exit.pKey[0]->IsPrepared());

      inv.exit.pKey[0]->SetType(KtNode);
      inv.exit.pKey[0]->subType = 0;
      inv.exit.pKey[0]->keyData = 0;

      // We have really just copied the input key and smashed it for a
      // node key.  We now have a prepared node key, UNLESS the input key
      // was actually a gate key, in which event we have a prepared node
      // key on the wrong keyring.  Fix up the keyring in that case:
  
      if (pContext) {
	inv.exit.pKey[0]->NH_Unchain();

	ObjectHeader *pObj = pContext->procRoot;
	assert (pObj);
	// Link as next key after object
	inv.exit.pKey[0]->ok.pObj = pObj;
  
	inv.exit.pKey[0]->ok.next = pObj->kr.next;
	inv.exit.pKey[0]->ok.prev = (KeyRing *) pObj;
  
	pObj->kr.next = (KeyRing*) inv.exit.pKey[0];
	inv.exit.pKey[0]->ok.next->prev = (KeyRing*) inv.exit.pKey[0];
      }
    }
  }

  return true;
}

void
ProcessToolKey(Invocation& inv)
{
#if 0
  Key& arg0Key = inv.entry.key[0]; // user-provided key arg 0
  Key& arg1Key = inv.entry.key[1]; // user-provided brand key
  Key& domKey = arg0Key;	// key to domain
#endif
  KeyType kt;
  
  // Until proven otherwise:
  inv.exit.code = RC_ProcTool_Fail;

  // All of these operations return a single key, which is DK0 until
  // proven otherwise:

  switch (inv.entry.code) {
  case OC_ProcTool_MkProcKey:
    {
      COMMIT_POINT();

      if (inv.entry.key[0]->IsType(KtNode) == false) {
#if 0
        MsgLog::dprintf(true, "domtool: entry key is not node key\n");
#endif
	return;
      }

      if ( inv.entry.key[0]->IsReadOnly() ) {
#if 0
        MsgLog::printf("domtool: entry key is read only\n");
#endif
	return;
      }

      if ( inv.entry.key[0]->IsNoCall() ) {
#if 0
        MsgLog::printf("domtool: entry key is no-call\n");
#endif
	return;
      }

      assert ( inv.entry.key[0]->IsHazard() == false );

      inv.exit.code = RC_OK;

      inv.SetExitKey(0, *inv.entry.key[0]);
      if (inv.exit.pKey[0]) {
	inv.exit.pKey[0]->SetType(KtProcess);
	inv.exit.pKey[0]->keyData = 0;
      }
      return;
    }
  case OC_ProcTool_IdentGateKey:
    {
      inv.entry.key[0]->Prepare();
      
      if (inv.entry.key[0]->IsType(KtStart) == false &&
	  inv.entry.key[0]->IsType(KtResume) == false) {
	COMMIT_POINT();
	return;
      }

      bool isResume = inv.entry.key[0]->IsType(KtResume);
      
      if ( CompareBrand(inv, inv.entry.key[0], inv.entry.key[1]) == false )
	return;
      
      if ( isResume )
	inv.exit.code += 65536;
      return;
    }
  case OC_ProcTool_IdentProcessKey:
    {
      inv.entry.key[0]->Prepare();
      

      if (inv.entry.key[0]->IsType(KtProcess) == false) {
	COMMIT_POINT();
	return;
      }

      if ( CompareBrand(inv, inv.entry.key[0], inv.entry.key[1]) ) 
	inv.exit.code = RC_OK;
      return;
    }
  case OC_ProcTool_IdentSegGateKey:
    {
      kt = KtStart;

      if (inv.entry.key[0]->IsType(KtSegment) == false &&
	  inv.entry.key[0]->IsType(KtNode) == false) {
	COMMIT_POINT();
	return;
      }

      if (inv.entry.key[0]->IsRedSegmentKey() == false) {
	COMMIT_POINT();
	return;
      }

      if ( inv.entry.key[0]->IsNoCall() ) {
	COMMIT_POINT();
	return;
      }

      inv.entry.key[0]->Prepare();

      Node *theNode = (Node *) inv.entry.key[0]->GetObjectPtr();
      Key& fmtKey = (*theNode)[RedSegFormat];

      if ( fmtKey.IsValidFormatKey() == false ) {
#if 0
	extern void db_eros_print_key(Key&);
	db_eros_print_key(fmtKey);
	MsgLog::dprintf(true, "Bad format key!\n");
#endif
	COMMIT_POINT();
	return;
      }

      // If no keeper key:
      uint32_t slot = REDSEG_GET_KPR_SLOT(fmtKey.nk);
      if ( slot == EROS_NODE_SLOT_MASK ||
	   (*theNode)[slot].IsGateKey() == false) {
	COMMIT_POINT();
	return;
      }

      Key& domKey = (*theNode)[slot];
      domKey.Prepare();
      
      if (domKey.IsType(KtStart) == false &&
	  domKey.IsType(KtResume) == false) {
	COMMIT_POINT();
	return;
      }

      bool isResume = domKey.IsType(KtResume);
      
      if ( CompareBrand(inv, &domKey, inv.entry.key[1]) == false )
	return;

      if ( isResume )
	inv.exit.code += 65536;

      return;
    }
  case OC_ProcTool_CompareProcessOrigins:
    {
      if ((inv.entry.key[0]->IsGateKey() == false) &&
	  (inv.entry.key[0]->IsType(KtProcess) == false)) {

	COMMIT_POINT();
	return;
      }

      if ((inv.entry.key[1]->IsGateKey() == false) &&
	  (inv.entry.key[1]->IsType(KtProcess) == false)) {

	COMMIT_POINT();
	return;
      }
      
      inv.entry.key[0]->Prepare();
      inv.entry.key[1]->Prepare();

      Node *domain0 = (Node *) inv.entry.key[0]->GetObjectPtr();
      Node *domain1 = (Node *) inv.entry.key[1]->GetObjectPtr();

      (*domain0)[ProcBrand].Prepare();
      (*domain1)[ProcBrand].Prepare();

      COMMIT_POINT();

      Key& brand0 = (*domain0)[ProcBrand];
      Key& brand1 = (*domain1)[ProcBrand];

      // The following works ONLY because neither the domain brand key nor
      // a key coming from a key register can be hazarded.
      if (memcmp( &brand0, &brand1, sizeof(Key)) == 0)
	inv.exit.code = RC_OK;
      else
	inv.exit.code = 1;

      return;
    }

  case KT:
    COMMIT_POINT();

    inv.exit.code = AKT_MISC(inv.key->subType);
    return;
  default:
    COMMIT_POINT();

    inv.exit.code = RC_UnknownRequest;
    return;
  }
  return;
}
