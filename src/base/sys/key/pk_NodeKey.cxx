/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Node.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/Process.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/NodeKey.h>
#include <eros/NumberKey.h>
#include <disk/MiscKeyType.hxx>
#include <eros/memory.h>

// See pk_DomainKey.cxx
extern void
MaybeRepairCpuReserveLinkages(Node *theNode, CpuReserve* oldReserve,
			      const Key &newKey);

// #define NODEKEYDEBUG

// Node key implements node keys, RO node keys, and sense keys.
// N.B. that the node we point to may NOT be prepared as a node!

// DANGER -- there is a potential gotcha in the node key
// implementation.  Suppose that the node key happens to name the
// caller's domain root. The operations (swap, zero, WriteNumbers)
// either alter the register values of the domain or cause the domain
// to become malformed.  The swap operation may, in addition, cause
// the key registers node to be changed, which has the effect of
// altering the return key slots.
//
// KeyKOS made this all transparent. It would be nice to make all of
// this transparent, but if you are holding a node key in the first
// place then you have sufficient authority to obtain a domain key to
// yourself, by which any alteration that would leave your domain
// running can be accomplished.  If you don't want your domain
// running, be a sport and return to the null key.
//
// Given this, I just made dorking the returnee generate a suitable
// return code.
//

void
Desensitize(Key *k)
{
  if (k == 0)
    return;
  
  switch (k->GetType()) {
  case KtNode:
  case KtSegment:
    k->SetReadOnly();
    k->SetNoCall();
    k->SetWeak();
    break;
  case KtDataPage:
    k->SetReadOnly();
    break;
  case KtNumber:
    break;
  case KtMisc:
    if (k->subType == MiscKeyType::Discrim ||
	k->subType == MiscKeyType::Returner)
      break;
    // FALL THROUGH
  default:
    // Safe because only called on invocation block keys:
    assert( k->IsHazard() == false );
    k->NH_ZeroKey();
    break;
  }
}

void
NodeKey(Invocation& inv)
{
  bool isSense = inv.key->IsWeak();
  bool isFetch = inv.key->IsReadOnly();
  
  Node *theNode = (Node *) inv.key->GetObjectPtr();

  if (inv.invokee && theNode == inv.invokee->procRoot
      && OC_Node_Swap(0) <= inv.entry.code
      && inv.entry.code <= OC_Node_Swap(31))
    MsgLog::dprintf(true, "Modifying invokee domain root\n");

  if (inv.invokee && theNode == inv.invokee->keysNode
      && OC_Node_Swap(0) <= inv.entry.code
      && inv.entry.code <= OC_Node_Swap(31))
    MsgLog::dprintf(true, "Modifying invokee keys node\n");

  switch (inv.entry.code) {
  case KT:
    {
      COMMIT_POINT();

      inv.exit.code = AKT_Node;
      inv.exit.w1 = inv.key->keyData;
      return;
    }

  case OC_Node_Copy(0):
  case OC_Node_Copy(1):
  case OC_Node_Copy(2):
  case OC_Node_Copy(3):
  case OC_Node_Copy(4):		// brand copy not permitted
  case OC_Node_Copy(5):
  case OC_Node_Copy(6):
  case OC_Node_Copy(7):
  case OC_Node_Copy(8):
  case OC_Node_Copy(9):
  case OC_Node_Copy(10):
  case OC_Node_Copy(11):
  case OC_Node_Copy(12):
  case OC_Node_Copy(13):
  case OC_Node_Copy(14):
  case OC_Node_Copy(15):
#if EROS_NODE_SIZE == 32
  case OC_Node_Copy(16):
  case OC_Node_Copy(17):
  case OC_Node_Copy(18):
  case OC_Node_Copy(19):
  case OC_Node_Copy(20):
  case OC_Node_Copy(21):
  case OC_Node_Copy(22):
  case OC_Node_Copy(23):
  case OC_Node_Copy(24):
  case OC_Node_Copy(25):
  case OC_Node_Copy(26):
  case OC_Node_Copy(27):
  case OC_Node_Copy(28):
  case OC_Node_Copy(29):
  case OC_Node_Copy(30):
  case OC_Node_Copy(31):
#endif
    {
      uint32_t slot = inv.entry.code - OC_Node_Copy(0);

      // All of these complete ok.
      inv.exit.code = RC_OK;
      
      COMMIT_POINT();

      if ( (*theNode)[slot].IsRdHazard() )
	theNode->ClearHazard(slot);

      // Does not copy hazard bits, but preserves preparation:
      inv.SetExitKey(0, theNode->slot[slot]);

#ifdef NODEKEYDEBUG
      MsgLog::dprintf(true, "Copied key to exit slot %d\n", slot);
#endif
      
      if (isSense)
	Desensitize(inv.exit.pKey[0]);
      
      Thread::Current()->Prepare();
      
      return;
    }      

  case OC_Node_Swap(0):
  case OC_Node_Swap(1):
  case OC_Node_Swap(2):
  case OC_Node_Swap(3):
  case OC_Node_Swap(4):
  case OC_Node_Swap(5):
  case OC_Node_Swap(6):
  case OC_Node_Swap(7):
  case OC_Node_Swap(8):
  case OC_Node_Swap(9):
  case OC_Node_Swap(10):
  case OC_Node_Swap(11):
  case OC_Node_Swap(12):
  case OC_Node_Swap(13):
  case OC_Node_Swap(14):
  case OC_Node_Swap(15):
#if EROS_NODE_SIZE == 32
  case OC_Node_Swap(16):
  case OC_Node_Swap(17):
  case OC_Node_Swap(18):
  case OC_Node_Swap(19):
  case OC_Node_Swap(20):
  case OC_Node_Swap(21):
  case OC_Node_Swap(22):
  case OC_Node_Swap(23):
  case OC_Node_Swap(24):
  case OC_Node_Swap(25):
  case OC_Node_Swap(26):
  case OC_Node_Swap(27):
  case OC_Node_Swap(28):
  case OC_Node_Swap(29):
  case OC_Node_Swap(30):
  case OC_Node_Swap(31):
#endif
    {
      if (theNode == Thread::CurContext()->keysNode)
	MsgLog::dprintf(true, "Swap involving sender keys\n");

      if (isFetch) {
	inv.exit.code = RC_NoAccess;
	return;
      }
	
      uint32_t slot = inv.entry.code - OC_Node_Swap(0);

      // All of these complete ok.
      inv.exit.code = RC_OK;
      
      theNode->MakeObjectDirty();
      
      COMMIT_POINT();			// advance the PC!
      
      // Following will not cause dirty node because we forced it
      // dirty above the commit point.
      theNode->ClearHazard(slot);

      // Tread carefully, because we need to rearrange the CPU
      // reserve linkages.
      CpuReserve *oldReserve = 0;
      if ( slot == ProcSched && theNode->slot[slot].IsType(KtSched) )
	oldReserve =
	  &CpuReserve::CpuReserveTable[theNode->slot[slot].subType];
	
      {
	Key k;			// temporary in case send and receive
				// slots are the same.

	k.IKS_Set(theNode->slot[slot]);
	
	(*theNode)[slot].NH_Set(*inv.entry.key[0]);
	inv.SetExitKey(0, k);

	// Unchain, but do not unprepare -- the objects do not have
	// on-disk keys. 
	k.NH_Unchain();
      }

      if (oldReserve)
	MaybeRepairCpuReserveLinkages(theNode, oldReserve,
				      theNode->slot[slot]);

#ifdef NODEKEYDEBUG
      MsgLog::dprintf(true, "Swapped key to slot %d\n", slot);
#endif
      Thread::Current()->Prepare();
      inv.nextPC = ((Process *) Thread::CurContext())->GetPC();

      return;
    }      

  case OC_Node_MakeNodeKey:
  case OC_Node_MakeSegmentKey:
    {
      COMMIT_POINT();

      uint32_t w = inv.entry.w1;

      if ( (w > EROS_KEYDATA_MAX) ) {
	inv.exit.code = RC_RequestError;
	MsgLog::dprintf(true, "Value 0x%08x is out of range\n", w);
	return;
      }

      w |= (inv.key->keyData & SEGMODE_ATTRIBUTE_MASK);
	
      inv.exit.code = RC_OK;
      
      inv.SetExitKey(0, *inv.key);
      if (inv.exit.pKey[0]) {
	inv.exit.pKey[0]->keyData = w;

	switch(inv.entry.code) {
	case OC_Node_MakeNodeKey:
	  break;
	case OC_Node_MakeSegmentKey:
	  inv.exit.pKey[0]->SetType(KtSegment);
	  break;
	}
      }
	
      return;
    }
    
  case OC_Node_CompareKey:
    {
      inv.exit.code = 1;		// until proven otherwise

      inv.entry.key[0]->Prepare();

      COMMIT_POINT();

      switch (inv.entry.key[0]->GetType()) {
      case KtNode:
      case KtSegment:
	// It's possible - check further below
	break;
      default:
	return;
      }

      if (inv.entry.key[0]->GetKeyOid() != inv.key->GetKeyOid())
	return;
      if (inv.entry.key[0]->GetAllocCount() !=
	  inv.key->GetAllocCount()) 
	return;
	
      // FIX: should this compare key data fields?
      inv.exit.code = RC_OK;
      return;
    }

  case OC_Node_Zero:
    {
      if (isFetch) {
	inv.exit.code = RC_NoAccess;
	return;
      }

      theNode->MakeObjectDirty();

      // If we zero it, we're going to nail all of it's dependencies
      // anyway:
      
      COMMIT_POINT();

      theNode->ZeroThisNode();
      
      Thread::Current()->Prepare();
      
      return;
    }

  case OC_Node_KeyDataField:
    {
      COMMIT_POINT();

      inv.exit.w1 = inv.key->keyData;
      inv.exit.code = RC_OK;
      return;
    }

  case OC_Node_WriteNumber(0):
  case OC_Node_WriteNumber(1):
  case OC_Node_WriteNumber(2):
  case OC_Node_WriteNumber(3):
  case OC_Node_WriteNumber(4):
  case OC_Node_WriteNumber(5):
  case OC_Node_WriteNumber(6):
  case OC_Node_WriteNumber(7):
  case OC_Node_WriteNumber(8):
  case OC_Node_WriteNumber(9):
  case OC_Node_WriteNumber(10):
  case OC_Node_WriteNumber(11):
  case OC_Node_WriteNumber(12):
  case OC_Node_WriteNumber(13):
  case OC_Node_WriteNumber(14):
  case OC_Node_WriteNumber(15):
#if EROS_NODE_SIZE == 32
  case OC_Node_WriteNumber(16):
  case OC_Node_WriteNumber(17):
  case OC_Node_WriteNumber(18):
  case OC_Node_WriteNumber(19):
  case OC_Node_WriteNumber(20):
  case OC_Node_WriteNumber(21):
  case OC_Node_WriteNumber(22):
  case OC_Node_WriteNumber(23):
  case OC_Node_WriteNumber(24):
  case OC_Node_WriteNumber(25):
  case OC_Node_WriteNumber(26):
  case OC_Node_WriteNumber(27):
  case OC_Node_WriteNumber(28):
  case OC_Node_WriteNumber(29):
  case OC_Node_WriteNumber(30):
  case OC_Node_WriteNumber(31):
#endif
    {
      if (isFetch) {
	inv.exit.code = RC_NoAccess;
	return;
      }

      uint32_t slot = inv.entry.code - OC_Node_WriteNumber(0);

      // If we overwrite it, we're going to nail all of it's
      // dependencies anyway:
      
      inv.exit.code = RC_OK;

      Key zeroKey;		// default constructor == DK(0)
      
      CpuReserve *oldReserve = 0;
      
      if (slot == ProcSched && theNode->slot[slot].IsType(KtSched) )
	oldReserve =
	  &CpuReserve::CpuReserveTable[theNode->slot[slot].subType];

      nk_value nkv;
      bzero (&nkv, sizeof(nkv));

      theNode->MakeObjectDirty();

      COMMIT_POINT();
	
      theNode->ClearHazard(slot);

      assert( (*theNode)[slot].IsUnprepared() );
      (*theNode)[slot].NH_ZeroKey();
      (*theNode)[slot].nk.value[0] = inv.entry.w1;
      (*theNode)[slot].nk.value[1] = inv.entry.w2;
      (*theNode)[slot].nk.value[2] = inv.entry.w3;

      if (inv.entry.w1 || inv.entry.w2 || inv.entry.w3)
	(*theNode)[slot].subType = 0;
      
      if (oldReserve)
	  MaybeRepairCpuReserveLinkages(theNode, oldReserve, zeroKey);
      
      Thread::Current()->Prepare();
      inv.nextPC = ((Process *) Thread::CurContext())->GetPC();

      return;
    }

  case OC_Node_Clone:
    {
      // copy content of node key in arg0 to current node

      if (isFetch) {
	inv.exit.code = RC_NoAccess;
	return;
      }

      // Mark the object dirty.
      theNode->MakeObjectDirty();

      inv.entry.key[0]->Prepare();
      assert(inv.key->IsPrepared());

      COMMIT_POINT();

      if (inv.entry.key[0]->GetType() != KtNode) {
	inv.exit.code = RC_RequestError;
	return;
      }

      bool isWeak = inv.entry.key[0]->IsWeak();
		     
      Node *fromNode = (Node *) inv.entry.key[0]->GetObjectPtr();

      for (unsigned slot = 0; slot < EROS_NODE_SIZE; slot++) {
	theNode->ClearHazard(slot);
	if (fromNode->slot[slot].IsRdHazard())
	  fromNode->ClearHazard(slot);
      }

      // The following will do the right thing if the two nodes are
      // identical, because the NH_Set code checks for this case.
      for (unsigned slot = 0; slot < EROS_NODE_SIZE; slot++) {
	/* hazards cleared above */
	theNode->slot[slot].NH_Set( (*fromNode)[slot] );
	if (isWeak)
	  Desensitize(&theNode->slot[slot]);
      }
						    
      return;
    }
#if 0
  // Removed because keeping the reserves straight is a pain in the ass.
  case OC_Node_WriteNumbers:
    {
      if (isFetch) {
	inv.exit.code = RC_NoAccess;
	return;
      }

      inv.exit.code = RC_RequestError;

      struct inMsg {
	Word start;
	Word end;
	Word numBuf[EROS_NODE_SIZE][3];	// number key data fields
      } req;

      // If we overwrite it, we're going to nail all of it's
      // dependencies anyway:
      
      theNode->Unprepare(true);

      theNode->MakeObjectDirty();

#ifndef PURE_ENTRY_STRINGS
      Thread::CurContext()->SetupEntryString(inv);
#endif

      COMMIT_POINT();

      bzero(&req, sizeof(req));
      inv.CopyIn(sizeof(req), &req);

      if (req.start >= EROS_NODE_SIZE ||
	  req.end >= EROS_NODE_SIZE ||
	  req.start > req.end)
	return;

      for (uint32_t k = req.start; k < req.end; k++) {
	(*theNode)[k].ZeroKey();
	(*theNode)[k].nk.value[0] = req.numBuf[(k-req.start)][0];
	(*theNode)[k].nk.value[1] = req.numBuf[(k-req.start)][1];
	(*theNode)[k].nk.value[2] = req.numBuf[(k-req.start)][2];

	if ( (*theNode)[k].nk.value[0] ||
	     (*theNode)[k].nk.value[1] ||
	     (*theNode)[k].nk.value[2] )
	  (*theNode)[k].subType = 0;
      }

      return;
    }
#endif

  default:
    COMMIT_POINT();

    inv.exit.code = RC_UnknownRequest;
    return;
  }
}
