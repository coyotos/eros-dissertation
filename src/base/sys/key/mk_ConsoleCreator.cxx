/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/Console.hxx>
#include <kerninc/Thread.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/ConsCreKey.h>

void
ConsoleCreatorKey(Invocation& inv)
{
  
  switch(inv.entry.code) {
  case OC_ConsCre_Put:
    {
#ifndef PURE_ENTRY_STRINGS
      Thread::CurContext()->SetupEntryString(inv);
#endif

      COMMIT_POINT();

      for (uint32_t i = 0; i < inv.entry.len; i++)
	Console::Put(inv.entry.data[i]);
      inv.exit.code = RC_OK;
      return;
    }
  case OC_ConsCre_KDB:
    {
      inv.exit.code = RC_OK;
      Debugger();
      break;
    }
  case KT:
    {
      inv.exit.code = AKT_MISC(inv.key->subType);
      break;
    }
  default:
    inv.exit.code = RC_UnknownRequest;
    break;
  }

  COMMIT_POINT();
  return;
}
