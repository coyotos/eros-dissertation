/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/Machine.hxx>
#include <kerninc/Thread.hxx>

#include <eros/PageKey.h>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/memory.h>
#include <disk/MiscKeyType.hxx>

#define OC_Page_MakeReadOnly	1
#define OC_Page_Zero		2
#define OC_Page_Write(offset)	(65536 + (offset))

void
PhysPageKey(Invocation& inv)
{
  COMMIT_POINT();
      
  kva_t pageAddress = PTOV(inv.key->nk.value[1]);

  // All mutating cases require us to map the physical page into the
  // kernel address space.  Since none of these invocations generate a
  // reply, map the page into the receive buffer area:

  switch(inv.entry.code) {
  case OC_Page_MakeReadOnly:
    inv.SetExitKey(0, *inv.key);
    if (inv.exit.pKey[0])
      inv.exit.pKey[0]->SetReadOnly();

    inv.exit.code = RC_OK;
    return;

  case OC_Page_Zero:		// zero page
    // FIX: Shouldn't this mark the object header dirty in case this
    // is actually an object page frame??
    if (inv.key->IsReadOnly()) {
      inv.exit.code = RC_NoAccess;
      return;
    }
    
    bzero((void*)PTOV(pageAddress), EROS_PAGE_SIZE);
    inv.exit.code = RC_OK;
    return;

  case KT:
    inv.exit.code = AKT_MISC(inv.key->subType);
    return;

  default:
    if (inv.entry.code >= OC_Page_Write(0) &&
	inv.entry.code < OC_Page_Write(EROS_PAGE_SIZE)) {
      if (inv.entry.len > EROS_PAGE_SIZE) {
	inv.exit.code = RC_RequestError;
	return;
      }

      uint32_t offset = inv.entry.code - 65536u;
      kva_t sndBuf = (kva_t) inv.entry.data;
      kva_t rcvBuf = (kva_t) PTOV(pageAddress) + offset;

      uint32_t count = min(inv.entry.len, EROS_PAGE_SIZE - offset);
      
      bcopy((const void *)(sndBuf + offset), (void*)rcvBuf, count);
      inv.entry.len -= count;

      if (inv.entry.len) {
	sndBuf += count;
	rcvBuf = PTOV(pageAddress);
	bcopy((const void *)sndBuf, (void *)rcvBuf, count);
      }

      inv.exit.code = RC_OK;
      return;
    }
    break;
  }

  inv.exit.code = RC_UnknownRequest;
  return;
}

