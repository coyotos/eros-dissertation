/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/Thread.hxx>

#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <disk/MiscKeyType.hxx>

void
PhysPageRangeKey(Invocation& inv)
{
  COMMIT_POINT();
      
  kpa_t pageAddr = inv.entry.code & ~EROS_PAGE_MASK;
  
  if (inv.exit.pKey[0]) {
    Key& key = *inv.exit.pKey[0];
    inv.flags |= INV_EXITKEY0;

    // Fabricate a new physical page key:
    assert( key.IsPrepared() == false );
    /* key is an exit block key, so not hazarded */
    key.NH_ZeroKey();
    key.ktByte = PREPARED_KT(KtMisc);
    key.subType = MiscKeyType::PhysPage;
    key.keyData = EROS_PAGE_BLSS;
    key.nk.value[0] = pageAddr;
  }
      
  return;
}

