/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// TimeOfDay key -- returns current time of day and/or current tick.

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/Machine.hxx>
#include <eros/TimeOfDay.h>
#include <kerninc/SysTimer.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>

void
TimeOfDayKey(Invocation& inv)
{
  switch(inv.entry.code) {
  case OC_TimeOfDay_Now:	// get current time of day.
    {
      TimeOfDay tod;
      Machine::GetHardwareTimeOfDay(tod);

#ifndef PURE_EXIT_STRINGS
      inv.invokee->SetupExitString(inv, sizeof(tod));
#endif
      
      COMMIT_POINT();

      (void) inv.CopyOut(sizeof(tod), &tod);
      inv.exit.code = RC_OK;

      // Make sure we do not commit redundantly!
      return;
    }
  case OC_TimeOfDay_UpTime:	// get ms since startup.
    {
      uint64_t curTick = SysTimer::Now();
      uint64_t ms = Machine::TicksToMilliseconds(curTick);
      
      inv.exit.w1 = ms;
      inv.exit.w2 = (ms>>32);
      inv.exit.code = RC_OK;
      break;
    }
  case KT:
    inv.exit.code = AKT_MISC(inv.key->subType);
    break;
  default:
    inv.exit.code = RC_UnknownRequest;
    break;
  }

  COMMIT_POINT();
}
