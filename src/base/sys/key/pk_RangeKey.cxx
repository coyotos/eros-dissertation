/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/BlockDev.hxx>
#include <kerninc/Checkpoint.hxx>
#include <disk/DiskNode.hxx>
#include <disk/DiskFrame.hxx>

#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/RangeKey.h>

// #define DEBUG_RANGEKEY

void
RangeKey(Invocation& inv)
{
  range_off_t range = 0ll;
  OID start = inv.key->rk.oid;
  OID end = inv.key->rk.oid + inv.key->rk.count;
  
  if (inv.key->subType == 1) {
    start = 0ll;
    end = UINT64_MAX;
  }
  
  switch(inv.entry.code) {
  case KT:
    COMMIT_POINT();

    inv.exit.code = AKT_Range;
    return;

  case OC_Range_Query:
    {
      COMMIT_POINT();

      range = end - start;

      inv.exit.w1 = range;
      inv.exit.w2 = (fixreg_t) (range >> 32);

      inv.exit.code = RC_OK;

      return;
    }
    
  case OC_Range_Identify:
    {
      // Key to identify is in slot 0
      Key& key = *inv.entry.key[0];
      key.Prepare();

      COMMIT_POINT();

      inv.exit.code = RC_OK;
      
      if (key.IsType(KtDataPage) == false &&
	  key.IsType(KtCapPage) == false &&
	  key.IsType(KtNode) == false) {
	inv.exit.code = RC_Range_RangeErr;
	return;
      }

#if 0
      // There is no reason why these should fail to identify.
      if (key.keyData & SEGMODE_ATTRIBUTE_MASK) {
	inv.exit.code = RC_RequestError;
	return;
      }
#endif
	
      OID oid = key.GetKeyOid();
      
      if ( oid < start )
	inv.exit.code = RC_Range_RangeErr;
      else if ( oid >= end )
	inv.exit.code = RC_Range_RangeErr;
      else
	range = oid - start;
      
      /* ALERT: output convention depends on register size! */
      if (key.IsType(KtDataPage))
	inv.exit.w1 = OT_DataPage;
      else if (key.IsType(KtCapPage))
	inv.exit.w1 = OT_CapPage;
      else if (key.IsType(KtNode))
	inv.exit.w1 = OT_Node;
      else
	inv.exit.w1 = OT_INVALID;
      
      // FIX: there is a register size assumption here!
      assert (sizeof(range) == sizeof(uint64_t));
      assert (sizeof(inv.exit.w2) == sizeof(uint32_t) ||
	      sizeof(inv.exit.w2) == sizeof(uint64_t));
      inv.exit.w2 = range;
      if (sizeof(inv.exit.w2) != sizeof(range_off_t))
	inv.exit.w3 = (range >> 32);
      else
	inv.exit.w3 = 0;
      
      return;
    }
  case OC_Range_Rescind:
    {
      Key& key = *inv.entry.key[0];
      key.Prepare();

      inv.exit.code = RC_OK;
      
      if (key.IsType(KtDataPage) == false &&
	  key.IsType(KtCapPage) == false &&
	  key.IsType(KtNode) == false) {
	inv.exit.code = RC_Range_RangeErr;
	COMMIT_POINT();

	return;
      }

      if (key.keyData & SEGMODE_ATTRIBUTE_MASK) {
	inv.exit.code = RC_RequestError;
	COMMIT_POINT();

	return;
      }
	  
      OID oid = key.GetKeyOid();
      
#ifdef DEBUG_PAGERANGEKEY
      MsgLog::dprintf(true, "Rescinding page OID 0x%08x%08x\n",
		      (uint32_t) (oid>>32),
		      (uint32_t) oid);
#endif
      
      if ( oid < start ) {
	inv.exit.code = RC_Range_RangeErr;
	COMMIT_POINT();

	return;
      }
      else if ( oid >= end ) {
	inv.exit.code = RC_Range_RangeErr;
	COMMIT_POINT();

	return;
      }

      ObjectHeader* pObject = key.GetObjectPtr();
      pObject->MakeObjectDirty();

      COMMIT_POINT();
      
      pObject->Rescind();
#ifdef DBG_WILD_PTR
      if (dbg_wild_ptr)
	Check::Consistency("Object rescind()");
#endif
      return;
    }

  case OC_Range_WaitObKey:
  case OC_Range_GetObKey:
    {
      inv.exit.code = RC_OK;  // set the exit code

      uint64_t offset =
	(((uint64_t) inv.entry.w3) << 32) | ((uint64_t) inv.entry.w2);

      // Figure out the OID for the new key:
      OID oid = start + offset;

      if (oid > end) {	// comparing ranges: INclusive
	MsgLog::dprintf(true, "oid 0x%X top 0x%X\n", oid, end);
	inv.exit.code = RC_Range_RangeErr;
	return;
      }

      uint32_t obNdx = offset % EROS_OBJECTS_PER_FRAME;
      
      ObjectHeader *pObj = 0;

      Key* key = inv.exit.pKey[0];
      inv.flags |= INV_EXITKEY0;
      
      switch(inv.entry.w1) {
      case OT_DataPage:
      case OT_CapPage:
	{
	  if (obNdx != 0) {
	    inv.exit.code = RC_Range_RangeErr;
	    return;
	  }
	
	  ObType::Type oty = ObType::PtDataPage;
	  uint32_t newTag = FRM_TYPE_ZDPAGE;
	  uint8_t kt = KtDataPage;

	  if (inv.entry.w1 == OT_CapPage) {
	    oty = ObType::PtCapPage;
	    newTag = FRM_TYPE_ZCPAGE;
	    kt = KtCapPage;
	  }

	  // If we don't get the object back, it's because of bad frame
	  // type:
	  pObj = Persist::GetPage(oid, 0, oty, false);

	  inv.MaybeDecommit();
	
	  if (pObj == 0) {
	    // Validate that there exists a corresponding node range:
	    CoreDivision *pcd = Persist::FindDivision(dt_Object, oid);

	    if (pcd == 0) {
	      if (inv.entry.code == OC_Range_WaitObKey) {
		MsgLog::printf("pg: And then they slept for a looooong time...\n");
		BlockDev::WaitForMount();
	      }
	      else {
		inv.exit.code = RC_Range_RangeErr;
		return;
	      }	  
	    }

	    Checkpoint::RetagFrame(oid, newTag);

	    pObj = Persist::GetPage(oid, 0, oty, false);
	  }

	  assert(inv.CanCommit());
	  assert(pObj);

	  COMMIT_POINT();

#if 1
	  // It's definitely an object key.  Pin the object it names.
	  pObj->TransLock();
#endif
	  
	  if (key) {
	    // Unchain the old key so we can overwrite it...
	    if (key)
	      key->NH_Unchain();

	    key->InitType(kt);
	    key->SetPrepared();
	    key->subType = 0;
	    key->keyData = EROS_PAGE_BLSS;
	  }
	  break;
	}
      case OT_Node:
	if (obNdx >= DISK_NODES_PER_PAGE) {
	  inv.exit.code = RC_Range_RangeErr;
	  return;
	}
	
	// If we don't get the object back, it's because of bad frame
	// type:
	pObj = Persist::GetNode(oid, 0, false);

	inv.MaybeDecommit();
	
	if (pObj == 0) {
	  // Validate that there exists a corresponding node range:
	  CoreDivision *pcd = Persist::FindDivision(dt_Object, oid);

	  if (pcd == 0) {
	    if (inv.entry.code == OC_Range_WaitObKey) {
	      MsgLog::printf("pg: And then they slept for a looooong time...\n");
	      BlockDev::WaitForMount();
	    }
	    else {
	      inv.exit.code = RC_Range_RangeErr;
	      return;
	    }	  
	  }

	  Checkpoint::RetagFrame(oid, FRM_TYPE_ZNODE);

	  pObj = Persist::GetNode(oid, 0, false);
	}

	assert(inv.CanCommit());
	assert(pObj);

	COMMIT_POINT();

#if 1
	// It's definitely an object key.  Pin the object it names.
	pObj->TransLock();
#endif

	if (key) {
	  // Unchain the old key so we can overwrite it...
	  if (key)
	    key->NH_Unchain();

	  key->InitType(KtNode);
	  key->SetPrepared();
	  key->subType = 0;
	  key->keyData = 0;
	}
	break;

#ifdef EROS_PROCS_PER_FRAME
      case OT_Process:
	MsgLog::fatal("Fabrication of cappage/process keys not "
		      "yet implemented\n");
#endif
      }
      
      if (key) {
	key->ok.pObj = pObj;
  
	// Link as next key after object
	key->ok.pObj = pObj;
  
	key->ok.next = pObj->kr.next;
	key->ok.prev = (KeyRing *) pObj;
  
	pObj->kr.next = (KeyRing*) key;
	key->ok.next->prev = (KeyRing*) key;
      }

#ifdef DEBUG_PAGERANGEKEY
      MsgLog::dprintf(true, "pObject is 0x%08x\n", pObj);
#endif
      
      
      return;
    }
    
  case OC_Range_Compare:
    {
      Key& key = *inv.entry.key[0];
      key.Prepare();

      COMMIT_POINT();

      inv.exit.code = 1;	// until proven otherwise

      if (!key.IsType(KtRange))
	return;			// RC_OK in this case probably wrong thing.

      OID kstart = key.rk.oid;
      OID kend = key.rk.oid + key.rk.count;

      if (key.subType == 1) {
	kstart = 0ll;
	kend = UINT64_MAX;
      }
  
      if (kstart >= end || kend <= start)
	return;

      // They overlap; need to figure out how.
      inv.exit.code = RC_OK;
      inv.exit.w1 = 0;
      inv.exit.w3 = 0;
      
      if (kstart < start) {
	inv.exit.w1 = EROS_REG_MAX; // poor person's -1
	inv.exit.w2 = (fixreg_t) (start - kstart);
	if (sizeof(inv.exit.w2) == sizeof(uint32_t)) // 32 bit system
	  inv.exit.w3 = (fixreg_t) ((start - kstart) >> 32);
      }
      else if (kstart == start) {
	inv.exit.w2 = 0;
      }
      else {
	inv.exit.w1 = 1;
	inv.exit.w2 = (fixreg_t) (kstart - start);
	if (sizeof(inv.exit.w2) == sizeof(uint32_t)) // 32 bit system
	  inv.exit.w3 = (fixreg_t) ((kstart - start) >> 32);
      }
      return;
    }
  default:
    COMMIT_POINT();

    inv.exit.code = RC_UnknownRequest;
    return;
  }

  return;
}
