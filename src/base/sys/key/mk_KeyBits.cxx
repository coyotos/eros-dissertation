/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <kerninc/Key.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/Thread.hxx>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/KeyBitsKey.h>

// WHAT KeyBits_Translate RETURNS:
//
// There are two design issues:
//   1. What should /KeyBits_Translate/ return for invalid keys
//   2. Should /KeyBits_Translate/ expose the allocation/call count?
//   3. Should /KeyBits_Translate/ return this information in some
//      well-canonicalized form?
//
// For invalid keys, we have chosen to have /KeyBits_Translate/ return
// a zero number key.  This is accomplished by first *preparing* the
// key we are to analyze (which renders it into canonical form) and
// then hand-depreparing it into a KeyBits structure.
//
// As to the allocation count, here we run into a question.  At the
// moment, the only object that uses KeyBits is KID, and this does not
// really require the allocation/call count.  For the moment, I am
// exposing them, though I strongly suspect this is the wrong thing to
// do.  The argument in favor is that watching the counts roll over is
// a help to debuggers...
//
// I am also extending KeyBits to return information concerning
// whether the key in question is in fact valid.  In the future, if we
// elect to let stale keys retain their information for the sake of
// debuggers, this may prove useful.

void
KeyBitsKey(Invocation& inv)
{
  switch(inv.entry.code) {
  case KT:
    COMMIT_POINT();
    inv.exit.code = AKT_MISC(inv.key->subType);
    return;

  case OC_KeyBits_Translate:
    {
      struct {
	uint32_t kbVersion;
	uint32_t valid;
	KeyBits dupKey;
      } info;

#ifndef PURE_EXIT_STRINGS
      inv.invokee->SetupExitString(inv, sizeof(info));
#endif

      // This is dirty - we need to arrive at the *unprepared* version
      // of the key, but without creaming the actual key:

      info.kbVersion = EROS_KEYBITS_VERSION;
      info.valid = 1;
      
      inv.entry.key[0]->Prepare();
      
      COMMIT_POINT();
      
      uint8_t kt = inv.entry.key[0]->GetType();
      
      // Note that we do NOT expose hazard bits!
      info.dupKey.InitType(kt);
      info.dupKey.SetUnprepared();
      info.dupKey.subType = inv.entry.key[0]->subType;
      info.dupKey.keyData = inv.entry.key[0]->keyData;

      if ( inv.entry.key[0]->IsObjectKey() ) {
	ObjectHeader *pObj = inv.entry.key[0]->ok.pObj;

	if ( inv.entry.key[0]->IsGateKey() )
	  pObj = inv.entry.key[0]->gk.pContext->procRoot;

	if ( inv.entry.key[0]->IsType(KtResume) )
	  info.dupKey.unprep.count = ((Node *) pObj)->callCount;
	else
	  info.dupKey.unprep.count = pObj->ob.allocCount;

	info.dupKey.unprep.oid = pObj->ob.oid;
      }
      else {
	info.dupKey.nk.value[0] = inv.entry.key[0]->nk.value[0];
	info.dupKey.nk.value[1] = inv.entry.key[0]->nk.value[1];
	info.dupKey.nk.value[2] = inv.entry.key[0]->nk.value[2];
      }

      inv.CopyOut(sizeof(info), &info);
      inv.exit.code = RC_OK;
      return;
    }
  default:
    COMMIT_POINT();
    inv.exit.code = RC_UnknownRequest;
    return;
  }
}
