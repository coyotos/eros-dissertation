#
# Copyright (C) 1998, 1999, Jonathan S. Shapiro.
#
# This file is part of the EROS Operating System.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#

# Script to create compiled-in symbol table.  This script critically
# relies on the fact that the debugging .o file is linked at the very
# end of the program and therefore does not perturb code space.  In
# particular, it trusts that for all functions of interest the function
# address does not move between 'eros' and 'eros.debug'
#
# This is wrong for the functions in 'DebuggerStubs'.


BEGIN {
  debug = 0;   # set to 0 for quiet tool
  
  digit_value["0"] = 0;
  digit_value["1"] = 1;
  digit_value["2"] = 2;
  digit_value["3"] = 3;
  digit_value["4"] = 4;
  digit_value["5"] = 5;
  digit_value["6"] = 6;
  digit_value["7"] = 7;
  digit_value["8"] = 8;
  digit_value["9"] = 9;
  digit_value["a"] = 10;
  digit_value["b"] = 11;
  digit_value["c"] = 12;
  digit_value["d"] = 13;
  digit_value["e"] = 14;
  digit_value["f"] = 15;
  digit_value["A"] = 10;
  digit_value["B"] = 11;
  digit_value["C"] = 12;
  digit_value["D"] = 13;
  digit_value["E"] = 14;
  digit_value["F"] = 15;

  FILECOUNT = 0
}

function cleanfun(s) {
  gsub(/:[fF][(),0-9=*]+$/, "", s);
  return s;
}

function basename(s) {
  while (pos = index(s, "/")) {
    s = substr(s, pos+1);
  }

  return s;
}

# Strangely enough, gawk does not appear to grok hex numbers for
# purposes of conversion...
function hextodec(s) {
  value = 0;
  
  while (length(s) > 0) {

#    printf("s to convert is: %s\n", s);
    
    digit = substr(s, 1, 1);
    s = substr(s, 2);
    value = value * 16;
    value += digit_value[digit];
  }

#  printf("Final value is 0x%x\n", value);

  return value;
}

# Following is to pick functions out of the --syms output:
$2=="l" && $3=="F" && $4==".text" {
  pc = hextodec($1);
  fun_name = $6;
  
  if (debug)
    printf("0x%08x FUN  %s\n", pc, fun_name);

  if (funtab[pc] && funtab[pc] != fun_name) {
    printf("[SYMS] Function table mismatch at 0x%08x old=%s new=%s\n",
	   pc, funtab[pc], fun_name);
    exit(1);
  }

  funtab[pc] = fun_name;
  funpc[pc] = pc;
}

$2=="g" && $3=="F" && $4==".text" {
  pc = hextodec($1);
  fun_name = $6;
  
  if (debug)
    printf("0x%08x FUN  %s\n", pc, fun_name);

  if (funtab[pc] && funtab[pc] != fun_name) {
    printf("[SYMS] Function table mismatch at 0x%08x old=%s new=%s\n",
	   pc, funtab[pc], fun_name);
    exit(1);
  }

  funtab[pc] = fun_name;
  funpc[pc] = pc;
}

$2=="w" && $3=="F" && $4==".text" {
  pc = hextodec($1);
  fun_name = $6;
  
  if (debug)
    printf("0x%08x FUN  %s\n", pc, fun_name);

  if (funtab[pc] && funtab[pc] != fun_name) {
    printf("[SYMS] Function table mismatch at 0x%08x old=%s new=%s\n",
	   pc, funtab[pc], fun_name);
    exit(1);
  }

  funtab[pc] = fun_name;
  funpc[pc] = pc;
}

$2=="SO" && NF==7 {
  pc = hextodec($5);

  bn = basename($7);

  if (debug)
    printf("0x%08x FILE %s\n", pc, bn);

  if (bn in file_list) {
    file_sym_ndx = file_list[bn];
  }
  else {
    file_list[bn] = FILECOUNT;
    file_sym_ndx = FILECOUNT;
    FILECOUNT++;
  }
    
  curfile = file_sym_ndx;
  compunit = bn;
}

$2=="SO" && NF==6 {
  # End of compilation unit.  Add an entry to the table to bound line
  # numbers and the like.

  pc = hextodec($5);

  if (debug)
    printf("0x%08x EOCU <%s>\n", pc, compunit);
}

$2=="SOL"   {
  pc = hextodec($5);
  bn = basename($7);
  
  if (debug)
    printf("0x%08x INC  %s\n", pc, bn);

  if (bn in file_list) {
    file_sym_ndx = file_list[bn];
  }
  else {
    file_list[bn] = FILECOUNT;
    file_sym_ndx = FILECOUNT;
    FILECOUNT++;
  }
    
  curfile = file_sym_ndx;
}

$2=="FUN"   {
  pc = hextodec($5);

  fun_name = cleanfun($7)
    
  # the following check is correct for later compilers, but earlier
  # compilers used in RH 4.2 generated bogus function entries, which
  # would in turn cause invalid line numbers.
  #if (funtab[pc] != fun_name) {
  #  printf("[FUN] Uncaught function 0x%08x old=%s new=%s\n",
  #         pc, funtab[pc], fun_name);
  #  exit(1);
  #}

#  fun_name = cleanfun($7);
#  
#  if (debug)
#    printf("0x%08x FUN  %s\n", pc, fun_name);
#
#  if (funtab[$6] && funtab[$6] != fun_name) {
#    printf("String table mismatch at %d old=%s new=%s\n",
#	   $6, funtab[$6], fun_name);
#    exit(1);
#  }
#
#  funtab[$6] = fun_name;
#  funpc[$6] = pc;

  if (funtab[pc] == fun_name) {
    cur_funpc = pc
  }

#  if (funtab[pc])
#    printf("%s => %s\n", funtab[pc], fun_name) >> "/dev/tty";
}

$2=="SLINE" || $2=="DSLINE" || $2=="BSLINE" {
  delta = hextodec($5);
  pc = delta + cur_funpc;
  
  line_no[pc] = $4;
  line_file[pc] = curfile;
    
  if (debug)
    printf("0x%08x LINE %s:%d\n", pc, basename(file_list[curfile]), $4);
}


# Once we have processed all of this shit, we need to go through it
# and output the relevant symbol tables.  One useful saving grace
# is that objdump gives us this stuff in sorted order, else we would
# need to sort here.
END {
  outfile = "symnames.s"
  
  printf("\t.file \"symtab.S\"\n") > outfile;
  printf("\t.version \"01.01\"\n") > outfile;

  #
  # First, output the function table entries.  Must be in .data
  # because kernel re-sorts it on the fly.
  #
  
  printf("\n\t.section .data\n") > outfile;
  printf("\t.align 4\n") > outfile;
  printf("\t.globl _7FuncSym.table\n") > outfile;
  printf("_7FuncSym.table:\n") > outfile;
  
  numsymbols = 0;
  
  for (i in funtab) {
    printf("\t.long 0\t\t#profile counter\n") > outfile;
    printf("\t.long 0x%08x\n", funpc[i]) > outfile;
    printf("\t.long .LM%d\n", i) > outfile;
    printf("\t.long .LH%d\n", i) > outfile;
    numsymbols++;
  }

  printf("\n\t.section .rodata\n") > outfile;
  printf("\n\t.align 4\n") > outfile;
  printf(".globl _7FuncSym.count\n") > outfile;
  printf("_7FuncSym.count:\n") > outfile;
  printf("\t.long %d\n", numsymbols) > outfile;

  #
  # Dump the line table in such a way that the linker will
  # complete the relocations for us:
  #
  printf("\n\n\t.section .data\n") > outfile;
  printf("\t.align 4\n") > outfile;
  printf("\t.globl _7LineSym.table\n") > outfile;
  printf("_7LineSym.table:\n") > outfile;

  numlines = 0;
  for (i in line_no) {
    # the line address is delta relative to function:
    printf("\t.long 0x%08x\n", i) > outfile;
    printf("\t.long %d\n", line_no[i]) > outfile;
    printf("\t.long .LF%d\n", line_file[i]) > outfile;
    numlines++;
  }
    
  printf("\n\t.section .rodata\n") > outfile;
  printf("\n\t.align 4\n") > outfile;
  printf(".globl _7LineSym.count\n") > outfile;
  printf("_7LineSym.count:\n") > outfile;
  printf("\t.long %d\n", numlines) > outfile;

  
  #
  # Dump the file name table:
  #

  printf("\n\t.section .rodata\n") > outfile;
  for (i in file_list) {
    printf(".LF%d:\n", file_list[i]) > outfile;
    printf("\t.asciz \"%s\"\n", i) > outfile;
  }

  #
  # Dump the *mangled* name table:
  #

  printf("\n\t.section .rodata\n") > outfile;
  for (i in funtab) {
    printf(".LM%d:\n", i) > outfile;
    printf("\t.asciz \"%s\"\n", funtab[i]) > outfile;
  }

#  printf("\n\t.section .rodata\n") > outfile;
#  printf("\n\t.align 4\n") > outfile;
#  printf(".globl _7SymName.NumSymbols\n") > outfile;
#  printf("_7SymName.NumSymbols:\n") > outfile;
#  printf("\t.long %d\n", numsymbols) > outfile;

  close(outfile);

  #
  # Finally, output the string table.  This must be last because it
  # proceeds via a pipe, and we have no way to forcibly flush that
  # pipe in order to append things after it.
  #
  # Note that not all mangled entries have a corresponding demangled
  # entry -- only those for functions.
  
  strfilt = "c++filt >> " outfile;
  
  for (i in funtab) {
    printf(".LH%d:\n", i) | strfilt;
    printf("\t.asciz \"%s\"\n", funtab[i]) | strfilt;
  }

  close(strfilt);

  
}
