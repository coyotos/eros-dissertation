#
# Copyright (C) 1998, 1999, Jonathan S. Shapiro.
#
# This file is part of the EROS Operating System.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#

# Python script to configure a kernel.
#
# Documentation for this script can be found in the 
# documentation tree at 
#
#         www/devel/ProgGuide/kernel-config.html
#
#

import sys
import string
import os
import glob
import regex

config_name_table = { }
def publish(x):
    config_name_table[x] = globals()[x]

################################################
##
## Global variables:
##
################################################

## Known device classes:

BT_NONE = -1
BT_BASE = 0
BT_PCI = 1
BT_EISA = 2
BT_ISA = 3
BT_SCSI = 4
BT_USB = 5

device_classes = {
    -1 : "BT_NONE",
    0  : "BT_BASE",
    1  : "BT_PCI",
    2  : "BT_EISA",
    3  : "BT_ISA",
    4  : "BT_SCSI",
    5  : "BT_USB"
    }
		   
publish('BT_BASE')
publish('BT_PCI')
publish('BT_EISA')
publish('BT_ISA')
publish('BT_SCSI')

conf_machine = None
conf_arch = None
conf_cpus = [ ]
conf_options = {}
conf_targets = [ ]
conf_busses = [ BT_BASE ]

targdir = "../build/%s" % sys.argv[1]
conf_name = os.path.basename(sys.argv[1])

port_table = { }
port_table[0] = -1

mem_addr_table = { }
mem_addr_table[0] = -1

# file_stack = []
 
# RE_devname = regex.compile("\([a-zA-Z]+\)\([0-9]*\)?");


################################################
##
## Error Messages
##
################################################
NoMachine = "Unknown machine"


################################################
##
## Known machine and CPU types:
##
################################################

machine_types = {
    'x86' : [ "pc" ],
    }
cpu_types = {
    'x86' : [ "i386", "i486", "pentium", "ppro" ],
    }

def cleanup_targdir():
    if not os.path.exists("../build"):
	os.mkdir("../build", 0755)
    elif not os.path.isdir("../build"):
	error("\"../build\" is not a directory")

    if not (os.path.exists(targdir)):
	os.mkdir(targdir, 0755)
    elif not os.path.isdir(targdir):
	error("\"%s\" is not a directory" % targdir)

    # Clean out the existing contents of that directory
    for nm in glob.glob("%s/.*" % targdir):
	os.remove(nm)
    for nm in glob.glob("%s/*" % targdir):
	os.remove(nm)

################################################
##
## Device template class and function to build 
## them
##
################################################

class ConfTemplate:
    Counter = 0

    def __init__(self, name, cls, irq, port, mem, sz):
	self.name = name
	self.index = ConfTemplate.Counter
	self.irq = irq
	self.io_port = port
	self.io_addr = -1
	self.mem_addr = mem
	self.mem_size = sz
	self.devClass = cls
	self.attach = None
	self.nInstance = 0

	if (port and type(port) != type([])):
	    error("Ports must be specified as a list")

	if (mem and type(mem) != type([])):
	    error("Memory addresses must be specified as a list")

	if (port):
	    self.port_table_offset = len(port_table)
	    for p in port:
		port_table[len(port_table)] = p
	    port_table[len(port_table)] = -1	# terminator
	else:
	    self.port_table_offset = 0

	if (mem):
	    self.mem_addr_table_offset = len(mem_addr_table)
	    for p in mem:
		mem_addr_table[len(mem_addr_table)] = p
	    mem_addr_table[len(mem_addr_table)] = -1	# terminator
	else:
	    self.mem_addr_table_offset = 0

	ConfTemplate.Counter = ConfTemplate.Counter + 1

    def irq_value(self):
	if (self.irq == None): return 0
	elif (type(self.irq) == type(5)):
	    return 1 << self.irq;
	else:
	    for i in self.irq:
		value = value | (1 << i)
	    return value;
	

#    def process_options(self, line, can_irq):
#	while (len(line) >= 2):
#	    print line
#
#	    if (can_irq and line[0] == "irq"):
#		self.irq = string.atol(line[1])
#	    elif (line[0] == "port"):
#		self.io_port = string.atol(line[1])
#	    elif (line[0] == "mem"):
#		self.mem_addr = string.atol(line[1])
#	    elif (line[0] == "sz"):
#		self.mem_size = string.atol(line[1])
#	    else:
#		error("unknown option")
#
#	    line = line[2:]
#
#	print line
#	if (len(line) != 0):
#	    error("syntax")

    def __repr__(self):

	attach = self.attach
	if (attach): attach = attach.name

	return "\n{{ %s %-5s irq=%s port=%s mem=%s sz=0x%x attach=%s}}\n" % (
	    device_classes[self.devClass],
	    self.name, 
	    str(self.irq), str(self.io_port), str(self.mem_addr), 
	    self.mem_size, attach)

def error(s):
#    print "Error in \"%s\" at line %d: %s" % (current_file, current_line, s)
    print "Error: %s" % s
    sys.exit(1)


config_table = {};
config_by_name = {};

def add_conf(tmpl):
    global config_table
    global config_by_name
    config_table[tmpl.index] = tmpl
    config_by_name[tmpl.name] = tmpl

def template(name, cls, irq = None, port = None, mem = None, sz = 0):
    global config_by_name

    tmpl = ConfTemplate(name, cls, irq, port, mem, sz)
    add_conf(tmpl)

def base_template(name, irq = None, port = None, mem = None, sz = 0):
    template(name, BT_BASE, irq, port, mem, sz);
publish('base_template')

def isa_template(name, irq = None, port = None, mem = None, sz = 0):
    template(name, BT_ISA, irq, port, mem, sz);
publish('isa_template')

def eisa_template(name, irq = None, port = None, mem = None, sz = 0):
    template(name, BT_EISA, irq, port, mem, sz);
publish('eisa_template')

def pci_template(name, irq = None, port = None, mem = None, sz = 0):
    template(name, BT_PCI, irq, port, mem, sz);
publish('pci_template')

#def device(name, parent=None, irq = None, port = None, mem = None, sz = 0):
#    global config_by_name
#    if (parent):
#	parent = config_by_name[parent]
#    else:
#	error("Templates must identify their parents")
#    tmpl = ConfTemplate(name, parent, irq, port, mem, sz, 'dev ')
#    add_conf(tmpl)
#

next_instance_index = {}

#def instance(name, instname, parent = None, irq = None, port = None, mem = None, sz = 0):
#    global next_instance_index
#
#    if not parent:
#	error("Instances require parents")
#
#    if (name): tmpl = config_by_name[name]
#    else: tmpl = None
#
#    parent = config_by_name[parent]
#
#    if parent.devClass != DC_INSTANCE:
#	error("Instances can only be hung off of instances")
#
#    if tmpl:
#	if (not irq and type(tmpl.irq) == type(5)):
#	    irq = tmpl.irq
#
#	if tmpl.irq and irq != tmpl.irq and not irq in tmpl.irq:
#	    error("Specified irq not in irq list")
#
#	if (not port and type(tmpl.io_port) == type(5)):
#	    port = tmpl.irq
#
#	if (tmpl.io_port and port != tmpl.io_port and not port in tmpl.io_port):
#	    error("Specified port not in port list")
#
#	if not mem and tmpl.mem_addr:
#	    error("Bad memory address specified")
#
#	if tmpl.mem_addr and not mem in tmpl.mem_addr:
#	    error("Specified memory address not in address list")
#
#	name = "%s%d" % (tmpl.name, tmpl.nInstance)
#	if (name != instname):
#	    error("That instance is already allocated")
#
#	tmpl.nInstance = tmpl.nInstance + 1
#    else:
#	RE_devname.match(instname)
#	if (not next_instance_index.has_key(RE_devname.group(1))):
#	    ndx = 0
#	else:
#	    ndx = next_instance_index[RE_devname.group(1)]
#
#	next_instance_index[RE_devname.group(1)] = ndx+1
#
#	if (RE_devname.group(2) != str(ndx)):
#	    error("%s: Unique instances must have sequential indices beginning at 0" % instname)
#	name = instname
#    
#    tmpl = ConfTemplate(name, DC_INSTANCE, parent, irq, port, mem, sz)
#    add_conf(tmpl)
#    print "Defined instance '%s'." % name

def machine(name):
    global conf_machine
    global machine_types

    valid_machines = machine_types[conf_arch]

    if (conf_machine != None):
	error("Machine already defined!");
    elif (name in valid_machines):
	print "Configuring for machine class '%s'." % name
	conf_machine = name
    else:
	error("Unknown machine type")

publish('machine')

def arch(name):
    global conf_arch
    global cpu_types
    if (conf_arch != None):
	error("Architecture already defined!");
    elif cpu_types.has_key(name):
	print "Configuring for architecture '%s'." % name
	conf_arch = name
    else:
	error("Unknown arch type");
publish('arch')

def cpu(name):
    global conf_machine
    global conf_cpus
    global cpu_types

    if (conf_machine == None):
	error("Unknown machine type!");

    valid_cpus = cpu_types[conf_arch]

    if (name in valid_cpus):
#	print "Adding cpu type '%s'." % name
	conf_cpus = conf_cpus + [name]
    else:
	error("Unknown CPU type")
publish('cpu')

def defoption(name):
    global conf_options
    conf_options[name] = 0
publish('defoption')

def option(name):
    global conf_options

    if (not conf_options.has_key(name)):
	error("Unknown option \"%s\"" % name)

    conf_options[name] = 1
publish('option')

def exclude(name):
    global conf_options

    if (not conf_options.has_key(name)):
	error("Unknown option \"%s\"" % name)

    conf_options[name] = 0
publish('exclude')

#def defbus(name):
#    global conf_busses
#    conf_busses[name] = 0
#publish('defoption')
#
def bus(name):
    global conf_busses

    conf_busses = conf_busses + [name]
publish('bus')

#def target(name):
#    global conf_targets
#
#    conf_targets = conf_targets + [conf_name + name]
#publish('target')

def isoption(name):
    if (not conf_options.has_key(name)):
	error("Unknown option \"%s\"" % name)
    return conf_options[name]

publish('isoption')

def ifdevice(name):
    if (config_by_name.has_key(name)): return 1
    else: return 0

publish('ifdevice')

def pseudo_device(name, count=1):
    global targdir

    filename = "%s/%s.h" % (targdir,name)
    out = open(filename, 'w')
    out.truncate()
    cpp_define = "#define N%s %d" % (string.upper(name), count)
    out.write("%s\n" % cpp_define)
    out.close()

#    print (filename, cpp_define)
publish('pseudo_device')


#root = ConfTemplate("root", DC_INSTANCE, parent = None, irq = None, port = None, mem = None, sz=0)
#main = ConfTemplate("mainboard", DC_BUS, parent = root, irq = None, port = None, mem = None, sz=0)

#add_conf(root)
#add_conf(main)

cleanup_targdir()

def include(name):
    execfile(name, config_name_table)
publish('include')

################################################
##
## Now process the machine description file.
##
################################################

execfile(sys.argv[1], config_name_table, {})

#def ctrlr_stmt(l, cls):
#    global parent_objects
#    global config_table
#
#    # Device name takes one of two forms:  cccc or cccc?
#    RE_devname.match(l[1])
#    devname = RE_devname.group(1)
#    isunique = RE_devname.group(2)
#
#    if len(l) < 4:
#	error ("device must be attached!")
#
#    if (l[2] != "at"):
#    	error ("device statement expects 'at'")
#
#    print parent_objects
#    if (not parent_objects.has_key(l[3])):
#    	error ("device must be attached to controller or bus")
#
#    parent = parent_objects[l[3]]
#
#    print "parent is"
#    print parent
#
#    dev = DeviceInfo(devname, parent, cls)
#
#    dev.process_options(l[4:], cls != 'dev')
#
#    config_table = config_table + [ dev ];
#    if (cls != 'dev'):
#	parent_objects[l[0]] = dev;


# print config_table

def dump_ac_types(out):
    out.write("\n/* Driver structure declarations: */\n");
    for i in range(len(config_table)):
	tmpl = config_table[i]
	out.write("extern struct Driver ac_%s;\n" % tmpl.name);

def dump_table(name, tbl, out):
    out.write("\nstatic int32_t %s[] = {" % name)

    for p in range(len(tbl)-1):
	if (p % 8 == 0):
	    out.write("\n")

	value = tbl[p]
	if (value == -1):
	    out.write(" -1,")
	else:
	    out.write(" 0x%x," % value)

    ndx = len(tbl)-1
    value = tbl[len(tbl)-1]

    if (ndx % 8 == 0): out.write("\n")

    if (value == -1):
	out.write(" -1")
    else:
	out.write(" 0x%x" % value)
    out.write("\n};\n")

def dump_conf(i, comma):
	tmpl = config_table[i]
	ac_template_name = "ac_%s" % tmpl.name

	if tmpl.devClass in conf_busses:
	    out.write(
		"  { %-10s, %-8s, &%-13s, 0x%04x, &::ports[%d], &maddr[%d] }%s\n" % (
		    "\"%s\"" % tmpl.name, 
		    device_classes[tmpl.devClass],
		    ac_template_name,
		    tmpl.irq_value(), tmpl.port_table_offset,
		    tmpl.mem_addr_table_offset, 
		    comma))

def dump_templates(out):
    out.write("\n/* Configuration template table: */\n")

    out.write("struct AutoConf\nAutoConf::ConfigTable[] = {\n")

    out.write(
	"/*  %-10s, %-8s, %-13s,  irq , ... */\n" % (
	    "name", "bus", "driver"))

    for i in range(len(config_table)-1):
	dump_conf(i, ",")
    dump_conf(len(config_table)-1, "")

    out.write("};\n");
    out.write("uint32_t AutoConf::MaxConfig = sizeof(AutoConf::ConfigTable)/sizeof(AutoConf);\n")

def dump_optvar(out):
    for b in device_classes.keys():
	if b < 0: continue;	# that one is a placeholder
	if b in conf_busses:
	    out.write("CONFIG_%s=1\n" % device_classes[b][3:])
	else:
	    out.write("CONFIG_%s=0\n" % device_classes[b][3:])
    for o in conf_options.keys():
        if conf_options[o]:
	    out.write("OPT_%s=1\n" % string.upper(o))
	else:
	    out.write("OPT_%s=0\n" % string.upper(o))

def dump_options(out):
    for b in device_classes.keys():
	if b in conf_busses:
	    out.write("OPTIONS += -DCONFIG_%s=1\n" % device_classes[b][3:])
    for o in conf_options.keys():
	if conf_options[o]:
	    out.write("OPTIONS += -D%s=1\n" % string.upper(o))
    for c in conf_cpus:
	out.write("OPTIONS += -DCPU_%s=1\n" % string.upper(c))
    out.write("OPTIONS += -DARCH_%s=1\n" % string.upper(conf_arch))
    out.write("OPTIONS += -DMACHINE_%s=1\n" % string.upper(conf_machine))

#def dump_targets(out):
#    for o in conf_targets:
#        out.write("TARGETS += %s\n" % o)

################################################
##
## Write out the autoconfiguration table
##
################################################

filename = "%s/autoconf.cxx" % targdir
out = open(filename, 'w')
out.truncate()
out.write("/* This file is automatically generated -- DO NOT EDIT */\n\n")

out.write("#include <eros/target.h>\n")
out.write("#include <kerninc/AutoConf.hxx>\n")

dump_ac_types(out)

out.write("\n/* Tables referenced from template list: */\n")
dump_table("ports", port_table, out)
dump_table("maddr", mem_addr_table, out)

dump_templates(out)

out.close()

# print port_table

# dump_options(sys.stdout)

# print globals().keys()



################################################
##
## Now process the file list.
##
################################################

filenames = "files.%s" % conf_machine
src_file_list = []
obj_file_list = []

config_name_table = { }

###############################################
##
## We permit files to be multiply specified,
## because this allows the machine-dependent
## code to pull forward some files that must
## be co-located for IPC performance.
##
###############################################
def file(name, condition = not None):
    global src_file_list
    global obj_file_list
    if (condition and not name in src_file_list):
	src_file_list = src_file_list + [name]
	ofile = os.path.basename(name)
	ofile = os.path.splitext(ofile)[0]
	ofile = ofile + ".o"
	obj_file_list = obj_file_list + [ofile]

publish('file')
publish('include')
publish('ifdevice')
publish('BT_BASE')
publish('BT_PCI')
publish('BT_EISA')
publish('BT_ISA')
publish('BT_SCSI')

for i in conf_options.keys():
    config_name_table[i] = conf_options[i]

for i in conf_cpus:
    config_name_table[i] = 1

#for i in range(len(config_table)):
#    config_name_table[config_table[i].name] = 1

def publish(x):
    config_name_table[x] = globals()[x]

execfile(filenames, config_name_table)

###############################################
##
## Generate the makefile fragment for the files
##
##

makefiletemplate = "Makefile.%s" % conf_machine
makefilename = "%s/Makefile" % targdir

print "building makefile %s" % makefilename

template = open(makefiletemplate, 'r')
out = open(makefilename, 'w')
out.truncate()

for line in template.readlines():
    if (line == "%config\n"):
	out.write("CONFIG=%s\n" % conf_name)
    #elif (line == "%targets\n"):
	#dump_targets(out)
    elif (line == "%optvar\n"):
	dump_optvar(out)
    elif (line == "%options\n"):
	dump_options(out)
    elif (line == "%objects\n"):
	for o in obj_file_list:
	    out.write("OBJECTS += %s\n" % o)
    elif (line == "%depend\n"):
	for f in src_file_list:
	    ofile = os.path.basename(f)
	    suffix =  os.path.splitext(ofile)[1]
	    ofile = os.path.splitext(ofile)[0]
	    out.write("%s.o: $(TOP)/%s\n" % (ofile, f))
	    if (suffix == ".c"):
		out.write("\t$(C_BUILD)\n\n")
	    elif (suffix == ".cxx"):
		out.write("\t$(CXX_BUILD)\n\n")
	    elif (suffix == ".S"):
		out.write("\t$(ASM_BUILD)\n\n")

	    out.write(".%s.m: $(TOP)/%s\n" % (ofile, f))
	    if (suffix == ".c"):
		out.write("\t$(C_DEP)\n\n")
	    elif (suffix == ".cxx"):
		out.write("\t$(CXX_DEP)\n\n")
	    elif (suffix == ".S"):
		out.write("\t$(ASM_DEP)\n\n")
    else:
	out.write(line)

out.close()
