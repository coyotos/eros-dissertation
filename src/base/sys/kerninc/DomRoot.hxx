#ifndef __DOMROOT_HXX__
#define __DOMROOT_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#if EROS_NODE_SIZE == 16
// Structure describing the layout of the machine-independent
// slots in the domain root for convenient access:
struct DomRoot {
  // Slot 0
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 1
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 2
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 3
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 4
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 5
  uint32_t : 32;
  uint32_t faultCode;
  uint32_t faultInfo;
#ifdef BITFIELD_PACK_LOW
  uint8_t drRunState;
  uint8_t flags;
  uint16_t : 16;
#else
  uint16_t : 16;
  uint8_t flags;
  uint8_t runState;
#endif
  // Slot 6
  uint32_t : 32;			// key type and subtype
  union {
    uint32_t all;
    struct {
#ifdef BITFIELD_PACK_LOW
      uint32_t slot0 : 4;
      uint32_t slot1 : 4;
      uint32_t slot2 : 4;
      uint32_t slot3 : 4;
      uint32_t strSize : 16;
#else
      uint32_t strSize : 16;
      uint32_t slot3 : 4;
      uint32_t slot2 : 4;
      uint32_t slot1 : 4;
      uint32_t slot0 : 4;
#endif
    } r;
  } rcvDescrip;
  uint32_t rcvAddress;
  uint32_t : 32;
  // Slot 7
  uint32_t : 32;			// key type and subtype
  union {
    uint32_t all;
    struct {
#ifdef BITFIELD_PACK_LOW
      uint32_t slot0 : 4;
      uint32_t slot1 : 4;
      uint32_t slot2 : 4;
      uint32_t slot3 : 4;
      uint32_t strSize : 16;
#else
      uint32_t strSize : 16;
      uint32_t slot3 : 4;
      uint32_t slot2 : 4;
      uint32_t slot1 : 4;
      uint32_t slot0 : 4;
#endif
    } s;
  } sndDescrip;
  uint32_t sndAddress;
  uint32_t : 32;
  // Slot 8
  uint32_t : 32;			// key type and subtype
  union {
    uint32_t orderCode;
    uint32_t returnCode;
  };
  union {
    uint32_t all;
    struct {
#ifdef BITFIELD_PACK_LOW
      uint32_t keyData : 16;
      uint32_t sndSpec : 4;
      uint32_t rcvSpec : 4;
      uint32_t invTy   : 4;
      uint32_t : 4;
#else
      uint32_t : 4;
      uint32_t invTy   : 4;
      uint32_t rcvSpec : 4;
      uint32_t sndSpec : 4;
      uint32_t keyData : 16;
#endif
    } ic;
  } invCtl;
  uint32_t invKey;
  // Slot 9
  uint32_t : 32;
  uint32_t pc;
  uint32_t sp;
  uint32_t : 32;
  // Slot 10
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 11
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 12
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 13
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 14
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  // Slot 15
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
  uint32_t : 32;
};
#elif EROS_NODE_SIZE == 32
#error "DomRoot layout unimplemented"
#else
#error "Unknown node size"
#endif

#endif // __DOMROOT_HXX__

