#ifndef __CONSOLE_HXX__
#define __CONSOLE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>

// The console is an output-only virtual device that accepts
// characters.  Where the characters go is machine dependent.  It is
// acceptable to implement a bytestream console; the kernel itself
// does not assume that the output device is capable of anything
// fancier than showing printable characters and handling a newline.
//
// That said, most systems provide a kernel-implemented X3.64 (ANSI
// Terminal standard) emulation.  In most cases this is supported by a
// memory-mapped console screen buffer.  This is sufficiently common
// that the kernel common code provides as much of an ANSI emulation
// for memory mapped consoles as I was able to build quickly.
//
// The DEPTH of console memory buffers varies.  The x86, for example,
// really wants to use (char, attribute) pairs.  Because of this, the
// kernel addresses the buffer in terms of POSITION.  POSITION starts
// at the upper left of the screen and proceeds rightward and
// downward.

struct Attr {
  enum Type {
    Normal,
    Blink,
    Underline,
    Bold,
  };
};

struct AnsiConsole {
  bool scroll;			// 1 == scroll, 0 == wrap
  uint32_t state;			// ansi interpreter state machine state

  uint32_t pos;
  uint32_t cols;
  uint32_t rows;

  Attr::Type curAttr;
  
  enum { MaxParam = 2 };
  uint32_t nParams;
  uint32_t param[MaxParam];
  
  void ProcessEsc(uint8_t b);
  void ProcessChar(uint8_t b);
  void SetPosition(uint32_t pos, uint8_t b, Attr::Type a);

  void Scroll(uint32_t startPos, uint32_t endPos, int distance);
  static void Beep();
  static void ShowCursorAt(uint32_t pos);

public:
  AnsiConsole(uint32_t rows, uint32_t cols);
  void ClearScreen();
  
  // Reset ansi interpretation state and clear screen
  void Reset();
  
  // Put() expects an ANSI-formatted byte stream
  void Put(uint8_t c);
  // PutLog() is the same as put, but clears to end of line on NL
  void PutLog(uint8_t c);
};

// This is the raw console byte stream:
struct Console {
  static void Init();
  static void Put(uint8_t c);
  static uint8_t Get();
  static void SetPoll(bool onoff); // set/clear polling mode
  //  static void PutLog(uint8_t c);	// clears to EOL on NL

  // Following shouldn't ever have been part of the standard message
  // logging system:
  static void ShowMessage(char *buf);
  static void ShowMsgChar(char c);
  static void ShowTwiddleChar(char c, uint32_t i);
  static void ResetTwiddle(uint32_t i);
  static void Twiddle(uint32_t i);
} ;

#endif // __CONSOLE_HXX__
