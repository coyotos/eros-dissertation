#ifndef __NETINTERFACE_HXX__
#define __NETINTERFACE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

struct Invocation;

#include <eros/NetInterfaceKey.h>

// Common base class for all network controllers:

struct NetInterface {
  bool isAlloc;
public:
  char      *name;
  uint16_t  devClass;

  io_t      ioAddr;		// I/O register base address
  uint32_t      ioLen;		// length of I/O map
  kva_t     addr;		// memory map addr if mapped
  uint32_t      len;		// length of mem map
  uint32_t      irq;
  uint32_t      xcvr;		// transceiver type

#if 0
  enum {			// status bits
    Attached  = 0x01,		// interface is attached
    Enabled   = 0x02,		// interface is running
    Station   = 0x04,		// interface is accepting station packets
    Broadcast = 0x08,		// interface is accepting broadcast packets
    Multicast = 0x10,		// interface is accepting multicase packets
  };
#endif
  
  uint32_t      devInfo[4];		// Device-specific information words
  
  ThreadPile      rdQ;		// queue of blocked readers
  ThreadPile      wrQ;		// queue of blocked writers

  Timer		  watchDog;	// watchdog timer

  // Network device statistics structure
  struct ni_stats stats;
  struct ni_info  info;
  
#if 0
  uint32_t      nUnits;
#endif
  
  static NetInterface *Alloc();
  static NetInterface *Get(uint32_t n);
  
  void (*updateStats)(NetInterface*);
  
  void (*Invoke)(NetInterface *, Invocation& inv);
  
  NetInterface();

  virtual ~NetInterface()
  {
  }
};

#endif // __NETINTERFACE_HXX__
