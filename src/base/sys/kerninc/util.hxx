#ifndef __UTIL_HXX__
#define __UTIL_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/memory.h>

#ifdef __cplusplus
extern "C"
{
#endif
  void halt(char c) NORETURN;
  void pause();
  void abort(void) NORETURN;

  /* void Stall(uint32_t howLong); */

  char hexdigit(uint8_t);
#ifdef __cplusplus
}
#endif

uint32_t align(uint32_t addr, uint32_t alignment); /* alignment must be power of 2 */
bool IsPrint(uint8_t c);

int strcmp(const char *c1, const char *c2);
void strcpy(char *c1, const char *c2);

inline int strncmp(const char *c1, const char *c2, uint32_t len)
{
  return memcmp(c1, c2, len);
}

extern void qsort(void *a, size_t n, size_t es, int (*cmp)(...));
#endif /* __UTIL_HXX__ */
