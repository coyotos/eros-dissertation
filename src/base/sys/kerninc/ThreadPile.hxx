#ifndef __THREADPILE_HXX__
#define __THREADPILE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/Link.hxx>

// CAUTION! If this representation changes in such a way as to become
// larger than 2 Words, it will break the representation pun in the
// qmk portion of the key data structure.
struct ThreadPile : public Link {
  bool IsEmpty();
  void WakeAll(uint32_t errCode = 0, bool verbose = false);
  void IoWakeAll(uint32_t errCode = 0, bool verbose = false);
  void WakeNext();

  ThreadPile() : Link()
  {
    next = prev = 0;
  };
} ;

// FIX: This only works for uniprocessor:
extern ThreadPile ProcessorQueue;
extern ThreadPile IdlePile;

#endif // __THREADPILE_HXX__
