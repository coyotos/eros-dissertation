#ifndef __SET_HXX__
#define __SET_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Set classes for internal use:
class Set32 {
  uint32_t bits;
public:
  Set32()
  { bits = 0; }

  Set32(uint32_t w)			// for ease of dma channel allocation
  { bits = w; }

  Set32(const Set32& that)
  { bits = that.bits; }

  Set32& operator=(Set32& that)
  { bits = that.bits; return *this; }

  // Single bit accessors/mutators:

  Set32& set(int i)
  { bits |= (1ul << i); return *this; }

  Set32& unset(int i)
  { bits &= ~(1ul << i); return *this; }

  bool isSet(int i) const
  { return (bits & (1ul << i)) ? true : false; }
    
  int firstSetBit() const
  {
    int i;
    for (i = 0; i < UINT32_BITS; i++)
      if (isSet(i))
	return i;
    return -1;
  }
    
  // Misc operations
  void clear()
  { bits = 0; }
  bool isEmpty()
  { return bits ? false : true; }
  
  // Update operators for bitwise style use:
  
  Set32& operator&=(const Set32& that)
  { bits |= that.bits; return *this; }
  Set32& operator|=(const Set32& that)
  { bits &= that.bits; return *this; }
  Set32& operator^=(const Set32& that)
  { bits ^= that.bits; return *this; }

  // Update operators for set algebra style use:

  Set32& operator+=(const Set32& that)
  { bits |= that.bits; return *this; }
  Set32& operator-=(const Set32& that)
  { bits &= ~(that.bits); return *this; }

  // Operators for bitwise style use:
  
  Set32 operator|(const Set32& that) const
  { Set32 s = *this; s |= that; return s; }
  Set32 operator&(const Set32& that) const 
  { Set32 s = *this; s &= that; return s; }
  Set32 operator^(const Set32& that) const
  { Set32 s = *this; s ^= that; return s; }

  // Operators for set algebra style use:

  Set32 operator+(const Set32& that) const
  { Set32 s = *this; s += that; return s; }
  Set32 operator-(const Set32& that) const
  { Set32 s = *this; s -= that; return s; }

  Set32 operator~() const
  { Set32 s = *this; s.bits = ~s.bits; return s; }
} ;

#endif // __SET_HXX__
