#ifndef __CKDIR_HXX__
#define __CKDIR_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

struct CoreDirent {
  CoreDirent *left;
  CoreDirent *right;
  CoreDirent *parent;

  OID         oid;
  ObCount     count;

  lid_t       lid : 28;		// object location ID
  uint32_t    type : 4;

  enum { red = 1, black = 0 };
  
  uint8_t        color;

  inline bool IsLessThan(CoreDirent *n)
  {
    if (oid < n->oid)
      return true;
    return false;
  }

  // Free list management for checkpoint directory entries:
private:
  static bool GrowFreeList();

  static CoreDirent *freeList;
public:
  static void InitFreeList(uint32_t ckLogSz);

  static uint32_t nFree;
  static uint32_t nAlloc;
  static uint32_t nTotal;

public:
  void *operator new(size_t);
  void operator delete(void *);

  static CoreDirent nil_sentinal;

  CoreDirent* Successor();
  CoreDirent* Predecessor();

  // Return min/max of tree rooted at 'this'
  CoreDirent* Minimum();
  CoreDirent* Maximum();

#ifdef DDB
  void DumpTree(int indent);
#endif

  CoreDirent();

  static bool Require(uint32_t num);
  
  // *** DIRENT TREE MANAGEMENT
private:
  static CoreDirent *B_Remove(CoreDirent * root, CoreDirent* node);
  static CoreDirent *B_Insert(CoreDirent * root, CoreDirent* node);

  static CoreDirent *RB_Tree_Remove_Fixup(CoreDirent *root, CoreDirent *x);
  
  static CoreDirent *RB_Tree_LeftRotate(CoreDirent* root, CoreDirent *node);
  static CoreDirent *RB_Tree_RightRotate(CoreDirent* root, CoreDirent *node);
  
  static CoreDirent *RB_Insert(CoreDirent * root, CoreDirent *node);
  static CoreDirent *RB_Remove(CoreDirent * root, CoreDirent *node);

  // *** DEBUGGING SUPPORT
#ifndef NDEBUG
  static CoreDirent *Tree_FindReference(CoreDirent* root, CoreDirent *y);
  static bool Tree_Validate(CoreDirent* root, CoreDirent *y);
#endif

public:  
  static CoreDirent *Tree_Find(CoreDirent* root, OID oid);
  static CoreDirent *Tree_Find_Frame(CoreDirent* root, OID oid);

  inline static CoreDirent *Tree_Insert(CoreDirent *& root, CoreDirent* node)
  { return RB_Insert(root, node); }

  inline static CoreDirent *Tree_Remove(CoreDirent *& root, CoreDirent* node)
  { return RB_Remove(root, node); }

  static void Tree_Dump(int indent, CoreDirent *root);

  inline static int Tree_Compare(CoreDirent *x, CoreDirent *y)
  {
    if (x->IsLessThan(y))
      return -1;
    return 1;
  }

#ifndef NDEBUG
  static bool CheckConsistency(const char *msg);
#endif
#ifdef DDB
  static void ddb_showalloc();
  void ddb_dump();
#endif
};

CoreDirent * const CkNIL = &CoreDirent::nil_sentinal;

#endif /* __CKDIR_HXX__ */
