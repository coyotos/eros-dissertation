#ifndef __INTACTION_HXX__
#define __INTACTION_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

class IntAction {
  friend class IRQ;
  
  void (*fn)(IntAction*);
  uint32_t irq;			// for sanity checking
  const char *driver_name;
  IntAction *next;
  bool wired;
  
public:
  union {
    uint32_t drvr_word;
    void *drvr_ptr;
  };

  void *operator new(size_t);
  void operator delete(void *);

  IntAction(uint32_t _irq,
	    const char *drvr_name,
	    void (*pfn)(IntAction*),
	    uint32_t warg = 0)
  {
    fn = pfn;
    irq = _irq;
    next = 0;
    drvr_word = warg;
    driver_name = drvr_name;
    wired = false;
  }

  uint32_t GetIrq()
  {
    return irq;
  }
  
  bool IsValid()
  {
    return (fn != 0);
  }
  
  bool IsWired()
  {
    return wired;
  }
  
  const char *DriverName()
  {
    return driver_name;
  }
  
  // For static initialization:
  IntAction()
  {
    fn = 0;
  }
} ;

#endif // __INTACTION_HXX__
