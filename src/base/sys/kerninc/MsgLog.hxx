#ifndef __MSGLOG_HXX__
#define __MSGLOG_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/ErosTypes.h>
#include <eros/stdarg.h>
#include <kerninc/util.hxx>

class MsgLog {
public:
  static bool SuppressDebug;

  static void RegisterSink(void (*)(uint8_t));

  static void putbuf(uint8_t *s, uint32_t len);

#ifdef DDB
  static void fatal(const char* msg, ...);
#else
  static void fatal(const char* msg, ...) NORETURN;
#endif

  static void nlputc(char c);
  static void putc(char c);
  static void printf(const char* msg, ...);
  static void dprintf(bool, const char* msg, ...);
  static void print(OID);
  static void print(ObCount);
} ;

#endif // __MSGLOG_HXX__
