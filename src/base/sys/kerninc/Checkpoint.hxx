#ifndef __CHECKPOINT_HXX__
#define __CHECKPOINT_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/Reserve.h>
#include <disk/DiskCkpt.hxx>
#include <kerninc/ObjectHeader.hxx>
#include <kerninc/CkMap.hxx>
#include <kerninc/CkDir.hxx>

struct ObjectHeader;
struct FrameInfo;
struct PagePot;
class Node;

struct CoreGeneration {
  uint32_t   index;		// generation index
  bool canReclaim;		// true if objects in this generation
				// can now be reclaimed to free
				// storage.

  lid_t  nextNodeLid;		// log id of next allocated node.

  CoreDirent *oidTree;		// RB-tree; directory of objects

  // Following structure captures total reservations.  Reservations
  // can shrink if object proves on write to be zero-filled.
  
  struct {			// reserved space:
    uint32_t nDirent;		//   directory entries
    uint32_t nNode;		//   nodes
    uint32_t nPage;		//   data pages
    uint32_t nCapPage;		//   capability pages

    uint32_t nDirFrame;		// directory frames
    uint32_t nNodeFrame;	// node (log pot) frames
    uint32_t nFrames;		// total number of reserved log frames
  } rsrv;
  
  // Following structure captures total allocations.  These cover only
  // those objects that occupy storage in the checkpoint area.
  // Directory entries, threads, and reserves are handled specially,
  // and therefore do not appear here.
  struct {			// reserved space:
    uint32_t nNode;		//   nodes
    uint32_t nPage;		//   data pages
    uint32_t nCapPage;		//   capability pages

    uint32_t nCoreDirent;	// in-core directory entries

    uint32_t nDirFrame;		// directory frames
    uint32_t nNodeFrame;	// node (log pot) frames
    uint32_t nFrames;		// total number of reserved log frames
  } alloc;

  // Following structure counts the number allocated objects that
  // have been released due to subsequent rewriting.
  struct {			// reserved space:
    uint32_t nNode;		//   nodes
    uint32_t nPage;		//   data pages
    uint32_t nCapPage;		//   capability pages
    uint32_t nDirFrame;		// directory frames
    uint32_t nNodeFrame;	// node (log pot) frames
    uint32_t nFrames;		// total number of reserved log frames
  } release;

  uint32_t nThread;		// number of threads
  uint32_t nReserve;		// number of reserves

  bool ReserveFrame();
  void UnreserveFrame();
  
  bool ReserveDirent(uint32_t count = 1);
  bool ReserveNode(uint32_t count = 1);
  bool ReserveDataPage();
  bool ReserveCapPage();

  void UnreserveDirent();
  void UnreservePage();
  void UnreserveCapPage();
  void UnreserveNode();

  void FinishActiveNodeFrame();
  lid_t AllocateLid(lid_t = UNDEF_LID);
  bool  DeallocateLid(lid_t);	// return true if frame now empty
  
  void Allocate(CoreDirent*);	// allocate storage for object
  void Release(CoreDirent*);	// release storage for object
  
  void Init();

  // *** MANAGEMENT FOR THE MAPS
  inline void AddToOidMap(CoreDirent *cld)
  {
    oidTree = CoreDirent::Tree_Insert(oidTree, cld);
  }

  inline void RemoveFromOidMap(CoreDirent *cld)
  {
    oidTree = CoreDirent::Tree_Remove(oidTree, cld);
  }

  inline CoreDirent *FindObject(OID oid)
  {
    return CoreDirent::Tree_Find(oidTree, oid);
  }
  inline CoreDirent *FindFrame(OID oid)
  {
    return CoreDirent::Tree_Find_Frame(oidTree, oid);
  }
  
  inline CoreDirent *FirstDirEntry()
  {
    return oidTree->Minimum();
  }
  
  void Tree_Dump()
  {
    CoreDirent::Tree_Dump(0, oidTree);
  }

  bool CheckConsistency(bool allAllocated);

  bool ReclaimLogPage();
  void Reclaim();
#ifdef DDB
  void ddb_showalloc();
#endif
};

class Checkpoint {
public:
  enum { // Generations:
    current = 0,
    last_ckpt = 1,
    first_migrated = 2,
    nGeneration = 3
  };

private:
  // Bitmap of free locations in the checkpoint area:
  static AllocMap   allocMap;
public:
  static uint32_t   totLogFrame; // total in ckpt area

  static uint32_t   ckGenLimit;	// max size of single checkpoint generation

  static uint32_t   nCheckpointsCompleted;
  static uint32_t   nAvailLogFrame;
  static uint32_t   nReservedLogFrame;
  static uint32_t   nAllocatedLogFrame;
  static uint32_t   nMasterPage;

  static lid_t FindFreeFrame();
  static lid_t AllocateLid(lid_t);
  static bool ReserveLogFrame(); // return true on success
  static bool DeallocateLid(lid_t); // return true if now empty
  static bool FrameIsEmpty(lid_t lid)
  {
    return allocMap.IsFree(lid / EROS_OBJECTS_PER_FRAME);
  }
  
private:
  static logframe_t LastAllocatedFrame;

  friend class CoreDirent;
  static CoreGeneration coreGeneration[nGeneration];
  
/* round x up to a multiple of y: */
#define NUM_CONTAINERS(x, y) (((x) + (y) - 1) / (y))

  enum {
    nThreadDirPage = NUM_CONTAINERS(KTUNE_NTHREAD, ThreadDirPage::maxDirEnt),
    nReserveDirPage = NUM_CONTAINERS(MAX_CPU_RESERVE, ReserveDirPage::maxDirEnt),
  };
#undef NUM_CONTAINERS

  // pointers to the in-core copy of the last written disk checkpoint
  // master header.
  static DiskCheckpoint *lastCkptHdr;
  static ObjectHeader   *lastCkptObHdr;
  static lid_t          lastCkptHdrLid;

  // pointers to the in-core copy of the next disk checkpoint
  // master header.
  static DiskCheckpoint *nextCkptHdr;
  static ObjectHeader   *nextCkptObHdr;
  static lid_t          nextCkptHdrLid;

  static void SwapCheckpointHeaders();
  static void SwapThreadDirs();
  
  // pointers to the pre-reserved in-core pages that will be used to
  // write the next disk generation directory.  The associated log
  // locations are reserved at startup, which is slightly wasteful,
  // but not terribly so.  They are therefore not counted against the
  // associated core generations, since only the current and last
  // checkpoints need them.  Index 0 always refers to current
  // checkpoint, Index 1 always refers to last stable checkpoint.
  static ObjectHeader   *threadDirHdr[nThreadDirPage];
  static ThreadDirPage  *threadDirPg[nThreadDirPage];
  static lid_t          lastThreadDirLid[nThreadDirPage];
  static lid_t          nextThreadDirLid[nThreadDirPage];

  static ObjectHeader   *reserveDirHdr[nReserveDirPage];
  static ReserveDirPage *reserveDirPg[nReserveDirPage];
  static lid_t          lastReserveDirLid[nReserveDirPage];
  static lid_t          nextReserveDirLid[nReserveDirPage];
  
  static ObjectHeader   *nextDirPageHdr;
  static CkptDirPage    *nextDirPage;
  
  // Core Page frame to write directory must be preallocated to ensure
  // we have space for it:
  static ObjectHeader *diskDirPageHdr;


  static bool enabled;
  
  static void InitNewCurrentGeneration();
  
  // Return the object in the youngest generation >= generation
public:
  static CoreDirent *FindObject(OID value, ObType::Type oty,
				uint8_t generation);

private:
  // Checkpoint load support:
  static void AddCkDirent(CkptDirent& de, uint8_t generation);
  static void LoadDirectoryHeaders();
  static void ReloadSavedReserves();  
  static void ReloadSavedThreads();  
  static void ReloadSavedDirectory();  
  static void ReserveCoreDirPages();

  static void AllocateMasterLid(lid_t ll);
public:
  // Used by persistence logic to decide which object to recover from
  // the log.  Note that this design RELIES on the fact that all
  // checkpoint entries for a given frame are zapped when the frame is
  // reallocated.
  static ObjectHeader *LoadCurrentPage(CoreDirent *);
  static Node *LoadCurrentNode(CoreDirent *);

  static void CleanCkptFrames(uint8_t curType, FrameInfo&);
  static uint8_t GetFrameInfo(FrameInfo& fi, PagePot *pagePot,
			      ObCount& count, uint8_t newType);
  static void RetagFrame(OID oid, uint8_t newTag);

  // Called each time an object's modified bit is set.  If the object
  // is paged out and then modifier again, this needs to be called
  // again. Internally handles creating new dirent structures if
  // required.  This will not be prompt if there was insufficient
  // storage available -- the process will get queued on either the
  // core page availability stall queue or the checkpoint completion
  // stall queue.

  static void RegisterDirtyObject(ObjectHeader *pObj);

  static void Init();
  static void LoadDirectory();
  static void StartThreads();

  // Called by the persist range mount logic:
  static void AttachRange(lid_t low, lid_t hi);

  static lid_t GetLidForPage(ObjectHeader *pObj);
  static void WriteNodeToLog(Node *pObj);

private:
  // SUPPORT ROUTINES FOR TakeCheckpoint():
  static void MarkDirtyNodesForCkpt();
  static void MarkDirtyPagesForCkpt();
  static void InitNextCkptHdr();
  static void SnapshotThreads();
  static void SnapshotReserves();
  static void AgeGenerations();

  enum MigrationStatus {
    mg_Idle,
    mg_StartCheckpoint,		// let I/O in progress settle
    mg_StabilizeNodes,		// stabilizing last checkpoint
    mg_StabilizePages,		// stabilizing last checkpoint
    mg_WriteDir,		// write ckpt directory and thread dir
    mg_DrainCheckpoint,		// wait for everything to drain
    mg_WriteHeader,		// write ckpt header
    mg_StartMigration,
    mg_MigrateObjects,
    mg_DrainMigration,
    mg_UpdateRangeHeaders,
    mg_nState
  };

  static char MigStateAbbrev();
  static MigrationStatus migrationStatus;
  static const char *MigStateName();
  
  // SUPPORT ROUTINES FOR ProcessMigration():
  static void DrainLastLogPot();
  static void StabilizeNodes();
  static void StabilizePages();
  static void AppendDiskDirent(CoreDirent* cde);
  static void DrainDirentPage();
  static void WriteCkptDirectory();
  static void DrainCheckpoint();
  static void WriteCkptHeader();
  static void DrainLastObjectPot();
  static void DrainLastTagPot();
  static ObjectHeader * GetObjectPot(OID);
  static void InitNodePot(struct DiskNode *, OID);
  static bool UpdateTagPot(CoreDirent *, ObCount);
  static void MigratePage(CoreDirent*);
  static void MigrateNode(CoreDirent*);
  static void MigrateObjects();
  static void DrainMigration();
  static void UpdateRangeHeaders();

  // Callbacks associated with above:
  static void DrainCheckpointCallback(struct DuplexedIO *);
  static void DrainMigrationCallback(struct DuplexedIO *);

public:
  static void TakeCheckpoint();
  static void StartMigration(bool needStabilize);
  static bool ProcessMigration();
  static bool IsStable()
    {
      return (coreGeneration[last_ckpt].alloc.nCoreDirent == 0); 
    }
  
  static bool CheckConsistency(const char *msg, bool allAllocated);
  
#ifdef DDB
  static void ddb_dumpdir(uint32_t generation);
  static void ddb_dump_curdir()
  { ddb_dumpdir(0); }
  static void ddb_dump_ckdir()
  { ddb_dumpdir(1); }
  static void ddb_dump_mig_status();
  static void ddb_dump_ndtree(uint32_t gen);
  static void ddb_dump_pgtree(uint32_t gen);
  static void ddb_showalloc();
  static void ddb_showhdrloc();
#endif

};

#endif // __CHECKPOINT_HXX__
