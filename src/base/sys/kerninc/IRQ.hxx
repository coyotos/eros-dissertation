#ifndef __IRQ_HXX__
#define __IRQ_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

class IntAction;

class IRQ {
  static IntAction* irqActions[NUM_HW_INTERRUPT];

public:
  static volatile uint32_t PendingInterrupts;
private:
  static uint32_t DisableDepth;

  static uint32_t enableMask;
  
  // These are inline because they are only called in one place.
  static void EnablePIC(uint32_t interrupt);
  static void DisablePIC(uint32_t interrupt);
public:
  static uint32_t GetEnableMaskFromPIC();

public:
  static inline void DISABLE();
  static inline void ENABLE();
  static inline bool INTERRUPTS_ENABLED()
  {
    return (DisableDepth == 0) ? true : false;
  }

  static inline uint32_t DISABLE_DEPTH()
  {
    return DisableDepth;
  }

  static inline bool InterruptsAreEnabled()
  {
    return (DisableDepth ==0 ) ? true : false;
  }

  // Interrupt initialization
  static bool WireShared(IntAction*);
  static bool WireExclusive(IntAction*);
  static void Unwire(IntAction*);

#ifdef DDB
  static IntAction* ddb_getaction(uint32_t interrupt)
  {
    return irqActions[interrupt];
  }
#endif
  
  static void Enable(uint32_t interrupt)
  {
    enableMask |= (1u << interrupt);
    EnablePIC(interrupt);
  }
  
  static void Disable(uint32_t interrupt)
  {
    enableMask &= ~(1u << interrupt);
    DisablePIC(interrupt);
  }
  
  static bool IsEnabled(uint32_t interrupt)
  {
    return (enableMask & (1u << interrupt)) ? true : false;
  }

  static uint32_t BeginProbe();
  static int EndProbe(uint32_t);

  // Public so can be called from machine-specific interrupt code.
  // returns false if no actions to run, so interrupts can be
  // disabled.
  static bool RunActions(uint32_t interrupt);

  static void RunInterruptHandlers();
};

#include <machine/IRQ-inline.hxx>

#endif // __IRQ_HXX__
