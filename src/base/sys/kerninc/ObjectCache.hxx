#ifndef __OBJECTCACHE_HXX__
#define __OBJECTCACHE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "ObjectHeader.hxx"

class VCPU;

struct Depend;

class ObjectCache {
  static uint32_t nNodes;
  static Node *nodeTable;

  static uint32_t nPages;
  static ObjectHeader *coreTable;

  static uint32_t nFreeNodeFrames;
  static uint32_t nFreePageFrames;
  static uint32_t nReservedIoPageFrames;
  static uint32_t nCommittedIoPageFrames;
  
  static Node *firstFreeNode;
  static ObjectHeader *firstFreePage;

  static void AllocateUserPages();
public:
  // Initialization
  static void Init();

  // Page management:
  static ObjectHeader *PageToObHdr(kva_t pageva);

  static kva_t ObHdrToPage(const ObjectHeader *pHdr)
    {
      return pHdr->pageAddr;
    }

  static ObjectHeader *OIDtoObHdr(uint32_t oidLo, uint16_t oidHi);
    
  static void AgeNodeFrames();
  static void WaitForAvailableNodeFrame();
  static void RequireNodeFrames(uint32_t n);
  static Node *GrabNodeFrame();
  static void ReleaseNodeFrame(Node *);

  static kva_t messageScratchPageKva;
  
protected:
  static void AgePageFrames();
  static void WaitForAvailablePageFrame();
  static ObjectHeader *DoGrabPageFrame();
public:
  static uint32_t TotalPages()
  {
    return nPages;
  }
  static uint32_t TotalNodes()
  {
    return nNodes;
  }
  
  // For an explanation of why this particular set of calls is needed,
  // see Persist.cxx
  static void ReserveIoPageFrame()
  {
    WaitForAvailablePageFrame();
    nReservedIoPageFrames++;
  }
  static void CommitIoPageFrame()
  {
    nCommittedIoPageFrames = nReservedIoPageFrames;
  }
  static void ReleaseUncommittedIoPageFrames()
  {
    nReservedIoPageFrames = nCommittedIoPageFrames;
  }
  
  static void RequirePageFrames(uint32_t n);
  static ObjectHeader *IoCommitGrabPageFrame();	// must be prompt!
  static ObjectHeader *GrabPageFrame();
  static void ReleasePageFrame(ObjectHeader *);

  static Node *ContainingNode(void *);

  // For use by the consistency checker:
  static uint32_t NumCorePageFrames()
  {
    return nPages;
  }
  static uint32_t NumCoreNodeFrames()
  {
    return nNodes;
  }
  
  static ObjectHeader *GetCorePageFrame(uint32_t ndx);
  static Node *GetCoreNodeFrame(uint32_t ndx);

#ifndef NDEBUG
  static bool ValidNodePtr(const Node *pNode);
  static bool ValidPagePtr(const ObjectHeader *pObj);
  static bool ValidKeyPtr(const struct Key *pKey);
#endif

  static void CleanPageFrame(ObjectHeader *pObj);
#ifdef DDB
  static void ddb_dump_pinned_objects();
  static void ddb_dump_pages();
  static void ddb_dump_nodes();
#endif
};

#endif // __OBJECTCACHE_HXX__
