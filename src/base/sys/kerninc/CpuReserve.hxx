#ifndef __CPURESERVE_HXX__
#define __CPURESERVE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Data structures for CPU reserve management.  The current kernel
// implementation uses a fixed-priority strategy for implementing
// cpu reserves.

#include <kerninc/Link.hxx>
#include <kerninc/SysTimer.hxx>

struct CpuReserve {
  // Following two items MUST be first, because they are directly
  // manipulated by the low-level interrupt logic.
  
  uint64_t residQuanta;		// remaining duration this quanta
  bool    expired;		// set TRUE when quanta has expired
  
  uint64_t residDuration;	// remaining duration this period
  
  uint64_t quanta;		// ticks till expiration
  uint64_t duration;		// in ticks
  uint64_t period;		// in ticks
  uint64_t start;		// when to start this reserve

  bool active;			// true if running under reserved cycles
  int curPrio;			// priority at which we are currently running.
  
  int normPrio;			// priority when reserve NOT active
  int rsrvPrio;			// priority when reserve is active

  Timer reserveTimer;		// reserve period timer

  Link threadChain;		// circularly linked list of threads
				// under this reserve 

  void OnQuantaExpiration();	// called whenever residQuanta goes to
				// zero

  void Deplenish();		// called when duration of active
				// reserve is exhausted.
  void Replenish();		// called at start of each period

  CpuReserve();
  CpuReserve(int inactivePrio);
  
  static void Replenish(Timer*); // used by timer logic.

  void AddUserThread(Thread *t);
  void AddKernelThread(Thread *t);
  
  void QuantaExpired();

  static CpuReserve *CpuReserveTable;
  static CpuReserve KernIdleCpuReserve;
  static CpuReserve KernThreadCpuReserve;

  static CpuReserve *Current;

  static void AllocUserCpuReserves();

  void Reset();
  
private:
  void AdjustCurPrio(int prio);
};

#endif // __CPURESERVE_HXX__
