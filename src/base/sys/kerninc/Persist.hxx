#ifndef __PERSIST_HXX__
#define __PERSIST_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/LowVolume.hxx>
#include <disk/ErosTypes.h>
#include "ObjectHeader.hxx"

// The Persist class is the central control point for the entire
// single-level store illusion.  If you need a OID, you go talk to
// Persist.

class CoreDivision;
struct ObjectHeader;
struct DuplexedIO;
class Partition;

// Structure of the in-core master division table.  Note that this
// will need to change to handle shadowed Divisions.
struct CoreDivision : public Division {
#if 0
  uint64_t nPages;		// Number of pages in this division,
				// less any page pots!
#endif

  Partition *partition;
  uint16_t divisionID;
  
  CoreDivision(struct Partition*, const Division&);

  CoreDivision()
  {
    partition = 0;
  }

  // Pool management:
  void *operator new(size_t);
  void operator delete(void *v)
  {
    ((CoreDivision*) v)->partition = 0;
  }

  bool IsDuplexOf(const CoreDivision *cd);
};


struct FrameInfo {
  CoreDivision *cd;
  OID oid;
  OID obFrameOid;
  uint32_t obFrameNdx;
  uint32_t relObFrame;
  uint32_t clusterNo;
  OID tagFrameOid;
  uint32_t relTagFrame;
  uint32_t tagEntry;

  FrameInfo(OID oid);
} ;

class Persist {
  static void ReadPageFrame(CoreDivision *pcd, uint32_t relFrame,
			    ObType::Type, OID oid,
			    ObCount count);
  static void ReadLogFrame(lid_t lid);

  // Used to implement the write:
  static void WritePageTo(struct ObjectHeader *pObj, CoreDivision *cd,
			  uint32_t atSector,
			  bool synchronous = false,
			  void (*callBack)(DuplexedIO*) = 0);
  static Node * CopyDiskNode(FrameInfo &fi, struct DiskNode *nodePot);
public:
  static CoreDivision* FindDivision(DivType dt, OID oid);

#ifdef DDB
  static void ddb_DumpDivisions();
#endif
  
  // Register an element in the per-device division table:
  static void RegisterDivision(Partition *, const Division&);
  static void UnregisterDivisions(Partition *);
  
  // Obtain the page/node associated with the given OID, bringing it
  // in to core if necessary. Must be called from a thread.
  static ObjectHeader *GetObjectPot(FrameInfo&);
  static ObjectHeader *GetTagPot(FrameInfo&);

  // Fetch the requested object IF IT EXISTS, else return 0.
  // FIX: This interface does not readily lend itself to error
  // reporting...
  static ObjectHeader *GetPage(OID oid, ObCount count,
			       ObType::Type oty, bool useCount);
  static Node *       GetNode(OID oid, ObCount count, bool useCount);

  static ObjectHeader *ZeroCkFrame(lid_t lid);
  static ObjectHeader *GetCkFrame(lid_t lid);
  static ObjectHeader *KernGetCkFrame(lid_t lid);

  // Called by the migrator only:
  static void WritePageToHome(struct ObjectHeader *pObj, OID oid);
  static void WriteRangeHeaders(struct ObjectHeader *pObj);

  // Called by the checkpointer only:
  static void WritePageToLog(ObjectHeader *pObj, lid_t lid,
			     bool synchronous = false,
			     void (*callBack)(DuplexedIO*) = 0);

  // Called by the ager:
  static void WritePage(ObjectHeader *pObj, bool synchronous = false);
  
  // Return true if one or more of the mounted divisions are
  // checkpoint log divisions:
  static bool HaveCkptLog();

  static bool CheckSequenceNumbers(uint64_t seqNo);
};
#endif // __PERSIST_HXX__
