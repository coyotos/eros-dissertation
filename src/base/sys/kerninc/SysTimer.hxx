#ifndef __SYSTIMER_HXX__
#define __SYSTIMER_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

struct Thread;
struct Timer;

struct SysTimer {
protected:
  static volatile uint64_t now;		// in TICKS
  static volatile uint64_t wakeup;	// in TICKS

  static void ResetWakeTime();
public:
  static void Wakeup(class IntAction *);
  
  static void AddSleeper(Thread&);
  static void CancelAlarm(Thread&);

  static void AddTimer(Timer&);
  static void CancelTimer(Timer&);

  static uint64_t Now()
  {
    uint64_t t1;
    uint64_t t2;

    do {
      t1 = now;
      t2 = now;
    } while (t1 != t2);

    return t1;
  }

  static void BootInit();

  static struct ObjectHeader *TimePageHdr;
  static void InitTimePage();
};

struct Timer {
  uint64_t wakeTime;		// if asleep, when to wake up, in milliseconds
  Timer *nextTimer;

  union {
    uint32_t client_word;
    void *client_ptr;
  };
  
  void (*wakeFun)(Timer*);

  void WakeupIn(uint64_t ms, void (*wkfn)(Timer*));
  void WakeupAt(uint64_t ms, void (*wkfn)(Timer*));
  void WakeupAtTick(uint64_t tick, void (*wkfn)(Timer*));

  void Disarm()
  {
    SysTimer::CancelTimer(*this);
    wakeTime = 0ll;
  }
};

#endif // __SYSTIMER_HXX__
