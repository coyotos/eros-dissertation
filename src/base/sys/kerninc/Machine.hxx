#ifndef __MACHINE_HXX__
#define __MACHINE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// This class captures the coupling between machine dependent and
// machine independent initialization.
class Machine {
public:
  static void BootInit();	// machine DEPENDENT
  static void InitCoreSpace();	// machine DEPENDENT
  static void InitHardClock();	// machine DEPENDENT
  //  static void AutoConfigure();	// machine DEPENDENT

  static void FlushTLB();
  static void FlushTLB(klva_t va);
  
  static void Machine::MarkMappingsForCOW();

  // Hardware clock support:
  static uint64_t MillisecondsToTicks(uint64_t ms);
  static uint64_t TicksToMilliseconds(uint64_t ticks);
  static void SpinWaitMs(uint32_t ms);
  static void SpinWaitUs(uint32_t us);

  // Generic interfaces, but machine-specific routines
  static uint32_t GetCpuType();
  static const char *GetCpuVendor();

  // Hardware event tracing support:
  static const char *ModeName(uint32_t mode);
  static bool SetCounterMode(uint32_t mode);
  static void ClearCounters();
  static void EnableCounters();
  static void DisableCounters();
  static uint64_t ReadCounter(uint32_t which);

#ifdef EROS_HAVE_FPU
  // FPU support
  static void InitializeFPU();
  static void DisableFPU();
  static void EnableFPU();
#endif

  // Ramdisk support:
  static uint32_t GetRamDiskSize();
  static uint32_t GetRamDiskAddress();

  static bool IsDebugBoot();

  static void GetHardwareTimeOfDay(struct TimeOfDay&);
  
  static void HardReset();

  // Mounting a disk volume is machine specific:
  static void MountDisk(struct DiskUnit&);

  enum BusType {
    bt_Unknown,			// unknown bus type
    bt_ISA,			// x86 ISA bus
    bt_EISA,			// x86 EISA bus
    bt_MCA,			// x86 MCA bus
    bt_PCI,			// PCI bus
    bt_VME,			// VME bus
    bt_SBUS,			// sun SBUS
  } ;
  
  // Return the bus architecture of the *primary* system bus -- this
  // tends to influence things like DMA bounce buffers.
  static uint32_t BusArchitecture();

  static uint16_t htonhw(uint16_t hw);
  static uint32_t htonw(uint32_t w);
  static uint16_t ntohhw(uint16_t hw);
  static uint32_t ntohw(uint32_t w);
  static int FindFirstZero(uint32_t w);

#if 0
  // Used to map send and receive buffers:
  static void MapBuffer(kva_t va, kpa_t p0, kpa_t p1);
#endif

#ifdef USES_MAPPING_PAGES
  static void SetMappingTable(kpa_t pAddr);
  static kpa_t GetMappingTable();
#endif
  static void EnableVirtualMapping();
};

#include <machine/Machine-inline.hxx>

#endif // __MACHINE_HXX__
