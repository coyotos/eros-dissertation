#ifndef __PHYSMEM_HXX__
#define __PHYSMEM_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/kernel.hxx>
#include <machine/MemClass.hxx>

class PhysMem {
  static struct MemInfo {
    uint32_t orig_base;
    uint32_t base;
    uint32_t top;
  } memClass[Mc_NUM_MEM_CLASS];

  static uint32_t physMemTop;
  static uint32_t kernMemTop;

public:
  static void Init();
  
  static void *Alloc(size_t sz, MemClass, uint32_t alignment = 4);

  static uint32_t KernelPages();
  static uint32_t PhysicalPages();

  static uint32_t AvailBytes();
  static uint32_t AvailPages();


  static uint32_t AvailBytes(MemClass mc)
  {
    return (memClass[mc].top - memClass[mc].base);
  }
  static uint32_t AvailPages(MemClass mc)
  {
    return AvailBytes(mc) / EROS_PAGE_SIZE;
  }
  
  static void PrintStatus();
};

#endif // __PHYSMEM_HXX__
