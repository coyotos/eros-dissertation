#ifndef __TASK_HXX__
#define __TASK_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// A task is just a function and an argument.  In this implementation,
// there can be up to 32 tasks, but that is readily extended.

#include <kerninc/SysTimer.hxx>

typedef uint32_t TaskVec;

struct Task {
  static TaskVec   activeTasks;
  
  enum { MaxTask = sizeof(TaskVec) * 8 };
  
  void (*fn)(Task *);

  union {
    uint32_t drvr_word;
    void *drvr_ptr;
  };

  inline void ActivateTask()		// activate current task
  {
    uint32_t tsk = this - TaskTable;

    activeTasks |= (1 << tsk);
  }

  Task(void (*pfn)(Task*), uint32_t warg = 0)
  {
    fn = pfn;
    drvr_word = warg;
  }

  // For static initialization:
  Task()
  {
    fn = 0;
  }

  void *operator new(size_t);
  void operator delete(void *);



  static Task TaskTable[MaxTask];

  static void ActivateTask(uint32_t tsk)
  {
    activeTasks |= (1 << tsk);
  }
  static void ActivateTaskVec(TaskVec tasks)
  {
    activeTasks |= tasks;
  }
  static void RunActiveTasks();
};

#endif // __TASK_HXX__
