#ifndef __OBJECTHEADER_HXX__
#define __OBJECTHEADER_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <disk/ErosTypes.h>
#include <disk/KeyRing.hxx>
#include <machine/KernTune.hxx>

// Enable the option OB_MOD_CHECK **in your configuration file** if
// you are debugging the kernel, and want to verify that modified bits
// are getting properly set.  Note that this option causes the
// ObjectHeader structure to grow by a word, which requires that a
// constant be suitably adjusted in the fast path implementation.  You
// therefore should NOT simply #define the value here.

// "Object Type" field values.  These capture simultaneously whether
// the object is a page or a node and how the object is currently
// being interpreted.  Note that the interpretation is a runtime
// artifact, and is never stored to the disk.

struct ObType {
  enum Type {
    NtUnprepared,		// unprepared vanilla node
    NtSegment,
    NtProcessRoot,
    NtKeyRegs,
    NtRegAnnex,
    NtFreeFrame,		// unallocated
    NtLAST_NODE_TYPE = NtFreeFrame,
    PtDataPage,			// page holding a user data Page
    PtCapPage,			// page holding a user capability Page
    PtAllocPot,			// page holding page allocation counts
    PtDriverPage,		// used by some kernel driver
    PtObjectPot,		// page holding an object pot
    PtLogPage,			// page holding an uninterpreted page
				// of log data.
    PtRawPage,			// a from-disk page that does not have
				// a role
#ifdef USES_MAPPING_PAGES
    PtMappingPage,		// used in a virtual mapping table
#endif
    PtFreeFrame			// unallocated
  };

#ifdef DDB
  static const char *ddb_name(uint8_t);
#endif
};

// OBJECT AGING POLICY:
//
// Objects are brought in as NewBorn objects, and age until they reach
// the Invalidate generation.  At that point all outstanding keys are
// deprepared.  If they make it to the PageOut generation we kick them
// out of memory (writing if necessary).
//
// When an object is prepared, we conclude that it is an important
// object, and promote it back to the NewBorn generation.
//
// PROJECT: Some student should examine the issues associated with
// aging policy management.

struct Age {
  enum {
    NewBorn = 0,		// just loaded into memory
    NewLogPg = 2,		// not as important as user pages.
    Invalidate = 6,		// time to invalidate to see if active
    PageOut = 7,		// time to page out if dirty
  };
};


struct KeyRing;
struct SegWalkInfo;
struct ObjectTable;
struct Process;
struct Node;

#define OHAZARD_NONE      0x1u
#define OHAZARD_WRITE     0x1u
#define OHAZARD_READ      0x2u
#define OHAZARD_READWRITE 0x2u


#define OFLG_DIRTY	0x01u	/* object has been modified */
#define OFLG_REDIRTY	0x02u	/* object has been modified since */
				/* write initiated */
#if 0
#define OFLG_PIN	0x04u	/* object pinned in memory */
#endif
#define OFLG_CURRENT	0x08u	/* current version */
#define OFLG_CKPT	0x10u	/* checkpoint version */
#define OFLG_IO		0x20u	/* object has active I/O in progress */
#define OFLG_DISKCAPS	0x40u	/* capabilities to this version exist */
				/* on disk */

// Note that the first three words of this structure must overlay
// properly with the KeyRing structure defined in KeyType.hxx

struct ObjectHeader {
  union {
    KeyRing	kr;

    // Special relationship pointers if prepared node or mapping page
    ObjectHeader  *producer;	// if mapping page
    ObjectHeader  *products;	// if segment or page
    Process	  *context;	// if prepared as Domain
    /*    ObjectHeader  *prevFree; */
    void          *freeList;	// used by malloc
#if 0
    DuplexedIO    *ioReq;	// if actively inbound
#endif
  };

  kva_t   pageAddr;		// speed up ObHdrToPage!
  
  ObjectHeader   *next;		// used by various chains
  
#ifdef OB_MOD_CHECK
  uint32_t		CalcCheck() const;
#endif

  union {
    struct {
      ObCount	allocCount;
      OID   	oid;

#ifdef OB_MOD_CHECK
      uint32_t		check;		// check field
#endif

      // Note: when I do background keys they should be unioned with the
      // allocation count, since they only apply to mapping pages.

      uint32_t	ioCount;	// for object frames
      //    uint32_t	refCount;	// for mapping pages
    } ob;

    struct {
      struct Node *redSeg;	// pointer to slot of keeper that
				// dominated this mapping frame
      uint64_t     redSpanBlss;	// blss of seg spanned by redSeg
      bool         redProducer;
    } mp;
    
    // The structure above is relevant to nodes also.  The structures
    // below are only relevant to page-sized frames.
    struct {
      int32_t  sz;		// object size.  -1 means encoded internally
      uint32_t mtype;		// malloc type
    } malloc;
    struct {
      // NOTE THIS IS NOT YET USED, AND REPRESENTS VERY TENTATIVE IDEAS
      ObjectHeader *first;	// first frame in physical range
      uint32_t npage;		// number of pages in physical range
    } p_range;
  };
  
  uint8_t		flags;
  inline void SetFlags(uint32_t w)
  {
    flags |= w;
  }
  inline uint32_t GetFlags(uint32_t w) const
  {
    return (flags & w);
  }
  inline void ClearFlags(uint32_t w)
  {
    flags &= ~w;
  }

  inline void SetDirtyFlag()
  {
    flags |= (OFLG_DIRTY|OFLG_REDIRTY);
  }
  inline uint32_t IsDirty() const
  {
    return GetFlags(OFLG_DIRTY|OFLG_REDIRTY);
  }
  
  uint8_t	obType;		// page type or node prepcode
  uint8_t	age;

  /* FIX: producerNdx isn't really big enough given 32-slot nodes; on
     machines that need this, it must hold log2(ndsz). */
  uint8_t	producerNdx : 3; // deal with map tbl/node non-congruence.
  uint8_t       producerBlss : 5; // biased lss of map tbl
				// producer. NOTE not the key, the object.
  uint8_t	rwProduct : 1;	// indicates mapping image is RW version
  uint8_t	caProduct : 1;	// indicates mapping image is callable version

  bool IsFree() const
    {
      return (obType == ObType::NtFreeFrame || obType == ObType::PtFreeFrame);
    }
  
  // Object pin counts.  For the moment, there are several in order to
  // let me debug the logic.  Eventually they should all be mergeable.
  //
  //   userPin -- pins held by the user thread.  Automatically
  //              released whenever the thread yields or leaves the
  //		  kernel.
  //
  //   kernPin -- pins held by a kernel thread.  Must be released
  //              explicitly.
  //
  // userPin and kernPin are acquired and released via the same
  // interface -- Pin/Unpin.  Unpin is a no-op if the caller is a user
  // thread.
  //
  // Eventually, we will add
  //
  //   prstPin -- Pins held by virtue of a pinned object authority.
  //              These pins are PERSISTENT.  For every such pin there
  //              is a corresponding entry in the pinned object table,
  //              which is saved and restored across checkpoint/restart.
  //
  // While I think this last could be merged, keeping it seperate has
  // the advantage that the consistency checker can validate it.
  //
  // If ANY of the pin counts is nonzero, the 'pinned' bit is set.
  //
  
  static      uint8_t CurrentTransaction; // current transaction number
  inline static void BeginTransaction()
    {
      CurrentTransaction += 2;
    }
  

  uint8_t		userPin;
  uint8_t		kernPin;
  
  void          KernPin();	// object is pinned for kernel reasons
  void          KernUnpin();	// object is pinned for kernel reasons

  inline void TransLock()	// lock for current transaction
    {
      userPin = CurrentTransaction;
    }
  inline void TransUnlock()
    {
      userPin = 0;
    }
  static inline void	ReleasePinnedObjects()
    {
    }
  inline bool   IsPinned()
    {
      return ((userPin == CurrentTransaction)|| kernPin);
    }
  
public:
  ObjectHeader* hashChainNext;

  void ResetKeyRing()
  {
    kr.ResetRing();
  }
  
  void Intern();		// intern object on the ObList.
  void Unintern();		// remove object from the ObList.

  void		FlushIfCkpt();
  void		MakeObjectDirty();
  void		Rescind();
  void		ZapResumeKeys();
#if 0
  // This is very important, but I am still sorting out implementation
  void          DoCopyOnWrite();
#endif
  void          InvalidateProducts();
  
  ObjectHeader *FindProduct(SegWalkInfo& wi, uint32_t ndx,
			    bool rw, bool ca);
  void          AddProduct(ObjectHeader *product);
  void          DelProduct(ObjectHeader *product);

  static ObjectHeader * Lookup(ObType::Type, OID oid);
  static Node *         LookupNode(OID oid)
  {
    return (Node *) Lookup(ObType::NtUnprepared, oid);
  }
  
  static struct ThreadPile&    ObjectSleepQueue(uint32_t ndx);

  static struct ThreadPile&    ObjectSleepQueue(OID oid)
  {
    uint32_t ndx = oid % KTUNE_NOBSLEEPQ;
    return ObjectSleepQueue(ndx);
  }
  
  inline struct ThreadPile&    ObjectSleepQueue()
  {
    return ObjectSleepQueue(ob.oid);
  }

  inline ObjectHeader()
    {
      kr.ResetRing();
      flags = 0;
      userPin = 0;
      obType = ObType::PtFreeFrame; // Node constructor overrides...
    }
  
#ifdef DDB
  void ddb_dump();
  static void ddb_dump_hash_hist();
  static void ddb_dump_bucket(uint32_t bucket);
#endif
};

// MEANINGS OF FLAGS FIELDS:
//
// free     object is on free list, and may be reclaimed. ObjectTable*
//          points to a free list linkage.
//
// dirty    object has been mutated, and needs to be written to disk.
//
// pinned   object is pinned in memory, and should not be reclaimed.
//
// current  object is the current version of the object.
//
// chkpt    object is the version that was current as of the last
//          checkpoint.  When the chkpt logic runs, it checks to see
//          if the object is current and dirty.  If so, it sets the
//          chkpt, pageOut, and wHazard bits.
//
// wHazard  Object must not be modified, either because their is an
//          outbound I/O using its contents or it is scheduled for
//          pageOut.
//
// rHazard  Object content is undefined because an I/O is in progress
//          into the object. Read/Write at your own risk, without
//          any guarantees.
//
// ioActive One or more I/Os are in progress on this object.  Implies
//          ioCount > 0.
//
// pageOut  Object is marked for pageout. Pageout will proceed to
//          either the current area or the chkpt area depending on the
//          chkpt bit value.  When pageout is complete, pageOut and
//          dirty bits will be cleared (assuming no other I/O is in
//          progress).
//
//          It is usually a side-effect of pageout completion that
//          ioCount goes to zero.  Whenever ioCount goes to zero,
//          wHazard and rHazard are cleared.
//
//          If object is no longer current and object is not already
//          free, its age will be set to Age::Free and it will be
//          placed on the free list.
//
// When a user goes to modify an object the wHazard bit is first
// checked to see if the modification can proceed.  If it can, dirty
// is set to 1.  If it cannot, the object may be COW'd or the user may
// be forced to wait.
//
// When the checkpointer starts up, it marks all dirty objects with
// chkpt=1, pageout=1, and wHazard=1.  As they are paged out, pageout
// and wHazard are cleared.
//
// When all pages have gone out, the migrator comes along and starts
// to migrate the pages.  Before it reloads each page from the
// checkpoint area, it checks to see if a version of the page is in
// memory with chkpt=1 and dirty=0.  If so, that version is the same
// as the one in the log, and the log version does not need to be
// reloaded.  Otherwise, it reloads the necessary objects from the
// log.  In either case, if a version was found in memory the chkpt
// bit is turned off.
//
// By the end of the migration phase, no chkpt bits should be on.
//

// FREE LIST HANDLING
//
// Objects marked for pageout can be on the free list before they are
// cleaned.  This creates a problem.  On the one hand, you don't want
// to preferentially steal clean pages from the free list, because
// this has the effect of preferentially removing code pages (which
// are often shared) from memory.  On the other hand, you need to know
// that there are enough free objects to satisfy any pending disk
// reads.
//
// The way we handle this is a bit crufty, but it pretty much works.
// When an object is "placed on the free list", it is first placed on
// the "free pageout list", WHETHER OR NOT it is dirty.  Pages on the
// free pageout list are preferentially cleaned by the pageing daemon,
// and moved to the "real" free list.  If a user-initiated operation
// would lower the number of objects on the "real" free list below the
// number of pending read requests, the operation is stalled until
// there are more clean free objects.
//
// Two optimizations can be done without inducing priority inversion:
// if an object is clean and not current when it is freed, it can be
// placed directly onto the "real" free list.  If it is dirty and not
// current, it is placed at the front of the "free pageout list."  The
// idea is to preferentially free stale object versions.
//
// To understand the implications of the dual free list design, we
// need to consider which operations remove objects from the free
// list:
//
//    1. COW processing
//    2. Object resurrection
//    3. Reading in new objects.
//
// Case 1 is relatively simple -- if there aren't enough free frames
// to COW an object without jeopardizing pending reads, then COWing
// the object was the wrong thing to do in any case.  The design
// decision to make is whether the user should wait for a free frame
// and COW or should wait for the object to go out.  The answer is to
// wait on the free list.
//
// Case 2 is fairly easy - it doesn't matter what free list the object
// is on, just hand the user back the object and it will turn into the
// COW case if the object is dirty.  That's actually good.  If the COW
// succeeds, the user will proceed, and the dirty object will get
// moved to the front of the dirty free list for prompt writeout,
// making it available.  If the COW fails, the next clean page will
// get allocated to this process, which will probably happen faster
// than their page will get written out, and in the worst case will
// only delay them by one write.  In either case, the COW operation
// will now succeed, and a page has been freed as a result.
//
// Case 3 is also simple - if there are not enough free frames on the
// clean free list, the reader should block until there are more.
//
// Note that all of this is fairly unlikely, as the pageout daemon
// tries fairly hard to force dirty pages to the disk before they get
// to the free list.

#endif // __OBJECTHEADER_HXX__
