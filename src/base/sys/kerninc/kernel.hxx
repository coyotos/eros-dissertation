#ifndef __KERNEL_HXX__
#define __KERNEL_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
     
/* These could be inline functions, but this way it works for C as
   well: */
#define min(a,b) ((a) <= (b) ? (a) : (b))
#define max(a,b) ((a) >= (b) ? (a) : (b))

/* Support for the assert macro: */

#ifndef DDB
inline void Debugger() {}
#else
extern void Debugger();
#endif

#ifdef NDEBUG
#define assert(ignore) ((void) 0)
#define assertex(ptr, ignore) ((void) 0)
#define REQUIRE(ignore) ((void) 0)
#else

extern int __assert(const char *, const char *, int);
extern int __assertex(const void *ptr, const char *, const char *, int);
extern void __require(const char *, const char *, int);

#define assert(expression)  \
  ((void) ((expression) ? 0 : __assert (#expression, __FILE__, __LINE__)))
#define assertex(ptr, expression)  \
  ((void) ((expression) ? 0 : __assertex (ptr, #expression, __FILE__, __LINE__)))
#define REQUIRE(expression)  \
  do { if ( ! (expression)  ) { __require (#expression, __FILE__, __LINE__); ckresult = false; } } while (0)
#endif

#ifdef offsetof
#undef offsetof
#endif
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)

#if defined(DBG_WILD_PTR)
extern bool dbg_wild_ptr;
#endif
#ifndef NDEBUG
extern bool dbg_inttrap;
#endif    

#ifdef __cplusplus
void *operator new (size_t);
void *operator new[] (size_t);
void *operator new(size_t, void *place);
void *operator new[](size_t, void *place);
#endif /* __cplusplus */

#endif // __KERNEL_HXX__
