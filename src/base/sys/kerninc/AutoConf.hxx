#ifndef __AUTOCONF_HXX__
#define __AUTOCONF_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Autoconfiguration structure - used by some machine independent
// code.  The structure is "generic," but it's usage is highly machine
// specific.

#define BT_BASE	0
#define BT_PCI	1
#define BT_EISA	2
#define BT_ISA	3
#define BT_SCSI	4
#define BT_USB	5

struct AutoConf;

struct Driver {
  const char* name;
  bool	(*probe)(AutoConf*);
  bool	(*attach)(AutoConf*);	// should be Device&
};

#define DT_CTRLR   0		// Generates interrupts (includes bridges)
#define DT_UNIT    1		// Attached to something that
				// generates interrupts

struct Invocation;

// Common base class for device configuration info
struct DevConfig {
  char      name[12];		   // name of device
  uint32_t  devType;		   // type of device entry
  void      (*Invoke)(Invocation& inv); // invocation interface
  DevConfig *next;		// next in config chain
  uint32_t  ndx;		// instance sequence number
};

struct CtrlrConfig : public DevConfig {
  AutoConf  *ac_tmpl;		// template we were built from
  int       irq;		// actual IRQ taken or -1
  int	    busType;		// BT_NONE or bus type if bridge
  uint16_t  devClass;		// device class per <eros/DeviceDef.h>
  DevConfig *child;		// first child (unit)
};

struct AutoConf {
  const char *name;
  uint32_t bus_type;
  const struct Driver *driver;
  uint32_t irqs;
  int32_t *ports;
  int32_t *mem_addrs;

  static void ArchConfigure();	// machine-specific config initialization
  static void Configure();	// machine-independent config logic
  
  static AutoConf ConfigTable[];
  static uint32_t MaxConfig;
};

#if 0
struct AutoConf {
  void		(*probe)(AutoConf*);
  void		(*attach)(AutoConf*);
  const char    *name;
  bool		present;
  
  static uint32_t MaxConfig;	// defined in machine-specific
				// configuration table file

  static void Configure();	// defined in machine-specific
				// configuration implementation.
  static AutoConf *ConfigTable[];
};

#define DEFMODULE(id, probe, attach, name) \
 static void probe(AutoConf*); \
 static void attach(AutoConf*); \
 struct AutoConf AutoConf_##id = { probe, attach, name, false };
#endif

#endif // __AUTOCONF_HXX__
