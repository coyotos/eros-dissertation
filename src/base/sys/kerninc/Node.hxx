#ifndef __NODE_HXX__
#define __NODE_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Node.hxx: Declaration of a Node.

#include "ObjectHeader.hxx"
#include "Key.hxx"
#include <eros/ProcessState.h>

class DiskNode;

class Node : public ObjectHeader {
  // bool PrepAsDomainSubnode(ObType::Type, Node *parent);

  inline bool IsCurrentDomain();
public:
  ObCount callCount;
  
  Key slot[EROS_NODE_SIZE];
  
public:
  Node();
  ~Node();

  bool Validate() const;
  
  /// void InvokeDomainKeeper();	// using existing fault code
  // void SetDomainFault(uint32_t code, uint32_t auxInfo = 0);
  // static void InvokeSegmentKeeper(uint32_t code, uva_t vaddr = 0);
  
  void ClearHazard(uint32_t ndx);
  void UnprepareHazardedSlot(uint32_t ndx);
  void RescindHazardedSlot(uint32_t ndx, bool mustUnprepare);
#if 0
  void ObMovedHazardedSlot(uint32_t ndx, ObjectHeader *pNewLoc);
#endif
  
  // Prepare node under various sorts of contracts.  In unprepare, set
  // zapMe to true if the *calling* domain should be deprepared in
  // order to satisfy the request.  It shouldn't to satisfy segment
  // walks or to prepare a domain, but it should to set a key in a
  // node slot.
  bool PrepAsSegment();
  void NewPrepAsDomain();
  bool NewPrepAsDomainSubnode(ObType::Type, Process*);
  // bool PrepAsDomain();

  Process *GetDomainContext();

  bool Unprepare(bool zapMe);
    
  void DoZeroThisNode();

  inline void ZeroThisNode()
  {
    MakeObjectDirty();
    DoZeroThisNode();
  }

  
  Key& operator[](int n)
  { return slot[n]; }

  const Key& operator[](int n) const
  { return slot[n]; }

  void operator=(const DiskNode&);
  
  void SetSlot(int ndx, Node& node, uint32_t otherSlot);
} ;
#endif // __NODE_HXX__
