#ifndef __LINK_HXX__
#define __LINK_HXX__
/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// This is a common base class for all of the various linked
// lists. Some objects, including UnitIoReq's, live on multiple lists.
//
// CAUTION! If this representation changes in such a way as to become
// larger than 2 Words, it will break the representation pun in the
// qmk portion of the key data structure.

struct Link {
  Link *next;
  Link *prev;

  inline void Unlink()
  {
    if (next)
      next->prev = prev;
    if (prev)
      prev->next = next;
    next = prev = 0;
  }

  Link()
  {
    next = prev = 0;
  }
} ;

#endif // __LINK_HXX__
