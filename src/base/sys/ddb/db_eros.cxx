/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <machine/db_machdep.hxx>
#include <ddb/db_output.hxx>
#include <ddb/db_lex.hxx>
#include <ddb/db_command.hxx>

#include <kerninc/Machine.hxx>
#include <kerninc/Check.hxx>
#include <kerninc/Invocation.hxx>
#include <kerninc/Process.hxx>
#include <kerninc/Thread.hxx>
#include <kerninc/CpuReserve.hxx>
#include <kerninc/util.hxx>
#include <kerninc/MsgLog.hxx>
#include <kerninc/SymNames.hxx>
#include <kerninc/IRQ.hxx>
#include <kerninc/IntAction.hxx>
#include <kerninc/NetInterface.hxx>
#include <kerninc/IoRequest.hxx>
#include <kerninc/BlockDev.hxx>
#include <kerninc/SysTimer.hxx>
#include <kerninc/Checkpoint.hxx>
#include <kerninc/ObjectCache.hxx>
#include <kerninc/Persist.hxx>
#include <kerninc/Depend.hxx>
#include <eros/SysTraceKey.h>
#include <eros/Reserve.h>
#include <eros/Invoke.h>
#include <ddb/db_sym.hxx>
#include <ddb/db_output.hxx>
#include <machine/PTE.hxx>

void db_eros_print_key(Key& key)
{
  uint32_t *pWKey = (uint32_t *) &key;

  if ( key.IsPreparedObjectKey() ) {
    ObjectHeader * pObj = key.GetObjectPtr();
    
    uint32_t oidlo = (uint32_t) pObj->ob.oid;
    uint32_t oidhi = (uint32_t) (pObj->ob.oid >> 32);

    if (key.IsType(KtResume))
      db_printf("rsum 0x%08x 0x%08x 0x%08x%08x (obj=0x%08x ctxt=0x%08x)\n",
		pWKey[0], ((Node *)pObj)->callCount,
		oidhi, oidlo, pObj, key.gk.pContext);
    else if (key.IsType(KtStart))
      db_printf("strt 0x%08x 0x%08x 0x%08x%08x (obj=0x%08x ctxt=0x%08x)\n",
		pWKey[0], ((Node *)pObj)->callCount,
		oidhi, oidlo, pObj, key.gk.pContext);
    else if (key.IsObjectKey())
      db_printf("pobj 0x%08x 0x%08x 0x%08x%08x (obj=0x%08x)\n",
		pWKey[0], pObj->ob.allocCount,
		oidhi, oidlo, pObj);
    else
      db_printf("pkt  0x%08x 0x%08x 0x%08x 0x%08x\n",
		pWKey[0], pWKey[1], pWKey[2], pWKey[3]);
  }
  else if (key.IsObjectKey()) {
    db_printf("uobj 0x%08x cnt=%u 0x%016X\n",
	      pWKey[0], key.unprep.count, key.unprep.oid);
  }
  else
    db_printf("ukt  0x%08x 0x%08x 0x%08x 0x%08x\n",
	      pWKey[0], pWKey[1], pWKey[2], pWKey[3]);
}

void
db_eros_print_node(Node *pNode)
{
  db_printf("Node (0x%08x) 0x%08x%08x ac=0x%08x cc=0x%08x ot=%d\n",
	    pNode,
	    (uint32_t) (pNode->ob.oid >> 32),
	    (uint32_t) (pNode->ob.oid),
	    pNode->ob.allocCount,
	    pNode->callCount, pNode->obType);

#if !defined(NDEBUG) && 0
  if (pNode->Validate() == false)
    db_error("...Is not valid\n");
#endif
  
  bool wasPrepared = ((pNode->obType == ObType::NtProcessRoot ||
#ifdef ProcGenRegs
		       pNode->obType == ObType::NtRegAnnex ||
#endif
		       pNode->obType == ObType::NtKeyRegs) &&
		      pNode->context);
  
  bool isRoot = (pNode->obType == ObType::NtProcessRoot &&
		 pNode->context);
  if (isRoot) {
    for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
      if (pNode->slot[i].IsRdHazard())
	((Process *) pNode->context)->FlushProcessSlot(i);
    }
  }


#ifdef ProcGenRegs
  bool isAnnex = (pNode->obType == ObType::NtRegAnnex &&
		  pNode->context);

  if (isAnnex) {
    for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
      if (pNode->slot[i].IsRdHazard())
	((Process *) pNode->context)->FlushAnnexSlot(pNode, i);
    }
  }
#endif

  bool isKeyRegs = (pNode->obType == ObType::NtKeyRegs &&
		  pNode->context);
  if (isKeyRegs) {
    for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
      if (pNode->slot[i].IsRdHazard())
	((Process *) pNode->context)->FlushKeySlot(i);
    }
  }

  for (uint32_t i = 0; i < EROS_NODE_SIZE; i++) {
    Key& key = (*pNode)[i];
    db_printf(" [%02d] ", i);
    db_eros_print_key(key);
  }

  if (wasPrepared)
    pNode->context->Prepare();
}

void
db_eros_print_string(uint8_t *data, uint32_t len)
{
  if ( len > 20)
    len = 20;
  
  for (uint32_t i = 0; i < len; i++) {
    uint8_t c = data[i];
  
    if (IsPrint(c) || c == '\n')
      db_printf("%c", c);
    else
      db_printf("\\x%02x", c);
  }
}


void
db_eros_print_context(Process *cc)
{
  if (cc == 0)
    db_printf("invokee=0x%08x\n", cc);
  else {
    db_printf("ctxt=0x%08x (%s)", cc, cc->Name());
    db_printf(" (%s) ",
	      ((cc->runState == RS_Running)
	       ? "Running"
	       : ((cc->runState == RS_Waiting)
		  ? "Waiting"
		  : "Avail")));
    if (cc->isUserContext) {
      uint32_t oidhi = (uint32_t) (cc->procRoot->ob.oid >> 32);
      uint32_t oidlo = (uint32_t) cc->procRoot->ob.oid;

      db_printf("domain root=0x%08x", cc->procRoot);
      db_printf("  oid=0x%08x%08x\n", oidhi, oidlo);
    }
    db_printf("  Fault Code=0x%08x Fault Info = 0x%08x procFlags=0x%02x\n"
	      "  haz=0x%x rsrv=0x%08x",
	      cc->faultCode, cc->faultInfo, cc->processFlags,
	      cc->hazards, cc->cpuReserve);
#ifdef SMALL_SPACES
    db_printf(" smallPTE=0x%08x bias=0x%08x lim=0x%08x\n",
	      cc->smallPTE, cc->bias, cc->limit);
#else
    db_printf("\n");
#endif

    cc->DumpFixRegs();
  }
}

void
db_show_savearea_cmd(db_expr_t addr, int have_addr,
		     db_expr_t /* count */, char * /* modif */)
{
  extern void DumpFixRegs(const fixregs_t *fx);
  
  if (have_addr == 0)
    db_error("requires address\n");

  DumpFixRegs((const fixregs_t *) addr);
}

void
db_show_sizes_cmd(db_expr_t /* addr */, int /* have_addr */,
		     db_expr_t /* count */, char * /* modif */)
{
  db_printf("sizeof(ObjectHeader) = 0x%x (%d)\n",
	    sizeof(ObjectHeader), sizeof(ObjectHeader));
  db_printf("sizeof(Node) = 0x%x (%d)\n",
	    sizeof(Node), sizeof(Node));
  db_printf("sizeof(Process) = 0x%x (%d)\n",
	    sizeof(Process), sizeof(Process));
  db_printf("nDirentPerPage = 0x%x (%d)\n",
	    CkptDirPage::maxDirEnt,
	    CkptDirPage::maxDirEnt);
}

extern "C" {
  extern uint32_t InterruptStackTop;
  extern uint32_t InterruptStackBottom;
}

void
db_eros_print_context_keyring(Process *cc)
{
  Key *kr = (Key*) &cc->kr;
  Key *k = (Key *) kr->gk.next;

  while (k != kr) {
    if ( ( (uint32_t) k >= (uint32_t) &InterruptStackBottom ) &&
	 ( (uint32_t) k < (uint32_t) &InterruptStackTop ) )
      db_printf("*** next key is on kernel stack:\n");

    db_printf("(0x%08x): ", k);
    db_eros_print_key(*k);

    k = (Key *) k->gk.next;
  }
}

void
db_eros_print_context_keyregs(Process *cc)
{
  if (cc == 0) {
    db_printf("invokee=0x%08x\n", cc);
    return;
  }

#ifndef KEYREGS_IN_CONTEXT
  if (cc->keyReg == 0) {
    db_printf("error: context has no key registers"
	      " /* can't happen :-) */\n");
    return;
  }
#endif

  for (unsigned int i = 0; i < EROS_NODE_SIZE; i++) {
    db_printf("[%02d] ", i);
    db_eros_print_key(cc->keyReg[i]);
  }
}

void
db_eros_print_thread(Thread *t)
{
  uint32_t ndx = t - Thread::ThreadTable;
  
  db_printf("thread 0x%08x:\n", t);
  OID oid = 0;

  char isGoodOid = '?';
  if ( t->IsUser() ) {
    if (t->context) {
      oid = ((Process *) (t->context))->procRoot->ob.oid;
      isGoodOid = 'u';
    }
    else if (t->processKey.IsObjectKey()) {
      oid = t->processKey.GetKeyOid();
      isGoodOid = 'u';
    }
  }
  else
      isGoodOid = 'k';
  
  if (t->IsUser())
    db_printf("[%4d] ", ndx);
  else
    db_printf("[kern] ", ndx);
    
  db_printf("0x%08x %c %s ctxt=0x%08x dr=%c0x%08x%08x\n"
	    "       q=0x%08x lnkd? %c wake=0x%08x%08x shouldwake? %c\n"
	    "       reserve=0x%08x curPrio %d shouldYield? %c canYield? %c\n",
	    t,
	    t->IsUser() ? 'u' : 'k',
	    Thread::stateNames[t->state], t->context,
	    isGoodOid,
	    (uint32_t) (oid>>32), (uint32_t) oid,
	    t->lastq,
	    t->prev ? 'y' : 'n',
	    (uint32_t) (t->wakeTime >> 32), (uint32_t) t->wakeTime,
	    t->wakeTime <= SysTimer::Now() ? 'y' : 'n',
	    t->cpuReserve,
	    t->cpuReserve->curPrio,
	    t->ShouldYield() ? 'y' : 'n',
	    t->CAN_PREEMPT() ? 'y' : 'n');
}

#ifdef DBG_WILD_PTR
bool dbg_wild_ptr = true;
void
db_eros_dbg_wild_n_cmd(db_expr_t, int, db_expr_t, char*)
{
  dbg_wild_ptr = false;
}

void
db_eros_dbg_wild_y_cmd(db_expr_t, int, db_expr_t, char*)
{
  dbg_wild_ptr = true;
}
#endif

#ifndef NDEBUG
bool dbg_inttrap = false;
void
db_eros_dbg_inttrap_n_cmd(db_expr_t, int, db_expr_t, char*)
{
  dbg_inttrap = false;
}

void
db_eros_dbg_inttrap_y_cmd(db_expr_t, int, db_expr_t, char*)
{
  dbg_inttrap = true;
}
#endif

void
db_eros_mesg_n_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  MsgLog::SuppressDebug = true;
}

void
db_eros_mesg_y_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  MsgLog::SuppressDebug = false;
}

bool ddb_thread_uqueue_debug = false;
void
db_eros_mesg_uqueue_y_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  ddb_thread_uqueue_debug = true;
}

void
db_eros_mesg_uqueue_n_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  ddb_thread_uqueue_debug = false;
}

bool ddb_uyield_debug = false;
void
db_eros_mesg_uyield_y_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  ddb_uyield_debug = true;
}

void
db_eros_mesg_uyield_n_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  ddb_uyield_debug = false;
}

void
db_eros_mesg_gate_y_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_gate_message;
  ddb_gate_message = true;
}

void
db_eros_mesg_gate_n_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_gate_message;
  ddb_gate_message = false;
}

void
db_eros_mesg_rtrn_y_cmd(db_expr_t /* addr */, int /* have_addr */,
			db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_return_message;
  ddb_return_message = true;
}

void
db_eros_mesg_rtrn_n_cmd(db_expr_t /* addr */, int /* have_addr */,
			db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_return_message;
  ddb_return_message = false;
}

void
db_eros_mesg_inv_y_cmd(db_expr_t /* addr */, int /* have_addr */,
			db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_inv_message;
  ddb_inv_message = true;
}

void
db_eros_mesg_inv_n_cmd(db_expr_t /* addr */, int /* have_addr */,
			db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_inv_message;
  ddb_inv_message = false;
}


void
db_eros_mesg_keyerr_y_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_keyerr_message;
  ddb_keyerr_message = true;
}

void
db_eros_mesg_keyerr_n_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_keyerr_message;
  ddb_keyerr_message = false;
}

void
db_eros_mesg_keeper_y_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_keeper_message;
  ddb_keeper_message = true;
}

void
db_eros_mesg_keeper_n_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  extern bool ddb_keeper_message;
  ddb_keeper_message = false;
}

void
db_ctxt_print_cmd(db_expr_t addr, int have_addr,
		  db_expr_t /* count */, char * /* modif */)
{
  Process *cc =
    have_addr ? (Process *) addr : (Process*) Thread::CurContext();
  db_eros_print_context(cc);
}

void
db_user_single_step_cmd(db_expr_t addr, int have_addr,
			db_expr_t /* count */, char * /* modif */)
{
  Process *cc =
    have_addr ? (Process *) addr : (Process*) Thread::CurContext();

  cc->SetInstrSingleStep();

  {
    extern void	db_continue_cmd(db_expr_t, int, db_expr_t, char*);
    
    db_continue_cmd(0, 0, 0, "");
  }
}

void
db_print_reserve(CpuReserve *r)
{
  int index = -1;
  
  if ((uint32_t)r > (uint32_t) CpuReserve::CpuReserveTable)
    index = r - CpuReserve::CpuReserveTable;
  if (index >= MAX_CPU_RESERVE)
    index = 01;
  
  db_printf("[%3d] (0x%08x) period 0x%08x%08x duration 0x%08x%08x\n"
	    "      quanta 0x%08x%08x normPrio %d rsrvPrio %d\n"
	    "      residQ 0x%08x%08x residD 0x%08x%08x expired? %c\n",
	    index,
	    r,
	    (uint32_t) (r->period >> 32),
	    (uint32_t) (r->period),
	    (uint32_t) (r->duration >> 32),
	    (uint32_t) (r->duration),
	    (uint32_t) (r->quanta >> 32),
	    (uint32_t) (r->quanta),
	    r->normPrio,
	    r->rsrvPrio,
	    (uint32_t) (r->residQuanta >> 32),
	    (uint32_t) (r->residQuanta),
	    (uint32_t) (r->residDuration >> 32),
	    (uint32_t) (r->residDuration),
	    r->expired ? 'y' : 'n');
}	      

void
db_show_reserves_cmd(db_expr_t /* addr */, int /* have_addr */,
		     db_expr_t /* count */, char * /* modif */)
{
  for (uint32_t i = 0; i < MAX_CPU_RESERVE; i++) {
    CpuReserve& r = CpuReserve::CpuReserveTable[i];
    if (r.threadChain.next == 0)
      continue;

    db_print_reserve(&r);
  }
}

void
db_show_kreserves_cmd(db_expr_t /* addr */, int /* have_addr */,
		     db_expr_t /* count */, char * /* modif */)
{
  db_print_reserve(&CpuReserve::KernIdleCpuReserve);
  db_print_reserve(&CpuReserve::KernThreadCpuReserve);
}

void
db_rsrv_print_cmd(db_expr_t addr, int have_addr,
		  db_expr_t /* count */, char * /* modif */)
{
  CpuReserve *pCpuReserve = CpuReserve::Current;
  
  if (have_addr)
    pCpuReserve = (CpuReserve *) addr;

  if (pCpuReserve)
    db_print_reserve(pCpuReserve);
  else
    db_error("no current reserve\n");
}

void
db_rsrvchain_print_cmd(db_expr_t addr, int have_addr,
		  db_expr_t /* count */, char * /* modif */)
{
  CpuReserve *pCpuReserve = CpuReserve::Current;
  
  if (have_addr)
    pCpuReserve = (CpuReserve *) addr;

  db_print_reserve(pCpuReserve);

  Link* tlnk = pCpuReserve->threadChain.next;
  while (tlnk) {
    Thread *t = Thread::ThreadFromCpuReserveLinkage(tlnk);
    db_eros_print_thread(t);
    tlnk = tlnk->next;
  }
}

void
db_ctxt_kr_print_cmd(db_expr_t addr, int have_addr,
		     db_expr_t /* count */, char * /* modif */)
{
  Process *cc =
    have_addr ? (Process *) addr : (Process*) Thread::CurContext();
  db_eros_print_context_keyring(cc);
}

void
db_ctxt_keys_print_cmd(db_expr_t /* addr */, int /* have_addr */,
			db_expr_t /* count */, char * /* modif */)
{
  Process *cc = (Process*) Thread::CurContext();
  db_eros_print_context_keyregs(cc);
}

void
db_thread_print_cmd(db_expr_t addr, int have_addr,
		    db_expr_t /* count */, char * /* modif */)
{
  Thread *t = Thread::Current();
  const char * cur_str = "current ";

  if (have_addr) {
    t = (Thread *) addr;
    cur_str="";
  }
  
  if (t) {
    if (t->context && t->IsUser())
      ((Process*) t->context)->SyncThread();
  
    db_printf("%sthread 0x%08x (%s) ctxt 0x%08x (%s) rsrv=0x%08x\n",
	      cur_str,
	      t, Thread::stateNames[t->state], t->context,
	      (t->IsKernel() ? "kernel" : "user"),
	      t->cpuReserve);
    db_printf("    ");
    db_eros_print_key(t->processKey);
  }
  else
    db_printf("No current thread.\n");
}

void
db_inv_print_cmd(db_expr_t /* addr */, int /* have_addr */,
		 db_expr_t /* count */, char * /* modif */)
{
  if (inv.IsActive() == false) {
    db_printf("No active invocation\n");
    return;
  }
  
  uint32_t invType = inv.invType;
  if (invType >= 3)
    invType = 3;

  static char invTypeName[] = { 'R', 'C', 'S', '?' };
  
  if (inv.key) {
    db_printf("Invoked key 0x%08x ity=%c ikt=%d kt=%d st=%d inv count=%U\n"
	      " OC=0x%08x (%d) nextPC=0x%08x\n",
	      inv.key, invTypeName[invType], inv.keyType,
	      inv.key->GetType(), inv.key->subType,
	      KernStats.nInvoke,
	      inv.entry.code,
	      inv.entry.code,
	      inv.nextPC);
    db_printf("(0x%08x): ", inv.key);
    db_eros_print_key(*inv.key);
  }
  else
    db_printf("Invoked key not yet determined\n");
}

void
db_entry_print_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  if (inv.IsActive() == false)
    db_printf("WARNING: no active invocation\n");
  
  db_printf("Entry: 0x%08x, data 0x%08x len %d invocation %U\n",
	    inv.entry.code, inv.entry.data, inv.entry.len,
	    (uint32_t) KernStats.nInvoke);
  for (int i = 0; i < 4; i++) {
    if (inv.entry.key[i]) {
      db_printf("%d: (0x%08x): ", i, inv.entry.key[i]);
      db_eros_print_key(*inv.entry.key[i]);
    }
  }

  db_printf("w0: 0x%08x w1: 0x%08x w2: 0x%08x w3: 0x%08x\n",
	    inv.entry.code, inv.entry.w1, inv.entry.w2, inv.entry.w3);

  db_printf(" str: ");
  db_eros_print_string(inv.entry.data, inv.entry.len);
}

void
db_exit_print_cmd(db_expr_t /* addr */, int /* have_addr */,
		   db_expr_t /* count */, char * /* modif */)
{
  if (inv.IsActive() == false)
    db_printf("WARNING: No active invocation\n");
  
  db_printf("Exit: 0x%08x, data 0x%08x len %d valid len %d invocation %u\n",
	    inv.exit.code, inv.exit.data, inv.exit.len, inv.validLen,
	    (uint32_t) KernStats.nInvoke);
  for (int i = 0; i < 4; i++) {
    if (inv.exit.pKey[i]) {
      db_printf("%d: (0x%08x): ", i, inv.exit.pKey[i]);
      db_eros_print_key(*inv.exit.pKey[i]);
    }
  }

  db_printf("w0: 0x%08x w1: 0x%08x w2: 0x%08x w3: 0x%08x\n",
	    inv.exit.code, inv.exit.w1, inv.exit.w2, inv.exit.w3);
}

void
db_invokee_print_cmd(db_expr_t /* addr */, int /* have_addr */,
		     db_expr_t /* count */, char * /* modif */)
{
  if (inv.IsActive() == false) {
    if (inv.invokee)
      db_printf("WARNING! Out-of-date invokee!\n");
    else {
      db_printf("No active invocation\n");
      return;
    }
  }

  db_eros_print_context(inv.invokee);
}

void
db_invokee_kr_print_cmd(db_expr_t /* addr */, int /* have_addr */,
			db_expr_t /* count */, char * /* modif */)
{
  if (inv.IsActive() == false) {
    db_printf("No active invocation\n");
    return;
  }

  Process *cc;
  if ( inv.key && inv.key->IsGateKey() ) {
    cc = inv.key->gk.pContext;
  }
  else if (inv.entry.key[3] && inv.entry.key[3]->IsGateKey() ) {
    cc = inv.entry.key[3]->gk.pContext;
  }
  else
    cc = 0;

  db_eros_print_context_keyring(cc);
}

void
db_invokee_keys_print_cmd(db_expr_t /* addr */, int /* have_addr */,
			db_expr_t /* count */, char * /* modif */)
{
  if (inv.IsActive() == false) {
    db_printf("No active invocation\n");
    return;
  }

  Process *cc;
  if ( inv.key && inv.key->IsGateKey() ) {
    cc = inv.key->gk.pContext;
  }
  else if (inv.entry.key[3] && inv.entry.key[3]->IsGateKey() ) {
    cc = inv.entry.key[3]->gk.pContext;
  }
  else
    cc = 0;

  db_eros_print_context_keyregs(cc);
}

void
db_show_uthread_cmd(db_expr_t /* addr */, int /* have_addr */,
		    db_expr_t /* count */, char * /* modif */)
{
  for (uint32_t i = 0; i < KTUNE_NTHREAD; i++) {
    Thread *t = &Thread::ThreadTable[i];
    if (t->state != Thread::Free)
      db_eros_print_thread(t);
  }
}

void
db_reboot_cmd(db_expr_t, int, db_expr_t, char*)
{
  Machine::HardReset();
}

// This is quite tricky.  Basically, we are trying to find the most
// expensive 10 procedures in the kernel.  The problem is that we
// don't really want to sort the symbol table to do it.

extern "C" {
  extern void etext();
  extern void start();
#ifdef KERN_PROFILE
  extern uint32_t *KernelProfileTable;
#endif
}

#ifdef KERN_STATS
#define DB64(x) ((uint32_t)(x>>32)), (uint32_t)(x)
void
db_kstat_show_cmd(db_expr_t, int, db_expr_t, char*)
{
  db_printf("nDepend   0x%08x%08x  "
	    "nDepMerge 0x%08x%08x\n"

	    "nDepInval 0x%08x%08x  "
	    "nDepZap   0x%08x%08x\n"

	    "nInvoke   0x%08x%08x  "
	    "nInvKpr   0x%08x%08x\n"

	    "nPfTraps  0x%08x%08x  "
	    "nPfAccess 0x%08x%08x\n"

	    "nWalkSeg  0x%08x%08x  "
	    "nWalkLoop 0x%08x%08x\n"

	    "nKeyPrep  0x%08x%08d  "
	    "nInter    0x%08x%08d\n"

	    "nGateJmp  0x%08x%08x  "
	    "nInvRetry 0x%08x%08x\n"

	    "nRetag    0x%08x%08x\n ",

	    DB64(KernStats.nDepend),
	    DB64(KernStats.nDepMerge),

	    DB64(KernStats.nDepInval),
	    DB64(KernStats.nDepZap),

	    DB64(KernStats.nInvoke),
	    DB64(KernStats.nInvKpr),

	    DB64(KernStats.nPfTraps),
	    DB64(KernStats.nPfAccess),

	    DB64(KernStats.nWalkSeg),
	    DB64(KernStats.nWalkLoop),

	    DB64(KernStats.nKeyPrep),
	    DB64(KernStats.nInter),

	    DB64(KernStats.nGateJmp),
	    DB64(KernStats.nInvRetry),

	    DB64(KernStats.nRetag)
	    );
}

#ifdef FAST_IPC_STATS
extern "C" {
  extern uint32_t nFastIpcPath;
  extern uint32_t nFastIpcFast;
  extern uint32_t nFastIpcRedSeg;
  extern uint32_t nFastIpcString;
  extern uint32_t nFastIpcSmallString;
  extern uint32_t nFastIpcLargeString;
  extern uint32_t nFastIpcNoString;
  extern uint32_t nFastIpcRcvPf;
  extern uint32_t nFastIpcEnd;
  extern uint32_t nFastIpcOK;
  extern uint32_t nFastIpcPrepared;
}
  
void
db_kstat_fast_cmd(db_expr_t, int, db_expr_t, char*)
{
  db_printf("nFastIpcPath        0x%08x  "
	    "nFastIpcFast        0x%08x\n"

	    "nFastIpcRedSeg      0x%08x  "
	    "nFastIpcString      0x%08x\n"

	    "nFastIpcSmallString 0x%08x  "
	    "nFastIpcLargeString 0x%08x\n"

	    "nFastIpcNoString    0x%08x  "
	    "nFastIpcRcvPf       0x%08x\n"

	    "nFastIpcEnd         0x%08x  "
	    "nFastIpcOK          0x%08x\n"

	    "nFastIpcPrepared    0x%08x\n"
	    ,

	    nFastIpcPath,
	    nFastIpcFast,
	    nFastIpcRedSeg,
	    nFastIpcString,
	    nFastIpcSmallString,
	    nFastIpcLargeString,
	    nFastIpcNoString,
	    nFastIpcRcvPf,
	    nFastIpcEnd,
	    nFastIpcOK,
	    nFastIpcPrepared
	    );
}
#endif /* FAST_IPC_STATS */

void
db_kstat_clear_cmd(db_expr_t, int, db_expr_t, char*)
{
  bzero(&KernStats, sizeof(KernStats));
  Invocation::ZeroStats();
}

void
db_kstat_ipc_cmd(db_expr_t, int, db_expr_t, char*)
{
  for (int i = 0; i < LAST_KEYTYPE; i++) {
    if (Invocation::KeyHandlerCounts[i][IT_Call] ||
	Invocation::KeyHandlerCounts[i][IT_Reply] ||
	Invocation::KeyHandlerCounts[i][IT_Send]) {
      db_printf("kt%02d: C [%8U] %13U cy R [%8U] %13U cy\n",
		i,
		Invocation::KeyHandlerCounts[i][IT_Call],
		Invocation::KeyHandlerCycles[i][IT_Call],
		Invocation::KeyHandlerCounts[i][IT_Reply],
		Invocation::KeyHandlerCycles[i][IT_Reply]);
      db_printf("      S [%8U] %13U cy\n",
		Invocation::KeyHandlerCounts[i][IT_Send],
		Invocation::KeyHandlerCycles[i][IT_Send]);
    }
  }
}
#endif

#ifdef KERN_PROFILE
typedef int (*qsortfn)(...);
  
int
ddb_CompareFunsByAddr(FuncSym *sn0, FuncSym*sn1)
{
  if (sn0->address < sn1->address)
    return -1;
  if (sn0->address == sn1->address)
    return 0;
  return 1;
}

int
ddb_CompareLinesByAddr(LineSym *sn0, LineSym*sn1)
{
  if (sn0->address < sn1->address)
    return -1;
  if (sn0->address == sn1->address)
    return 0;
  return 1;
}

// Note - higher sorts first:
int
ddb_CompareFunsByCount(FuncSym *sn0, FuncSym*sn1)
{
  if (sn0->profCount > sn1->profCount)
    return -1;
  if (sn0->address == sn1->address)
    return 0;
  return 1;
}

enum HowSorted {
  Unknown,
  ByAddr,
  ByCount
};

static HowSorted FunSort = Unknown;
static HowSorted LineSort = Unknown;

void
ddb_SortLinesByAddr()
{
  if (LineSort != ByAddr) {
    qsort(LineSym::table, LineSym::count, sizeof(LineSym),
	  (qsortfn)ddb_CompareLinesByAddr);
    LineSort = ByAddr;
  }
}

void
ddb_SortFunsByAddr()
{
  if (FunSort != ByAddr) {
    qsort(FuncSym::table, FuncSym::count, sizeof(FuncSym),
	  (qsortfn)ddb_CompareFunsByAddr);
    FunSort = ByAddr;
  }
}

void
ddb_SortFunsByCount()
{
  if (FunSort != ByCount) {
    qsort(FuncSym::table, FuncSym::count, sizeof(FuncSym),
	  (qsortfn)ddb_CompareFunsByCount);
    FunSort = ByCount;
  }
}

void
db_prof_clear_cmd(db_expr_t, int, db_expr_t, char*)
{
  uint32_t kernelCodeLength = (uint32_t) etext;
  uint32_t tableSizeInWords = (kernelCodeLength >> 4);
  
  for (uint32_t i = 0; i < tableSizeInWords; i++)
    KernelProfileTable[i] = 0;
}

static uint64_t
prep_prof()
{
  uint64_t totCount = 0ll;
  
  // One profile word for every 16 bytes:
  
  const int topsz = 10;
  
  FuncSym *top[topsz];

  for (int i = 0; i < topsz; i++)
    top[i] = 0;

  uint32_t kernelCodeLength = (uint32_t) etext;
  uint32_t tableSizeInWords = (kernelCodeLength >> 4);
  
  for (uint32_t i = 0; i < tableSizeInWords; i++)
    totCount += KernelProfileTable[i];

  ddb_SortFunsByAddr();

  // Symbol table is sorted by address.  We take advantage of that
  // here for efficiency:
  uint32_t limit = (uint32_t) etext;

  for (uint32_t i = 0; i < FuncSym::count; i++) {
    if (FuncSym::table[i].address >= limit)
      continue;
    
    FuncSym::table[i].profCount = 0;
    uint32_t address = FuncSym::table[i].address;
    address += 0xf;
    address &= ~0xfu;

    while (address < FuncSym::table[i+1].address) {
      FuncSym::table[i].profCount += KernelProfileTable[address >> 4];
      address += 16;
    }
  }

  ddb_SortFunsByCount();

  return totCount;
}

static void
show_prof(uint64_t tot, uint32_t limit)
{
  uint32_t printCount = 0;

  for (uint32_t i = 0; i < FuncSym::count; i++) {
    if (FuncSym::table[i].profCount == 0)
      continue;
    printCount++;
    db_printf("%02d: %-10u 0x%08x  %s\n",
	      i, FuncSym::table[i].profCount,
	      FuncSym::table[i].address,
	      FuncSym::table[i].name);
    if (printCount >= limit)
      break;
  }

  db_printf("Done.  Total ticks: 0x%08x%08x\n",
	    (uint32_t) (tot >> 32), (uint32_t) tot);
}

void
db_prof_top_cmd(db_expr_t, int, db_expr_t, char*)
{
  uint64_t totCount = prep_prof();

  show_prof(totCount, 20);
}

void
db_prof_all_cmd(db_expr_t, int, db_expr_t, char*)
{
  uint64_t totCount = prep_prof();

  show_prof(totCount, UINT32_MAX);
}
#endif

void
db_show_irq_cmd(db_expr_t, int, db_expr_t, char*)
{
  db_printf("IRQ disable depth is %d\n", IRQ::DISABLE_DEPTH());
	    
  for (uint32_t i = 0; i < NUM_HW_INTERRUPT; i++) {
    IntAction *ia = IRQ::ddb_getaction(i);
    if (ia && ia->IsValid()) {
      db_printf("IRQ %d: \"%s\" (%s)\n", ia->GetIrq(),
		ia->DriverName(),
		ia->IsWired() ? "wired" : "not wired");
    }
  }
}

void
db_kstat_hist_dio_cmd(db_expr_t, int, db_expr_t, char*)
{
  DuplexedIO::ddb_dump_hist();
}

void
db_kstat_hist_depend_cmd(db_expr_t, int, db_expr_t, char*)
{
  int t;
  uint32_t bucket;
  
  t = db_read_token();
  
  if (t != tNUMBER)
    Depend::ddb_dump_hist();
  
  bucket = db_tok_number;
	
  t = db_read_token();
  if (db_read_token() != tEOL) {
    db_error("?\n");
    /*NOTREACHED*/
  }

  Depend::ddb_dump_bucket(bucket);
}

void
db_kstat_hist_objhash_cmd(db_expr_t, int, db_expr_t, char*)
{
  int t;
  uint32_t bucket;
  
  t = db_read_token();
  
  if (t != tNUMBER)
    ObjectHeader::ddb_dump_hash_hist();
  
  bucket = db_tok_number;
	
  t = db_read_token();
  if (db_read_token() != tEOL) {
    db_error("?\n");
    /*NOTREACHED*/
  }

  ObjectHeader::ddb_dump_bucket(bucket);
}

void
db_show_ior_cmd(db_expr_t, int, db_expr_t, char*)
{
  Request::ddb_dump();
}

void
db_show_dio_cmd(db_expr_t, int, db_expr_t, char*)
{
  DuplexedIO::ddb_dump();
}

void
db_show_divisions(db_expr_t, int, db_expr_t, char*)
{
  Persist::ddb_DumpDivisions();
}

void
db_check_nodes_cmd(db_expr_t, int, db_expr_t, char*)
{
  if (Check::Nodes())
    db_printf("nodes are okay\n");
  else
    db_printf("nodes are crocked\n");
  
}

void
db_check_pages_cmd(db_expr_t, int, db_expr_t, char*)
{
  if (Check::Pages())
    db_printf("pages are okay\n");
  else
    db_printf("pages are crocked\n");
  
}

void
db_check_ctxt_cmd(db_expr_t, int, db_expr_t, char*)
{
  if (Check::Contexts("ddb"))
    db_printf("contexts are okay\n");
  else
    db_printf("contexts are crocked\n");
}

void
db_check_ckpt_cmd(db_expr_t, int, db_expr_t, char*)
{
  if (Checkpoint::CheckConsistency("from ddb", false))
    db_printf("ckpt stuff okay\n");
  else
    db_printf("ckpt stuff crocked\n");
}

void
db_show_pgtree_cmd(db_expr_t, int, db_expr_t, char*)
{
  uint32_t gen = 0;
  int t;
  
  t = db_read_token();
  if (t == tNUMBER) {
    gen = db_tok_number;

    t = db_read_token();
  }
  if (db_read_token() != tEOL) {
    db_error("expects generation\n");
    db_error("?\n");
    /*NOTREACHED*/
  }

  db_printf("Dumping page tree for generation %d\n", gen);
  Checkpoint::ddb_dump_pgtree(gen);
}

void
db_show_key_cmd(db_expr_t addr, int have_addr, db_expr_t, char*)
{
  if (have_addr == 0)
    db_error("requires address\n");

  db_eros_print_key(*((Key *) addr));
}

void
db_show_node_cmd(db_expr_t addr, int have_addr, db_expr_t, char*)
{
  if (have_addr == 0)
    db_error("requires address\n");

  db_eros_print_node(((Node *) addr));
}

void
db_show_obhdr_cmd(db_expr_t addr, int have_addr, db_expr_t, char*)
{
  if (have_addr == 0)
    db_error("requires address\n");

  ((ObjectHeader *) addr)->ddb_dump();
}

void
db_show_ndtree_cmd(db_expr_t, int, db_expr_t, char*)
{
  uint32_t gen = 0;
  int t;
  
  t = db_read_token();
  if (t == tNUMBER) {
    gen = db_tok_number;

    t = db_read_token();
  }
  if (db_read_token() != tEOL) {
    db_error("expects generation\n");
    db_error("?\n");
    /*NOTREACHED*/
  }

  db_printf("Dumping node tree for generation %d\n", gen);
  Checkpoint::ddb_dump_ndtree(gen);
}

void
db_show_mig_status_cmd(db_expr_t, int, db_expr_t, char*)
{
  Checkpoint::ddb_dump_mig_status();
}

void
db_show_cde_cmd(db_expr_t addr, int have_addr, db_expr_t, char*)
{
  if (have_addr == 0)
    db_error("requires address\n");

  ((CoreDirent *) addr)->ddb_dump();
}

void
db_show_ckdir_cmd(db_expr_t, int, db_expr_t, char*)
{
  Checkpoint::ddb_dump_ckdir();
}

void
db_show_curdir_cmd(db_expr_t, int, db_expr_t, char*)
{
  Checkpoint::ddb_dump_curdir();
}

void
db_show_ckalloc_cmd(db_expr_t, int, db_expr_t, char*)
{
  Checkpoint::ddb_showalloc();
}

void
db_show_ckhdrloc_cmd(db_expr_t, int, db_expr_t, char*)
{
  Checkpoint::ddb_showhdrloc();
}

void
db_show_pins_cmd(db_expr_t, int, db_expr_t, char*)
{
  ObjectCache::ddb_dump_pinned_objects();
}

void
db_show_pte_cmd(db_expr_t addr, int have_addr, db_expr_t, char*)
{
  if (have_addr == 0)
    db_error("requires address\n");

  ((PTE *) addr)->ddb_dump();
}

void
db_show_pages_cmd(db_expr_t, int, db_expr_t, char*)
{
  ObjectCache::ddb_dump_pages();
}

void
db_show_nodes_cmd(db_expr_t, int, db_expr_t, char*)
{
  ObjectCache::ddb_dump_nodes();
}

void
db_user_continue_cmd(db_expr_t, int, db_expr_t, char*)
{
  extern bool continue_user_bpt;
  continue_user_bpt = true;
}

void
db_node_cmd(db_expr_t, int, db_expr_t, char*)
{
  int	t;
  OID     oid;

  t = db_read_token();
  if (t != tNUMBER)
    db_error("expects OID\n");

  oid = db_tok_number;
	
  t = db_read_token();
  if (db_read_token() != tEOL) {
    db_error("?\n");
    /*NOTREACHED*/
  }

  Node *pNode = ObjectHeader::LookupNode(oid);
  if (pNode == 0)
    db_error("not in core\n");

  db_eros_print_node(pNode);
}

void
db_show_counters_cmd(db_expr_t, int, db_expr_t, char*)
{
  int	t;
  OID     oid;

  t = db_read_token();
  if (t != tNUMBER)
    db_error("expects OID\n");

  oid = db_tok_number;
	
  t = db_read_token();
  if (db_read_token() != tEOL) {
    db_error("?\n");
    /*NOTREACHED*/
  }

  Node *pNode = ObjectHeader::LookupNode(oid);
  if (pNode == 0)
    db_error("not in core\n");

  db_printf("Node 0x%08x%08x ac=0x%08x cc=0x%08x\n",
	    (uint32_t) (pNode->ob.oid >> 32),
	    (uint32_t) (pNode->ob.oid),
	    pNode->ob.allocCount,
	    pNode->callCount);

#ifndef NDEBUG
  if (pNode->Validate() == false)
    db_error("...Is not valid\n");
#endif
  
  bool isProcess =
    (pNode->obType == ObType::NtProcessRoot ||
     pNode->obType == ObType::NtRegAnnex) &&
    pNode->context;

  if (isProcess) {
    db_printf("     count = 0x%016X\n", pNode->context->stats.evtCounter0);
  }
  else {
    Key& k = pNode->slot[10];
    uint32_t hi = k.nk.value[1];
    uint32_t lo = k.nk.value[2];
    db_printf("     count = 0x%08x%08x\n",
	      hi, lo);
  }
}

void
db_logpage_cmd(db_expr_t, int, db_expr_t, char*)
{
  int	t;
  OID     oid;

  t = db_read_token();
  if (t != tNUMBER)
    db_error("expects OID\n");

  oid = db_tok_number;
	
  t = db_read_token();
  if (db_read_token() != tEOL) {
    db_error("?\n");
    /*NOTREACHED*/
  }

  ObjectHeader *pObj = ObjectHeader::Lookup(ObType::PtLogPage, oid);
  if (pObj == 0)
    db_error("not in core\n");

  kva_t kva = ObjectCache::ObHdrToPage(pObj);
  
  db_printf("Log Page (hdr=0x%08x, data=0x%08x) 0x%08x%08x ac=0x%08x ot=%d\n",
	    pObj,
	    kva,
	    (uint32_t) (pObj->ob.oid >> 32),
	    (uint32_t) (pObj->ob.oid),
	    pObj->ob.allocCount,
	    pObj->obType);
}

void
db_page_cmd(db_expr_t, int, db_expr_t, char*)
{
  int	t;
  OID     oid;

  t = db_read_token();
  if (t != tNUMBER)
    db_error("expects OID\n");

  oid = db_tok_number;
	
  t = db_read_token();
  if (db_read_token() != tEOL) {
    db_error("?\n");
    /*NOTREACHED*/
  }

  ObjectHeader *pObj = ObjectHeader::Lookup(ObType::PtDataPage, oid);
  if (pObj == 0)
    db_error("not in core\n");

  kva_t kva = ObjectCache::ObHdrToPage(pObj);
  
  db_printf("Page (hdr=0x%08x, data=0x%08x) 0x%08x%08x ac=0x%08x ot=%d\n",
	    pObj,
	    kva,
	    (uint32_t) (pObj->ob.oid >> 32),
	    (uint32_t) (pObj->ob.oid),
	    pObj->ob.allocCount,
	    pObj->obType);
}

void
db_pframe_cmd(db_expr_t, int, db_expr_t, char*)
{
  int	t;
  uint32_t  frame;

  t = db_read_token();
  if (t != tNUMBER)
    db_error("expects OID\n");

  frame = db_tok_number;
	
  t = db_read_token();
  if (db_read_token() != tEOL) {
    db_error("?\n");
    /*NOTREACHED*/
  }

  ObjectHeader *pObj = ObjectCache::GetCorePageFrame(frame);
  if (pObj == 0)
    db_error("not in core\n");

  kva_t kva = ObjectCache::ObHdrToPage(pObj);
  
  db_printf("Frame (hdr=0x%08x, data=0x%08x) 0x%08x%08x ac=0x%08x ot=%d\n",
	    pObj,
	    kva,
	    (uint32_t) (pObj->ob.oid >> 32),
	    (uint32_t) (pObj->ob.oid),
	    pObj->ob.allocCount,
	    pObj->obType);
}

static void
db_print_net_stats(uint32_t ndx, NetInterface *ni)
{
  ni->updateStats(ni);
    
  db_printf("[%2d] (0x%08x) %4s [%02x:%02x:%02x:%02x:%02x:%02x]\n"
	    "     txWatchdog 0x%x  txCarrier 0x%x txHeartBeat 0x%x\n"
	    "     collis 0x%x txWindow 0x%x rxFifo 0x%x txAbort %d\n"
	    "     rxDrop 0x%08x%08x rxCRC 0x%08x%08x\n"
	    "     rxFrame 0x%08x%08x rxLen 0x%08x%08x\n"
	    "     rxOvrrn 0x%08x%08x rxPkts 0x%08x%08x\n"
	    "     txPkts 0x%08x%08x rxMore 0x%08x%08x\n"
	    "     rdStall 0x%08x%08x wrStall 0x%08x%08x\n"
	    "     rxCpuCy 0x%08x%08x txCpuCy 0x%08x%08x\n",
	    ndx,
	    ni,
	    ni->name,
	    ni->info.niAddr[0],
	    ni->info.niAddr[1],
	    ni->info.niAddr[2],
	    ni->info.niAddr[3],
	    ni->info.niAddr[4],
	    ni->info.niAddr[5],
	    ni->stats.txWatchdog,
	    ni->stats.txCarrierErrors,
	    ni->stats.txHeartbeatErrors,
	    ni->stats.collisions,
	    ni->stats.txWindowErrors,
	    ni->stats.rxFifoErrors,
	    ni->stats.txAbort,
	    (uint32_t) (ni->stats.rxDropped >> 32),
	    (uint32_t) (ni->stats.rxDropped),
	    (uint32_t) (ni->stats.rxCrcErrors >> 32),
	    (uint32_t) (ni->stats.rxCrcErrors),
	    (uint32_t) (ni->stats.rxFrameErrors >> 32),
	    (uint32_t) (ni->stats.rxFrameErrors),
	    (uint32_t) (ni->stats.rxLengthErrors >> 32),
	    (uint32_t) (ni->stats.rxLengthErrors),
	    (uint32_t) (ni->stats.rxOverrun >> 32),
	    (uint32_t) (ni->stats.rxOverrun),
	    (uint32_t) (ni->stats.rxPackets >> 32),
	    (uint32_t) (ni->stats.rxPackets),
	    (uint32_t) (ni->stats.txPackets >> 32),
	    (uint32_t) (ni->stats.txPackets),
	    (uint32_t) (ni->stats.rxMore >> 32),
	    (uint32_t) (ni->stats.rxMore),
	    (uint32_t) (ni->stats.rdStall >> 32),
	    (uint32_t) (ni->stats.rdStall),
	    (uint32_t) (ni->stats.wrStall >> 32),
	    (uint32_t) (ni->stats.wrStall),
	    (uint32_t) (ni->stats.rxCpuCycles >> 32),
	    (uint32_t) (ni->stats.rxCpuCycles),
	    (uint32_t) (ni->stats.txCpuCycles >> 32),
	    (uint32_t) (ni->stats.txCpuCycles));
}

void
db_eros_net_stats_cmd(db_expr_t addr, int have_addr,
		      db_expr_t /* count */, char * /* modif */)
{
  for (uint32_t ndx = 0; ndx < KTUNE_NNETDEV; ndx++) {
    NetInterface *ni = NetInterface::Get(ndx);
    if (ni == 0)
      continue;
    if (have_addr && ni != (NetInterface *) addr)
      continue;
    db_print_net_stats(ndx, ni);
  }
}

static void
db_clear_net_stats(NetInterface *ni)
{
  ni->updateStats(ni);
    
  bzero(&ni->stats, sizeof(ni->stats));
}

void
db_eros_net_clear_cmd(db_expr_t addr, int have_addr,
		      db_expr_t /* count */, char * /* modif */)
{
  for (uint32_t ndx = 0; ndx < KTUNE_NNETDEV; ndx++) {
    NetInterface *ni = NetInterface::Get(ndx);
    if (ni == 0)
      continue;
    if (have_addr && ni != (NetInterface *) addr)
      continue;
    db_clear_net_stats(ni);
  }
}

void
db_eros_net_conf_cmd(db_expr_t /* addr */, int /* have_addr */,
		      db_expr_t /* count */, char * /* modif */)
{
  for (uint32_t ndx = 0; ndx < KTUNE_NNETDEV; ndx++) {
    NetInterface *ni = NetInterface::Get(ndx);
    if (ni == 0)
      continue;

    db_printf("[%2d] (0x%08x) %4s irq %d io=0x%x mem=0x%08x [%02x:%02x:%02x:%02x:%02x:%02x]\n",
	      ndx,
	      ni,
	      ni->name,
	      ni->irq, ni->ioAddr, ni->addr,
	      ni->info.niAddr[0],
	      ni->info.niAddr[1],
	      ni->info.niAddr[2],
	      ni->info.niAddr[3],
	      ni->info.niAddr[4],
	      ni->info.niAddr[5]);
  }
}

void
pte_print(uint32_t addr, char *note, PTE *pte)
{
  db_printf("0x%08x %s ", addr, note);
  pte->ddb_dump();
}

void
db_show_pterange_cmd(db_expr_t addr, int have_addr, db_expr_t, char*)
{
  PTE *space;
  uint32_t base, len, top;
  int	t;
  
  t = db_read_token();
  if (t != tNUMBER)
    db_error("expects space base len\n");

  space = (PTE *) db_tok_number;
	
  t = db_read_token();
  if (t != tNUMBER)
    db_error("expects space base len\n");

  base = db_tok_number;
  
  t = db_read_token();
  if (t != tNUMBER)
    db_error("expects space base len\n");

  len = db_tok_number;
  
  t = db_read_token();
  if (db_read_token() != tEOL) {
    db_error("?\n");
    /*NOTREACHED*/
  }

  if ((uint32_t)space & EROS_PAGE_MASK)
    db_error("base must be a page address\n");
  
  top = base + len;

  while (base < top) {
    uint32_t hi = (base >> 22) & 0x3ffu;
    uint32_t lo = (base >> 12) & 0x3ffu;

    PTE* pde = space + hi;
    pte_print(base, "PDE", pde);
    if (PTE_ISNOT(*pde, PTE_DV))
      db_printf("0x%08x PTE <invalid>\n", base);
    else {
      PTE *pte = (PTE *) pde->PageFrame();
      pte += lo;
      pte_print(base, "PTE", pte);

      uint32_t frm = pte->PageFrame();
      ObjectHeader *pHdr = ObjectCache::PageToObHdr(PTOV(frm));
      if (pHdr == 0)
	db_printf("*** NOT A VALID USER FRAME!\n");
      else if (pHdr->obType != ObType::PtDataPage)
	db_printf("*** FRAME IS INVALID TYPE!\n");

    }

    base += EROS_PAGE_SIZE;
  }
}
