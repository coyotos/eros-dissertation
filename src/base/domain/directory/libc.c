/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


typedef unsigned int    size_t;

/*
 * bzero -- vax movc5 instruction
 */
void
bzero(void *b, register size_t length)
{
  register char *p;

  for (p = b; length--;)
    *p++ = '\0';
}

void
bcopy(const void *from, void *to, register size_t len)
{
  /*
   * standard EROS bcopy
   */

  register char *fp = (char *) from;
  register char *tp = (char *) to;
    
  while (len--)
    *tp++ = *fp++;
}

char *
strcpy(char * dest,const char *src)
{
        char *tmp = dest;

        while ((*dest++ = *src++) != '\0')
          /* nothing */;
        return tmp;
}

int
strcmp(s1, s2)
     register const char *s1, *s2;
{
  while (*s1 == *s2++)
    if (*s1++ == 0)
      return (0);
  return (*(const unsigned char *)s1 - *(const unsigned char *)(s2 - 1));
}

