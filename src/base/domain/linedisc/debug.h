/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __DEBUG_H__
#define __DEBUG_H__

#ifndef NDEBUG
#define dbg_init      0x01u
#define dbg_destroy   0x02u
#define dbg_write     0x04u
#define dbg_writebuff 0x08u
#define dbg_waitevent 0x10u
#define dbg_postevent 0x20u
#define dbg_inpproc   0x40u
#define dbg_control   0x80u

#define dbg_all       0xFFu

#define dbg_flags (0u /*|dbg_init*/ |dbg_destroy ) 

#define DEBUG_CND(x) ((dbg_flags) & (dbg_ ## x))

#else
#define DEBUG_CND(x) (0u)
#endif /* NDEBUG */

#define DEBUG(x) if (DEBUG_CND(x))


#define KR_OSTREAM 7

#endif /* __DEBUG_H__ */

