/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __LINEDISC_H__
#define __LINEDISC_H__

#include "assert.h"

#include <domain/LineDiscKey.h>

#include "writebuff.h"
#include "debug.h"
#include "util.h"

#define K_READERKEY  8   /* make sure this does not interfere with any
			    of linedisc_buddy's registers */

typedef enum bool {
  false = 0,
  true = 1
} bool;

enum LD_Status {
  LD_Stat_Uninitialized = 0, /* at startup */
  LD_Stat_Ready = 1,         /* after initialization -- while running */
  LD_Stat_TearDown = 2,      /* in linedisc's destroy_self routine --
				to notify buddy to stop using the
				buffer and destroy itself */
  LD_Stat_Gone = 3           /* When buddy is done with the buffer, it
				uses this to tell linedisc it is safe */
};

enum LD_WakeupReasons {
  LD_Wakeup_Now          = CharSrc_UserEvent(0),
    /* always posted by the linedisc process 
       -- just there to make sure that the buddy gets up. */
  LD_Wakeup_GotReader    = CharSrc_UserEvent(1),
    /* a new reader was stashed */
  LD_Wakeup_WriteRequest = CharSrc_UserEvent(2), 
    /* new data in write buffer */
  LD_Wakeup_EventPosted  = CharSrc_UserEvent(3), 
    /* an event was put into postedEvents */
  LD_Wakeup_ProcFlags    = CharSrc_UserEvent(4), 
    /* a change of processing flags occured */
  LD_Wakeup_Chars        = CharSrc_UserEvent(5),
    /* The special or control characters have been changed */
  LD_Wakeup_TearDown     = CharSrc_UserEvent(6)
   /* changed status to tear down -- tear down, buddy! */
};

typedef uint16_t cc_type;

struct linedisc_data {

};

struct buddy_data {
  uint32_t chrSrcEvents;
  uint32_t currentLDEvents;
  uint32_t curInpFlags;

  uint32_t wakeupEvents; /* unprocessed wakeupEvents from 
			    enum LD_WakeupReasons */
  uint8_t *buffPos;
  uint8_t *first_special; /* LINEMODE only -- first non-quoted special
			     character in buffer */

  uint8_t quoteNext; /* last character was the Quote character. */
  uint8_t topWrote; /* Top has written since last character read */

  cc_type current_CC[LD_Num_CC]; /* local copy of control copies */

  uint32_t cur_char_mask[256u >> 5];/* 256 / (8*sizeof(uint32_t) */
                                /* bit per character -- indicates
				   which are special chars -- local
				   copy */
  

};

struct shared {
  struct write_buff wrt_buff;

  volatile uint32_t readerWants;   /* == WORD_MAX means no reader */
  volatile uint32_t readerEventMask;

  volatile uint32_t postedEvents;  /* 0 -> events  by linedisc,
				      events -> 0  by buddy     */
  
  volatile uint32_t inpProcFlags;  /* mod by linedisc only */
  volatile uint32_t outpProcFlags;

  volatile uint32_t out_of_write_space; /* A write request failed to
					   write it's entire argument.
					   Reset at first successful
					   write */
  volatile cc_type control_chars[LD_Num_CC];

  volatile uint32_t char_mask[256u >> 5];
                                /* 256 / (8*sizeof(uint32_t) */
                                /* bit per character -- indicates
				   which are special chars */
  
  volatile enum LD_Status linedisc_status;

  /* the state buffers for linedisc and buddy
     -- NOTE: these should ONLY be touched by the associated process. */
  struct linedisc_data linedisc;
  struct buddy_data buddy; 

};

/* for checking the character mask */
#define CHECK_CHAR(char_mask, char) \
    !!(((char_mask)[((unsigned)(char))>>5]) & (1u<<((char) & ((1<<5)-1))))

#define SET_CHAR(char_mask, char) \
     (((char_mask)[((unsigned)(char))>>5]) |= (1u<<((char) & ((1<<5)-1))))

#define CLEAR_CHAR(char_mask, char) \
     (((char_mask)[((unsigned)(char))>>5]) &= ~(1u<<((char) & ((1<<5)-1))))

#define CLEAR_MASK(char_mask) \
     ((char_mask[0] = 0), (char_mask[1] = 0), (char_mask[2] = 0), \
      (char_mask[3] = 0), (char_mask[4] = 0), (char_mask[5] = 0), \
      (char_mask[6] = 0), (char_mask[7] = 0))

/*  memory structure of both linedisc and linedisc_buddy:
 * Their memory consists of a single node with the following structure:
 *
 * The first n < 10 pages (0x0000-0x9FFF) hold read-only code 
 * and constant data
 *
 * The 10th page (0xA000-0xAFFF) holds a shared page for the shared structure
 *
 * The 12th page (0xC000-0xCFFF) holds a non-shared page for
 *                                       recieve/send buffers
 * 
 * The 15th (or 31st) page (0xF000-0xFFFF of 0x1F000-0x1FFFF) holds a
 *                                              non-shared stack page 
 *
 * The holes in the memory are there to make sure smashing stuff
 * causes a keeper invocation.
 */
#define SHARED_SLOT 10
#define SHARED (*(struct shared *)(SHARED_SLOT * EROS_PAGE_SIZE))
                       /* location of shared structure */

/* non-shared recieve buffer */
#define DATABUFF_SLOT 12
#define DATABUFF_LEN  EROS_PAGE_SIZE
#define DATABUFF ((uint8_t *)(DATABUFF_SLOT * EROS_PAGE_SIZE))
                         /* location of non-shared buffer */
#define DATABUFF_END (DATABUFF+DATABUFF_LEN)

#endif /* __LINEDISC_H__ */
