/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __WRITEBUFF_H__
#define __WRITEBUFF_H__

#define WRITE_BUFFSIZE 2048 /* must be a power of 2
			      -- buffer actually takes up this + 12 bytes*/

#define WRITE_BMASK (WRITE_BUFFSIZE - 1)

struct write_buff {
  char writebuffer[WRITE_BUFFSIZE];
  struct write_buff_ctl {
    /* uint32_t bottom; */ /* Commented out -- always == top - WRITE_BUFFSIZE */ 
    volatile uint32_t reserved; /* here for speed */
    volatile uint32_t top;
    volatile uint32_t written;
  } ctl;
};

/* INVARIENT:  bottom <= written <= reserved < top */
/*   (not actually true, because uint32_t will roll over, but close enough)
 *   NOTE: top must always be greater than reserved, or it is
 *         impossible to tell whether the buffer is full or not.
 */

/* x is a integer between bottom and top */

/*   bottom <= x < written  -> data ready to be written       */
/*  written <= x < reserved -> space reserved for writing     */
/* reserved <= x < top      -> free space avail for reserving */

/*  free space availible = (top - reserved)                   */

void
init_write_buff_ctl(register struct write_buff_ctl *toInit);

#define WBUFF_AVAIL_FOR_RESERVE(buf) \
     (((buf)->ctl.top - (buf)->ctl.reserved - 1) & WRITE_BMASK)

uint32_t
reserve_space(register struct write_buff_ctl *ctl,
	      uint32_t len,
	      uint32_t *reservedPos);
/* reserves /len/ characters in /ctl/, returning the offset (which
   should be ANDed with WRITE_BMASK when used) to start writing in the
   buffer controlled by /ctl/. */
   
   
#define FINISH_WRITING(ctl,len,resPos) \
              (((ctl)->written == (resPos))?  /* if written == pos */ \
                 ((ctl)->written += (len)),1 :  /* written  += len */ \
                                           0) /* else fail         */
/* Takes a passed in length (/len/), and a reservedPos, which should
   be the same as were passed in/returned (respectively) from a call
   to reserve_space, and attempts to mark the space specified as
   "written".  This call should be repeated until it returns a
   non-zero value (possibly with a yield between calls), at which
   point the data is available for taking out of the buffer. */
     
#define WBUFF_NUM_WRITTEN(buf) \
     (((buf)->ctl.top - (buf)->ctl.written) & WRITE_BMASK)

uint32_t
get_chars_to_remove(register struct write_buff *buffer,
		    const char **outPos,
		    uint32_t *num);
/* Returns a pointer and count to the current set of removeable
   characters in /buffer/.  Returns 0 if there are no characters
   available, 1, plus a pointer in *(/outPos/) and a count in *(/num),
   if there are characters available.  Note that the count *can* be
   less than that returned by WBUFF_NUM_WRITTEN, since the stored
   characters may not be contiguous. */


#define REMOVE_WRITTEN(buff,numChars) \
   do { \
     assert((((buff)->ctl.written - (buff)->ctl.top) & WRITE_BMASK) \
	                 >= (numChars)); \
     (buff)->ctl.top += (numChars); \
   } while(0)

/* Removes the first /numChars/ written bytes from /buffer/.  Allows
   the space they take up to be used for writes. */
#endif /* __WRITEBUFF_H__ */

