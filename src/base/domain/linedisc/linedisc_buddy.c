/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/ReturnerKey.h>
#include <eros/ProcessKey.h>
#include <eros/NodeKey.h>
#include <eros/SleepKey.h>
#include <eros/Key.h>
#include <eros/StdKeyType.h>

#include "linedisc.h"
#include "chars.h"

#include <domain/SpaceBankKey.h>
#include <domain/ProtoSpace.h>

#include <domain/domdbg.h>

#define KR_ZERO      0
#define KR_CONSTIT   1
#define KR_SELF      2
#define KR_DOMCRE    3
#define KR_BANK      4
#define KR_SCHED     5
#define KR_RETURNER  6
#define KR_OSTREAM   7 /* DEBUGGING */
#define KR_READERKEY K_READERKEY /* make sure this doesn't conflict */
#define KR_CHRSRC    9
#define KR_SCRATCH  10
#define KR_SCRATCH2 11

#if EROS_NODE_SIZE == 16
#define KR_ARG0     12  /* at startup, holds charsrc key */
#define KR_SCRATCH3   13  /* Holds a distingueshed start key to linedisc */
#define KR_ARG2     14
#define KR_RESUME   15 /* DON'T USE IT FOR ANYTHING ELSE! */
#elif EROS_NODE_SIZE == 32
#define KR_ARG0     28  /* at startup, holds charsrc key */
#define KR_SCRATCH3   29 /* Holds a distingueshed start key to linedisc */
#define KR_ARG2     30
#define KR_RESUME   31 /* DON'T USE IT FOR ANYTHING ELSE! */
#endif

#define KC_RETURNER 1
#define KC_OSTREAM  2  /* DEBUGGING */

#define KC_PROTO    4
#define KC_SLEEP    5

#define BUDDY SHARED.buddy

#define POST_EVENT(evnts) (void)(BUDDY.currentLDEvents |= (evnts))
#define READER_READY() (!!(SHARED.readerEventMask & BUDDY.currentLDEvents))

/* setup stack */
const uint32_t __rt_stack_pages = 0x1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif

/* this is the under-routine for assert, defined in debug.h */
#ifndef NDEBUG
int __assert(const char *expr, const char *file, int line)
{
  kdprintf(KR_OSTREAM, "%s:%d: Assertion failed: '%s'\n",
	   file, line, expr);
  return 0;
}
#endif

/* this is the under-routine for YIELD, defined in util.h */
void
__yield()  /* SMASHES KR_SCRATCH2 */
{
   node_copy(KR_CONSTIT, KC_SLEEP, KR_SCRATCH2);
   sl_sleep(KR_SCRATCH2, 0); /* sleep for 0 ms -- yields the processor */
}

static inline void *
memcpy(void *destptr, const void *srcptr, unsigned len)
{
  register uint8_t *dest = destptr;
  register const uint8_t *src = srcptr;
  register const uint8_t *end = src + len;
  
  while (src < end) *(dest++) = *(src++);

  return destptr;
}

void
destroy_self(uint32_t retCode)
{
  register uint32_t retval;
  
  /* get my address space into scratch */
  retval = process_copy(KR_SELF, ProcAddrSpace, KR_SCRATCH);
  assert(retval == RC_OK);

  DEBUG(destroy)
    kprintf(KR_OSTREAM,
	    "LineDiscBuddy: destroying databuff page\n");

  /* get data page into scratch2 */
  retval = node_copy(KR_SCRATCH, DATABUFF_SLOT, KR_SCRATCH);
  assert(retval == RC_OK);

  if (key_kt(KR_SCRATCH) == AKT_DataPage) {
    /* sell the page back to the space bank */
    retval = spcbank_return_data_page(KR_BANK, KR_SCRATCH);
    if (retval != RC_OK) {
      kdprintf(KR_OSTREAM,"Returning databuff page to spacebank failed!\n");
    }
  } else {
    DEBUG(destroy)
      kprintf(KR_OSTREAM,
	      "LineDiscBuddy: Databuff not page\n");
  }

  /* Don't sell back the shared page -- that's linedisc's job */
  /* remove it from our address space, though -- don't want protospace
     removing it */
  retval = process_copy(KR_SELF, ProcAddrSpace, KR_SCRATCH);
  assert(retval == RC_OK);
  
  node_swap(KR_SCRATCH,SHARED_SLOT,KR_ZERO,KR_ZERO);
  
  DEBUG(destroy)
    kprintf(KR_OSTREAM,
	    "LineDiscBuddy: Suiciding\n");
  
  /* all of my stuff is gone -- better kill myself */
  node_copy(KR_CONSTIT,KC_PROTO,KR_SCRATCH);

  /* FIXME: Due to Intel's broken architecture, I can't return anything but
     RC_OK to the caller.  If retCode != RC_OK, what should I do? */
  
  DEBUG(destroy)
    kdprintf(KR_OSTREAM,
	    "LineDiscBuddy: Suiciding\n");
  
  protospace_destroy(KR_RETURNER,
		     KR_SCRATCH,
		     KR_SELF,
		     KR_DOMCRE,
		     KR_BANK,
		     1); /* small space */
  /* NOTREACHED */
}

uint32_t
finish_init_protocol(uint32_t krForShared)
{
  register uint32_t retval;
  Message msg;

  assert(krForShared != KR_SCRATCH3);
  
  retval = process_make_start_key(KR_SELF, 0u, KR_SCRATCH3);
  assert("making start key" && retval == RC_OK);
  
  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "LineDiscBuddy: RETURNing to linedisc\n");
  
  msg.snd_key0 = KR_SCRATCH3; /* send him a start key */
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_w1 = 0u;
  msg.snd_w2 = 0u;
  msg.snd_w3 = 0u;
  msg.snd_len = 0u; /* no data */

  msg.rcv_key0 = krForShared; /* he'll return shared page */
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_RESUME;  /* for his resume key */
  msg.rcv_len = 0u; /* no data */
  
  msg.snd_invKey = KR_RESUME;
  msg.snd_code = RC_OK; /* We're doing fine */

  retval = RETURN(&msg); /* he should CALL us back */
  assert("returning to linedisc" && retval == RC_OK);

  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "LineDiscBuddy: We're back! SEND him domain key\n");
  
  /* he's now CALLed us -- we need to SEND him our domain key */
  msg.snd_key0 = KR_SELF; /* send him my domain key */

  msg.rcv_key0 = KR_ZERO; /* no return */

  msg.snd_invKey = KR_RESUME;
  msg.snd_code = RC_OK;

  SEND(&msg);  /* fork linedisc */
  /* no way to tell if it succeeded  */

  return RC_OK;
}

void
initialize_linedisc_buddy(void)
{
  register uint32_t retval;

  /* copy stuff out of constit */
  node_copy(KR_CONSTIT, KC_RETURNER, KR_RETURNER);
  node_copy(KR_CONSTIT, KC_OSTREAM,  KR_OSTREAM);

  /* copy the character source key into its key reg. (I could use
     KR_SELF, but the Returner key is much cooler) */
  copy_key_reg(KR_RETURNER, KR_ARG0, KR_CHRSRC);

  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "LineDiscBuddy: Returner worked -- buying shared page\n");
  
  /* buy a new page and insert it into address space for the databuff*/
  retval = process_copy(KR_SELF, ProcAddrSpace, KR_SCRATCH);
  assert("process_copy(ProcAddrSpace)" && retval == RC_OK);

  retval = spcbank_buy_data_pages(KR_BANK, 1, KR_SCRATCH2, 0, 0);
  assert("spcbank_buy_data_pages(1)" && retval == RC_OK);

  /* swap the new node in, putting the old contents key over address node */
  retval = node_swap(KR_SCRATCH, DATABUFF_SLOT, KR_SCRATCH2, KR_SCRATCH);
  assert("node_swap(AddrSpc,DATABUFF_SLOT,newPage)" && retval == RC_OK);

  DEBUG(init) {
    kprintf(KR_OSTREAM,
	    "LineDiscBuddy: Databuff page installed -- checking "
	    "previous occupant.\n");
    if (key_kt(KR_SCRATCH) != AKT_Number) {
      kdprintf(KR_OSTREAM,
	      "Hey! the previous occupant wasn't a number!\n");
    }
    kprintf(KR_OSTREAM,
	    "LineDiscBuddy: Checking that databuff is actually there.\n");
    assert(DATABUFF[0] == 0);
    DATABUFF[0] = 1;
    assert(DATABUFF[0] != 0);
    DATABUFF[0] = 0;

    kprintf(KR_OSTREAM,
	    "LineDiscBuddy: Databuff there. \n");
    
  }

  /* get the shared page from linedisc */
  retval = finish_init_protocol(KR_SCRATCH2);
             /* puts the shared page in SCRATCH2 */
  assert("finish_init_protocol" && retval == RC_OK);

  assert(key_kt(KR_SCRATCH2) == AKT_DataPage);
  
  retval = process_copy(KR_SELF, ProcAddrSpace, KR_SCRATCH);
  assert("process_copy(ProcAddrSpace)" && retval == RC_OK);

  /* swap the new node in, putting the old contents over the address
     node */
  retval = node_swap(KR_SCRATCH, SHARED_SLOT, KR_SCRATCH2, KR_SCRATCH);
  assert("node_swap(AddrSpc,SHARED_SLOT,newPage)" && retval == RC_OK);

  DEBUG(init) {
    kprintf(KR_OSTREAM,
	    "Shared page there -- checking to make sure there was no "
	    "previous occupant.\n");
    if (key_kt(KR_SCRATCH) != AKT_Number) {
      kdprintf(KR_OSTREAM,
	      "Hey! the previous occupant wasn't a number! (%08x)\n",
	       key_kt(KR_SCRATCH));
    }
  }

  if (SHARED.linedisc_status == LD_Stat_Uninitialized) {
    kdprintf(KR_OSTREAM, "LineDiscBuddy: Shared page isn't initialized!\n");
  }
  /* initialiaze BUDDY */
  BUDDY.buffPos = DATABUFF;
  SET(BUDDY.wakeupEvents,LD_Wakeup_Chars|LD_Wakeup_ProcFlags);
  
  /* done intializing */
  return;
}

/* tty_write:
      This code is complicated by the need to to NL->CRNL
    translation.  This is done with some extra code and a flag.  The
    extra code does two things:

    1.  It makes sure that we only send a newline as the first
      character sent.

    2.  If the first character to send *is* a newline, it checks the
      flag.  If the flag (returnSent) is:

      true:  the \r char has been successfully sent
     false:  the \r char has not yet been sent.  It attempts to send a
            \r char.  If this succeeds, the flag is set.  If it fails,
	    the whole routine is exited, to be retried later.

      After getting past the test, it knows that \r has been sent, so
      it can then continue, having the \n sent with the rest of the
      characters.

   The returnSent flag is set to false whenever a character is
   successfully sent.

   NOTE: This expects that all other output processing was done using
         write_data_to_buffer.
  */  
void
try_write(void)
{
  uint32_t count;
  uint32_t newCount;
  
  const char *buffp;
  static bool returnSent = false;
  register uint32_t doProc = (SHARED.outpProcFlags & LD_Out_DoOutpProc)
                          && (SHARED.outpProcFlags & LD_Out_NLtoCRNL);
  
  while (get_chars_to_remove(&SHARED.wrt_buff,
			     &buffp,
			     &count)) {
    register uint32_t retval;

    if (doProc) {
      const char *curp = buffp;
      const char *end = buffp + count;

      if (*curp == '\n') {
	if (!returnSent) {
	  retval = charsrc_write(KR_CHRSRC,
				 1,"\r",
				 &newCount);
	  assert("charsrc_write \\r" && retval == RC_OK);

	  if (newCount == 1) {
	    returnSent = true; /* true -- the flag is set */
	    POST_EVENT(CharSrc_WriteSpaceAvailEvent);
	  } else {
	    break; /* failed -- get out of the entire routine */
	  }
	}
	/* \r has been sent and the flag is set
	   -- treat this as a normal character */
	curp++;
      }

      while (curp < end && *curp != '\n')
	curp++;

      if (curp < end)
	count = curp - buffp; /* write up to, but not inc., the NL */
    }

	
    retval = charsrc_write(KR_CHRSRC,
			   count, buffp,
			   &newCount);
    /* overwrote count with the actually written count */

    assert("charsrc_write" && retval == RC_OK);

    DEBUG(write)
      kprintf(KR_OSTREAM,
	      "LineDiscBuddy: charsrc_write done "
	      "(actual_count = %08x)\n",
	      newCount);
    
    REMOVE_WRITTEN(&SHARED.wrt_buff, newCount);

    DEBUG(write)
      kprintf(KR_OSTREAM,
	      "LineDiscBuddy: REMOVE_WRITTEN returned. "
	      "(written %08x, reserved %08x, top %08x)\n",
	      SHARED.wrt_buff.ctl.written,
	      SHARED.wrt_buff.ctl.reserved,
	      SHARED.wrt_buff.ctl.top);

    if (newCount != 0)
       returnSent = false;
    /* we sent the associated \n, so the returnSent flag is cleared */
    
    if (newCount != count) {
      /* we didn't write the entire buffer -- return */
      break;
    } else {
      /* Only necessary to post this if they have failed a write
	 request */
      if (SHARED.out_of_write_space) {
	POST_EVENT(CharSrc_WriteSpaceAvailEvent);
	SHARED.out_of_write_space = 0;
      }
    }
  } 
}

void 
write_string(char *string)
{
  while (!write_data_to_buffer(&SHARED.wrt_buff,
			       SHARED.outpProcFlags,
			       string,
			       strlen(string))) {
    try_write();
  }
}

void
write_beep(void)
{
  write_string(BELL_STRING);
}

void
fork_reader(uint32_t numBytes)
{
  Message msg;

  assert(numBytes <= BUDDY.buffPos - DATABUFF); 

  DEBUG(waitevent)
    kdprintf(KR_OSTREAM,
             "Linedisc_buddy: FORKing reader (events 0x%08x bytes 0x%08x)\n",
             BUDDY.currentLDEvents,
             numBytes);

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;

  msg.snd_w1 = numBytes;
  msg.snd_w2 = BUDDY.currentLDEvents;
  BUDDY.currentLDEvents = 0u;

  msg.snd_w3 = 0u;

  msg.snd_len = numBytes;
  msg.snd_data = DATABUFF;
  
  msg.snd_code = RC_OK;

  /* stash the key into a temp register, then reset readerwants.  This
     way, we can recieve a reader request even if we don't run for a
     while after the SEND. (if we didn't copy the key, then linedisc
     could smash it between our resetting readerWants and invoking it)*/
  copy_key_reg(KR_RETURNER,KR_READERKEY,KR_SCRATCH);
  msg.snd_invKey = KR_SCRATCH;

  SHARED.readerWants = UINT32_MAX; /* okay, all of the reader
				    information can be smashed. */

  SEND(&msg);

  if (numBytes < BUDDY.buffPos - DATABUFF) {
    memcpy(DATABUFF + numBytes, DATABUFF, numBytes);
    /* this assumes memcopy copies first->last */
    BUDDY.buffPos -= numBytes;
  } else {
    BUDDY.buffPos = DATABUFF;
  }
}

void
raw_fork_reader(uint32_t numBytes)
{
  /* SEND to the reader, returning at maximum /numBytes/ bytes. */
  DEBUG(waitevent)
    kprintf(KR_OSTREAM,
	    "LineDiscBuddy -- Forking reader: maxuint8_ts %d buffPos 0x%04x"
	    " readerWants %d\n",
	    numBytes,
	    BUDDY.buffPos - DATABUFF,
	    SHARED.readerWants
	    );
  
  if (BUDDY.buffPos - DATABUFF < numBytes)
    numBytes = BUDDY.buffPos - DATABUFF;

  if (numBytes > SHARED.readerWants)
    numBytes = SHARED.readerWants;

  DEBUG(waitevent)
    kprintf(KR_OSTREAM,
	    "                 actualBytes: %d\n",numBytes);
  
  fork_reader(numBytes); /* do the fork and update the buffer */
}

void
rawInputProcess(char *buffpos, uint32_t num)
{
  DEBUG(inpproc)
    kprintf(KR_OSTREAM,
	    "Processing characters\n");
  
  if (BUDDY.curInpFlags & LD_In_Echo) {
    const char *curp = buffpos;
    uint32_t maxCnt = num;
    uint32_t count;
	
    while (maxCnt > 0) {
      /* FIXME: assumes ascii, is slow -- do a lookup table*/
      uint32_t retval = 0;

      if ((BUDDY.curInpFlags & LD_In_EchoCtrl)
	  && CCLASS(*curp) != 0) {
	char toprnt[2] = "^A";
	if (*curp <= 32) {
	  toprnt[1] = *curp + '@';
	} else { /* *curp == 127 */
	  toprnt[1] = '?';
	}

	do {
	  retval = write_data_to_buffer(&SHARED.wrt_buff,
					SHARED.outpProcFlags,
					toprnt,
					2);
	  if (retval == 0)
	    try_write();
	      
	} while (retval == 0);
	    
	/* skip past the character */
	curp++;
	maxCnt--;
	continue;
      }

      count = 0;

      if (BUDDY.curInpFlags & LD_In_EchoCtrl) {
	/* skip past all non-ctrl chars */
	while (count < maxCnt &&
	       CCLASS(curp[count]) == 0)
	  count++;
      } else {
	/* we can write them all */
	count = maxCnt;
      }

      do {
	retval = write_data_to_buffer(&SHARED.wrt_buff,
				      SHARED.outpProcFlags,
				      curp,
				      count);
	if (retval == 0)
	  try_write();
	    
      } while (retval == 0);

      curp += count;
      maxCnt -= count;
    }
  }
}

void
rawLoop(void)
{
  uint32_t event_mask = CharSrc_ALL_Events;
  uint32_t numRet;
  uint32_t numToGet;
  
  if (SHARED.readerWants == UINT32_MAX) {
    /* no reader */
    numToGet = DATABUFF_END - BUDDY.buffPos;
    DEBUG(waitevent)
      kprintf(KR_OSTREAM,
	      "LineDiscBuddy: no reader -- numToGet %04x\n",
	      numToGet);
  } else {
    uint32_t got = BUDDY.buffPos - DATABUFF;
    DEBUG(waitevent)
      kprintf(KR_OSTREAM,
	      "LineDiscBuddy: reader -- got %04x, wants %04x\n",
	      got, SHARED.readerWants);
    if (got >= SHARED.readerWants) {
      DEBUG(waitevent)
	kprintf(KR_OSTREAM,
		"LineDiscBuddy: reader got what he wants -- "
		"post FilledBufferEvent\n");
      POST_EVENT(CharSrc_FilledBufferEvent);
      if (READER_READY()) {
	raw_fork_reader(SHARED.readerWants);
	return;
      }
      return; 
    } else {
      numToGet = SHARED.readerWants - got;
      DEBUG(waitevent)
	kprintf(KR_OSTREAM,
		"LineDiscBuddy: Reader needs %04x\n",
		numToGet);
    }
  }
  
  while (RC_OK == charsrc_wait_for_event(KR_CHRSRC,
					 numToGet,
					 BUDDY.buffPos,
					 &numRet,
					 event_mask,
					 &BUDDY.chrSrcEvents)) {

    DEBUG(waitevent)
      kprintf(KR_OSTREAM,
	      "LineDiscBuddy: WaitForEvent returned. (numRet %03x, "
	      "events %02x)\n",
	      numRet,
	      BUDDY.chrSrcEvents);
    
    BUDDY.wakeupEvents |= (BUDDY.chrSrcEvents & CharSrc_UserEventMask);

    if (SHARED.postedEvents) {
      DEBUG(postevent)
	kprintf(KR_OSTREAM,
		"LineDiscBuddy: Posting events %02x\n",SHARED.postedEvents);
      
      BUDDY.currentLDEvents |= SHARED.postedEvents;
      SHARED.postedEvents = 0u;
    }
    BUDDY.wakeupEvents &= ~LD_Wakeup_EventPosted;
    
    if (WBUFF_NUM_WRITTEN(&SHARED.wrt_buff) != 0u) {
      DEBUG(write)
	kprintf(KR_OSTREAM, "LineDiscBuddy: Calling tty_write\n");
      try_write();
    }
    BUDDY.wakeupEvents &= ~LD_Wakeup_WriteRequest;

    /* process the characters */
    if (BUDDY.curInpFlags & LD_In_DoInpProc) {
      rawInputProcess(BUDDY.buffPos, numRet);
    }

    /* mark then as processed */
    if (numRet) {
      BUDDY.buffPos += numRet;
    }

    /* process events -- first, post any events from above that aren't
     * for me.
     */
    POST_EVENT(BUDDY.chrSrcEvents
	       & ~(CharSrc_FilledBufferEvent
		   | CharSrc_UserEventMask));

    /* then, if we have a reader, check readerwants. */
    if (SHARED.readerWants != UINT32_MAX) {
      uint32_t got = BUDDY.buffPos - DATABUFF;

      if (got >= SHARED.readerWants) {
	POST_EVENT(CharSrc_FilledBufferEvent);
	numToGet = 0u;  
      } else {
	numToGet = SHARED.readerWants - got;
      }
    }

    if (WBUFF_NUM_WRITTEN(&SHARED.wrt_buff) != 0u) 
      try_write();

    if (READER_READY()) 
      raw_fork_reader(SHARED.readerWants);

    if (SHARED.readerWants == UINT32_MAX) {
      /* no reader */
      numToGet = DATABUFF_END - BUDDY.buffPos;
    }

    if (BUDDY.wakeupEvents)
      break;
    /* FIXME: flush buffer? */
  }

  kprintf(KR_OSTREAM,
	  "Linedisc_buddy -- end of loop!\n");
  
  return;
}

uint8_t *
linemode_find_first_special_char(void)
{
  /* find the first non-escaped control character */
  register uint8_t *pos;
  register uint8_t *end = BUDDY.buffPos;
  cc_type quote_char = BUDDY.current_CC[LD_CC_Quote];

  if (quote_char) { /* we have to check for quoted chars */
    int quoted = 0;
    for (pos = DATABUFF; pos < end; pos++) {
      if (quoted) continue;
      if (CCLASS(*pos) == CC_NEWLINE)
	break; 
      else if (*pos == (uint8_t)quote_char)
	quoted = 1;
      else if (CHECK_CHAR(SHARED.char_mask,*pos)) 
	break; /* this is a special character */
    }
  } else {
    for (pos = DATABUFF; pos < end; pos++) {
      if (CCLASS(*pos) == CC_NEWLINE
	  || CHECK_CHAR(SHARED.char_mask,*pos))
	break;
    }
  }
  if (pos == end) {
    return NULL; /* no special characters */
  }
  return pos;
}

void
linemode_fork_reader(void)
{
  uint32_t numBytes;

  /* SEND to the reader.  */
  if (BUDDY.currentLDEvents & CharSrc_SpecialCharEvent) {
    if (!BUDDY.first_special) {
      kdprintf(KR_OSTREAM,
	       "LineDisc_Buddy: HEY! I can't find the "
	       "special character to end the line!\n");
      /* for now, send the whole thing, I guess */
      numBytes = BUDDY.buffPos - DATABUFF;
      
    } else {
      numBytes = BUDDY.first_special - DATABUFF + 1; /* include first
							special */
    }
  } else {
    numBytes = 0; /* Don't have a full line yet. */
  }

  DEBUG(waitevent)
    kprintf(KR_OSTREAM,
	    "                 actualBytes: %d\n",numBytes);
  
  fork_reader(numBytes); /* do the fork and update the buffer */
  
  /* update the first_special pointer */
  BUDDY.first_special = linemode_find_first_special_char();
  if (BUDDY.first_special) POST_EVENT(CharSrc_SpecialCharEvent);
}

void
linemode_echo(uint8_t theChar) 
{
  uint8_t theOut[3] = {theChar,0,0};

  if ( !ISSET(BUDDY.curInpFlags, LD_In_Echo) )
    return;

  if (CCLASS(theChar) != 0
      && ISSET(BUDDY.curInpFlags, LD_In_EchoCtrl)
      && CCLASS(theChar) != CC_NEWLINE
      && CCLASS(theChar) != CC_TAB) {
    theOut[0] = '^';
    theOut[1] = (theChar == 0x7F)? '?':theChar + 'A' - 1;
  } 
  write_string(theOut);
}

void
lineModeProcess(uint32_t numRet) /* process chars in linemode */
{
  register uint8_t *curChar = BUDDY.buffPos;

  for (; numRet > 0; numRet--,curChar++) {
    /* basically a large case statement. Continue when done with
       character (make sure to increment buffPos if necessary */
    uint32_t pos = BUDDY.buffPos - DATABUFF;

    assert(pos < EROS_PAGE_SIZE);
    if (pos == EROS_PAGE_SIZE - 1) {
      write_beep();
      continue;
    }

    if (ISSET(BUDDY.curInpFlags,LD_In_SwapCRNL)) {
      switch (*curChar) {
      case '\n':
	*curChar = '\r';
	break;
      case '\r':
	*curChar = '\n';
	break;
      }
    }

    if (pos == EROS_PAGE_SIZE - 2) {
      /* we are running out of space -- Accept a special character for
	 the 2nd to last character, and nothing for the last (we must
	 wait until the data is returned */
      if (BUDDY.quoteNext == 1
	  || (CCLASS(*curChar) != CC_NEWLINE
	      && CHECK_CHAR(BUDDY.cur_char_mask,*curChar))) {
	write_beep();
	BUDDY.quoteNext = 0;
	continue;
      }
    }

    if (BUDDY.quoteNext) {
      BUDDY.quoteNext = 0;
      linemode_echo(*curChar);
      *BUDDY.buffPos++ = *curChar;
      continue;
    } else { /* not quoted */

      if (CCLASS(*curChar) == CC_NEWLINE) 
	goto is_special_character; /* goto the real routine */
      
      if (*curChar != 0) {
	/* possibly a controlling character -- this way, we don't have
	   to check if the control character is set.
	*/
	if (*curChar == BUDDY.current_CC[LD_CC_Erase]
	    || *curChar == BUDDY.current_CC[LD_CC_AltErase]) {
	  if (pos == 0) continue;
	  BUDDY.buffPos--; /* remove the previous character */
	  if (ISSET(BUDDY.curInpFlags,LD_In_VisErase)) {
#if 0
	    linemode_rubout(*BUDDY.buffPos);
#else
	    char str[2] = {BUDDY.current_CC[LD_CC_Erase],0};
	    write_string(str);
#endif
	  } else {
	    linemode_echo(BUDDY.current_CC[LD_CC_Erase]);
	  }
	  if (BUDDY.buffPos > DATABUFF
	      && BUDDY.current_CC[LD_CC_Quote]
	      && *(BUDDY.buffPos - 1) == BUDDY.current_CC[LD_CC_Quote]) {
#if 0
	    /* we need to check if this is quoted.  What we do is
	       count the number of consecutive quote chars before
	       this character, and if it is odd, eat the first one.
	    */
#else
	    BUDDY.buffPos--; /* for now, just assume no-one quotes the
				Quote character */
#endif
	  } 
	  continue;
	}
	if (*curChar == BUDDY.current_CC[LD_CC_EraseWord]) {
	  continue; /* FIXME: implement */
	}
	if (*curChar == BUDDY.current_CC[LD_CC_KillLine]) {
	  continue; /* FIXME: implement */
	}
	if (*curChar == BUDDY.current_CC[LD_CC_Reprint]) {
	  continue; /* FIXME: implement */
	}
	if (*curChar == BUDDY.current_CC[LD_CC_Quote]) {
	  if (ISSET(BUDDY.curInpFlags,LD_In_Echo)) {
	    if (ISSET(BUDDY.curInpFlags,LD_In_EchoCtrl)
		&& ISSET(BUDDY.curInpFlags,LD_In_VisErase)) {
	      uint8_t toSend[] = {'^',
				  BUDDY.current_CC[LD_CC_Erase],
				  0};
	      write_string(toSend);			     
	    } else {
	      linemode_echo(*curChar);
	    }
	  }
	    
	  *BUDDY.buffPos++ = *curChar;
	  BUDDY.quoteNext = 1;
	  continue;
	} 
      } /* if (*curChar != 0) */
      
      if (CHECK_CHAR(BUDDY.cur_char_mask,*curChar)) {
	/* this is a special character */
	linemode_echo(*curChar); /* echo it */
      is_special_character: /* from '\n' check */
	*BUDDY.buffPos = *curChar;

	POST_EVENT(CharSrc_SpecialCharEvent);
	if (!BUDDY.first_special) {
	  BUDDY.first_special = BUDDY.buffPos;
	}
	BUDDY.buffPos++;
	/* no matter what character it was, afterwards we echo
	   carriage return */
	if (ISSET(BUDDY.curInpFlags,LD_In_AlwEchoNL)
	    || ISSET(BUDDY.curInpFlags,LD_In_Echo))
	  write_string("\n");
      } else {
        /* this is a normal character */
        linemode_echo(*curChar);
        BUDDY.buffPos++;
      }
    }
  }
}

/* FIXME:  Currently, linemode makes assumptions that come down to the
           calling person is expected to be able to handle the full
	   buffer size -- EROS_PAGE_SIZE.  */
void
lineModeLoop(void)
{
  uint32_t event_mask = CharSrc_ALL_Events;
  uint32_t numRet;
  uint32_t numToGet;
  
  if (SHARED.readerWants != UINT32_MAX) {
    /* there is a reader -- check if he we're ready */
    if (READER_READY()) {
      linemode_fork_reader();
      return;
    }
  }

  /* no timeouts for now -- read input a character at a time. */
  /* NOTE: we must never let BUDDY.buffPos get out of the page */
  numToGet = 1;

  while (RC_OK == charsrc_wait_for_event(KR_CHRSRC,
					 numToGet,
					 BUDDY.buffPos,
					 &numRet,
					 event_mask,
					 &BUDDY.chrSrcEvents)) {
    DEBUG(waitevent)
      kprintf(KR_OSTREAM,
	      "LineDiscBuddy: WaitForEvent returned. (numRet %03x, "
	      "events %08x, %sreader)\n",
	      numRet,
	      BUDDY.chrSrcEvents,
              (SHARED.readerWants == UINT32_MAX)?"no ":"");
    
    BUDDY.wakeupEvents |= (BUDDY.chrSrcEvents & CharSrc_UserEventMask);
    
    if (SHARED.postedEvents) {
      DEBUG(postevent)
	kprintf(KR_OSTREAM,
		"LineDiscBuddy: Posting events %02x\n",SHARED.postedEvents);
      
      BUDDY.currentLDEvents |= SHARED.postedEvents;
      SHARED.postedEvents = 0u;

    }
    BUDDY.wakeupEvents &= ~LD_Wakeup_EventPosted;
    
    if (WBUFF_NUM_WRITTEN(&SHARED.wrt_buff) != 0u) {
      DEBUG(write)
	kprintf(KR_OSTREAM, "LineDiscBuddy: Calling tty_write\n");
      try_write();
    } else if (BUDDY.chrSrcEvents & CharSrc_WriteSpaceAvailEvent) {
      POST_EVENT(CharSrc_WriteSpaceAvailEvent);
    }
    BUDDY.wakeupEvents &= ~LD_Wakeup_WriteRequest;

    lineModeProcess(numRet); /* process the characters */
    /* BUDDY.buffPos now points to the end of the characters. */
    /* any applicable events have been posted. */


    /*    POST_EVENT(BUDDY.chrSrcEvents
	       & ~(CharSrc_FilledBufferEvent | CharSrc_UserEventMask));
    */

    if (WBUFF_NUM_WRITTEN(&SHARED.wrt_buff) != 0u) 
      try_write();

    if (READER_READY()) 
      linemode_fork_reader();

    if (BUDDY.wakeupEvents)
      break;
    /* FIXME: flush buffer? */
  }

  kprintf(KR_OSTREAM,
	  "Linedisc_buddy -- end of loop!\n");
  
  return;
}

void
raw_update_specialChars(void)
{
  uint16_t buff[256];
  uint16_t *buffp = buff;
  uint32_t idx;

  for (idx = 0; idx < 256; idx++) {
    if (CHECK_CHAR(BUDDY.cur_char_mask,idx))
      *buffp++ = idx;
  }
  charsrc_set_special_chars(KR_CHRSRC, buff, buffp - buff);
}

void
line_update_specialChars(void)
{
  uint16_t buff[256 + LD_Num_CC + 1]; /* overkill, but guarenteed to work */
  uint16_t *buffp = buff;
  uint32_t idx;

  DEBUG(control)
    kprintf(KR_OSTREAM,"LineDisc_buddy: Updating line-mode special chars:");

  for (idx = 0; idx < 256; idx++) {
    if (CHECK_CHAR(BUDDY.cur_char_mask,idx)) {
      DEBUG(control)
	kprintf(KR_OSTREAM," 0x%02x",idx);
      *buffp++ = idx;
    }
  }
  for (idx = 0; idx < LD_Num_CC; idx++) {
    uint16_t curChar = BUDDY.current_CC[idx];
    if (curChar) {
      /* the control character is active */
      if (((curChar & 0xFF) == curChar)
	  && CHECK_CHAR(BUDDY.cur_char_mask,(uint8_t)curChar)) {
	DEBUG(control)
	  kprintf(KR_OSTREAM," *0x%02x*",curChar);
      } else {
	DEBUG(control)
	  kprintf(KR_OSTREAM," 0x%02x",curChar);
	*buffp++ = curChar;
      }
    }
  }
  if (!CHECK_CHAR(BUDDY.cur_char_mask,'\r')) {
    DEBUG(control)
      kprintf(KR_OSTREAM," \\r");
    *buffp++ = '\r';
  }
  DEBUG(control)
    kdprintf(KR_OSTREAM," total %d\n",buffp-buff);

  
  charsrc_set_special_chars(KR_CHRSRC, buff, buffp - buff);
}

int 
main(void)
{
  initialize_linedisc_buddy(); /* either successful or destroys me. */

  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "LineDiscBuddy: Initialization successful\n");
  
  while (1) {
    uint32_t update_special_chars = 0;

    uint32_t new_flags = SHARED.inpProcFlags;
    
    if (ISSET(BUDDY.wakeupEvents,LD_Wakeup_Chars)) {
      DEBUG(control)
	kdprintf(KR_OSTREAM,
		 "Linedisc_buddy: Copying control_chars and char_mask\n");
      memcpy(BUDDY.current_CC,
	     (const char *)SHARED.control_chars, /* cast away volatile */
	     sizeof(SHARED.control_chars));
      memcpy(BUDDY.cur_char_mask,
	     (const char *)SHARED.char_mask, /* cast away volatile */
	     sizeof(SHARED.control_chars));

      update_special_chars = 1;

      BUDDY.wakeupEvents &= ~LD_Wakeup_Chars;      
    }

    if (BUDDY.curInpFlags != new_flags) {
      uint32_t inpProc = (BUDDY.curInpFlags & LD_In_DoInpProc);
      uint32_t nowInpProc = (new_flags & LD_In_DoInpProc);
      
      uint32_t lineMode = (BUDDY.curInpFlags & LD_In_LineMode);
      uint32_t nowLineMode = (new_flags & LD_In_LineMode);
      
      if ((inpProc && lineMode) && (!nowInpProc || !nowLineMode)) {
	/* line-mode to raw-mode */
	update_special_chars = 1;
	/* for now, flush all of the data */
	BUDDY.buffPos = DATABUFF;
	write_string("\r\nNow in raw mode\r\n");
      }
      if ((!inpProc || !lineMode) && (nowInpProc && nowLineMode)) {
	/* raw -> line mode */
	update_special_chars = 1;
	BUDDY.buffPos = DATABUFF;
	BUDDY.quoteNext = 0;
	BUDDY.topWrote = 0;
	BUDDY.first_special = NULL;
	write_string("\r\nNow in line mode\r\n");
      }
      /* assume for now that any other changes are safe */
      BUDDY.curInpFlags = new_flags;
      BUDDY.wakeupEvents &= ~LD_Wakeup_ProcFlags;
    }
    BUDDY.curInpFlags = new_flags;

    DEBUG(control)
      kprintf(KR_OSTREAM,
	      "Linedisc_buddy: entering main loop. %s spc_chars_upd\n",
	      update_special_chars?"YES":"no");

    if (!(BUDDY.curInpFlags & LD_In_DoInpProc)
	|| !(BUDDY.curInpFlags & LD_In_LineMode)) {
      DEBUG(inpproc)
	kdprintf(KR_OSTREAM,"Hey!  We're in raw mode!\n");
      if (update_special_chars) 
	raw_update_specialChars();

      rawLoop();
    } else {
      if (update_special_chars) 
	line_update_specialChars();
      lineModeLoop(); 
    }

    /* we've checked it */
    BUDDY.wakeupEvents &= ~LD_Wakeup_TearDown;
    if (SHARED.linedisc_status == LD_Stat_TearDown) {
      SHARED.linedisc_status = LD_Stat_Gone;
      destroy_self(RC_OK);
      /* NOTREACHED */
      break;
    }
    
  }

  kdprintf(KR_OSTREAM,
	   "Linedisc_buddy -- broke out of main!\n");

  return 0;
}
