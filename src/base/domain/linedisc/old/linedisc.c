/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "linedisc.h"
#include <domain/SpaceBankKey.h>


const uint32_t __rt_stack_pages = 0; 
const uint32_t __rt_stack_pointer = 0x21000;


static uint8_t rcvData[EROS_PAGE_SIZE];
static uint8_t snd_buffer[SIK_BUFSZ];


#define KR_CONSOLEKEY 9
#define KR_SLEEPKEY 10
#define KR_KEYBITS 11
#define KR_TTYIN 5
#define KR_TMP 8
#define KR_STASHKEY 7
#define KR_BANK 6

#define KR_RK0     12
#define KR_RK1     13
#define KR_RK2     14
#define KR_RETURN  15



struct stash {			 /* holds our stash of reader resume keys  */
  uint16_t keyslot;
  uint32_t rdbytes;
  struct stash *next;
};

/*
 * stash data structures
 */

struct stash stash_buf[MAX_STASH]; 
struct stash *stash_head;
struct stash *stash_tail;
uint16_t stash_full;
uint32_t current_stash;

struct shared *kdb_buf;		  /* points to the shared keyboard buffer */
uint16_t notify_flag = 0;

struct device console;		/* a single global device per instance
					   of linedisc */


void
bcopy(const void *from, void *to, uint32_t len)
{
     /*
      * standard EROS bcopy
      */

     uint8_t *fp = (uint8_t *) from;
     uint8_t *tp = (uint8_t *) to;
    
     while(len--)
	  *tp++ = *fp++;
}


int
strlen(const char *s)
{
     const char *send = s;

     while (*send)
	  send++;

     return send - s;
}


void
init_device (struct device *thisdevice)
{
     /*
      * FIX: more to come
      */

     thisdevice->pos = 1;	 /* ANSI console [1..80] */
     thisdevice->start = 1;	/* note the beginning of the line */
     thisdevice->width = 80;
     thisdevice->suspend = 0;
}

void
init ()
{

     /*
      * initialize various global vars; attach the shared page;
      * buy a node from spacebank for stashing resume keys;
      * initialize the stash list
      */

     uint16_t i;
     uint32_t Result;
     
     current_stash = 0;
     stash_head = 0;
     stash_tail = 0;
     stash_full = 0;

     kdb_buf = (struct shared *)  SH_ADDR;

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "linedisc: calling the spacebank\n");     
#endif
     
     Result = spcbank_buy_nodes(KR_BANK, 1, KR_STASHKEY, 0, 0);

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "linedisc: Result from spcbank is %x\n",Result);     
#endif
     
     if (Result != RC_OK)
       return;
     
     for (i = 0; i < MAX_STASH; i++)
     {
	  stash_buf[i].keyslot = i;
	  stash_buf[i].rdbytes = 0;
	  if (MAX_STASH - 1 == i)
	       stash_buf[i].next = stash_buf;
	  else
	       stash_buf[i].next = &stash_buf[i+1];
     }

     stash_head = stash_buf;
     stash_tail = stash_buf;

     return;
}

void
Writen(uint32_t n, const char *s)
{
     /*
      * a simple mechanism for writing characters to the console.
      * 
      * will eventually be replaced with somethine that buffers
      * and handles flow control
      */


     Message msg;

     msg.snd_key0 = KR_ZERO;
     msg.snd_key1 = KR_ZERO;
     msg.snd_key2 = KR_ZERO;
     msg.snd_key3 = KR_ZERO;

     msg.rcv_key0 = KR_ZERO;
     msg.rcv_key1 = KR_ZERO;
     msg.rcv_key2 = KR_ZERO;
     msg.rcv_key3 = KR_ZERO;

     msg.snd_data = (uint8_t *) s;
     msg.snd_len = n;

     msg.rcv_len = 0;		/* no data returned */
     msg.snd_invKey = KR_CONSOLEKEY;
     msg.snd_code = 1;

     (void) CALL(&msg);
}

void
Write(const char *s)
{
  Writen(strlen(s), s);
}

uint32_t 
diff(uint32_t head, uint32_t tail, uint32_t max, uint32_t full)
{
     /*
      * return the number of bytes between "head" and "tail", given
      * a max bufsz of "max"
      */

     if (full)
	  return max;
     
     if (head == tail)
	  return 0;
     if (head > tail)
	  return (head - tail);
     else
	  return (head + (max - tail));
  
}

void
fork_ttyin ( )
{
     /*
      * we've been told that TTYin is available and waiting
      * for us to SEND it when the buffer starts to empty.
      * Now it's time to do it.
      */

     Message msg;
     
#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "linedisc: SEND ttyin on a notify\n");
#endif


     msg.snd_key0 = KR_ZERO;
     msg.snd_key1 = KR_ZERO;
     msg.snd_key2 = KR_ZERO;
     msg.snd_key3 = KR_ZERO;
     msg.snd_len = 0;

     msg.snd_invKey = KR_TTYIN;
     msg.snd_code = RC_OK;

     (void) SEND(&msg); 
}



uint32_t
read_buf (uint32_t rdbytes, void *data)
{

     /*
      * perform data xfer between shared keyboard buffer
      * and data buffer
      *
      * update our pointers in shared buf.
      *
      * return number of bytes transferred.
      */

     uint32_t top;

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "line disc: READbuf %x bytes ... t %x h %x \n",
	      rdbytes, kdb_buf->tail, kdb_buf->head);
#endif
     
     top = SIK_BUFSZ-kdb_buf->tail;
     
     if (top > rdbytes)		        /* no wrap */
     {
	  bcopy( &(kdb_buf->buf[kdb_buf->tail]), data,
		 rdbytes);
	  kdb_buf->tail += rdbytes;
     }
     else				/* wrap buf */
     {
	  bcopy(&(kdb_buf->buf[kdb_buf->tail]), data,
		top );
	  kdb_buf->tail = 0;
	  bcopy(&(kdb_buf->buf[kdb_buf->tail]), data + top , 
		rdbytes - top);
	  kdb_buf->tail += rdbytes - top;
     }
	
     if ((rdbytes) && (kdb_buf->full)) /* reset full ptr */
     {
	  kdb_buf->full = 0;
	  if (notify_flag)
	       fork_ttyin();
	  notify_flag = 0;
     }

     return rdbytes;
}


void
fork_reader ( struct stash *reader )
{
     /*
      * SEND off a single reader from our stash structure
      * first copy the key from our stash into a tmpslot,
      * then SEND the key in that slot
      */


     Message msg;

     uint32_t rdbytes;

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "linedisc: Grab key in stash slot %d and SEND\n",reader->keyslot);
#endif

     node_copy(KR_STASHKEY, reader->keyslot, KR_TMP);

     msg.snd_key0 = KR_ZERO;
     msg.snd_key1 = KR_ZERO;
     msg.snd_key2 = KR_ZERO;
     msg.snd_key3 = KR_ZERO;
     msg.snd_data = snd_buffer;

     if (kdb_buf->mode == COOKED)
       rdbytes = diff(kdb_buf->head, kdb_buf->tail,
		      SIK_BUFSZ, kdb_buf->full);
     else
       rdbytes = reader->rdbytes;
     
     msg.snd_len = read_buf (rdbytes, snd_buffer);

     msg.snd_invKey = KR_TMP;
     msg.snd_code = RC_OK;

     (void) SEND(&msg); 
}


uint32_t 
stash_reader (uint16_t keyNum, uint32_t rdbytes)
{

     /*
      * stash a reader's return key and number of bytes
      * requested in an internal fixed-size list
      * 
      * the readers will be returned to once we
      * have enough bytes to satisfy their requests
      *
      * return 1 on succ; 0 if too many already stashed
      */


#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "linedisc: stashing reader for %x bytes\n",rdbytes);
#endif
  
     if ((!stash_head) || (stash_full))
	  return 0;

     node_swap(KR_STASHKEY, stash_head->keyslot, 
	       keyNum, KR_ZERO);  
     stash_head->rdbytes = rdbytes;
     stash_head = stash_head->next;
     if (stash_head == stash_tail)
	  stash_full = 1;
  
     current_stash += rdbytes;
     return 1;
}


uint32_t
ld_wakeup()
{

     /*
      * SEND stashed readers if necessary.  This function is
      * called when we get "awakened" by the TTYreader domain
      *
      * NOTE: We may remove support for *multiple* stashed
      * readers -- it just doesn't seem necessary in a
      * layered model
      */
     
     uint32_t avail = diff(kdb_buf->head, kdb_buf->tail,
		       SIK_BUFSZ, kdb_buf->full);

     if (kdb_buf->mode == RAW)
       {

	 /*
	  * why do we have the while loop?
	  */
	 
	 while ((avail) && (stash_head != stash_tail))
	   {
	     if (stash_tail->rdbytes <= avail)
	       fork_reader(stash_tail);
	     avail -= stash_tail->rdbytes;
	     current_stash -= stash_tail->rdbytes;
	     if (stash_full)
	       stash_full = 0;
	     stash_tail = stash_tail->next;
	   }
	 kdb_buf->wakeup = SIK_BUFSZ + 1; /* set wakeup out of range */
	 return (!current_stash);	      /* stash is zero on success */
       }
     else			/* COOKED */
       {

	 /*
	  * we got a \n, so wakeup the oldest stashee
	  */
	 
	 fork_reader(stash_tail);
	 current_stash -= avail;
	 if (stash_full)
	   stash_full = 0;
	 stash_tail = stash_tail->next;
	 return (!current_stash);
	 
       }
}


void
set_wakeup (uint32_t rdbytes, uint32_t extra_bytes, uint32_t current_head)
{
     /*
      * set the wakeup pointer to a position in the kbb_buffer.
      * never set the pointer further back.
      */
     
     uint32_t current_wakeup = diff(kdb_buf->wakeup, current_head,
				SIK_BUFSZ, kdb_buf->full);
     
     if (current_wakeup > extra_bytes)
     {
	  kdb_buf->wakeup = current_head + (rdbytes - extra_bytes);
	  if (kdb_buf->wakeup >= SIK_BUFSZ)           /* wrap buf */
	       kdb_buf->wakeup = kdb_buf->wakeup - SIK_BUFSZ;
     }
}


uint32_t
check_readable(uint32_t rdbytes)
{
     /*
      * check to see if there are rdbytes available for reading.
      * return 1 if there are enough bytes.  return 0 and set
      * the wakeup pointer if there are not enough bytes.
      */

     uint32_t current_head = kdb_buf->head; /* don't use the shared variable */
     int32_t extra_bytes;		    /* bytes available for reading*/

     extra_bytes = diff(current_head, kdb_buf->tail, SIK_BUFSZ,
			kdb_buf->full) - current_stash; 

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "line disc: READ %x / %x bytes\n",
	      rdbytes, extra_bytes);
#endif
     
     if (extra_bytes >= rdbytes) {
	  return 1;
     }
     else {
	  set_wakeup (rdbytes, extra_bytes, current_head);
	  return 0;
     }
	  
}

void
subtract_buf (uint32_t len)
{
  /*
   * delete len number of characters from the keybd buffer
   */

  int i;
  
  for (i = 0 ; i < len; i++)
    {
      if (kdb_buf->head == 0)
	kdb_buf->head = SIK_BUFSZ-1;
      if (kdb_buf->head == kdb_buf->tail)
	break;
      kdb_buf->head--;
    }

}

void
discipline (struct device *thisdevice, char ctrlchar)
{
     /*
      * beginnings of handling non-printing characters
      */
  int tmp;
  
#ifdef DEBUG
  kprintf (KR_CONSOLEKEY, "linedisc: discipline on %x\n",
	   ctrlchar);
#endif

     
     switch (ctrlchar) {

     case ETB:		         /* ^W; kill to beginning of line */
          tmp = thisdevice->pos;
          for ( ; thisdevice->pos > thisdevice->start; thisdevice->pos--)
	    kprintf (KR_CONSOLEKEY, "%c %c", BS, BS);
	  subtract_buf(tmp - thisdevice->pos);
	  break;
     case DC1:			 /* ^Q; resume */
	  Writen(thisdevice->pos - thisdevice->suspend,
		 &thisdevice->buf[thisdevice->suspend]);
	  thisdevice->suspend = 0;
	  break;
     case DC3:			 /* ^S; suspend */
	  thisdevice->suspend = thisdevice->pos;
	  break;
     case DC2:		         /* ^R: refresh */
	  kprintf (KR_CONSOLEKEY, "%c\033[K", CR);
	  Writen(thisdevice->pos, thisdevice->buf);
	  break;
     case BS:			 /* backspace */
	  if (thisdevice->pos > thisdevice->start)
	    {
	      thisdevice->pos = thisdevice->pos - 1;
	      subtract_buf(1);
	      kprintf (KR_CONSOLEKEY, "%c %c", BS, BS);
	    }
	  break;
     case CR:
     case LF:
	  thisdevice->pos = 1;
	  thisdevice->start = 1;
	  kprintf (KR_CONSOLEKEY, "%c%c", CR, LF);
	  break;
     case TAB:			 /* tab */

       /*
	* FIX:  we need to remember the tab stops
	*/
       
          thisdevice->pos = thisdevice->pos + 5;
	  if (thisdevice->pos > thisdevice->width) {
	       	  thisdevice->pos = thisdevice->pos - thisdevice->width;
		  kprintf (KR_CONSOLEKEY, "%c%c", CR, LF);
	  }
	  kprintf (KR_CONSOLEKEY, "\033[%dC", 5);
	  break;
				 /* ADD: is there a kill to end of line? */

     };

}
		   
int
ProcessRequest(Message *msg)
{
     /*
      * start key has been invoked; read/write/wakeup/change status
      * depending on the orderCode received
      *
      */

     uint32_t rdbytes;			/* how many bytes to read */
     uint32_t rcvlen;			/* copy of msg->rcv_len */

     
#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "line disc: start: got a new request OC %x rcvlen %x\n",
	       msg->rcv_code, msg->rcv.len);
#endif

     rcvlen = msg->rcv_len;		/* copy rcvlen */
     msg->rcv_len = EROS_PAGE_SIZE;	/* re-initialize rcvlen */


     
     
     rdbytes = 0;			/* intialize so gcc doesn't complain */

     if ((msg->rcv_code != LD_WRITE) &&
	 (msg->rcv_code != LD_WRITEPERM))
       {
	 if (rcvlen > sizeof(rdbytes))
	   {
	     msg->snd_code = LD_ERR;
	     return 1;
	   }
	 bcopy (msg->rcv_data, &rdbytes, rcvlen); 
       }
     
     switch (msg->rcv_code) {


       /*
	* FIX: SETCOOKED and SETRAW may need to flush
	* the buffers before switching modes.
	*/
       
     case LD_SETCOOKED:
	  init_device(&console);
	  kdb_buf->mode = COOKED;	
	  msg->snd_code = RC_OK; 
	  return 1;

     case LD_SETRAW:
	  init_device(&console);
	  kdb_buf->mode = RAW;	
	  msg->snd_code = RC_OK;
	  return 1;

     case LD_WAKEUP:
	  if (ld_wakeup())
	       msg->snd_code = RC_OK;
	  else
	       msg->snd_code = LD_ERRUNK;
	  return 1;		      

     case LD_NOTIFY:
	  if (kdb_buf->full) {
	       notify_flag = 1;
	       msg->snd_code = RC_OK;
	  }
	  else {
	       notify_flag = 0;
	       msg->snd_code = LD_NOTFULL;
	  }
	  return 1;		      

     case LD_READ:

	  msg->snd_data = snd_buffer;

	  if (rdbytes > SIK_BUFSZ)
	  {
	       msg->snd_code = LD_ERRBUFSZ;
	       return 1;
	  }

	  /*
	   * This looks really sleazy, because it is.  If
	   * check_readable fails it sets the wakeup
	   * pointer and then we call it again to avoid
	   * a potential race condition with TTYin and
	   * the wakeup pointer.
	   *
	   * Guarantee from TTYin: when the wakeup pointer
	   * is checked, the "addtobuffer" action cooresponding
	   * to it has already occured.
	   */

	  if (kdb_buf->mode == RAW)
	    {
	      
	      if (check_readable(rdbytes) || check_readable(rdbytes))
		{
		  
		  if (!ld_wakeup())
		    {
		      msg->snd_code = LD_ERRUNK;
		      return 1;
		    }
		  
		  msg->snd_len = read_buf (rdbytes, snd_buffer);
		  msg->snd_code = RC_OK;
		}
	      else
		{
		  if (!stash_reader(msg->rcv_key3, rdbytes))
		    {
		      msg->snd_code = LD_ERRSTASH;
		      return 1;
		    }
		  
		  msg->snd_invKey = 0;
		}
	      return 1;
	    }
	  else
	    {			/* COOKED */

	      /*
	       * the logic for now is to stash
	       * any cooked request automatically
	       */
	      
	      if (!stash_reader(msg->rcv_key3, rdbytes))
		{
		  msg->snd_code = LD_ERRSTASH;
		  return 1;
		}
	      
	      msg->snd_invKey = 0;
	      return 1;


	    }
     case LD_WRITE:
     case LD_WRITEPERM:


       
     if (kdb_buf->mode == RAW)
     {

       Writen (rcvlen, msg->rcv_data);    
	  msg->snd_code = RC_OK;
	  return 1;
     }
     else				/* COOKED */
     {
	  uint32_t i, x, disp;
	  char *dataptr = msg->rcv_data;

	  i = 0;
	  while (i < rcvlen) {
	       disp = 0;
	       for (x = i; i < rcvlen; i++ ) {
		    if ((char) dataptr[i] <= (char) US) {
			 disp = 1;
			 break;
		    }
	       }
	       if (i) {
		    if (!console.suspend)
			 Writen (i, &dataptr[x]);
		    bcopy (&dataptr[x], &console.buf[console.pos], i);
		    console.pos += i;
		    if (console.pos > console.width)
			 console.pos = console.pos - console.width;
	       }
	       if (disp) {
		    discipline(&console, dataptr[i++]);
	       }
	  }

	  if (msg->rcv_code == LD_WRITEPERM)
	    {
	      console.start = console.pos;
	    }
	  
	  msg->snd_code = RC_OK;

	  return 1;	  
     }

     
     default:
	  msg->snd_code = LD_ERR;
	  return 1;
     };
}






void
main ()
{

     Message msg;

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "linedisc: starting...\n");
#endif
				/* why can't this be in a header file? */
     msg.snd_invKey = KR_ZERO;
     msg.snd_key0 = KR_ZERO;
     msg.snd_key1 = KR_ZERO;
     msg.snd_key2 = KR_ZERO;
     msg.snd_key3 = KR_ZERO;
     msg.snd_data = 0;
     msg.snd_len = 0;
     msg.snd_code = 0;
     msg.snd_w1 = 0;
     msg.snd_w2 = 0;
     msg.snd_w3 = 0;
     
     msg.rcv_key0 = KR_RK0;
     msg.rcv_key1 = KR_RK1;
     msg.rcv_key2 = KR_RK2;
     msg.rcv_key3 = KR_RETURN;
     msg.rcv_data = rcvData;
     msg.rcv_code = 0;
     msg.rcv_w1 = 0;
     msg.rcv_w2 = 0;
     msg.rcv_w3 = 0;

     init();
     init_device(&console);

     
     do {
	  msg.rcv_len = EROS_PAGE_SIZE;
	  RETURN(&msg);
	  msg.snd_invKey = KR_RETURN;
     } while ( ProcessRequest(&msg) );
}


