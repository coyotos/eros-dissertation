/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <domain/domdbg.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>


#define COOKED 0
#define RAW    1
#define SIK_BUFSZ (uint32_t) 256

/*
 * this is the shared data structure between the two domains
 */

struct shared {
  char buf[SIK_BUFSZ];          /* incoming keyboard buffer */
  uint32_t head;			/* buf ptr; kdb read thread mods this */
  uint32_t tail;			/* buf ptr; line disp thread mods this */
  uint16_t full;		/* flag set to 1 if head == tail && buf is full */
  uint32_t wakeup;			/* wakeup line disc at head==wakeup */
  uint16_t mode;		/* raw, cooked, etc. */
};

/*
 * FIX: what we want evenutually is support for multiple
 * devices -- what we would see here is a struct for
 * each device and pointers to their member functions:
 * put, control, etc.
 */

#define MAX_DEV_WIDTH 256

struct device {
  uint16_t width;
  uint16_t start;
  uint16_t pos;
  uint16_t suspend;
  char buf[MAX_DEV_WIDTH];		
};

#define SH_ADDR      0x30000	/* address of shared page; see in test.imgmap */
#define SH_SLOT      3		/* slot in segment for the shared page */
#define MAX_STASH    8		/* how many readers to stash? can't be more than 16*/
#define LD_READ       0		/* data == numchars */
#define LD_WRITE      1		/* data == chars */
#define LD_WRITEPERM  2		/* data == chars */

#define LD_SETCOOKED  3
#define LD_SETRAW     4
#define LD_WAKEUP     5		/* from ttyin; data == kdb_buf */
#define LD_NOTIFY     6		/* from ttyin; data == kdb_buf */

#define LD_ERR        1		/* request error */
#define LD_ERRUNK     2		/* unknown error */
#define LD_ERRSZ      4		/* error in size of request */
#define LD_ERRSTASH   5		/* too many stashed readers */
#define LD_ERRBUFSZ   6		/* request for more bytes than buffer can hold*/
#define LD_NOTFULL    7		/* reply to TTYin on ld_notify if buffer
				   isn't full */
#define KR_ZERO       0		/* ZERO number key is always 0 */

#define SUCCESS       1         /* generic internal funtion return code */
#define FAIL          0         /* generic internal funtion return code */

enum  {
    NUL = 0x00,
    SOH = 0x01,
    STX = 0x02,
    ETX = 0x03,
    EOT = 0x04,
    ENQ = 0x05,
    ACK = 0x06,
    BEL = 0x07,
    BS  = 0x08,
    TAB = 0x09,
    LF  = 0x0a,
    VT  = 0x0b,
    FF  = 0x0c,
    CR  = 0x0d,
    SO  = 0x0e,
    SI  = 0x0f,
    DLE = 0x10,
    DC1 = 0x11,
    DC2 = 0x12,
    DC3 = 0x13,
    DC4 = 0x14,
    NAK = 0x15,
    SYN = 0x16,
    ETB = 0x17,
    CAN = 0x18,
    EM  = 0x19,
    SUB = 0x1a,
    ESC = 0x1b,
    FS  = 0x1c,
    GS  = 0x1d,
    RS  = 0x1e,
    US  = 0x1f,
    DEL = 0x7f
  };


