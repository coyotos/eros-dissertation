/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __UTIL_H__
#define __UTIL_H__

#ifndef YIELD
#define YIELD() __yield()
void __yield(void);  /* must be defined in each program */
#endif

uint32_t
write_data_to_buffer(register struct write_buff *buffer,
		     uint32_t proc, const char *data, uint32_t length);
/* Writes the first /length/ bytes of /data/ into /buffer/ atomically
 * equivilent to:
 *
 * if (!reserve_space(...)) return 0;
 * write to buffer with /proc/ processing
 * while (!FINISH_WRITING(...)) YIELD();
 * return 1; 
 */

#endif /* __UTIL_H__ */
