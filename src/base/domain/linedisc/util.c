/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <eros/target.h>
#include <domain/domdbg.h>

#include "linedisc.h"
#include "writebuff.h"

/* This *ONLY* handles CR->NL and NL->CR handling.  It works in
   concert with try_write in linedisc_buddy.c to do all of the output
   processing. */

uint32_t
write_data_to_buffer(register struct write_buff *buffer,
		     uint32_t proc, const char *data, uint32_t length)
{
  uint32_t resPos;

  if (!length) return 1; /* success! (okay, I cheated.  Sue me.)*/
  /* length is now the correct length. Reserve it */
  
  if (!reserve_space(&buffer->ctl,length,&resPos)) {
    /* not enough space */
    DEBUG(writebuff)
      kprintf(KR_OSTREAM,
	      "reserve_space failed.\n");
      
    return 0;
  } else {
    uint32_t len = length;
    /* resPos & WRITE_BUFFSIZE is the location to write to */

    register char *pos = buffer->writebuffer + (resPos & WRITE_BMASK);
    register char *buffend = buffer->writebuffer + WRITE_BUFFSIZE;

    /* copy it */
    if (proc & LD_Out_DoOutpProc) {
      while (len--) {
	char c = *data++;
	if (c == '\r' && (proc & LD_Out_CRtoNL))
	  c = '\n';
	else if (c == '\n' && (proc & LD_Out_NLtoCR))
	  c = '\r';
	*pos++ = c;
	if (pos == buffend) pos = buffer->writebuffer;
      }
    } else {
      while (len--) {
	*pos++ = *data++;
	if (pos == buffend) pos = buffer->writebuffer;
      }
    }
    /* mark "written" */
    while (!FINISH_WRITING(&buffer->ctl,length,resPos)) {
      YIELD();
    }
    return 1; /* success! */
  }  
}
