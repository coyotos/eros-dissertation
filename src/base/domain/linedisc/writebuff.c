/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <eros/target.h>

#include <eros/Invoke.h>

#include "debug.h"

#include "writebuff.h"
#include "cmpxchg.h"
#include "assert.h"
#include "util.h"

#include <domain/domdbg.h>

void
init_write_buff_ctl(register struct write_buff_ctl *toInit)
{
  toInit->top = WRITE_BUFFSIZE;
  toInit->reserved = 0u;
  toInit->written = 0u;
}

uint32_t
reserve_space(register struct write_buff_ctl *ctl,
	      uint32_t numChars,
	      uint32_t *reservedPos)
{
  if (!reservedPos) return 0;
  
  while (1) { /* loop until success or failure */
    register uint32_t oldPos = ctl->reserved;
    register uint32_t max    = ctl->top;

    if (((max - oldPos - 1) & WRITE_BMASK) < numChars) {
      DEBUG(writebuff)
	kprintf(KR_OSTREAM,
		"writebuff: reserve_space failed "
		"(max %08x, oldPos %08x, numChars %08x)\n",
		max, oldPos, numChars);
      return 0;
    }

    DEBUG(writebuff)
      kprintf(KR_OSTREAM,
	      "writebuff: reserve_space before CMPXCHG\n"
	      "(reserved %08x, oldPos %08x, numChars %08x)\n",
	      ctl->reserved, oldPos, numChars);
    
    /* try to add numChars to it. */
    if (CMPXCHG(&ctl->reserved, oldPos, oldPos + numChars)) {
      /* success */
      DEBUG(writebuff)
	kprintf(KR_OSTREAM,
		"writebuff: reserve_space succeeded "
		"(reserved %08x, returned %08x, top %08x, numChars %08x)\n",
		ctl->reserved, oldPos, ctl->top, numChars);
      
      *reservedPos = oldPos;
      return 1;
    } else {
      DEBUG(writebuff)
	kprintf(KR_OSTREAM,
		"writebuff: cmpxchg failed "
		"(reserved %08x, returned %08x, top %08x, numChars %08x)\n",
		ctl->reserved, oldPos, ctl->top, numChars);
      
      /* changed underneath us -- try again */
      continue;
    }
  }
  /* NOTREACHED */
}

uint32_t
get_chars_to_remove(register struct write_buff *buffer,
		    const char **outPos,
		    uint32_t *num)
{
  uint32_t top     = (buffer->ctl.top & WRITE_BMASK);
  uint32_t written = (buffer->ctl.written & WRITE_BMASK);

  assert(outPos && num);
    
  if (top == written) {
    return 0;
  } else {
    *outPos = buffer->writebuffer + top;
    if (top < written) {
      *num = written - top;
    } else {
      *num = WRITE_BUFFSIZE - top;
    }
    return 1;
  }
}
