/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/* CMPX*/

#if 0

uint32_t
__cmpxchg(volatile uint32_t *addr, uint32_t oldVal, uint32_t newVal);

#define CMPXCHG(addr, oldVal, newVal) __cmpxchg((addr),(oldVal),(newVal))

#else

#include <eros/machine/atomic.h>
#define CMPXCHG(addr, oldVal, newVal) ATOMIC_SWAP32(addr, oldVal, newVal)

#endif
     
/* performs:
 * if (*addr == oldVal) {
 *    *addr = newVal;
 *    return 1;
 * } else {
 *    return 0;
 * }
 *
 * Guarentees that no other process using CMPXCHG can interrupt and
 * change *addr to a different value if this successfuly completes.
 *
 * Usually used in code like the following:
 * (this is a non-circular buffer reservation routine)
 *
 * uint32_t value; // holds the returned value -- the old *ptr
 * uint32_t *ptr;  // holds the "max reserved" pointer
 * uint32_t asked; // how many chars to reserve
 *
 * while() {  // loop until successful
 *   uint32_t new;
 *   value = *ptr;         // store the old value
 *   uint32_t max = *maxptr;   // get the current "max available"
 *   if (max - value < asked) { // Check if /asked/ bytes are avail.
 *     return 0;                // if not, return failure
 *   }
 *   
 *   new = value + asked;       // compute the new max reserved
 *
 *   if (CMPXCHG(ptr,result,value) == 0)) { // try to write it in
 *      break; // success -- value is now correct
 *   } else {
 *      YIELD();  // failed -- yield to some other process and try again
 *      continue;
 *   }
 * } 
 *
 * As long as everyone modifying or testing *ptr uses code like this, 
 * you are guarenteed to have correctly computed off of
 * *ptr. 
 *
 * Returns non-zero on success. (the value has been changed)
 */
