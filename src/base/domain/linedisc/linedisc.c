/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/ReturnerKey.h>
#include <eros/ProcessKey.h>
#include <eros/NodeKey.h>
#include <eros/SleepKey.h>
#include <eros/Key.h>
#include <eros/StdKeyType.h>

#include "linedisc.h"
#include "cmpxchg.h"

#include <domain/SpaceBankKey.h>
#include <domain/ConstructorKey.h>
#include <domain/ProtoSpace.h>

#include <domain/domdbg.h>

#define KR_ZERO      0
#define KR_CONSTIT   1
#define KR_SELF      2
#define KR_DOMCRE    3
#define KR_BANK      4
#define KR_SCHED     5
#define KR_RETURNER  6
#define KR_OSTREAM   7  /* DEBUGGING */
#define KR_BUDDOM    8  /* for shoving the return key into */
#define KR_CHRSRC    9
#define KR_SCRATCH  10
#define KR_SCRATCH2 11
#if EROS_NODE_SIZE == 16
#define KR_ARG0     12  /* at startup, holds charsrc key */
#define KR_ARG1     13
#define KR_ARG2     14
#define KR_RESUME   15 /* DON'T USE IT FOR ANYTHING ELSE! */
#elif EROS_NODE_SIZE == 32
#define KR_ARG0     28  /* at startup, holds charsrc key */
#define KR_ARG1     29
#define KR_ARG2     30
#define KR_RESUME   31 /* DON'T USE IT FOR ANYTHING ELSE! */
#endif

#define KC_RETURNER 1
#define KC_OSTREAM  2  /* DEBUGGING */
#define KC_BUDDYC   3  /* buddy constructor */
#define KC_PROTO    4
#define KC_SLEEP    5

/* setup stack */
const uint32_t __rt_stack_pages = 0x1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif

const uint32_t defInputFlags = LD_In_DoInpProc | LD_In_Echo;
/*
                           (LD_In_DoInpProc
			    | LD_In_LineMode
			    | LD_In_Echo
			    | LD_In_EchoCtrl
			    | LD_In_VisErase
			    | LD_In_VisKill
			    | LD_In_SwapCRNL
			   );
 */
const uint32_t defOutputFlags = (LD_Out_DoOutpProc
			     | LD_Out_NLtoCRNL
			     | LD_Out_CRtoNL
			    );

/* make sure this matches the positions in LineDiscKey.h */
const uint16_t defCtrlChars[LD_Num_CC] = {
  LD_CC_EraseDef,
  LD_CC_AltEraseDef,
  LD_CC_EraseWordDef,
  LD_CC_KillLineDef,
  LD_CC_ReprintDef,
  LD_CC_QuoteDef,
  LD_CC_SFStopDef,
  LD_CC_SFStartDef
};

/* this is the under-routine for YIELD, defined in util.h */
void __yield(void)  /* SMASHES KR_SCRATCH2 */
{
  node_copy(KR_CONSTIT,KC_SLEEP,KR_SCRATCH2);
  sl_sleep(KR_SCRATCH2,0); /* sleep for 0 ms -- yields the processor */
}

void init_shared(void)
{
  register uint32_t idx;

  assert(sizeof(struct shared) <= EROS_PAGE_SIZE);
  
  init_write_buff_ctl(&SHARED.wrt_buff.ctl);

  SHARED.readerWants = UINT32_MAX;
  SHARED.inpProcFlags = defInputFlags;
  SHARED.outpProcFlags = defOutputFlags;

  for (idx = 0; idx < LD_Num_CC; idx++) {
    SHARED.control_chars[idx] = defCtrlChars[idx];
  }

  CLEAR_MASK(SHARED.char_mask);

  SHARED.linedisc_status = LD_Stat_Ready;
}

uint32_t start_buddy(uint32_t krSharedPage)
{
  Message msg;
  register uint32_t retval;
  
  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "LineDisc: Calling buddy constructor\n");

  /* make sure we won't smash the shared page */
  assert(krSharedPage != KR_BUDDOM);
  
  node_copy(KR_CONSTIT, KC_BUDDYC, KR_BUDDOM);
  retval = constructor_request(KR_BUDDOM,
			       KR_BANK,
			       KR_SCHED,
			       KR_CHRSRC, /* pass over the char source */
			       KR_BUDDOM);
  assert("Constructing buddy" && retval == RC_OK);

  /* KR_SCRATCH now holds a START key to our buddy.
     We CALL him back to trade the shared page for his domain key. */

  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "LineDisc: Calling buddy with shared page, expecting a domkey\n");

  msg.snd_key0 = krSharedPage;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_w1 = 0u;
  msg.snd_w2 = 0u;
  msg.snd_w3 = 0u;
  msg.snd_len = 0u; /* no data */

  msg.rcv_key0 = KR_BUDDOM; /* he'll return a domain key */
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;
  msg.rcv_len = 0u; /* no data */
  
  msg.snd_invKey = KR_BUDDOM;
  msg.snd_code = RC_OK; /* We're doing fine */

  retval = CALL(&msg);
  assert("starting buddy" && retval == RC_OK);
  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "LineDisc: Buddy CALLED me -- success!\n");
  
  return RC_OK;
}

uint32_t wakeup_buddy(enum LD_WakeupReasons reasons)
{
  charsrc_post_event(KR_CHRSRC,
		     reasons | CharSrc_UserEvent(0));

  return RC_OK;
}

void destroy_self(uint32_t retCode, bool buddyThere)
{
  register uint32_t retval;
  
  if (buddyThere) {
    /* tell my pal to jump off the bridge first */
    DEBUG(destroy)
      kprintf(KR_OSTREAM,
	      "LineDisc: Telling my buddy to suicide\n");
    
    SHARED.linedisc_status = LD_Stat_TearDown;
    wakeup_buddy(LD_Wakeup_TearDown); 

    /* wait for him to acknowledge */
    while (SHARED.linedisc_status == LD_Stat_TearDown)
      YIELD(); 

    /* he's gone now */
  }

  
  /* get my address space into scratch */
  retval = process_copy(KR_SELF, ProcAddrSpace, KR_SCRATCH);
  assert(retval == RC_OK);

  DEBUG(destroy)
    kprintf(KR_OSTREAM,
	    "LineDisc: destroying databuff page\n");

  /* get data page into scratch2 */
  retval = node_copy(KR_SCRATCH, DATABUFF_SLOT, KR_SCRATCH2);
  assert(retval == RC_OK);

  if (key_kt(KR_SCRATCH2) == AKT_DataPage) {
    /* sell the page back to the space bank */
    retval = spcbank_return_data_page(KR_BANK, KR_SCRATCH2);
    if (retval != RC_OK) {
      kdprintf(KR_OSTREAM,"Returning databuff page to spacebank failed!\n");
    }
  } else {
    DEBUG(destroy)
      kprintf(KR_OSTREAM,
	      "LineDisc: Databuff not page\n");
  }
  
  DEBUG(destroy)
    kprintf(KR_OSTREAM,
	    "LineDisc: destroying shared page\n");

  /* get shared page into scratch2 */
  retval = node_copy(KR_SCRATCH, SHARED_SLOT, KR_SCRATCH2);
  assert(retval == RC_OK);

  if (key_kt(KR_SCRATCH2) == AKT_DataPage) {
    /* sell the page back to the space bank */
    retval = spcbank_return_data_page(KR_BANK, KR_SCRATCH2);
    if (retval != RC_OK) {
      kdprintf(KR_OSTREAM,"Returning shared page to spacebank failed!\n");
    }
  } else {
    DEBUG(destroy)
      kprintf(KR_OSTREAM,
	      "LineDisc: Shared not page\n");
  }
  
  DEBUG(destroy)
    kdprintf(KR_OSTREAM,
	    "LineDisc: Suiciding\n");
  
  /* all of my stuff is gone, my buddy's gone -- better kill myself */
  node_copy(KR_CONSTIT,KC_PROTO,KR_SCRATCH);

  /* FIXME: Due to Intel's broken architecture, I can't return anything but
     RC_OK to the caller.  If retCode != RC_OK, what should I do? */
  
  protospace_destroy(KR_RETURNER,
		     KR_SCRATCH,
		     KR_SELF,
		     KR_DOMCRE,
		     KR_BANK,
		     1); /* small space */
  /* NOTREACHED */
}

#ifndef NDEBUG
int __assert(const char *expr, const char *file, int line)
{
  kdprintf(KR_OSTREAM, "%s:%d: Assertion failed: '%s'\n",
	   file, line, expr);
  return 0;
}
#endif

void
initialize_linedisc(void)
{
  register uint32_t retval;

  /* copy stuff out of constit */
  node_copy(KR_CONSTIT, KC_RETURNER, KR_RETURNER);
  node_copy(KR_CONSTIT, KC_OSTREAM,  KR_OSTREAM);

  /* copy the character source key into its key reg. (I could use
     KR_SELF, but the Returner key is much cooler) */
  copy_key_reg(KR_RETURNER, KR_ARG0, KR_CHRSRC);

  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "LineDisc: Returner worked -- buying shared page\n");
  
  retval = process_copy(KR_SELF, ProcAddrSpace, KR_SCRATCH);
  assert("process_copy(ProcAddrSpace)" && retval == RC_OK);

  /* buy a new page and insert it into address space for the databuff*/
  retval = spcbank_buy_data_pages(KR_BANK, 1, KR_SCRATCH2, 0, 0);
  assert("spcbank_buy_data_pages(1)" && retval == RC_OK);

  /* swap the new node in, putting the old contents over the address node */
  retval = node_swap(KR_SCRATCH, DATABUFF_SLOT, KR_SCRATCH2, KR_SCRATCH);
  assert("node_swap(AddrSpc,DATABUFF_SLOT,newPage)" && retval == RC_OK);

  DEBUG(init) {
    kprintf(KR_OSTREAM,
	    "LineDisc: Databuff page installed -- checking "
	    "previous occupant.\n");
    if (key_kt(KR_SCRATCH) != AKT_Number) {
      kdprintf(KR_OSTREAM,
	      "Hey! the previous occupant wasn't a number!\n");
    }
    kprintf(KR_OSTREAM,
	    "LineDisc: Checking that databuff is actually there.\n");
    assert(DATABUFF[0] == 0);
    DATABUFF[0] = 1;
    assert(DATABUFF[0] != 0);
    DATABUFF[0] = 0;

    kprintf(KR_OSTREAM,
	    "LineDisc: Databuff there. \n");
    
  }
  
  retval = process_copy(KR_SELF, ProcAddrSpace, KR_SCRATCH);
  assert("process_copy(ProcAddrSpace)" && retval == RC_OK);

  /* buy the page for the shared data */
  retval = spcbank_buy_data_pages(KR_BANK, 1, KR_SCRATCH2, 0, 0);
  assert("spcbank_buy_data_pages(1)" && retval == RC_OK);

  /* swap the new node in, putting the old contents over the address
     node */
  retval = node_swap(KR_SCRATCH, SHARED_SLOT, KR_SCRATCH2, KR_SCRATCH);
  assert("node_swap(AddrSpc,SHARED_SLOT,newPage)" && retval == RC_OK);

  DEBUG(init) {
    kprintf(KR_OSTREAM,
	    "LineDisc: Shared page installed -- checking "
	    "previous occupant.\n");
    if (key_kt(KR_SCRATCH) != AKT_Number) {
      kdprintf(KR_OSTREAM,
	      "Hey! the previous occupant wasn't a number!\n");
    }
  }

  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "Initializing shared data\n");
  
  init_shared();

  DEBUG(init)
    kprintf(KR_OSTREAM,
	    "LineDisc: Successfully initialized shared page\n");

  /* initialize buddy */

  retval = start_buddy(KR_SCRATCH2);
  assert("starting buddy" && retval == RC_OK);
  
  /* now get a start key so we can rock-and-roll */
  retval = process_make_start_key(KR_SELF, 0u, KR_ARG0);
  assert("Making start key" && retval == RC_OK);
  
  /* done intializing */
  return;
}

void
out_of_write_space(void)
{
  /* this should be called whenever we run out of write space */
  for( ; ; ) {
    if (CMPXCHG(&SHARED.out_of_write_space, 
		SHARED.out_of_write_space,
		1))
      break;
  }
}

uint32_t
doWrite(const char *data, uint32_t len, uint32_t *outLen)
{
  if (len == 0) {
    if (outLen) *outLen = 0;
    return RC_OK;
  }
  while(1) {
    register uint32_t avail = WBUFF_AVAIL_FOR_RESERVE(&SHARED.wrt_buff);

    if (!avail) {
      /* no space at the inn -- return failure */
      DEBUG(write)
	kprintf(KR_OSTREAM,
		"LineDisc: doWrite -- no space available\n");

      out_of_write_space();

      if (outLen) *outLen = 0;
      return RC_OK;
    } else {
      uint32_t retval;

      DEBUG(write)
	kprintf(KR_OSTREAM,
		"LineDisc: doWrite: %d avail\n",
		avail);
    
      
      if (len < avail) avail = len;
      
      DEBUG(write)
	kprintf(KR_OSTREAM,
		"LineDisc: doWrite: writing %d characters\n",
		avail);
    
      
      retval = write_data_to_buffer(&SHARED.wrt_buff,
				    SHARED.outpProcFlags,
				    data,
				    avail);
      if (retval) {
	
	DEBUG(write)
	  kprintf(KR_OSTREAM,
		  "LineDisc: doWrite: successful!\n");

	if (avail != len) {
	  /* we couldn't write everything */
	  out_of_write_space();
	}
	if (outLen) *outLen = avail;
	break;
      }
    }
    DEBUG(write)
      kprintf(KR_OSTREAM,
	      "LineDisc: doWrite unsuccessful. retrying.\n");
    
    /* failed -- loop around again */
  }
  return RC_OK;
}

uint32_t
ProcessRequest(Message *msg)
{

  msg->snd_len = 0;

  msg->snd_w1 = 0u;
  msg->snd_w2 = 0u;
  msg->snd_w3 = 0u;

  msg->snd_key0 = KR_ZERO;
  msg->snd_key1 = KR_ZERO;
  msg->snd_key2 = KR_ZERO;
  msg->snd_key3 = KR_ZERO;
  
  switch(msg->rcv_code) {
  case OC_CharSrc_Write:
    DEBUG(write)
      kprintf(KR_OSTREAM,
	      "LineDisc: got a write request (len = %08x)\n",
	      msg->rcv_len);
    doWrite(msg->rcv_data,
	    msg->rcv_len,
	    &msg->snd_w1);
    wakeup_buddy(LD_Wakeup_WriteRequest);
    msg->snd_code = RC_OK;
    break;
  case OC_CharSrc_PostEvent:
    if ((msg->rcv_w1 & (~CharSrc_UserEventMask)) != 0
	|| msg->rcv_w1 == 0u) {
      /* invalid request -- either we got a non-user event, or the
	 event mask was zero. */
      msg->snd_code = RC_RequestError;
      break;
    }

    while (SHARED.postedEvents) {
      wakeup_buddy(LD_Wakeup_EventPosted);
      YIELD(); /* give him a chance to process the events */
    }
    SHARED.postedEvents = msg->rcv_w1;
    msg->snd_code = RC_OK;
    wakeup_buddy(LD_Wakeup_EventPosted);
    break;
    
  case OC_CharSrc_WaitForEvent:

    if (SHARED.readerWants != UINT32_MAX) {
      DEBUG(waitevent)
	kdprintf(KR_OSTREAM, "LineDisc: Got too many waitforevents!\n");
      msg->snd_code = RC_CharSrc_OneWaiterOnly;
      break;
    }
    DEBUG(waitevent)
      kprintf(KR_OSTREAM,
	      "LineDisc: Got waitforevent -- stashing count %u mask 0x%08x\n",
	      msg->rcv_w1, msg->rcv_w2);
    /* squirrel away key in child */
    process_swap_keyreg(KR_BUDDOM,K_READERKEY,KR_RESUME,KR_ZERO);
    /* don't invoke it on the way out */
    msg->snd_invKey = KR_ZERO;
    
    if (msg->rcv_w1 > DATABUFF_LEN) msg->rcv_w1 = DATABUFF_LEN;
    
    SHARED.readerEventMask = msg->rcv_w2;
    SHARED.readerWants = msg->rcv_w1;
    
    DEBUG(waitevent)
      kdprintf(KR_OSTREAM, "LineDisc: Stashed -- waking buddy\n");
    wakeup_buddy(LD_Wakeup_GotReader);

    break;

  case OC_CharSrc_Control:

    DEBUG(control)
      kdprintf(KR_OSTREAM,"Linedisc: Control %d, %d\n",msg->snd_w1,msg->snd_w2);

    switch (msg->rcv_w1) {
    case LD_Control_GetInpProcessing:
      msg->snd_w1 = SHARED.inpProcFlags;
      msg->snd_code = RC_OK;
      break;
    case LD_Control_SetInpProcessing:
      /* FIXME: check if the flags are valid */
      SHARED.inpProcFlags = msg->rcv_w2;
      wakeup_buddy(LD_Wakeup_ProcFlags);
      msg->snd_code = RC_OK;
      break;

    case LD_Control_GetOutpProcessing:
      msg->snd_w1 = SHARED.outpProcFlags;
      msg->snd_code = RC_OK;
      break;
    case LD_Control_SetOutpProcessing:
      /* FIXME: check if the flags are valid */
      SHARED.outpProcFlags = msg->rcv_w2;
      wakeup_buddy(LD_Wakeup_ProcFlags);
      msg->snd_code = RC_OK;
      break;

    case LD_Control_GetControlChars:
      msg->snd_len = sizeof(SHARED.control_chars[0]) * LD_Num_CC;
      msg->snd_data = (void *)SHARED.control_chars;
      msg->snd_code = RC_OK;
      break;
    case LD_Control_SetControlChars:
      {
	if (msg->rcv_len == sizeof(SHARED.control_chars)) {
	  int idx;
	  cc_type *in = msg->rcv_data;
	  volatile cc_type *out = SHARED.control_chars;
	  
	  for (idx = LD_Num_CC; idx > 0; idx--) {
	    *in++ = *out++;
	  }
	  wakeup_buddy(LD_Wakeup_Chars);

	  msg->snd_code = RC_OK;
	} else {
	  /* incorrectly sized message */
	  msg->snd_code = RC_RequestError;
	}
      }
      break;

#if 0
    case CharSrc_Control_SetTimeout:
    case CharSrc_Control_GetTimeout:
      
      goto def; /* pass on the request */
#endif

    case CharSrc_Control_GetSpecialChars:
      {
	register int idx;
	char *buffp = DATABUFF;
	int len = 0;

	for (idx = 0; idx < 256; idx++) { /* byte-centric! */
	  if (CHECK_CHAR(SHARED.char_mask,idx)) {
	    *buffp++ = 0;
	    *buffp++ = idx;
	    len += 2;
	  }
	}
	msg->snd_data = DATABUFF;
	msg->snd_len = len;
	msg->snd_code = RC_OK;
      }
      break;

    case CharSrc_Control_SetSpecialChars:
      {
	register int idx;
	register int len = msg->rcv_len;
	uint16_t *curp = (uint16_t *)DATABUFF; 

        DEBUG(control)
          kdprintf(KR_OSTREAM,
                   "LineDisc: Setting %d/2 special characters\n",len);
	
        DEBUG(control)
          kprintf(KR_OSTREAM,
                  "LineDisc: Setting special characters");

	if (len % 2 != 0) {
	  /* invalid request length */
          DEBUG(control)
            kdprintf(KR_OSTREAM,
                     " FAILED: not even number of chars\n");

	  msg->snd_code = RC_RequestError;
	}
        DEBUG(control)
          kprintf(KR_OSTREAM,":");

	/* first, set up character mask */
	CLEAR_MASK(SHARED.char_mask);
	for (idx = (len / sizeof(*curp)) ; idx > 0; idx--) {
	  if ((*curp & ~0xFF) != 0) {
	    /* unicode -- throw it away for now */
            DEBUG(control)
	      kdprintf(KR_OSTREAM,
	  	       "\nLineDisc: Hey! non-ASCII unicode stuff in"
	 	       "SetSpecialChars! (0x%04x)\n",*curp);
	    curp++;
	    continue;
	  }
          DEBUG(control)
            kprintf(KR_OSTREAM," 0x%02x",*curp);

	  SET_CHAR(SHARED.char_mask,(uint8_t)*(curp++));
	}
        DEBUG(control)
          kdprintf(KR_OSTREAM,
	           ". Done\n");

	/* now the character mask is set up -- it's my buddy's job
	   from here */
	wakeup_buddy(LD_Wakeup_Chars);
	msg->snd_code = RC_OK;
	break;
      }
    default: 
      DEBUG(control)
        kdprintf(KR_OSTREAM,
                 "Linedisc: Passing control along to KR_CHRSRC\n");

      msg->snd_code = OC_CharSrc_Control;
      msg->snd_w1 = msg->rcv_w1;
      msg->snd_w2 = msg->rcv_w2;
      msg->snd_w3 = msg->rcv_w3;
      
      msg->snd_key0 = msg->rcv_key0;
      msg->snd_key1 = msg->rcv_key1;
      msg->snd_key2 = msg->rcv_key2;
      msg->snd_key3 = msg->rcv_key3; /* pass along the resume key */

      msg->snd_data = msg->rcv_data;
      msg->snd_len = msg->rcv_len;
      
      msg->snd_invKey = KR_CHRSRC; /* pass it along to the charsrc */
      break;
    }
    break;
/*
 * case KT:
 *   msg->snd_code = AKT_LineDiscipline;
 *   break;
 */
  case OC_Destroy:
    DEBUG(destroy)
      kdprintf(KR_OSTREAM,
	       "LineDisc: Destroying self!\n");
    
    destroy_self(RC_OK, 1); /* destroy, with buddy there */
    /* NOT REACHED */
    break;
  default:
    msg->snd_code = RC_UnknownRequest;
    break;
  }
  return 1;
}

int
main(void)
{
  Message msg;
  
  initialize_linedisc(); /* either successful or destroys me. */
 
  msg.snd_invKey = KR_RESUME;
  msg.snd_key0 = KR_ARG0;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = KR_ARG0;
  msg.rcv_key1 = KR_ARG1;
  msg.rcv_key2 = KR_ARG2;
  msg.rcv_key3 = KR_RESUME;
  msg.rcv_data = DATABUFF;
  
  msg.rcv_code = RC_OK;

  do {
    msg.rcv_len = DATABUFF_LEN;

    RETURN(&msg);

    msg.snd_invKey = KR_RESUME;
    
  } while ( ProcessRequest(&msg) );

  return 0;
}
