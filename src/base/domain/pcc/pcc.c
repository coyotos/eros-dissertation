/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* DCC: Domain Creator Creator

   This is one of the primordial domains; it is not created by a
   factory.

   DCC creates all domain creators.
   */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/ProcessKey.h>
#include <eros/NumberKey.h>
#include <eros/ProcessState.h>
#include <eros/ReturnerKey.h>
#include <eros/Key.h>
#include <eros/ProcessToolKey.h>
#include <eros/StdKeyType.h>
#include <eros/machine/Registers.h>
#include <eros/KeyBitsKey.h>
#include <domain/SpaceBankKey.h>
#include <domain/PccKey.h>

#include <domain/domdbg.h>

#include "proccrekr.h"

#define KC_OSTREAM          0
#define KC_DOMTOOL          1
#define KC_DOMCRE_PC        2
#define KC_DOMCRE_SEG       3
#define KC_OURBRAND   	    4
#define KC_DOMCRE_CONSTIT   5

#define DEBUG if (0)
/* #define DEBUG if (1) */

typedef struct {
  uint32_t domcre_pc;
} domcre_info;


#define DCC_CONSTIT		1
#define DCC_DOM			2	/* dom key to this program */
#define DCC_TRUESB		4	/* prime space bank */
#define DCC_SCRATCH		5	/* scratch reg */
#define DCC_NEW_DOMCRE		6	/* scratch reg */
#define DCC_DOMCRE_CONSTIT	7	/* so dcc can tell us what's up */
#define DCC_OSTREAM		8	/* so dcc can tell us what's up */
#define DCC_DOMTOOL		9	/* domain tool */
#define DCC_DOMCRE_SEG		10	/* code seg for DomCre (RO) */
#define DCC_NEWKEYREGS		11	/* new key regs node */
#define DCC_OURBRAND		12	/* distinguished start key to DCC */
#if EROS_NODE_SIZE == 16
#define DCC_ARG0		13	/* space bank */
#define DCC_ARG1		14	/* schedule */
#define DCC_RSM			15	/* resume key */
#elif EROS_NODE_SIZE == 32
#define DCC_ARG0		29	/* space bank */
#define DCC_ARG1		30	/* schedule */
#define DCC_RSM			31	/* resume key */
#endif

#define FALSE 0
#define TRUE 1

const uint32_t __rt_stack_pages = 1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif

uint32_t create_new_domcre(uint32_t krBank, uint32_t krSched, uint32_t domKeyReg,
		       domcre_info *pInfo);
uint32_t create_new_domain(uint32_t krBank, uint32_t domKeyReg);
uint32_t identify_domcre(uint32_t krDomCre);

int
ProcessRequest(Message *argmsg, domcre_info *pInfo)
{
  uint32_t result = RC_OK;
  argmsg->snd_key0 = 0;		/* until proven otherwise */

  switch (argmsg->rcv_code) {
  case OC_PCC_CreateProcessCreator:
    {
      result = create_new_domcre(DCC_ARG0, DCC_ARG1, DCC_NEW_DOMCRE, pInfo);
      if (result == RC_OK)
	argmsg->snd_key0 = DCC_NEW_DOMCRE;
      break;
    }
    
  case OC_PCC_IdentifyProcCre:
    result = identify_domcre(DCC_ARG0);
    break;

  default:
    result = RC_UnknownRequest;
    break;
  };

  argmsg->snd_code = result;
  return 1;
}

void
init_dcc(domcre_info *pInfo)
{
  node_copy(DCC_CONSTIT, KC_DOMCRE_PC, DCC_SCRATCH);

  number_get_word(DCC_SCRATCH, &pInfo->domcre_pc);


  node_copy(DCC_CONSTIT, KC_OSTREAM, DCC_OSTREAM);
  node_copy(DCC_CONSTIT, KC_DOMTOOL, DCC_DOMTOOL);
  node_copy(DCC_CONSTIT, KC_DOMCRE_SEG, DCC_DOMCRE_SEG);
  node_copy(DCC_CONSTIT, KC_OURBRAND, DCC_OURBRAND);
  node_copy(DCC_CONSTIT, KC_DOMCRE_CONSTIT, DCC_DOMCRE_CONSTIT);

  DEBUG kdprintf(DCC_OSTREAM, "DCC Initialized...\n");
}

int
main()
{
  Message msg;
  domcre_info info;
  
  init_dcc(&info);
  
  msg.snd_invKey = KR_ZERO;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = DCC_ARG0;
  msg.rcv_key1 = DCC_ARG1;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ARG3;
  msg.rcv_data = 0;
  msg.rcv_len = 0;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  do {
    RETURN(&msg);
    msg.snd_invKey = KR_ARG3;
  } while ( ProcessRequest(&msg, &info) );

  return 0;
}


#ifdef TARGET_i486

int
destroy_domain(uint32_t krBank, uint32_t krDomKey)
{
  if (pt_canopener(DCC_DOMTOOL, krDomKey, DCC_OURBRAND, krDomKey) != RC_OK)
    return FALSE;

#ifdef ProcGenRegs
  (void) node_copy(krDomKey, ProcGenRegs, DCC_SCRATCH);
  (void) spcbank_return_node(krBank, DCC_SCRATCH);
#endif

  (void) node_copy(krDomKey, ProcGenKeys, DCC_NEWKEYREGS);
  (void) spcbank_return_node(krBank, DCC_NEWKEYREGS);

  (void) spcbank_return_node(krBank, krDomKey);
      
  DEBUG kdprintf(DCC_OSTREAM, "Sold domain back to bank\n");

  return TRUE;
}

uint32_t
create_new_domain(uint32_t krBank, uint32_t krDomKey)
{
  DEBUG kdprintf(DCC_OSTREAM, "Check official bank...\n");

  if (spcbank_verify_bank(DCC_TRUESB, krBank) != RC_OK)
    return RC_PCC_BadBank;
  
  DEBUG kdprintf(DCC_OSTREAM, "OK -- buy domain nodes\n");
#ifdef ProcGenRegs
  /* Bank is okay, try to buy the space: */
  if (spcbank_buy_nodes(krBank, 3, krDomKey, DCC_NEWKEYREGS,
			DCC_SCRATCH) != RC_OK)
    return RC_NoMoreNodes;
#else
  /* Bank is okay, try to buy the space: */
  if (spcbank_buy_nodes(krBank, 2, krDomKey, DCC_NEWKEYREGS,
			KR_ZERO) != RC_OK)
    return RC_NoMoreNodes;
#endif

  DEBUG kdprintf(DCC_OSTREAM, "Assemble them\n");
  /* We have the nodes.  Make the second the key registers node of the
     first: */
  (void) node_swap(krDomKey, ProcGenKeys, DCC_NEWKEYREGS, KR_ZERO);
#ifdef ProcGenRegs
  (void) node_swap(krDomKey, ProcGenRegs, DCC_SCRATCH, KR_ZERO);
#endif
  
  /* Now install the brand: */
  (void) node_swap(krDomKey, ProcBrand, DCC_OURBRAND, KR_ZERO);

  DEBUG kdprintf(DCC_OSTREAM, "Build domain key:\n");

  /* Now make a domain key of this: */
  (void) pt_make_process_key(DCC_DOMTOOL, krDomKey, krDomKey);

  DEBUG kdprintf(DCC_OSTREAM, "Return from create_new_domain()\n");

  return RC_OK;
}

uint32_t
create_new_domcre(uint32_t krBank, uint32_t krSched, uint32_t krDomKey,
		  domcre_info *pInfo)
{
  uint32_t result;
  struct Registers regs;

  DEBUG kdprintf(DCC_OSTREAM, "About to call create_new_domain()\n");
  result = create_new_domain(krBank, krDomKey);
  DEBUG kdprintf(DCC_OSTREAM, "Result = 0x%08x\n", result);

  if (result != RC_OK)
    return result;

#if 0
  /* Buy a node to hold the new segment root. */
  result = spcbank_buy_nodes(krBank, 1, DCC_NEWSEG, KR_ZERO, KR_ZERO);
  if ( result != RC_OK) {
    DEBUG kdprintf(DCC_OSTREAM, "buy node result is 0x%08x\n", result);
    /* Acquisition failed - return what we have and give up. */
    destroy_domain(krBank, krDomKey);
    return RC_PCC_NoSpace;
  }

  DEBUG kdprintf(DCC_OSTREAM, "Got new aspace node\n");

  /* Install into newseg slot 0 the basic domcre segment: */
  node_swap(DCC_NEWSEG, 0, DCC_DOMCRE_SEG, KR_ZERO);

  DEBUG kdprintf(DCC_OSTREAM, "Installed codeseg\n");

  /* Buy a page to hold the new domcre's stack. */
  result = spcbank_buy_pages(krBank, 1, DCC_NEWSTACK, KR_ZERO, KR_ZERO);
  if ( result != RC_OK) {
    DEBUG kdprintf(DCC_OSTREAM, "buy page result is 0x%08x\n", result);
    /* Acquisition failed - return what we have and give up. */
    spcbank_return_node(krBank, DCC_NEWSEG);
    destroy_domain(krBank, krDomKey);
    return RC_PCC_NoSpace;
  }

  DEBUG kdprintf(DCC_OSTREAM, "Got new page\n");

  /* Install into newseg slot 1 the stack page: */
  node_swap(DCC_NEWSEG, 1, DCC_NEWSTACK, KR_ZERO);

  DEBUG kdprintf(DCC_OSTREAM, "Installed it\n");

  /* WE WIN -- THE REST IS INITIALIZATION */
  
  /* Fetch a node key of the right BLSS for this address space: */
  (void) node_make_node_key(DCC_NEWSEG, 4 /* BLSS */, DCC_NEWSEG);

  DEBUG kdprintf(DCC_OSTREAM, "Made segtree key\n");

  /* Install this address space into the domain root: */
  (void) process_swap(krDomKey, ProcAddrSpace, DCC_NEWSEG, KR_ZERO);
#else
  (void) process_swap(krDomKey, ProcAddrSpace, DCC_DOMCRE_SEG, KR_ZERO);
#endif

  DEBUG kdprintf(DCC_OSTREAM, "Installed aspace\n");

  /* Install the schedule key into the domain: */
  (void) process_swap(krDomKey, ProcSched, krSched, KR_ZERO);
  
  DEBUG kdprintf(DCC_OSTREAM, "Installed sched\n");

  /* Fetch out the register values, mostly for the benefit of
     Retrieving the PC -- this prevents us from needing to hard-code
     the PC, which will inevitably change. */
  (void) process_get_regs(krDomKey, &regs);

  DEBUG kdprintf(DCC_OSTREAM, "Got regs\n");

  /* Unless we set them otherwise, the register values are zero.  The
     PC has already been set.  We now need to initialize the stack
     pointer and the segment registers. */
  regs.pc = pInfo->domcre_pc;
  regs.CS = DOMAIN_CODE_SEG;
  regs.SS = DOMAIN_DATA_SEG;
  regs.DS = DOMAIN_DATA_SEG;
  regs.ES = DOMAIN_DATA_SEG;
  regs.FS = DOMAIN_DATA_SEG;
  regs.GS = DOMAIN_DATA_SEG;
  regs.EFLAGS = 0x200;
  regs.faultCode = 0;
  regs.faultInfo = 0;
  regs.domState = RS_Waiting;
  regs.domFlags = 0;
  
  /* Set the new register values. */
  (void) process_set_regs(krDomKey, &regs);

  DEBUG kdprintf(DCC_OSTREAM, "Wrote regs\n");

  /* Populate the new domcre's key registers: */
  process_swap_keyreg(krDomKey, KR_CONSTIT, DCC_DOMCRE_CONSTIT, KR_ZERO);
  process_swap_keyreg(krDomKey, KR_BANK, DCC_TRUESB, KR_ZERO);
  process_swap_keyreg(krDomKey, KR_OURPROCESS, krDomKey, KR_ZERO);
  
  DEBUG kdprintf(DCC_OSTREAM, "About to call get fault key\n");
  /* Make a restart key to start up the new domain creator: */
  (void) process_make_fault_key(krDomKey, DCC_SCRATCH);

  {
    Message msg;
    msg.snd_key0 = KR_ZERO;
    msg.snd_key1 = KR_ZERO;
    msg.snd_key2 = KR_ZERO;
    msg.snd_key3 = KR_ZERO;
    msg.snd_code = 0;		/* ordinary restart */
    msg.snd_len = 0;
    msg.snd_invKey = DCC_SCRATCH;

    SEND(&msg);
  }

  DEBUG kdprintf(DCC_OSTREAM, "About to call get start key\n");
  /* Now make a start key to return: */
  (void) process_make_start_key(krDomKey, 0, krDomKey);

  DEBUG kdprintf(DCC_OSTREAM, "Got start key\n");
  return RC_OK;
}
#endif

int
is_our_progeny(uint32_t krStart, uint32_t krNode)
{
  uint32_t result = pt_canopener(DCC_DOMTOOL, krStart, DCC_OURBRAND, krNode);
  
  if (result != (uint32_t) -1)
    return 1;
  return 0;
}

uint32_t make_start_key(uint32_t krDomain, uint16_t keyData, uint32_t krStart);

uint32_t spcbank_return_node(uint32_t krBank, uint32_t krNode);

uint32_t
identify_domcre(uint32_t krDomCre)
{
  return RC_UnknownRequest;
}
