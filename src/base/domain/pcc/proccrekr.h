/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* Key conventions for the domain creator.  These need to be known to
   both the domain creator and DCC.
   */

/* PROCESS CREATOR REGISTER CONVENTIONS */

#define KR_ZERO       0
#define KR_CONSTIT    1		/* where the 1st created key (if any) goes */
#define KR_OURPROCESS 2		/* Our domain key. */

#define KR_OSTREAM    3
#define KR_BANK       4		/* Bank from which we were built. */
#define KR_SCRATCH0   5		/* from the space bank */
#define KR_SCRATCH1   6
#define KR_SCRATCH2   7		/* Where we put the components we buy */
#define KR_OUTKEY0    7

#define KR_RETURNER   8		/* Returner key */
#define KR_OURBRAND   9		/* A 65536 start key to us */
#define KR_OUTKEY1   10		/* where the 2nd created key (if any) goes */
#define KR_PROCTOOL   11	/* The process tool. */

#if EROS_NODE_SIZE == 16
#define KR_ARG0    12
#define KR_ARG1    13
#define KR_ARG2    14
#define KR_ARG3    15
#define KR_RESUME  15
#elif EROS_NODE_SIZE == 32
#define KR_ARG0    28
#define KR_ARG1    29
#define KR_ARG2    30
#define KR_ARG3    31
#define KR_RESUME  31
#else
#error "Unhandled node size"
#endif

#define KR_RETURNEE KR_ARG1	/* if needed, returnee should be here. */
