/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Echo -- domain that simply echos what you send it.
 *
 * Key Registers:
 *
 * KR12: arg0
 * KR13: arg1
 * KR14: arg2
 * KR15: arg3
 */

#include <eros/target.h>
#include <eros/Invoke.h>

#if EROS_NODE_SIZE == 16
#define KR_RK0		12
#define KR_RK1		13
#define KR_RK2		14
#define KR_RESUME	15
#elif EROS_NODE_SIZE == 32
#define KR_RK0		28
#define KR_RK1		29
#define KR_RK2		30
#define KR_RESUME	31
#endif

const uint32_t __rt_stack_pointer = 0x10000;
const uint32_t __rt_stack_pages = 0x0;

int
ProcessRequest(Message *msg)
{
  msg->snd_len = msg->rcv_len;
  msg->snd_data = msg->rcv_data;

  msg->snd_key0 = msg->rcv_key0;
  msg->snd_key1 = msg->rcv_key1;
  msg->snd_key2 = msg->rcv_key2;
  msg->snd_key3 = msg->rcv_key3;

  /* leave receive info unchanged, but reset the length limit: */
  msg->rcv_len = EROS_PAGE_SIZE;
  msg->snd_invKey = msg->rcv_key3;
  
  msg->snd_code = msg->rcv_code;
  return 1;
}

int
main()
{
  Message msg;
  msg.snd_invKey = KR_ZERO;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = KR_RK0;
  msg.rcv_key1 = KR_RK1;
  msg.rcv_key2 = KR_RK2;
  msg.rcv_key3 = KR_RESUME;
  msg.rcv_data = (void *) 0x22000;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  do {
    msg.rcv_len = 4096;
    RETURN(&msg);
    msg.snd_invKey = msg.rcv_key3;
  } while ( ProcessRequest(&msg) );

  return 0;
}
