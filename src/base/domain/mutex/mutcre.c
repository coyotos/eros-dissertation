/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* Mutex Creator Object

   This is a temporary placeholder until we manage to get factories
   working, and I'm going to have to build a test to demo how it
   works.
   */


/*
 * Key Registers:
 *
 * KR8:  domcre
 * KR9:  DCC (FOR NOW)
 * KR10: prime spc bank (FOR NOW)
 * KR11: sched key (FOR NOW)
 *
 * KR13: arg0  (spc bank)
 * KR14: arg1  (sched cap)
 * KR15: arg3  (resume cap)
 *
 * Initial stack pointer at 0x4096
 * Accepts no data
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/Key.h>
#include <eros/ProcessKey.h>
#include <eros/ProcessState.h>
#include <domain/PccKey.h>
#include <domain/ProcessCreatorKey.h>
#include <domain/domdbg.h>

const uint32_t __rt_stack_pages = 1;
const uint32_t __rt_stack_pointer = 0x10000;

#define KR_ZERO     0
#define KR_OSTREAM  1		/* for debugging */

#define KR_DOM		2	/* dom key to this program */
#define KR_SPCBANK      4	/* our space bank */

#define KR_FAULT     5		/* to init the new mutex */
#define KR_MUTEX_PC  6		/* mutex program segment */
#define KR_MUTEX_SEG 7		/* mutex program segment */

#define KR_DOMCRE   8
#define KR_DCC      9
#define KR_SCHED    11
#define KR_NEWDOM   12
#define KR_ARGBANK  13
#define KR_ARGSCHED 14
#define KR_RETURN   15

#define DEBUG if (0)
//#define DEBUG if (1)

int
ProcessRequest(Message *req)
{
  static uint32_t isInit = 0;
  struct Registers regs;

  req->snd_key1 = KR_ZERO;
  req->snd_key2 = KR_ZERO;
  req->snd_key3 = KR_ZERO;
  req->snd_len = KR_ZERO;
  req->snd_len = 0;

  if (isInit == 0) {
    Message msg;
    
    msg.snd_key0 = KR_SPCBANK;
    msg.snd_key1 = KR_SCHED;
    msg.snd_key2 = KR_ZERO;
    msg.snd_key3 = KR_ZERO;
    msg.snd_data = 0;
    msg.snd_len = 0;

    msg.rcv_key0 = KR_DOMCRE;
    msg.rcv_key1 = KR_ZERO;
    msg.rcv_key2 = KR_ZERO;
    msg.rcv_key3 = KR_ZERO;
    msg.rcv_len = 0;		/* no data returned */

    {
      uint32_t result;
      msg.snd_code = OC_PCC_CreateProcessCreator;
      msg.snd_invKey = KR_DCC;
      result = CALL(&msg);
      DEBUG kdprintf(KR_OSTREAM, "Newdomcre: Result is 0x%08x\n", result);
      if (result != RC_OK) {
	req->snd_code = result;
	return 1;
      }
    }
  }

  // Create a new domain:
  {
    Message msg;

    msg.snd_key0 = KR_ARGBANK;
    msg.snd_key1 = KR_ZERO;
    msg.snd_key2 = KR_ZERO;
    msg.snd_key3 = KR_ZERO;
    msg.snd_data = 0;
    msg.snd_len = 0;

    msg.rcv_key0 = KR_NEWDOM;
    msg.rcv_key1 = KR_ZERO;
    msg.rcv_key2 = KR_ZERO;
    msg.rcv_key3 = KR_ZERO;
    msg.rcv_len = 0;		/* no data returned */

    {
      uint32_t result;
      msg.snd_code = OC_ProcCre_CreateProcess;
      msg.snd_invKey = KR_DOMCRE;
      result = CALL(&msg);

      DEBUG kdprintf(KR_OSTREAM, "Newdom: result is 0x%08x\n", result);
      if (result != RC_OK) {
	req->snd_code = result;
	return 1;
      }
    }
  }
  
  /* Install address space into the domain root: */
  (void) process_swap(KR_NEWDOM, ProcAddrSpace, KR_MUTEX_SEG, KR_ZERO);
  /* Install sched cap into the domain root: */
  (void) process_swap(KR_NEWDOM, ProcSched, KR_ARGSCHED, KR_ZERO);
  (void) process_swap(KR_NEWDOM, ProcPCandSP, KR_MUTEX_PC, KR_ZERO);

  /* Unless we set them otherwise, the register values are zero.  The
     PC has already been set.  We now need to initialize the stack
     pointer and the segment registers.

     Fetch out the register values, mostly for the benefit of
     Retrieving the PC -- this prevents us from needing to hard-code
     the PC, which will inevitably change. */
  (void) process_get_regs(KR_NEWDOM, &regs);

  regs.sp = EROS_PAGE_SIZE;
  regs.CS = DOMAIN_CODE_SEG;
  regs.SS = DOMAIN_DATA_SEG;
  regs.DS = DOMAIN_DATA_SEG;
  regs.ES = DOMAIN_DATA_SEG;
  regs.FS = DOMAIN_DATA_SEG;
  regs.GS = DOMAIN_DATA_SEG;
  regs.EFLAGS = 0x0200;
  regs.domState = RS_Waiting;
  regs.domFlags = 0;
  
  /* Set the new register values. */
  (void) process_set_regs(KR_NEWDOM, &regs);

  DEBUG kdprintf(KR_OSTREAM, "About to call get fault key\n");
  /* Make a restart key to start up the new domain creator: */
  (void) process_make_fault_key(KR_NEWDOM, KR_FAULT);

  {
    Message msg;
    msg.snd_key0 = KR_ZERO;
    msg.snd_key1 = KR_ZERO;
    msg.snd_key2 = KR_ZERO;
    msg.snd_key3 = KR_ZERO;
    msg.snd_code = 0;		/* ordinary restart */
    msg.snd_len = 0;
    msg.snd_invKey = KR_FAULT;

    SEND(&msg);
  }

  /* Now make a start key to return: */
  (void) process_make_start_key(KR_NEWDOM, 0, KR_NEWDOM);

  req->snd_key0 = KR_NEWDOM;
  req->snd_code = RC_OK;
  return 1;
}

int
main()
{
  Message msg;
  msg.snd_invKey = KR_ZERO;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = KR_ARGBANK;
  msg.rcv_key1 = KR_ARGSCHED;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_RETURN;
  msg.rcv_data = 0;
  msg.rcv_len = 0;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  do {
    RETURN(&msg);
    msg.snd_invKey=KR_RETURN;
  } while ( ProcessRequest(&msg) );

  return 0;
}

