/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/machine/atomic.h>
#include <eros/ReturnerKey.h>
#include <eros/Invoke.h>

const uint32_t __rt_stack_pages = 1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#endif

#define KR_ZERO 0
#define MAX_SLEEPER 31

int
main()
{
  Message msg;

  int next_sleeper = 1;
  
  msg.snd_invKey = KR_ZERO;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = next_sleeper;
  msg.rcv_len = 0;
  msg.rcv_data = 0;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  for(;;) {
    int delta;
    
    msg.rcv_key3 = next_sleeper; /* where to put resume key */
    msg.rcv_len = 0; /* reset receive length */

    RETURN(&msg);

#ifdef USE_RETURNER
    msg.snd_invKey = KR_RETURNER;
    msg.snd_key3 = next_sleeper;
#else    
    msg.snd_invKey = next_sleeper;
#endif
    
    delta = (int) msg.rcv_code;

    if (delta < 1) {
      int i;
      Message m;

      m.snd_key0 = KR_ZERO;
      m.snd_key1 = KR_ZERO;
      m.snd_key2 = KR_ZERO;
      m.snd_key3 = KR_ZERO;
      m.snd_data = 0;
      m.snd_len = 0;
      m.snd_code = 0;
      m.snd_w1 = 0;
      m.snd_w2 = 0;
      m.snd_w3 = 0;

      for (i = 1; i < next_sleeper; i++) {
	msg.snd_invKey = i;
	SEND(&msg);
      }
      next_sleeper = 1;
    }
    else if (delta > 1)
      msg.snd_invKey = KR_ZERO;
  }
}

