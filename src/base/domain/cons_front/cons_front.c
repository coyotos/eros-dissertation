/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "cons_front.h"
#include <eros/ReturnerKey.h>
#include <eros/DiscrimKey.h>

const uint32_t __rt_stack_pages = 0; 
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x21000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x41000;
#endif

static uint8_t rcvData[EROS_PAGE_SIZE];
static uint8_t snd_buffer[SIK_BUFSZ];


#define KR_DISCRIM    4
#define KR_CONS_IN    5

#define KR_STASHEDKEY 7
#define KR_TMP        8
#define KR_CONSOLEKEY 9
#define KR_SLEEPKEY  10
#define KR_RETURNER  11
#if EROS_NODE_SIZE == 16
#define KR_RK0       12
#define KR_RK1       13
#define KR_RK2       14
#define KR_RETURN    15
#elif EROS_NODE_SIZE == 32
#define KR_RK0       28
#define KR_RK1       29
#define KR_RK2       30
#define KR_RETURN    31
#endif


uint32_t stash_wants_rdbytes;
uint32_t stash_event_mask;
uint32_t current_events;

#define FLAG_EVENTS(evnts) (void)(current_events |= (evnts))

#define STASH_READY() ((stash_event_mask & current_events))
/* != 0 iff current_events and stash_event_mask have at least one 1
   bit in the same place */

/*
 * stash data structures
 */

struct shared * const kdb_buf = (void *)SH_ADDR;
                 /* points to the shared keyboard buffer */

uint16_t notify_flag = 0;

void
bcopy(const void *from, void *to, uint32_t len)
{
  /*
   * standard EROS bcopy
   */

  uint8_t *fp = (uint8_t *) from;
  uint8_t *tp = (uint8_t *) to;
    
  while(len--)
    *tp++ = *fp++;
}


int
strlen(const char *s)
{
  register uint32_t count = 0;
  
  while (*s++)
    count++;

  return count;
}


void
init ()
{
  kdb_buf->timeoutChars = UINT32_MAX;
  kdb_buf->timeoutMsecs = 0u;
  stash_wants_rdbytes = 0u;
  stash_event_mask = 0u;
  return;
}

uint32_t
writen(uint32_t n, const char *s)
{
  /*
   * a simple mechanism for writing characters to the console.
   * 
   */


  Message msg;
  
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_w1 = 0u;
  msg.snd_w2 = 0u;
  msg.snd_w3 = 0u;
  
  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;
  
  msg.snd_data = (uint8_t *) s;
  msg.snd_len = n;
  
  msg.rcv_len = 0;		/* no data returned */
  msg.snd_invKey = KR_CONSOLEKEY;
  msg.snd_code = 1;
  
  (void) CALL(&msg);

  return n; /* always send all characters. */
}

void
Write(const char *s)
{
  writen(strlen(s), s);
}

uint32_t 
diff(uint32_t head, uint32_t tail, uint32_t max, uint32_t full)
{
  /*
   * return the number of bytes between "head" and "tail", given
   * a max bufsz of "max"
   */

  if (full)
    return max;
     
  if (head == tail)
    return 0;
  if (head > tail)
    return (head - tail);
  else
    return (head + (max - tail));
  
}


void
fork_cons_in ( )
{
  /*
   * we've been told that cons_in is available and waiting
   * for us to SEND it when the buffer starts to empty.
   * Now it's time to do it.
   */
  
  Message msg;
  
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_len = 0;
  msg.snd_w1 = 0u;
  msg.snd_w2 = 0u;
  msg.snd_w3 = 0u;
  
  msg.snd_invKey = KR_CONS_IN;
  msg.snd_code = RC_OK;

  (void) SEND(&msg); 
}



uint32_t
read_buf (uint32_t rdbytes, void *data)
{

  /*
   * perform data xfer between shared keyboard buffer
   * and data buffer
   *
   * update our pointers in shared buf.
   *
   * return number of bytes transferred.
   */

  uint32_t top;

  top = SIK_BUFSZ-kdb_buf->tail;
     
  if (top > rdbytes)		        /* no wrap */
    {
      bcopy( &(kdb_buf->buf[kdb_buf->tail]), data,
	     rdbytes);
      kdb_buf->tail += rdbytes;
    }
  else				/* wrap buf */
    {
      bcopy(&(kdb_buf->buf[kdb_buf->tail]), data,
	    top );
      kdb_buf->tail = 0;
      bcopy(&(kdb_buf->buf[kdb_buf->tail]), data + top , 
	    rdbytes - top);
      kdb_buf->tail += rdbytes - top;
    }
	
  if ((rdbytes) && (kdb_buf->full)) /* reset full ptr */
    {
      kdb_buf->full = 0;
      if (notify_flag)
	fork_cons_in();
      notify_flag = 0;
    }

  return rdbytes;
}


void
SetupStashReturn(Message *msg, uint32_t krStash)
{
  uint32_t cur_head = kdb_buf->head;
  uint32_t bytes_avail = diff(cur_head, kdb_buf->tail, SIK_BUFSZ, kdb_buf->full);

  uint32_t toRead = stash_wants_rdbytes;

  if (bytes_avail < toRead) toRead = bytes_avail;

  if (!current_events) {
    kdprintf(KR_CONSOLEKEY,
	     "Cons_front: SetupStashReturn called when no events were flagged!\n");
  }
  
  msg->snd_len = read_buf(toRead, snd_buffer);
  msg->snd_data = snd_buffer;

  msg->snd_code = RC_OK;
  msg->snd_invKey = krStash;

  msg->snd_w1 = msg->snd_len;
  msg->snd_w2 = current_events;
  msg->snd_w3 = 0u;
  
  current_events = 0u;

  stash_wants_rdbytes = 0u;
  stash_event_mask = 0u;

  kdb_buf->wakeup = SIK_BUFSZ + 1;
}

void
SendStashAndPrepReturn(Message *msg, uint32_t krStash, uint32_t result)
{
  uint32_t returnKey = msg->snd_invKey;

  SetupStashReturn(msg,krStash);

  SEND(msg);

  msg->snd_invKey = returnKey;
  msg->snd_code = result;
  msg->snd_key0 = KR_ZERO;
  msg->snd_key1 = KR_ZERO;
  msg->snd_key2 = KR_ZERO;
  msg->snd_key3 = KR_ZERO;
  msg->snd_w1 = 0u;
  msg->snd_w2 = 0u;
  msg->snd_w3 = 0u;
  msg->snd_len = 0;

  return;
}


uint32_t 
stash_reader (uint16_t keyNum, uint32_t rdbytes, uint32_t eventMask)
{

  /*
   * stash the reader's return key and number of bytes
   * 
   * return 1 on succ; 0 if too many already stashed
   */
  
  if (discrim_classify(KR_DISCRIM, keyNum) != Discrim_Class_Resume) {
    return 0; /* not a resume key */
  }
  if (stash_event_mask != 0) {
    return 0;
  } else {
    copy_key_reg(KR_RETURNER, keyNum, KR_STASHEDKEY);
    copy_key_reg(KR_RETURNER, keyNum, KR_ZERO);
    
    stash_wants_rdbytes = rdbytes;
    stash_event_mask = eventMask;
    
    /* reset the filled buffer event */
    current_events &= ~(CharSrc_FilledBufferEvent);
    
    return 1;
  }
}

uint32_t
verify_stashed_key(void)
{
  /* make sure the current stashed key is still valid */
  if (discrim_classify(KR_DISCRIM, KR_STASHEDKEY) != Discrim_Class_Resume) {
    stash_wants_rdbytes = 0;
    stash_event_mask = 0;
    return 0; /* not a resume key */
  }
  return 1;
}

void
set_wakeup (uint32_t rdbytes, uint32_t extra_bytes, uint32_t current_head)
{
  /*
   * set the wakeup pointer to a position in the kbb_buffer.
   * never set the pointer further back.
   */

#if 0
  uint32_t current_wakeup = diff(kdb_buf->wakeup, current_head,
			     SIK_BUFSZ, kdb_buf->full);
#endif

#if 0
  DEBUG(unsorted)
    kprintf(KR_CONSOLEKEY,
	     "cons_front: set_wakeup: kdb->wakeup %x curr_wakeup %x\n",
	     kdb_buf->wakeup, current_wakeup);

  if (current_wakeup > extra_bytes) {
#endif
    kdb_buf->wakeup = current_head + (rdbytes - extra_bytes);
    if (kdb_buf->wakeup >= SIK_BUFSZ)           /* wrap buf */
      kdb_buf->wakeup = kdb_buf->wakeup - SIK_BUFSZ;
#if 0
  }
#endif
}

uint32_t
check_readable(uint32_t rdbytes)
{
  /*
   * check to see if there are rdbytes available for reading.
   * return 1 if there are enough bytes.  return 0 and set
   * the wakeup pointer if there are not enough bytes.
   */

  uint32_t current_head = kdb_buf->head;	/* don't use the shared variable */
  uint32_t extra_bytes;			/* bytes available for reading*/

  extra_bytes = diff(current_head, kdb_buf->tail, SIK_BUFSZ,
		     kdb_buf->full);

  if (extra_bytes >= rdbytes) {
    return 1;
  } else {
    set_wakeup (rdbytes, extra_bytes, current_head);
    return 0;
  }
	  
}

void
subtract_buf (uint32_t len)
{
  /*
   * delete len number of characters from the keybd buffer
   */

  int i;
  
  for (i = 0 ; i < len; i++)
    {
      if (kdb_buf->head == 0)
	kdb_buf->head = SIK_BUFSZ-1;
      if (kdb_buf->head == kdb_buf->tail)
	break;
      kdb_buf->head--;
    }

}
		   
int
ProcessRequest(Message *msg)
{
  /*
   * start key has been invoked; read/write/wakeup/change status
   * depending on the orderCode received
   *
   */

  uint32_t rcvlen;			/* copy of msg->rcv_len */
     
  rcvlen = msg->rcv_len;		/* copy rcvlen */
  msg->rcv_len = EROS_PAGE_SIZE;	/* re-initialize rcvlen */

  switch (msg->rcv_code) {
 
  case LD_WAKEUP:
    if (msg->rcv_keyInfo != 1u) { /* is this really cons_in? */
      msg->snd_code = RC_UnknownRequest;
      break;
    }

    FLAG_EVENTS(msg->rcv_w1);

    if (STASH_READY()) {
      SendStashAndPrepReturn(msg, KR_STASHEDKEY, RC_OK);
    } else {
      msg->snd_code = RC_OK;
    }

    break;		      
       
  case LD_NOTIFY:
    if (msg->rcv_keyInfo != 1u) { /* is this really cons_in? */
      msg->snd_code = RC_UnknownRequest;
      break;
    }

    if (kdb_buf->full) {
      notify_flag = 1;
      
      /* squirrel away the resume key to invoke when there is space */
      copy_key_reg(KR_RETURNER, KR_RETURN, KR_CONS_IN);
      msg->snd_invKey = KR_ZERO;
    } else {
      notify_flag = 0;
      
      /* we aren't full anymore -- fooled you, didn't I? */
      msg->snd_code = 1;
    }
    break;		      
       
  case OC_CharSrc_WaitForEvent:

    /* FIXME: add event handling */
    if (stash_event_mask != 0
	&& verify_stashed_key() != 0) {
      /* we've already got a stashed reader! */
      msg->snd_len = 0;
      msg->snd_code = RC_CharSrc_OneWaiterOnly;
      break;
    }

    /* verify_stashed_key will have reset everything */
    
    if ((msg->rcv_w2 & (~CharSrc_ALL_Events)) != 0) {
      /* invalid event mask */
      msg->snd_len = 0;
      msg->snd_code = RC_RequestError;
      break;
    }
       
    if (msg->rcv_w1 > SIK_BUFSZ) {
      msg->rcv_w1 = SIK_BUFSZ; /* cheat */
    }

    
    if (!stash_reader(msg->rcv_key3, msg->rcv_w1, msg->rcv_w2)) {
      msg->snd_code = RC_RequestError;
      break;
    }

    msg->snd_invKey = KR_ZERO;
		  
    /*
     * This looks really sleazy, because it is.  If
     * check_readable fails it sets the wakeup
     * pointer and then we call it again to avoid
     * a potential race condition with cons_in and
     * the wakeup pointer.
     *
     * Guarantee from cons_in: when the wakeup pointer
     * is checked, the "addtobuffer" action cooresponding
     * to it has already occured.
     */

    if (check_readable(msg->rcv_w1) || check_readable(msg->rcv_w1)) {
      FLAG_EVENTS(CharSrc_FilledBufferEvent);
    }

    if (STASH_READY()) {
      SetupStashReturn(msg, KR_STASHEDKEY);
    }
    break;

  case OC_CharSrc_PostEvent:

    if ((msg->rcv_w1 & (~CharSrc_UserEventMask)) != 0
	|| msg->rcv_w1 == 0u) {
      /* invalid request -- either we got a non-user event, or the
	 event mask was zero. */
      DEBUG(unsorted)
        kdprintf(KR_CONSOLEKEY,"cons_front: Non-User-events posted (%08x)\n",
                 msg->rcv_w1);
      msg->snd_code = RC_RequestError;
      break;
    }

    /* post the event, and if we are going to wake up the stashed key,
       SEND the sender's resume key, and RETURN to the stashed resume
       key.
       */

    FLAG_EVENTS(msg->rcv_w1);
    DEBUG(unsorted)
      kprintf(KR_CONSOLEKEY,"cons_front: Posted events %08x. ",msg->rcv_w1);

    if (STASH_READY()) {
      DEBUG(unsorted)
        kprintf(KR_CONSOLEKEY,"Sending stashed key\n");

      SendStashAndPrepReturn(msg, KR_STASHEDKEY, RC_OK);
    } else {
      DEBUG(unsorted)
        kprintf(KR_CONSOLEKEY,"Reciever not ready\n");
      msg->snd_code = RC_OK;
    }
    break;
    
  case OC_CharSrc_Write:

    /* always succeeds */
    msg->snd_w1 = writen(rcvlen, msg->rcv_data);    
    msg->snd_code = RC_OK;
    break;

  case OC_CharSrc_Control:

    DEBUG(control)
      kdprintf(KR_CONSOLEKEY,
               "cons_front: Got Control %d: %d\n",msg->rcv_w1,msg->rcv_w2);

    switch (msg->rcv_w1) {
    case CharSrc_Control_SetTimeout:
      DEBUG(control)
        kdprintf(KR_CONSOLEKEY,
                 "cons_front: Timeout set to %d chars %d msecs\n",
                 msg->rcv_w2,
                 msg->rcv_w3);

      kdb_buf->timeoutChars = msg->rcv_w2;
      kdb_buf->timeoutMsecs = msg->rcv_w3;
      msg->snd_code = RC_OK;
      break;

    case CharSrc_Control_GetTimeout:

      msg->rcv_w2 = kdb_buf->timeoutChars;
      msg->rcv_w3 = /*kdb_buf->timeoutMsecs*/ 0; 
                                      /* FIXME: Someday real timeout support */

      msg->snd_code = RC_OK;
      break;

    case CharSrc_Control_GetSpecialChars:
      {
	uint32_t idx = 0;
	uint32_t *char_mask = kdb_buf->char_mask;
	uint16_t *out = (uint16_t *)rcvData;

	for (idx = 0; idx < 256; idx++) {
	  if (CHECK_CHAR(char_mask,idx)) {
	    *out++ = idx; /* record the Special Character */
	  }
	}
	msg->snd_data = rcvData;
	msg->snd_len = sizeof(uint16_t)*(out - (uint16_t *)rcvData);
	msg->snd_code = RC_OK;
	break;
      }
    case CharSrc_Control_SetSpecialChars:
    
      DEBUG(control)
        kdprintf(KR_CONSOLEKEY,"Cons_front: Setting %d specialChars\n",
                 rcvlen);
 
      DEBUG(control)
        kprintf(KR_CONSOLEKEY,"Cons_front: Set SpecialChars (len %d)",
                rcvlen); 
      if ((rcvlen & 1u) != 0) {
	 msg->snd_code = RC_RequestError;
         DEBUG(control)
           kdprintf(KR_CONSOLEKEY," FAILED -- rcv_len not even\n");
      } else {
	uint32_t idx;
	uint16_t *src = msg->rcv_data;

        DEBUG(control)
          kprintf(KR_CONSOLEKEY," verify");

	/* verify the characters first */
	for (idx = (rcvlen / 2); idx > 0; idx--) {
	  /* make sure it is ASCII */
	  if ((*src & ~0xFFu) != 0) {
	    msg->snd_code = RC_RequestError;
            DEBUG(control)
              kdprintf(KR_CONSOLEKEY," FAILED -- non-ascii char %04x\n",*src);
	    break;
	  }
	  src++;
	}
	if (idx) break; /* unsuccessful. */
	
	/* verified. reset src. */
	src = msg->rcv_data;
	/* clear the mask */
	for (idx = 0; idx < (256u >> 5); idx++) {
	  kdb_buf->char_mask[idx] = 0u;
	}

        DEBUG(control)
          kprintf(KR_CONSOLEKEY,"Chars:");
           
	for (idx = (rcvlen / 2); idx > 0; idx--) {
          DEBUG(control)
            kprintf(KR_CONSOLEKEY," 0x%02x",*src);
	  SET_CHAR(kdb_buf->char_mask,(*src));
	  src++;
	}
        DEBUG(control)
          kdprintf(KR_CONSOLEKEY,".\n");

	msg->snd_code = RC_OK;
      }
      break;
      
    default:
      msg->snd_code = RC_RequestError;
      break;
    }
    DEBUG(control)
      kdprintf(KR_CONSOLEKEY,
               "Cons_front: Done processing Control\n");

    break;

/*
  case KT:
    msg->snd_code = AKT_TtyKey;
    break;
 */   
  default:
    msg->snd_code = RC_UnknownRequest;
    break;
  }
  return 1;
}

int
main ()
{

  Message msg;

  /* why can't this be in a header file? */
  msg.snd_invKey = KR_ZERO;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;
     
  msg.rcv_key0 = KR_RK0;
  msg.rcv_key1 = KR_RK1;
  msg.rcv_key2 = KR_RK2;
  msg.rcv_key3 = KR_RETURN;
  msg.rcv_data = rcvData;
  msg.rcv_code = 0;

  init();
     
  do {
    msg.rcv_len = EROS_PAGE_SIZE;

    RETURN(&msg);
    msg.snd_key0 = KR_ZERO;
    msg.snd_key1 = KR_ZERO;
    msg.snd_key2 = KR_ZERO;
    msg.snd_key3 = KR_ZERO;
    msg.snd_w1 = 0;
    msg.snd_w2 = 0;
    msg.snd_w3 = 0;
    msg.snd_len = 0u;
    msg.snd_invKey = KR_RETURN;
  } while ( ProcessRequest(&msg) );

  return 0;
}
