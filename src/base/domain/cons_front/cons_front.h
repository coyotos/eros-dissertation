/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <domain/domdbg.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>
#include <eros/TtyKey.h>

#ifndef NDEBUG

#define dbg_unsorted 0x1u
#define dbg_control  0x2u

#define dbgflgs (0u)

#define DEBUG_CND(x) (dbgflgs & dbg_ ## x)

#else /*NDEBUG*/

#define DEBUG_CND(x) (0)

#endif /*NDEBUG*/

#define DEBUG(x) if (DEBUG_CND(x)) 

#define SIK_BUFSZ (uint32_t) 256

/*
 * this is the shared data structure between the two domains
 */

struct shared {
  char buf[SIK_BUFSZ];          /* incoming keyboard buffer */
  uint32_t head;			/* buf ptr; kdb read thread mods this */
  uint32_t tail;			/* buf ptr; line disp thread mods this */
  uint16_t full;		/* flag set to 1 if head == tail
				                    && buf is full */
  uint32_t wakeup;			/* wakeup line disc at head==wakeup */

  uint32_t timeoutChars;            /* number of chars before timeout
				   starts */
  uint32_t timeoutMsecs;            /* length of timeout, in milliseconds
				 */
  
  uint32_t char_mask[256u >> 5];    /* 256 / (8*sizeof(uint32_t) */
                                /* bit per character -- indicates
				   which to wake up line disc on */
};

#define CHECK_CHAR(char_mask, char) \
    !!(((char_mask)[((unsigned)(char))>>5]) & (1u<<((char) & ((1<<5)-1))))

#define SET_CHAR(char_mask, char) \
     (((char_mask)[((unsigned)(char))>>5]) |= (1u<<((char) & ((1<<5)-1))))

#if EROS_NODE_SIZE == 16
#define SH_ADDR      0x30000	/* address of shared page; see in
				   test.imgmap */
#elif EROS_NODE_SIZE == 32
#define SH_ADDR      0x60000	/* address of shared page; see in
				   test.imgmap */
#endif

#define SH_SLOT      3		/* slot in segment for the shared page */

#define LD_WAKEUP     5		/* from ttyin; data == kdb_buf */
#define LD_NOTIFY     6		/* from ttyin; data == kdb_buf */
