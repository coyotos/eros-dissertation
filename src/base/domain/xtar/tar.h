/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/* Standard Archive Format - Standard TAR - USTAR.  */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <KEYDEFS.h>
#include <domain/domdbg.h>

#include <sys/fcntl.h>
#include <sys/errno.h>
#include <unistd.h>
#include <string.h>

#define RECORDSIZE	512
#define NAMSIZ		100
#define TUNMLEN		32
#define TGNMLEN		32
#define SPARSE_IN_HDR	4

#define TMAGIC          "ustar  " /* 7 chars and a null */
#define TAREXIT_SUCCESS 0
#define LF_LINK		'1'	/* link to previously dumped file */
/* Identifies the NEXT file on the tape as having a long linkname.  */
#define LF_LONGLINK	'K'
/* Identifies the NEXT file on the tape as having a long name.  */
#define LF_LONGNAME	'L'

#define LF_OLDNORMAL	'\0'	/* normal disk file, Unix compat */
#define LF_NORMAL	'0'	/* normal disk file */
#define LF_CONTIG	'7'	/* contiguous file */
#define LF_DIR		'5'	/* directory */
#define LF_DUMPDIR	'D'

struct stat {
  unsigned long  st_size;
  unsigned long  st_atime;
  unsigned long  st_mtime;
  unsigned long  st_ctime;
  unsigned long  st_mode;
};

struct sparse {
  char offset[12];
  char numbytes[12];
};

struct header {
  char arch_name[NAMSIZ];
  char mode[8];
  char uid[8];
  char gid[8];
  char size[12];
  char mtime[12];
  char chksum[8];
  char linkflag;
  char arch_linkname[NAMSIZ];
  char magic[8];
  char uname[TUNMLEN];
  char gname[TGNMLEN];
  char devmajor[8];
  char devminor[8];
  
  /* The following fields were added for GNU and are not standard.  */
  char atime[12];
  char ctime[12];
  char offset[12];
  char longnames[4];
  /* Some compilers would insert the pad themselves, so pad was
     once autoconfigured.  It is simpler to always insert it!  */
  char pad;
  struct sparse sp[SPARSE_IN_HDR];
  char isextended;
  char realsize[12];	/* true size of the sparse file */
};

struct extended_header {
  struct sparse sp[21];
  char isextended;
} ext_hdr;

union record {
  char charptr[RECORDSIZE];
  
  void * offset;		/* offset in the file */
  struct header header;
  struct extended_header ext_hdr;
};

typedef struct global_vars {
  void *head;			/* points to current archive header */
  int blocking;			/* Size of each block, in records.  */
  int blocksize;		/* Size of each block, in bytes.  */
  void *ar_last;		/* Last+1 record of archive block.  */
  void *ar_block;		/* Start of block of archive.  */
  void *ar_record;		/* Current record of archive.  */
  struct header block;		/* A block */
  int archive;			/* file descriptor for archive file */
  int exit_status;		/* status for exit when tar finishes */
  void *input;			/* Input mmaped */
  int hit_eof;			/* Hit the end-of-file */
  char current_file_name[NAMSIZ];
  char current_link_name[NAMSIZ];
  struct stat hstat;		/* File statistics */
} Globals;

/* EROS #defines these */
#define xmalloc      malloc
#define xstrdup(x)   strcpy(malloc (strlen (x) + 1), x)   

/* Module buffer.c.  */
void *findrec (Globals *);
void flush_archive (Globals *);
void *endofrecs (Globals *);
void open_archive (Globals *);
void fl_read (Globals *);
void userec (void *, Globals *);
void close_archive (Globals *);

/* Module extract.c.  */
void extract_archive (Globals *);

/* Module list.c.  */
void read_and (void (*do_) (), Globals *);
long from_oct __P ((int, char *));
int read_header (Globals *);
void decode_header (Globals *);
void skip_file (register long , Globals *);

/* EROS #defines these */
#define xmalloc       malloc
#define xstrdup(x)    strcpy(malloc (strlen (x) + 1), x)   
#define EOF           -1

#define KR_ZERO       0
#define KR_RESUME     15

#define TARBALL       0x80000000;       /* LSS=7 */

#define OC_Create     01
#define OC_Modify     02
#define MEMCOPY
#define STRCHR
extern int mkdir(const char *name, mode_t mode);
#define BLOCKINGSIZE  1
