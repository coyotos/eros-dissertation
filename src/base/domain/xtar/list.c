/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "system.h"
#define	ISODIGIT(Char) \
  ((unsigned char) (Char) >= '0' && (unsigned char) (Char) <= '7')
#define ISSPACE(Char) (ISASCII (Char) && (Char == ' '))
#include "tar.h"

void
read_and (void (*do_something) (), Globals *gv)
{
  int status = 3;		/* initial status at start of archive */
  int prev_status;

  open_archive (gv);		/* open for reading */

  while (1)
    {
      prev_status = status;
      status = read_header (gv);
      switch (status)
	{
	case 1:
	  /* Valid header.  We should decode next field (mode) first.
	     Ensure incoming names are null terminated.  */
	  (*do_something) (gv);
	  continue;

	  /* If the previous header was good, tell them that we are
	     skipping bad ones.  */

	case 2:			/* record of zeroes */
	  userec (gv->head, gv);
	  status = prev_status;	/* if error after 0's */
	  /* Fall through.  */

	case EOF:		/* end of archive */
	  break;
	}
      break;
    };

  close_archive (gv);
}

int
read_header (Globals *gv)
{
  register int i;
  register long sum, signed_sum, recsum;
  register char *p;
  char *header;

  while (1) {
    header = findrec (gv);
    memcpy(&gv->block, header, sizeof(gv->block));
    kdprintf(KR_OSTREAM, "header=%x 0=%x 1=%x 148=%x 149=%x, %s\n",
	     header, header[0], header[1], header[148], header[149],
	     gv->block.chksum);
    gv->head = header;		/* this is our current header */
    if (!header)
      return EOF;
    
    recsum = from_oct (8, gv->block.chksum);
    
    sum = 0;
    signed_sum = 0;
    p = (char*)header;
    for (i = RECORDSIZE; --i >= 0;) {
      /* We can't use unsigned char here because of old compilers, e.g. V7.  */
      sum += 0xFF & *p;
      signed_sum += *p++;
    }
    
    /* Adjust checksum to count the "chksum" field as blanks.  */
    
    for (i = sizeof (gv->block.chksum); --i >= 0;) {
      sum -= 0xFF & gv->block.chksum[i];
      signed_sum -= (char) gv->block.chksum[i];
    }
    sum += ' ' * sizeof gv->block.chksum;
    signed_sum += ' ' * sizeof gv->block.chksum;
    
    if (sum == 8 * ' ') {
      /* This is a zeroed record...whole record is 0's except for the 8
	 blanks we faked for the checksum field.  */
      
      return 2;
    }
    
    if (sum != recsum && signed_sum != recsum)
      return 0;
    
    /* Good record.  Decode file size and return.  */
    
    if (gv->block.linkflag == LF_LINK)
      gv->hstat.st_size = 0;	/* links 0 size on tape */
    else
      gv->hstat.st_size = from_oct (1 + 12, gv->block.size);
    
    kdprintf(KR_OSTREAM, "Set file size to %d\n", gv->hstat.st_size);
    gv->block.arch_name[NAMSIZ - 1] = '\0'; /* Make sure well-defined */
    gv->block.arch_linkname[NAMSIZ - 1] = '\0';
    strcpy(gv->current_file_name, gv->block.arch_name);
    strcpy(gv->current_link_name, gv->block.arch_linkname);

    return 1;
  }
}

void
decode_header (Globals *gv)
{
  gv->hstat.st_mtime = from_oct (1 + 12, gv->block.mtime);
}

long
from_oct (register int digs, register char *where)
{
  register long value;

  kdprintf(KR_OSTREAM, "Entered from_oct w/ %s %d\n", where, digs);
  while (ISSPACE (*where)) {	/* skip spaces */
    where++;
    if (--digs <= 0)
      return -1;		/* all blank field */
  }
  value = 0;
  while (digs > 0 && ISODIGIT (*where)) {
    /* Scan til nonoctal.  */
    value = (value << 3) | (*where++ - '0');
    --digs;
  }
  if (digs > 0 && *where && !ISSPACE (*where))
    return -1;			/* ended on non-space/nul */

  return value;
}

void
skip_file (register long size, Globals *gv)
{
  void *x;

  while (size > 0) {
    x = findrec (gv);
    userec (x, gv);
    size -= RECORDSIZE;
  }
}
