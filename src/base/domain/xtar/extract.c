/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "system.h"
#include "tar.h"

void
extract_archive (Globals *gv)
{
  void *data;
  int fd, check, namelen, written;
  long size;
  register int skipcrud;

#define CURRENT_FILE_NAME (skipcrud + gv->current_file_name)

#ifdef MDEBUG
  kdprintf(KR_OSTREAM, "Entered extract archive\n");
#endif
  
  userec (gv->head, gv);	/* and go past it in the archive */
  decode_header (gv);

  skipcrud = 0;
  while (CURRENT_FILE_NAME[0] == '/') {
    skipcrud++;			/* force relative path */
  }
  switch (gv->block.linkflag) {
    default:
    case LF_OLDNORMAL:
    case LF_NORMAL:
    case LF_CONTIG:
      namelen = strlen (CURRENT_FILE_NAME) - 1;
      if (CURRENT_FILE_NAME[namelen] == '/')
	goto really_dir;

      fd = open(CURRENT_FILE_NAME, O_RDWR|O_CREAT, gv->hstat.st_mode);
      kdprintf(KR_OSTREAM, "Opened file %s with fd %d size %d errno %d\n",
	       CURRENT_FILE_NAME, fd, gv->hstat.st_size, errno);
      
      if (fd < 0) {
	skip_file ((long) gv->hstat.st_size, gv);
	goto quit;
      }
      
      for (size = gv->hstat.st_size; size > 0; size -= written) {
	data = findrec(gv);
	kdprintf(KR_OSTREAM, "Found data 0x%08x\n", data);
	if (data == NULL)
	  break;

	written = endofrecs(gv) - data;

	if (written > size)
	  written = size;
	errno = 0;
	check = write (fd, data, (size_t) written);

	/* The following is in violation of strict typing, since the
	   arg to userec should be a struct rec *.  FIXME.  */
	
	kdprintf(KR_OSTREAM, "aft written %d, check %d, size %d\n",
		 written, check, size);
	userec (data + written - 1, gv);
	
	if (check == written)
	  continue;
	/* Error in writing to file.  Print it, skip to next file in archive. */
	skip_file ((long) (size - written), gv);
	break;		/* still do the close, mod time, chmod, etc */
      }
      check = close (fd);
  quit:
      break;

  case LF_DIR:
  case LF_DUMPDIR:
    namelen = strlen (CURRENT_FILE_NAME) - 1;
    
  really_dir:
    /* Check for trailing /, and zap as many as we find.  */
    
    while (namelen && CURRENT_FILE_NAME[namelen] == '/')
      CURRENT_FILE_NAME[namelen--] = '\0';
    
    if (gv->block.linkflag == LF_DUMPDIR)
      skip_file ((long) (gv->hstat.st_size), gv);
    
    check = mkdir (CURRENT_FILE_NAME, 0 | (int) gv->hstat.st_mode);
    if (check != 0) {
      /* If we're trying to create '.', let it be.  */
      
      if (CURRENT_FILE_NAME[namelen] == '.'
	  && (namelen == 0 || CURRENT_FILE_NAME[namelen - 1] == '/'))
	goto check_perms;
      break;
    }
    
  check_perms:
    if (0300 != (0300 & (int) gv->hstat.st_mode))
      gv->hstat.st_mode |= 0300;
  }
    
#undef CURRENT_FILE_NAME
}
