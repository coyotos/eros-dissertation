/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Xtar -- domain that untars an archive and creates appropriate
 *         directories and files.
 *
 * Key Registers:
 *
 * KR12: arg0 Target directory
 * KR13: arg1 User's spacebank
 * KR14: arg2 
 * KR15: arg3
 */

#include "system.h"
#include "tar.h"

const uint32_t __rt_stack_pages = 0x0;
const uint32_t __rt_stack_pointer = 0x21000;

int
ProcessRequest(Message *msg)
{
  Globals global_vars;
  

  kdprintf(KR_OSTREAM, "procreq:Keys: 0x%08x 0x%08x 0x%08x 0x%08x\n",
           key_kt (KR_RETURNER), key_kt(KR_CWD), key_kt(KR_FSROOT),
	   key_kt(KR_FD_TAB));

  msg->snd_len = msg->rcv_len;
  msg->snd_data = msg->rcv_data;

  msg->snd_key0 = msg->rcv_key0;
  msg->snd_key1 = msg->rcv_key1;
  msg->snd_key2 = msg->rcv_key2;
  msg->snd_key3 = msg->rcv_key3;

  global_vars.blocking = BLOCKINGSIZE;
  global_vars.blocksize = global_vars.blocking * RECORDSIZE;
  global_vars.exit_status = TAREXIT_SUCCESS;
  global_vars.head = global_vars.ar_block = NULL;
  global_vars.ar_last = global_vars.ar_record = NULL;
  global_vars.hit_eof = 0;

  read_and (extract_archive, &global_vars);

  /* leave receive info unchanged, but reset the length limit: */
  msg->snd_code = RC_OK;
  return global_vars.exit_status;
}

int
main()
{
  Message msg;

  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  node_copy(KR_CONSTIT, KC_SLEEP, KR_SCRATCH);

  kdprintf(KR_OSTREAM, "Hit xtar main\n");
  msg.snd_invKey = KR_ZERO;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = KR_RETURNER;
  msg.rcv_key1 = KR_CWD;
  msg.rcv_key2 = KR_FSROOT;
  msg.rcv_key3 = KR_RESUME;
  msg.rcv_len = 0;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  do {
    RETURN(&msg);
    msg.snd_invKey = msg.rcv_key3;
  } while ( ProcessRequest(&msg) );
}
