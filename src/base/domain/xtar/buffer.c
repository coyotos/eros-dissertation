/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "system.h"
#include "tar.h"

int
t_read(Globals *gv)
{
  static int offset;

  /*  if ( (offset+gv->blocksize) > TARBALL_SIZE)
    return 0; if we need this we need the size */
  gv->ar_block = gv->input + offset;
  offset += gv->blocksize;
  return gv->blocksize;
}

void
userec (void *rec, Globals *gv)
{
  while (rec >= gv->ar_record)
    gv->ar_record += RECORDSIZE; /* Jump a record */

  if (gv->ar_record > gv->ar_last)
    kdprintf(KR_OSTREAM, "ABORT SHIP!!!!!!!\n");
}

void *
endofrecs (Globals *gv)
{
  return gv->ar_last;
}

void
open_archive (Globals *gv)
{
  gv->input = (void*) TARBALL;
  
  findrec (gv);			/* read it in, check for EOF */
}

void *
findrec (Globals *gv)
{
  if (gv->ar_record == gv->ar_last) {
    if (gv->hit_eof)
      return NULL;
    flush_archive (gv);
    if (gv->ar_record == gv->ar_last) {
      gv->hit_eof++;
      return NULL;
    }
  }
  return gv->ar_record;
}

void
flush_archive (Globals *gv)
{
  fl_read (gv);

  gv->ar_record = gv->ar_block;	/* restore pointer to start */
  gv->ar_last = gv->ar_block + gv->blocksize; /* restore pointer to end */
}

void
fl_read (Globals *gv)
{
  int err;			/* result from system call */

  err = t_read(gv);
}

void
close_archive (Globals *gv)
{
}
