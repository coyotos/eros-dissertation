/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#define KR_CONSTIT     1
#define KR_SELF        2
#define KR_DOMCRE      3
#define KR_BANK        4
#define KR_SCHED       5
#define KR_KEYBITS     6
#define KR_OSTREAM     7
#define KR_SCRATCH     8
#define KR_RETURNER    9
#define KR_ADDRNODE   10
#define KR_DISCRIM    11
#if EROS_NODE_SIZE == 16
#define KR_ARG0       12
#define KR_SCRATCH2   13
#define KR_SETRESUME  14
#define KR_RESUME     15 /* don't use it for anything else! */
#elif EROS_NODE_SIZE == 32
#define KR_ARG0       28
#define KR_SCRATCH2   29
#define KR_SETRESUME  30
#define KR_RESUME     31 /* don't use it for anything else! */
#endif

#define KC_RETURNER    1
#define KC_ZSF         2	/* zero space factory */
#define KC_PROTOSPC    3
#define KC_OSTREAM     4
#define KC_KEYBITS     5	/* Keybits */
#define KC_DISCRIM     6	/* discrim */

struct table_entry {
  uint32_t w[5]; /* 4 words of keybits, 1 word of additional data */
};

#ifdef GNU
#define INLINE inline
#else
#define INLINE
#endif

#ifndef INLINE_COMPARE
extern int compare(uint32_t *k1, uint32_t *k2);
#else
static INLINE int
compare(uint32_t *k1, uint32_t *k2)
{
  if (k1[0] < k2[0]) {
    return -1;
  } else if (k1[0] > k2[0]) {
    return 1;
  }

  if (k1[1] < k2[1]) {
    return -1;
  } else if (k1[1] > k2[1]) {
    return 1;
  }

  if (k1[2] < k2[2]) {
    return -1;
  } else if (k1[2] > k2[2]) {
    return 1;
  }

  if (k1[3] < k2[3]) {
    return -1;
  } else if (k1[3] > k2[3]) {
    return 1;
  }

  return 0; /* equal */
}
#endif

struct KeySetStat {
  uint32_t startKeyBitsVersion;

  struct table_entry *end_of_table;
  uint32_t numSorted;
  uint32_t numUnsorted;

  enum initState {
    START = 0,
    LOBOTOMIZED,
    ZSBOUGHT,
    RUNNING
  } initstate;
};

#define MAX_UNSORTED (16u) /* maximum number of unsorted before sorting */
			     
/* sort the table */
void
sortTable(struct table_entry *table, uint32_t length);

/* search /table/ -- assumes first /lengthSorted/ items are sorted,
                     next /lengthUnsorted/ items are not.  */

struct table_entry *
findEntry(struct table_entry *table,
	  uint32_t toFind[4],
	  uint32_t lengthSorted,
	  uint32_t lengthUnsorted);

#define FIND(table, toFind, STAT) \
    findEntry(table, toFind, (STAT).numSorted, (STAT).numUnsorted)

/* debugging stuff */
#define dbg_init     0x01
#define dbg_cmds     0x02
#define dbg_add      0x04
#define dbg_contains 0x08
#define dbg_find     0x10
#define dbg_protocol 0x20

#define dbg_all      0x3F

/* Following should be an OR of some of the above */
#define dbg_flags   ( dbg_contains | dbg_add | 0u )

#ifndef NDEBUG
#define DEBUG(x) if (dbg_##x & dbg_flags)
#else
#define DEBUG(x) if (0)
#endif

#ifdef NDEBUG
#define assert(ignore) ((void) 0)
#else

extern int __assert(const char *, const char *, int);

#define assert(expression)  \
  ((void) ((expression) ? 0 : __assert (#expression, __FILE__, __LINE__)))
#endif

