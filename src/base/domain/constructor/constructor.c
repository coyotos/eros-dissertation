/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* A constructor is responsible for building program instances.  The
 * constructor holds 19 so-called "constituents":
 *
 * 0..15	Are passed to the created process in a constituents
 * 		capage by way of a fetch capability.
 *
 * 16		The process keeper, or a factory for that keeper.
 *
 * 17		The process address space, or a factory for that
 *              address space.
 *
 * 18		The process symbol table.
 *
 * 19		The process initial PC (as a number key)
 *
 * BOOTSTRAP NOTE 1:
 * 
 * To simplify system image construction, the constructor does some
 * minimal analysis at startup time.  If KC_PROD_CON0 and KC_PROD_CON1
 * are not DK0, they are accepted as holding the product constituents,
 * and KC_DCC should hold the domain creator for the constituents.
 * 
 * If initial constituents are found, the factory startup code assumes
 * that the factory should be initially sealed, and that no holes beyond
 * those that are apparent from the initial constituents are present,
 * and that KC_DCC is the actual domain creator.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/DiscrimKey.h>
#include <eros/ReturnerKey.h>
#include <eros/NodeKey.h>
#include <eros/ProcessKey.h>
#include <eros/Key.h>
#include <eros/StdKeyType.h>
#include <eros/ProcessState.h>
#include <domain/domdbg.h>
#include <domain/ConstructorKey.h>
#include <domain/ProcessCreatorKey.h>
#include <domain/PccKey.h>
#include <domain/ProtoSpace.h>
#include <domain/SpaceBankKey.h>

const uint32_t __rt_stack_pages = 1; /* specially built using metaconstructor */
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif

#define KR_ZERO     0
#define KR_CONSTIT  1
#define KR_SELF     2
#define KR_BANK     4
#define KR_SCRATCH  5
#define KR_RETURNER 6
#define KR_OSTREAM  7
#define KR_DOMCRE   8
#define KR_NEWDOM   9

#define KR_PROD_CON0 10		/* product's constituents */
#define KR_PROD_CON1 11		/* product's constituents */

#if EROS_NODE_SIZE == 16
#define KR_ARG0    12
#define KR_ARG1    13
#define KR_ARG2    14
#define KR_RESUME  15
#elif EROS_NODE_SIZE == 32
#define KR_ARG0    28
#define KR_ARG1    29
#define KR_ARG2    30
#define KR_RESUME  31
#else
#error "Unhandled node size"
#endif


#define KC_DISCRIM  0
#define KC_RETURNER 1
#define KC_OSTREAM  2
#define KC_DCC      3
#define KC_PROTOSPC 4
#define KC_MYDOMCRE 5
#define KC_PROD_CON0 14
#define KC_PROD_CON1 15

#define DEBUG if (0)
/* #define DEBUG if (1) */

typedef struct {
  int frozen;
  int has_holes;
} ConstructorInfo;

void
CheckDiscretion(uint32_t kr, ConstructorInfo *ci)
{
  uint32_t keyInfo;
  
  node_copy(KR_CONSTIT, KC_DISCRIM, KR_SCRATCH);
  if (discrim_verify(KR_SCRATCH, kr))
    return;

  node_copy(KR_CONSTIT, KC_DCC, KR_SCRATCH);
  keyInfo = proccre_amplify_gate(KR_SCRATCH, kr, KR_SCRATCH);
  if (keyInfo == 0) {
    /* This key is a requestor's key to a constructor. Ask the
       constructor if it is discrete */
    if (constructor_is_discreet(kr))
      return;
  }

  ci->has_holes = 1;
}

void
InitConstructor(ConstructorInfo *ci)
{
  uint32_t result;

  ci->frozen = 0;
  ci->has_holes = 0;
  
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);

  DEBUG kdprintf(KR_OSTREAM, "constructor init\n");
  
  node_copy(KR_CONSTIT, KC_PROD_CON0, KR_PROD_CON0);
  if (key_kt(KR_PROD_CON0) != AKT_Number) {
    /* This is an initially frozen constructor.  Use the provided
       constituents, and assume that KC_DCC in fact holds the proper
       domain creator. */
    node_copy(KR_CONSTIT, KC_DCC, KR_DOMCRE);
    node_copy(KR_CONSTIT, KC_PROD_CON0, KR_PROD_CON0);
    node_copy(KR_CONSTIT, KC_PROD_CON1, KR_PROD_CON1);

    ci->frozen = 1;
  }
  else {
    /* Build a new domain creator for use in crafting products */

    /* use KR_DOMCRE and KR_DISCRIM as scratch regs for a moment: */
    node_copy(KR_CONSTIT, KC_DCC, KR_DOMCRE);
    node_copy(KR_SELF, ProcSched, KR_SCRATCH);

    {
      Message msg;

      msg.snd_key0 = KR_BANK;
      msg.snd_key1 = KR_SCRATCH;
      msg.snd_key2 = KR_ZERO;
      msg.snd_key3 = KR_ZERO;
      msg.snd_data = 0;
      msg.snd_len = 0;
      msg.snd_code = OC_PCC_CreateProcessCreator;
      msg.snd_invKey = KR_DOMCRE;

      msg.rcv_key0 = KR_DOMCRE;
      msg.rcv_key1 = KR_ZERO;
      msg.rcv_key2 = KR_ZERO;
      msg.rcv_key3 = KR_ZERO;
      msg.rcv_len = 0;		/* no data returned */

      result = CALL(&msg);
      DEBUG kdprintf(KR_OSTREAM, "GOT DOMCRE Result is 0x%08x\n", result);
    }
  
    spcbank_buy_nodes(KR_BANK, 2, KR_PROD_CON0, KR_PROD_CON1, KR_ZERO);
  }
    
  node_copy(KR_CONSTIT, KC_RETURNER, KR_RETURNER);
}

/* In spite of unorthodox fabrication, the constructor self-destructs
   in the usual way. */
void
Sepuku()
{
  /* OH SHIT giving up the constituent nodes breaks our products */
  
  /* Give up the first constituent node. */
  /* Give up the second constituent node */
  spcbank_return_node(KR_BANK, KR_PROD_CON0);
  spcbank_return_node(KR_BANK, KR_PROD_CON1);
  node_copy(KR_CONSTIT, KC_MYDOMCRE, KR_DOMCRE);
  node_copy(KR_CONSTIT, KC_PROTOSPC, KR_PROD_CON0);

  spcbank_return_node(KR_BANK, KR_CONSTIT);

  /* Invoke the protospace with arguments indicating that we should be
     demolished as a small space domain */
  protospace_destroy(KR_RETURNER, KR_PROD_CON0, KR_SELF,
		     KR_DOMCRE, KR_BANK, 1);
}

uint32_t
MakeNewProduct(Message *msg)
{
  uint32_t result;
  struct Registers regs;

  DEBUG kdprintf(KR_OSTREAM, "Making new product...\n");

  result = proccre_create_process(KR_DOMCRE, KR_ARG0, KR_NEWDOM);
  if (result != RC_OK)
    return result;

  /* NOTE that if proccre_create_process succeeded, we know it's a good
     space bank. */
  
  /* Build a constiuents node, since we will need the scratch register
     later */
  result = spcbank_buy_nodes(KR_ARG0, 1, KR_SCRATCH, KR_ZERO, KR_ZERO);
  if (result != RC_OK)
    goto return_product;

  /* clone the product constituents into the new constituents node: */
  node_clone(KR_SCRATCH, KR_PROD_CON0);

  /* constituents to product KR 1 */
  (void) process_swap_keyreg(KR_NEWDOM, 1, KR_SCRATCH, KR_ZERO);
  
  DEBUG kdprintf(KR_OSTREAM, "Populate new domain\n");

  (void) node_copy(KR_CONSTIT, KC_PROTOSPC, KR_SCRATCH);

  /* Install protospace into the domain root: */
  (void) process_swap(KR_NEWDOM, ProcAddrSpace, KR_SCRATCH, KR_ZERO);

  DEBUG kdprintf(KR_OSTREAM, "Installed protospace\n");

  /* Install the schedule key into the domain: */
  (void) process_swap(KR_NEWDOM, ProcSched, KR_ARG1, KR_ZERO);
  
  DEBUG kdprintf(KR_OSTREAM, "Installed sched\n");

  /* Fetch out the register values, mostly for the benefit of
     Retrieving the PC -- this prevents us from needing to hard-code
     the PC, which will inevitably change. */
  (void) process_get_regs(KR_NEWDOM, &regs);

  DEBUG kdprintf(KR_OSTREAM, "Got regs\n");

  /* Unless we set them otherwise, the register values are zero.  The
     PC has already been set.  We now need to initialize the stack
     pointer and the segment registers. */
  regs.CS = DOMAIN_CODE_SEG;
  regs.SS = DOMAIN_DATA_SEG;
  regs.DS = DOMAIN_DATA_SEG;
  regs.ES = DOMAIN_DATA_SEG;
  regs.FS = DOMAIN_DATA_SEG;
  regs.GS = DOMAIN_DATA_SEG;
  regs.pc = 0;			/* Place Holder!! */
  regs.EFLAGS = 0x200;
  regs.faultCode = 0;
  regs.faultInfo = 0;
  regs.domState = RS_Waiting;
  regs.domFlags = 0;
  
  /* Set the new register values. */
  (void) process_set_regs(KR_NEWDOM, &regs);

  DEBUG kdprintf(KR_OSTREAM, "Installed program counter\n");

  /* process to KR 2 */
  (void) process_swap_keyreg(KR_NEWDOM, 2, KR_NEWDOM, KR_ZERO);
  /* process creator to KR 3 */
  (void) process_swap_keyreg(KR_NEWDOM, 3, KR_DOMCRE, KR_ZERO);
  /* Bank to KR 4 */
  (void) process_swap_keyreg(KR_NEWDOM, 4, KR_ARG0, KR_ZERO);
  /* Sched to KR 5 */
  (void) process_swap_keyreg(KR_NEWDOM, 5, KR_ARG1, KR_ZERO);

  /* Keeper constructor to KR 6 */
  (void) node_copy(KR_PROD_CON1, Constructor_Product_Kpr - 16, KR_SCRATCH);
  (void) process_swap_keyreg(KR_NEWDOM, 6, KR_SCRATCH, KR_ZERO);

  /* Space constructor to KR 7 */
  (void) node_copy(KR_PROD_CON1, Constructor_Product_Spc - 16, KR_SCRATCH);
  (void) process_swap_keyreg(KR_NEWDOM, 7, KR_SCRATCH, KR_ZERO);

  /* Symtab to DR 3 */
  (void) node_copy(KR_PROD_CON1, Constructor_Product_Sym - 16, KR_SCRATCH);
  (void) process_swap(KR_NEWDOM, ProcSymSpace, KR_SCRATCH, KR_ZERO);

  /* Initial PC to KR 8 */
  (void) node_copy(KR_PROD_CON1, Constructor_Product_PC - 16, KR_SCRATCH);
  (void) process_swap_keyreg(KR_NEWDOM, 8, KR_SCRATCH, KR_ZERO);

#if EROS_NODE_SIZE == 16
  /* User ARG2 to KR 12 */
  (void) process_swap_keyreg(KR_NEWDOM, 12, KR_ARG2, KR_ZERO);

  /* Resume key to KR 15 */
  (void) process_swap_keyreg(KR_NEWDOM, 15, KR_RESUME, KR_ZERO);
#elif EROS_NODE_SIZE == 32
  /* User ARG2 to KR 28 */
  (void) process_swap_keyreg(KR_NEWDOM, 28, KR_ARG2, KR_ZERO);

  /* Resume key to KR 31 */
  (void) process_swap_keyreg(KR_NEWDOM, 31, KR_RESUME, KR_ZERO);
#else
#error "Unknown node size"
#endif

  /* Make up a fault key to the new process so we can set it in motion */

  DEBUG kdprintf(KR_OSTREAM, "About to call get fault key\n");
  (void) process_make_fault_key(KR_NEWDOM, KR_SCRATCH);

  msg->snd_key0 = KR_ZERO;
  msg->snd_key1 = KR_ZERO;
  msg->snd_key2 = KR_ZERO;
  msg->snd_key3 = KR_ZERO;
  msg->snd_code = 0;		/* ordinary restart */
  msg->snd_w1 = 0;
  msg->snd_w2 = 0;
  msg->snd_w3 = 0;
  msg->snd_len = 0;
  msg->snd_invKey = KR_SCRATCH;
  
  return RC_OK;

return_product:
  (void) proccre_destroy_process(KR_DOMCRE, KR_ARG0, KR_NEWDOM);
  return RC_NoMoreNodes;
}

int
is_not_discreet(uint32_t kr, ConstructorInfo *ci)
{
  uint32_t keyInfo;
  
  node_copy(KR_CONSTIT, KC_DISCRIM, KR_SCRATCH);
  
  DEBUG kdprintf(KR_OSTREAM, "constructor: is_not_discreet(): discrim_verify\n");

  if (discrim_verify(KR_SCRATCH, kr))
    return 0;			/* ok */

  DEBUG kdprintf(KR_OSTREAM, "constructor: is_not_discreet(): proccre_amplify\n");
  keyInfo = proccre_amplify_gate(KR_DOMCRE, kr, KR_SCRATCH);
  if (keyInfo == 0) {
    /* This key is a requestor's key to a constructor. Ask the
       constructor if it is discrete */
    if (constructor_is_discreet(kr))
      return 0;			/* ok */
  }

  return 1;
}


/* Someday this should start building up a list of holes... */
void
add_new_hole(uint32_t kr, ConstructorInfo *ci)
{
  ci->has_holes = 1;
}

uint32_t
insert_constituent(uint32_t ndx, uint32_t kr, ConstructorInfo *ci)
{
  DEBUG kdprintf(KR_OSTREAM, "constructor: insert constituent %d\n", ndx);

  if (ndx == 19) {
    /* This copy IS redundant with the one in is_not_discreet(), but
       this path is not performance critical and clarity matters too. */
    node_copy(KR_CONSTIT, KC_DISCRIM, KR_SCRATCH);
  
    if (discrim_classify(KR_SCRATCH, kr) != Discrim_Class_Number)
      return RC_RequestError;
  }

  if (ndx > 19)
    return RC_RequestError;
    
  if ( is_not_discreet(kr, ci) )
    add_new_hole(kr, ci);
  
  /* now insert the constituent in the proper constituents node: */
  node_swap((ndx < 16) ? KR_PROD_CON0 : KR_PROD_CON1,
	    ndx % 16, kr, KR_ZERO);
	    
  return RC_OK;
}

int
ProcessRequest(Message *msg, ConstructorInfo *ci)
{
  
  /*initialize the keys being sent*/
  msg->snd_len = 0;
  msg->snd_key0 = KR_ZERO;
  msg->snd_key1 = KR_ZERO;
  msg->snd_key2 = KR_ZERO;
  msg->snd_key3 = KR_ZERO;
  msg->snd_code = RC_OK;
  msg->snd_w1 = 0;
  msg->snd_w2 = 0;
  msg->snd_w3 = 0;
  msg->snd_invKey = KR_RESUME;

  switch (msg->rcv_code) {
  case OC_Constructor_IsDiscrete:
    {
      if (ci->frozen && !ci->has_holes)
	msg->snd_code = 1;
      else
	msg->snd_code = 0;

      return 1;
    }      
    
  case OC_Constructor_Request:
    {
      DEBUG kdprintf(KR_OSTREAM, "constructor: request product\n");

      msg->snd_code = MakeNewProduct(msg);
      return 1;
    }      
    
  case OC_Constructor_Seal:
    {
      DEBUG kdprintf(KR_OSTREAM, "constructor: seal\n");

      ci->frozen = 1;

      process_make_start_key(KR_SELF, 0, KR_NEWDOM);
      msg->snd_key0 = KR_NEWDOM;

      return 1;
    }      

  case OC_Constructor_Insert:
    {
      msg->snd_code = insert_constituent(msg->rcv_w1, KR_ARG0, ci);
      
      return 1;
    }      

  case OC_Destroy:
    {
      Sepuku();
    }
  
  case KT:			/* check alleged keytype */
    {
      switch(msg->rcv_keyInfo) {
      case 0:
	msg->snd_code = AKT_ConstructorRequestor;
	break;
      case 1:
	msg->snd_code = AKT_ConstructorBuilder;
	break;
      }
      return 1;
    }      

  default:
    break;
  }

  msg->snd_code = RC_UnknownRequest;
  return 1;
}

int
main()
{
  Message msg;
  ConstructorInfo ci;
  
  InitConstructor(&ci);

  /* Recover our own process key from constit1 into slot 1 */
  process_make_start_key(KR_SELF, 1, KR_SCRATCH);

  msg.snd_key0 = KR_SCRATCH;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;
  msg.snd_len = 0;
  msg.snd_data = 0;
  msg.snd_invKey = KR_RESUME;

  msg.rcv_key0 = KR_ARG0;
  msg.rcv_key1 = KR_ARG1;
  msg.rcv_key2 = KR_ARG2;
  msg.rcv_key3 = KR_RESUME;
  msg.rcv_data = 0;
  msg.rcv_len = 0;

  do {
    /* no need to re-initialize rcv_len, since always 0 */
    RETURN(&msg);
  } while (ProcessRequest(&msg, &ci));

  return 0;
}
