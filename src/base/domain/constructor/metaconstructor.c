/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* Metaconstructor builds constructors.  This is a primordial domain,
 * and it is one of the places where the recursion stops.
 *
 * Metaconstructor is considered a constructor by administrative fiat,
 * even though it does not run the constructor code. */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/ReturnerKey.h>
#include <eros/ProcessKey.h>
#include <eros/Key.h>
#include <eros/StdKeyType.h>
#include <eros/ProcessState.h>
#include <eros/NodeKey.h>
#include <eros/NumberKey.h>
#include <domain/domdbg.h>
#include <domain/ConstructorKey.h>
#include <domain/ProcessCreatorKey.h>
#include <domain/SpaceBankKey.h>
#include <domain/PccKey.h>

#define DEBUG if (0)
/* #define DEBUG if (1) */

const uint32_t __rt_stack_pages = 1; /* primordial domain */
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif

#define KR_ZERO     	0
#define KR_CONSTIT  	1
#define KR_SELF         2
#define KR_CON_CONSTIT  3
#define KR_BANK         4
#define KR_RETURNER     5
#define KR_DISCRIM      6
#define KR_DOMCRE       7	/* constructor domain creator */
#define KR_SCRATCH      8	/* constructor domain creator */
#define KR_CON_SEG      9	/* constructor program segment */
#define KR_OSTREAM     10	/* our mouth */
#define KR_NEWDOM      11	/* where new constructor goes */

#if EROS_NODE_SIZE == 16
#define KR_ARG0    12
#define KR_ARG1    13
#define KR_ARG2    14
#define KR_RESUME  15
#elif EROS_NODE_SIZE == 32
#define KR_ARG0    28
#define KR_ARG1    29
#define KR_ARG2    30
#define KR_RESUME  31
#else
#error "Unhandled node size"
#endif

#define KC_DISCRIM     0
#define KC_RETURNER    1
#define KC_OSTREAM     2
#define KC_DOMCRE      3
#define KC_CON_SEG     4
#define KC_CON_PC      5
#define KC_CON_CONSTIT 6

#if 0
#define KR_CON_CONSTIT0 1	/* constructor only */
#define KR_CON_CONSTIT1 2	/* constructor only */
#define KR_NEWSPC 2		/* metaconstructor only */
#define KR_CONSPC  3		/* constructor address space */
#define KR_CON_MYBANK  3	/* constructor address space */
#define KR_DISCRIM  4		/* same as for constructor */
#define KR_RETURNER 5		/* the returner key */
#define KR_SCRATCH  6		/* same as for constructor */
#define KR_OSTREAM  7		/* same as for constructor */
#define KR_PROTOSPC 8		/* same as for constructor */
#define KR_DOMCRE   9		/* same as for constructor */
#define KR_NEWDOM   10		/* same as for constructor */
#define KR_DCC      11		/* metaconstructor only */
#define KR_CON_MYDOMCRE  11	/* constructor only */

#define KR_ARG0    12		/* same as for constructor */
#define KR_ARG1    13		/* same as for constructor */
#define KR_ARG2    14		/* same as for constructor */
#define KR_RESUME  15		/* same as for constructor */
#endif

typedef struct {
  uint32_t constructor_pc;
} MetaConInfo;

/* On startup, entrypt of constructor is in KR_ARG0 */
void
InitMetaCon(MetaConInfo *mci)
{
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  DEBUG kdprintf(KR_OSTREAM, "Metacon inits\n");

  node_copy(KR_CONSTIT, KC_CON_PC, KR_NEWDOM);
  number_get_word(KR_NEWDOM, &mci->constructor_pc);

  node_copy(KR_CONSTIT, KC_DISCRIM, KR_DISCRIM);
  node_copy(KR_CONSTIT, KC_RETURNER, KR_RETURNER);
  node_copy(KR_CONSTIT, KC_DOMCRE, KR_DOMCRE);
  node_copy(KR_CONSTIT, KC_CON_SEG, KR_CON_SEG);
  node_copy(KR_CONSTIT, KC_CON_CONSTIT, KR_CON_CONSTIT);
}

uint32_t
MakeNewProduct(Message *msg, MetaConInfo *mci)
{
  uint32_t result;
  struct Registers regs;

  msg->snd_key0 = KR_ZERO;
  msg->snd_key1 = KR_ZERO;
  msg->snd_key2 = KR_ZERO;
  msg->snd_key3 = KR_ZERO;
  msg->snd_code = 0;		/* ordinary restart */
  msg->snd_w1 = 0;
  msg->snd_w2 = 0;
  msg->snd_w3 = 0;
  msg->snd_len = 0;

  result = proccre_create_process(KR_DOMCRE, KR_ARG0, KR_NEWDOM);
  if (result != RC_OK)
    return result;

  /* NOTE that if proccre_create_process succeeded, we know it's a good
     space bank. */
  
  /* Install the schedule.  KR_ARG1 can be reused after this. */
  (void) process_swap(KR_NEWDOM, ProcSched, KR_ARG1, KR_ZERO);
  (void) process_swap_keyreg(KR_NEWDOM, 5, KR_ARG1, KR_ZERO);

#define KR_ALTSCRATCH KR_ARG1
  /* Build a constiuents node: */
  result = spcbank_buy_nodes(KR_ARG0, 1, KR_ALTSCRATCH, KR_ZERO, KR_ZERO);
  if (result != RC_OK)
    goto return_product;

  /* clone the product constituents into the new constituents node: */
  node_clone(KR_ALTSCRATCH, KR_CON_CONSTIT);
  (void) process_swap_keyreg(KR_NEWDOM, 1, KR_ALTSCRATCH, KR_ZERO);
#undef KR_ALTSCRATCH

  /* Install the address space of the new constructor */
  (void) process_swap(KR_NEWDOM, ProcAddrSpace, KR_CON_SEG, KR_ZERO);

  /* POPULATE KEY REGISTERS */
  
  /* Place the new domain creator in the appropriate key register of
     the new constructor domain. */
  (void) process_swap_keyreg(KR_NEWDOM, KR_SELF, KR_NEWDOM, KR_ZERO);
  (void) process_swap_keyreg(KR_NEWDOM, KR_BANK, KR_ARG0, KR_ZERO);
  (void) process_swap_keyreg(KR_NEWDOM, KR_RESUME, KR_RESUME, KR_ZERO);
  
  /* Fetch out the register values, mostly for the benefit of
     Retrieving the PC -- this prevents us from needing to hard-code
     the PC, which will inevitably change. */
  (void) process_get_regs(KR_NEWDOM, &regs);

  DEBUG kdprintf(KR_OSTREAM, "Got regs\n");

  /* Unless we set them otherwise, the register values are zero.  The
     PC has already been set.  We now need to initialize the stack
     pointer and the segment registers. */
  regs.CS = DOMAIN_CODE_SEG;
  regs.SS = DOMAIN_DATA_SEG;
  regs.DS = DOMAIN_DATA_SEG;
  regs.ES = DOMAIN_DATA_SEG;
  regs.FS = DOMAIN_DATA_SEG;
  regs.GS = DOMAIN_DATA_SEG;
  regs.pc = mci->constructor_pc;
  regs.EFLAGS = 0x200;
  regs.faultCode = 0;
  regs.faultInfo = 0;
  regs.domState = RS_Waiting;
  regs.domFlags = 0;
  
  /* Set the new register values. */
  (void) process_set_regs(KR_NEWDOM, &regs);

  DEBUG kdprintf(KR_OSTREAM, "Wrote regs\n");

  (void) process_make_fault_key(KR_NEWDOM, KR_SCRATCH);

  msg->snd_invKey = KR_SCRATCH;

  return RC_OK;

return_product:
  (void) proccre_create_process(KR_DOMCRE, KR_ARG0, KR_NEWDOM);
  return RC_NoMoreNodes;
}

int
ProcessRequest(Message *msg, MetaConInfo* mci)
{
  /*initialize the keys being sent*/
  msg->snd_len = 0;
  msg->snd_key0 = KR_ZERO;
  msg->snd_key1 = KR_ZERO;
  msg->snd_key2 = KR_ZERO;
  msg->snd_key3 = KR_ZERO;
  msg->snd_code = RC_OK;
  msg->snd_w1 = 0;
  msg->snd_w2 = 0;
  msg->snd_w3 = 0;
  msg->snd_invKey = KR_RESUME;

  switch (msg->rcv_code) {
  case OC_Constructor_IsDiscrete:
    {
      msg->snd_code = 1;	/* answer YES */

      return 1;
    }      
    
  case OC_Constructor_Request:
    {
      msg->snd_code = MakeNewProduct(msg, mci);
      return 1;
    }      
    
  case OC_Constructor_Seal:
    {
      msg->snd_code = 1;	/* answer YES */
      return 1;
    }      

  case KT:			/* check alleged keytype */
    {
      msg->snd_code = AKT_MetaConstructor;
      return 1;
    }      

  default:
    break;
  }

  msg->snd_code = RC_UnknownRequest;
  return 1;
}

int
main()
{
  Message msg;
  MetaConInfo mci;
  
  InitMetaCon(&mci);

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;
  msg.snd_len = 0;
  msg.snd_data = 0;
  msg.snd_invKey = KR_ZERO;

  msg.rcv_key0 = KR_ARG0;
  msg.rcv_key1 = KR_ARG1;
  msg.rcv_key2 = KR_ARG2;
  msg.rcv_key3 = KR_RESUME;
  msg.rcv_data = 0;
  msg.rcv_len = 0;

  do {
    /* no need to re-initialize rcv_len, since always 0 */
    RETURN(&msg);
  } while (ProcessRequest(&msg, &mci));

  return 0;
}
