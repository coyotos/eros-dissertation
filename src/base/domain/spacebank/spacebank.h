/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef SPACEBANK_H
#define SPACEBANK_H

#include <domain/SpaceBankKey.h>

#ifdef NDEBUG
#define TREE_NO_TYPES
#endif

/* Invalid type for internal use */
#define SBOT_INVALID 0xff

extern const char *type_name(int t);
extern bool valid_type(int t);
extern uint32_t objects_per_frame[OT_NUM_TYPES];
extern uint32_t objects_map_mask[OT_NUM_TYPES];

#define kpanic kdprintf

bool heap_insert_page(uint32_t addr, uint32_t pageKR);

#define MAX_RANGES 256

/* Following MUST agree with values in primebank.map: */
#define STACK_TOP           0x100000
#define SRM_BASE           0x1000000
#define HEAP_BASE          0x2000000
#define SB_BRAND_KEYDATA       65535

/* Key registers */
#define KR_ZERO        0

#define KR_CONSTIT     1
#define  KC_DOMTOOL      0      /* Domain tool -- for validating
				   spacebank keys. */

#define KR_VOLSIZE     2

#define KR_TMP3        2        /* once initialization is over with, we
				 * can re-use the volsize node. 
				 */

#define KR_OSTREAM     3        /* only used for debugging */

#define KR_SELF        4        /* Domain key to myself -- for
				   creating start keys and getting my
				   address space. */
#define KR_NU5         5

#define KR_TMP2        6
#define KR_TMP         7

#define KR_WALK0       8        /* reserved for internal use by malloc */
#define KR_WALK1       9        /* reserved for internal use by malloc */

#define KR_SRANGE     10        /* Super Range key -- what fun! */
#define KR_RETURNER   11        /* returner for guarenteeing returns */

#define KR_ARG0       12
#define KR_ARG1       13
#define KR_ARG2       14
#define KR_RESUME     15        /* don't use this for anything but! */

/* NOTE: On startup, KR_ARG0 holds a node key to the prime space bank
   key's node. */

/* kprintf for uint64_ts convienience */
#define DW_HEX "%08x%08x" /* hexadecimal format string -- 16 chars */
#define DW_HEX_ARG(x) (uint32_t)(x >> 32),(uint32_t)(x) /* hex. arg. string*/

#endif /* SPACEBANK_H */

