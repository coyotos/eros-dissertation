/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef BANK_H
#define BANK_H

#include "AllocTree.h"

struct Message;
typedef uint32_t BankPrecludes;

/* note:  There can only be 15 distinct precludes, as all start keys >=
   0x8000 are reserved for other uses. (like the brand!) */

#define BANKPREC_NO_DESTROY       (0x01u)
#define BANKPREC_NO_LIMITMODS     (0x02u)

#define BANKPREC_NUM_PRECLUDES    (0x04u)
/* should be 2 * the maximum limit bit value */


#define BANKPREC_MASK             (0x03u)

#define BANKPREC_CAN_DESTROY(lim)     !((lim) & BANKPREC_NO_DESTROY  )
#define BANKPREC_CAN_MOD_LIMIT(lim)   !((lim) & BANKPREC_NO_LIMITMODS)

struct Bank_MOFrame {
  uint64_t frameOid;
  uint32_t    frameMap;
};

typedef struct Bank Bank;       
struct Bank {
  Bank *parent;
  Bank *nextSibling;
  Bank *firstChild;

  bool  exists[BANKPREC_NUM_PRECLUDES];
  OID   limitedKey[BANKPREC_NUM_PRECLUDES];
                     /* holds the OIDs of the nodes for the various
		      * variants on this bank.
		      */

  TREE allocTree;
  
  uint64_t limit;
  uint64_t allocCount; /* == #allocated frames */

  struct Bank_MOFrame nodeFrame;
#ifdef EROS_PROCS_PER_FRAME
  struct Bank_MOFrame procFrame;
#endif

  uint64_t allocs[OT_NUM_TYPES];
  uint64_t deallocs[OT_NUM_TYPES];
};

extern Bank bank0;		/* The primordial allocate-only bank. */
extern Bank primebank;		/* The prime bank. */
extern Bank verifybank;         /* A zero-limit bank which can only be
				   used to validate banks */

void bank_init(void);
/* bank_init:
 *     Initialized the Bank structures etc.
 */

uint32_t
bank_ReserveFrames(Bank *bank, uint32_t count);
/* bank_ReserveFrames:
 *     Logically, this bumps the number of frames allocated by /bank/
 *   after making sure that that does not cross the limits of this
 *   bank and its parents.
 *
 *     Returns RC_OK on success, -- /count/ frames are reserved, and
 *                                  you are responible for unreserving
 *                                  the frames when done with them.
 *             RC_SB_LimitReached on failure (always due to a
 *                                limit somewhere) -- No frames are resered.
 */

void
bank_UnreserveFrames(Bank *bank, uint32_t count);
/* bank_UnreserveFrames:
 *     Returns /count/ frames to /bank/ -- this should be called any
 *   time a frame is returned to the ObjSpace.  This never fails
 *   (though it will panic if somehow you unreserve more frames than
 *   you reserved.)
 */

uint32_t
BankSetLimits(Bank *bank, const struct bank_limits *newLimits);
/* BankSetLimits:
 */

uint32_t
BankGetLimits(Bank *bank, /*OUT*/ struct bank_limits *getLimit);
/* BankGetLimits:
 */

uint32_t
bank_MarkAllocated(uint32_t type, uint64_t oid);
/* bank_MarkAllocated:
 *     Marks /oid/ as an allocated object of type /type/ in the
 *   PRIMEBANK.
 *
 *     Returns 0 on success,
 *             1 if the object is already marked allocated,
 *             2 on failure
 */

Bank *BankFromInvocation(struct Message *argMsg);
#define BankFromInvocation(argMsg) \
  ( (Bank*) (argmsg)->rcv_w1 )
/* BankFromInvocation:
 *     Given invocation message /argMsg/, recovers pointer to
 *     internal bank data structure.
 *
 *     Cannot fail, because spacebank fabricates the red seg nodes
 *     with proper values at all times. (also, it's a macro)
 */

BankPrecludes PrecludesFromInvocation(struct Message *argMsg);
#define PrecludesFromInvocation(argMsg) \
  ( (BankPrecludes) (argmsg)->rcv_keyInfo )
/* LimitsFromInvocation:
 *     Given invocation message /argMsg/, recovers limits field.
 *
 *     Cannot fail, because spacebank fabricates the red seg nodes
 *     with proper values at all times. (also, it's a macro)
 */


uint32_t
BankAllocObjects(Bank *bank, uint8_t type, uint8_t number, uint32_t kr);
/* BankAllocObjects:
 *     Allocates /number/ objects of type /type/ from /bank/.  Places
 *   the key slots into kr, kr+1, kr+2...
 *
 *     Returns RT_OK on success, error code if some error occurs.
 */

uint32_t
BankDeallocObjects(Bank *bank, uint8_t type, uint8_t number, uint32_t kr);
/* BankDeallocObjects:
 *     Deallocates /number/ objects that were allocated from /bank/.
 *
 *     An object will fail to deallocate if it is not an object key,
 *   it was not allocated from /bank/, or if it is not a strong
 *   (read/write) key to the object. 
 *
 *     The lower /number/ bits of the returned word indicate
 *   success/failure for each of the objects. 
 */

uint32_t
BankIdentifyObjects(Bank *bank, uint8_t type, uint8_t number, uint32_t kr);
/* BankDeallocObjects:
 *     Deallocates /number/ objects that were allocated from /bank/.
 *
 *     An object will fail to deallocate if it is not an object key,
 *   it was not allocated from /bank/, or if it is not a strong
 *   (read/write) key to the object. 
 *
 *     The lower /number/ bits of the returned word indicate
 *   success/failure for each of the objects. 
 */

uint32_t
BankCreateKey(Bank *bank, BankPrecludes limits, uint32_t kr);
/* BankCreateKey:
 *     Creates a key in register /kr/ to /bank/ with /limits/.  Note
 *     that this key names a red segment node.
 *
 *     Returns RC_OK on success.
 */

uint32_t
BankCreateChild(Bank *bank, uint32_t kr);
/* BankCreateChild:
 *     Creates a new child of /bank/, and creates an unrestricted key
 *   to it in /kr/.
 *
 *     Returns RC_OK on success,
 *     Returns RC_SB_LimitReached if the limits on /bank/ preclude
 *   adding a child.
 *     Returns RC_SB_OutOfSpace if there is no more space on disk for
 *   the new child bank.
 */

uint32_t
BankDestroyBankAndStorage(Bank *bank, bool andStorage);
/* BankDestroyBankAndStorage:
 *     Destroys bank and all of its children.  If /andStorage/ is
 *   false, the allocated storage is put into the control of /bank/'s
 *   parent. If /andStorage/ is true, all the storage allocated by
 *   both /bank/ and its children is rescinded.
 *
 *     All keys referencing the destroyed banks are useless, and will
 *   be turned into zero number keys. 
 *
 *     Returns RC_OK on success.
 */

void
BankPreallType(Bank *bank,
	       uint32_t type,
	       uint64_t startOID,
	       uint64_t number);
/* BankPreallType:
 *     Marks /number/ objects of type /type/ starting at /startOID/
 *   (which should be a frame OID) as allocated in /bank/.
 */

#endif /* BANK_H */

