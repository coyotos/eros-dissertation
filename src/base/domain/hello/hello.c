/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* Hello World

   A silly test program that we use often enough that I decided to
   just put it in the domain tree.
   */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/ProcessKey.h>
#include <domain/domdbg.h>
#if 0
#include <eros/ProcessKey.h>
#include <eros/Key.h>
#include <eros/StdKeyType.h>
#endif

#define KR_CONSTIT  1
#define KR_SELF     2
#define KR_BANK     4
#define KR_OSTREAM  5
#define KC_OSTREAM  0

#if EROS_NODE_SIZE == 16
#define KR_ARG0     12
#define KR_ARG1     13
#define KR_ARG2     14
#define KR_ARG3     15
#elif EROS_NODE_SIZE == 32
#define KR_ARG0     28
#define KR_ARG1     29
#define KR_ARG2     30
#define KR_ARG3     31
#endif

const uint32_t __rt_stack_pages = 1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif


int
ProcessRequest(Message *argmsg)
{
  kdprintf(KR_OSTREAM, "hello, world\n");
  
  argmsg->snd_code = RC_OK;
  return 1;
}

void
init_hello()
{
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  kdprintf(KR_OSTREAM, "hello: init\n");
}

int
main()
{
  Message msg;

  init_hello();

  process_make_start_key(KR_SELF, 0, KR_ARG0);
  
  msg.snd_invKey = KR_ARG3;
  msg.snd_key0 = KR_ARG0;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = KR_ARG0;
  msg.rcv_key1 = KR_ARG1;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ARG3;
  msg.rcv_len = 0;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  do {
    RETURN(&msg);
    msg.snd_key0 = KR_ARG0;	/* until proven otherwise */
    msg.snd_invKey = KR_ARG3;
  } while ( ProcessRequest(&msg) );

  return 0;
}
