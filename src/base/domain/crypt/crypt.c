/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Encrypt -- domain that sends back encrypted what you send it.
 *
 * Key Registers:
 *
 * KR12: arg0
 * KR13: arg1
 * KR14: arg2
 * KR15: arg3
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include "libc.h"
#include "des_crypt.h"

#define KR_ZERO 0
#define KR_LOGKEY 9

void
Request(Message *msg)
{
  
  uint8_t *data = (uint8_t *) 0x10000;	/* MUST match value in test.imgmap */
  uint8_t key[8];  /* 8 byte key to encrypt with */
  uint8_t *pointer;
  unsigned datalen;  /* length of the data */
  int loop;
  uint8_t ivec[8];
  
  switch (msg->rcv_code)
    {
    case 1: /* set key */
      pointer = msg->rcv_data;
      for(loop=0;loop<8;loop++){
	key[loop] = *pointer;
	pointer++;
      }
      break;
    case 2: /* encrypt */
      datalen=msg->rcv_len;
      strcpy(msg->rcv_data,data);
      pointer=&data[datalen];
      for(loop=0;loop<((8-(datalen%8))%8);loop++){
	*pointer=' ';
	pointer++;
      }
      for(loop=0;loop<8;loop++){ /* init ivec */
	ivec[loop]=0;
      }
      des_setparity(key);
      cbc_crypt(key, data, datalen+((8-(datalen%8))%8), DES_ENCRYPT, ivec);
      msg->snd_len = datalen+((8-(datalen%8))%8);
      msg->snd_data = (uint8_t *) data;
      break;
    case 3: /* decrypt */
      datalen=msg->rcv_len;
      data=msg->rcv_data;
      for(loop=0;loop<8;loop++){ /* init ivec */
	ivec[loop]=0;
      }
      des_setparity(key);
      cbc_crypt(key, data,datalen+(8-(datalen%8)),DES_DECRYPT, ivec);
      msg->snd_len = datalen+((8-(datalen%8))%8);
      msg->snd_data = (uint8_t *) data;
      break;
    case 4: /* clear key */
      for(loop=0;loop<8;loop++){
	key[loop] = 0;
	pointer++;
      }
      break;
    }
  
  msg->snd_key0 = KR_ZERO;
  msg->snd_key1 = KR_ZERO;
  msg->snd_key2 = KR_ZERO;
  msg->snd_key3 = KR_ZERO;

  /* leave receive info unchanged, but reset the length limit: */
  msg->rcv_len = DES_MAXDATA;
       
  /*  return orderCode + 1; */
  msg->snd_code = msg->rcv_code +1;
}
