/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <domain/domdbg.h>

#define KR_ZERO 0
#define KR_ECHO 8
#define KR_LOGKEY 9
#define KR_OSTREAM 7

const char *str = "Hello from call.c\n";

uint32_t __rt_stack_pages = 0;
uint32_t __rt_stack_pointer = 0x2100;

void
main()
{
  Message msg;

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = (uint8_t *) str;
  msg.snd_len = sizeof("Hello from call.c\n") - 1; /* eat trailing null! */

  msg.rcv_key0 = 12;
  msg.rcv_key1 = 13;
  msg.rcv_key2 = 14;
  msg.rcv_key3 = 15;
  msg.rcv_len = 0;		/* no data returned */

  msg.snd_code = 1;
  msg.snd_invKey = KR_ECHO;
  (void) CALL(&msg);
  msg.snd_code = 1;
  msg.snd_invKey = KR_LOGKEY;
  (void) CALL(&msg);

  /*
   * Test the segment keeper
   */
  
  kprintf(KR_OSTREAM, "Call domain: Now I'm going to need the segment keeper. ");
  *((uint32_t *) 0x30000) = 0;	/* Way out-of-bounds */
#if 0
  Write (&str[30000]);		/* Way out-of-bounds */
#endif
}
