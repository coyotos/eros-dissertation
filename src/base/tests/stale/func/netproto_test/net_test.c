/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <domain/ConstructorKey.h>
#include <domain/domdbg.h>

#define KR_CONSTIT    1
#define KR_SELF       2
#define KR_BANK       4
#define KR_SCHED      5
#define KR_OSTREAM    6
#define KR_NETPROTOC  7
#define KR_NETPROTO   8

#define KC_NETPROTOC   0
#define KC_OSTREAM     1

const uint32_t __rt_stack_pages = 1;
const uint32_t __rt_stack_pointer = 0x20000;

void
main()
{
  uint32_t result;
  
  node_copy(KR_CONSTIT, KC_NETPROTOC, KR_NETPROTOC);
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  
  kdprintf(KR_OSTREAM, "Net test starts\n");
  
  result = constructor_is_discreet( KR_NETPROTOC );
  
  kdprintf(KR_OSTREAM, "netprotoc alleges discretion? 0x%08x\n", result);
  
  result = constructor_request(KR_NETPROTOC, KR_BANK, KR_SCHED, KR_ZERO,
			 KR_NETPROTO);


  kdprintf(KR_OSTREAM, "Got product? 0x%08x\n", result);

#if 0
  {
    Message msg;
    msg.snd_key0 = KR_ZERO;
    msg.snd_key1 = KR_ZERO;
    msg.snd_key2 = KR_ZERO;
    msg.snd_key3 = KR_ZERO;
    msg.snd_len = 0;
    msg.snd_code = 0;
    msg.snd_invKey = KR_NEW_HELLO;
    
    msg.rcv_key0 = KR_ZERO;
    msg.rcv_key1 = KR_ZERO;
    msg.rcv_key2 = KR_ZERO;
    msg.rcv_key3 = KR_ZERO;
    msg.snd_len = 0;

    CALL(&msg);
  }
  
  key_destroy(KR_NEWCON);
#endif
}
