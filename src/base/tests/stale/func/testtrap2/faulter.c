/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/SleepKey.h>
#include <domain/domdbg.h>

/*
 * a simple program to simulate a syscall trap
 */

#define KR_ZERO 0
#define KR_OSTREAM 1
#define KR_SLEEPKEY 2
const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x21000;

void
main ()
{
  sl_sleep(KR_SLEEPKEY, 4000);	/* sleep 4 secs */
  
  kprintf(KR_OSTREAM, "I am about to fault by doing a linux syscall...\n");

  /* for (i=0;i<10;i++) {
    if (i%2==1) {
    __asm__ __volatile__ ("movl   $0x88,%eax\n\t");  
    __asm__ __volatile__ ("movl   $0x1,%ebx\n\t");
    __asm__ __volatile__ ("movl   $0x2,%ecx\n\t");
    __asm__ __volatile__ ("movl   $0x3,%edx\n\t");
    __asm__ __volatile__ ("int    $0x80\n\t");
      
    } else {

      __asm__ __volatile__ ("movl   $0x1,%eax\n\t");  
      __asm__ __volatile__ ("movl   $0x1,%ebx\n\t");
      __asm__ __volatile__ ("movl   $0x2,%ecx\n\t");
      __asm__ __volatile__ ("movl   $0x3,%edx\n\t");
      }
    }
    */
  __asm__ __volatile__ ("movl   $0x88,%eax\n\t");  
  __asm__ __volatile__ ("movl   $0x1,%ebx\n\t");
  __asm__ __volatile__ ("movl   $0x2,%ecx\n\t");
  __asm__ __volatile__ ("movl   $0x3,%edx\n\t");
  __asm__ __volatile__ ("int    $0x80\n\t");
}


