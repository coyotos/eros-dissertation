/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * el3_inet.c - all the inet stuff that won't compile in the same file
 * as any of the eros stuff
 */

#define _KERNEL
#include <sys/types.h>
#include <sys/sockaddr.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/if_ether.h>
#include <netinet/ip_icmp.h>

#define ETHER_ADDR_HALIFAX	{ 0x00, 0x60, 0x97, 0x37, 0x91, 0xd4 }
#define ETHER_ADDR_COLUMBIA	{ 0x00, 0x60, 0x97, 0x2c, 0xf4, 0x22 }
#define ETHER_ADDR_DUCHAMP	{ 0x00, 0xA0, 0x24, 0x23, 0x7D, 0x38 }
#define ETHER_ADDR_MOOCOW	{ 0x00, 0x00, 0xC0, 0x71, 0xE8, 0xE7 }
#define ETHER_ADDR_ZERO		{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

#undef HTONS
#define HTONS(x) (((u_short)(x)>>8) | ((x) & 0x00FF)<<8)
#undef HTONL
#define HTONL(x) (HTONS((u_long)(x)>>16) | HTONS((u_long)(x) & 0x00FFFF)<<16)
#undef NTOHS
#define NTOHS(x) HTONS(x)

#define IP_ADDR_MOOCOW		{ HTONL(0x9E82068C) }
#define IP_ADDR_DUCHAMP		{ HTONL(0x9E820682) }
#define LEN_ICMP_HDR		16
#define LEN_ICMP_DATA		48
#define LEN_ICMP		(LEN_ICMP_HDR+LEN_ICMP_DATA)
#define LEN_IP			(sizeof(struct ip)+LEN_ICMP)

void GetEnetAddr(u_int8_t enetAddrArray[]);
void kprintf(u_int32_t streamkey, const char *fmt, ...);
#define	KR_CONSOLEKEY_SLOT	9

struct ether_addr myEtherAddr;

void InetInit(void)
{
  GetEnetAddr(myEtherAddr.ether_addr_octet);
}

void bcopy(const void *from, void *to, int n)
{
  u_char *pFrom=(void *)from,
    *pTo=to;

  while (n--)
    *pTo++=*pFrom++;
}

u_short ip_cksum(u_short *buf, int len)
{
  u_long cksum=0;

  if ((u_long)buf & 0x01)
    {
      cksum=(*(u_char *)buf)<<8;
      buf=(u_short *)((caddr_t)buf+1);
      len--;
    }
  while (len>1)
    {
      cksum+=*buf++;
      if (cksum & 0x10000)
	cksum=(cksum & 0xFFFF)+1;
      len-=2;
    }
  if (len)
    {
      cksum+=*(u_char *)buf;
      if (cksum & 0x10000)
	cksum=(cksum & 0xFFFF)+1;
    }
  return ~cksum;
}

static IpIdNext;

int PingPkt(u_int8_t *pPkt, u_int32_t len)
{
  struct ether_header *h_ether=(struct ether_header *)pPkt;
  struct ip *h_ip=(struct ip *)(pPkt+sizeof(struct ether_header));
  struct icmp *h_icmp=(struct icmp *)(((caddr_t)h_ip)+(h_ip->ip_hl<<2));
  struct in_addr tmp;

  if (!(h_ether->ether_type==HTONS(ETHERTYPE_IP) &&
	len>=LEN_IP &&
	h_ip->ip_p==IPPROTO_ICMP &&
	h_icmp->icmp_type==ICMP_ECHO))
    return -1;

  bcopy(h_ether->ether_shost, h_ether->ether_dhost, sizeof(struct ether_addr));
  bcopy(&myEtherAddr, h_ether->ether_shost, sizeof(struct ether_addr));

  if (ip_cksum((u_short *)h_ip, h_ip->ip_hl<<2))
    return -2;

  h_ip->ip_sum=0;
  tmp=h_ip->ip_dst;
  h_ip->ip_dst=h_ip->ip_src;
  h_ip->ip_src=tmp;
  h_ip->ip_id=IpIdNext++;
  h_ip->ip_sum=ip_cksum((u_short *)h_ip, h_ip->ip_hl<<2);

  h_icmp->icmp_cksum=0;
  h_icmp->icmp_type=ICMP_ECHOREPLY;
  h_icmp->icmp_cksum=ip_cksum((u_short *)h_icmp,
			      NTOHS(h_ip->ip_len)-(h_ip->ip_hl<<2));

  kprintf(KR_CONSOLEKEY_SLOT, "Build reply\n");
  return 0;
}
