/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include	<eros/machine/target.h>
#include	<eros/Invoke.h>
#include	<eros/Device.h>
#include	<eros/KeyBitsKey.h>
#include	<domdbg/domdbg.h>
#include	<eros/DevCreKey.h>
#include	<eros/NetInterfaceKey.h>

#define	KR_ZERO			0
#define	KR_KEYBITS		7	/* temporary, for debugging */
#define	KR_EL3_SLOT		8
#define	KR_CONSOLEKEY_SLOT	9
#define	KR_NETCREATOR_SLOT	10
#define	KR_LOGKEY_SLOT	        11

char	testInvokeMsg[] = "This is the EROS ping reflector";

void InetInit(void);
int PingPkt(uint8_t *pPkt, uint32_t len);

void
WriteHexData(const char *s, uint32_t len)
{
  static char hexdigits[16] = {
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
  };
  static char buffer[4096];
  Message msg;
  char *bufnext = buffer;
  
  while (len--)
    {
      char c = *s++;

      *(bufnext++) = hexdigits[(c >> 4) & 0xfu];
      *(bufnext++) = hexdigits[c & 0xfu];
    }

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_invKey=KR_LOGKEY_SLOT;
  msg.snd_code=1;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;

  msg.snd_data = buffer;
  msg.snd_len = (bufnext - buffer);

  msg.rcv_len = 0;		/* no data returned */

  (void) CALL(&msg);
}

void
GetELNK3Key()
{
 devcre_make_instance(KR_NETCREATOR_SLOT, 2, KR_EL3_SLOT);
}

#if 0
void
TestInvocation()
{
  Message msg;

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_invKey=KR_EL3_SLOT;
  msg.snd_code=OC_CHK_INVOKE;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;

  msg.snd_len = sizeof(testInvokeMsg);
  msg.snd_data = testInvokeMsg;
  msg.rcv_len = 0;
  msg.rcv_data = NULL;

  CALL(&msg);
}
#endif

void
GetEnetAddr(uint8_t enetAddrArray[])
{
  int i;
  ni_info_t info;

  ni_get_info(KR_EL3_SLOT, &info);

  for (i = 0; i < 6; i++)
    enetAddrArray[i] = info.niAddr[i];
}

void
WritePkt(uint8_t *pkt, uint32_t len)
{
  uint32_t result = ni_write_packet(KR_EL3_SLOT, pkt, len);
  kprintf(KR_CONSOLEKEY_SLOT, "Wrote 0x%08x:%d result=0x%x\n",
	  pkt, len, result);
}

void
ReadPkt(uint8_t *pkt, uint32_t *pLen)
{
  ni_read_packet(KR_EL3_SLOT, pkt, pLen);
}

void bcopy(const void *from, void *to, int n);

#define MAX_PKT_LEN 2048

void
Startup()
{
  uint8_t          pktData[MAX_PKT_LEN];
  Word	        pktLen;
  //int           i;            

  ShowKey(KR_CONSOLEKEY_SLOT, KR_KEYBITS, KR_NETCREATOR_SLOT);

  /* Obtain key for ELNK3. */
  GetELNK3Key();

  ShowKey(KR_CONSOLEKEY_SLOT, KR_KEYBITS, KR_EL3_SLOT);

#if 0
  /* Chk invocation */
  TestInvocation();
#endif

  {
    /* shap added this for home testing so he knows what to say to
       'arp -s' */
    
    uint8_t enetAddr[6];
    GetEnetAddr(enetAddr);
    kprintf(KR_CONSOLEKEY_SLOT,
	    "My enet addr: %02x:%02x:%02x:%02x:%02x:%02x\n",
	    enetAddr[0],
	    enetAddr[1],
	    enetAddr[2],
	    enetAddr[3],
	    enetAddr[4],
	    enetAddr[5]);
  }

  /* Initialise inet code */
  InetInit();

  for ( ; ; ) {
      /* Read a packet */
      pktLen=MAX_PKT_LEN;
      ReadPkt(pktData, &pktLen);
      //kprintf(KR_CONSOLEKEY_SLOT, "Rcv pkt len was %d\n", pktLen);

      /* Process packet */
      if (!PingPkt(pktData, pktLen))
	{
	  /* Print packet data for programmer verification. */
	  kprintf(KR_CONSOLEKEY_SLOT, "Sending packet:\n");
	  WriteHexData(pktData, pktLen);

	  /* Write packet */
	  WritePkt(pktData, pktLen);
	}
    }
}
