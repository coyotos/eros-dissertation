/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>
#include <domain/SpaceBankKey.h>
#include <domain/domdbg.h>
#include <eros/SleepKey.h>


/*
 * a simple domain to test drive the dbvol utility
 *
 */

#define KR_ZERO 0
#define KR_CONSOLEKEY 8
#define KR_SLEEPKEY 9
#define KR_TTY 7
#define KR_LINEDISC 6
#define KR_TESTSEG 4
#define KR_DBVOL 5
#define KR_KEYBITS 10
#define KR_BANK 11

#define KR_TMP0 1
#define KR_TMP1 2


#define KR_RK0     12
#define KR_RK1     13
#define KR_RK2     14
#define KR_RETURN  15


				/* from linedisc.h */
#define LD_READ      0		/* data == numchars */
#define LD_WRITE     1		/* data == chars */


#define SLEEP_TME  (1000 * 2) /* 2 secs */

void
bcopy(const void *from, void *to, uint32_t len)
{
     uint8_t *fp = (uint8_t *) from;
     uint8_t *tp = (uint8_t *) to;
     
     while(len--)
	  *tp++ = *fp++;
}

void
fork_tty ()
{

     /*
      * start a new thread in TTY
      */

     Message msg;
     msg.snd_key0 = KR_ZERO;
     msg.snd_key1 = KR_ZERO;
     msg.snd_key2 = KR_ZERO;
     msg.snd_key3 = KR_ZERO;
     msg.snd_len = 0;
     msg.rcv_key0 = KR_ZERO;
     msg.rcv_key1 = KR_ZERO;
     msg.rcv_key2 = KR_ZERO;
     msg.rcv_key3 = KR_ZERO;
     msg.rcv_len = 0;
  
     msg.snd_invKey = KR_TTY;
     msg.snd_code = 0;


#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: FORK TTY\n");
#endif

     (void) SEND(&msg);


#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: back from FORK TTY\n");
#endif
}

uint32_t
read (char *buf, uint32_t rdbytes)		
{

     Message msg;

     msg.snd_key0 = KR_ZERO;
     msg.snd_key1 = KR_ZERO;
     msg.snd_key2 = KR_ZERO;
     msg.snd_key3 = KR_ZERO;
     msg.snd_data = &rdbytes;
     msg.snd_len = sizeof(rdbytes);

     msg.rcv_key0 = KR_ZERO;
     msg.rcv_key1 = KR_ZERO;
     msg.rcv_key2 = KR_ZERO;
     msg.rcv_key3 = KR_ZERO;

     msg.rcv_len = rdbytes * sizeof(char);
     msg.rcv_data = buf;


     msg.snd_invKey = KR_LINEDISC;
     msg.snd_code = LD_READ;

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: CALL linedisc for READ\n");
#endif
     CALL(&msg);

     /*
       if (CALL(&msg) != RC_OK)
       return 0;
       */

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: back from CALL linedisc for READ code %x\n", msg.rcv_code);
#endif

     return 1;

}



uint32_t
write (char *buf, uint32_t rdbytes)		
{

     Message msg;

     msg.snd_key0 = KR_ZERO;
     msg.snd_key1 = KR_ZERO;
     msg.snd_key2 = KR_ZERO;
     msg.snd_key3 = KR_ZERO;
     msg.snd_data = buf;
     msg.snd_len = rdbytes * sizeof(char);

     msg.rcv_len = 0;
     msg.rcv_key0 = KR_ZERO;
     msg.rcv_key1 = KR_ZERO;
     msg.rcv_key2 = KR_ZERO;
     msg.rcv_key3 = KR_ZERO;

     msg.snd_invKey = KR_LINEDISC;
     msg.snd_code = LD_WRITE;

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: CALL linedisc for WRITE\n");
#endif
     if (CALL(&msg) != RC_OK)
	  return 0;
#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: back from CALL linedisc for WRITE\n");
#endif
     return rdbytes;
}


void
dbvol (uint32_t kr_dbvol, uint32_t kr_linedisc, uint32_t kr_nodepass)		
{
  /*
   * call dbvol
   */

     Message msg;

     msg.snd_key0 = kr_linedisc;
     msg.snd_key1 = kr_nodepass;
     msg.snd_key2 = KR_ZERO;
     msg.snd_key3 = KR_ZERO;	/* send a test page */
     msg.snd_data = 0;
     msg.snd_len = 0;

     msg.rcv_len = 0;
     msg.rcv_key0 = KR_ZERO;
     msg.rcv_key1 = KR_ZERO;
     msg.rcv_key2 = KR_ZERO;
     msg.rcv_key3 = KR_ZERO;

     msg.snd_invKey = kr_dbvol;
     msg.snd_code = 1;

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: CALL dbvol\n");
#endif
     if (CALL(&msg) != RC_OK)
	  return;
#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: back from CALL dbvol\n");
#endif
     return;
}



void
main ()
{
     /*
      * Simple little single character echo test case; infinite
      * loop of READ/WRITE 
      */

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: starting...\n");
#endif     

#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: sleeping...\n");
#endif     
     
#if 1
     /* just sleep for a bit so we can get our bearings */
     sl_sleep(KR_SLEEPKEY, SLEEP_TME); 
#endif

     /*
      * now that we've forked the tty, we can
      * pass control over to dbvol
      */

     
#ifdef DEBUG
     kprintf (KR_CONSOLEKEY, "dbvoltest: calling dbvol...\n");
#endif     
     
     dbvol(KR_DBVOL, KR_LINEDISC, KR_TESTSEG);
     return;

}
