/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>
#include <domain/SpaceBankKey.h>
#include <domain/ConstructorKey.h>
#include <domain/domdbg.h>
#include <domain/FileKey.h>

#include <eros/SleepKey.h>

#include "md5.h"

const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x201000;


/*
 * a simple domain to test drive the file system
 *
 * we map a pre-existing segment with 64k of test data
 * into our address space, then write some of it to the new
 * file; we then read it back and see how everything worked.
 *
 */

#define KR_ZERO       0
#define KR_TMP0       1
#define KR_SELF       2
#define KR_TMP1       3
#define KR_BANK	      4
#define KR_SCHED      5
#define KR_F0         6
#define KR_ADDR       7
#define KR_OSTREAM    8
#define KR_SLEEPKEY   9
#define KR_KEYBITS   10
#define KR_TESTDAT   11

#define KR_RK0       12
#define KR_RK1       13
#define KR_RK2       14
#define KR_RETURN    15

#define TESTMAPSLOT   8
				/* from linedisc.h */
#define LD_READ      0		/* data == numchars */
#define LD_WRITE     1		/* data == chars */


#define SLEEP_TME  (1000 * 2) /* 2 secs */

static uint8_t rcvData[EROS_PAGE_SIZE];

#define xtoa(x) ((x)>9 ? (x)-10+'A' : (x)+'0')

void
main()
{
  int i;
  int j;
  int pgs;
  static int sizes[4] = { 1, 2, 8, 17 };
  int whichsz;
  uint32_t result;
     
  uint32_t x;
  
#if 0
  uint32_t blss = 5;		/* this we hardcode, because it's just
				   a test and we know how big the test
				   data file will be */

  uint32_t offset = TESTMAPSLOT << (blss << 2);
  char *addr = (char *) offset;
     

  /* map the test data into our space at slot TESTMAPSLOT */
     
  node_swap(KR_ADDR, TESTMAPSLOT, KR_TESTDAT, KR_ZERO); 
     
  kdprintf (KR_OSTREAM, "FSTEST:START; testdata mapped at 0x%x\n",
	    offset);
#endif

  kdprintf (KR_OSTREAM, "FSTEST:START; build file...\n");

  /* Build the file object */
  if ( constructor_request(KR_F0, KR_BANK, KR_SCHED, KR_ZERO, KR_F0)
       != RC_OK ) {
    kdprintf(KR_OSTREAM, "Could not create file\n");
    return;
  }
  
  kdprintf (KR_OSTREAM, "FSTEST:START; file built\n");

#define UNIT 1024
  
  for (whichsz = 0; whichsz < 4; whichsz++) {
    static md5_ctx_t	md5_ctx;

    uint8_t	md5_res[16];
    uint8_t	md5_str_in[33];
    uint8_t	md5_str_out[33];

    pgs = sizes[whichsz];
    
    /* Initialise the MD5 context structures. */
    md5_init_ctx(&md5_ctx);

    kprintf (KR_OSTREAM, "FSTEST: build %d page file\n", pgs);

    x = 0;

    for (i = 0; i < pgs; i++) {
      uint32_t outLen;
      
      for (j = 0; j < EROS_PAGE_SIZE; j+= 4)
	*((uint32_t *) &rcvData[j]) = x++;

      /* Calculate MD5 checksum of page. */
      for (j = 0; j < EROS_PAGE_SIZE; j += 64)
	md5_process_block(&rcvData[j], 64, &md5_ctx);
    
      kprintf (KR_OSTREAM, "FSTEST: calling FILE for WRITE\n");

      result = file_write(KR_F0, i * EROS_PAGE_SIZE, EROS_PAGE_SIZE,
			  rcvData, &outLen); 

      if (result != RC_OK)
	kdprintf(KR_OSTREAM, "FSTEST: Write of page %d failed\n", i);
    }

    {
      md5_finish_ctx(&md5_ctx, md5_res);

      for (i=0; i<16; i++)
	{
	  md5_str_in[i*2]=xtoa(md5_res[i]>>4);
	  md5_str_in[i*2+1]=xtoa(md5_res[i] & 0x0F);
	}
      md5_str_in[32]='\0';
    }

    /* Initialise the MD5 context structures. */
    md5_init_ctx(&md5_ctx);

    kprintf (KR_OSTREAM, "FSTEST: read %d page file\n", pgs);

    for (i = 0; i < pgs; i++) {
      uint32_t outLen;
      
      /* Fill the buffer with garbage before reading: */
      for (j = 0; j < EROS_PAGE_SIZE; j+= 4)
	*((uint32_t *) &rcvData[j]) = UINT32_MAX;

      result = file_read(KR_F0, i * EROS_PAGE_SIZE, EROS_PAGE_SIZE,
			 rcvData, &outLen); 

      if (result != RC_OK)
	kprintf(KR_OSTREAM, "FSTEST: Read of page %d failed\n", i);
      else
	kprintf(KR_OSTREAM,
		"FSTEST: Page %d: start: 0x%08x 0x%08x 0x%08x 0x%08x... \n"
		"                 end:   ...0x%08x 0x%08x 0x%08x 0x%08x\n",
		i,
		* ((uint32_t *) &rcvData[0]),
		* ((uint32_t *) &rcvData[4]),
		* ((uint32_t *) &rcvData[8]),
		* ((uint32_t *) &rcvData[12]),
		* ((uint32_t *) &rcvData[4080]),
		* ((uint32_t *) &rcvData[4084]),
		* ((uint32_t *) &rcvData[4088]),
		* ((uint32_t *) &rcvData[4092])
		);

      /* Calculate MD5 checksum of this page. */
      for (j = 0; j < EROS_PAGE_SIZE; j += 64)
	md5_process_block(&rcvData[j], 64, &md5_ctx);
    
    }

    {
      md5_finish_ctx(&md5_ctx, md5_res);

      for (i=0; i<16; i++)
	{
	  md5_str_out[i*2]=xtoa(md5_res[i]>>4);
	  md5_str_out[i*2+1]=xtoa(md5_res[i] & 0x0F);
	}
      md5_str_out[32]='\0';
    }

    kdprintf(KR_OSTREAM,
	     "FSTEST: %d page file results:\n"
	     "  in:  %s\n"
	     "  out: %s\n", pgs, md5_str_in, md5_str_out);

  }
}
