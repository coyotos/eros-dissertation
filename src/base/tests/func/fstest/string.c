/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * string.c - string and memory manipulation functions.
 */

#include <eros/target.h>

#include "string.h"

void
bcopy(const void       *from,
      void	       *to,
      size_t		size)
  {
  uint8_t	       *p1=(void *)from,
	       *p2=to;

  while (size--)
    *p2++=*p1++;
  }

void
bzero(void     *addr,
      size_t	size)
  {
  uint8_t	       *p=addr;

  while (size--)
    *p++=0;
  }


int
bcmp(const void        *x,
     const void	       *y,
     size_t		size)
  {
  uint8_t	       *p1=(void *)x,
	       *p2=(void *)y;

  while (size--)
    if (*p1++!=*p2++)
      return -1;

  return 0;
  }

void *
memcpy(void	       *to,
       const void      *from,
       size_t   	size)
  {
  bcopy(from, to, size);
  return to;
  }

int
strlen(const char      *x)
  {
  int	len=0;

  while (*x++)
    len++;

  return len;
  }

char *
strcpy(char	       *to,
       const char      *from)
  {
  char *start=to;

  while (*from)
    *to++=*from++;
  *to='\0';

  return start;
  }

char *
strncpy(char	       *to,
	const char     *from,
	size_t		size)
  {
  char *start=to;

  while (*from && size)
    {
    *to++=*from++;
    size--;
    }
  if (size)
    *to='\0';

  return start;
  }

int
strcmp(const char      *c1,
       const char      *c2)
  {
  while (*c1)
    {
    if (!*c2 || *c1>*c2)
      return 1;
    if (*c1<*c2)
      return -1;
    c1++;
    c2++;
    }
  return *c2 ? -1 : 0;
  }
