/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * string.h - string and memory manipulation functions.
 */

#ifndef _STRING_H
#define _STRING_H

#include <eros/target.h>

void
bcopy(const void       *from,
      void	       *to,
      size_t		size);

void
bzero(void     *addr,
      size_t	size);

int
bcmp(const void        *x,
     const void	       *y,
     size_t		size);

void *
memcpy(void	       *to,
       const void      *from,
       size_t   	size);

int
strlen(const char      *x);

char *
strcpy(char	       *to,
       const char      *from);

char *
strncpy(char	       *to,
	const char     *from,
	size_t		size);

int
strcmp(const char      *c1,
       const char      *c2);

#endif
