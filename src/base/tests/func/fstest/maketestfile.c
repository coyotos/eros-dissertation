/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * maketestfile.c
 *
 * cheesy little program to create
 * a test data file with contents
 * that approximate their offset
 * in the file.
 */

#include<stdio.h>

#define LINE     256

int
main (int argc, char **argv)
{
     
     char filename[LINE];
     char str[LINE];
     FILE *fp;
     int size;
     int x = 0;
     
     if (argc != 3)
     {
	  printf ("usage: %s filename size\n",
		  argv[0]);
	  exit ( 2 );
     }
     
     strncpy (filename, argv[1], LINE);
     size = atoi(argv[2]);

     fp = fopen (filename, "w");
     if (!fp)
     {
	  printf ("file open failed\n");
	  exit ( 2 );
     }
     
     while (x < size)
     {
	  sprintf (str, "0x%-3.3x ", x);
	  x += strlen(str);
	  fputs (str, fp);
     }

     return 0;
}

      
  
