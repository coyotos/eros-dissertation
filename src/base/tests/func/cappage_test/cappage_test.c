/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* This is a pretty minimal test; it covers the prepare logic for the
   cappage key, dirties a cappage, and then forces a checkpoint.  It's
   really a test for the kernel.

   At some later time I shall extend it to cover cappage store and
   cappage load. */
   
#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/Key.h>
#include <eros/NodeKey.h>
#include <eros/ProcessKey.h>
#include <eros/PageKey.h>
#include <eros/CkptKey.h>
#include <eros/machine/cap-instr.h>
#include <domain/ConstructorKey.h>
#include <domain/domdbg.h>

#define KR_CONSTIT 1
#define KR_SELF    2
#define KR_BANK    4
#define KR_SCHED   5
#define KR_OSTREAM 16
#define KR_CP0     17
#define KR_CP1     18
#define KR_CKPT    19
#define KR_KEYBITS 20

#define dbg_init    0x1
#define dbg_test    0x2

/* Following should be an OR of some of the above */
#define dbg_flags   ( 0u )

/* This is truly sleazy -- it turns into one of:

   (kprintf) (args... )
   (kdprintf) (args... )

   according to the test result */

#define KPRINTF(x) ( (dbg_##x & dbg_flags) ? kdprintf : kprintf )

const uint32_t __rt_stack_pages = 1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif

void *cap_page = (void *) 0x8000;

void
main()
{
  uint32_t result;
  
  /* Test key preparation: */
  KPRINTF(test)(KR_OSTREAM, "Prepare CP0:\n");
  (void) key_kt(KR_CP0);

  KPRINTF(test)(KR_OSTREAM, "Prepare CP1:\n");
  (void) key_kt(KR_CP1);

  /* Test cappage dirty operation: */
  KPRINTF(test)(KR_OSTREAM, "Clone cp1 to cp0:\n");
  result = page_clone(KR_CP0, KR_CP1);

  /* Force a checkpoint */
  KPRINTF(test)(KR_OSTREAM, "Force a checkpoint:\n");
  result = ckpt_TakeCheckpoint(KR_CKPT);
  
  KPRINTF(test)(KR_OSTREAM, "result was 0x%08x\n", result);

  /* Wait for checkpoint to complete */
  KPRINTF(test)(KR_OSTREAM, "Wait for migration to complete:\n");

  do {
    result = ckpt_ProcessMigration(KR_CKPT);
  } while (result != RC_OK);

  KPRINTF(test)(KR_OSTREAM, "result was 0x%08x\n", result);
  
  KPRINTF(test)(KR_OSTREAM, "Testing COPY_KEYREG.  Before:\n");
  
  ShowKey(KR_OSTREAM, KR_KEYBITS, 21);
  ShowKey(KR_OSTREAM, KR_KEYBITS, 22);

  COPY_KEYREG(21, 22);
  COPY_KEYREG(0, 21);

  KPRINTF(test)(KR_OSTREAM, "After:\n");

  ShowKey(KR_OSTREAM, KR_KEYBITS, 21);
  ShowKey(KR_OSTREAM, KR_KEYBITS, 22);

  KPRINTF(test)(KR_OSTREAM, "After exchanging those keys:\n");
  
  XCHG_KEYREG(21, 22);

  ShowKey(KR_OSTREAM, KR_KEYBITS, 21);
  ShowKey(KR_OSTREAM, KR_KEYBITS, 22);

  KPRINTF(test)(KR_OSTREAM, "Trying capability store...\n");
  STORE_KEYREG(21, cap_page);
  
  kdprintf(KR_OSTREAM, "Capability now allegedly at 0x%x\n",
	   cap_page);
  
  COPY_KEYREG(22, 21);

  KPRINTF(test)(KR_OSTREAM, "Note what 21 now holds:\n");
  ShowKey(KR_OSTREAM, KR_KEYBITS, 21);

  KPRINTF(test)(KR_OSTREAM, "Reloading from memory to KR21:\n");
  LOAD_KEYREG(21, cap_page);

  KPRINTF(test)(KR_OSTREAM, "Note what 21 now holds:\n");
  ShowKey(KR_OSTREAM, KR_KEYBITS, 21);

  KPRINTF(test)(KR_OSTREAM, "Test DONE.  Should now segfault:\n");

  kdprintf(KR_OSTREAM,
	   "About to seg fault loading capability from data addr\n");
  LOAD_KEYREG(21, (void *) (__rt_stack_pointer - EROS_PAGE_SIZE));
}
