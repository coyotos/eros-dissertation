/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/SleepKey.h>
#include <domain/ConstructorKey.h>
#include <domain/SigMuxKey.h>
#include <domain/domdbg.h>

#define DEBUG if (0)

#define KR_CONSTIT 1
#define KR_SELF    2
#define KR_BANK    4
#define KR_SCHED   5

#define KR_OSTREAM 6
#define KR_SIGMUXF 7
#define KR_READERF 8
#define KR_SCRATCH 9
#define KR_SIGMUX  10		/* the newly created sigmux */
#define KR_MUXCLD  11		/* for the MUX children */
#define KR_SLEEP   12

#define KC_OSTREAM   0
#define KC_SIGMUXF   1
#define KC_READERF   2
#define KC_SLEEP     3

const uint32_t __rt_stack_pages = 1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif

#define NCLIENT 16

void
main()
{
  uint32_t result;
  int i;
  
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  node_copy(KR_CONSTIT, KC_SIGMUXF, KR_SIGMUXF);
  node_copy(KR_CONSTIT, KC_READERF, KR_READERF);
  node_copy(KR_CONSTIT, KC_SLEEP, KR_SLEEP);
  
  DEBUG kdprintf(KR_OSTREAM, "About to create sigmux\n");

  /* Fabricate the SIGMUX object: */
  result = constructor_request(KR_SIGMUXF, KR_BANK, KR_SCHED, KR_ZERO,
			       KR_SIGMUX);

  if (result != RC_OK)
    kdprintf(KR_OSTREAM, "Result from sigmux creation: 0x%x\n", result);

  /* Fabricate each of the readers, and tell each one who it is: */
  for (i = 0; i < NCLIENT; i++) {
    Message msg;

    result = sigmux_make_recipient(KR_SIGMUX, i, KR_SCRATCH);
    if (result != RC_OK)
      kdprintf(KR_OSTREAM, "Built sigmux recipient key %d: 0x%x\n", i, result);
    
    result = constructor_request(KR_READERF, KR_BANK, KR_SCHED, KR_ZERO,
				 KR_MUXCLD);
    
    if (result != RC_OK)
      kdprintf(KR_OSTREAM, "Constructing reader %d: 0x%x\n", i, result);

    msg.snd_key0 = KR_SCRATCH;
    msg.snd_key1 = KR_ZERO;
    msg.snd_key2 = KR_ZERO;
    msg.snd_key3 = KR_ZERO;
    msg.snd_len = 0;
    msg.snd_code = 0;
    msg.snd_w1 = i;
    msg.snd_invKey = KR_MUXCLD;
    
    msg.rcv_key0 = KR_ZERO;
    msg.rcv_key1 = KR_ZERO;
    msg.rcv_key2 = KR_ZERO;
    msg.rcv_key3 = KR_ZERO;
    msg.snd_len = 0;

    SEND(&msg);

    kprintf(KR_OSTREAM, "Started reader %d.  Result=0x%x\n", i, result);
  }

  for (i = 0; i < 16; i++) {
    sl_sleep(KR_SLEEP, 1000);
    kdprintf(KR_OSTREAM, "Posting %d to readers\n", i);

    sigmux_post(KR_SIGMUX, i);
  }

  kdprintf(KR_OSTREAM, "ALL DONE\n");
}
