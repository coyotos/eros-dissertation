/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * TrivKpr -- a trivial domain keeper, just (about) smart enough to
 * dump the information it was passed by the kernel onto the console.
 *
 * It receives as input a DomCtlInfo structure, plus a restart key to
 * the domain and also a domain key to the domain.
 *
 * This version contrives to return to the NULL key.  I need to fix
 * the library convention to deal with specifying a different return
 * key. 
 *
 * Key Registers:
 *
 * KR12: arg0
 * KR13: arg1
 * KR14: arg2
 * KR15: arg3
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/ReturnerKey.h>
#include <domain/domdbg.h>
// #include <eros/DomCtlInfo.h>
#ifdef __REGISTERS_I486_H__
#error "registers anti-macro defined"
#endif
#include <eros/machine/Registers.h>

#define KR_ZERO 0
#define KR_OSTREAM 1
#define KR_RETURNER 2

#define KR_RK0   12
#define KR_RK1   13
#define KR_RK2   14
#define KR_RESUME  15		/* so don't use it for anything else! */

typedef struct DomCtlInfo32_s DomCtlInfo;

int
ProcessRequest(Message *msg)
{
  struct Registers * regs = (struct Registers*) msg->rcv_data;

  msg->snd_len = 0;
  
  msg->snd_key0 = KR_ZERO;
  msg->snd_key1 = KR_ZERO;
  msg->snd_key2 = KR_ZERO;
  msg->snd_key3 = KR_ZERO;
  
  kprintf(KR_OSTREAM, "faultCode: 0x%08x\n", regs->faultCode);
  kprintf(KR_OSTREAM, "faultInfo: 0x%08x\n", regs->faultInfo);
  kprintf(KR_OSTREAM, "fault PC:  0x%08x\n", regs->pc);

  __asm__ __volatile__ ("hlt");

  msg->snd_code = msg->rcv_code;

  /* NOTREACHED */
  return 1;
}

uint8_t buf[EROS_PAGE_SIZE];

void
main()
{
  Message msg;
  
  msg.snd_invKey = KR_ZERO;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = KR_RK0;
  msg.rcv_key1 = KR_RK1;
  msg.rcv_key2 = KR_RK2;
  msg.rcv_key3 = KR_RESUME;
  msg.rcv_data = (void *) buf;
  msg.rcv_len = EROS_PAGE_SIZE;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  do {
    RETURN(&msg);
 } while ( ProcessRequest(&msg) );
}
