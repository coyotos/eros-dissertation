/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NodeKey.h>
#include <eros/PageKey.h>
#include <eros/RangeKey.h>
#include <eros/SleepKey.h>
#include <eros/StdKeyType.h>
#include <domain/domdbg.h>

#undef VERBOSE
#define VERBOSE

#define KR_ZERO       0
#define KR_TMP0       1

#define KR_RANGE      6

#define KR_CONSTIT    7
#define  KC_RANGEKEY   3

#define KR_OSTREAM    8
#define KR_SLEEPKEY   9
#define KR_KEYBITS   10
#define KR_CONTAINER 11

#define SLEEP_TME  0 /* (100/7) */

static int __assert(const char *, const char *, int);

#define assert(expression)  \
  ((void) ((expression) ? 0 : __assert (#expression, __FILE__, __LINE__)))

/**Handle the stack stuff**/
const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x10000;

void runLoop(uint64_t baseOffset)
{
  /* a loop consists of allocating 15 nodes, rescinding them, then
   * allocating a page, rescinding it.
   */
  uint32_t idx;
  uint32_t result;

  for (idx = 0; idx < EROS_NODES_PER_FRAME; idx++) {
    kprintf(KR_OSTREAM,
	    "RetagTest: Grabbing node (0x%08x%08x)\n",
	    (uint32_t) (baseOffset >> 32),
	    (uint32_t) baseOffset | idx);
    
    result = range_getobjectkey(KR_RANGE,
				OT_Node,
				baseOffset | idx,
				KR_TMP0);

    assert("creating subnode" && result == RC_OK );

    /* copy the node key into the top node */
    result = node_swap(KR_CONTAINER, idx, KR_TMP0, KR_ZERO);

    assert("swapping new node into top node" && result == RC_OK );
  }

  kprintf(KR_OSTREAM,
	   "RetagTest: finished creating nodes.\n");

  kprintf(KR_OSTREAM,
	  "RetagTest: Grabbing page (0x%08x%08x)\n",
	  (uint32_t) (baseOffset >> 32),
	  (uint32_t) baseOffset);

  result = range_getobjectkey(KR_RANGE,
			      OT_Page,
			      baseOffset,
			      KR_TMP0);

  assert("creating top page" && result == RC_OK );

  /* copy the node key into the top node */
  result = node_swap(KR_CONTAINER, 0, KR_TMP0, KR_ZERO);

  assert("swapping new page into top node" && result == RC_OK );

  kprintf(KR_OSTREAM,
	   "RetagTest: finished creating page.\n");

}

#define NUM_LOOPS (1000u)

void main(void)
{
  uint32_t result;
  uint32_t idx;
  
  uint64_t baseOffset; 

  kprintf(KR_OSTREAM,
	  "RetagTest: Initialiazing\n");

  result = node_copy(KR_CONSTIT, KC_RANGEKEY, KR_RANGE);

  assert("copying range key from volsize" && result == RC_OK );
  
  assert("validating range key" && key_kt(KR_RANGE) == AKT_Range );

  result = range_query(KR_RANGE, &baseOffset);

  assert("querying range length" && result == RC_OK );

  /* subtract one frame to get last frame in range */
  baseOffset -= EROS_OBJECTS_PER_FRAME;
  
  kprintf(KR_OSTREAM,
	  "RetagTest: Done initializing.\n");
  
  kprintf(KR_OSTREAM,
	  "NOTE: Master node key will be in slot %u\n"
	  "      Temporary holding place will be slot %u\n",
	  KR_CONTAINER,
	  KR_TMP0);
  
  kdprintf(KR_OSTREAM,
	   "RetagTest: Beginning test with baseOffset 0x%08x%08x\n",
	   (uint32_t) (baseOffset >> 32),
	   (uint32_t) baseOffset);
  
  
  for (idx = 0; idx < NUM_LOOPS; idx++) {
    kprintf(KR_OSTREAM, "RetagTest: Starting %u\n", idx);
    
    runLoop(baseOffset);
  }
  kdprintf(KR_OSTREAM, "RetagTest PASSES\n");
}

static int __assert(const char *expr, const char *file, int line)
{
  kdprintf(KR_OSTREAM, "%s:%d: Assertion failed: '%s'\n",
	   file, line, expr);
  return 0;
}

