/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/SleepKey.h>
#include <eros/Reserve.h>
#include <eros/SchedCreKey.h>
#include <eros/ProcessKey.h>
#include <eros/Key.h>
#include <domain/domdbg.h>

#define KR_ZERO 0

#define KR_SCHEDKEY 8

#define KR_SLEEPKEY 9
#define KR_OSTREAM 10
#define KR_TALKER 11
#define KR_SPINNER 12
#define KR_SPINDOM 14
#define KR_SCHEDCRE 15

void
main()
{
  Message msg;
  struct CpuReserveInfo ri;

  ri.index = 16;
  ri.period = 15000;
  ri.duration = 5000;
  ri.normPrio = -2;
  ri.rsrvPrio = 9;		/* higher than other counter */
  ri.quanta = 10;
  ri.start = 0ll;
  
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_len = 0;
  msg.snd_code = 1;

  msg.snd_invKey = KR_TALKER;
  
  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;
  msg.rcv_len = 0;

  {
    uint32_t result = schedcre_make_reserve(KR_SCHEDCRE, &ri, KR_SCHEDKEY);

    kprintf(KR_OSTREAM, "Got reserve key.  Result=0x%08x\n", result);
  }

  process_swap(KR_SPINDOM, ProcSched, KR_SCHEDKEY, KR_ZERO);
  
  kprintf(KR_OSTREAM, "Starting counter 1.\n");

  SEND(&msg);

  msg.snd_invKey = KR_SPINNER;
  
  kprintf(KR_OSTREAM, "Starting counter 2.\n");
  SEND(&msg);

  msg.snd_invKey = KR_ZERO;
  RETURN(&msg);
  __asm__ __volatile__ ("hlt");
}
