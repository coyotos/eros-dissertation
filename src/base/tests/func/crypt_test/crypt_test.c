/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <domain/domdbg.h>

#define KR_ZERO 0
#define KR_CRYPT 8
#define KR_OSTREAM 10

const char *str = "Hello from call.";
const char *key = "thiskey1";
char data[EROS_PAGE_SIZE];

size_t strlen(const char *s)
{
  size_t len = 0;
  while (*s++)
    len++;

  return len;
}

void Startup()
{
  Message msg;
  uint32_t result;

  kdprintf(KR_OSTREAM, "About to call encrypt to set key\n");

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;

  msg.snd_data = key;
  msg.snd_len = strlen(key);

  msg.snd_invKey = KR_CRYPT;
  msg.snd_code = 1;

  msg.rcv_key0 = 12;
  msg.rcv_key1 = 13;
  msg.rcv_key2 = 14;
  msg.rcv_key3 = 15;

  msg.rcv_data = (void*) 0x40000;
  msg.rcv_len = 2 * EROS_PAGE_SIZE;

  result = CALL(&msg);
  kdprintf(KR_OSTREAM, "Returned with RC=0x%08x\n", result);

  kdprintf(KR_OSTREAM, "About to call encrypt to encrypt\n");

  msg.snd_data = str;
  msg.snd_len = strlen(str);
  msg.rcv_len = 2 * EROS_PAGE_SIZE;
      
  msg.snd_invKey = KR_CRYPT;
  msg.snd_code = 2;

  result = CALL(&msg);
  msg.rcv_len = strlen(msg.rcv.data);
  ((uint8_t *)msg.rcv_data)[msg.rcv.len] = 0;

  kdprintf(KR_OSTREAM, "Got back rc=0x%08x: %s\n", result, msg.rcv_data);
    
  kdprintf(KR_OSTREAM, "About to call encrypt to decrypt\n");

  msg.snd_len = strlen(msg.rcv_data);
  msg.snd_data = msg.rcv_data;
  msg.rcv_len = 2 * EROS_PAGE_SIZE;
  
  msg.snd_invKey = KR_CRYPT;
  msg.snd_code = 3;

  result =  CALL(&msg);
  msg.rcv_len = strlen(msg.rcv.data);
  ((uint8_t *)msg.rcv_data)[msg.rcv.len] = 0;
  kdprintf(KR_OSTREAM, "Got back rc=0x%08x: %s\n", result, msg.rcv_data);
}

