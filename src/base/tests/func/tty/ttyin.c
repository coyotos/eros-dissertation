/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>


#define KR_ZERO 0
#define KR_CONSOLEKEY 9
#define KR_SLEEPKEY 10
#define KR_KBDKEY 8
#define KR_KEYBITS 11

typedef struct Bits {
  uint32_t w[4];
} Bits;



int
strlen(const char *s)
{
  const char *send = s;

  while (*send)
    send++;

  return send - s;
}

void
Write(const char *s)
{
  Message msg;

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;

  msg.snd_data = (uint8_t *) s;
  msg.snd_len = strlen(s);	/* omit trailing null! */

  msg.rcv_len = 0;		/* no data returned */

  (void) CALL(KR_CONSOLEKEY, 1, &msg);
}

int 
putc (int chr)
{

  char chrbuf[2];
  
  chrbuf[0] = chr;
  chrbuf[1] = 0;

  Write(chrbuf);

  return 1;

}

char *
hexcat(char *buf, uint32_t w)
{
  static char hexdigits[16] = {
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
  };

  int i;
  char val[11];

  *buf++ = '0';
  *buf++ = 'x';

  for (i = 0; i < 8; i++) {
    unsigned xc = w & 0xfu;
    buf[7-i] = hexdigits[xc];
    w >>= 4;
  }

  buf += 8;
  *buf = 0;

  return buf;
}

char *
strcat(char *buf, char *str)
{
  while (*str)
    *buf++ = *str++;

  *buf = 0;
  
  return buf;
}

char msgbuf[128];

void
GetKeyBits(uint32_t kr, uint32_t *version, Bits *bits)
{
  int i;
  struct kbinfo {
    uint32_t ver;
    Bits bits;
  } kbi;

  Message msg;

  msg.snd_key0 = kr;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;

  msg.rcv_data = &kbi;
  msg.rcv_len = sizeof(kbi);

  msg.snd_len = 0;		/* no data returned */

  (void) CALL(KR_KEYBITS, OC_KeyBits_Translate, &msg);
  
  *version = kbi.ver;
  for(i = 0; i < 4; i++)
    bits->w[i] = kbi.bits.w[i];
}


void
ShowKey(uint32_t kr)
{
  struct Bits bits;
  uint32_t version;
  char *buf;

  GetKeyBits(kr, &version, &bits);
  
  buf = strcat(msgbuf, "Keybits version is ");
  buf = hexcat(buf, version);
  buf = strcat(buf, "\r\n");
  
  Write(msgbuf);
  
  Write("Keybits data is:\r\n");

  buf = strcat(msgbuf, "  ");
  buf = hexcat(buf, bits.w[0]);
  buf = strcat(buf, "  ");
  buf = hexcat(buf, bits.w[1]);
  buf = strcat(buf, "  ");
  buf = hexcat(buf, bits.w[2]);
  buf = strcat(buf, "  ");
  buf = hexcat(buf, bits.w[3]);
  buf = strcat(buf, "\r\n");
  Write(msgbuf);
  
}


int
getc()
{
  Message msg;
  char chr[2];			/* chr + 0 */
  uint16_t Result;
  uint32_t bytes = 1;
  char output_buf[256];		

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;

  msg.snd_len = sizeof(bytes);
  msg.snd_data = &bytes;

  msg.rcv_len = sizeof(chr);
  msg.rcv_data = chr;

  Result = CALL(KR_KBDKEY, 0, &msg); /* OC = 0 */

  if (Result != RC_OK)
    {
      Write ("Result is bad ");
      hexcat (output_buf, Result);
      Write (output_buf);
      Write ("\n\r");
      return 0;
    }
  else
    {
      Write ("result is good\n\r");
      return (chr[0]);		/* return the character */
    }
}


void
Startup()
{
  uint16_t Result;
  char output_buf[256];		

  for ( ; ; )
    {
      Write ("TTY domain.  Testing.\n\r");
      Result = getc();		/* try to read a character */
      Write ("Result from getc is ");
      hexcat (output_buf, Result);
      Write (output_buf);
      Write ("\n\r");
    }

}

