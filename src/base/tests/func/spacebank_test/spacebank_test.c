/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NodeKey.h>
#include <eros/PageKey.h>
#include <eros/StdKeyType.h>
#include <eros/SleepKey.h>
#include <domain/SpaceBankKey.h>
#include <domain/domdbg.h>

#define NODE_ALLOC_CHECK

#ifdef GNU
#define INLINE inline
#else
#define INLINE
#endif

/* define the following if you want the test to stop after each stage */
/* #define STPRINTF kdprintf */
#define VERBOSE

#ifndef STPRINTF
#define STPRINTF kprintf
#endif

#define KR_ZERO       0

#define KR_SPACEBANK  1
#define KR_CONSOLEKEY 2
#define KR_SLEEPKEY   3
#define KR_KEYBITS    4

#define KR_PARBANK    5
#define KR_SUBBANK    6

#define KR_TMP0       7
#define KR_TMP1       8
#define KR_TMP2       9
#define KR_TMP3      10

/* key stuff */
#define KR_KEYBASE   11
#define KR_KEY(x)    (KR_KEYBASE+(x))

#define NUM_KEYS     (EROS_PROCESS_KEYREGS-KR_KEYBASE)



#define SLEEP_TME  0 /* (100/7) */

/* define DIRTY_OBJECTS if you want all allocated objects dirtied */
#define DIRTY_OBJECTS 1


#define NUM_LEVELS (3u)
/* nnumber of levels in the node tree */

#define NUM_ITEMS (1u << (4*NUM_LEVELS))
/* == 16 ^ NUM_LEVELS */

/**Handle the stack stuff**/
const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x10000;

struct key_array_info
{
  uint32_t alloced[NUM_KEYS];  /* stores the number of slots in use
				  in the *last* node of this
				  level.  Only == 0 if nothing has
				  ever been alloced in this slot.
				  0 is the LOWEST level, with
				  increasing numbers indicating
				  higher levels. 
				  
				  A 0 indicated not-allocated.
			       */
  uint32_t curKeys;  /* number of keys in 0th-level node */
};

uint32_t
init_key_array(struct key_array_info *info,
	       const char *name,
	       uint32_t krBank)
{
  int idx;

  for (idx = 0; idx < NUM_KEYS; idx++) 
    info->alloced[idx] = 0;
  
  /* allocate the starting node */
  if (spcbank_buy_nodes(krBank,1,KR_KEY(0),KR_ZERO,KR_ZERO) 
      != RC_OK) {
    return 1;
  }

  info->alloced[0]++;  /*mark it as allocated */
  info->curKeys = 0;

  return 0;
}

uint32_t
insert_key(struct key_array_info *info, 
	   const char *name,
	   uint32_t krBank,
	   uint32_t fromSlot)
{
  if (info->curKeys >= EROS_NODE_SIZE) {
    /* We need to grow the tree. */
    uint32_t maxEff;
    uint32_t cur;
    uint32_t didAllocBase = 0;

    /* find the first non-full level */
    for (maxEff = 0;
          maxEff < NUM_KEYS
	  && info->alloced[maxEff] >= EROS_NODE_SIZE;
           maxEff++)
      ; /* NOTHING */
    
    /* at this point, info->alloced[maxEff] is > 0.  
     *   If it == 1, then we first need to grow the tree one level, by
     *  allocating a node at level maxEff+1 and inserting the KEY(maxEff)
     *  node in it's zeroth slot.
     *
     *   In either case, we now go down the tree, allocating new nodes
     *  until we reach the bottom.
     */

    if (maxEff >= NUM_KEYS
	|| info->alloced[maxEff] == 0
	|| (maxEff < NUM_KEYS-1 
	    && info->alloced[maxEff] == 1
	    && info->alloced[maxEff+1] != 0)) {
      /* SHOULDN'T HAPPEN */
      kdprintf(KR_CONSOLEKEY,
	       "%s: Impossible condition in insert_key\n",
	       name);
      return 1;
    }
    
    if (info->alloced[maxEff] == 1) {
      if (maxEff == NUM_KEYS - 1) {
	kdprintf(KR_CONSOLEKEY,
		 "%s: Hey!  We're going to use more than %d levels!\n"
		 "     That's pow(%d,%d) keys!\n",
		 name,NUM_KEYS,EROS_NODE_SIZE,NUM_KEYS);
	return 1; /* FAILED */
      }
      
      /* Buy the node for level maxEff+1 */
      if (spcbank_buy_nodes(krBank,1,KR_KEY(maxEff+1),KR_ZERO,KR_ZERO) 
	  != RC_OK) {
	kprintf(KR_CONSOLEKEY,
		"%s: Hey! Failed to allocate new head node (%d levels).\n",
		name,maxEff);
	return 1; /* FAILED */
      }
      /* mark level maxEff+1 as having a single node */
      info->alloced[maxEff+1]++;
      /* mark this true so we can remove it later. */
      didAllocBase = 1;

      /* copy in the maxEff level key as the first key in the new node */
      node_swap(KR_KEY(maxEff+1),0,KR_KEY(maxEff),KR_ZERO);
      
#ifdef VERBOSE
      kprintf(KR_CONSOLEKEY,"+");
#endif
      /* now that we are set up, the normal logic works */
    }
    /* loop, creating a new node at each level */
    cur = maxEff;
    do {
      if (spcbank_buy_nodes(krBank,1,KR_KEY(cur),KR_ZERO,KR_ZERO)
	  != RC_OK) {
	kprintf(KR_CONSOLEKEY,"\n%s: Failed to buy node.\n",name);
	/* clean up */
	while (cur < maxEff) {
	  cur++;
	  if (spcbank_return_node(krBank,KR_KEY(cur)) != RC_OK) {
	    kdprintf(KR_CONSOLEKEY,"%s: Failed to return node.\n",name);
	  }
	  info->alloced[cur]--;
	  node_copy(KR_KEY(cur + 1),info->alloced[cur],KR_KEY(cur));
	}
	if (didAllocBase) {
	  if (spcbank_return_node(krBank,KR_KEY(maxEff+1)) != RC_OK) {
	    kdprintf(KR_CONSOLEKEY,"%s: Failed to return node.\n",name);
	  }	  
	  info->alloced[maxEff+1] = 0;
	}
	return 1;
      }
      node_swap(KR_KEY(cur+1),
		info->alloced[cur],
		KR_KEY(cur),
		KR_ZERO);
      info->alloced[cur]++;

#ifdef VERBOSE
      kprintf(KR_CONSOLEKEY,"+");
#endif
      
    } while (cur-- > 0);

    /* now that we have successfully allocated everything, update the
       table. */
    for (cur = 0; cur < maxEff; cur++) {
      info->alloced[cur] = 1; /* the new node is empty */
    }
    info->curKeys = 0; /* we now have a new bottom level node */
  }

  /* curKeys is now guarenteed to be < EROS_NODE_SIZE -- we can insert
     the new key into the curKeys slot...*/

  node_swap(KR_KEY(0),
	    info->curKeys++,
	    fromSlot,
	    KR_ZERO);

  return RC_OK;
}

uint32_t
dealloc_array(struct key_array_info *info,
	      const char *name,
	      uint32_t (*dealloc_func)(uint32_t fromSlot, uint32_t krBank),
	      uint32_t krBank,
	      uint32_t tmpSlot)
{
  /* smashes tmpSlot */

  uint32_t curpos[NUM_KEYS];
  uint32_t lvl;
  uint32_t maxLvl;
  uint32_t slotNum;
  uint32_t numDeallocs;

  
  /* clear the curpos array */
  for (lvl = 0; lvl < NUM_KEYS; lvl++)
    curpos[lvl] = 0;
 
  /* find the maximum level */
  maxLvl = 0;
  while(1) {
    if (info->alloced[maxLvl] == 1
	&& (maxLvl == NUM_KEYS-1 || info->alloced[maxLvl+1] == 0))
      break;

    maxLvl++;

    /* sanity check */
    if (maxLvl >= NUM_KEYS) {
      kdprintf(KR_CONSOLEKEY,"%s: dealloc_array Impossible condition\n",name);
      return 1;
    }
  }
  
  curpos[maxLvl] = 1;
  
  /* grab the first items all the way down the tree */
  lvl = maxLvl;
  while (lvl > 0) {
    lvl--;
    node_swap(KR_KEY(lvl+1),0,KR_ZERO,KR_KEY(lvl));
  }

  /* the bottom level isn't incremented until we have deallocated it
   */
  curpos[0] = 0;

#ifdef VERBOSE
  /* NO NEWLINE -- we print a dot/object deallocated */
  kprintf(KR_CONSOLEKEY,"%s: Deallocating",name);
#endif

  numDeallocs = 0;

  while (maxLvl > 0) {
    /* first, deallocate the items in the key(0) slot */
    for (slotNum = 0; slotNum < info->curKeys; slotNum++) {
      numDeallocs++;
      
      node_swap(KR_KEY(0),slotNum,KR_ZERO,tmpSlot);
      if ((*dealloc_func)(tmpSlot,krBank) != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "\n%s: Error deallocating object #%d.\n",
		 name,
		 numDeallocs);
	return 1;
      }
#ifdef VERBOSE
      kprintf(KR_CONSOLEKEY,".");
#endif
    }
    if (spcbank_return_node(krBank,KR_KEY(0)) != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "\n%s: Error deallocating node!\n",name);
      return 1;
    }
    curpos[0]++; /* we are done with this node */

#ifdef VERBOSE
      kprintf(KR_CONSOLEKEY,"+");
#endif

    for (lvl = 0;
	  lvl < maxLvl - 1
	  && curpos[lvl] == EROS_NODE_SIZE;
  	   lvl++) {
      /* this level is empty -- remove the enclosing node */
      if (spcbank_return_node(krBank,KR_KEY(lvl+1)) != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "\n%s: Error deallocating node.\n",
		 name);
      }
#ifdef VERBOSE
      kprintf(KR_CONSOLEKEY,"+");
#endif
      curpos[lvl+1]++;
      curpos[lvl] = 0;
    }

    if (maxLvl == lvl + 1
 	   && curpos[lvl] + 1 == info->alloced[lvl]) {
      /* We will be putting in the last node from the maxLvl pile.
	 get the last node by hand, and deallocate the outer node. */
      
      node_swap(KR_KEY(lvl+1),curpos[lvl],KR_ZERO,KR_KEY(lvl));

      if (spcbank_return_node(krBank,KR_KEY(maxLvl)) != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "\n%s: Error deallocating node.\n",
		 name);
      }
#ifdef VERBOSE
      kprintf(KR_CONSOLEKEY,"+");
#endif
      maxLvl--;
      lvl--;
    } else if (maxLvl == lvl + 1 
	       && curpos[lvl] + 1 > info->alloced[lvl]) {
      kdprintf(KR_CONSOLEKEY,
	       "%s: Impossible condition in dealloc_tree\n",
	       name);
    }
    /* now, we need to walk back down, getting the new nodes and
       resetting the current pointers. */

    do {
      node_swap(KR_KEY(lvl+1),curpos[lvl],KR_ZERO,KR_KEY(lvl));
    } while (lvl-- > 0);

  }

  /* do the last one by hand */
  for (slotNum = 0; slotNum < info->curKeys; slotNum++) {
    numDeallocs++;

    node_swap(KR_KEY(0),slotNum,KR_ZERO,tmpSlot);
    if ((*dealloc_func)(tmpSlot,krBank) != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "\n%s: Error deallocating object #%d.\n",
	       name,
	       numDeallocs);
      return 1;
    }
#ifdef VERBOSE
    kprintf(KR_CONSOLEKEY,".");
#endif
  }
  if (spcbank_return_node(krBank,KR_KEY(0)) != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "\n%s: Error deallocating last node!\n",name);
    return 1;
  }
#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"+");
#endif
  return 0;
}


uint32_t
RunAllocTest(const char *test_name,
             uint32_t (*alloc_func)(uint32_t toSlot, uint32_t sbSlot),
             uint32_t (*dealloc_func)(uint32_t fromSlot, uint32_t sbSlot))
{
  /* this is a generic testing interface -- It basically
    calls *alloc_func with a slot number repeatedly, storing the
    result key away somewhere, until it returns a non-zero value, or
    is unable to allocate more storage for the keys.  At this point,
    it walks through the allocated objects, calling *dealloc_func for
    each one.
   
    if dealloc_func is NULL, then the allocated objects are not

    SMASHES KR_TMP0 and KR_PARBANK!!!
   */

  struct key_array_info info;  /* holds the key array data */
  uint32_t count; /* number of alloced objects */

  /* first create a bank for the test */
#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,
	  "%s: Allocating bank\n",test_name);
#endif

  if (spcbank_create_subbank(KR_SPACEBANK,KR_PARBANK) != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "%s: Unable to create subbank for testing.\n",
	     test_name);
    return 1;
  }
  if (init_key_array(&info,test_name,KR_PARBANK) != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "%s: Unable in init Key Array.\n",
	     test_name);
    return 1;
  }

  count = 0; /* initialize count */

  STPRINTF(KR_CONSOLEKEY,"%s: Beginning Test\n",test_name);

#ifdef VERBOSE
  /* NO TRAILING NEWLINE -- we print dots as we allocate */
  kprintf(KR_CONSOLEKEY,"%s: Allocating",test_name); 
#endif

  for (count = 0; ; count++) { /* infernal loop */
    /* note:  count is incremented only on completion of loop */

    if ((*alloc_func)(KR_TMP0,KR_PARBANK) != RC_OK) {
#ifdef VERBOSE      
      kprintf(KR_CONSOLEKEY,
	      "\n%s: alloc_func failed\n",
	      test_name);
#endif
      break; /* done */
    }

    if (insert_key(&info,test_name,KR_PARBANK,KR_TMP0) != RC_OK) {
#ifdef VERBOSE      
      if (dealloc_func) {
	kprintf(KR_CONSOLEKEY,
		"\n%s: insert_key failed -- deallocating last object\n",
		test_name);
      } else {
	kprintf(KR_CONSOLEKEY,
		"\n%s: insert_key failed -- last object will be destroyed"
		"with the bank\n",
		test_name);
      }
#endif

      if (dealloc_func
	  && (*dealloc_func)(KR_TMP0,KR_PARBANK) != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "%s: Insert_key failed, and I can't deallocate "
		 "the last object!!\n",
		 test_name);
	return 1;
      }
      break;
    }
#ifdef VERBOSE
    kprintf(KR_CONSOLEKEY,".");
#endif
  } /* for (count = 0;;count++) */

  STPRINTF(KR_CONSOLEKEY,
	   "\n%s: Finished allocating.  A total of %d objects allocated.\n", 
	   test_name,
	   count);

  if (dealloc_func) {
    kprintf(KR_CONSOLEKEY,
	    "%s: Beginning Deallocation pass...\n",
	    test_name);

    if (dealloc_array(&info,test_name,dealloc_func,
		      KR_PARBANK,KR_TMP0) != RC_OK) {
      kdprintf(KR_CONSOLEKEY,"%s: Error deallocating objects.\n",test_name);
      return 1;
    }
    kprintf(KR_CONSOLEKEY,
	    "%s: Destroying bank...\n",
	    test_name);
  } else {
    kprintf(KR_CONSOLEKEY,
	    "%s: Destroying bank and space...\n",
	    test_name);
  }
  
  if (spcbank_destroy_bank(KR_PARBANK,(dealloc_func == NULL)) 
      != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "%s: Failed to destroy bank%s.\n",
	     test_name,
	     (dealloc_func == NULL)?" and space":"");
  }
  STPRINTF(KR_CONSOLEKEY,
	   "%s: Finished deallocation.\n",test_name);

  return 0;
}

uint32_t
page_allocate(uint32_t toKey, uint32_t krBank)
{
  return spcbank_buy_data_pages(krBank,1,toKey,KR_ZERO,KR_ZERO);
}

uint32_t
page_dealloc(uint32_t fromKey, uint32_t krBank)
{
  return spcbank_return_data_page(krBank,fromKey);
}

uint16_t
PageAllocTest(uint32_t deallocManually)
{
  return RunAllocTest("PageAllocTest",
		      page_allocate,
		      (deallocManually)? page_dealloc : NULL);
}

uint32_t
cappage_allocate(uint32_t toKey, uint32_t krBank)
{
  return spcbank_buy_cap_pages(krBank,1,toKey,KR_ZERO,KR_ZERO);
}

uint32_t
cappage_dealloc(uint32_t fromKey, uint32_t krBank)
{
  return spcbank_return_cap_page(krBank,fromKey);
}

uint16_t
CapPageAllocTest(uint32_t deallocManually)
{
  return RunAllocTest("CapPageAllocTest",
		      cappage_allocate,
		      (deallocManually)? cappage_dealloc : NULL);
}

uint32_t
node_allocate(uint32_t toKey, uint32_t krBank)
{
  return spcbank_buy_nodes(krBank,1,toKey,KR_ZERO,KR_ZERO);
}

uint32_t
node_dealloc(uint32_t fromKey, uint32_t krBank)
{
  return spcbank_return_node(krBank,fromKey);
}

uint16_t
NodeAllocTest(uint32_t deallocManually)
{
  return RunAllocTest("NodeAllocTest",
		      node_allocate,
		      (deallocManually)? node_dealloc : NULL);
}


uint16_t 
SpaceBankInit()
{
  uint32_t Result;


  kprintf(KR_CONSOLEKEY, "Spacebank test domain: identifying spacebank.\n");
  
  Result = key_kt(KR_SPACEBANK);
             /* this step will also initialize the spacebank */
  
  if (Result == AKT_SpaceBank) {
    STPRINTF(KR_CONSOLEKEY, "Spacebank verified. \n");
  } else {
    kprintf(KR_CONSOLEKEY,
	    "Spacebank NOT verified.\n"
	    "Result from spacebank was 0x%08x\n",
	    Result);
    return 0;
  }      

  return 1;
}

uint16_t
SubBankTest()
{
  uint32_t result;
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest: Starting SubBankTest\n");

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating parent\n");
#endif
  result = spcbank_create_subbank(KR_SPACEBANK, KR_PARBANK);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error creating Parent! (0x%08x)\n",
	     result);
    return 0;
  }

  result = spcbank_verify_bank(KR_SPACEBANK, KR_PARBANK);
  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Verification of new bank failed! (0x%08x)\n",
	     result);
  }
#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating first child\n");
#endif
  result = spcbank_create_subbank(KR_PARBANK, KR_SUBBANK);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error creating first sub-bank! (0x%08x)\n",
	     result);
    return 0;
  }

  result = spcbank_verify_bank(KR_PARBANK, KR_SUBBANK);
  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Verification of new bank failed! (0x%08x)\n",
	     result);
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating first two pages\n");
#endif
  result = spcbank_buy_data_pages(KR_SUBBANK, 2, KR_TMP0, KR_TMP1, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error buying first two pages! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP0);
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP1);
#endif
  

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating first two nodes\n");
#endif

  result = spcbank_buy_nodes(KR_SUBBANK, 2, KR_TMP2, KR_TMP3, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error buying first two nodes! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP2);
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP3);
#endif
  

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: destroying 1st subbank + space\n");
#endif

  /* destroy bank and space */
  result = spcbank_destroy_bank(KR_SUBBANK, 1);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error destroying first subbank and space! (0x%08x)\n",
	     result);
    return 0;
  }

  if (key_kt(KR_TMP3) != AKT_Number) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Keys not zeroed on destroy first bank and space\n",
	     result);
    return 0;    
  }

  result = spcbank_verify_bank(KR_SPACEBANK, KR_SUBBANK);
  if (result == RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Verification of destroyed bank key succedded!\n",
	     result);
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating 2nd subbank\n");
#endif
  result = spcbank_create_subbank(KR_PARBANK, KR_SUBBANK);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error creating second sub-bank! (0x%08x)\n",
	     result);
    return 0;
  }

  result = spcbank_verify_bank(KR_SPACEBANK, KR_SUBBANK);
  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Verification of new bank failed! (0x%08x)\n",
	     result);
  }
#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating second two pages\n");
#endif
  result = spcbank_buy_data_pages(KR_SUBBANK, 2, KR_TMP0, KR_TMP1, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error buying second two pages! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP0);
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP1);
#endif
  
#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating second two nodes\n");
#endif

  result = spcbank_buy_nodes(KR_SUBBANK, 2, KR_TMP2, KR_TMP3, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error buying second two nodes! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP2);
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP3);
#endif
  

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: destroying 2nd subbank\n");
#endif

  /* destroy bank, returning space to parent*/
  result = spcbank_destroy_bank(KR_SUBBANK, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error destroying second subbank! (0x%08x)\n",
	     result);
    return 0;
  }

  if (key_kt(KR_TMP3) != AKT_Node) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Keys zeroed on destroy 2nd bank return to parent!\n",
	     result);
    return 0;    
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: returning a page and a node\n");
#endif
  /* return one node and one page to the parent bank */
  result = (spcbank_return_data_page(KR_PARBANK, KR_TMP0)
	    || spcbank_return_node(KR_PARBANK, KR_TMP2));

  if (result) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error returning items to parent bank\n");
    return 0;
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: destroying parent bank and space\n");
#endif
  
  result = spcbank_destroy_bank(KR_PARBANK, 1);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error destroying parent bank and space! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: verifying destruction\n");
#endif

  if (key_kt(KR_TMP3) != AKT_Number || key_kt(KR_TMP1) != AKT_Number) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Keys not zeroed on parent destroy bank and space\n",
	     result);
    return 0;    
  }

  
  return 1;
}

void
main()
{
  int idx;
  /*
   * simple spacebank test domain.  the test
   * is divided into three stages: init, nodes, pages.
   */

  sl_sleep(KR_SLEEPKEY, SLEEP_TME);
  if (SpaceBankInit()
      && NodeAllocTest(1)
      && PageAllocTest(1)
      && CapPageAllocTest(1)
      && SubBankTest())
    kprintf(KR_CONSOLEKEY, "\nSpacebank test domain:  Success.\n");
  else
    kprintf(KR_CONSOLEKEY, "\nSpacebank test domain.  Failure.\n");

  kdprintf(KR_CONSOLEKEY, "Stress test?\n");

  for(idx = 0; ; idx++) {
    kprintf(KR_CONSOLEKEY,"Loop %x: NodeTest\n",idx);
    NodeAllocTest(1);
    kprintf(KR_CONSOLEKEY,"Loop %x: PageTest\n",idx);
    PageAllocTest(1);
    kprintf(KR_CONSOLEKEY,"Loop %x: SubBankTest\n",idx);
    SubBankTest();
  }

  return;

}
