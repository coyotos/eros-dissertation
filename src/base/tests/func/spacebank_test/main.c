/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>

#define KR_ZERO 0
#define KR_CONSOLEKEY 8
#define KR_SLEEPKEY 9
#define KR_SPACEBANK 7

int
strlen(const char *s)
{
  const char *send = s;

  while (*send)
    send++;

  return send - s;
}

void
Write(const char *s)
{
  Message msg;

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;

  msg.snd_data = (uint8_t *) s;
  msg.snd_len = strlen(s);	/* omit trailing null! */

  msg.rcv_len = 0;		/* no data returned */

  (void) CALL(KR_CONSOLEKEY, 1, &msg);
}


uint32_t
SpaceBank ()
{
  Message msg;

  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;


  msg.snd_len = 0;		/* no data sent */

  msg.rcv_len = 0;		/* no data returned */

  return (CALL(KR_SPACEBANK, KT, &msg));

}


void
Startup()
{

  uint32_t Result;
  Write ("Spacebank test domain.  Calling Spacebank");
  Result = SpaceBank();
  if (Result == 0x040000)
      Write ("Spacebank verified.");
  else
      Write ("Spacebank NOT verified.");
  Sleep(10);


}
