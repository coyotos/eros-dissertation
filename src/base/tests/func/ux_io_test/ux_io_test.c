/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/ReturnerKey.h>
#include <eros/SleepKey.h>
#include <domain/ConstructorKey.h>
#include <domain/domdbg.h>
#include <KEYDEFS.h>

#include <sys/fcntl.h>
#include <sys/errno.h>
#include <unistd.h>

extern int mkdir(const char *name, mode_t mode);
	  
void
init()
{
  /* Need to fabricate a root directory: */
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  node_copy(KR_CONSTIT, KC_SLEEP, KR_SCRATCH);
  node_copy(KR_CONSTIT, KC_RETURNER, KR_RETURNER);

  sl_sleep(KR_SCRATCH, 1000);

  /* Need to fabricate a root directory: */
  node_copy(KR_CONSTIT, KC_DIRC, KR_SCRATCH);

  kdprintf(KR_OSTREAM, "Fabricate root directory:\n");

  if (constructor_request(KR_SCRATCH, KR_BANK, KR_SCHED, KR_ZERO,
			  KR_CWD) != RC_OK) {
    kdprintf(KR_OSTREAM, "root dir build failed\n");
  }
  copy_key_reg(KR_RETURNER, KR_CWD, KR_FSROOT);

  kdprintf(KR_OSTREAM, "Fabricate file table:\n");

  /* Need to fabricate open file table: */
  node_copy(KR_CONSTIT, KC_SNODEC, KR_SCRATCH);
  if (constructor_request(KR_SCRATCH, KR_BANK, KR_SCHED, KR_ZERO,
			  KR_FD_TAB) != RC_OK) {
    kdprintf(KR_OSTREAM, "fd tab build failed\n");
  }
}

const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x21000;

int
dotest(char *filename)
{
  int fd;
  char buf[32];
  
  kdprintf(KR_OSTREAM, "\"%s\" - ABOUT TO CALL OPEN\n", filename);

  fd = open(filename, O_RDWR|O_CREAT, 0666);
  if (fd == -1)
    return -1;

  kdprintf(KR_OSTREAM, "\"%s\" - ABOUT TO CALL WRITE\n", filename);

  if ( write(fd, filename, strlen(filename)) == -1 )
    return -1;

  kdprintf(KR_OSTREAM, "\"%s\" - ABOUT TO CALL CLOSE\n", filename);

  if ( close(fd) == -1 )
    return -1;

  kdprintf(KR_OSTREAM, "\"%s\" - ABOUT TO CALL OPEN AGAIN\n", filename);

  fd = open(filename, O_RDWR, 0666);
  if (fd == -1)
    return -1;

  kdprintf(KR_OSTREAM, "\"%s\" - ABOUT TO CALL READ\n", filename);

  if ( read(fd, buf, strlen(filename)) == -1 )
    return -1;

  buf[strlen(filename)] = 0;
  
  kdprintf(KR_OSTREAM, "\"%s\" - got back: \"%s\"\n", filename, buf);
  kdprintf(KR_OSTREAM, "\"%s\" - ABOUT TO CALL CLOSE\n", filename);

  return close(fd);
}

void
main()
{
  init();
  
  if ( dotest("foobar") == -1 )
    kdprintf(KR_OSTREAM, "!!! fails with errno %d\n", errno);

  if ( dotest("bletch/bad") )
    kdprintf(KR_OSTREAM, "!!! fails with errno %d (should be %d)\n",
	     errno, ENOENT);

  mkdir("bletch", 0755);
  
  if ( dotest("bletch/good") )
    kdprintf(KR_OSTREAM, "!!! fails with errno %d (should be %d)\n",
	     errno, ENOENT);

  mkdir("bletch/blurbus", 0755);
  
  if ( dotest("bletch/good/better") )
    kdprintf(KR_OSTREAM, "!!! fails with errno %d (should be %d)\n",
	     errno, ENOENT);

  if ( dotest("/star") )
    kdprintf(KR_OSTREAM, "!!! fails with errno %d\n", errno);

  if ( dotest("foobar/baz") )
    kdprintf(KR_OSTREAM, "!!! fails with errno %d\n", errno);
}
