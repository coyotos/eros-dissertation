/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NodeKey.h>
#include <eros/PageKey.h>
#include <eros/StdKeyType.h>
#include <eros/SleepKey.h>
#include <domain/SpaceBankKey.h>
#include <domain/domdbg.h>

#define NODE_ALLOC_CHECK

#ifdef GNU
#define INLINE inline
#else
#define INLINE
#endif

/* define the following if you want the test to stop after each stage */
/* #define STPRINTF kdprintf */
/* #define VERBOSE */

#ifndef STPRINTF
#define STPRINTF kprintf
#endif

#define KR_ZERO       0
#define KR_CONSOLEKEY 8
#define KR_SLEEPKEY   9
#define KR_SPACEBANK  7
#define KR_KEYBITS   10

#define KR_PARBANK    5
#define KR_SUBBANK    6

#define KR_TMP0   1
#define KR_TMP1   2
#define KR_TMP2   3
#define KR_TMP3   4


#define SLEEP_TME  0 /* (100/7) */


#define NUM_LEVELS (3u)
/* nnumber of levels in the node tree */

#define NUM_ITEMS (1u << (4*NUM_LEVELS))
/* == 16 ^ NUM_LEVELS */

/**Handle the stack stuff**/
const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x10000;

uint16_t
PageAllocTest(uint32_t sbKey, uint32_t deallocManually)
{
  uint32_t nodeIndex;
  uint32_t subnodeIndex;
  uint32_t pageIndex;

  uint32_t pagesAllocated = 0;
  
  /* page alloc test:
   *   Attempt to allocate 1 meg of memory (16*16 pages), storing the
   *  pages as leaves in a two-level tree of nodes.  If we get a
   *  limit error at any time (i.e. the bank is out of space), we
   *  stop.  If we get any other error, we return failure.
   *
   *   Once we are done allocating, we deallocate everything, starting
   *  from the first page.  Once we reach a number key, we know we are
   *  done.
   */

  /* KR_TMP0 holds top node,
   * KR_TMP1 holds current subnode,
   * KR_TMP2 holds current subsubnode,
   * KR_TMP3 holde current page
   */

  uint32_t result; /* to hold return codes */
  
  /* first, create the initial node */

  STPRINTF(KR_CONSOLEKEY,"SpaceBankTest: PageAllocTest start\n");
#ifdef VERBOSE  
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: PageAllocTest creating top node\n");
#endif
  
  result = spcbank_buy_nodes(sbKey, 1, KR_TMP0, 0, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "Error buying top node (0x%08x)\n",result);
    return 0;
  }

#ifdef VERBOSE  
  /* show off the key */
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP0);
  
  sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif

  for (nodeIndex = 0; nodeIndex < EROS_NODE_SIZE; nodeIndex++) {
    /*  allocate a new node, place it in the top node, then fill it
     * with pages.
     */

#ifdef VERBOSE  
    kprintf(KR_CONSOLEKEY,
	    "SpaceBankTest: PageAllocTest creating node %d\n",nodeIndex);
#endif

    result = spcbank_buy_nodes(sbKey, 1, KR_TMP1, 0, 0);

    if (result != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "Error buying node %d (0x%08x)\n",nodeIndex,result);
      return 0;
    }

#ifdef VERBOSE
    /* show off the key */
    ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP1);
#endif

    result = node_swap(KR_TMP0, nodeIndex, KR_TMP1, KR_ZERO);

    if (result != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "sb test line %d: Error 0x%08x (%d) inserting node %d into top node(0x%08x)\n",
	       __LINE__,
	       result, result,
	       nodeIndex,
	       result);
      return 0;
    }
    
#ifdef VERBOSE
    sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif

    for (subnodeIndex = 0; subnodeIndex < EROS_NODE_SIZE; subnodeIndex++) {
      /*  allocate a new node, place it in the top node, then fill it
       * with pages.
       */
      
#ifdef VERBOSE
      kprintf(KR_CONSOLEKEY,
	      "SpaceBankTest: PageAllocTest creating node %d:%d\n",
	      nodeIndex,subnodeIndex);
#endif
      
      result = spcbank_buy_nodes(sbKey, 1, KR_TMP2, 0, 0);

      if (result != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "Error buying node ndx=%d subndx=%d (0x%08x)\n",
		 nodeIndex, subnodeIndex, result);
	return 0;
      }
      
#ifdef VERBOSE
      /* show off the key */
      ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP2);
#endif
      
      result = node_swap(KR_TMP1, subnodeIndex, KR_TMP2, KR_ZERO);

      if (result != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "test Line %d: Error 0x%08x (%d) inserting node\n",
		 __LINE__, result, result,
		 nodeIndex,
		 result);
	return 0;
      }
    
#ifdef NODE_ALLOC_CHECK
      if (nodeIndex == 0 && subnodeIndex == 0)
	node_copy(KR_TMP1, subnodeIndex, 17);
      
      result = spcbank_identify_node(sbKey, 17);
      if (result != RC_OK)
	kdprintf(KR_CONSOLEKEY, "Node 0:0 seems to have disappeared\n");
#endif

#ifdef VERBOSE
      sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif

      for (pageIndex = 0; pageIndex < EROS_NODE_SIZE; pageIndex++) {
#ifdef VERBOSE
	kprintf(KR_CONSOLEKEY,
		"SpaceBankTest: PageAllocTest allocating page %d:%d:%d\n",
		nodeIndex,
		subnodeIndex,
		pageIndex);
#endif

	result = spcbank_buy_data_pages(sbKey, 1, KR_TMP3, 0, 0);

	if (result != RC_OK) {
	  if (result == RC_SB_LimitReached) {
#ifdef VERBOSE
	    kprintf(KR_CONSOLEKEY,
		    "SpaceBankTest: PageAllocTest limit reached\n",
		    nodeIndex,
		    pageIndex);
#endif
	    goto doneAllocating;
	  } else {
	    kdprintf(KR_CONSOLEKEY,
		     "Error buying page %d:%d:%d (0x%08x)\n",
		     nodeIndex,
		     subnodeIndex,
		     pageIndex,
		     result);
	    return 0;
	  }
	}

#ifdef VERBOSE
	ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP3);
#endif

#ifdef NODE_ALLOC_CHECK
	result = spcbank_identify_node(sbKey, 17);
	if (result != RC_OK)
	  kdprintf(KR_CONSOLEKEY, "Node 0:0 seems to have disappeared\n");
#endif

	/* modify the thing */
	page_write(KR_TMP3, 5, &pagesAllocated, sizeof(pagesAllocated));

	/* put it in the tree */
	result = node_swap(KR_TMP2, pageIndex, KR_TMP3, KR_ZERO);

	if (result != RC_OK) {
	  kdprintf(KR_CONSOLEKEY,
		   "Error inserting page (0x%08x)\n",
		   pageIndex,
		   nodeIndex,
		   result);
	  return 0;
	}
	pagesAllocated++;
#ifdef VERBOSE
	sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
      }
    }
  }
  
doneAllocating:

  STPRINTF(KR_CONSOLEKEY,
	   "SpaceBankTest: Finished allocating.  Allocated %u pages (%ukB)\n",
	   pagesAllocated,
	   pagesAllocated * (EROS_PAGE_SIZE >> 10));

#ifdef VERBOSE
  sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif

  if (!deallocManually) {
    /* destroy the bank and it's space */
    result = spcbank_destroy_bank(sbKey, 1);
    return (result == RC_OK);
  }
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest: PageAllocTest now deallocating storage.\n");

  for (nodeIndex = 0; nodeIndex < EROS_NODE_SIZE; nodeIndex++) {
    uint32_t kt;
    
    /* grab the node key */
#ifdef VERBOSE 
   kprintf(KR_CONSOLEKEY,
	    "SpaceBankTest: PageAllocTest: Grabbing Node %d\n",
	    nodeIndex);
#endif    
    result = node_copy(KR_TMP0, nodeIndex, KR_TMP1);
    if (result != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "Error grabbing node %d (0x%08x)\n",
	       nodeIndex,
	       result);
      return 0;
    }

#ifdef VERBOSE 
    ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP1);
#endif
    kt = key_kt(KR_TMP1);
    if (kt != AKT_Node) {
      if (kt == AKT_Number) {
	kprintf(KR_CONSOLEKEY,
		"Finished deallocating pages\n");
	break; /* BREAK */
      } else {
	kdprintf(KR_CONSOLEKEY,
		 "Error: invalid kt (0x%08x) for node %d\n",
		 kt,
		 nodeIndex);
	return 0;
      }
    }

#ifdef VERBOSE 
    sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
    
    for (subnodeIndex = 0; subnodeIndex < EROS_NODE_SIZE; subnodeIndex++) {
      uint32_t kt;
    
      /* grab the node key */
#ifdef VERBOSE 
      kprintf(KR_CONSOLEKEY,
	      "SpaceBankTest: PageAllocTest: Grabbing Node %d:%d\n",
	      nodeIndex,subnodeIndex);
#endif
    
      result = node_copy(KR_TMP1, subnodeIndex, KR_TMP2);
      if (result != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "Error grabbing node (0x%08x)\n",
		 result);
	return 0;
      }
      
#ifdef VERBOSE 
      ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP2);
#endif
      kt = key_kt(KR_TMP2);
      if (kt != AKT_Node) {
	if (kt == AKT_Number) {
	  break; /* BREAK */
	} else {
	  kdprintf(KR_CONSOLEKEY,
		   "Error: invalid kt (0x%08x) for node\n",
		   kt,
		   nodeIndex);
	  return 0;
	}
      }

#ifdef VERBOSE 
      sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif      
      for (pageIndex = 0; pageIndex < EROS_NODE_SIZE; pageIndex++) {
	/* grab the page key */
#ifdef VERBOSE  
	kprintf(KR_CONSOLEKEY,
		"SpaceBankTest: PageAllocTest: Grabbing page %d:%d:%d\n",
		nodeIndex,
		subnodeIndex,
		pageIndex);
#endif	
	result = node_copy(KR_TMP2, pageIndex, KR_TMP3);
	if (result != RC_OK) {
	  kdprintf(KR_CONSOLEKEY,
		   "Error grabbing node (0x%08x)\n",
		   result);
	  return 0;
	}
	
#ifdef VERBOSE
	ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP3);
#endif      
	kt = key_kt(KR_TMP3);
	if (kt != AKT_DataPage) {
	  if (kt == AKT_Number) {
	    /* done */
	    break; /* BREAK */
	  } else {
	    kdprintf(KR_CONSOLEKEY,
		     "Error: invalid kt (0x%08x) for page %d:%d:%d\n",
		     kt,
		     nodeIndex,
		     subnodeIndex,
		     pageIndex);
	    return 0;
	  }
	}
	
	/* free the page */
	result = spcbank_return_data_page(sbKey, KR_TMP3);
	if (result != RC_OK) {
	  kdprintf(KR_CONSOLEKEY,
		   "Error returning page (0x%08x)\n",
		   result);
	  return 0;
	}
#ifdef VERBOSE	
	kprintf(KR_CONSOLEKEY,
		"SpaceBankTest: Page %d:%d:%d successfully returned.\n",
		nodeIndex,
		subnodeIndex,
		pageIndex);
	
	sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
      }

      /* all the pages are dealloced, now free the node. */
      result = spcbank_return_node(sbKey, KR_TMP2);
      if (result != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "Error returning node %d:%d (0x%08x)\n",
		 nodeIndex,
		 subnodeIndex,
		 result);
	return 0;
      }
#ifdef VERBOSE 
      kprintf(KR_CONSOLEKEY,
	      "SpaceBankTest: Node %d successfully returned.\n",
	      nodeIndex);
      
      sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
    }
    
    /* all the pages are dealloced, now free the node. */
    result = spcbank_return_node(sbKey, KR_TMP1);
    if (result != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "Error returning node %d (0x%08x)\n",
	       nodeIndex,
	       result);
      return 0;
    }
#ifdef VERBOSE 
    kprintf(KR_CONSOLEKEY,
	    "SpaceBankTest: Node %d successfully returned.\n",
	    nodeIndex);
      
    sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
  }

#ifdef VERBOSE 
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest: Finished deallocating.  Rescinding top node.\n");
#endif
  result = spcbank_return_node(sbKey, KR_TMP0);
  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "Error returning top node (0x%08x)\n",
	     result);
    return 0;
  }
#ifdef VERBOSE 
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest: Top node returned.\n");
#endif  
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest:  PageAllocTest complete.\n");
  
  sl_sleep(KR_SLEEPKEY, SLEEP_TME);
  return 1;
}

uint16_t
NodeAllocTest(uint32_t sbKey, uint32_t deallocManually)
{
  uint32_t nodeIndex;
  uint32_t subnodeIndex;
  uint32_t theNodeIndex;

  uint32_t nodesAllocated = 0;
  
  /* KR_TMP0 holds top node,
   * KR_TMP1 holds current subnode,
   * KR_TMP2 holds current subsubnode,
   * KR_TMP3 holde current node
   */

  uint32_t result; /* to hold return codes */
  
  /* first, create the initial node */

  kprintf(KR_CONSOLEKEY,"SpaceBankTest: NodeAllocTest start\n");
#ifdef VERBOSE  
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: NodeAllocTest creating top node\n");
#endif
  
  result = spcbank_buy_nodes(sbKey, 1, KR_TMP0, 0, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "Error buying top node (0x%08x)\n",result);
    return 0;
  }

#ifdef VERBOSE  
  /* show off the key */
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP0);
  
  sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif

  for (nodeIndex = 0; nodeIndex < EROS_NODE_SIZE; nodeIndex++) {
    /*  allocate a new node, place it in the top node, then fill it
     * with nodes.
     */

#ifdef VERBOSE  
    kprintf(KR_CONSOLEKEY,
	    "SpaceBankTest: NodeAllocTest creating node %d\n",nodeIndex);
#endif

    result = spcbank_buy_nodes(sbKey, 1, KR_TMP1, 0, 0);

    if (result != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "Error buying node %d (0x%08x)\n",nodeIndex,result);
      return 0;
    }

#ifdef VERBOSE
    /* show off the key */
    ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP1);
#endif

    result = node_swap(KR_TMP0, nodeIndex, KR_TMP1, KR_ZERO);
    
    if (result != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "sb test line %d: Error 0x%08x (%d) inserting node %d into top node(0x%08x)\n",
	       __LINE__, result, result,
	       nodeIndex,
	       result);
      return 0;
    }
    
#ifdef VERBOSE
    sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif

    for (subnodeIndex = 0; subnodeIndex < EROS_NODE_SIZE; subnodeIndex++) {
      /*  allocate a new node, place it in the top node, then fill it
       * with nodes.
       */
      
#ifdef VERBOSE
      kprintf(KR_CONSOLEKEY,
	      "SpaceBankTest: NodeAllocTest creating node %d:%d\n",
	      nodeIndex,subnodeIndex);
#endif
      
      result = spcbank_buy_nodes(sbKey, 1, KR_TMP2, 0, 0);

      if (result != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "Error buying node ndx=%d subndx=%d (0x%08x)\n",
		 nodeIndex, subnodeIndex, result);
	return 0;
      }
      
#ifdef VERBOSE
      /* show off the key */
      ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP2);
#endif
      
      result = node_swap(KR_TMP1, subnodeIndex, KR_TMP2, KR_ZERO);

      if (result != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "sb test line %d: Error 0x%08x (%d) inserting node\n",
		 __LINE__, result, result,
		 nodeIndex,
		 result);
	return 0;
      }
    
#ifdef VERBOSE
      sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif

      for (theNodeIndex = 0; theNodeIndex < EROS_NODE_SIZE; theNodeIndex++) {
#ifdef VERBOSE
	kprintf(KR_CONSOLEKEY,
		"SpaceBankTest: NodeAllocTest allocating node %d:%d:%d\n",
		nodeIndex,
		subnodeIndex,
		theNodeIndex);
#endif

	result = spcbank_buy_nodes(sbKey, 1, KR_TMP3, 0, 0);

	if (result != RC_OK) {
	  if (result == RC_SB_LimitReached) {
#ifdef VERBOSE
	    kprintf(KR_CONSOLEKEY,
		     "SpaceBankTest: NodeAllocTest limit reached\n");
#endif
	    goto doneAllocating;
	  } else {
	    kdprintf(KR_CONSOLEKEY,
		     "Error buying node %d:%d (0x%08x)\n",
		     nodeIndex,
		     theNodeIndex,
		     result);
	    return 0;
	  }
	}

#ifdef VERBOSE
	ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP3);
#endif

	/* modify the thing */
	node_swap(KR_TMP3, 0, KR_SLEEPKEY, KR_ZERO);
	
	result = node_swap(KR_TMP2, theNodeIndex, KR_TMP3, KR_ZERO);

	if (result != RC_OK) {
	  kdprintf(KR_CONSOLEKEY,
		   "sb test line %d: Error 0x%08x (%d) inserting node (0x%08x)\n",
		   __LINE__, result, result,
		   nodeIndex,
		   theNodeIndex,
		   result);
	  return 0;
	}
	nodesAllocated++;
#ifdef VERBOSE
	sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
      }
    }
  }
  
doneAllocating:

  STPRINTF(KR_CONSOLEKEY,
	   "SpaceBankTest: Finished allocating.  Allocated %u nodes\n",
	   nodesAllocated);

#ifdef VERBOSE
  sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif

  if (!deallocManually) {
    /* destroy the bank and it's space */
    result = spcbank_destroy_bank(sbKey, 1);
    return (result == RC_OK);
  }
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest: NodeAllocTest now deallocating storage.\n");

  for (nodeIndex = 0; nodeIndex < EROS_NODE_SIZE; nodeIndex++) {
    uint32_t kt;
    
    /* grab the node key */
#ifdef VERBOSE 
   kprintf(KR_CONSOLEKEY,
	    "SpaceBankTest: NodeAllocTest: Grabbing Node %d\n",
	    nodeIndex);
#endif    
    result = node_copy(KR_TMP0, nodeIndex, KR_TMP1);
    if (result != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "Error grabbing node %d (0x%08x)\n",
	       nodeIndex,
	       result);
      return 0;
    }

#ifdef VERBOSE 
    ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP1);
#endif
    kt = key_kt(KR_TMP1);
    if (kt != AKT_Node) {
      if (kt == AKT_Number) {
	kprintf(KR_CONSOLEKEY,
		"Finished deallocating nodes\n");
	break; /* BREAK */
      } else {
	kdprintf(KR_CONSOLEKEY,
		 "Error: invalid kt (0x%08x) for node %d\n",
		 kt,
		 nodeIndex);
	return 0;
      }
    }

#ifdef VERBOSE 
    sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
    
    for (subnodeIndex = 0; subnodeIndex < EROS_NODE_SIZE; subnodeIndex++) {
      uint32_t kt;
    
      /* grab the node key */
#ifdef VERBOSE 
      kprintf(KR_CONSOLEKEY,
	      "SpaceBankTest: NodeAllocTest: Grabbing Node %d:%d\n",
	      nodeIndex,subnodeIndex);
#endif
    
      result = node_copy(KR_TMP1, subnodeIndex, KR_TMP2);
      if (result != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "Error grabbing node (0x%08x)\n",
		 result);
	return 0;
      }
      
#ifdef VERBOSE 
      ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP2);
#endif
      kt = key_kt(KR_TMP2);
      if (kt != AKT_Node) {
	if (kt == AKT_Number) {
	  break; /* BREAK */
	} else {
	  kdprintf(KR_CONSOLEKEY,
		   "Error: invalid kt (0x%08x) for node\n",
		   kt,
		   nodeIndex);
	  return 0;
	}
      }

#ifdef VERBOSE 
      sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif      
      for (theNodeIndex = 0; theNodeIndex < EROS_NODE_SIZE; theNodeIndex++) {
	/* grab the node key */
#ifdef VERBOSE  
	kprintf(KR_CONSOLEKEY,
		"SpaceBankTest: NodeAllocTest: Grabbing node %d:%d:%d\n",
		nodeIndex,
		subnodeIndex,
		theNodeIndex);
#endif	
	result = node_copy(KR_TMP2, theNodeIndex, KR_TMP3);
	if (result != RC_OK) {
	  kdprintf(KR_CONSOLEKEY,
		   "Error grabbing node (0x%08x)\n",
		   result);
	  return 0;
	}
	
#ifdef VERBOSE
	ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP3);
#endif      
	kt = key_kt(KR_TMP3);
	if (kt != AKT_Node) {
	  if (kt == AKT_Number) {
	    /* done */
	    break; /* BREAK */
	  } else {
	    kdprintf(KR_CONSOLEKEY,
		     "Error: invalid kt (0x%08x) for node %d:%d:%d\n",
		     kt,
		     nodeIndex,
		     subnodeIndex,
		     theNodeIndex);
	    return 0;
	  }
	}
	
	/* free the node */
	result = spcbank_return_node(sbKey, KR_TMP3);
	if (result != RC_OK) {
	  kdprintf(KR_CONSOLEKEY,
		   "Error returning node (0x%08x)\n",
		   result);
	  return 0;
	}
#ifdef VERBOSE	
	kprintf(KR_CONSOLEKEY,
		"SpaceBankTest: Node %d:%d:%d successfully returned.\n",
		nodeIndex,
		subnodeIndex,
		theNodeIndex);
	
	sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
      }

      /* all the nodes are dealloced, now free the node. */
      result = spcbank_return_node(sbKey, KR_TMP2);
      if (result != RC_OK) {
	kdprintf(KR_CONSOLEKEY,
		 "Error returning node %d:%d (0x%08x)\n",
		 nodeIndex,
		 subnodeIndex,
		 result);
	return 0;
      }
#ifdef VERBOSE 
      kprintf(KR_CONSOLEKEY,
	      "SpaceBankTest: Node %d successfully returned.\n",
	      subnodeIndex);
      
      sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
    }
    
    /* all the nodes are dealloced, now free the node. */
    result = spcbank_return_node(sbKey, KR_TMP1);
    if (result != RC_OK) {
      kdprintf(KR_CONSOLEKEY,
	       "Error returning node %d (0x%08x)\n",
	       nodeIndex,
	       result);
      return 0;
    }
#ifdef VERBOSE 
    kprintf(KR_CONSOLEKEY,
	    "SpaceBankTest: Node %d successfully returned.\n",
	    nodeIndex);
      
    sl_sleep(KR_SLEEPKEY, SLEEP_TME);
#endif
  }

#ifdef VERBOSE 
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest: Finished deallocating.  Rescinding top node.\n");
#endif
  result = spcbank_return_node(sbKey, KR_TMP0);
  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "Error returning top node (0x%08x)\n",
	     result);
    return 0;
  }
#ifdef VERBOSE 
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest: Top node returned.\n");
#endif  
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest:  NodeAllocTest complete.\n");
  
  sl_sleep(KR_SLEEPKEY, SLEEP_TME);
  return 1;
}

uint16_t 
SpaceBankInit()
{
  uint32_t Result;


  kprintf(KR_CONSOLEKEY, "Spacebank test domain: identifying spacebank.\n");
  
  Result = key_kt(KR_SPACEBANK);
             /* this step will also initialize the spacebank */
  
  if (Result == AKT_SpaceBank) {
    STPRINTF(KR_CONSOLEKEY, "Spacebank verified. \n");
  } else {
    kprintf(KR_CONSOLEKEY,
	    "Spacebank NOT verified.\n"
	    "Result from spacebank was 0x%08x\n",
	    Result);
    return 0;
  }      

  return 1;
}

uint16_t
SubBankTest()
{
  uint32_t result;
  kprintf(KR_CONSOLEKEY,
	  "SpaceBankTest: Starting SubBankTest\n");

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating parent\n");
#endif
  result = spcbank_create_subbank(KR_SPACEBANK, KR_PARBANK);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error creating Parent! (0x%08x)\n",
	     result);
    return 0;
  }

  result = spcbank_verify_bank(KR_SPACEBANK, KR_PARBANK);
  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Verification of new bank failed! (0x%08x)\n",
	     result);
  }
#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating first child\n");
#endif
  result = spcbank_create_subbank(KR_PARBANK, KR_SUBBANK);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error creating first sub-bank! (0x%08x)\n",
	     result);
    return 0;
  }

  result = spcbank_verify_bank(KR_PARBANK, KR_SUBBANK);
  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Verification of new bank failed! (0x%08x)\n",
	     result);
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating first two pages\n");
#endif
  result = spcbank_buy_data_pages(KR_SUBBANK, 2, KR_TMP0, KR_TMP1, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error buying first two pages! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP0);
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP1);
#endif
  

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating first two nodes\n");
#endif

  result = spcbank_buy_nodes(KR_SUBBANK, 2, KR_TMP2, KR_TMP3, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error buying first two nodes! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP2);
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP3);
#endif
  

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: destroying 1st subbank + space\n");
#endif

  /* destroy bank and space */
  result = spcbank_destroy_bank(KR_SUBBANK, 1);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error destroying first subbank and space! (0x%08x)\n",
	     result);
    return 0;
  }

  if (key_kt(KR_TMP3) != AKT_Number) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Keys not zeroed on destroy first bank and space\n",
	     result);
    return 0;    
  }

  result = spcbank_verify_bank(KR_SPACEBANK, KR_SUBBANK);
  if (result == RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Verification of destroyed bank key succedded!\n",
	     result);
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating 2nd subbank\n");
#endif
  result = spcbank_create_subbank(KR_PARBANK, KR_SUBBANK);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error creating second sub-bank! (0x%08x)\n",
	     result);
    return 0;
  }

  result = spcbank_verify_bank(KR_SPACEBANK, KR_SUBBANK);
  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Verification of new bank failed! (0x%08x)\n",
	     result);
  }
#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating second two pages\n");
#endif
  result = spcbank_buy_data_pages(KR_SUBBANK, 2, KR_TMP0, KR_TMP1, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error buying second two pages! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP0);
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP1);
#endif
  
#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: creating second two nodes\n");
#endif

  result = spcbank_buy_nodes(KR_SUBBANK, 2, KR_TMP2, KR_TMP3, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error buying second two nodes! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP2);
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_TMP3);
#endif
  

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: destroying 2nd subbank\n");
#endif

  /* destroy bank, returning space to parent*/
  result = spcbank_destroy_bank(KR_SUBBANK, 0);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error destroying second subbank! (0x%08x)\n",
	     result);
    return 0;
  }

  if (key_kt(KR_TMP3) != AKT_Node) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Keys zeroed on destroy 2nd bank return to parent!\n",
	     result);
    return 0;    
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: returning a page and a node\n");
#endif
  /* return one node and one page to the parent bank */
  result = (spcbank_return_data_page(KR_PARBANK, KR_TMP0)
	    || spcbank_return_node(KR_PARBANK, KR_TMP2));

  if (result) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error returning items to parent bank\n");
    return 0;
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: destroying parent bank and space\n");
#endif
  
  result = spcbank_destroy_bank(KR_PARBANK, 1);

  if (result != RC_OK) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Error destroying parent bank and space! (0x%08x)\n",
	     result);
    return 0;
  }

#ifdef VERBOSE
  kprintf(KR_CONSOLEKEY,"SpaceBankTest: verifying destruction\n");
#endif

  if (key_kt(KR_TMP3) != AKT_Number || key_kt(KR_TMP1) != AKT_Number) {
    kdprintf(KR_CONSOLEKEY,
	     "SpaceBankTest: Keys not zeroed on parent destroy bank and space\n",
	     result);
    return 0;    
  }

  
  return 1;
}

void
main()
{
  uint32_t pass = 0;
  /*
   * simple spacebank test domain.  the test
   * is divided into three stages: init, nodes, pages.
   */

  sl_sleep(KR_SLEEPKEY, SLEEP_TME);
  if (!SpaceBankInit())
    kdprintf(KR_CONSOLEKEY, "\nSpacebank test domain.  Init Failure.\n");
    
  if (NodeAllocTest(KR_SPACEBANK, 1)
      && PageAllocTest(KR_SPACEBANK, 1)
      && SubBankTest())
    kprintf(KR_CONSOLEKEY, "\nSpacebank test domain:  Success.\n");
  else
    kprintf(KR_CONSOLEKEY, "\nSpacebank test domain.  Failure.\n");

  kprintf(KR_CONSOLEKEY, "Starting stress test?\n");

  for(;;) {
    ++pass;
    kprintf(KR_CONSOLEKEY, "Stress test pass %d\n", pass);
    NodeAllocTest(KR_SPACEBANK, 1);
    PageAllocTest(KR_SPACEBANK, 1);
    SubBankTest();
  }

  return;

}
