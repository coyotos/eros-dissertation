/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>
#include <domain/SpaceBankKey.h>
#include <domain/domdbg.h>
#include <eros/SleepKey.h>

#include <eros/NumberKey.h>
#include <eros/ProcessKey.h>
#include <eros/DomCtlInfo.h>

#define KR_ZERO 0
#define KR_ME 7			/* a key to my domain */
#define KR_CONSOLEKEY 8
#define KR_SLEEPKEY 9
#define KR_KEYBITS 10
#define KR_AS2 11

#define KR_TMP0 1
#define KR_TMP1 2

#define KR_RK0     12
#define KR_RK1     13
#define KR_RK2     14
#define KR_RETURN  15

#define NEWPC 0x2		/* according to domcrt0 */

void main()
{
  Message msg;
  
  kprintf (KR_CONSOLEKEY, "lobotomize test: hello as1\n");

  msg.snd_key0 = KR_AS2;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;

  msg.snd_w1 = NEWPC;

  msg.rcv_key0 = 12;
  msg.rcv_key1 = 13;
  msg.rcv_key2 = 14;
  msg.rcv_key3 = 15;
  msg.rcv_len = 0;		/* no data returned */

  msg.snd_invKey = KR_ME;
  msg.snd_code = OC_Process_SwapMemory32;

  (void) CALL(&msg);

				/* should never get here */
  
  kprintf (KR_CONSOLEKEY, "lobotomize test: as1 failed\n");

}
