/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/SleepKey.h>
#include <domain/domdbg.h>

// The purpose of this domain is to issue escape sequences to the
// console to test ANSI terminal emulation.  It is meant to be watched
// interactively.
#define KR_ZERO 0
#define KR_NUM1 1
#define KR_OSTREAM 8
#define KR_SLEEPKEY 9
#define KR_KEYBITS 10

const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x21000;

void
main()
{
  kprintf(KR_OSTREAM, "WAITING TO STABILIZE...\n");
  sl_sleep(KR_SLEEPKEY, 5000);
  
  kprintf(KR_OSTREAM, "\033[H\033[J");	/* clear screen */
  kprintf(KR_OSTREAM, "Here we go...\n\n");

  ShowKey(KR_OSTREAM, KR_KEYBITS, KR_ZERO);

  ShowKey(KR_OSTREAM, KR_KEYBITS, KR_NUM1);

  ShowKey(KR_OSTREAM, KR_KEYBITS, KR_OSTREAM);

  ShowKey(KR_OSTREAM, KR_KEYBITS, KR_SLEEPKEY);

  ShowKey(KR_OSTREAM, KR_KEYBITS, KR_KEYBITS);
  
#if 0
  buf = strcat(msgbuf, "FLAGS are: ");
  buf = hexcat(buf, flags);
  buf = strcat("\r\n");
#endif

#if 0
  for(;;)
    ;
#endif
}
