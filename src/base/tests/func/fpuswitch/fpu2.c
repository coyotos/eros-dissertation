/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/SleepKey.h>
#include <domain/domdbg.h>
#include <eros/SysTraceKey.h>


#define KR_ZERO 0
#define KR_SLEEP 9
#define KR_OSTREAM 10
#define KR_TRACE 11

const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x20000;

extern float fv;
extern float f;
extern float fzero;

void main()
{
  kprintf(KR_OSTREAM, "fpu2.c: About to sleep:\n");
  sl_sleep(KR_SLEEP, 5000);

  kprintf(KR_OSTREAM, "fpu2.c: Steal FPU from fpu.c\n");
  fv = f * 5.7;
  kprintf(KR_OSTREAM, "fpu2.c: exits\n");
}
