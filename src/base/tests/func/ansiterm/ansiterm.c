/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/SleepKey.h>
#include <domain/domdbg.h>

/**Handle the stack stuff**/
const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x21000;

// The purpose of this domain is to issue escape sequences to the
// console to test ANSI terminal emulation.  It is meant to be watched
// interactively.
#define KR_ZERO 0
#define KR_OSTREAM 8
#define KR_SLEEPKEY 9

int
main()
{
  kprintf(KR_OSTREAM, "THIS IS A TEST\n");
  kprintf(KR_OSTREAM, "\033[HTHIS SHOULD APPEAR IN UPPER LEFT\n");
#if 1
  for(;;) {
    kprintf(KR_OSTREAM, "In 4 seconds I will clear the screen\n");
    sl_sleep(KR_SLEEPKEY, 4000);
    kprintf(KR_OSTREAM, "\033[H\033[J");		/* clear screen */
    kprintf(KR_OSTREAM, "This should appear in the top left corner.\n");
    kprintf(KR_OSTREAM, "And this on the second line.\n");
    sl_sleep(KR_SLEEPKEY, 4000);
    kprintf(KR_OSTREAM, "Now I place an X at the end of line two using exact positioning.\n");
    kprintf(KR_OSTREAM, "\033[2;80HX");
    kprintf(KR_OSTREAM, "And a Y beneath it via MOVE CURSOR FORWARD.\033[K\n");
    kprintf(KR_OSTREAM, "\033[2;80HX");
    kprintf(KR_OSTREAM, "\033[79CY");
    sl_sleep(KR_SLEEPKEY, 4000);
    kprintf(KR_OSTREAM, "In a few seconds I will clear to EOL from 'this' in line 2\n");
    kprintf(KR_OSTREAM, "That will test both absolute positioning and the cleareol function.");
    sl_sleep(KR_SLEEPKEY, 4000);
    kprintf(KR_OSTREAM, "\033[2;5H\033[K");
    kprintf(KR_OSTREAM, "\033[6;1HWasn't that cool?  Let's do it again...\n");
  }
#endif

  sl_sleep(KR_SLEEPKEY, 600000);

  return 0;
}
