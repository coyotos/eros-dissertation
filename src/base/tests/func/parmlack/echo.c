/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Echo -- domain that simply echos what you send it.
 *
 * Key Registers:
 *
 * KR12: arg0
 * KR13: arg1
 * KR14: arg2
 * KR15: arg3
 */

#include <eros/target.h>
#include <eros/Invoke.h>

#define KR_ZERO 0
#define KR_LOGKEY 9
#define KR_RK0   12
#define KR_RK1   13
#define KR_RK2   14
#define KR_RESUME  15		/* so don't use it for anything else! */

uint8_t buf[EROS_PAGE_SIZE];

int
ProcessRequest(Message *msg)
{
  msg->snd_len = msg->rcv_len;
  msg->snd_data = msg->rcv_data;

  msg->snd_key0 = msg->rcv_key0;
  msg->snd_key1 = msg->rcv_key1;
  msg->snd_key2 = msg->rcv_key2;
  msg->snd_key3 = msg->rcv_key3;

  {
    Message logmsg;

    logmsg.snd_key0 = KR_ZERO;
    logmsg.snd_key1 = KR_ZERO;
    logmsg.snd_key2 = KR_ZERO;
    logmsg.snd_key3 = KR_ZERO;
    logmsg.snd_data = msg->rcv_data;
    logmsg.snd_len = msg->rcv_len;

    ((uint8_t *) logmsg.snd_data)[0] = 'E';

    logmsg.rcv_key0 = KR_ZERO;
    logmsg.rcv_key1 = KR_ZERO;
    logmsg.rcv_key2 = KR_ZERO;
    logmsg.rcv_key3 = KR_ZERO;
    logmsg.rcv_len = 0;		/* no data returned */

    logmsg.snd_code = 1;
    logmsg.snd_invKey = KR_LOGKEY;
    (void) CALL(&logmsg);
  }
  
  /* leave receive info unchanged, but reset the length limit: */
  msg->rcv_len = EROS_PAGE_SIZE;
       
  msg->snd_code = msg->rcv_code + 1;
  return 1;
}


void
main()
{
  Message msg;
  
  msg.snd_invKey = KR_ZERO;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = KR_RK0;
  msg.rcv_key1 = KR_RK1;
  msg.rcv_key2 = KR_RK2;
  msg.rcv_key3 = KR_RESUME;
  msg.rcv_data = (void *) buf + 0xffb; /* deliberately bogus! */
  msg.rcv_len = EROS_PAGE_SIZE;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  do {
    RETURN(&msg);
 } while ( ProcessRequest(&msg) );
}
