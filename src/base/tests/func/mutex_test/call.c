/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/SleepKey.h>
#include <domain/domdbg.h>

#define KR_ZERO 0
#define KR_INIT_KEYS 1
#define KR_SELF 2
#define KR_BANK 4
#define KR_MY_SCHED 5
#define KR_LOCK 6
#define KR_RELEASE 7
#define KR_ECHO 8
#define KR_CONSOLE 9
#define KR_MTX_CON 10
#define KR_SCHED 12
#define KR_SLEEP 13

#define STRING "Hello from call.c"
const char *str = STRING;

const Word	__rt_stack_pages=1;
const Word	__rt_stack_pointer=0x10000;

int
build_lock(int kr_mutcre_dom,
	   int kr_spc_bank,
	   int kr_sched_cap,
	   int kr_new_lock);

int
get_lock(int kr_lock, int kr_release);

#define release_lock(kr_release) get_lock((kr_release), (kr_release))

void
main()
{
  Message msg;

  node_copy(KR_INIT_KEYS, KR_ECHO, KR_ECHO);
  node_copy(KR_INIT_KEYS, KR_CONSOLE, KR_CONSOLE);
  node_copy(KR_INIT_KEYS, KR_MTX_CON, KR_MTX_CON);
  node_copy(KR_INIT_KEYS, KR_SCHED, KR_SCHED);
  node_copy(KR_INIT_KEYS, KR_SLEEP, KR_SLEEP);
  kprintf(KR_CONSOLE, "call: building lock...\n");
  kprintf(KR_CONSOLE, "call: build_lock returned %08x\n",
	  build_lock(KR_MTX_CON, KR_BANK, KR_SCHED, KR_LOCK));
  kprintf(KR_CONSOLE, "call: acquiring lock...\n");
  kprintf(KR_CONSOLE, "call: get_lock returned %08x\n",
	  get_lock(KR_LOCK, KR_RELEASE));

  msg.snd_key0 = KR_LOCK;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = (uint8_t *) str;
  msg.snd_len = sizeof(STRING);

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;
  msg.rcv_len = 0;		/* no data returned */

  msg.snd_invKey = KR_ECHO;
  msg.snd_code = 1;

  kprintf(KR_CONSOLE, "call: forking echo...\n");
  (void) SEND(&msg);

  kprintf(KR_CONSOLE, "call: sleeping for 10 seconds...\n");
  kprintf(KR_CONSOLE, "call: sleep returned %08x\n",
	  sl_sleep(KR_SLEEP, 10000));

  kprintf(KR_CONSOLE, "call: releasing lock...\n");
  release_lock(KR_RELEASE);
  kprintf(KR_CONSOLE, "call: released lock...\n");
}
