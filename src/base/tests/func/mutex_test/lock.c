/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * lock.c - mutex operations
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <domain/ConstructorKey.h>

#define KR_ZERO		0

int
build_lock(int kr_mtx_con,
	   int kr_bank,
	   int kr_sched,
	   int kr_new_lock)
  {
  int	error;

  error=constructor_request(kr_mtx_con,
			    kr_bank,
			    kr_sched,
			    KR_ZERO,
			    kr_new_lock);
  return error;
  }

int
get_lock(int kr_lock, int kr_release)
  {
  Message	msg;

  msg.snd_key0=KR_ZERO;
  msg.snd_key1=KR_ZERO;
  msg.snd_key2=KR_ZERO;
  msg.snd_key3=KR_ZERO;
  msg.snd_len=0;

  msg.rcv_key0=KR_ZERO;
  msg.rcv_key1=KR_ZERO;
  msg.rcv_key2=KR_ZERO;
  msg.rcv_key3=kr_release;
  msg.rcv_len=0;

  msg.snd_invKey=kr_lock;
  msg.snd_code=0;
  return CALL(&msg);
  }
