/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NumberKey.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>
#include <domain/SpaceBankKey.h>
#include <domain/domdbg.h>
#include <domain/SuperNodeKey.h>

#include <eros/SleepKey.h>

const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x21000;


/*
 * a simple domain to test drive the supernode
 */


/*
 * these keyregs aren't standard, but who cares,
 * this test will be gone soon anyway
 */

#define KR_ZERO       	0
#define KR_TMP1       	2
#define KR_SUPERNODE  	5
#define KR_CONSOLEKEY 	8
#define KR_SLEEPKEY   	9
#define KR_KEYBITS   	10
#define KR_NODE		11
#define KR_SEND		12
#define KR_RCV		13

#define KR_RK2       14
#define KR_RETURN    15

#define SLEEP_TME  (1000 * 2) /* 2 secs */



void
main()
{

  uint32_t ndx;
  uint32_t maxNdx = 17;
  nk_value nkv;

  nkv.value[0] = 0;
  nkv.value[1] = 1;
  nkv.value[2] = 2;
  
  for (ndx = 0; ndx < maxNdx; ndx++)
    {
      kdprintf (KR_CONSOLEKEY, "SUPERTEST:SWAP ndx 0x%x\n", ndx);

      nkv.value[0] = ndx + 1;
      nkv.value[1] = 0;
      nkv.value[2] = 0;
      
      /* build a number key to insert */
      node_write_number(KR_NODE, 0, &nkv);
      node_copy(KR_NODE, 0, KR_SEND);
      
      supernode_swap(KR_SUPERNODE, ndx, KR_SEND, KR_RCV);
      
      number_get_value(KR_RCV, &nkv);

      kdprintf (KR_CONSOLEKEY, "Old value at ndx %d was 0x%08x %08x %08x\n",
		ndx, nkv.value[2], nkv.value[1], nkv.value[0]);
    }

  
  /* doing this downward fails, because ndx is unsigned */
  for ( ndx = 0; ndx < maxNdx; ndx++ )
    {
      kprintf (KR_CONSOLEKEY, "SUPERTEST:COPY ndx%x\n",
	       ndx);

      supernode_copy(KR_SUPERNODE, ndx, KR_RCV);
      
      number_get_value(KR_RCV, &nkv);

      kdprintf (KR_CONSOLEKEY, "Value at ndx %d was 0x%08x %08x %08x\n",
		ndx, nkv.value[2], nkv.value[1], nkv.value[0]);
    }
}
