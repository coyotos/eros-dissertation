/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* WARNING -- this test is a bit of a kludge, in so far as it relies
   on sleeping to create the stall conditions for the test.  The
   sleeps are long enough that one should not run into coordination
   difficulties, but caveat emptor! */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/SleepKey.h>
#include <domain/ConstructorKey.h>
#include <domain/EventQueueKey.h>
#include <domain/domdbg.h>
#include <memory.h>

#define DEBUG if (0)

#define KR_CONSTIT 1
#define KR_SELF    2
#define KR_BANK    4
#define KR_SCHED   5

#define KR_OSTREAM 6
#define KR_EVTQ    8		/* the newly created evtq */
#define KR_SLEEP   9
#define KR_RESUME  31

#define KC_OSTREAM   0
#define KC_SLEEP     1

const uint32_t __rt_stack_pages = 1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif

#define NCLIENT 16

void
main()
{
  uint32_t result;
  event_s evt;
  
  Message msg;

  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  node_copy(KR_CONSTIT, KC_SLEEP, KR_SLEEP);
  
  bzero (&msg, sizeof(msg));
  msg.rcv_key3 = KR_RESUME;
  msg.rcv_key0 = KR_EVTQ;
  
  RETURN(&msg);
  
  kprintf(KR_OSTREAM, "Got evtq key.  Now wait for message\n");

  result = evtq_wait(KR_EVTQ, &evt);
  kprintf(KR_OSTREAM, "Got msg %d, rslt=0x%08x\n", evt.w[0], result);
  
  sl_sleep(KR_SLEEP, 2000);

  kprintf(KR_OSTREAM, "Polling...\n");
  result = evtq_poll(KR_EVTQ, &evt);
  kprintf(KR_OSTREAM, "Got msg %d, rslt=0x%08x\n", evt.w[0], result);

  kprintf(KR_OSTREAM, "Polling...\n");
  result = evtq_poll(KR_EVTQ, &evt);
  kprintf(KR_OSTREAM, "Got msg %d, rslt=0x%08x\n", evt.w[0], result);

  kprintf(KR_OSTREAM, "Waiting...\n");
  result = evtq_wait(KR_EVTQ, &evt);
  kprintf(KR_OSTREAM, "Got msg %d, rslt=0x%08x\n", evt.w[0], result);

  kprintf(KR_OSTREAM, "Sleeping to allow saturation\n");
  sl_sleep(KR_SLEEP, 5000);

  {
    uint32_t last;
    do {
      last = evt.w[0];
      result = evtq_poll(KR_EVTQ, &evt);
    } while (result == RC_OK);
    kprintf(KR_OSTREAM, "Last msg %d, rslt=0x%08x\n", last, result);
  }

  result = evtq_wait(KR_EVTQ, &evt);
  kprintf(KR_OSTREAM, "Final msg %d, rslt=0x%08x\n", evt.w[0], result);

  kprintf(KR_OSTREAM, "READER DONE\n");
}
