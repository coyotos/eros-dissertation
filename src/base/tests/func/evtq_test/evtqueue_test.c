/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* WARNING -- this test is a bit of a kludge, in so far as it relies
   on sleeping to create the stall conditions for the test.  The
   sleeps are long enough that one should not run into coordination
   difficulties, but caveat emptor! */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/SleepKey.h>
#include <domain/ConstructorKey.h>
#include <domain/EventQueueKey.h>
#include <domain/domdbg.h>

#define DEBUG if (0)

#define KR_CONSTIT 1
#define KR_SELF    2
#define KR_BANK    4
#define KR_SCHED   5

#define KR_OSTREAM 6
#define KR_EVTQF   7
#define KR_EVTQ    8		/* the newly created evtq */
#define KR_SLEEP   9
#define KR_READER  10
#define KR_SCRATCH 11

#define KC_OSTREAM   0
#define KC_SLEEP     1
#define KC_EVTQF     2
#define KC_READER    3

const uint32_t __rt_stack_pages = 1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#elif EROS_NODE_SIZE == 32
const uint32_t __rt_stack_pointer = 0x20000;
#else
#error "Unhandled node size"
#endif

#define NCLIENT 16

void
main()
{
  uint32_t result;
  event_s evt;
  
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  node_copy(KR_CONSTIT, KC_EVTQF, KR_EVTQF);
  node_copy(KR_CONSTIT, KC_SLEEP, KR_SLEEP);
  node_copy(KR_CONSTIT, KC_READER, KR_READER);
  
  DEBUG kdprintf(KR_OSTREAM, "About to create evtq\n");

  /* Fabricate the SIGMUX object: */
  result = constructor_request(KR_EVTQF, KR_BANK, KR_SCHED, KR_ZERO,
			       KR_EVTQ);

  if (result != RC_OK)
    kdprintf(KR_OSTREAM, "Result from evtq creation: 0x%x\n", result);

  /* Pass that key to the reader, so that they have it too: */
  {
    Message msg;
    msg.snd_key0 = KR_EVTQ;
    msg.snd_invKey = KR_READER;
    msg.snd_len = 0;
    SEND(&msg);
  }
  
  /* Sleep two seconds to allow the reader to block for a message */
  sl_sleep(KR_SLEEP, 2000);

  /* Post two messages in rapid sequence.  The first will wake up the
     reader.  The second will be read by the reader to test the
     polling logic. */
  
  evt.w[0] = 1;
  result = evtq_post(KR_EVTQ, &evt);
  kprintf(KR_OSTREAM, "Posted msg %d, rslt=0x%08x\n", evt.w[0], result);
  
  evt.w[0] = 2;
  result = evtq_post(KR_EVTQ, &evt);
  kprintf(KR_OSTREAM, "Posted msg %d, rslt=0x%08x\n", evt.w[0], result);
  
  /* Wait for the reader to go back to sleep after the polling test. */
  sl_sleep(KR_SLEEP, 2000);
  
  /* Send a third message to awaken the reader. */
  evt.w[0] = 3;
  result = evtq_post(KR_EVTQ, &evt);
  kprintf(KR_OSTREAM, "Posted msg %d, rslt=0x%08x\n", evt.w[0], result);

  /* Reader is now sleeping for a while.  Send enough messages to
     saturate the event queue and verify that none get lost: */
  {
    int i;
    for (i = 0; i < 128; i++) {
      evt.w[0] = i + 128;
      result = evtq_post(KR_EVTQ, &evt);
      if (result != RC_OK) {
	kprintf(KR_OSTREAM, "Saturated at value %d events, rslt 0x%08x\n",
		128 + i - 1, result);
	break;
      }
    }
  }

  kprintf(KR_OSTREAM, "SENDER WAITS FOR DRAIN\n");

  sl_sleep(KR_SLEEP, 10000);
  evt.w[0] = -1;
  evtq_post(KR_EVTQ, &evt);
  
  kprintf(KR_OSTREAM, "SENDER DONE\n");
}
