/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <domain/ConstructorKey.h>
#include <domain/domdbg.h>

#define KR_CONSTIT 1
#define KR_SELF    2
#define KR_BANK    4
#define KR_SCHED   5
#define KR_OSTREAM 6
#define KR_NEWCON  7
#define KR_METACON 8
#define KR_NEW_HELLO 9
#define KR_HELLO_SEG 10
#define KR_HELLO_PC  11
#define KR_CONREQ    12

#define KC_METACON   0
#define KC_OSTREAM   1
#define KC_HELLO_SEG 2
#define KC_HELLO_PC  3

const uint32_t __rt_stack_pages = 1;
#if EROS_NODE_SIZE == 16
const uint32_t __rt_stack_pointer = 0x10000;
#else
const uint32_t __rt_stack_pointer = 0x20000;
#endif

void
main()
{
  uint32_t result;
  
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  node_copy(KR_CONSTIT, KC_METACON, KR_METACON);
  node_copy(KR_CONSTIT, KC_HELLO_SEG, KR_HELLO_SEG);
  node_copy(KR_CONSTIT, KC_HELLO_PC, KR_HELLO_PC);
  
  if ( constructor_is_discreet( KR_METACON ) )
    kdprintf(KR_OSTREAM, "Metacon alleges discretion\n");
  
  result = constructor_request(KR_METACON, KR_BANK, KR_SCHED, KR_ZERO,
			 KR_NEWCON);

  kdprintf(KR_OSTREAM, "Populate new constructor\n");

  /* KR_OSTREAM is constituent 0 */
  constructor_insert(KR_NEWCON, 0, KR_OSTREAM);

  constructor_insert(KR_NEWCON, Constructor_Product_Spc, KR_HELLO_SEG);
  constructor_insert(KR_NEWCON, Constructor_Product_PC, KR_HELLO_PC);

  constructor_seal(KR_NEWCON, KR_CONREQ);

  kdprintf(KR_OSTREAM, "Request product...\n");

  constructor_request(KR_CONREQ, KR_BANK, KR_SCHED, KR_ZERO,
		      KR_NEW_HELLO);

  kdprintf(KR_OSTREAM, "Got product!\n", result);

  {
    Message msg;
    msg.snd_key0 = KR_ZERO;
    msg.snd_key1 = KR_ZERO;
    msg.snd_key2 = KR_ZERO;
    msg.snd_key3 = KR_ZERO;
    msg.snd_len = 0;
    msg.snd_code = 0;
    msg.snd_invKey = KR_NEW_HELLO;
    
    msg.rcv_key0 = KR_ZERO;
    msg.rcv_key1 = KR_ZERO;
    msg.rcv_key2 = KR_ZERO;
    msg.rcv_key3 = KR_ZERO;
    msg.snd_len = 0;

    CALL(&msg);
  }
  
  key_destroy(KR_NEWCON);
}
