/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NumberKey.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>
#include <domain/SpaceBankKey.h>
#include <domain/domdbg.h>
#include <domain/SuperNodeKey.h>
#include <domain/ConstructorKey.h>

#include <eros/SleepKey.h>

const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x21000;


/*
 * a simple domain to test drive the directory
 */

#define KR_ZERO       	0
#define KR_SELF       	2
#define KR_BANK       	4
#define KR_SCHED       	5
#define KR_DIRECTORY  	6
#define KR_OSTREAM 	8
#define KR_SLEEPKEY   	9
#define KR_KEYBITS   	10
#define KR_NODE		11
#define KR_SEND		12
#define KR_RCV		13

#define KR_RK2          14
#define KR_RETURN       15

#define SLEEP_TME       (1000 * 2) /* 2 secs */

/* These should be included from a .h file */
#define OC_Directory_Link      1
#define OC_Directory_Unlink    2
#define OC_Directory_Lookup    3

void
link_test(char *str, uint32_t ndx)
{
  Message msg;
  uint32_t Result;
  nk_value nkv;

  kdprintf (KR_OSTREAM, "DIRTEST:LINK ndx 0x%x, \"%s\"\n", ndx, str);

  nkv.value[0] = ndx + 1;
  nkv.value[1] = 0;
  nkv.value[2] = 0;

  /* Build a number key to insert */
  node_write_number(KR_NODE, 0, &nkv);
  node_copy(KR_NODE, 0, KR_SEND);

  /* Setup a message */
  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;
  msg.rcv_len = 0;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  msg.snd_invKey = KR_DIRECTORY;
  msg.snd_key0 = KR_SEND;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = str;
  msg.snd_len = strlen(str);
  msg.snd_code = OC_Directory_Link;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  Result = CALL(&msg);

  if (Result != RC_OK) {
    kdprintf(KR_OSTREAM, "Problem with linking \"%s\"", str);
    return;
  }
}

void
unlink_test(char *str)
{
  Message msg;
  uint32_t Result;
  nk_value nkv;

  kdprintf (KR_OSTREAM, "DIRTEST:UNLINK str \"%s\"\n", str);

  /* Setup a message */
  msg.rcv_key0 = KR_RCV;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;
  msg.rcv_len = 0;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  msg.snd_invKey = KR_DIRECTORY;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = str;
  msg.snd_len = strlen(str);
  msg.snd_code = OC_Directory_Unlink;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  Result = CALL(&msg);

  if (Result != RC_OK) {
    kdprintf(KR_OSTREAM, "Problem with unlinking \"%s\"", str);
    return;
  }
  else {
    number_get_value(KR_RCV, &nkv);
    kdprintf(KR_OSTREAM, "Unlinked \"%s\" and received back 0x%08x %08x %08x\n",
	     str, nkv.value[2], nkv.value[1], nkv.value[0]);
  }
}

void
lookup_test(char *str)
{
  Message msg;
  uint32_t Result;
  nk_value nkv;

  kdprintf (KR_OSTREAM, "DIRTEST:LOOKUP of \"%s\"\n", str);

  /* Setup a message */
  msg.rcv_key0 = KR_RCV;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;
  msg.rcv_len = 0;
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  msg.snd_invKey = KR_DIRECTORY;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = str;
  msg.snd_len = strlen(str);
  msg.snd_code = OC_Directory_Lookup;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  Result = CALL(&msg);

  if (Result != RC_OK) {
    kdprintf(KR_OSTREAM, "lookup_test(): \"%s\" not found\n", str);
    return;
  }
  else {
    number_get_value(KR_RCV, &nkv);

    kdprintf (KR_OSTREAM, "For \"%s\" found 0x%08x %08x %08x\n",
	      str, nkv.value[2], nkv.value[1], nkv.value[0]);
  }
}

void
main()
{
  if (constructor_request(KR_DIRECTORY, KR_BANK, KR_SCHED, KR_ZERO,
			  KR_DIRECTORY) != RC_OK) {
    kdprintf(KR_OSTREAM, "Directory creation fails\n");
    return;
  }

  link_test("this_is_a_very_long_name", 0);
  link_test("x", 1);
  link_test("y", 2);
  link_test("z", 3);

  lookup_test("y");

  unlink_test("y");
  lookup_test("y");

  /* following tests empty name at start of page: */
  unlink_test("this_is_a_very_long_name");
  link_test("a", 5);
  /* following tests page coalesce. */
  link_test("b", 6);
}
