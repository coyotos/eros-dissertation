/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/ProcessKey.h>
#include <eros/TimePage.h>
#include <eros/Key.h>
#include <eros/SleepKey.h>
#include <domain/domdbg.h>

#define KR_CONSTIT    1
#define KR_SELF       2
#define KR_BANK       4
#define KR_SCHED      5
#define KR_OSTREAM    6
#define KR_SLEEP      7
#define KR_TIMEPG     8
#define KR_SEG        9

#define KC_SLEEP      0
#define KC_OSTREAM    1
#define KC_TIMEPG     2

const uint32_t __rt_stack_pages = 1;
const uint32_t __rt_stack_pointer = 0x20000;

#define TIME_PAGE_SLOT 30

volatile TimePageStruct *tps = (TimePageStruct *)
     (TIME_PAGE_SLOT * EROS_PAGE_SIZE);

timeval
GetTimeNow()
{
  timeval tv;

  do {
    tv = tps->tps_wall;
  } while ((tv.tv_secs != tps->tps_wall.tv_secs) ||
	   (tv.tv_usecs != tps->tps_wall.tv_usecs));

  return tv;
}

void
main()
{
  timeval tv;
  int i;

  node_copy(KR_CONSTIT, KC_SLEEP, KR_SLEEP);
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  node_copy(KR_CONSTIT, KC_TIMEPG, KR_TIMEPG);

  process_copy(KR_SELF, ProcAddrSpace, KR_SEG);

  node_swap(KR_SEG, TIME_PAGE_SLOT, KR_TIMEPG, KR_ZERO);
  
  kprintf(KR_OSTREAM, "Time Page test starts\n");
  
  sl_sleep(KR_SLEEP, 5000);

  kprintf(KR_OSTREAM, "Sleeping by 10 ms intervals\n");
  for (i = 0; i < 5; i++) {
    sl_sleep(KR_SLEEP, 10);

    tv = GetTimeNow();
    kprintf(KR_OSTREAM, "tv.secs = %d; tv.usecs = %d\n", tv.tv_secs,
	    tv.tv_usecs);
  }

  kprintf(KR_OSTREAM, "Sleeping by 5000 ms intervals\n");
  for (i = 0; i < 5; i++) {
    sl_sleep(KR_SLEEP, 5000);

    tv = GetTimeNow();
    kprintf(KR_OSTREAM, "time.secs = %d; time.usecs = %d\n", tv.tv_secs,
	    tv.tv_usecs);
  }

  kprintf(KR_OSTREAM, "Time Page test completed\n");
}
