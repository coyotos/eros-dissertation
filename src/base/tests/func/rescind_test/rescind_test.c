/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NodeKey.h>
#include <eros/PageKey.h>
#include <eros/RangeKey.h>
#include <eros/SleepKey.h>
#include <eros/StdKeyType.h>
#include <domain/domdbg.h>

// #define VERBOSE

#define KR_ZERO       0
#define KR_TMP0       1
#define KR_TMP1       2

#define KR_RANGE      6

#define KR_CONSTIT    7
#define  KC_RANGEKEY   3

#define KR_OSTREAM    8
#define KR_SLEEPKEY   9
#define KR_KEYBITS   10

#define SLEEP_TME  0 /* (100/7) */

static int __assert(const char *, const char *, int);

#define assert(expression)  \
  ((void) ((expression) ? 0 : __assert (#expression, __FILE__, __LINE__)))

/**Handle the stack stuff**/
const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x10000;

void runLoop(uint64_t baseOffset)
{
  /* A loop consists of allocating a page and a node, then rescinding them.
   */
  uint32_t result;
  uint64_t offset = baseOffset;

  kprintf(KR_OSTREAM,
	  "RescindTest: Grabbing page (0x%08x%08x)\n",
	  (uint32_t) (offset >> 32),
	  (uint32_t) offset);

  result = range_getobjectkey(KR_RANGE,
			      OT_Page,
			      offset,
			      KR_TMP0);

  assert("creating page" && result == RC_OK );

  /* increment for node */
  offset += EROS_OBJECTS_PER_FRAME;

  kprintf(KR_OSTREAM,
	  "RescindTest: Grabbing Node (0x%08x%08x)\n",
	  (uint32_t) (offset >> 32),
	  (uint32_t) offset);

  result = range_getobjectkey(KR_RANGE,
			      OT_Node,
			      offset,
			      KR_TMP1);

  assert("creating page" && result == RC_OK );

  kprintf(KR_OSTREAM,
	   "RescindTest: finished creating objects.\n");

#ifdef VERBOSE
  kdprintf(KR_OSTREAM,
	   "Pausing before rescind page\n");
#endif

  /* reset offset */
  offset = baseOffset;

  kprintf(KR_OSTREAM,
	  "RescindTest: Rescinding page (0x%08x%08x)\n",
	  (uint32_t) (offset >> 32),
	  (uint32_t) offset);

  result = range_rescind(KR_RANGE, KR_TMP0);

  assert("rescinding page" && result == RC_OK );

  result = key_kt(KR_TMP0);

  assert("checking page AKT after rescind" && result == AKT_Number );

#ifdef VERBOSE
  kdprintf(KR_OSTREAM,
	   "Pausing after rescind page, before rescind node\n");
#endif

  /* reset offset */
  offset = baseOffset + EROS_OBJECTS_PER_FRAME;

  kprintf(KR_OSTREAM,
	  "RescindTest: Rescinding node (0x%08x%08x)\n",
	  (uint32_t) (offset >> 32),
	  (uint32_t) offset);

  result = range_rescind(KR_RANGE, KR_TMP1);

  assert("rescinding page" && result == RC_OK );

  result = key_kt(KR_TMP1);

  assert("checking node AKT after rescind" && result == AKT_Number );

#ifdef VERBOSE
  kdprintf(KR_OSTREAM,
	   "Pausing after rescind node\n");
#endif
}

#define NUM_LOOPS (1000u)

void main(void)
{
  uint32_t result;
  uint32_t idx;
  
  uint64_t baseOffset; 

  kprintf(KR_OSTREAM,
	  "RescindTest: Initialiazing\n");

  result = node_copy(KR_CONSTIT, KC_RANGEKEY, KR_RANGE);

  assert("copying range key from volsize" && result == RC_OK );
  
  assert("validating range key" && key_kt(KR_RANGE) == AKT_Range );

  result = range_query(KR_RANGE, &baseOffset);

  assert("querying range length" && result == RC_OK );

  /* subtract two frame to get second-to-last frame in range */
  baseOffset -= 2*EROS_OBJECTS_PER_FRAME;
  
  kprintf(KR_OSTREAM,
	  "RescindTest: Done initializing.\n");
  
  kprintf(KR_OSTREAM,
	  "NOTE: Page key will be in slot %u\n"
	  "      Node key will be in slot %u\n",
	  KR_TMP0,
	  KR_TMP1);
  
  kdprintf(KR_OSTREAM,
	   "RescindTest: Beginning test with baseOffset 0x%08x%08x\n",
	   (uint32_t) (baseOffset >> 32),
	   (uint32_t) baseOffset);
  
  
  for (idx = 0; idx < NUM_LOOPS; idx++) {
    kprintf(KR_OSTREAM, "RescindTest: Starting %u\n", idx);
    
    runLoop(baseOffset);
  }
  kdprintf(KR_OSTREAM, "RescindTest PASSES\n");
}

static int __assert(const char *expr, const char *file, int line)
{
  kdprintf(KR_OSTREAM, "%s:%d: Assertion failed: '%s'\n",
	   file, line, expr);
  return 0;
}

