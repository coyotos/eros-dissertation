/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <domain/domdbg.h>
#include <domain/ConstructorKey.h>
#include <eros/SleepKey.h>
#include <eros/ReturnerKey.h>
#include <KEYDEFS.h>

#include <sys/fcntl.h>
#include <unistd.h>

/*
 * a simple domain to test the line discipline modules
 *
 */

#define KR_ZERO 0
#define KR_XTAR 12
#define KR_SLEEPKEY 13

#define SLEEP_TIME (1000*2)	/* 2 secs */

const uint32_t __rt_stack_pages = 0x0;
const uint32_t __rt_stack_pointer = 0x21000;

void
init()
{
  /* Need to fabricate a root directory: */
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  node_copy(KR_CONSTIT, KC_SLEEP, KR_SCRATCH);
  node_copy(KR_CONSTIT, KC_RETURNER, KR_RETURNER);

  sl_sleep(KR_SCRATCH, 1000);

  /* Need to fabricate a root directory: */
  node_copy(KR_CONSTIT, KC_DIRC, KR_SCRATCH);

  kdprintf(KR_OSTREAM, "Fabricate root directory:\n");

  if (constructor_request(KR_SCRATCH, KR_BANK, KR_SCHED, KR_ZERO,
			  KR_CWD) != RC_OK) {
    kdprintf(KR_OSTREAM, "root dir build failed\n");
  }
  copy_key_reg(KR_RETURNER, KR_CWD, KR_FSROOT);

  kdprintf(KR_OSTREAM, "Fabricate file table:\n");

  /* Need to fabricate open file table: */
  node_copy(KR_CONSTIT, KC_SNODEC, KR_SCRATCH);
  if (constructor_request(KR_SCRATCH, KR_BANK, KR_SCHED, KR_ZERO,
			  KR_FD_TAB) != RC_OK) {
    kdprintf(KR_OSTREAM, "fd tab build failed\n");
  }
}

void
main()
{
  /*
   * Simple little single file xtar test case;
   */
  Message msg;
  uint32_t result;
  uint32_t fd;
  char buf[255];
  
#ifdef DEBUG
     /* just sleep for a bit so we can get our bearings */
     sl_sleep(KR_SLEEPKEY, SLEEP_TIME); 
#endif

  init();			/* Create root directory */

  msg.snd_key0 = KR_RETURNER;
  msg.snd_key1 = KR_CWD;
  msg.snd_key2 = KR_FSROOT;
  msg.snd_key3 = KR_ZERO;

  msg.snd_data = NULL;
  msg.snd_len = 0;

  msg.snd_invKey = KR_XTAR;
  msg.snd_code = 1;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;

  msg.rcv_data = 0;
  msg.rcv_len = 0;

  kdprintf(KR_OSTREAM, "tartest:Keys: 0x%08x 0x%08x 0x%08x 0x%08x\n",
           key_kt (KR_RETURNER), key_kt(KR_CWD), key_kt(KR_FSROOT),
	   key_kt(KR_FD_TAB));

  kdprintf(KR_OSTREAM, "About to call xtar\n");
  result = CALL(&msg);
  kdprintf(KR_OSTREAM, "Done from call xtar with result %d\n", result);

  kdprintf(KR_OSTREAM, "ABOUT TO CALL OPEN\n");

  fd = open("tartest.c", O_RDWR, 0666);
  if (fd == -1)
    kdprintf(KR_OSTREAM, "Open failed\n");

  kdprintf(KR_OSTREAM, "ABOUT TO CALL READ\n");

  if ( read(fd, buf, 30) == -1 )
    kdprintf(KR_OSTREAM, "Read failed\n");
  else
    kdprintf(KR_OSTREAM, "Read 0x%02x 0x%02x 0x%02x\n", buf[0],
	     buf[1], buf[2]);
  return;
}
