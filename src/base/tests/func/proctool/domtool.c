/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/ProcessToolKey.h>
#include <eros/SleepKey.h>
#include <domain/domdbg.h>

// The purpose of this domain is to issue escape sequences to the
// console to test ANSI terminal emulation.  It is meant to be watched
// interactively.
#define KR_ZERO 0
#define KR_NUM1 1
#define KR_CONSOLEKEY 8
#define KR_SLEEPKEY 9
#define KR_KEYBITS 10
#define KR_N0 11
#define KR_D0 12
#define KR_DOMTOOL 13

void
main()
{
  kprintf(KR_CONSOLEKEY, "WAITING TO STABILIZE...\n");
  sl_sleep(KR_SLEEPKEY, 5000);
  
  kprintf(KR_CONSOLEKEY, "\033[H\033[J");	/* clear screen */
  kprintf(KR_CONSOLEKEY, "Here we go...\n\n");

  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_N0);

  pt_make_process_key(KR_DOMTOOL, KR_N0, KR_D0);
  
  ShowKey(KR_CONSOLEKEY, KR_KEYBITS, KR_D0);
}
