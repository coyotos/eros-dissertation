/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>
#include <domain/SpaceBankKey.h>
#include <domain/domdbg.h>

#define KR_ZERO 0
#define KR_CONSOLEKEY 8
#define KR_SLEEPKEY 9
#define KR_SEG1 7
#define KR_TMP 10
#define KR_SEG2 6		/* my own segment? */
#include "domain.h"

uint32_t
Request(Message *inmsg)

{

  /*
   * Called by domain1.  Take no arguments.
   * Replace the key in our segment with the key in domain1's segment
   * that "holds" the data at address 0xa0000.  
   *
   * Now, all the pages from 2,560 to 2,816 are shared
   * between domain1 and domain2.  In our case, only one page
   * occupies this range: that page is at 0xa0000.  
   *
   * 0xa0000 now has the same struct that domain1 has.
   *
   * We will then change a member of that struct, and return to 
   * the caller.
   */

  struct shared *sh_ptr; 

  kprintf(KR_CONSOLEKEY, "Domain2 test case: running \n");

  /* we know it's the 10th slot because a == 10 */

  node_copy(KR_SEG1, 10, KR_TMP); /* copy domain1's 10th slot, put into tmp */
  node_swap(KR_SEG2, 10, KR_TMP, KR_ZERO); /* copy tmp into our 10th slot */
  
  sh_ptr = (struct shared *) 0xa0000; /* now we can assign the struct ptr */
  
  kprintf(KR_CONSOLEKEY, "Domain2 test case: seq is 0x%x.\n",
	  sh_ptr->sequence);

  sh_ptr->sequence = 3;		/* change the struct */

  kprintf(KR_CONSOLEKEY, "Domain2 test case: finished.\n");

  return 1;			/* return to caller */

}
