/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>
#include <domain/SpaceBankKey.h>
#include <domain/domdbg.h>

#define KR_ZERO 0
#define KR_CONSOLEKEY 8
#define KR_SLEEPKEY 9
#define KR_START2 7

#include "domain.h"

void
callDomain2 ()
{

  Message msg;


  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;

  msg.snd_len = 0;

  msg.rcv_len = 0;		/* no data returned */

  (void) CALL(KR_START2, 0, &msg);

}

void
Startup()
{

  /*
   * We have a added a page to our segment at 0xa0000.  Since
   * this is the highest address that got assigned to our segment
   * in the imgmap file, makeimg gave our segment a BLSS of 5, which
   * means that our address range is from 0x0->0xfffff, and each
   * of the 16 slots in our segment can access 256 pages.
   *
   * We're going to put the shared struct at 0xa0000 and then 
   * call domain2, which will change it.
   */

  struct shared *sh_ptr;

  sh_ptr = (struct shared *) 0xa0000; /* assign shared struct addr */

  kprintf(KR_CONSOLEKEY, "Domain1 test case: running\n");
  
  sh_ptr->sequence = 2;		/* initialize the struct */

  kprintf(KR_CONSOLEKEY, "Domain1 test case: seq is 0x%x.\n",
	  sh_ptr->sequence);
  
  callDomain2();		/* call domain2, which changes struct */
  
  kprintf(KR_CONSOLEKEY, "Domain1 test case: seq is now 0x%x.\n",
	  sh_ptr->sequence);
  
  kprintf(KR_CONSOLEKEY, "Domain1 test case: finished.\n");
  
  return;

}
