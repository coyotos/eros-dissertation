/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>

#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/StdKeyType.h>
#include <eros/SleepKey.h>

#include <domain/SpaceBankKey.h>
#include <domain/ConstructorKey.h>
#include <domain/LineDiscKey.h>
#include <domain/domdbg.h>

/*
 * a simple domain to test the line discipline modules
 *
 */

#define KR_ZERO        0
#define KR_TMP0        1
#define KR_TMP1        2

#define KR_SCHED       4
#define KR_BANK        5
#define KR_LINEDISC    6
#define KR_TTY         7
#define KR_OSTREAM     8
#define KR_SLEEPKEY    9

#define KR_RK0        12
#define KR_RK1        13
#define KR_RK2        14
#define KR_RETURN     15

const uint32_t __rt_stack_pages = 0;
const uint32_t __rt_stack_pointer = 0x10000;

#define LDT_NUMCHARS    1  /* number of characters between returns */
#define LDT_TIMEOUT     0  /* timeout (millisecs) before returns   */

uint16_t specialChars[] = {
  '\x4'
};
#define NUM_SPECHARS (sizeof(specialChars)/sizeof(specialChars[0]))

int
strncmp(const char *s1, const char *s2, unsigned len)
{
  while(len && *s1 == *s2 && *s1 && *s2) {
    s1++;
    s2++;
    len--;
  }
  if (!len || *s1 == *s2) return 0;

  if (*s1 < *s2) return -1;
  else return 1;
}

#define buff ((uint8_t *)0xd000)  /* EROS_PAGE_SIZE long */

void
main ()
{

  uint32_t result;

  buff[0] = 0;
  
#ifdef DEBUG
  kprintf (KR_CONSOLEKEY, "linetest: starting...\n");
#endif

  result = constructor_is_discreet(KR_LINEDISC);
  if (result == RC_OK) {
    kprintf(KR_OSTREAM,
            "LineDisc Constructor alleges discretion.\n");
  } else {
    kprintf(KR_OSTREAM,
            "LineDisc Constructor is not discreet. (%08x)\n",
            result);
  }

  kprintf(KR_OSTREAM, "Creating line discipline\n");

  result = constructor_request(KR_LINEDISC,
                               KR_BANK,
                               KR_SCHED,
                               KR_TTY,
                               KR_LINEDISC);

  if (result != RC_OK) {
    kprintf(KR_OSTREAM,"Failed to create line discipline.\n");
  }
  kprintf(KR_OSTREAM,"Created line discipline\n");

#if 0
  kprintf(KR_OSTREAM,"Setting timeout\n");

  if (charsrc_set_timeout(KR_LINEDISC, LDT_NUMCHARS, LDT_TIMEOUT) != RC_OK) {
    kdprintf(KR_OSTREAM,
	     "linetest: error setting timeout\r\n"
	     );
  }    
#endif
      
  kprintf(KR_OSTREAM,"Setting cooked mode\n");

  if (linedisc_make_cooked(KR_LINEDISC,
	 	           specialChars,
		  	   NUM_SPECHARS) != RC_OK) {
    kdprintf(KR_OSTREAM,
	     "linetest: error setting cooked mode\r\n"
	     );
  }

  kprintf(KR_OSTREAM,
           "linetest: Done setting cooked mode\n");

  {
    const char *str = "Type \"bye\" on a line by itself to destroy linedisc.\r\n";
    uint32_t len = strlen(str);
    uint32_t written;

    kdprintf(KR_OSTREAM,
             "Testing LineDisc.  First, trying to write %d chars\n",
             len);

    while(len) {
      result = charsrc_write(KR_LINEDISC,
                             len, str, &written);
      if (result != RC_OK)
        kdprintf(KR_OSTREAM,
                 "Write did not return RC_OK\n");

      if (written == 0) {
        kdprintf(KR_OSTREAM,
                 "Write did not write anything\n");
      }
      kprintf(KR_OSTREAM,"Write wrote %d\n",written);
      len -= written;
      str += written;
    }
  }
  
  do {
    uint32_t events_fired;
    uint32_t len;

    uint32_t retval = charsrc_wait_for_event(KR_LINEDISC,
					 EROS_PAGE_SIZE, buff,
					 &len,
					 CharSrc_ALL_Events,
					 &events_fired);
    uint32_t idx;

 
    if (retval != RC_OK) {
      kdprintf(KR_OSTREAM, "Error waiting for event %08x\n",retval);
    }
    kprintf(KR_OSTREAM,
	    "Events fired: %02x buff: \"",
            events_fired);
    for (idx = 0 ; idx < len ; idx++ ) {
      uint8_t curChar = buff[idx];
      if (curChar < 32)
        kprintf(KR_OSTREAM,"^%c",curChar + 'A' - 1);
      else if (curChar == 127)
        kprintf(KR_OSTREAM,"^?");
      else if (curChar > 127)
        kprintf(KR_OSTREAM,"\\x%02x",curChar);
      else
        kprintf(KR_OSTREAM,"%c",curChar);
    }
    kprintf(KR_OSTREAM,"\"\n");
 
  } while (strncmp(buff,"bye",3) != 0);
  
  kdprintf(KR_OSTREAM,
	   "Destroying linedisc.\n");
  key_destroy(KR_LINEDISC);

  kdprintf(KR_OSTREAM,
           "LineDisc destroyed.\n");
  return;

}



