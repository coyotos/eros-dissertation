/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/TimeOfDay.h>

#define KR_ZERO 0
#define KR_TOCK 8
#define KR_LOGKEY 9
#define KR_SLEEP 10
#define KR_TOD 11

TimeOfDay tod;

uint32_t __rt_stack_pages = 0;
uint32_t __rt_stack_pointer = 0x21000;

void
main()
{
  Message sleepMsg, logMsg;
  uint64_t nxtTick;

  sleepMsg.snd_key0 = KR_ZERO;
  sleepMsg.snd_key1 = KR_ZERO;
  sleepMsg.snd_key2 = KR_ZERO;
  sleepMsg.snd_key3 = KR_ZERO;
  sleepMsg.snd_len = 0;

  sleepMsg.rcv_key0 = 12;
  sleepMsg.rcv_key1 = 13;
  sleepMsg.rcv_key2 = 14;
  sleepMsg.rcv_key3 = 15;
  sleepMsg.rcv_len = sizeof(uint64_t);
  sleepMsg.rcv_data = &nxtTick;

  sleepMsg.snd_code = 16;
  sleepMsg.snd_invKey = KR_TOD;
  (void) CALL(&sleepMsg);

  sleepMsg.rcv_len = 0;
  sleepMsg.snd_len = sizeof(uint64_t);
  sleepMsg.snd_data = &nxtTick;
  
#if 0
  sleepMsg.rcv_len = sizeof(tod);
  sleepMsg.rcv_data = &tod;
#endif

  {
    /* Start up tock, passing it the starting tick we just
       received. */
    Message tockStart;
    tockStart.snd_key0 = KR_ZERO;
    tockStart.snd_key1 = KR_ZERO;
    tockStart.snd_key2 = KR_ZERO;
    tockStart.snd_key3 = KR_ZERO;
    tockStart.snd_len = sizeof(uint64_t);
    tockStart.snd_data = &nxtTick;
    tockStart.rcv_key0 = KR_ZERO;
    tockStart.rcv_key1 = KR_ZERO;
    tockStart.rcv_key2 = KR_ZERO;
    tockStart.rcv_key3 = KR_ZERO;
    tockStart.rcv_len = 0;

    tockStart.snd_code = 1;
    tockStart.snd_invKey = KR_TOCK;
    (void) SEND(&tockStart);
  }
  
  logMsg.snd_key0 = KR_ZERO;
  logMsg.snd_key1 = KR_ZERO;
  logMsg.snd_key2 = KR_ZERO;
  logMsg.snd_key3 = KR_ZERO;
  logMsg.snd_len = sizeof("tick") - 1;
  logMsg.snd_data = "tick";

  logMsg.rcv_key0 = 12;
  logMsg.rcv_key1 = 13;
  logMsg.rcv_key2 = 14;
  logMsg.rcv_key3 = 15;
  logMsg.rcv_len = 0;

  for (;;) {
    nxtTick += 4000llu;

    sleepMsg.snd_code = 2;
    sleepMsg.snd_invKey = KR_SLEEP;
    CALL(&sleepMsg);

    logMsg.snd_code = 1;
    logMsg.snd_invKey = KR_LOGKEY;
    CALL(&logMsg);
  }

#if 0
  (void) CALL(KR_LOGKEY, 1, &msg);
#endif
}
