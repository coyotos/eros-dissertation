/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/TimeOfDay.h>

#define KR_ZERO 0
#define KR_LOGKEY 9
#define KR_SLEEP 10
#define KR_TOD 11

#define KR_RK0   12
#define KR_RK1   13
#define KR_RK2   14
#define KR_RESUME  15		/* so don't use it for anything else! */

TimeOfDay tod;

uint64_t nxtTock;

uint32_t __rt_stack_pages = 0;
uint32_t __rt_stack_pointer = 0x21000;

int
ProcessRequest(Message *inmsg)
{
  /*
   * We received the initial current tick on entry.
   */
  Message sleepMsg, logMsg;
  
  nxtTock += 2000llu;

  sleepMsg.snd_key0 = KR_ZERO;
  sleepMsg.snd_key1 = KR_ZERO;
  sleepMsg.snd_key2 = KR_ZERO;
  sleepMsg.snd_key3 = KR_ZERO;
  sleepMsg.snd_len = sizeof(uint64_t);
  sleepMsg.snd_data = &nxtTock;

  sleepMsg.rcv_key0 = 12;
  sleepMsg.rcv_key1 = 13;
  sleepMsg.rcv_key2 = 14;
  sleepMsg.rcv_key3 = 15;
  sleepMsg.rcv_len = 0;

  logMsg.snd_key0 = KR_ZERO;
  logMsg.snd_key1 = KR_ZERO;
  logMsg.snd_key2 = KR_ZERO;
  logMsg.snd_key3 = KR_ZERO;
  logMsg.snd_len = sizeof("tock") - 1;
  logMsg.snd_data = "tock";

  logMsg.rcv_key0 = 12;
  logMsg.rcv_key1 = 13;
  logMsg.rcv_key2 = 14;
  logMsg.rcv_key3 = 15;
  logMsg.rcv_len = 0;

  for (;;) {
    nxtTock += 4000llu;

    sleepMsg.snd_code = 2;
    sleepMsg.snd_invKey = KR_SLEEP;
    CALL(&sleepMsg);

    logMsg.snd_code = 1;
    logMsg.snd_invKey = KR_LOGKEY;
    CALL(&logMsg);
  }

  return 1;
}

void
main()
{
  Message msg;
  
  msg.snd_invKey = KR_ZERO;
  msg.snd_key0 = KR_ZERO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_data = 0;
  msg.snd_len = 0;
  msg.snd_code = 0;
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  msg.rcv_key0 = KR_RK0;
  msg.rcv_key1 = KR_RK1;
  msg.rcv_key2 = KR_RK2;
  msg.rcv_key3 = KR_RESUME;
  msg.rcv_data = (void *) &nxtTock;
  msg.rcv_len = sizeof(nxtTock);
  msg.rcv_code = 0;
  msg.rcv_w1 = 0;
  msg.rcv_w2 = 0;
  msg.rcv_w3 = 0;

  do {
    RETURN(&msg);
 } while ( ProcessRequest(&msg) );
}
