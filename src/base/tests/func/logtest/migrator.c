/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/TimeOfDay.h>
#include <eros/SleepKey.h>
#include <eros/CkptKey.h>
#include <domain/domdbg.h>

#define KR_ZERO 0
#define KR_CKPT 8
#define KR_OSTREAM 9
#define KR_SLEEP 10
#define KR_TOD 11

const uint32_t __rt_stack_pointer = 0x21000;
const uint32_t __rt_stack_pages = 0;

void
main()
{
  /*
   * We received the initial current tick on entry.
   */
  sl_sleep(KR_SLEEP, 17000);

  kprintf(KR_OSTREAM, "Migrator: About to take checkpoint\n");

  ckpt_TakeCheckpoint(KR_CKPT);

  kprintf(KR_OSTREAM, "migrator: start forcing migration\n");

  while (ckpt_ProcessMigration(KR_CKPT) != RC_OK)
    sl_sleep(KR_SLEEP, 250);

  kprintf(KR_OSTREAM, "Migrator: Migration has completed\n");
}
