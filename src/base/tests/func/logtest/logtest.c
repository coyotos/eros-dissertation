/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/SleepKey.h>
#include <domain/domdbg.h>

#define KR_ZERO 0
#define	KR_CONSOLEKEY_SLOT	9
#define KR_SLEEP 10

#define NPAGE 10

const uint32_t __rt_stack_pointer = 0x21000;
const uint32_t __rt_stack_pages = 0;

uint8_t PagePool[(NPAGE + 1) * EROS_PAGE_SIZE];

void
main()
{
  int i;

  uint32_t wPagePool = (uint32_t) &PagePool;
  char *pagePool;
  
  /* This is inefficient as hell, but tough: */
  while (wPagePool % EROS_PAGE_SIZE)
    wPagePool++;
  
  pagePool = (char *) wPagePool;
    
  sl_sleep(KR_SLEEP, 5000);
  
  kprintf(KR_CONSOLEKEY_SLOT, "logtest begins\n");

  for(;;) {
    for (i = 0; i < NPAGE; i++) {
      kprintf(KR_CONSOLEKEY_SLOT, "Touch page %d\n", i);
      pagePool[i * EROS_PAGE_SIZE] = i;
      sl_sleep(KR_SLEEP, 6000);
    }
  }


  kprintf(KR_CONSOLEKEY_SLOT, "logtest completes\n");
}
