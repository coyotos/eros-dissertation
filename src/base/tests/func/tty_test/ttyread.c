/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/TtyKey.h>

#include <domain/domdbg.h>

const uint32_t __rt_stack_pointer = 0x10000u;
const uint32_t __rt_stack_pages = 0u;

int sprintf(char *targ, const char *form, ...);

#define KR_TTYKEY   6

#define KR_OSTREAM  8

#if 0 /*prototype kdprintf */
#ifdef VERBOSE
  kdprintf(KR_OSTREAM,
	   "ttyread: \r\n"
	   );
#endif
#endif

char *
my_strcpy(char *buf, char *str)
{
  while (*buf) buf++;
  
  while (*str)
    *buf++ = *str++;

  *buf = 0;
  
  return buf;
}

char *
my_hexcpy(char *buf, uint32_t w, uint32_t minChars)
{
  static char hexdigits[16] = {
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
  };

  int i;
  char val[11];
  char *valp;

  if (minChars > 8) minChars = 8;
  
  *buf++ = '0';
  *buf++ = 'x';

  
  for (i = 0; i < 8; i++) {
    unsigned xc = w & 0xfu;
    val[7-i] = hexdigits[xc];
    w >>= 4;
  }
  val[8] = 0;

  valp = val;
  
  while (*valp == '0' && valp[minChars] != 0)
    valp++;

  /* copy valp into buf */
  while (*valp)
    *buf++ = *valp++;

  *buf = 0; 

  return buf;
}

#define EVENT_MASK (CharSrc_ALL_Events)
#define MAX_CHARS 10

#define RESET_CHAR '\01' /* CTRL-A */
#define RST_CHR_ASC "Ctrl-A"

#define ENTER_MESS "\r\nttyread: please enter how many chars to grab at " \
                   "a time (1 - 9)\r\n"

#define _INTRO_MESS "This is ttyread.  My job is to wait on the tty for " \
                  "either:\r\n" \
                  " 1)  full buffer recieved (actually the 2nd case " \
		  "in disguise)\r\n" \
		  "  or\r\n" \
		  " 2)  an event occuring.  Every 2 seconds, my " \
		  "\"friend\", tty_test, sends a user\r\n" \
		  "   event to me.  Other events are timeouts, special " \
		  "characters recieved, and\r\n" \
		  "   getting a full buffer.\r\n" \
		  "\r\n"
		   
#ifdef VERBOSE
#define _KEYRESP_MESS \	   
		  "  When the tty returns, I print out:\r\n" \
		  "   1.  The character(s) recieved (in hex and as chars)\r\n" \
		  "   2.  What events were fired.\r\n"
#else /* if !VERBOSE */
#define _KEYRESP_MESS \
		  "  When the tty returns, I echo the characters recieved.\r\n"
#endif /* if !VERBOSE */		   
#define _END_MESS \
		  "\r\n" \
		  "You can change the buffer size by hitting " \
		  RST_CHR_ASC".\r\n" \
		  "\r\n" \
		  "  Have fun!\r\n\r\n\r\n"

#define STAR_MESS _INTRO_MESS _KEYRESP_MESS _END_MESS

uint16_t specialChars[] = {
  '\r',
  RESET_CHAR,
  '\x4'
};

#define NUM_SPECHARS (sizeof(specialChars)/sizeof(specialChars[0]))


void writeString(const char *str, uint32_t length)
{
  uint32_t actualLength;

  while (length) {
    if (charsrc_write(KR_TTYKEY, length, str, &actualLength) != RC_OK) {
      kdprintf(KR_OSTREAM,
	       "ttyread: write didn't return RC_OK!\r\n"
	       );
      return;
    }
    if (length < actualLength) {
      kdprintf(KR_OSTREAM,
	       "ttyread: actualLength > length! in writeString\r\n"
	       );
      
    }
    length -= actualLength;
  }
}

void getDigitResult(const char *prompt, uint32_t krTty, uint32_t *reslt)
{
  writeString(prompt, strlen(prompt));
  
  while (1) {
    char theDigit;
  
    uint32_t result = charsrc_wait_for_event(KR_TTYKEY,
					 1, &theDigit,
					 NULL,
					 CharSrc_FilledBufferEvent,
					 NULL);
    if (result != RC_OK) {
      continue;
    }
    if (theDigit > '0' && theDigit <= '9') {
      *reslt = theDigit - '0';
      writeString(&theDigit, 1);
      writeString("\r\n",2);
      break;
    }
  }
}

void main(void)
{
  uint32_t numChars;

#ifdef VERBOSE
  kdprintf(KR_OSTREAM,
	   "ttyread: starting up\r\n"
	   );
#endif

  if (charsrc_set_special_chars(KR_TTYKEY,
				specialChars,
				NUM_SPECHARS) != RC_OK) {
    kdprintf(KR_OSTREAM,
	     "ttyread: error setting special chars\r\n"
	     );
  }
			    
  writeString(STAR_MESS, strlen(STAR_MESS));

  getDigitResult(ENTER_MESS, KR_TTYKEY, &numChars);
  
  kprintf(KR_OSTREAM,
	  "ttyread: Getting characters in batches of %d\n",
	  numChars);
  
  while (1) { /* forever */
    uint32_t idx;
    char buffer[MAX_CHARS]; /* for holding recieved characters */
    char outbuff[256]; /* for holding printed strings */
    uint32_t reset_entered = 0;
    
    char *outptr = outbuff;

    uint32_t charsRead;
    uint32_t triggeredEvents;
    
    uint32_t result = charsrc_wait_for_event(KR_TTYKEY,
					 numChars, buffer,
					 &charsRead,
					 EVENT_MASK,
					 &triggeredEvents);

    if (result != RC_OK) {
      kdprintf(KR_OSTREAM,
	       "ttyread: wait_for_event failed!\r\n"
	       );
      continue;
    }

    *outptr = 0;

    /* if VERBOSE, print out a bunch of information.  Otherwise, just
       print the characters */
#ifdef VERBOSE

    outptr = my_strcpy(outptr,"Got ");
    outptr = my_hexcpy(outptr,charsRead,1);

    if (charsRead) outptr = my_strcpy(outptr,":");
#endif /*VERBOSE*/
    for (idx = 0; idx < charsRead; idx++) {
#ifdef VERBOSE
      outptr = my_strcpy(outptr," ");
      outptr = my_hexcpy(outptr,buffer[idx],2);
      outptr = my_strcpy(outptr," (");
#endif      
      /* add a line-feed before the return */
      if (buffer[idx] == '\r') {
	*outptr++ = '\n';
      }
      if (buffer[idx] == RESET_CHAR) {
	reset_entered = 1;
	continue;
      }
      *outptr++ = buffer[idx];
      *outptr = 0;
#ifdef VERBOSE
      outptr = my_strcpy(outptr,")");
#endif
    }
#ifdef VERBOSE
    outptr = my_strcpy(outptr,". Flags = ");
    outptr = my_hexcpy(outptr,triggeredEvents, 4);
    outptr = my_strcpy(outptr,".\r\n");
#endif
    writeString(outbuff,strlen(outbuff));

    if (reset_entered) {
      getDigitResult(ENTER_MESS, KR_TTYKEY, &numChars);
    }
  }
}
