/*
 * Copyright (C) 1998, 1999, Jonathan Adams.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/TtyKey.h>
#include <eros/SleepKey.h>

#include <domain/domdbg.h>

const uint32_t __rt_stack_pointer = 0x10000u;
const uint32_t __rt_stack_pages = 0u;

#define KR_TTYKEY   6

#define KR_OSTREAM  8
#define KR_SLEEPKEY 9

#define EVENT_INTERVAL 2 /* in seconds */

void
main(void)
{
  sl_sleep(KR_SLEEPKEY, 2000u);
  kprintf(KR_OSTREAM,
	  "Tty_test: starting up.  1 UserEvent every %d secs.\n",
	  EVENT_INTERVAL);

  while(1) {
    sl_sleep(KR_SLEEPKEY, EVENT_INTERVAL * 1000u);
#ifdef VERBOSE
    kprintf(KR_OSTREAM, "Tty_test: posting UserEvent\n");
#endif
    charsrc_post_event(KR_TTYKEY, CharSrc_UserEvent);
  }
	  
  return;
}
