/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <eros/target.h>
#include <eros/Key.h>
#include <eros/Invoke.h>
#include <eros/NodeKey.h>
#include <eros/ReturnerKey.h>
#include <eros/SleepKey.h>
#include <eros/ProcessKey.h>
#include <domain/ConstructorKey.h>
#include <domain/SpaceBankKey.h>
#include <domain/SuperNodeKey.h>
#include <domain/domdbg.h>

#define KR_CONSTIT  1
#define KR_SELF     2
#define KR_BANK     4
#define KR_SCHED    5
#define KR_ZSF      6
#define KR_SLEEP    7
#define KR_RETURNER 8
#define KR_OSTREAM  9
#define KR_SCRATCH0 10
#define KR_SCRATCH1 11
#define KR_ZS       12
#define KR_SNODE    13


#define KC_ZSF      1
#define KC_SLEEP    2
#define KC_RETURNER 3
#define KC_OSTREAM  4
#define KC_SNODEF   5

#define PAGE_COUNT  4
#define NODE_COUNT  4

const uint32_t __rt_stack_pages = 1;
const uint32_t __rt_stack_pointer = 0x10000;

extern uint64_t rdtsc();

void
init()
{
  uint32_t result;
  
  /* Need to fabricate a root directory: */
  node_copy(KR_CONSTIT, KC_OSTREAM, KR_OSTREAM);
  node_copy(KR_CONSTIT, KC_SLEEP, KR_SLEEP);
  node_copy(KR_CONSTIT, KC_RETURNER, KR_RETURNER);
  node_copy(KR_CONSTIT, KC_ZSF, KR_ZSF);
  node_copy(KR_CONSTIT, KC_SNODEF, KR_SNODE);

  result = spcbank_buy_nodes(KR_BANK, 1, KR_SCRATCH0, KR_ZERO, KR_ZERO);

  if (result)
    kdprintf(KR_OSTREAM, "Node purchase failed\n");
  
  // Make it blss 5:
  node_make_node_key(KR_SCRATCH0, 7, KR_SCRATCH0);
  
  process_copy(KR_SELF, ProcAddrSpace, KR_SCRATCH1);

  node_swap(KR_SCRATCH0, 0x0, KR_SCRATCH1, KR_ZERO);

  kprintf(KR_OSTREAM, "Make new node be my address space:\n");

  process_swap(KR_SELF, ProcAddrSpace, KR_SCRATCH0, KR_ZERO);

  result = constructor_request(KR_SNODE, KR_BANK, KR_SCHED, KR_ZERO,
			       KR_SNODE);

  sl_sleep(KR_SLEEP, 1000);
}

void
make_fresh_space()
{
  uint32_t result;
  
  process_copy(KR_SELF, ProcAddrSpace, KR_SCRATCH0);

  result = constructor_request(KR_ZSF, KR_BANK, KR_SCHED, KR_ZERO,
			 KR_ZS);

  kdprintf(KR_OSTREAM,
	   "result: 0x%08x. Insert new ZSF in seg node:\n",
	   result); 

  node_swap(KR_SCRATCH0, 0x8, KR_ZS, KR_ZERO);
#if 0
  kdprintf(KR_OSTREAM, "Make new thing be my address space:\n");

  process_swap(KR_SELF, ProcAddrSpace, KR_SCRATCH0, KR_ZERO);
#endif
}

void
main()
{
  char *addr = (char *) 0x80000000;
  uint64_t t_start, t_end;
  int i;
  
  init();
  
#if 0
  make_fresh_space();
#endif

  for (i = 0; i < PAGE_COUNT; i++) {
    uint32_t result;
    
    kdprintf(KR_OSTREAM, "Buy page %d\n", i);
    result = spcbank_buy_pages(KR_BANK, 1, KR_SCRATCH0, KR_ZERO,
			       KR_ZERO);
    if (result != RC_OK)
      kdprintf(KR_OSTREAM, "Spacebank scrod\n");
    
    supernode_swap(KR_SNODE, i, KR_SCRATCH0, KR_ZERO);
  }

  for (i = 0; i < PAGE_COUNT; i++) {
    supernode_copy(KR_SNODE, i, KR_SCRATCH0);

    kdprintf(KR_OSTREAM, "Returning page %d\n", i);
    spcbank_return_page(KR_BANK, KR_SCRATCH0);
  }

  for (i = 0; i < PAGE_COUNT; i++) {
    uint32_t result;
    kdprintf(KR_OSTREAM, "Buy page %d\n", i);

    result = spcbank_buy_pages(KR_BANK, 1, KR_SCRATCH0, KR_ZERO,
			       KR_ZERO);
    if (result != RC_OK)
      kdprintf(KR_OSTREAM, "Spacebank scrod\n");
    
    supernode_swap(KR_SNODE, i, KR_SCRATCH0, KR_ZERO);
  }

#if 0
  for (i = 0; i < NODE_COUNT; i++) {
    uint32_t result = spcbank_buy_nodes(KR_BANK, 1, KR_SCRATCH0, KR_ZERO,
				    KR_ZERO);
    if (result != RC_OK)
      kdprintf(KR_OSTREAM, "Spacebank scrod\n");
    
    supernode_swap(KR_SNODE, PAGE_COUNT+i, KR_SCRATCH0, KR_ZERO);
  }


  for (i = 0; i < NODE_COUNT; i++) {
    supernode_copy(KR_SNODE, PAGE_COUNT+i, KR_SCRATCH0);

    spcbank_return_node(KR_BANK, KR_SCRATCH0);
  }
#endif

  kdprintf(KR_OSTREAM, "Now time it\n");

  t_start = rdtsc();

  for (i = 0; i < PAGE_COUNT; i++) {
    addr[i*EROS_PAGE_SIZE] = i;
  }
  
  t_end = rdtsc();
  t_end -= t_start;

  kdprintf(KR_OSTREAM, "Time to grow and touch %d pages: %d cycles\n",
	   PAGE_COUNT, (unsigned long) t_end);
}
