/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <KEYDEFS.h>

/* EROS INCLUDES HERE */
#include <eros/target.h>

/* UNIX INCLUDES HERE */
#include <unistd.h>
#include <limits.h>
#include <sys/errno.h>

/* EMULATION INCLUDES HERE */
#include "EROS_EMUL.h"


/* Per-file-descriptor fd info table */
fd_info_t __fd_info[OPEN_MAX];

/* Current working directory */
char __cwd[PATH_MAX];


