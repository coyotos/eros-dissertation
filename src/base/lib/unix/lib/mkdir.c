/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <KEYDEFS.h>

#include <eros/target.h>
#include <eros/ReturnerKey.h>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/NodeKey.h>
#include <domain/DirectoryKey.h>
#include <domain/ConstructorKey.h>
#include <domain/SuperNodeKey.h>
#include <domain/domdbg.h>

#include <sys/errno.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>

#include "EROS_EMUL.h"

#define DEBUG if (0)

int
mkdir(const char *name, mode_t mode)
{
  int fd;
  const char *tail;

  if (*name == 0) {
    errno = ENOENT;
    return -1;
  }
    
  /* Find an available FD first: */
  for (fd = 0; fd < OPEN_MAX; fd++)
    if (__fd_info[fd].isOpen == 0)
      break;

  if (fd == OPEN_MAX) {
    errno = EMFILE;
    return -1;
  }
  
  tail = _rsearch_char(name, name + strlen(name), '/');

  DEBUG kdprintf(KR_OSTREAM, "mkdir(%s, 0x%08d) "
	   "[hd=0x%08x, tl=0x%08x]\n", name,
	   mode, name, tail);
    
  if ( _eros_walktree(name, tail) == 0 )
    return -1;
    
  DEBUG kdprintf(KR_OSTREAM, "mkdir(): Got the directory...\n");

  if (*tail == '/')
    tail++;			/* skip the trailing '/' */

  if (key_kt(KR_SCRATCH) != AKT_Directory) {
    DEBUG kdprintf(KR_OSTREAM, "mkdir(): cur object not a directory\n");
    errno = ENOTDIR;
    return -1;
  }
      
  node_copy(KR_CONSTIT, KC_DIRC, KR_SCRATCH2);
  if (constructor_request(KR_SCRATCH2, KR_BANK, KR_SCHED, KR_ZERO,
			  KR_SCRATCH2) != RC_OK) {
    errno = ENOSPC;
    return -1;
  }

  DEBUG kdprintf(KR_OSTREAM, "mkdir(): Inserting (\"%s\",<key>) into dir\n",
	   tail);
    
  /* Now have new directory in KR_SCRATCH2 */
  if (directory_link(KR_SCRATCH, tail, strlen(tail), KR_SCRATCH2) !=
      RC_OK) {
    DEBUG kdprintf(KR_OSTREAM, "Link fails\n");
    errno = ENOSPC;
    return -1;
  }

  /* mkdir does not return an FD */
  return 0;
}
