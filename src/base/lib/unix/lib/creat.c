/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <KEYDEFS.h>

#include <eros/target.h>
#include <eros/ReturnerKey.h>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <eros/NodeKey.h>
#include <domain/DirectoryKey.h>
#include <domain/ConstructorKey.h>
#include <domain/SuperNodeKey.h>
#include <domain/domdbg.h>

#include <sys/errno.h>
#include <sys/fcntl.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>

#include "EROS_EMUL.h"

#define DEBUG if (0)

int
_eros_creat(const char *name, int flags, mode_t mode)
{
  int fd;
  const char *tail;

  if (*name == 0) {
    errno = ENOENT;
    return -1;
  }
    
  /* Find an available FD first: */
  for (fd = 0; fd < OPEN_MAX; fd++)
    if (__fd_info[fd].isOpen == 0)
      break;

  if (fd == OPEN_MAX) {
    errno = EMFILE;
    return -1;
  }
  
  /* find the directory component of the name */
  tail = _rsearch_char(name, name + strlen(name), '/');

  DEBUG kdprintf(KR_OSTREAM, "creat(%s, 0x%08x, 0x%08d) "
	   "[hd=0x%08x, tl=0x%08x]\n", name, flags,
	   mode, name, tail);
    
  if ( _eros_walktree(name, tail) == 0 )
    return -1;
  
  if (*tail == '/')
    tail++;			/* skip the trailing '/' */

  DEBUG kdprintf(KR_OSTREAM, "creat(): Got the directory...\n");

  if (key_kt(KR_SCRATCH) != AKT_Directory) {
    DEBUG kdprintf(KR_OSTREAM, "creat(): cur object not a directory\n");
    errno = ENOTDIR;
    return -1;
  }
      
  node_copy(KR_CONSTIT, KC_FILEC, KR_SCRATCH2);
  if (constructor_request(KR_SCRATCH2, KR_BANK, KR_SCHED, KR_ZERO,
			  KR_SCRATCH2) != RC_OK) {

    DEBUG kdprintf(KR_OSTREAM, "creat(): couldn't build file\n");
    
    errno = ENOSPC;
    return -1;
  }

  DEBUG kdprintf(KR_OSTREAM, "creat(): Inserting (\"%s\",<key>) into dir\n",
	   tail);
    
  /* Now have new file in KR_SCRATCH2 */
  if (directory_link(KR_SCRATCH, tail, strlen(tail), KR_SCRATCH2) !=
      RC_OK) {
    DEBUG kdprintf(KR_OSTREAM, "Link fails\n");
    errno = ENOSPC;
    return -1;
  }

  /* Need to move file to KR_SCRATCH, which is the output register */
  copy_key_reg(KR_RETURNER, KR_SCRATCH2, KR_SCRATCH);

  DEBUG kdprintf(KR_OSTREAM, "creat(): Add fd %d to file tab\n", fd);
    
  if (supernode_swap(KR_FD_TAB, fd, KR_SCRATCH, KR_ZERO) != RC_OK) {
    /* FIX: Destroy file object here */
    errno = ENOSPC;		/* NOT ENOSPC, which should only  be
				   reported for creates */
    return -1;
  }
  
  DEBUG kdprintf(KR_OSTREAM, "creat(): File is now open\n", fd);
    
  __fd_info[fd].isOpen = 1;
  __fd_info[fd].offset = 0;
  return fd;
}

int
creat(const char *name, mode_t mode)
{
  return _eros_creat(name, O_WRONLY|O_TRUNC|O_CREAT, mode);
}
