/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <KEYDEFS.h>

/* EROS INCLUDES HERE */
#include <eros/target.h>
#include <eros/ReturnerKey.h>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <domain/SuperNodeKey.h>
#include <domain/FileKey.h>
#include <domain/domdbg.h>


/* UNIX INCLUDES HERE */
#include <unistd.h>
#include <limits.h>
#include <sys/errno.h>

/* EMULATION INCLUDES HERE */
#include "EROS_EMUL.h"

#define DEBUG if (1)

int
write(int fd, const void *buf, size_t len)
{
  size_t resid = len;
  
  if (fd == -1) {
    errno = ENOENT;
    return -1;
  }
  
  DEBUG kdprintf(KR_OSTREAM, "write(): Fetch key to fd %d\n", fd);
    
  supernode_copy(KR_FD_TAB, fd, KR_SCRATCH);
  if (key_kt(KR_SCRATCH) == AKT_Number) {
    errno = ENOENT;
    return -1;
  }
  
  while (resid) {
    uint32_t result;
    uint32_t outLen;
    uint64_t offset = __fd_info[fd].offset;
    
    size_t ask = (resid < EROS_PAGE_SIZE) ? resid : EROS_PAGE_SIZE;
    
    DEBUG kdprintf(KR_OSTREAM,
	     "write(): file_write(<key>,off=0x%08x%08x,len=%d,buf=0x%08x,...)\n",
	     (uint32_t)(offset >>32),
	     (uint32_t) offset,
	     len,
	     buf);
    
    result = file_write(KR_SCRATCH, offset, ask, buf, &outLen);
    if (result != RC_OK) {
      errno = EINVAL;
      return -1;
    }
    
    buf += outLen;
    resid -= outLen;
    __fd_info[fd].offset += outLen;

    if (outLen < ask)
      break;
    
  }
    
  DEBUG kdprintf(KR_OSTREAM, "write() returns %d\n", len - resid);

  return (len - resid);
}
