/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/* Per-file-descriptor fd info table */

extern const char * _search_char(const char *start, const char *end, char c);
extern const char * _rsearch_char(const char *start, const char *end, char c);
extern int _eros_walktree(const char *name, const char* name_end);
int _eros_creat(const char *name, int flags, mode_t mode);

typedef struct {
  uint64_t offset;
  int isOpen;
} fd_info_t;

extern fd_info_t __fd_info[OPEN_MAX];
extern char __cwd[PATH_MAX];
