/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <KEYDEFS.h>

#include <eros/target.h>
#include <eros/ReturnerKey.h>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <domain/DirectoryKey.h>
#include <domain/SuperNodeKey.h>
#include <domain/domdbg.h>

#include <sys/errno.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>

#include "EROS_EMUL.h"

#define DEBUG if (0)

int
close(int fd)
{
  /* Find an available FD first: */
  if (__fd_info[fd].isOpen == 0) {
    errno = EBADF;
    return -1;
  }

  DEBUG kdprintf(KR_OSTREAM, "close(%d): swap out old key\n", fd);
  
  if (supernode_swap(KR_FD_TAB, fd, KR_ZERO, KR_SCRATCH) != RC_OK) {
    errno = ENOENT;		/* NOT ENOSPC, which should only  be
				   reported for creates */
    return -1;
  }
  
  DEBUG kdprintf(KR_OSTREAM, "close(%d): someday tell file to decrement refcnt\n", fd);
  /* FIX: Destroy file object in KR_SCRATCH here */

  __fd_info[fd].isOpen = 0;
  __fd_info[fd].offset = 0;
  return fd;
}
