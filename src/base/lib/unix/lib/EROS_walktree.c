/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <KEYDEFS.h>

#include <eros/target.h>
#include <eros/ReturnerKey.h>
#include <eros/Invoke.h>
#include <eros/StdKeyType.h>
#include <domain/DirectoryKey.h>
#include <domain/SuperNodeKey.h>
#include <domain/domdbg.h>

#include <sys/errno.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>

#include "EROS_EMUL.h"

#define DEBUG if (0)

/* leaves result in KR_SCRATCH */
int
_eros_walktree(const char *name, const char* name_end)
{
  int depth;

  const char *next_chunk = name;
  
  if ((name_end - name) > PATH_MAX) {
    errno = ENAMETOOLONG;
    return 0;
  }
    
  /* walktree must do directory walk for creat/open.  If the user
     provided a file name with no directory component, then name_end
     == name. note that the following test still does the right thing
     in this case, since the only way it can occur is if there is no
     '/' character in the name. */

  if (*name == '/') {
    DEBUG kdprintf(KR_OSTREAM, "walktree(): Grab fs root\n");
    copy_key_reg(KR_RETURNER, KR_FSROOT, KR_SCRATCH);
  }
  else {
    DEBUG kdprintf(KR_OSTREAM, "walktree(): Grab cwd\n");
    copy_key_reg(KR_RETURNER, KR_CWD, KR_SCRATCH);
  }
  
  while (name != name_end && *name == '/')
    name++;

  /* It is perfectly okay if there was no name... */
  if (name == name_end) {
    DEBUG kdprintf(KR_OSTREAM, "walktree(): name is empty\n");
    return 1;
  }

  for (depth = 0; depth < 255; depth++) {
    size_t len;

    while (name != name_end && *name == '/')
      name++;

    if (name == name_end)		/* found it. */
      break;
    
    if (key_kt(KR_SCRATCH) != AKT_Directory) {
      DEBUG kdprintf(KR_OSTREAM, "walktree(): cur object not a directory\n");
      errno = ENOTDIR;
      return 0;
    }
      
    next_chunk = _search_char(name, name_end, '/');

    len = next_chunk - name;

    DEBUG {
      char thename[PATH_MAX];
      strncpy(thename, name, len);
      thename[len] = 0;
      DEBUG kdprintf(KR_OSTREAM, "walktree(): looking up \"%s\"\n", thename);
    }

    if (directory_lookup(KR_SCRATCH, name, next_chunk - name,
			 KR_SCRATCH) != RC_OK) { 
      DEBUG kdprintf(KR_OSTREAM, "walktree(): name not found in cur directory\n");
      errno = ENOENT;
      return 0;
    }

    name = next_chunk;
  }

  if (depth == 255) {
    DEBUG kdprintf(KR_OSTREAM, "walktree(): looping directory structure\n");
    errno = EMLINK;		/* too many links (?) -- should be ELOOP */
    return 0;
  }

  /* success */
  return 1;
}
