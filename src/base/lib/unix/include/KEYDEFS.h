/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */


#define KR_ZERO      0
#define KR_CONSTIT   1
#define KR_SELF      2
#define KR_FSROOT    3
#define KR_BANK      4
#define KR_SCHED     5
#define KR_FD_TAB    6
#define KR_RETURNER  7
#define KR_CWD       8
#define KR_SCRATCH   9
#define KR_SCRATCH2  10
#define KR_OSTREAM   11

#define KC_FILEC     1
#define KC_DIRC      2
#define KC_SNODEC    3
#define KC_OSTREAM   4
#define KC_SLEEP     5
#define KC_RETURNER  6
