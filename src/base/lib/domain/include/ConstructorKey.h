#ifndef __CONSTRUCTOR_H__
#define __CONSTRUCTOR_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

#define OC_Constructor_IsDiscrete             1
#define OC_Constructor_Request                2
#define OC_Constructor_Seal                   3
#define OC_Constructor_Insert                 4

/* Also add #defines for the error return values... */
#define RC_Constructor_Indiscrete             1
#define RC_Constructor_NotSealed              2
#define RC_Constructor_NotBuilder             3

#define Constructor_Product_Kpr   16
#define Constructor_Product_Spc   17
#define Constructor_Product_Sym   18
#define Constructor_Product_PC    19

#ifndef __ASSEMBLER__
uint32_t constructor_request(uint32_t krConstructor, uint32_t krBank,
			     uint32_t krSched, uint32_t krArg0,
			     uint32_t krProduct /* OUT */);
uint32_t constructor_is_discreet(uint32_t krConstructor);
uint32_t constructor_seal(uint32_t krConstructor, uint32_t krRequestor);
uint32_t constructor_insert(uint32_t krConstructor, uint32_t ndx,
			    uint32_t krConstit);
#endif

#endif /* __CONSTRUCTOR_H__ */

