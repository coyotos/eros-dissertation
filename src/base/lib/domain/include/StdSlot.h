#ifndef __STDSLOT_H__
#define __STDSLOT_H__

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

#define KR_ZERO			0
#define KR_SELF			1
#define KR_BANK			2
#define KR_INIT_DOMCRE		3
#define KR_INIT_DESTRUCT	4
#define KR_ARG0			12
#define KR_ARG1			13
#define KR_ARG2			14
#define KR_RESUME		15

#endif /* __STDSLOT_H__ */

