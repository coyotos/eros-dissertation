/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/KeyBitsKey.h>
#include "domdbg.h"

void
ShowKey(uint32_t krConsole, uint32_t krKeyBits, uint32_t kr)
{
  struct Bits bits;
  uint32_t version;

  GetKeyBits(krKeyBits, kr, &version, &bits);
  
  kprintf(krConsole, "Keybits version is 0x%08x\n", version);

  kprintf(krConsole, "Keybits data is 0x%08x 0x%08x 0x%08x 0x%08x\n",
	  bits.w[0],
	  bits.w[1],
	  bits.w[2],
	  bits.w[3]);
}
