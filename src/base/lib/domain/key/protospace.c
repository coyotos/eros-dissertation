/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

#include <eros/target.h>
#include <eros/Invoke.h>
#include <eros/Key.h>
#include <eros/ProcessKey.h>
#include <domain/ProtoSpace.h>

/* NOTE: This code assumes that the resume key to which we should
   return is sitting in KR_RESUME. */
void
protospace_destroy(uint32_t krReturner, uint32_t krProto, uint32_t krMyDom,
		   uint32_t krMyProcCre,
		   uint32_t krBank, int smallSpace)
{
  Message msg;
  
  msg.snd_w1 = 0;
  msg.snd_w2 = 0;
  msg.snd_w3 = 0;

  /* Problem is to permute 5 keys into their proper positions.  What a
     pain in the butt! First get the returner out of the way! */

  if (krReturner != PSKR_RETURNER) {
    msg.snd_key0 = krReturner;
    msg.snd_key1 = PSKR_RETURNER;
    msg.snd_key2 = KR_ZERO;
    msg.snd_len = 0;
    msg.rcv_len = 0;
    msg.rcv_key0 = PSKR_RETURNER;
    msg.rcv_key1 = krReturner;
    msg.rcv_key2 = KR_ZERO;
    msg.snd_invKey = krReturner;
  
    CALL(&msg);

    /* Now update the other arg locs to reflect this swap */
    if (krProto == PSKR_RETURNER)
      krProto = krReturner;
    if (krMyDom == PSKR_RETURNER)
      krMyDom = krReturner;
    if (krMyProcCre == PSKR_RETURNER)
      krMyProcCre = krReturner;
    if (krBank == PSKR_RETURNER)
      krBank = krReturner;

    krReturner = PSKR_RETURNER;
  }
  
  /* Now deal with the bank: */
  if (krBank != PSKR_BANK) {
    msg.snd_key0 = krBank;
    msg.snd_key1 = PSKR_BANK;
    msg.snd_key2 = krProto;
    msg.snd_len = 0;
    msg.rcv_len = 0;
    msg.rcv_key0 = PSKR_BANK;
    msg.rcv_key1 = krBank;
    msg.rcv_key2 = KR_ZERO;
    msg.snd_invKey = krReturner;
  
    CALL(&msg);

    /* Now update the other arg locs to reflect this swap */
    if (krProto == PSKR_BANK)
      krProto = krBank;
    if (krMyDom == PSKR_BANK)
      krMyDom = krBank;
    if (krMyProcCre == PSKR_BANK)
      krMyProcCre = krBank;

    krBank = PSKR_BANK;
  }

  /* Rearrange the remaining keys into their desired slots carefully,
     as they may currently be in each other's positions.  First swap
     the process creator into the right position, and then permute all
     of the others. */
  
  msg.snd_key0 = krProto;
  msg.snd_key1 = krMyDom;
  msg.snd_key2 = krMyProcCre;
  msg.snd_len = 0;
  msg.rcv_len = 0;
  msg.rcv_key0 = PSKR_PROTO;
  msg.rcv_key1 = PSKR_DOM;
  msg.rcv_key2 = PSKR_PROCCRE;
  msg.snd_invKey = krReturner;
  
  CALL(&msg);

  /* Fetch out the current address space to key reg 3: */
  (void) process_copy(PSKR_DOM, ProcAddrSpace, PSKR_SEG);
  
  msg.snd_key0 = PSKR_PROTO;
  msg.snd_key1 = KR_ZERO;
  msg.snd_key2 = KR_ZERO;
  msg.snd_key3 = KR_ZERO;
  msg.snd_w1 = 0;		/* well known protospace address */
  msg.snd_w2 = (smallSpace ? 2 : 1);
  msg.snd_w3 = 0;
  msg.snd_data = 0;
  msg.snd_len = 0;

  msg.rcv_key0 = KR_ZERO;
  msg.rcv_key1 = KR_ZERO;
  msg.rcv_key2 = KR_ZERO;
  msg.rcv_key3 = KR_ZERO;
  msg.rcv_len = 0;		/* no data returned */

  /* No string arg == I'll take anything */
  msg.snd_invKey = PSKR_DOM;
  msg.snd_code = OC_Process_SwapMemory32;

  CALL(&msg);
  /* NOTREACHED */
}
