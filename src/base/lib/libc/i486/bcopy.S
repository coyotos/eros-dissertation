/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */


#include <eros/machine/asm.h>


/*
 * bcopy(caddr_t from, caddr_t to, size_t len);
 * Copy len bytes.
 */
ENTRY(bcopy)
#if 0
LEXT(ovbcopy)
#endif
	pushl	%esi
	pushl	%edi
	pushl	%ecx
	
	movl	16(%esp),%esi
	movl	20(%esp),%edi
	movl	24(%esp),%ecx
	cmpl	%esi,%edi		/* potentially overlapping? */
	jnb	1f
	cld				/* nope, copy forward */
	shrl	$2,%ecx			/* copy by 32-bit words */
	rep
	movsl
	movl	24(%esp),%ecx
	andl	$3,%ecx			/* any bytes left? */
	rep
	movsb
	popl	%ecx
	popl	%edi
	popl	%esi
	ret

	.align  ALIGN
1:	addl	%ecx,%edi		/* copy backward */
	addl	%ecx,%esi
	std
	andl	$3,%ecx			/* any fractional bytes? */
	decl	%edi
	decl	%esi
	rep
	movsb
	movl	24(%esp),%ecx		/* copy remainder by 32-bit words */
	shrl	$2,%ecx
	subl	$3,%esi
	subl	$3,%edi
	rep
	movsl
	popl	%ecx
	popl	%edi
	popl	%esi
	cld
	ret
