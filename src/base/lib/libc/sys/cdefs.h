#ifndef	_SYS_CDEFS_H
#define	_SYS_CDEFS_H	1

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */



#ifdef	__GNUC__


/* Figure out how to declare functions that (1) depend only on their
   parameters and have no side effects, or (2) don't return.  */

#if __GNUC__ < 2 || (__GNUC__ == 2 && (__GNUC_MINOR__ < 5 || (defined(__cplusplus) && __GNUC_MINOR__ < 6)))
  /* Old GCC way. */
#error "Old GCC not supported"

#else

#define __inline inline

  /* New GCC way. */
#ifndef	__CONSTVALUE
#define	__CONSTVALUE
#define	__CONSTVALUE2	__attribute__ ((const))
#endif

#ifndef	__NORETURN
#define	__NORETURN
#define	__NORETURN2	__attribute__ ((noreturn))
#endif

#endif

#else	/* Not GCC.  */

#define	__inline		/* No inline functions.  */
#define	__CONSTVALUE		/* No idempotent functions.  */
#define	__CONSTVALUE2
#define	__NORETURN		/* No functions-of-no-return.  */
#define	__NORETURN2

#endif	/* GCC.  */

#if (defined (__STDC__) && __STDC__) || defined (__cplusplus)

#ifndef __P
#define	__P(args)	args
#endif
#define	__const		const
#define	__signed	signed
#define	__volatile	volatile
#define	__DOTS		, ...

#else	/* Not ANSI C or C++.  */

#ifndef __P
#define	__P(args)	()	/* No prototypes.  */
#endif
#define	__const			/* No ANSI C keywords.  */
#define	__signed
#define	__volatile
#define	__DOTS

#endif	/* ANSI C or C++.  */

/* This is not a typedef so `const __ptr_t' does the right thing.  */
#define __ptr_t void *
typedef long double __long_double_t;

/* C++ needs to know that types and declarations are C, not C++.  */
#ifdef	__cplusplus
#define	__BEGIN_DECLS	extern "C" {
#define	__END_DECLS	}
#else
#define	__BEGIN_DECLS
#define	__END_DECLS
#endif

#endif	 /* sys/cdefs.h */
