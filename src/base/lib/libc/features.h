#ifndef	_FEATURES_H
#define	_FEATURES_H	1

/*
 * Copyright (C) 1998, 1999, Jonathan S. Shapiro.
 *
 * This file is part of the EROS Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/* This is a highly elided version of the features.h file from linux, 
   since we know the answers for domain-oriented code. */

#undef __USE_ANSI
#undef __USE_POSIX
#undef __USE_POSIX2
#undef __USE_BSD
#undef __USE_MISC
#undef __FAVOR_BSD

#define	__USE_ANSI	1
#define __USE_POSIX     1
#define __USE_POSIX2    1
#define __USE_BSD       1
#define __USE_MISC      1
#define __FAVOR_BSD     1

#undef	__GNU_LIBRARY__
#define	__GNU_LIBRARY__	1

#ifdef __GNUC__

#ifndef __P
#define	__P(args)	args	/* GCC can always grok prototypes.  */
#endif
#define	__DOTS		, ...

#endif

/* Figure out how to declare functions that (1) depend only on their
   parameters and have no side effects, or (2) don't return.  */

#if __GNUC__ < 2 || (__GNUC__ == 2 && (__GNUC_MINOR__ < 5 || (defined(__cplusplus) && __GNUC_MINOR__ < 6)))

#error "Older GCC not supported"

#else

  /* New GCC way. */
#ifndef	__CONSTVALUE
#define	__CONSTVALUE
#if defined(const) || !defined(__STDC__)
#define	__CONSTVALUE2	/* We don't want empty __attribute__ (()). */
#else
#define	__CONSTVALUE2	__attribute__ ((const))
#endif
#endif
#ifndef	__NORETURN
#define	__NORETURN
#ifdef noreturn
#define	__NORETURN2	/* We don't want empty __attribute__ (()). */
#else
#define	__NORETURN2	__attribute__ ((noreturn))
#endif
#endif
#endif

/* This is here only because every header file already includes this one.  */
#ifndef __ASSEMBLER__
#include <sys/cdefs.h>
#endif

#endif	/* __features.h  */
