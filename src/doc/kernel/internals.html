<html>
<head>
<title>EROS Kernel Internals</title>
</head>

<body>
<h1>EROS Kernel Internals</h1>
<address>
<a href="http://www.cis.upenn.edu/~shap/home.html">Jonathan
S. Shapiro</a>
<br> Extremely Reliable OS Group
<br> 210 Seven Bridges Road
<br> Mt Kisco, NY  10549
<br> <a href="mailto:shap@eros-os.org">shap@eros-os.org</a>
</address>
<p> This document is an attempt to describe the internal structure of
the EROS kernel.  It's purpose is to provide an introduction to the
EROS kernel suitable for anyone interested in modifying or extending
the EROS kernel.  The document provides a description of the most
important design principles of the EROS kernel, and a description of
the major structural components of the system.  
<p> It has been my experience that exhaustive internals documents tend
to become rapidly out of date.  Rather than attempt to be exhaustive,
this document endeavours to focus on those architectural highlights
that are likely to be lasting fixtures of the implementation.  Where
appropriate, the document illustrates its points with a description of
the i386 implementation. Though the i386 implementation will
inevitably drift from the description provided here, and may differ
significantly from the implementation for other architectures, this
document should provide sufficient background that a reader of the
EROS kernel code can readily figure out what is going on.
<p> The <em>EROS Kernel Internals</em> document covers the following
areas:
<ul>
  <li> Implementation History
  <li> Architectural Overview
  <li> Implementation Principles
  <li> Process and Interrupt Management
  <li> The Block I/O Subsystem
  <li> Kernel Daemons
  <li> Segment Logic
  <li> The Persistence Subsystem
</ul>
<p> This document describes the <strong>third</strong> EROS kernel.
Where reference is made to <em>the EROS kernel</em>, you should assume
that the reference is to the third implementation unless otherwise
specified.
<H1>Implementation History</H1>
<p> The original goal of the EROS project was to build a faithful
reproduction of the <a
href="http://www.cis.upenn.edu/~KeyKOS">KeyKOS</a> system, and was
motivated in large measure by Key Logic's ongoing failure to license
KeyKOS under reasonable terms.  After many attempts to obtain a KeyKOS
license, I finally hit tilt and decided to build a new kernel.  In
March 1992 (or thereabouts) the EROS project was born, and in April
work began on the first implementation of the kernel.
<p> The original EROS project envisioned only small departures from
the KeyKOS system.  The implementation proceeded in a low key way
until November (or so - I need to check the date) of 1993, when the
entire implementation effort was lost due to a media failure.  The
available backups were several months old, and since they were
critical months, the backups were essentially worthless.  For this and
other reasons, the project was set aside.
<p> In January 1995, I restarted work on EROS in connection with my
thesis at the University of Pennsylvania.  This marked the start of
the second implementation.  In the intervening time, I had made some
study of context switching and scheduling techniques, and had come to
the conclusion that significant changes to both the design and the
implementation were desirable:
<ul>
  <li> <em>Capabilities</em> By making a conceptually small change to the
       internal structure of capabilities, it was possible to cut the
       message send time nearly in half.  The consequences of this
       change, however, touch almost every part of the EROS kernel.
       <p> 
  <li> <em>Priority scheduling</em> The KeyKOS system provided no
       static priority system, preferring to leave scheduling policy
       in the hands of the user.  In the interest of multimedia
       and high-speed networking support, the meter system was
       abandoned in favor of a more conventional priority structure.
</ul>
The second implementation, as designed, reflected these changes.  Like
the first, it was not completed.
<p> By the July 1995, it had become clear that the driver architecture
assumptions of the EROS architecture was getting in the way, and that
the kernel would be much cleaner if it were multithreaded.  Two issues
in particular motivated this change:
<ul>
  <li> <em>Disk Drivers</em> The state machines necessary to make a
       reentrant driver architecture work were proving to be a royal
       pain in the ass.  In particular, the handling of disk mount
       logic was mushrooming into a major source of complexity that I
       thought would be better avoided.  I found that I couldn't
       remember the state machine logic from one weekend to the next,
       which made a compelling case for getting rid of it.
       <p> 
  <li> <em>BIOS Support</em> It became more and more clear that there
       are lots of disk controllers out there, and rather than try to
       build drivers for all of them it might make more sense to use
       the BIOS as a default and write customized drivers only where
       there was particular advantage in doing so.  Since more than
       half of the Version 2 kernel code was expected to be disk
       drivers, this seemed worthy of some attention.
</ul>
<p> The multithreading change, however, was the proverbial final
straw, and I decided once again to build a new kernel, stealing code
from the second implementation where possible.  In fairness, it should
be noted that while little of the code was directly transferable, most
of the conceptual framework that had actually been constructed in the
second implementation was preserved.
<p>
My sincere hope is that the third implementation will actually make it
out the door!  The whole point was for people to be able to
<em>use</em> this thing.
<H2>A Word on Intellectual Property</H2>
<p> I have never seen the KeyKOS code, and none of my knowledge of
KeyKOS is proprietary.  As a result, all implementations of the EROS
kernel have been free of any limitations on redistribution.
<p> In June, 1995, after consultations with my patent attorney, I
invented an alternative to the KeyKOS factory mechanism, which was
patented.  It's not clear if the KeyKOS patent would hold up in court
(I suspect it would not), but I decided not to take any chances.  To
the best of my knowledge, the current system is free of intellectual
encumberance.
<H1>Architectural Overview</H1>
<p> The EROS kernel supplies a relatively small number of services.
While the processor architecture can make some of these services quite
complicated, the conceptual architecture can be divided into a small
number of components:
<ul>
  <li> The thread scheduler
  <li> The process manager
  <li> The segment manager
  <li> The key invocation mechanism
  <li> The object I/O subsystem (*)
  <li> The persistence subsystem (*)
</ul>
Items marked with a {*} are present only in systems having some form
of persistent store.
<p> The heart of the EROS kernel is the thread scheduler.  The kernel
itself is multithreaded, and the scheduler is responsible for
scheduling both user and kernel threads.  Kernel threads are generally
scheduled in response to specific interrupts or user requests.  User
threads are scheduled in accordance with the requirements imposed by
their priorities and the execution of the user programs.
<p> The process manager is responsible for responding to all
execution-related faults, and dispatching them to the appropriate
handler.  Each execution fault that can be encountered by a user-level
application is caught by the process manager and redirected to the
relevant handler.
<p> The segment manager is responsible for constructing all of the
necessary data structures to support hardware address translation, and
for invalidating those structures when the underlying segments change.
In the EROS kernel, page tables (or their equivalent) are simply a
cache of the state captured by a segment.  The segment manager
performs the initial construction of the hardware tables, and
maintains a number of supporting data structures to ensure that they
can be properly invalidated.
<p> The key invocations subsystem handles per-process capability
invocations.  This includes delivery of messages from one process to
another and invocations of kernel-provided capabilities.  The message
passing path is fairly carefully optimized, and is seperated from most
of the other paths in the kernel.
<p> The object I/O subsystem is responsible for all movement of nodes
and pages into and out of memory.  While we will consider this
primarily from the perspective of EROS object I/O, the object I/O
subsystem conceptually includes all disk drivers.  Since some systems
allow disks to be partitioned and used by multiple operating systems,
the object I/O subsystem must support raw disk I/O as well.
<p> The persistence subsystem is concerned with a variety of details,
including in-core object ageing, checkpointing, and migration.  This
subsystem is described in greater detail below.
<p> Each of these topics will be covered in further detail in the
sections that follow.
<H1>Implementation Principles</H1> 
<p> The EROS kernel design is guided by a number of principles.  This
section identifies those principles and the restrictions that they
impose on the implementation.
<p> The principle objectives of the EROS architecure are reliability
and security.  Philosophically, we believe that these two objectives
are inextricably intertwined: a machine whose reliability is
inadequate cannot be used for critical computation any more than a
machine that is insecure.  Conversely, any unreliability in the kernel
presents a risk to security; even if the nature of the failure is to
crash the system rather than breach security, a system crash by its
nature places the machine in a state where it is no longer under the
control of the trusted computing base.
</html>
