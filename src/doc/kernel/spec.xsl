/* -*- Mode: c -*- */
/*************************************************************
 * Architecture-dependent parameterizations -- these are
 * externally visible
 *************************************************************/

typedef uint5_t blss_t;
typedef uint4_t slot_t;

const uint Log2PageSize = 12; /* assume page size a power of 2 */
const uint Log2NodeSize = 4; /* assume page size a power of 2 */
const uint Log2AddressSize = 32; /* assume page size a power of 2 */
const uint PageSize = (2 ** Log2PageSize);
const uint NodeSlots = (2 ** Log2NodeSize);
const slot_t LastNodeSlot = NodeSlots - 1; /* fucking type checker! */
const uint MsgLimit = 65535;
const uint IpcKeys = 4;
const blss_t PageBlss = (Log2PageSize / 4) - 1;
const blss_t RedSegBlss = PageBlss - 1;

/* Format key fields. Note that changing NodeSlots requires redefining
   these. */
const uint format_initial_slots = 0;
const uint format_inv_convention = 4;
const uint format_keeper = 8;
const uint format_bg_seg = 12;
const uint format_blss = 16;

typedef uint32_t caddr_t;	/* character address */
typedef uint32_t coff_t;	/* character offset */

/*************************************************************
 * Externally visible types and enumerations
 *************************************************************/

const uint5_t MAX_BLSS = 22;

/* must not be smaller than MAX_BLSS+1, allow a factor of two for BG
   window processing */
const uint32_t TranslateDepthLimit = MAX_BLSS * 2;

/* Value of a number key -- note that the semantics feels free to use
   bitfield, numeric, and logical operations on these. In such a
   context assume the math is as for an unsigned 96 bit quantity. */
struct Number {
  uint32_t w[3];		/* w[0] least order, w[3] highest order */
};

enum FaultCode {
  FC_NoFault            = 0,	/* nothing is wrong */
  FC_SegAccess          = 1,	/* write on RO subseg */
  FC_SegInvalidAddr     = 2,	/* address not defined in segment */
  FC_SegDepth           = 3,	/* traversal depth exceeded */
  FC_SegMalformed       = 4,	/* improper key in segmode slot OR
				   segment traverses active domain */

#if 0
  /* FOLLOWING are placeholders -- I am not sure what fault codes are
     the right solution for these.  In any case, the model
     simplification of removing the disk means that the segment logic
     cannot generate them anyway. */
  
  FC_SegThroughDomain   = 5,	/* segment node is part of current domain */
  FC_SegHwPageDamage    = 6,	/* page is broken */
  FC_SegHwNodeDamage    = 7,	/* node is broken */
#endif

  FC_NoAddrSpace	= 16,	/* domain has no address space */
  FC_MalformedDomain    = 17,	/* domain malformed */
  FC_NoSchedule	        = 18,	/* domain lacks a schedule key */
  FC_BadGenRegs	        = 19,	/* gen regs holds non-number key */
  FC_RegValue	        = 20,	/* reg values inappropriate */
  FC_BreakPointFault    = 21,	/* BPT with PC at bpt instr */
  FC_BreakPointTrap     = 22,	/* BPT with PC after bpt instr */
  FC_BadOpcode	        = 23,	/* bad or undefined opcode */
  FC_DivZero	        = 24,	/* divide by zero exception */
  FC_ForeignInvocation  = 25,	/* Domain has no key registers */
  FC_BadEntryBlock      = 26,	/* Invocation had bad entry block */
  FC_ParmLack           = 27,	/* Some part of exit data is block missing */
  FC_BadSegReg          = 28	/* Segment register holds invalid value */
};

enum SlotConventions {
  ProcSched           = 0,
  ProcKeeper          = 1,
  ProcAddrSpace       = 2,
  ProcBrand           = 4,
  ProcTrapCode        = 5,
  ProcRcvBlock        = 6,
  ProcSndBlock        = 7,
  ProcOrderAndInvoke  = 8,
  ProcPCandSP         = 9,
  ProcGenRegs         = 14,
  DomKeyRegs         = 15,

#if 0
  /* the following are conventions that it is useful for keepers
     to follow universally, but the kernel does not assume that they
     do so. */
  RedSegBank         = 12,
  RedSegBackground   = 13,
  RedSegKeeper       = 14,
#endif
  RedSegFormat       = 15
}

/* User-visible attributes of a key: */
const uint16_t SEGMODE_RO          0x8000u;
const uint16_t SEGMODE_NC          0x4000u;
const uint16_t SEGMODE_BLSS_MASK   0x3fff;

/* Information passed to segment keeper */
struct SegFaultInfo {
  uint32_t  keptSegBlss;
  uint32_t  keptSegInitialSlots;
  FaultCode faultCode;
  Number    offset;
};

struct DomainFaultInfo {
  uint32_t  keptSegBlss;
  uint32_t  keptSegInitialSlots;
  FaultCode faultCode;
  Number    offset;
};

struct ArchitectureRegisters {
  /* varies according to machine, but includes all architected
     registers. */
};

/* Return Code values */
const uint32_t RC_class_number = 1;
const uint32_t RC_class_resume = 2;
const uint32_t RC_class_memory = 3;
const uint32_t RC_class_other  = 256;

/*************************************************************
 * Internally visible types, enumerations, and constants
 *************************************************************/

const uint32_t MASK4 = 0xfu;
const uint32_t MASK16 = 0xffffu;
const uint32_t MASK32 = 0xffffffffu;

typedef uint64_t PGOID;
typedef uint64_t NDOID;

enum KeyType {
  KtResume,
  KtStart,
  KtPage,
  KtNode,
  KtSegment,
  KtNode,
  KtSense,
  KtDomain,
  KtNumber,
  KtMisc,

  /* Device capabilities omitted */
};

enum InvType {
  IT_Return = 0,
  IT_Call  = 1,
  IT_Fork  = 2,
};

union Key (KeyType type) {
case KtResume:
  bool isRestart;
  NDOID oid;
case KtRestart:
  NDOID oid;
case KtStart:
  NDOID oid;
  uint16_t keyData;
case KtPage:
  PGOID oid;
  bool     readOnly;
case KtSegment:
  NDOID oid;
  uint16_t keyData;
case KtNode:
  NDOID oid;
  uint16_t keyData;
case KtSense:
  NDOID oid;
  uint16_t keyData;
case KtDomain:
  NDOID oid;
  uint16_t keyData;
case KtNumber:
  Number n;
case KtMisc:
  uint32_t subType;
};

/* Some functions require particular key types */
typedef Key(KtDomain) DomainKey;

struct Page {
  Byte data[PageSize];
  PGOID oid;
};

struct Node {
  Key slot[NodeSlots];
  NDOID oid;
};

/* Information collected during a segment traversal */
struct TranslateResult {
  Key       keptSeg;		/* last kept segment traversed */
  uint32_t  keptSegBlss;
  Number    keptSegOffset;
  Key       bgSeg;		/* last seg w/ bg seg traversed /
  FaultCode faultCode;		/* hopefully FC_NoFault */
  Key       page;
};

/*************************************************************
 * Global state
 *************************************************************/

Page PageSpace[];		/* indexed by PGOID */
Node NodeSpace[];		/* indexed by NDOID */

bool idle;
NDOID current_domain;		/* may be NULL */

Set(NDOID) running_domains;
Set(NDOID) stalled_domains;	/* waiting for domain to become available */
Set(NDOID) available_domains;
Set(NDOID) waiting_domains;

/* In spite of appearances, the following "procedures" can be
   macro-expanded in place. */

void
main()
{
  idle = true;
  
  for(;;) {
    if (idle)
      resched();
    else {
      try {
	if ( !WellFormedDomain(current_domain) ) {
	  running_domains -= current_domain;
	  resched();
	}
	
	execute_next_instr(current_domain);
      }
      catch (RESTART) {
	/* don't do anything -- just continue */
      }
    }
  }
}

void
execute_next_instr()
{
  Instr instr = fetch_instr();
  perform_instr(instr);
}

Instr
fetch_instr
{
  pc = fetch_pc(*current_domain);
  instr = decode_instr(fetch_uint32(*current_domain, pc));
  return instr;
}

void
perform_instr()
{
  /* deferred */
}

void
SetDomainFault(DomainKey dom, uint32_t code, caddr_t addr)
{
  Key faultInfo = FetchNodeSlot(dom, ProcTrapCode);
  if (faultInfo.type != KtNumber)
    raise(NOTREACHED);

  if (faultInfo.w[0] != FC_NoFault) {
    /* no double faults -- simplifies copyout */
    faultInfo.w[0] = code;
    faultInfo.w[1] = addr;
  }
  SetNodeSlot(dom, ProcTrapCode, faultInfo);
}


void
DeliverSegmentFault(DomainKey dom, TranslateResult t)
{
  if (t.keptSeg != DK(0)) {
    /* We know that t.keptSeg names a red segment because that is all
       we put there.  For various reasons red segments are not a
       distinct key type.  Arguably they ought to be, and for the
       semantics they probably could be made so. */

    Key keeper = GetSegmentKeeper(t.keptSeg);

    if (keeper.type == KtStart && WellFormedDomain(keeper)) {
      SegFaultInfo sfi;
      sfi.keptSegBlss = GetSegBlss(t.keptSeg);
      sfi.keptSegInitialSlots = InitialSlotsOf(t.keptSeg);
      sfi.faultCode = t.faultCode;
      sfi.offset = t.keptSegOffset;

      InvokeKeeper(dom, keeper, &sfi, sizeof(sfi));

      raise(NOTREACHED);
    }
  }

  Key keeper = GetDomainKeeper(dom);
  Registers registers = GetFixRegs(); /* architecture dependent */
  InvokeKeeper(dom, keeper, &registers, sizeof(Registers));

  raise(NOTREACHED);
}

bool
WellFormedDomain(DomainKey dom)
{
  /* Key regs slot must hold node key */
  Key key = FetchNodeSlot(dom, DomKeyRegs);
  if (key.type != KtNode || key.IsReadOnly())
    return false;
  
  /* Domain root slots corresponding to registers must hold number
     keys. WHICH SLOTS IS ARCHITECTURE DEPENDENT.  Following true for
     all 32 bit architectures assuming 16 slots per node. */

  for (int k = 5; k <= 11; k++) {
    Key key = FetchNodeSlot(dom, k);
    if (key.type != KtNode || key.IsReadOnly())
      return false;
  }

  /* Key reg 0 must be DK(0) -- someone with a node key might have
     modified it. */
  Key key = FetchNodeSlot(key, 0);
  if (key != DK(0))
    return false;

  /* All general registers node slots must hold number keys */
  Key genRegs = FetchNodeSlot(dom, ProcGenRegs);
  for (i = 0; i < NodeSlots; i++) {
    Key key = FetchNodeSlot(ProcGenRegs, i);
    if (key.type != KtNumber)
      return false;
  }
}

uint8_t
fetch_uint8(DomainKey dom, caddr_t addr)
{
  /* possible outcomes -- a word successfully read or a boatload of
     changes to the old current domain's state and a change to the
     current_domain pointer followed by a raise(RESTART). */

  Key segKey = FetchNodeSlot(dom, ProcAddrSpace);

  TranslateResult t = translate_offset(segKey, NumberFrom(addr), false);

  if (t.faultCode != FC_NoFault) {
    SetDomainFault(dom, t.faultCode, addr);
    DeliverSegmentFault(dom, t);
  }

  offset = addr % PageSize;
  return extract_uint8(pagespace[pgoid],offset);
}

void
store_uint8(DomainKey dom, caddr_t addr, uint8_t b, bool prompt)
{
  /* possible outcomes -- a word successfully read or a boatload of
     changes to the old current domain's state and a change to the
     current_domain pointer followed by a raise(RESTART).
     possible outcomes -- a word successfully read or a boatload of
     changes to the old current domain's state and a change to the
     current_domain pointer followed by a raise(RESTART).
     */

  Key segKey = FetchNodeSlot(dom, ProcAddrSpace);

  TranslateResult t = translate_offset(segKey, NumberFrom(addr), false);

  if (t.faultCode != FC_NoFault) {
    if (prompt) {
      SetDomainFault(dom, FC_ParmLack, addr);
      return;			/* drop on floor and proceed */
    }
    else {
      SetDomainFault(dom, t.faultCode, addr);
      DeliverSegmentFault(dom, t);
      raise(NOTREACHED);
    }
  }

  offset = addr % PageSize;
  set_uint8(pagespace[pgoid], offset, b);
}

Word
get_field(Number value, uint32_t shift, uint32_t mask)
{
  return (value >> shift) & mask;
}

void
set_field(Number& value, uint32_t shift, uint32_t mask, uint32_t w)
{
  value &= ~(mask << shift);
  value |= (Number(w) << shift);
}

bool
IsForbiddenNode(Key key)
{
  /* Should check if the key names a constituent
     of the currently running domain -- I need to define a set for
     that. */
  return false;
}

bool
SegmodeType(Key key)
{
  if (key.type == KtNode
      || key.type == KtSense
      || key.type == KtSegment
      || key.type == KtPage)
    return true;
  else
    return false;
}

bool
IsReadOnlySegment(Key key)
{
  switch (key.type) {
  case KtSense:
    return true;
  case KtNode:
  case KtSegment:    
  case KtPage:
    if (key.keyData & SEGMODE_RO)
      return true;
    return false;
  default:
    raise(NOTREACHED);
  }
}

bool
IsCallableSegment(Key key)
{
  switch (key.type) {
  case KtSense:
    return false;
  case KtNode:
  case KtSegment:    
  case KtPage:
    if (key.keyData & SEGMODE_NC)
      return false
    return true;
  default:
    raise(NOTREACHED);
  }
}

uint32_t
GetSegBlss(Key key)
{
  switch (key.type) {
  case KtPage:
    return PageBlss;
    
  case KtSense:
  case KtNode:
  case KtSegment:    
    segBlss = keyData & SEGMODE_BLSS_MASK;

    if (segBlss != RedSegBlss)
      return segBlss;

    /* It's a red segmode key.  Get BLSS from format key instead */
    Key fmt = FetchNodeSlot(key, RedSegFormat);
    return get_field(fmt.n, format_blss, MASK16);

  default:
    raise(NOTREACHED);
  }
}

uint32_t
InitialSlotsOf(Key key)
{
  switch (key.type) {
  case KtSense:
  case KtNode:
  case KtSegment:    
    segBlss = keyData & SEGMODE_BLSS_MASK;

    if (segBlss != RedSegBlss)
      return NodeSlots;

    /* It's a red segmode key.  Get BLSS from format key instead */
    Key fmt = FetchNodeSlot(key, RedSegFormat);
    return get_field(fmt.n, 0, MASK4);

  default:
    raise(NOTREACHED);
  }
}

bool
OffsetFitsInSegment(Number offset, uint32_t segBlss)
{
  Number segMultiple = (offset >> (segBlss + 1));
  if (segMultiple == 0)
    return true;

  return false;
}
     
uint32_t slot
GetSlotFromOffset(Number offset, uint32_t segBlss)
{
  Number segMultiple = (offset >> (segBlss + 1));
  if (segMultiple > NodeSlots)
    raise(NOTREACHED);

  return get_field(segMultiple, 0, 4);
}

Number TruncateOffsetTo(Number offset, uint32_t blss)
{
  Number blssSize = 1 << ((segBlss + 2) * 4);
  return offset % blssSize;
}

bool
IsRedSegment(Key key)
{
  switch (key.type) {
  case KtSense:
  case KtNode:
  case KtSegment:    
    segBlss = GetSegBlss(key);
    if (segBlss == RedSegBlss)
      return true;

    return false;
  default:
    return false;
  }
}

bool
HasSegKeeper(Key key)
{
  if (!IsRedSegment(key))
    raise(NOTREACHED);

  Key fmt = FetchNodeSlot(key, RedSegFormat);
  if ( get_field(fmt.n, format_keeper, MASK4) != RedSegFormat )
    return true;
  return false
}

Key
GetSegmentKeeper(Key key)
{
  if (!HasSegKeeper(key))
    raise(NOTREACHED);

  Key fmt = FetchNodeSlot(key, RedSegFormat);
  uint32_t slot = get_field(fmt.n, format_keeper, MASK4);

  return FetchNodeSlot(key, RedSegFormat);
}

bool
KeeperGetsNodeKey(key)
{
  if (!HasSegKeeper(key))
    raise(NOTREACHED);

  Key fmt = FetchNodeSlot(key, RedSegFormat);
  uint32_t convention = get_field(fmt.n, format_inv_convention, MASK4);

  if (convention == 0)
    return true;
  return false;
}

Key
GetDomainKeeper(DomainKey key)
{
  return FetchNodeSlot(key, ProcKeeper);
}

bool
HasBgSegment(Key key)
{
  if (!IsRedSegment(key))
    raise(NOTREACHED);

  Key fmt = FetchNodeSlot(key, RedSegFormat);
  if ( get_field(fmt.n, format_bg_segment, MASK4) != RedSegFormat )
    return true;
  return false
}

bool
IsBackgroundWindow(Key key)
{
  if (key.type == KtNumber && get_field(key.n, 0, MASK4) == 3)
    return true;
  else
    return false;
}

bool
IsLocalWindow(Key key)
{
  if (key.type == KtNumber && get_field(key.n, 0, MASK4) == 2)
    return true;
  else
    return false;
}

bool
IsInvalidRedSegment(Key key)
{
  if (!IsRedSegment)
    return false;

  /* Check the format key for validity.  See 'Segment' page in object
     reference for explanation */

  Key fmt = FetchNodeSlot(key, RedSegFormat);
  if (fmt.type != KtNumber)
    return true;

  /* upper words must be 0 */
  if ( fmt.w[2] || fmt.w[1] )
    return true;
    
  /* invocation convention must be zero or 1 */
  if ( get_field(fmt.n, format_bg_seg, MASK4) > 1 )
    return true;

  /* upper bits must be 0 */
  if ( get_field(fmt.n, format_blss, MASK16) > 0 )
    return true;
      
  return false;
}

/* One of the two ugliest algorithms in the kernel, the other being
     invocation. */
TranslateResult
translate_offset(Key segKey, Number offset, bool write)
{
  TranslateResult t;

  t.keptSeg = DK(0);
  t.keptSegOffset = 0;
  t.bgSeg = DK(0);
  t.faultCode = FC_NoFault;
  t.page = DK(0);

  for (int i = 0; i < TranslateDepthLimit; i++) {
    /* Check all the reasons why we might not be able to make
       progress */ 
  
    /* assertion: segKey type unknown */

    if ( segKey == DK(0) ) {	/* null subsegment */
      t.faultCode = FC_SegInvalid;
      return t;
    }

    /* Y'all will probably want to expand this into a big typecase. */
    if ( !SegmodeType(segKey) ) {
      f.faultCode = FC_SegMalformed;
      return t;
    }

    /* assertion: segKey is now node, segment, sense, or page key */

    if ( IsForbiddenNode(segKey) ) {
      f.faultCode = FC_SegMalformed;
      return t;
    }

    if ( IsInvalidRedSegment(segKey) ) {
      f.faultCode = FC_SegMalformed;
      return t;
    }

    if ( IsReadOnlySegment(segKey) && write ) {
      t.faultCode = FC_SegAccess;
      return t;
    }

    uint32_t segBlss = GetSegBlss(segKey);
    
    if ( segBlss > MAX_BLSS ) {
      t.faultCode = FC_SegMalformed;
      return t;
    }
    
    if ( segBlss < PageBlss ) {
      t.faultCode = FC_SegMalformed;
      return t;
    }
    
    if ( OffsetFitsInSegment(offset, segBlss) ) {
      t.faultCode = FC_SegInvalid;
      return t;
    }

    if ( key.type == KtPage )
      return t;

    /* assertion: key is now node, segment, or sense key */

    uint32_t validSlots = InitialSlotsOf(segKey);
    uint32_t slot = GetSlotFromOffset(offset, segBlss);
    uint32_t slotBlss = segBlss - 1;
    Number subSegOffset = TruncateOffsetTo(offset, slotBlss);

    if (slot > validSlots) {
      t.faultCode = FC_SegInvalid;
      return t;
    }

    /* Before we traverse, update the background segment and kept
       segment if segKey names a red segment: */

    if ( HasSegKeeper(segKey) ) {
      if ( IsCallableSegment(segKey) )
	t.keptSeg = DK(0);
      else
	t.keptSeg = segKey;
      t.keptSegOffset = offset;
      t.bgSeg = DK(0);		/* kept seg overrides BG seg */
    }

    if ( HasBgSegment(segKey) )
      t.bgSeg = segKey;		/* BG seg does NOT override keeper */

    /* We are about to traverse into the subsegment. */
    Key subSeg = FetchNodeSlot(segKey, slot);

    /* The tricky case is if the subsegment proves to be a local or
       background window key, in which case we have to shift into
       the background segment.

       Local window keys are not implemented in the current
       semantics.  They are simply another type of number key. */

    if ( IsBackgroundWindow(subSeg) ) {

      Number windowOffset = ExtractBackgroundOffset(segKey);

      /* window offset must name a subsegment whose alignment is
	 appropriate for the expected alignment of this slot, which
	 means that the appropriate low order bits of the offset
	 must be zero. */

      Number sizeModulo =  1 << ((slotBlss + 2) * 4);

      if (windowOffset % sizeModulo != 0) {
	t.faultCode = FC_SegMalformed;
	return t;
      }

      /* Not clear in principle if this should be tested here or
	 tested when we pick the key up. I put it here because this
	 way you don't get bit if you don't traverse a window.  The
	 other choice would be more paranoid and probably safer. */
	
      if ( !SegmodeType(subSeg) ) {
	t.faultCode = FC_SegMalformed;
	return t;
      }

      t.bgSeg = DK(0);
      segKey = GetBackgroundSegmentKey(t.bgSeg);
      offset = windowOffset + subSegOffset;

      /* We now continue down the background segment. You might wish
	 to think of this as having captured a continuation on the
	 seg walk there. */
    }
#if 0
    else if ( IsLocalWindow(subSeg) ) {
      /* not yet implemented */
    }
#endif
    else if ( subSeg == DK(0) ) {
      segKey = subSeg;
      offset = subSegOffset;
    }
    else {
      segKey = subSeg;
      offset = subSegOffset;
    }
  }

  t.faultCode = FC_SegDepth;
  return t;
}

bool
IsGateKey(Key key)
{
  switch (key.type) {
  case KtResume:
  case KtRestart:
  case KtStart:
    return true;
  default:
    return false;
  }
}


/* Invoke -- copy in the from args, do the operation, copy out the
     result */
struct IpcBlock {
  Byte data[MsgLimit];
  Key  key[IpcKeys];
  Word code;
  Word len;
};

InvokeKey(DomainKey dom)
{
  IpcBlock entry = GetEntryBlock(dom);
  Key key = GetInvokedKey(dom);
  InvType inv = GetInvocationType(dom);
  
  IpcBlock result;

  /* Initialize result block: */
  result.code = RC_OK;		/* until proven otherwise */
  result.len = MsgLimit;		/* until restricted */
  result.key[0] = DK(0);
  result.key[1] = DK(0);
  result.key[2] = DK(0);
  result.key[3] = DK(0);

#if 0
  Key invokee = dom;		/* until proven otherwise -- e.g. gate
				   key, return to kernel key */
#endif

  /* Invoking a kept red segment key is a special case: */
  if ( key.type == KtSegment &&
       IsRedSegment(key) &&
       IsCallableSegment(Key key) ) {

    if ( KeeperGetsNodeKey(key) )
      entry.key[2] = NodeKey(key.oid, key.keyData);

    key = GetSegmentKeeper(key);
  }

  /* Invoking a malformed domain behaves like invoking DK(0): */
  if ( IsGateKey(key) ) {
    if ( !WellFormedDomain(key.oid) )
      key = DK(0);
    else if (type == KtStart && !(key.oid in available_domains)) {
      running_domains -= dom.oid;
      stalled_domains += dom.oid;
      raise(RESTART);
    }
    else if (type == KtStart && !(key.oid in waiting_domains))
      raise(NOTREACHED);
  }

  NDOID invokee = dom.oid;

  /* If resume key, might be destroyed by handler, so need to squirrel
     this away: */
  if (IsGateKey(key))
    invokee = key.oid;
  
  if (inv == IT_Call)
    entry.key[3] = ResumeKey(dom.oid);
  
  DoKeyOperation(key, entry, &result);

  switch(inv) {
  case IT_Call:
    {
      if (wasGateKey) {
	running_domains -= dom.oid;
	waiting_domains += dom.oid;
      }
      break;
    }
  case IT_Return:
    {
      running_domains -= dom.oid;
      available_domains += dom.oid;
      running_domains += stalled_domains;
      stalled_domains = {};
      break;
    }
  }

  /* return, fork to kernel key don't do much */
  if (IsGateKey(key) || inv == IT_Call)
    DeliverResult(key, result, invokee);

  if (IsGateKey(key) && WellFormedDomain(invokee)) {
    running_domains += invokee;
    available_domains -= invokee;
  }

  if (IsResumeKey(key))
    ConsumeResumeKeys(key);
}

void
InvokeKeeper(Key dom, Key keeper, Byte *buf, uint32_t len)
{
  if ((keeper.type != KtStart) || !WellFormedDomain(keeper)) {
    /* domain has bad keeper.  thread of control dies. */
    running_domains -= dom.oid;
    raise(RESTART);
  }

  if (!(keeper.oid in available_domains)) {
    running_domains -= dom.oid;
    stalled_domains += dom.oid;
    raise(RESTART);
  }

  /* We now know we will proceed. */
  IpcBlock result;

  for (int i = 0; i < len; i++)
    result.data[i] = buf[i];

  result.code = 0;
  result.len = len;
  result.key[0] = DK(0);
  result.key[1] = DK(0);
  result.key[2] = DK(0);
  result.key[3] = RestartKey(dom.oid);

  running_domains -= dom.oid;
  waiting_domains += dom.oid;

  DeliverResult(key, result, invokee);

  running_domains += invokee;
  available_domains -= invokee;

  if (IsResumeKey(key))
    ConsumeResumeKeys(key);
}

IpcBlock
GetEntryBlock(NDOID oid)
{
  IpcBlock iblock;

  Key domRoot = DomainKey(oid);

  Key entryBlockKey = FetchNodeSlot(domRoot, ProcSndBlock);
  uint32_t entryLen = get_field(entryBlockKey.n, 0, MASK16);
  caddr_t  entryAddr = get_field(entryBlockKey.n, 32, MASK32);
  uint32_t slot0 = get_field(entryBlockKey.n, 16, MASK4);
  uint32_t slot1 = get_field(entryBlockKey.n, 20, MASK4);
  uint32_t slot2 = get_field(entryBlockKey.n, 24, MASK4);
  uint32_t slot3 = get_field(entryBlockKey.n, 28, MASK4);
  
  Key invCtl = FetchNodeSlot(domRoot, ProcOrderAndInvoke);
  uint32_t orderCode = get_field(entryBlockKey.n, 0, MASK32);
  uint32_t invCtlBlock = get_field(entryBlockKey.n, 32, MASK32);
  uint32_t invokedSlot = get_field(entryBlockKey.n, 64, MASK32);

  iblock.code = orderCode;
  iblock.len = entryLen;
  for (i = 0; i < iblock.len; i++)
    Byte b = fetch_uint8(domRoot, entryAddr + i);
  
  Key keyRegs = FetchNodeSlot(domRoot, DomKeyRegs);

  iblock.key[0] = FetchNodeSlot(keyRegs, slot0);
  iblock.key[1] = FetchNodeSlot(keyRegs, slot1);
  iblock.key[2] = FetchNodeSlot(keyRegs, slot2);
  iblock.key[3] = FetchNodeSlot(keyRegs, slot3);

  return iblock;
}

Key
GetInvokedKey(DomainKey dom)
{
  Key invCtl = FetchNodeSlot(domRoot, ProcOrderAndInvoke);
  uint32_t invokedSlot = get_field(entryBlockKey.n, 64, MASK32);

  if (invokedSlot == 0)
    return DK(0);
  
  Key keyRegs = FetchNodeSlot(domRoot, DomKeyRegs);

  return FetchNodeSlot(keyRegs, invokedSlot);
}

InvType
GetInvocationType(DomainKey dom)
{
  Key invCtl = FetchNodeSlot(domRoot, ProcOrderAndInvoke);
  uint32_t ty = get_field(entryBlockKey.n, 56, MASK4);

  switch (ty) {
  case IT_Return:
    return IT_Return;
  case IT_Call:
    return IT_Call;
  case IT_Fork:
    return IT_Fork;
  default:
    SetDomainFault(dom, FC_BadEntryBlock, 0);
    raise(RETRY);
  }
}

void
DeliverResult(Key invokedKey, IpcBlock result, NDOID oid)
{
  if (invokedKey.isRestart)
    return;
  
  Key domRoot = DomainKey(oid);

  Key exitBlockKey = FetchNodeSlot(domRoot, ProcRcvBlock);
  uint32_t exitLen = get_field(exitBlockKey.n, 0, MASK16);
  caddr_t  exitAddr = get_field(exitBlockKey.n, 32, MASK32);
  uint32_t slot0 = get_field(exitBlockKey.n, 16, MASK4);
  uint32_t slot1 = get_field(exitBlockKey.n, 20, MASK4);
  uint32_t slot2 = get_field(exitBlockKey.n, 24, MASK4);
  uint32_t slot3 = get_field(exitBlockKey.n, 28, MASK4);

  if (result.len < exitLen)
    exitLen = result.len;
      
  set_field(exitBlockKey.n, 0, MASK16, exitLen);
  SetNodeSlot(domRoot, ProcRcvBlock, exitBlockKey);

  uint16_t keyData = IsGateKey(invokedKey) ? key.keyData : 0;
  
  Key invCtl = FetchNodeSlot(domRoot, ProcOrderAndInvoke);
  set_field(invCtl.n, 0, MASK32, result.code);
  set_field(invCtl.n, 32, MASK16, keyData);
  SetNodeSlot(domRoot, ProcOrderAndInvoke, invCtl);
  
  for (i = 0; i < exitLen; i++)
    Byte b = store_uint8(domRoot, exitAddr + i, result.data[i], prompt);
  
  Key keyRegs = FetchNodeSlot(domRoot, DomKeyRegs);

  Key exitBlockKey = FetchNodeSlot(domRoot, ProcRcvBlock);

  if (slot 0) SetNodeSlot(keyRegs, slot0, result.key[0]);
  if (slot 0) SetNodeSlot(keyRegs, slot1, result.key[1]);
  if (slot 0) SetNodeSlot(keyRegs, slot2, result.key[2]);
  if (slot 0) SetNodeSlot(keyRegs, slot3, result.key[3]);
}


void DoKeyOperation(Key key, IpcBlock entry, IpcBlock &result)
{
  switch(key.type) {
  case KtResume:
    DoGateKey(key, entry, result);
    break;
  case KtStart:
    DoGateKey(key, entry, result);
    break;
  case KtPage:
    DoPageKey(key, entry, result);
    break;
  case KtSegment:
    DoSegmentKey(key, entry, result);
    break;
  case KtNode:
    DoNodeKey(key, entry, result);
    break;
  case KtDomain:
    DoDomainKey(key, entry, result);
    break;
  case KtNumber:
    DoNumberKey(key, entry, result);
    break;
  case KtMisc:
    DoMiscKey(key, entry, result);
    break;
  }
}

void
DoGateKey(const Key& key, const IpcBlock& entry, IpcBlock& result /* OUT */)
{
  result.code = entry.code;
  result.len = entry.len;
  for (i = 0; i < 4; i++)
    result.key[i] = entry.key[i];
  for (i = 0; i < entry.len; i++)
    result.data[i] = entry.data[i];
}

void
DoPageKey(const Key& key, const IpcBlock& entry, IpcBlock& result /* OUT */)
{
  if (entry.code >= OC_Page_Write(0) &&
      entry.code < OC_Page_Write(PageSize)) {

    if (key.IsReadOnly()) {
      // Nice try!
      result.code = RC_RequestError;
      return;
    }
    
    if (entry.len > PageSize) {
      result.code = RC_RequestError;
      return;
    }

    /* Wrapping is a pain in the ass. */
    Word offset = entry.code - 65536u;
    Word count = entry.len;
      
    if (offset + count > PageSize) {
      Word cnt = PageSize - offset;
	
      for (int i = 0; i < count; i++)
	page[key.oid][i + offset] = entry.data[i + offset];
      
      count -= cnt;
      sndBuf += cnt;
      offset = 0;
    }

    for (int i = 0; i < count; i++)
      page[key.oid][i] = entry.data[i];

    return;
  }
  else if (entry.code >= OC_Page_Read(0) &&
      entry.code < OC_Page_Read(PageSize)) {

    /* Wrapping is STILL a pain in the ass. */
    Word offset = entry.code - 65536u;
    kva_t sndBuf = (kva_t) result.data;
    Word count = result.len;
      
    if (offset + count > PageSize) {
      Word cnt = PageSize - offset;
	
      for (int i = 0; i < count; i++)
	result.data[i + offset] = page[key.oid][i + offset];
      count -= cnt;
      sndBuf += cnt;
      offset = 0;
    }

    for (int i = 0; i < count; i++)
      result.data[i] = page[key.oid][i];

    return;
  }
  else switch(inv.entry.code) {
  case KT:
    result.code = (key.IsReadOnly()) ? AKT_RoPage : AKT_Page;
    return;

  case OC_Page_MakeReadOnly:	/* Make RO page key */
    result.key[0] = key;
    result.key[0].SetReadOnly();
    return;

  case OC_Page_Zero:		/* zero page */
    if (key.IsReadOnly()) {
      result.code = RC_NoAccess;
      return;
    }
    
    for (int i = 0; i < PageSize; i++)
      page[key.oid][i] = 0;

    return;
    
  case OC_Page_Clone:
    {
      /* copy content of page key in arg0 to current page */

      if (key.IsReadOnly()) {
	result.code = RC_NoAccess;
	return;
      }

      if (entry.key[0].type != KtPage) {
	result.code = RC_RequestError;
	return;
      }

      for (i = 0; i < PageSize; i++)
	page[key.oid][i] = page[entry.key[0].oid][i];
      return;
    }

  default:
    result.code = RC_UnknownRequest;
    return;
  }
}

void
DoSegmentKey(const Key& key, const IpcBlock& entry, IpcBlock& result /* OUT */)
{
}

void
DoNodeKey(const Key& key, const IpcBlock& entry, IpcBlock& result /* OUT */)
{
}

void
DoDomainKey(const Key& key, const IpcBlock& entry, IpcBlock& result /* OUT */)
{
}

void
DoNumberKey(const Key& key, const IpcBlock& entry, IpcBlock& result /* OUT */)
{
}

void
DoMiscKey(const Key& key, const IpcBlock& entry, IpcBlock& result /* OUT */)
{
}

