<html> <head>
<title>The EROS Threaded Kernel Design</title>
</head>

<body>
<p> Kernel and user threads are scheduled by a common mechanism in the
EROS system.  This note describes the structure of EROS threads and
the design differences between kernel and user threads.  In addition,
it describes the design of kernel thread queues, which differs
somewhat from previous designs.
<h1>1. Introduction</h1>
<p> The EROS kernel is fully multithreaded.  Any EROS code that blocks
for any reason has it's own thread.  We chose a multithreaded design
for several reasons:
<ul>
  <li> <em>Simplicity</em>: The kernel can implement a single common
       scheduler; it need not differentiate between kernel tasks that
       block on an event and user tasks that block on an event.  For
       example, there are several kernel tasks that initiate I/O and
       wait for I/O completion.  In the EROS design these tasks use
       the same thread queueing mechanism that is used by user
       threads.
       <p> 
  <li> <em>Performance</em>: Drivers are somewhat faster, since they
       are able to retain state on the stack.  This also tends to make
       them simpler and clearer, since they are not constantly
       rechecking conditions.
       <p> 
  <li> <em>Control</em>: The use of a common scheduler means that
       performance tradeoffs can be made between kernel activities and
       user activites.  One consequence is that kernel activities
       cannot take up more than their fair slice of system resources.
</ul>
<p> There are three disadvantages that should be acknowledged:
<ul>
  <li> <em>Size</em>: The kernel requires more memory, because each
       kernel thread requires its own dedicated stack.  It is unclear
       how much of this is made up for by code simplification.
       <p> 
  <li> <em>Latency</em>: A few drivers (typically for asynchronous
       devices) have such aggressive interrupt response requirements
       that their interrupt handler must now do more work.
       <p> 
  <li> <em>Layering</em>: Asynchronous drivers are now divided into
       three layers rather than two:
       <p> 
       <ol>
	 <li> A traditional upper layer that queues requests.
	 <li> A bottom (interrupt) layer that buffers incoming traffic
	      in the traditional way.
	 <li> A middle layer that acts on requests and demultiplexes
	      the input buffer. <em>This design is tentative.</em>
       </ol>
       <p> Synchronous device drivers also have upper and lower
       layers, but the lower layer is implemented by a thread which
       waits <em>synchronously</em> for interrupts.
</ul>
<p> The balance of this document describes how threads work and what
sorts of queueing and rendevous mechanisms are supported by the EROS
kernel.
<h1>2. Structure of a Thread</h1> 
<p> The basic structure of a thread is captured in two data
structures: a <code>Thread</code> object and the per-thread stack.
The <code>Thread</code> object 

<p> The EROS scheduler is depressingly simple, mostly because it
pushes the work of saving and restoring thread state back onto the
thread.  The principle reason for doing it this way is that not all
threads need to save and restore the same state.
<p> On invocation, the thread scheduler assumes that the invoking
thread has already saved it's state in a safe place and appended
itself to the appropriate stall queue.  The scheduler simply makes a
pass over the run queues in order of priority, and sets the highest
priority thread it can find running.
<h2>2.1 Per-Thread State</h2>
<p> As seen by the dispatcher switching logic, a thread consists of
two pieces of state:
<ul>
  <li> A <code>Thread</code> object, and
  <li> A stack.  The <code>Thread</code> object contains a pointer to
       the bottom of this stack.
</ul>
<p> As far as the dispatcher code is concerned, the only requirement
on the stack is that it end with an address space descriptor and a
resume PC. The resume PC always refers to a location in the kernel.
<ul>
  <pre>
  +-----------------+
  |     saved       |
  ~     thread      ~
  |     state       |
  +-----------------+
  |  Address Space  |
  +-----------------+
  </pre>
</ul>
<p> On most architectures, the <code>Address Space</code> field will
in the kernel will be a pointer to the mapping table associated with
this thread.  On architectures that implement hash-structured mapping,
it will be the address space ID value.  In either case, it will have a
well-defined out of band value.
<h2>2.2 Dispatching a Thread</h2>
<p> There are two possible impediments to dispatching a thread:
<ul>
  <li> The thread must have a valid (nonzero) stack.
  <li> The stack must contain a valid address space descriptor.
</ul>
<p> Assuming that these constraints are met, The thread can be
initiated.  The context switcher does so by simply branching to the
<code>Resume PC</code>.  It does <em>not</em> actually reload the
address space pointer.  For kernel threads, both constraints are
always satisfied.  User thread stacks live in the context cache, and
can therefore be impeded by one or both of the above constraints.
<p> If the user domain has been kicked out of the context cache, the
stack pointer in the <code>Thread</code> object will be invalid.  In
this event, the scheduler attempts to reload the thread into the
context cache.  If the domain's Nodes remain in memory, the reload
will succeed promptly.  Otherwise, the domain will end up asleep on an
I/O stall queue waiting for some node to be faulted in.  If the thread
is not asleep at this point, we proceed on to check the <code>address
space</code> validity.
<p> Once the context cache has been loaded, the dispatcher may find
that the root mapping table page for the thread has been reclaimed.
In this event, the address space descriptor will be invalid (typically
zero).  As before, the thread dispatcher attempts to reload the
mapping table, and the thread is now either runnable or asleep.
<p> In either case, if the the thread ends up asleep the scheduler
simply starts over with the highest priority thread.
<h2>2.3 Yielding the Processor</h2>
<p> When a thread wishes to yield the processor, what happens depends
on whether the thread is a kernel thread or a user thread.  Kernel
threads simply push all of the state they wish to save onto the stack,
creating a stack image as described above.  In saving it's state, the
thread may assume that it will be running in the kernel when it is
resumed.  On the x86, for example, this emplies that kernel threads do
not need to bother to save the segment registers, and can ignore the
address space pointer on reload.
<p> User threads have things a bit more difficult, because they are
not allowed to retain a kernel stack.  When a user thread first enters
the kernel (due to either an interrupt or a kernel invocation), the
first thing it does builds a context image on the stack.  Through
careful connivance, this image gets saved into the domain's context
cache entry.  The stack pointer is then modified to point to a kernel
stack that is used for all interrupt and kernel invocation processing.
<p> When a user thread wishes to yield the processor, it first
releases any locks that it may hold.  It then stashes a pointer <em>to
the context cache save area</em> in it's <code>Thread</code> object.
This has two effects:
<ul>
  <li> When resumed, the thread behaves as though it has just entered
       the kernel.
  <li> All state on the kernel stack is discarded.  The kernel stack
       pointer is adjusted to point back to the top of the kernel
       stack.
</ul>
<h2>3 Interrupts and Preemption</h2>
<p> A crucial piece of the scheduling design is understanding what
happens when an interrupt occurs, and under what conditions the
processor may be preempted.
<h2>3.2 Interrupts</h2> 
<p> When an interrupt occurs, the processor may be running in either
user or supervisor mode.  If it is supervisor mode, it might be
running a kernel thread or it might be servicing a kernel invocation
for a domain.  In any of these cases, the interrupt logic builds a
save area on the current supervisor stack and then switches stacks to
the kernel interrupt stack.
<p> If the processor was running in user mode, the supervisor stack
pointer pointed to the domain's context cache entry.  The interrupt
routine has saved the thread's state directly into the context cache.
Since it is not already running on the interrupt stack, it switches to
the interrupt stack.  Exactly the same thing happens when a domain
invokes a key (except that the resume PC is different).
<p> If the processor was running a kernel thread, the saved state goes
directly onto the thread stack.  The context image will end up at the
foot of the thread stack, which is where it belongs.  Threads do not
require an explicit context cache entry (and should not have one!).
<p> On some machines, including the x86, it is permissable for
interrupts to nest, and/or to interrupt the servicing of a kernel
invocation.  In this event, we are already running off the kernel
interrupt stack when the interrupt occurs.  We will build a context
image on this stack, but no stack switch will occur (we are already on
the right stack).
<h2>3.2 Preemption</h2> 
<p> The EROS scheduler is quasi-preemptive.  User threads are
preempted by the kernel when their quanta runs out, but kernel threads
are not.  This both an implementation artifact and an essential part
of the design.
<p> When a kernel thread's quanta expires, there is no way to know
what the thread was doing when it was interrupted, and whether it is
in a reasonable state to be rescheduled.  While critical regions are a
theoretically satisfactory way of handling this, kernel logic spends
more time in critical regions than otherwise.  Rather than spend a lot
of time entering and leaving critical regions, it makes more sense to
design kernel threads to yield voluntarily.
<p> When the scheduling timer goes off, the kernel sets a reschedule
semaphore indicating that a reschedule needs to be done, <em>but it
does not actually reschedule the processor</em>.  Kernel threads that
run for any length of time are expected to periodically check the
reschedule semaphore and voluntarily yield the processor.
<p> Preemption of user-mode threads is an artifact of the way the
interrupt mechanisms works.  Conceptually, a user-mode interrupt
occurs in two phases: the thread is first transited into kernel mode
and then interrupted from kernel mode.  When a running thread attempts
to return from supervisor mode, it checks the rescheduling semaphore
to see if a reschedule is required.  If so, it places itself to sleep
on the end of the appropriate priority queue and invokes the
<code>yield()</code> routine to "voluntarily" yield the processor.
<h1>4 The Context Cache</h1>
<p> Thus far, we have described the thread dispatcher and the
dispatcher's view of the save area, but we have not gone into much in
the way of detail about the context cache itself.
<p> The context cache is a table of domain context entries.  Context
cache entries are used only for domains, and therefore only for user
threads. Each cache entry consists of a context image plus some
overhead information:
<ul>
  <pre>
  +-------------------+
  |       saved       |
  ~       context     ~
  |       image       |
  +-------------------+
  | Thread *curThread |
  +-------------------+
  | Node  *domainRoot |
  +-------------------+
  | Node *generalKeys |
  +-------------------+
  | Node *generalRegs |
  +-------------------+
  | HalfWord runState |
  +-------------------+
  | HalfWord  flags   |
  +-------------------+
  </pre>
</ul>
<p> The <code>curThread</code> pointer points to the thread that is
currently associated with this context, if any (threads migrate from
one context to another).  A given context has at most one associated
thread.
<p> The <code>runState</code> field is used to indicate the state of
the domain associated with this context cache entry.  A domain can be
in the <strong>running</strong>, <strong>kernel</strong>,
<strong>waiting</strong>, or <strong>available</strong> states.  While
the run state of a kernel is available from the domain itself, it is
useful to cache it in the context cache to improve the performance of
message passing.
<p> The <code>flags</code> field is used to indicate some unusual
conditions that may pertain to the thread.  In practice, the only use
of the flags word in current implementations is to indicate that the
current hardware floating point registers contain state associated
with this particular context.
<p> The remaining fields are used to facilitate loading and flushing
the context cache.  Context cache entries can be loaded and flushed
whenever the associated thread is not actively running.
<h2>4.1 Loading and Flushing Contexts</h2>
<p> When a thread is about to run, it may discover that it has no
saved context image (not possible for kernel threads).  In this event,
it calls <code>Context::load()</code>, passing it a key to the domain
that is to be loaded into the context cache.  If a context is loaded,
the <code>curThread</code> field is set to point to the running
thread, and the thread's saved image pointer is set to point to the
saved context image in the context cache entry.
<p> Implicit in loading a context cache entry is that some slot in the
context cache must be found and it's current contents flushed.  As
long as the cache entry is not associated with a currently running
thread, it is a candidate for replacement.
<p> When a context entry is flushed, several things happen:
<ol>
  <li> If there is an associated thread (<code>curThread</code>
       nonzero), the saved image pointer in the thread is zeroed.
       This will force the context to be reloaded if the thread is set
       in motion.  Simultaneously, the <code>curThread</code> field is
       zeroed.
       <p>
  <li> If the cache entry is currently in use (<code>domainRoot</code>
       nonzero), the state in the context cache entry is copied back
       into it's associated domain, including (if necessary) the
       floating point state.  The domain root's context cache entry
       pointer is zeroed, indicating that the domain is no longer
       cached.
       <p> 
  <li> The cache entry is now available to be loaded with whatever
       domain the new thread will be running.
</ol>
<h1>5 Creating a Kernel Thread</h1>
<p> Creating a kernel thread is fairly simple.  The driver
implementation file simply declares a <code>Thread</code> object and
allocates enough words of storage to act as a per-thread stack.  The
stack must be large enough to hold all of the local variables of the
kernel thread's procedures plus the save area image for the kernel
thread.
<h1>6 The X86 Implementation</h1>
<p> To make some of this a bit clearer, it may be worthwhile to
provide some further detail about the x86 implementation.
<h2>6.1 The X86 Stack Image</h2> 
<p> Here is the stack image that is created when an interrupt occurs
on the 386.  If the interrupt occurred while running user code, this
has the effect of creating the user save area:
<ul>
  <pre>
  cr3    <- top
  edi
  esi
  ebp
  cr2  if page fault, else unused
  ebx
  edx
  ecx
  eax
  trap number/interrupt number
  error code (zero if none)
  eip
  cs
  eflags
  esp    if from user mode, else unused
  ss     if from user mode, else unused
  es     if from user mode, else unused
  ds     if from user mode, else unused
  fs     if from user mode, else unused
  gs     if from user mode, else unused
  </pre>
</ul>
<p> Note that some state is not saved if we interrupted kernel-mode
code.  Kernel threads on the x86 are expected not to alter the segment
register values.
<h2>6.2 Call Gate</h2>
<p> When an X86 domain invokes a key, it performs a call gate
instruction.  When first entering the kernel from a call gate, the
stack looks like:
<ul>
  <pre>
  eip    <-top
  cs
  esp
  ss
  </pre>
</ul>
<p> Note that the EFLAGS register has not been saved.  We will need to
adjust the stack layout to fix this, but doing so is fairly
straightforward.  The kernel code at the entry point adjusts the stack
to look like the one shown above.
<h2>6.3 Interrupting a Call Gate</h2>
<p> Generally speaking, the layout above works just fine.  There is
one case where it gets us into a bit of trouble, which is when an
interrupt happens while taking a system call.
<p> The first instruction executed in the call gate path is the
<code>CLI</code> instruction, which disables interrupts.  Once we get
past the CLI instruction, all is well.  It is barely possible,
however, to receive an interrupt <em>before</em> the <code>CLI</code>
instruction has been executed.  At this point we have a problem.  We
haven't switched to the interrupt stack yet, and we don't have enough
room to save the interrupt context where we are.  Following this
interrupt, the stack looks like:
<ul>
  <pre>
  trap number/interrupt number
  error code (zero if none)
  eip
  cs
  eflags
  --- below here is stack prior to interrupt --- 
  eip
  cs
  esp
  ss
  </pre>
</ul>
<p> The worst problem is that we are approaching the bottom of the
context cache.  We have 7 words left before we overflow.
There are two reasonable solutions to adopt:
<ol>
  <li> Assume that this was a hardware interrupt, and simply return
       with interrupts disabled.  If we do this, we will retake the
       interrupt later.
       <p>
  <li> Attempt to recover by completing the save area construction by
       hand. 
</ol>
<p> In the EROS kernel, we adopted the first option.  It's a
reasonable bet for several reasons:
<ul>
  <li> We know that it wasn't a fault.  The instruction we were
       executing was <code>CLI</code>, and by virtue of the kernel
       design there is no fault that can be taken executing this
       instruction in this place.
       <p>
  <li> We know that it wasn't a kernel thread, because kernel threads
       to not perform call gates.
       <p>
  <li> We know that we haven't altered the <code>PIC</code> state,
       which means that the interrupt condition will still be asserted
       when we re-enable interrupts later.
</ul>
<p> Fortunately, this case is fairly easy to recognize.  If we took
this interrupt, the saved PC will be the PC of the <code>CLI</code>cli
instruction in the call gate entry point.
<p> At the moment, the context switch code doesn't deal with
this case at all, and I'm seriously tempted not to bother.
</body>
</html>
