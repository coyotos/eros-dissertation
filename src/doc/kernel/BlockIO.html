<html>
<head>
<title>The EROS Duplexed I/O Subsystem</title>
</head>

<body>
This document provides a kernel developers perspective on the EROS
I/O subsystem.  It describes
<ul>
  <li> the motivation for duplexed I/O,
  <li> the design of the EROS duplexing mechanism
  <li> how I/O requests are structured,
  <li> what device I/O request queues look like and how they behave.
  <li> how raw I/O (as opposed to object I/O) is handled.
</ul>
<p> While this document uses the term <strong>duplexing</strong>
throughout, there is no intrinsic limit on the number of duplicates
that can be supported under EROS.
<h1>1. Motivation</h1>
<p> The basic motivation for duplexed I/O is reliability.  From the
perspective of the operating system, magnetic storage devices have two
failure modes:
<ul>
  <li> Media failure, in which one or more sectors become unreadable, and
       <p> 
  <li> Device failure, in which an entire device ceases to be
       accessible.  A sufficient number of media failures constitutes
       a device failure.
</ul>
<p> While it is useful to make finer distinctions for diagnostic
purposes, the operating system itself is really concerned only with
the two cases identified above.  In practice, media failures are more
common and therefore more important.
<p> Duplexing provides a mechanism to deal with both sorts of
failures.  The DOS VFAT file system, for example, keeps two copies of
the FAT table, because loss of a sector in the FAT means loss of all
of the corresponding files.  This approach is useful for critical data
even on single disk systems.
<p> While duplexing on the same device solves the problem of media
failures, device failures require that the data be duplexed on
different disk devices.  EROS does not impose a policy in this regard,
but for mission critical environments we encourage the use of
cross-device duplexing.
<p> It should be noted that duplexing is not the only solution to
reliability problems.  For environments with serious reliabiliry
requirements, <strong>RAID</strong> technologies may provide a better
solution.
<h2>1.1 Methods of Duplication</h2>
<p> There are essentially two conventional implementations of
duplexing:
<ul>
  <li> <em>Mirroring</em>. The idea of mirroring is fairly simple: for
       every disk drive you have, buy a second one that is identical
       to the first and write all your information to both drives
       simultaneously.  If one fails, you can recover from the other.
       <p> Depending on the implementation, mirroring can be performed
       by either the disk subsystem or the operating system.
       <p>
  <li> <em>Duplexing</em>.  Duplexing is like mirroring, but operates
       at the level of logical volumes (IBM AIX) or file systems (most
       others) instead of physical drives.
       <p> Duplexing is almost always performed by the operating
       system.
</ul>
<p> While mirroring is simpler, it suffers from three basic problems:
<ol>
  <li> Both drives must be the same size.  This makes adapting to
       changing drive technologies more costly.
       <p> 
  <li> There is no selectivity; everything on a mirrored drive is
       mirrored. Not all data is important enough to be worth
       mirroring.
       <p> 
  <li> Because it works at the level of devices, mirroring is
       difficult to distribute.
       <p> 
</ol>
<p> More or less all of these problems are addressed by duplexing.
<h2>1.2 The Classical Implementation</h2>
<p> While several implementations of duplexing have been built, they
all appear to be built on the same model: virtual volumes
(equivalently: virtual file systems).  The traditional implementation
is to take two physical volumes and hide them behind a single logical
volume.  User requests are made to the logical volume, which simply
reflects these requests to the underlying physical volumes:
<center>
  <img src="TradDuplex.gif">
</center>
<p> The operations on the replicates can be done sequentially or in
parallel. Some care must be taken to make sure that data can be
recovered if the system fails in the midddle of a write.
<p> The conventional duplexing design evolved out of the existing I/O
architectures, which for the most part assumed that an I/O operation
goes to exactly one device.  Given such an architecture as a starting
point, the notion of sticking a virtual volume on top that makes calls
on individual devices makes a certain amount of sense.  The mechanism
nests gracefully, which is good, but it suffers from some flaws.
<p> Since the design inherits the "one operation, one device" model
from it's predecessors, no clear separation is maintained between the
logical I/O operation that was intended by the user and the physical
I/O operation that is being performed in response on a given
replicate.  This has three significant consequences:
<ul>
  <li> The distinction between device errors, which should be
       transparently handled by the duplexing logic, and user
       errors, is obscured by the data structures.
       <p> 
  <li> Additional kernel state is needed to keep track of the
       relationships between I/O requests.
       <p> 
  <li> Coordination is difficult.  Read operations, for example, would
       like to proceed on the "earliest read wins" model, but should
       not allocate multiple in-core page frames to do so.
       <p>
</ul>
<p> The first two problems are fairly simple to deal with - most I/O
structures provide for a finalization procedure and an argument to be
passed to that procedure.  This mechanism can be adapted to dealing
with duplexing.  The last problem is rather more serious, and suggests
a bit of a change to the design.
<h1>2. The EROS Duplexing Design</h1>
<center>
  <img src="ErosDuplex.gif">
</center>
<h1>2.2. Free List Management</h1>
<h3>The Cost of Eager Allocation </h3> 
<p> Free list management is surprisingly important in high performance.
<p> In most operating systems, allocatable core memory can be viewed
as falling into one of three categories:
<ul>
  <li> A cache of disk storage, either from files or the paging area.
       Clean pages in this area can be discarded at need and reread
       from backing store.
  <li> Reconstructable kernel state, such as page tables.
  <li> State whose expected lifespan is very short, including
       temporary copies of objects that were involved in I/O and
       therefore could not be modified.
</ul>
<p> Most operating systems pre-reserve space for read operations.
This has the effect of tying down that space, preventing it's use for
other purposes.  As a consequence, reconstructable kernel state and
temporary state forces useful data out of memory.
<p> Memory is cheap, you say?  Perhaps, but suppose you are doing
<strong>N</strong> simultaneous page read operations on a disk with a
seek delay of <strong>T</strong> ms.  The first page will be pinned
fot time <strong>T</strong>, the second for <strong>2T</strong>, the
third for <strong>3T</strong>, etc.  In general, the total pinned
page-milliseconds is given by:
<ul>
  T * (1/2)(N)(N+1)
</ul>
<p> This function is exponential:
<center>
<table border>
  <tr>
    <th>Operations</th>
    <th>Cost (page secs)</th>
  </tr>
  <tr>
    <td>4</td>
    <td>0.080</td>
  </tr>
  <tr>
    <td>8</td>
    <td>0.288</td>
  </tr>
  <tr>
    <td>12</td>
    <td>0.624</td>
  </tr>
  <tr>
    <td>16</td>
    <td>1.088</td>
  </tr>
  <tr>
    <td>32</td>
    <td>4.224</td>
  </tr>
</table>
</center>
<p> Some observations can be made about these numbers:
<ul>
  <li> An awful lot of those pages could have been profitably used for
       temporary storage.  Half a second (12 reads) is long enough for
       many processes whose data is in core to start, run, and exit!
       <p> 
  <li> Contrary to popular opinion, cascading I/O's of this sort
       happen all the time.  When a window is closed, all of the
       windows below it are exposed, causing lots and lots of page
       reads from the swap area.
       <p> 
  <li> Many other page frames will become available while these I/O's
       are in progress.  In particular, write I/O's associated with
       page cleaning will make frames available that are completely
       unallocated.
       <p>
  <li> The fact that these pages were pinned meant that other were
       clobbered for temporary storage.  Over the course of several
       seconds, many many temporary storage allocations may be
       performed, causing the ager to be invoked, which forces useful
       pages out of memory.
</ul>
<p> In a persistent system, the entirety of main memory is effectively
used as a buffer cache, and temporary operating system storage is
borrowed from the 

The problem is that
eagerly allocated pags have a high opportunity cost. Suppose that our
machine has 32 outstanding I/O requests for paging.  Further, let's
suppose that we run both replicates in parallel.  Effectively, each
replicate is going to do 16 I/O operations, each probably involving a
seek operation.  Seeks take on the order of 8ms on current technology
disk drives.  
<p> To get a sense of the opportunity cost of eager
allocation, a useful metric is 
<p> 
In order for the
classical design to work correctly, core page frames for these
requests must be allocated as soon as the logical volume commits to
perform the I/O.  These frames will eventually be allocated in any
case, but the corresponding I/O operations

<p> This leaves the designer with one of two bad
design options:
<ul>
  <li> Limit the number of outstanding I/O requests the logical volume
       will attempt to simultaneously initiate.  This limits the
       number of core page frames that must be reserved.
       allocation of core space for inbound I/O, which increases core
       pressure.
       <p>
  <li> The distinction between device errors, which should be
       transparently handled by the duplexing logic, and user
       errors, is obscured by the data structures.
       <p> 
  <li> Additional kernel state is needed to keep track of the
       relationships between I/O requests.
</ul>

t , it reserve 32 pages for those
I/O requests to complete
<h1>The EROS Implementation</h1>

how 
</body>
</html>
