<html>
<head>
<title>EROS: The Extremely Reliable Operating System</title>
</head>

<body>
<p> This document provides a high-level overview of the EROS project,
which is part of the ENIAC 2000 research initiative at the University
of Pennsylvania.  EROS, the Extremely Reliable Operating System, is a
new distributed system designed to support a large-scale distributed
computing fabric over a medium to high speed networking fabric.  The
EROS effort builds on previous high-speed networking work in Penn's
Distributed Systems Laboratory.
<h1>The Objective</h1>
<p> We envision a community of users who perform a variety of
computational activities.  Most of these activities can be
accomplished with the computational resources available at each user's
desktop.  A few activities require centralized facilities for reasons
of cost amortization or administrative advantage (e.g. storage
management).  Other activities would be advanced by mechanisms for
ephemeral access to the idle resources available within the community
(e.g. swapping to remote memory).
<p> We wish to provide a unified computing fabric to support such a
pool of users.  We envision a computing fabric made up of hundreds or
thousands of nodes ranging from personal computers to high-end
servers.  Conceptually, we can think of these computers as organized
into clusters of varying size (from one to several hundred), where a
cluster is a mutually trusting collection of nodes.  Some clusters are
large, centralized distributed systems; others consist of single
machines that sit on individual desks within an organization.  In
modern networks, logical clustering has no necessary relationship to
physical locality.
<p> Further, we wish to provide mechanism(s) for applications within a
cluster to dynamically grow and shrink their available resources by
obtaining secure access to the resources of one or more other
clusters.  Resource sharing within a cluster is transparent.  Resource
sharing across clusters requires explicit management of trust, access,
and recovery.
<p> Finally, we wish this fabric to be reliable, recoverable, and
trusted at all levels.
<h1>Research Issues</h1>
In order to accomplish these long term goals, we must investigate
reliability, recovery, and security at three levels:
<ul>
  <li> On individual nodes, which form the substrate for the larger system;
       <p> 
  <li> Within a cluster, where we assume the trust and reliability of
       the nodes, but not of the connecting network fabric; and
       <p> 
  <li> Between clusters, where we must be prepared to deal with
       adversarial intercluster relationships.
</ul>
We will address each of these issues in turn.
<h2>Individual Nodes</h2>
<p> Since the early 1980's we have had all of the neceesary pieces to
build reliable and recoverable operating systems on individual
machines.  KeyKOS, a commercial operating system, demonstrated a
software MTBF in excess of 18 months [<a href="#Hardy85">Hardy85</a>,
<a href="#Bomberger92">Bomberger92</a>, <a
href="#Landau92">Landau92</a>].  From a practical perspective, the
only nonrecoverable failures in the KeyKOS system are due to the lack
of adequate hardware error checking in commodity memories.
<p> Regrettably, the necessary knowledge for constructing such systems
has not been collected in one place, and the KeyKOS realization is not
available for wide study.  The first phase of the EROS project, which
is well underway, has two objectives:
<ul>
  <li> To construct a single-node reliable operating system, EROS, and
       make this system available for study
       <p> 
  <li> To widely disseminate the architectural principles that support
       it to encourage further study.
</ul>
<p> EROS is a pure capability system closely derived from the KeyKOS
effort.  It extends the prior art in performance and in providing a
new solution to the confinement problem [<a
HREF="#Lampson73">Lampson73</a>].  EROS is also designed to run on
widely available, commodity platforms.
<p> EROS provides a high degree of software reliability on each node
by virtue of careful architecture.  The kernel is designed to minimize
the number of actions that might lead to ill-defined states, and to
fail promptly when such states are detected.  The system performs
periodic asynchronous checkpoints [<a href="#Landau92">Landau92</a>],
and uses the saved state to roll back when an error is detected.
Processes are persistent, and computation resumes from the previously
stored state.  This approach ensures that a bounded amount of work is
lost in the event of a failure.
<p> A key to the practical success of the EROS recovery mechanism is
that it requires less than a 200 ms interruption of user service. 200
ms is an important threshold, as above this users begin to notice
things like mouse tracking and character echo delays.  More recent
studies of user interfaces suggest that even this delay is noticable.
<h2>Reliable Clusters</h2>
<p> Extending the checkpoint/rollback model to a cluster has proven
challenging. While mechanisms for resolving naming and network
transport security issues among mutually trusting nodes are well
understood, the so-called "distributed snapshot" problem [Lampson72??]
has proven quite difficult.
<p> Simply stated, the goal of a distributed snapshot is to capture a
globally consistent image of the state of a distributed system.  Given
the ability to snapshot individual nodes, this problem can be
straightforwardly solved using global synchronization, but a globally
synchronizing solution causes very noticible delays - far above the
200 ms target imposed by human reaction time.
<p> The second phase of the EROS project is to extend the initial
kernel to implement the SNOCRASH mechanism for distributed snapshot
and recovery.  The SNOCRASH mechanism, invented at the University of
Pennsylvania provides asynchronous distributed snapshots without
requiring global synchronization.  It guarantees bounded loss of work
in a distributed system at the cost of an acceptable amount of memory
overhead.
<p> A collection of machines using the SNOCRASH mechanism act as a
unified reliable cluster.  Any subset of the nodes in the cluster may
fail without damaging the cluster as a whole, and any computation on
live nodes that is independent of the failed subset will proceed
unimpeded.
<p> Security within a cluster is maintained by administrative fiat.
Once a node has been identified as properly belonging within the
cluster (admission control), any node in the cluster may assume that
it is running the EROS trusted kernel, and therefore will not seek to
compromise the security of the cluster by forging capabilities.
<h2>Inter-Cluster Distribution</h2>
<p> Beyond the scope of a cluster, we must assume that a remote node
is not running a trusted kernel.  This raises several research
questions:
<ul>
  <li> If we wish clusters to be dynamically adjustable, how can we
       prove that a remote node is running a suitable kernel?
       <p> 
  <li> What policies must be introduced to ensure inter-cluster
       security?
       <p> 
  <li> What mechanisms may be needed to allow cluster resources to
       be "leased" to an untrusted third party on a temporary basis?
       How are we to prevent forgery in such a situation?
</ul>
The third phase of the EROS project will investigate each of these
issues, and attempt to address each in turn.
<h1>Bibliography</h1>
<dl>
  <dt> <a name="Bomberger92">[Bomberger92]</a>
  <dd> Bomberger, Alan, et al., <a
       HREF="HTTP://www.cis.upenn.edu/~KeyKOS/NanoKernel/NanoKernel.html">
       The KeyKOS NanoKernel Architecture</a>, <i> Proceedings of the
       USENIX Workshop on Micro-Kernels and Other Kernel
       Architectures</i>, USENIX Association, April 1992. pp 95-112
       <p> This paper is also available in <a
       HREF="HTTP://www.cis.upenn.edu/~KeyKOS/NanoKernel/NanoKernel.ps.Z">postscript</a> form.
       <p>
  <dt> <a name="Hardy85">[Hardy85]</a>
  <dd> Hardy, Norman, "<a
       HREF="HTTP://www.cis.upenn.edu/~KeyKOS/OSRpaper.html">The
       KeyKOS Architecture</a>", <i>Operating Systems Review</i>, v.19
       n.4, October 1985. pp 8-25
       <p> The online version is a later, slightly corrected version
       of the paper.  This paper is also available in
       <a HREF="HTTP://www.cis.upenn.edu/~KeyKOS/OSRpaper.ps.Z">postscript</a>
       form. 
       <p>
  <dt> <a name="Lampson73">[Lampson73]</a>
  <dd> Lampson, Butler,
       "<a HREF="HTTP://www.cis.upenn.edu/~KeyKOS/Confinement.html">Note on the Confinement Problem</a>",
       <i>Communications of the ACM</i>, V 16, N 10, October, 1973.
       <p>
  <dt> <a name="Landau92">[Landau92]</a>
  <dd> Landau, Charles R., "<A
       HREF="HTTP://www.cis.upenn.edu/~KeyKOS/Checkpoint.html">The
       Checkpoint Mechanism in KeyKOS</a>", <i>Proceedings of the
       Second International Workshop on Object Orientation in
       Operating Systems</i>, IEEE, September 1992. pp 86-91
       <p>

</dl>
</body>
</html>
