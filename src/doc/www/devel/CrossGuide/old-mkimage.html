<html>
<head>
<TITLE>Crafting a System Image</TITLE>
</head>
<BODY BGCOLOR="#ffeedd" text="#000000" link="#0000ee" vlink="#551a8b" alink="#ff0000"><table><tr valign=top><td width="10%">&nbsp;</td><td><div class=nocss><br class=nocss>&nbsp;<br class=nocss>&nbsp;</div>
<center>
  <H1>Crafting a System Image</H1>
  <p> The <strong>mkimage</strong>, <strong>mkvol</strong>, and
  <strong>sysgen</strong> utilites
</center>

<P> This document describes the programs used to generate an initial
bootable EROS environment.  It provides a brief description of the
cross environment and system generation tools.
<H1>1. Overview</H1>
<P> EROS is usually cross developed from a substantial programming
environment.  The original implementation of EROS (for the x86) was
coded under LINUX.  Subsequent editions will hopefully be produced in
an EROS-hosted environment, but the generated floppy image is always
viewed by the running EROS as a "raw" floppy.  There are two reasons
for this:
<UL>
  <LI> Domains placed on the initial boot volume run under some very
       unusual constraints, and generally need to be hand constructed.
  <LI> The generated system is generally a different EROS version,
       either for a foreign architecture or for a different version of
       the same architecture, so the native tools (e.g. domain
       creator) are probably not suitable for creating the components
       on the generated system.
</UL>
<P>
Creating a bootable system image entails the following steps:
<OL>
  <LI> Creating, using a set of cross compilation tools, an EROS kernel
       image and boot sector(s) for the target architecture.
  <LI> Creating, using a set of cross compilation tools, the domains
       required during the initial system load (ISL).
  <LI> Producing from these domains a single <em>eros image file</em>
       describing the relationships between the domains and all of the
       keys they hold.
       (<STRONG>mkimage</STRONG>).
  <LI> Formatting a volume to include a boot sector, a kernel image
       and suitably prepared EROS divisions, including performing any
       necessary bad sector detection and remapping
       (<STRONG>mkvol</STRONG>).
  <LI> Mapping the master eros image file onto the nodes and pages
       that are actually available on a given volume
       (<STRONG>sysgen</STRONG>).
</OL>
<p> <strong>mkimage</strong> provides the necessary mechanisms to
convert UNIX a.out files into EROS segments, set the values of domain
and node registerss, and generally wire up a family of collaborating
domains.  This note is primarily concerned with the
<strong>mkimage</strong> utility.
<p> <strong>mkvol</strong> creates an initial volume image, including
bootstrap loader, an optional kernel image, and user-specified node
and page ranges.  The node and page ranges  created by
<strong>mkvol</strong> are initially empty.
<p> The <strong>sysgen</strong> utility populates a volume created by
<strong>mkvol</strong> with nodes and pages by loading one or more
EROS image files into the volume.  The EROS image files contain
relocatable node and page keys; the <strong>sysgen</strong> utility
assigns final locations to these nodes and pages. <em>This is about to
change - the ErosImage files will no longer hold relocatable page and
node keys.  All system initialization nodes/pages will be numbered
from oid 0.</em>
<P> This document describes the EROS volume structure and boot
process, and the use of the <strong>mkimage</strong>,
<strong>mkvol</strong>, and <strong>sysgen</strong> tools.
<h1>2. Mkimage: Constructing EROS Images</h1>
<p> An EROS image file consists of sequentially numbered nodes and
pages, and is constructed using the <strong>mkimage</strong> utility.
<strong>mkimage</strong> provides the facilities to turn UNIX a.out
files into domains, and to establish the initial contents of nodes and
pages.
<p> A <strong>mkimage</strong> file consists of node and pages, 
each having object identifiers starting at 0.  Node and page object
identifiers are in independent name spaces, so there is no confusion
of numbering.
<p> Every EROS image file has at least one node, whose contents are
used by the space bank.  The first key of this node is a number key
giving the object identifier of the highest allocated (equivalently:
first available) node.  The second key similarly gives the object
identifier of the highest allocated page.  A node key to this node is
the first key in the image directory, and is inserted under the name
<code>volsize</code>.  A key to this node can be handed to the the
prime space bank, and is used to avoid reallocating the space that has
gone to nodes and pages in the boot image.
<h2>2.1. The Image Specification Language</h2>
<p> <strong>mkimage</strong> produces an eros image file based on the
instructions provided in a <em>description</em> file.
<p> Statements in the description file either import an existing eros
image into the current image, declare new objects or modify existing
objects.  In the following description, keywords are shown in bold
face text and user-supplied items in italics.  An explanation of each
statement follows the statement itself.
<dl>
  <dt> 
       <p> <strong>import</strong> <em>imagefile</em>
  <dd> 
       <p> The import command appends the contents of another eros
       image file to the current image file.  Image directory entries
       must not collide, or the operation will fail.
  <dt> 
       <p> <strong>program segtree</strong> <em>basename</em>
       <strong>=</strong> <em>unix_aout_file</em>
       <br> <strong>program segment</strong> <em>basename</em>
       <strong>=</strong> <em>unix_aout_file</em>
  <dd> 
       <p> Converts a UNIX format a.out file into an EROS segment.
       The file is interpreted as an executable image and its contents
       are copied into the segment at offsets corresponding to their
       proper virtual addresses.  Page permissions dictated by the
       executable file are honored.
       <p> Two directory entries are added to the image directory:
       <ul>
       <table>
	 <tr>
	   <td>
	     <em>basename</em>.seg
	   </td>
	   <td>
	     a key to the segment tree
	   </td>
	 </tr>
	 <tr>
	   <td>
	     <em>basename</em>.pc
	   </td>
	   <td>
	     a number key containing the program entry point address
	   </td>
	 </tr>
       </table>
       </ul>
       <p> The <strong>segtree</strong> form registers a node key to
       the segment tree.  The <strong>segment</strong> form registers
       a segment key.
  <dt> 
       <p> <strong>raw segtree</strong> <em>basename</em>
       <strong>=</strong> <em>unix_aout_file</em>
       <br> <strong>raw segment</strong> <em>basename</em>
       <strong>=</strong> <em>unix_aout_file</em>
  <dd> 
       <p> Converts a UNIX format a.out file into an EROS segment.
       The file is viewed only as an uninterpreted byte sequence, and
       fills the segment starting at offset 0.  All pages are created
       with read-write permission, and a single directory entry is
       added to the image directory:
       <ul>
       <table>
	 <tr>
	   <td>
	     <em>basename</em>.seg
	   </td>
	   <td>
	     a key to the segment tree
	   </td>
	 </tr>
       </table>
       </ul>
       <p> The <strong>segtree</strong> form registers a node key to
       the segment tree.  The <strong>segment</strong> form registers
       a segment key.
  <dt> 
       <p> <strong>zero segtree</strong> <em>basename</em>
       <strong>pages</strong> <em>N</em>
       <br> <strong>zero segment</strong> <em>basename</em>
       <strong>pages</strong> <em>N</em>
  <dd> 
       <p> Creates a segment filled with <em>N</em> zero-filled pages.
       All pages are created with read-write permission, and a single
       directory entry is added to the image directory:
       <ul>
       <table>
	 <tr>
	   <td>
	     <em>basename</em>.seg
	   </td>
	   <td>
	     a key to the segment tree
	   </td>
	 </tr>
       </table>
       </ul>
       <p> The <strong>segtree</strong> form registers a node key to
       the segment tree.  The <strong>segment</strong> form registers
       a segment key.
  <dt> 
       <p> <strong>red segment</strong> <em>basename</em>
       <strong>= blss</strong> <em>blss</em>
       <br> <strong>red segtree</strong> <em>basename</em>
       <strong>= blss</strong> <em>blss</em>
  <dd> 
       <p> Fabricates an initially empty red segment of the size
       specified by BLSS.  The background segment and keeper for the
       red segment are initially undefined.  The red segment may
       subsequently grow as the result of the addition of pages.
       <p> If a background has been defined before such changes are
       applied, the segment will be expanded to map any regions of the
       red segment that are not explicitly overridden to the
       corresponding offsets in the background segment.  Otherwise,
       these regions will remain invalid.  This means that you will
       want to specify the background segment <em>before</em> any
       pages or subsegments are added (using <strong>add page</strong>
       or <strong>add subseg</strong>), and before any locations in
       the segment are given new values.
       <p> A single directory entry is added to the image directory:
       <ul>
       <table>
	 <tr>
	   <td>
	     <em>basename</em>.seg
	   </td>
	   <td>
	     a key to the segment tree
	   </td>
	 </tr>
       </table>
       </ul>
       <p> The <strong>segtree</strong> form registers a node key to
       the segment tree.  The <strong>segment</strong> form registers
       a segment key.
  <dt> 
       <p> <strong>domain</strong> <em>basename</em>
  <dd> 
       <p> Adds a new, uninitialized domain in the EROS image file.
       Empty general keys and general registers nodes are constructed
       and their keys placed in the appropriate slots of the domain
       root.  A single directory entry is added to the image
       directory:
       <ul>
       <table>
	 <tr>
	   <td>
	     <em>basename</em>.dom
	   </td>
	   <td>
	     a key to the segment tree
	   </td>
	 </tr>
       </table>
       </ul>
       <p> The <strong>segtree</strong> form registers a node key to
       the segment tree.  The <strong>segment</strong> form registers
       a segment key.
  <dt> 
       <p> <em>domain_name</em> <strong>keeper = start</strong>
       <em>domain_name data_byte</em>
       <br> <em>domain_name</em> <strong>keeper = </strong>
       <em>key</em>
       <br> <em>segment_name</em> <strong>keeper = start</strong>
       <em>domain_name data_byte</em>
       <br> <em>segment_name</em> <strong>keeper = </strong>
       <em>key</em>
  <dd> 
       <p> Fills in the keeper slot of the domain or segment with a
       start key to the specified domain.  The start key contains the
       provided data byte.
  <dt> 
       <p> <em>domain_name</em> <strong>priority = </strong>
       <em>key</em>
  <dd> 
       <p> Sets the domain's priority slot to the specified key, which
       should be a scheduling key.
  <dt> 
       <p> <em>domain_name</em> <strong>brand = </strong>
       <em>key</em>
  <dd> 
       <p> Sets the domain's brand slot to the specified key.
  <dt> 
       <p> <em>domain_name</em> <strong>root slot</strong> <em>slot</em>
       <strong>=</strong> <em>number_key</em>
  <dd> 
       <p> Sets the specified domain root <em>slot</em> of the named
       domain to <em>number_key</em>.  Restricted to those slots
       designated for register values; others should be set using the
       appropriate commands.
  <dt> 
       <p> <em>domain_name</em> <strong>root slot</strong> <em>slot</em>
       <strong>|=</strong> <em>number_key</em>
  <dd> 
       <p> <strong>Ors</strong> the specified domain root
       <em>slot</em> of the named domain with <em>number_key</em>.
       Restricted to those slots designated for register values;
       others should be set using the appropriate commands.
  <dt> 
       <p> <em>domain_name</em> <strong>reg slot</strong> <em>slot</em>
       <strong>=</strong> <em>number_key</em>
  <dd> 
       <p> Sets the specified general registers <em>slot</em> of the
       specified domain to <em>number_key</em>.
  <dt> 
       <p> <em>domain_name</em> <strong>reg slot</strong> <em>slot</em>
       <strong>|=</strong> <em>number_key</em>
  <dd> 
       <p> <strong>Ors</strong> the specified general registers
       <em>slot</em> with the value of <em>number_key</em>, and places
       the result in <em>slot</em>.
  <dt> 
       <p> <em>domain_name</em> <strong>key slot</strong> <em>slot</em>
       <strong>=</strong> <em>key</em>
       <br> <em>domain_name</em> <strong>key slot</strong> <em>slot</em>
       <strong>=</strong> <em>name</em>
       <br> <em>domain_name</em> <strong>key slot</strong> <em>slot</em>
       <strong>= start</strong> <em>domain_name data_byte</em>
  <dd> 
       <p> Sets the specified key registers <em>slot</em> of the
       specified domain to the indicated key value.  Keep in mind that
       key slot 0 is forcibly converted to a zero number key whenever
       a domain is prepared to execute.  While it is not an error to
       do so, there is no benefit to setting this key slot.
  <dt> 
       <p> <em>segment_name</em> <strong>add page</strong> <em>offset</em>
       [<strong>ro</strong>]
       <br> <em>segtree_name</em> <strong>add page</strong> <em>offset</em>
       [<strong>ro</strong>]
  <dd> 
       <p> Adds a new page to an existing segment at the indicated
       <em>offset</em>.  The page is initially zero-filled. The
       optional <strong>ro</strong> attribute indicates that the page
       should be read-only.
       <p> It is an error to add a page where one already exists.
  <dt> 
       <p> <em>segment_name</em> <strong>add subseg</strong>
       <em>subseg</em> <strong>at</strong> <em>offset</em>
       [<strong>ro</strong>]
       <br> <em>segtree_name</em> <strong>add subseg</strong>
       <em>subseg</em> <strong>at</strong> <em>offset</em>
       [<strong>ro</strong>]
  <dd> 
       <p> Adds a new subsegment to an existing segment at the
       indicated <em>offset</em>.  At the moment, the insertion
       strategy is fairly stupid, in that it will happily insert
       subsegments at an unnecessarily high place in the tree,
       sometimes overwriting other subsegments.  This should be fixed
       (someday).  The optional <strong>ro</strong> attribute
       indicates that the inserted segment/segtree should be
       read-only.
       <p> It is an error to add a page where one already exists.
  <dt> 
       <p> <em>segment_name offset</em> <strong>= word</strong>
       <em>value</em> 
       <br> <em>segtree_name offset</em> <strong>= word</strong>
       <em>value</em> 
  <dd> 
       <p> Sets the word at the given <em>offset</em> to
       <em>value</em>.  If the page at that offset in the segment is a
       zero page, it is implicitly converted to a nonzero page.
       <p> <em>Automatic conversion is not yet implemented.</em>
  <dt> 
       <p> <em>domain_name</em> <strong>accepts keys</strong>
       <em>slot</em> <em>slot</em> <em>slot</em> <em>slot</em>
  <dd> 
       <p> Defines the key slots into which received key arguments should
       be placed.  Arguments to be ignored should be received into key
       register 0, which is hardwired to the zero number key.
       <p> Unless key acceptance is specified, all keys are accepted
       into key register zero.
  <dt> 
       <p> <em>domain_name</em> <strong>accepts string at</strong>
       <em>offset</em> [ <strong>length</strong> <em>length</em> ]
<!--        <br> <em>domain_name</em> <strong>accepts string to register</strong> -->
  <dd> 
       <p> Defines where a received string should be placed and how
       long the string may be. <em>length</em> must be less than or
       equal to one page, and defaults to the message limit (currently
       65535 bytes).
       <p> Unless string acceptance is specified, all received strings
       are discarded.
       <!--
  <dt> 
       <p> <em>domain_name</em> <strong>sends string at</strong>
       <em>offset</em> [ <strong>length</strong> <em>length</em> ]
       <br> <em>domain_name</em> <strong>sends string from register</strong>
  <dd> 
       <p> Defines where a sent string is sent from and how many bytes
       will be sent. <em>length</em> must be less than or equal to one
       page, and defaults to one page.
       <p> Unless string acceptance is specified, all received strings
       are discarded.
       <p> <em>A running domain can set this for itself -- is this
       needed?</em>
  <dt> 
       <p> <em>domain_name</em> <strong>sends keys</strong>
       <em>slot</em> <em>slot</em> <em>slot</em> <em>slot</em>
  <dd> 
       <p> Defines the key slots from which sent key arguments should
       be taken by the next invocation.  Unsent arguments should be
       sent from  key register 0, which is hardwired to
       the zero number key.
       <p> Unless key acceptance is specified, all keys are sent from
       key register zero.
       <p> <em>A running domain can set this for itself -- is this
       needed?</em>
  <dt> 
       <p> <em>domain_name</em> <strong>invokes</strong> <em>slot</em>
       <strong>with order</strong> <em>value</em>
  <dd> 
       <p> Defines the key slot and order codethat will be invoked by
       the next key invocation.
       <p> <em>A running domain can set this for itself -- is this
       needed?</em>
       -->
  <dt> 
       <p> <em>domain_name</em> <strong>pc =</strong> <em>value</em>
       <br> <em>domain_name</em> <strong>pc =</strong> <em>entry_name</em>
       <br> <em>domain_name</em> <strong>sp =</strong> <em>value</em>
  <dd> 
       <p> Defines the initial program counter and stack pointer
       values for the domain.
  <dt> 
       <p> <em>domain_name</em> <strong>space =</strong> <em>segment_name</em>
       <br> <em>domain_name</em> <strong>space =</strong> <em>segtree_name</em>
  <dd> 
       <p> Defines the address space of a domain.
  <dt> 
       <p> <em>domain_name</em> <strong>slot</strong> <em>slot</em>
       <strong>=</strong> <em>key</em>
       <br> <em>domain_name</em> <strong>slot</strong> <em>slot</em>
       <strong>=</strong> <em>name</em>
       <br> <em>domain_name</em> <strong>slot</strong> <em>slot</em>
       <strong>= start</strong> <em>domain_name data_byte</em>
  <dd> 
       <p> Sets the specified <em>slot</em> of the domain root to the
       specified key.  This directive should be used with care, as it
       can override the effect of other domain root manipulation
       directives (all of them, in fact).
  <dt> 
       <p> <strong>node</strong> <em>basename</em>
  <dd> 
       <p> Adds a new, zeroed node to the EROS image file.
       .  A single directory entry is added to the image
       directory:
       <ul>
       <table>
	 <tr>
	   <td>
	     <em>basename</em>.node
	   </td>
	   <td>
	     a key to the node
	   </td>
	 </tr>
       </table>
       </ul>
  <dt> 
       <p> <strong>semaphore</strong> <em>basename</em>
       <strong>=</strong> <em>node_name</em> <em>slot</em>
  <dd> 
       <p> Adds a semaphore key to the EROS image file which
       references the named <em>slot</em> of the node
       <em>node_name</em>.  A single directory entry is added to the
       image directory:
       <ul>
       <table>
	 <tr>
	   <td>
	     <em>basename</em>.sem
	   </td>
	   <td>
	     a key for the semaphore
	   </td>
	 </tr>
       </table>
       </ul>
  <dt> 
       <p> <strong>ipl</strong> <em>domain_name</em>
  <dd> 
       <p> Identifies the domain to run by default when the system is
       first bootstrapped.  The OID of this domain is written into the
       volume header of the boot volume, and the domain is marked
       <em>running</em>.  There can only be one IPL domain.
  <dt> 
       <p> <strong>volsize</strong> <em>name</em>
  <dd> 
       <p> Allocates a node that can be handed to the initial space
       bank.  Slots 0 and 1 of this node will be filled in by the
       <strong>sysgen</strong> utility with number keys giving the
       highest allocated (equivalently: first available) node and page
       OID's.  A key to this node is handed to the prime space bank,
       and is used for initialization when the prime space bank is
       first initiated.
</dl>
<H1>3. Mkvol: Building an EROS Volume</H1>
<P> An EROS volume is a logical disk.  On an EROS-formatted floppy,
the volume spans the entire disk.  On hard disks things are a bit more
complicated.  Typically, the system firmware is designed to allow
multiple independent operating systems to be loaded onto a single
disk.  The disk is divided into non-overlapping regions, typically
called <STRONG>partitions</STRONG>, and each operating system acts for
the most part as it's partition was a single disk.  EROS is no
different; it views it's partition as a single volume, and largely
ignores the rest of the disk.
<P> Every EROS volume has up to 64 non-overlapping areas.  To avoid
collision with firmware terminology, we use the term
<STRONG>division</STRONG>.  Divisions come in several types:
<ul>
  <table>
    <tr>
      <td>
	Unused
      </td>
      <td>
	Empty. Placeholder for unallocated space.
      </td>
    </tr>
    <tr>
      <td>
	Boot
      </td>
      <td>
	Contains the system-dependent boot code.
      </td>
    </tr>
    <tr>
      <td>
	DivTbl
      </td>
      <td>
	A table describing the division layout.
      </td>
    </tr>
    <tr>
      <td>
	Kernel
      </td>
      <td>
	The kernel's code.
      </td>
    </tr>
    <tr>
      <td>
	Spare
      </td>
      <td>
	Division containing spare sectors.
      </td>
    </tr>
    <tr>
      <td>
	Swap
      </td>
      <td>
	A swap area.
      </td>
    </tr>
    <tr>
      <td>
	Page
      </td>
      <td>
	Holds EROS Pages.
      </td>
    </tr>
    <tr>
      <td>
	Node
      </td>
      <td>
	Holds EROS Nodes.
      </td>
    </tr>
  </table>
</ul>
<P> Every volume has exactly one boot division at sector 0.  Every
volume has <EM>at most one</EM> kernel division.
<P> Every disk technology that we know about reserves sector 0 of a
partition the "boot sector."  The EROS boot sector has reserved
locations from which the locations of the primary (and optional
secondary) division tables can be read.
<P> For each division, the division table describes the type, the
starting sector, the ending sector, and a unique system identifier.
If the division is a Node, Page, or Swap division, the division table
also contains the starting CDA or SwapLoc of the division.
<H2>3.1 Formatting and Spare Sectors</H2> 
<P> <em>How much of this is still true?</em>
<P> Volume formatting is performed by the <STRONG>mkvol</STRONG>
utility.  The <STRONG>mkvol</STRONG> program formats a raw volume for
use as an EROS volume.  It takes as input a volume map file that
describes how the disk should be formatted and the name of the volume
file (on UNIX, the <EM>raw</EM> file corresponding to the EROS
parition), and an optional disk descriptor type.  Currently valid
descriptor types are <STRONG>fd144</STRONG> and <STRONG>fd</STRONG>.
Descriptor types are used to describe the desired layout of the target
volume when not writing directly to the raw device.
<P> In order for a volume to be usable, the <STRONG>kernel</STRONG>,
<STRONG>boot</STRONG>, and <STRONG>divtbl</STRONG> divisions must be
error free.  (We know how to remove this restriction for the kernel
division, but have not done the necessary work in the boot loader to
support sector remapping).
<P> Some disk subsystems (SCSI, some IDE) provide automatic sector
remapping.  On these subsystems, no <STRONG>spare</STRONG> division is
required, and the EROS kernel relies on the underlying hardware to
provide remapping services.  On disk subsystems that require
OS-implemented remapping, the <STRONG>spare</STRONG> division will be
used, if present, as a pool of spare sectors.  <EM>The spare division
is always optional.</EM> If it is not present, no remapping will be
performed.  The system generation process, for example, does not place
a <STRONG>spare</STRONG> division on the ISL floppies, because we
assume that the distribution media will be free of errors.
<P> If a <STRONG>spare</STRONG> division is present, the bad sector
remapping information is kept in the same disk page as the division
table, and directly follows the division table entries.  This allows a
disk to have up to 384 bad sectors.  Beyond that number, we think it's
probably time to replace the disk.
<P> As an EROS volume is formatted, a list of bad sectors is
accumulated by the formatting program.  Bad sectors that are found in
Page, Node, or Swap divisions are added to the BadMap.  Bad sectors
that are found in other types of divisions cause formatting to fail.
Usually these failures can be worked around by rearranging the volume
map to avoid the bad sectors.
<H2>3.2 The mkvol Volume Map</H2>
<P> The <STRONG>mkvol</STRONG> utility performs formatting according
to a volume map.  The volume map is essentially a human-readable
version of the division table to be created.  It describes the start
and end of each division, and for some divisions it provides the file
that should be used to initialize that division (for example: the file
containing the code for the boot sector).
<P> The <CODE>volmap</CODE> file syntax is very simple.  Comments are
preceded by the '<KBD>#</KBD>' character.  Valid statements in the
volmap file are:
<DL>
  <DT>
       <p> <STRONG>kernel</STRONG> [<EM>size</EM>] [ <EM>kernel file</EM> ]
       </STRONG>
  <DD>
       <p> Describes the kernel division.  The size, if provided, indicates the
       number of pages that should be reserved for the kernel.  The kernel
       file, if provided, will be written into the kernel division after the
       a.out file header is stripped off.  If no kernel image name is
       provided, /eros/lib/boot/eros.image will be used.  If no size is
       provided, the smallest division that will hold the kernel image will
       be created.
       <P> At most one kernel division can be present per volume.
  <DT>
       <p> <STRONG>divtbl</STRONG>
  <DD>
       <p> Indicates that a division table should be written at this
       point on the disk.  Up to two <STRONG>divtbl</STRONG> statements can
       appear in the volmap file.  The first will be used as the primary
       division table location.  The second will be used as the alternate
       division table location.
  <DT>
       <p> <STRONG>spare</STRONG> [<EM>secs</EM>]
  <DD>
       <p> Indicates that a division of <EM>secs</EM> sectors (rounded
       up to the nearest page) should be reserved.  There can be only
       one spare division per volume.  If a volume contains a spare
       division, the EROS kernel will perform bad sector handling for
       that volume.
  <DT>
       <p> <STRONG>page</STRONG> <EM>count</EM>
       <STRONG>OID=</STRONG><EM>oid</EM> 
  <DD>
       <p> Indicates that a page division containing <EM>count</EM>
       EROS pages should be created.  The <STRONG>mkvol</STRONG>
       utility automatically computes the number of sectors required
       to hold the pages and the associated page pots.  All pages are
       created with an initial allocation count of 0.  All data space
       in the page division is initialized to zero.
  <DT>
       <p> <STRONG>node</STRONG> <EM>count</EM>
       <STRONG>OID=</STRONG><EM>oid</EM>
  <DD>
       <p> Indicates that a node division containing <EM>count</EM>
       EROS nodes should be created.  The <STRONG>mkvol</STRONG>
       utility automatically computes the number of sectors required
       to hold the pages and the associated page pots.  All nodes are
       created with an initial allocation count and call count of 0.
       All keys in the node division are initialized to zero number
       keys.
  <DT>
       <P> <STRONG>swap</STRONG> <EM>count</EM>
  <DD>
       <P> Indicates that a swap division countaining <EM>count</EM>
       pages should be created.  EROS uses a ring buffer approach to
       managing swap areas.  It is not necessary to specify what
       generation the swap area belongs to.
</DL>
<H1>3. Sysgen: Mapping Eros Images Onto Volumes</H1>
<p> Once an EROS system image has been constructed and a volume has
been formatted, mapping one onto the other is fairly straightforward.
Pages from the EROS image are copied to the actual volume starting
with the first page division, and nodes are copied starting with the
first node division.
<p> As a matter of practical simplicity, the sysgen utility assumes
that only zero pages will need to be relocated.  When the time comes
to actually write such pages to the volume, final OID's are assigned
to these pages and the necessary keys are relocated.  All other
objects are assumed to end up at the OID's assigned in the eros image
volume, which precludes the need for any relocation.
<p> A more sophisticated design would enable range placement, but I
haven't the energy to build it at this time.
<H1>4. The Boot Process</H1>
<p> <em>This may want to move to another document</em>
<P> When the machine is powered up or reset, the firmware determines
which volume (disk or partition) will be booted.  Though the details
are machine specific, the firmware typically proceeds by loading a
small machine-dependent boot sector from sector 0 of the boot volume.
Usually, the boot sector must first load a larger bootstrap routine,
which in turn examines the volume map and loads the kernel itself.  At
this point, the EROS kernel proceeds with it's own startup procedure.
<P> At a high level, the EROS kernel startup proceeds as follows:
<OL>
  <LI> Perform the minimum required hardware initialization.
  <LI> Determine the unique system identifier (USID) of the running
       kernel.
  <LI> Locate and mount all divisions on all disks whose USID matches
       the USID of the running kernel.
  <LI> Locate the run list and begin executing the running processes.
       The run list can come from one of two locations:
       <UL>
	 <LI> If a swap areas has been located, the list of running
	      processes is obtained from the most recent valid
	      checkpoint image in the swap area.
	 <LI> If no swap area exists, the ipl key in the boot volume's
	      boot sector is used to invoke the first domain.
       </UL>
</OL>
<P> The net effect of this is that EROS will restart the existing
system image if possible.  If no existing system image can be found,
it will load a minimal system starting set.  The intention is that
this minimal process set will be sufficient to enable system recovery.
<P> There are two security-related assumptions embedded in this
bootstrap mechanism:
<UL>
  <LI> If unauthorized users are able to physically access the boot
       media, the system is not secure.
  <LI> If unauthorized users are able to physically access the system
       console, and the swap area(s) have been damaged or do not
       exist, the system is not secure.
</UL>
<strong>OBSOLETE:</strong>
<ul>
  <P> The intent of the FailStart mechanism is to simplify initial
  installation of the system.  It eliminates the need to construct a
  swap area on the installation floppy.  Being able to run the kernel
  before the swapping logic has been debugged greatly simplifies initial
  testing.
  <P> Some other potential uses of the FailStart mechanism include:
  <UL>
    <LI> Degrading gracefully when a swap device cannot be found because
	 it is offline.  <EM>This is not implemented.</EM>
    <LI> Recovering from sector 0 failures on non-boot volumes.
	 <EM>This is not implemented.</EM>
    <LI> Implementing system startup in embedded systems that boot from
	 a ROM and do not have a swap area at all.
  </UL>
  <P> The FailStart mechanism was not present in the KeyKOS system.
  It's uses and abuses have not been fully explored.
</ul>
<H2>3.1. Finding the Unique System Identifier</H2>
<P> When the EROS kernel starts up, it performs some minimal hardware
initialization and then locates the boot volume.  It loads the boot
sector to learn the location of the division table on the boot volume,
and then loads the division table itself.  From the kernel entry in
the division table, it learns the system id of the kernel that was
booted.
<P> <I>An alternative would be to have the system ID passed to the
kernel by the bootstrap routine, which has the information available
in any case.</I>
<H2>3.2. Mounting the Divisions</H2>
<P> Having determined the unique system identifier, the kernel now
loads the master partition table of every disk, and searches for EROS
volumes.  For every EROS volume that it finds, it loads the division
table.  For every Page, Node, and Swap division whose USID matches
that of the running system, the division is mounted and the pages and
nodes in that division are made available to the object loading
subsystem.
<H2>3.3. Finding the Run List</H2>
<P> Once all of the divisions are known, the kernel must locate the
list of processes to run.  If a swap area is available, the run list
is loaded from the swap area.  If no swap area is available, then the
FailStart entries from the boot volume's division table are used.  The
processes named in the run list are swapped in and scheduled.
<P> Once these processes have been started, the system is running
normally, and bootstrap processing terminates.
<hr>
<em>Copyright 1998 by Jonathan Shapiro.  All rights reserved.  For terms of 
redistribution, see the 
<a href="../legal/license/GPL.html">GNU General Public License</a></em>
</td><td width="10%">&nbsp;</td></tr></table></BODY>
</html>
