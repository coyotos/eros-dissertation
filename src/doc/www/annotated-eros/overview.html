<HTML>
<style>
</style>
  <HEAD>
    <TITLE>The Annotated EROS: Overview</TITLE>
  </HEAD>
  <BODY BGCOLOR="#FFEEDD">
    <table>
      <tr valign=top>
	<td width="10%" nowrap>
	  &nbsp;
	</td>
	<td>
	  <center>
	    <H1>The Annotated EROS: Overview</H1>
	  </center>
	  <div class=nocss>
	    &nbsp;<br>
	  </div>
	  <p>
	    Before turning to the source code, a brief introduction to
	    the EROS system is appropriate: what it is, what issues
	    impact its design, and how it looks to the user.
	  </p>
	  <h2>1.&nbsp;&nbsp;What is EROS?</h2>
	  <p>
	    EROS is a policy-neutral, persistent capability system.
	  </p>
	  <p>
	    A <em>policy-neutral system</em> is one that does not
	    dictate specific behaviors for exceptional conditions.
	    When something goes wrong, such as a page fault or an
	    illegal instruction, EROS notifies an appropriate
	    user-level process of the problem.  It is the
	    responsibility of the user level process to decide what
	    policy should apply for that error and to implement that
	    policy.  In EROS, Space allocation, memory management, and
	    exception handling are all application-managed activities.
	  </p>
	  <p>
	    A <em>persistent system</em> is one that remembers what
	    programs were doing across system shutdowns.  Very little
	    data is lost in such a system when the power goes out or
	    the machine is reset.  Periodically, or when asked to do
	    so, a persistent system saves a copy of everything that is
	    going on in the machine: processes, data, access rights,
	    etc.  When the system restarts after a failure, programs
	    resume what they were doing when the system was last
	    saved.  All processes are restored, complete with their
	    temporary data, windows, memory, and access rights.  In
	    EROS, all of the processes and data in the system are
	    periodically saved (typically every 5 minutes).
	  </p>
	  <p>
	    A <em>capability system</em> is one in which access to
	    objects and resources is provided exclusively through
	    <em>capabilities</em>.  A capability is an unforgeable
	    token that names an object or resource and authorizes a
	    set of operations on that resource.  In EROS, objects are
	    things like <b>nodes</b>, <b>pages</b>, and
	    <b>processes</b>.  Operations are things like
	    <em>read</em>, <em>write</em>, <em>start</em>, or
	    <em>stop</em>.  Every object in the EROS system defines a
	    set of operations that it knows how to perform.  Every
	    capability authorizes some subset of those operations on a 
	    particular object.
	  </p>
	  <p>
	    In EROS, capabilities are rendered unforgeable by
	    <em>partitioning</em>.  Programs can neither read nor
	    write the bits of a capability using data instructions,
	    nor can they read or write data using capability
	    instructions.  Other capability systems use encryption or
	    signing to protect capabilities.  While encrypted
	    capabilities are easier to manipulate and manage,
	    encrypted capability systems are both less efficient and
	    more difficult to do security analysis on than partitioned 
	    capability systems.
	  </p>
	  <p>
	    These three facts dictate much of the design of the EROS
	    system.  The other two relevant issues are correctness and
	    performance.
	  </p>
	  <h3>Correctness</h3>
	  <p>
	    One key job of any kernel is to ensure that the system
	    doesn't crash or make mistakes.  This seems so obvious
	    that it goes unstated by most operating system books.
	    Try looking up ``correctness,'' ``constraint,'' or
	    ``consistency'' in your favorite textbook.
	  </p>
	  <p>
	    The correctness of the EROS system as a whole is layered
	    in two parts:
	  </p>
	  <ol>
	    <li>
	      <p>
		The correctness of the operating system kernel.
	      </p>
	    </li>
	    <li>
	      <p>
		The correctness of the essential system applications,
		<em>assuming</em> that the operating system behaves
		correctly.
	      </p>
	    </li>
	  </ol>
	  <p>
	    If the kernel is well architected, the correctness
	    requirements can be expressed as a set of
	    <em>constraints</em> that the kernel maintains during its
	    execution.  For example: every running thread of control
	    must have an associated process structure.  
	  </p>
	  <p>
	    In the EROS design, a fairly small set of constency rules
	    can be stated for the operating system.  Provided these
	    consistency rules are maintained, the operating system is
	    in one sense correct.  There may still be bugs in the
	    code, but the design and the concept of the implementation
	    is sound.  Because the requirements are clearly stated, it
	    is possible to implement a reliable system.  
	  </p>
	  <p>
	    With the exception of EROS's predecessor <a
	    href="http://www.cis.upenn.edu/~KeyKOS">KeyKOS</a>, I am
	    aware of no other system that has clearly <em>stated</em>
	    such consistency requirements, never mind actually checked
	    them.  EROS does both, which is why we call it the
	    Extremely <em>Reliable</em> Operating System.  Until the
	    equivalent requirements are clearly stated for POSIX or
	    Win32, we would argue that it is <em>not</em> possible
	    (even in principle) to build a reliably implementation of
	    those systems.
	  </p>
	  <h3>Performance</h3>
	  <p>
	    The EROS code base is a high-performance implementation.
	    Maintaining and verifying correctness constraints has a
	    certain cost.  Performance is in some cases achieved by
	    selectively replacing the simple constraints with
	    equivalent but more complicated constraints that shift the
	    burden of constraint maintainance onto less-frequently
	    executed code.  From a reader's perspective, this makes
	    the code more complicated to understand.  EROS code base
	    is a high-performance implementation.
	  </p>
	  <p>
	    The EROS system is conceptually lean, having only a few
	    critical paths.  As a result, only a few constraints
	    needed to be revised in this way.  The book explains these
	    optimizations as they appear, describing the original,
	    simple constraint, the more complicated replacement, and
	    the rationale for the change.
	  </p>
	  <h2>2.&nbsp;&nbsp;Objects</h2>
	  <p>
	    Capability systems, by their nature, are object oriented
	    systems.  Capabilities name objects, and all operations
	    are performed by <em>invoking</em> a capability.  The
	    operation to be performed is identified by a <em>request
	    code</em>, and the result of that operation is described
	    by the <em>result code</em>.  Invocations can also pass
	    and return arguments, as we will see in a moment.  The
	    operation itself is performed by the object.
	  </p>
	  <p>
	    Objects in EROS can be divided into several categories:
	  </p>
	  <ul>
	      <p>
		<b>Primary Objects</b> include <em>nodes</em>,
		<em>data pages</em>, <em>capability pages</em>, and
		<em>processes</em>.  These objects are implemented by
		the kernel, but their content is controlled by the
		application.  The kernel knows how to interpret these
		objects, and in some cases caches their content in the
		interest of performance.
	      </p>
	      <p>
		<b>Kernel Objects</b> are implemented directly by the
		kernel.  These objects either provide access to
		information about the kernel or export kernel services
		to applications.  Examples of kernel objects include
		the <em>null capability</em> and the <em>range
		capability</em>.
	      </p>
	      <p>
		<b>Devices</b> (really device drivers) are implemented
		by the kernel, and provide access to particular pieces
		of hardware.  The EROS philosophy is that device
		drivers should be minimal; most of the responsibility
		for device management is left to applications.
	      </p>
	      <p>
		<b>User Objects</b> include all services implemented
		by application processes.  Because EROS processes
		survive system shutdown, most ``objects'' are actually
		implemented by application programs.  Like the other
		kinds of objects, user objects are accessed via
		capabilities.  EROS defines two capability types that
		are used in connection with user objects: <em>start
		capabilities</em> and <em>resume capabilities</em>.
	      </p>
	  </ul>
	  <p>
	    A complete list of EROS objects and the services they
	    perform can be found in the <a
	    href="http://www.eros-os.org/devel/ObRef/Cover.html">EROS
	    Object Reference Manual</a>.  We provide here only a brief
	    summary of the primary objects.
	  </p>
	  <h3>Data Pages</h3>
	  <p>
	    Data pages are the basic unit of memory in the EROS
	    system.  A data page can hold program code or data, or
	    perhaps the content of a database. Whenever a program
	    references an address using a load or store instruction,
	    the address is ultimately resolved into a data page and an
	    offset into that page.  The size of a data page is
	    determined by the host architecture (e.g. the x86 family),
	    so they can be mapped directly by the hardware memory
	    mapping mechanism.
	  </p>
	  <p>
	    EROS does <em>not</em> have any equivalent to the ``memory
	    objects'' of systems like Mach or Chorus.  Instead, data
	    pages are assembled into memory segments, which are
	    described below.
	  </p>
	  <h3>Capability Pages</h3>
	  <p>
	    Because EROS protects capabilities by partitioning, they
	    cannot be stored in data pages.  Capability pages provide
	    for capabilities exactly the same sort of storage that
	    data pages provide for ordinary data.  A capability page
	    can be mapped into an address space just as a data page
	    can, but cannot be read or written by data memory
	    references.  The EROS kernel provides emulated
	    instructions that allow capabilities to be loaded from or
	    stored to capability pages.  The size of a capability page
	    is the same as the size of a data page.  On the x86
	    architecture, capability pages hold 256 capabilities.
	  </p>
	  <p>
	    Capability pages were a relatively late addition to the
	    EROS design, and their current implementation could be
	    significantly improved.
	  </p>
	  <h3>Nodes</h3>
	  <p>
	    Nodes are basically small capability pages, holding 32
	    capabilities each (regardless of the processor
	    architecture).  They are used as components of processes
	    and as the structuring elements of the EROS memory mapping
	    mechanism.  Historically, they were also used as a unit of
	    storage for capabilities, but this usage has become
	    vestigial with the introduction of capability pages.
	  </p>
	  <hr>
	  <p>
	    <em>Copyright 1998 by Jonathan Shapiro.  All rights
	    reserved.  For terms of redistribution, see the <a
	    href="./legal/license/EPL.html">EROS License
	    Agreement</a></em>
	  </p>
	</td>
	<td width="5%">
	  &nbsp;
	</td>
      </tr>
    </table>
  </body>
</html>
