BEGIN {
  QWIKLEN = 8;
  for (i = 0; i < QWIKLEN; i++)
    pred[i] = "";
}

{
  for (i = 0; i < QWIKLEN; i++)
    if (pred[i] != "") printf("%s ", pred[i]);
  printf("%s\n", $1);
  
  for (i = 1; i < QWIKLEN; i++)
    pred[i-1] = pred[i];
  pred[QWIKLEN-1] = $1;
}
