\chapter{Introduction}

\index{components!design using} Modern applications are increasingly
built as assemblies of binary only components from many vendors.
As the number of authors increases, the interests and
incentives of the parties involved diverge.  As a result, it is no
longer possible to trust an application taken as a whole \emph{unless}
an object-granularity access control mechanism is incorporated into
the basic system design.
%%% At
%%% best, these components are non-specifically tested, are written by
%%% unknown authors, and are subject to the same sources of flaws and
%%% errors that any software system suffers; if the service provided by
%%% the component were trivial, there would be no need to design
%%% components.  Dis-integration of application design implies
%%% non-alignment of the incentives and objectives of the parties
%%% contributing to the final software artifact.  
Objects should be individually named and protected. Programs should
hold authority to the objects they require -- and no more.

Capabilities provide just such an object-oriented protection model.
The term ``capability'' was first introduced in 1966 by Van Horn
\cite{Dennis:Capabilities} as a generalization of the descriptor
concept implemented in the Burroughs B5000 computer
\cite{Burroughs:B5000}.  A capability refers to a procedure,
segment, or other object, and specifies a set of access rights on
the object that it names.  Systems that use capabilities as their
primitive protection mechanism are known as ``capability systems.''

\index{components!security requirements}\index{access control
  list!vs.  component software|(}Capability systems can support
several basic properties that access control list (ACL) based
systems do not (Figure~\ref{fig:cap-properties}).
\begin{figure}[htbp]
  \begin{center}
    \begin{description}
    \item[\textbf{Least Privilege}] \index{least privilege, principle
        of}\index{principle!least privilege}Programs should have no more
      authority than they require.
    \item[\textbf{Selective Access Right Delegation}] \index{access
        rights!selective delegation}\index{delegation, access
        right}\index{principle!selective delegation}A program that
      possesses authority should be able to selectively delegate that
      authority to its component subprograms.
      
      It must be possible to run components with lesser authority than
      that of their principal.
    \item[\textbf{Rights Transfer Control}] \index{rights transfer
        control}\index{principle!rights transfer control}A (sub)program
      should be able to receive additional authority only if that
      authority is granted via an explicitly authorized channel.
    \item[\textbf{Information Transfer Control}] \index{information
        transfer control}\index{principle!information transfer control}A
      (sub)program should be able to exchange informatoin only by way of
      an explicitly authorized channel.
    \item[\textbf{Endogenous Verification}] \index{endogenous
        verification}\index{principle!endogenous verification}It must be
      possible to verify from within the system itself that certain
      restrictions on rights transfer and information flow are met.

      In many systems, such tests are possible only if the examiner is in
      a position to stand outside the system and examine all of its
      state at some instant.
%%%   Systems must be \emph{seen} to be secure.\footnote{``Justice must
%%%     not only be done, it must be seen to be done.'' Roy v. Jones, 349
%%%     F. Supp.  315, 319 (W.D. Pa.  1972), aff'd,
%%%     {\textless}=42{\textgreater} 484 F.2d 96 (3d Cir. 1973)}
    \end{description}
    
    \caption{Basic properties supported by capabilities}
    \label{fig:cap-properties}
  \end{center}
\end{figure}
\index{capability system!challenges}In addition, capability systems
\emph{may} support the security policies that are necessary in
component based application architectures.  Advocates have long
claimed that they do.  In spite of this, no specific capability
architecture has been formally described that demonstrates these
properties, nor has the correctness of such an architecture with
respect to essential security policies such as confinement
\cite{Lampson:Confinement} been shown.

%%%The current push for distributed ``middleware'' is leading to system
%%%designs in which multiple parties come to a common platform for
%%%commerce-related services.  The seller has a good to sell that the
%%%buyer wishes to buy.  Their respective banks wish to ensure that
%%%credit relationships are not compromised and that their outstanding
%%%close-of-day balances do not spiral further out of
%%%control.\footnote{Today, a typical United States bank finishes the day
%%%  with outstanding uncleared transactions that exceed its liquidity by
%%%  a factor of ten or more.  Even if the total dollar value of
%%%  inter-bank obligations did not change, increasing the number of
%%%  transactions increases the risk of cascading collapse in the banking
%%%  system.} The shipper wishes to ensure that they have clearly
%%%stated obligations to all parties.  While the incentives associated
%%%with any single transaction of this form are basically aligned, the
%%%incentives across transactions in aggregate are not.  To support such
%%%commerce, it is necessary that parties with \emph{conflicting}
%%%objectives be allowed mediated access to common state.

%%% \section{Requirements}

Supposing that such an architecture existed, no existing
\emph{implementation} of a capability system has delivered performance
competitive with access list based, monolithic system implementations.
Quite the contrary, previous implementations have proven almost
universally disappointing.  Further, capability architectures appear
to most naturally express themselves as dekernelized designs, and a
great deal of convincing evidence has accumulated to cast doubt on the
potential performance of such designs.
Before capability systems can be useful, these challenges to their
practical feasibility must be addressed.

This dissertation makes four basic contributions.  First, it presents
{\dimtake}, an extension of the {\takegrant} model of capability
access control proposed by Jones, Lipton, and Snyder
\cite{Jones:linear} and enhanced by Bishop \cite{Bishop:transfer} to
address object metadata and certain endogenous access checks (i.e.
checks performed from within the system).  These extensions bridge the
gap from the {\takegrant} model to the behavior of real systems.

Second, it provides a formal model of capability architectures,
{\swmodel,} that allows precise statements about access, and more
particularly, confinement, to be made and proven.\footnote{SW stands
  for Shapiro--Weber.} {\Swmodel} defines a family of capability
systems, of which any realization satisfies these security properties.
{\Swmodel} is the first formal model of a general purpose operating
system architecture.  It is also the first model for which the outcome
of such a proof has been favorable.  Previously successful proof
efforts such as \cite{Harrison:Protection} have shown that enforcement
of a desired security policy was either undecidable or impossible
within the protection frameworks examined.
  
Third, this dissertation presents the Extremely Reliable Operating
System (EROS).  EROS is one realization of an {\swmodel} system.  This
dissertation discusses the EROS virtual machine architecture and how
the EROS capabilities are mapped to {\swmodel}.  By virtue of this
mapping, the EROS design is in fact covered by both the model and the
proof.

Finally, this dissertation presents a prototype implementation of the
EROS system on the Intel x86 machine architecture.  By mapping EROS
virtual machine abstractions to hardware-supported objects, this
implementation yields measured performance competative with, or even
exceeding, conventionally-architected ACL-based operating systems and
microkernels.

%%%   Capability systems have consistently failed due to poor performance
%%%   -- a problem that this dissertation resolves.  As their history is
%%%   amply covered elsewhere
%%%   \cite{Gehringer:CapSystems,Levy:CapabilitySystems}, I note
%%%   here only the result: that capability systems have fallen out of
%%%   favor over the last 20 years.

\section{Why Capability Systems?}
Since capability designs have fallen out of favor in the last 20
years, it may be helpful to give an illustrating example of how they
work and why current (ACL) systems fail to satisfy the properties of
Figure~\ref{fig:cap-properties}.

Consider a simple system configuration having only two users: Fred and
Wilma.  Each user has a process that runs on their behalf, $p1$ and
$p2$.  There are three objects in the system, $o1$, $o2$, and $o3$.
Fred created $o1$ and $o2$.  Fred can read and write $o1$, and has
read-only access to $o2$.  Wilma created $o3$, and can read and write
it.

Using this example, I will briefly illustrate that the ACL model fails
for each property. The capability model succeeds on all but one, and
can be straightforwardly extended to handle all of these properties
without sacrificing efficiency.

\subsection{ACL-Based Protection}
Figure~\ref{fig:simple-acl} shows these users and objects using the
access control list model.  Every user has an assigned principal
identifier.  Every program is tagged with the principal identifier of
its user.  Each object has an associated access control list, which is
a mapping from principal ids to a set of access rights, and is shown
below the object.  The resulting access rights associated with each
process are shown by dashed arrows.
\begin{figure*}[htbp]
  \begin{center}
    \leavevmode
    \psfig{figure=simple-acl.eps}
    \caption{A simple system using access control lists.}
    \label{fig:simple-acl}
  \end{center}
\end{figure*}

\subsubsection{Failure of Least Privilege}
In access control list systems, authorities are granted to programs
according to the user identity of their principal.  All of the
authorities associated with a principal are granted to every program
running on behalf of that principal.  There is no means for a program
to reduce this authority.  Thus, the least privilege property is not
supported by access control list systems.

\subsubsection{Failure of Selective Access Right Delegation}
Selective access right delegation  is also unsupported.
Suppose that Fred wishes to create a new program $p3$, which should
have access only to object $o1$
(Figure~\ref{fig:simple-acl-nodelegate}).  This new process is created
by $p1$, and inherits the principal id \textbf{fred} from its parent.
Because access rights are based solely on principal id, the access
rights of $p3$ are identical to those of $p1$.  \emph{There is no
  means to reduce these rights.}  In the illustration, access rights
are shown by dashed arrows, with the associated rights attached to
each arrow.
\begin{figure*}[htbp]
  \begin{center}
    \leavevmode
    \psfig{figure=simple-acl-nodelegate.eps}
    \caption{ACL Model: All of Fred's processes have identical access rights.}
    \label{fig:simple-acl-nodelegate}
  \end{center}
\end{figure*}

\subsubsection{Failure of Rights and Information Transfer Control}
Consider the case where Wilma's program has access to restricted
information, but does not have access to an external high-speed
communication channel. Fred does not have access to the restricted
information, and therefore has been granted access to the network.
Fred wishes to collude with Wilma to disclose the restricted
information.

To do so, all that is necessary is for Fred to add Wilma to the access
list for $o2$, granting write access to every process running on
behalf of the \textbf{wilma} principal.  Once Wilma has write access
to this object, the information can be transferred to Fred, who in
turn can transfer it over the network.

Because access control list systems cannot control the transfer of
access rights, they cannot control the transfer of information.  This
has been shown formally in \cite{Harrison:Protection}.

\subsubsection{Failure of Endogenous Verification}

Since Fred's  programs can increase the authority of Wilma's programs
at any time, rights transter and information flow are unrestricted.
It is therefore not possible to verify that constraints on rights
transfer or information flow are satisfied.

\subsection{Capability-Based Protection}
Figure~\ref{fig:simple-cap} shows the same objects and users as they
would be managed in a capability system.  Arrows represent
capabilities, and are annotated with the access rights granted by that
capability.  Every process holds capabilities for the objects it can
access.  Neither process holds a capability to the other, and their
access to $o2$ is read-only.  The two processes therefore cannot
exchange either information or authority.
\begin{figure*}[htbp]
  \begin{center}
    \leavevmode
    \psfig{figure=simple-cap.eps}
    \caption{A simple system using capabilities.}
    \label{fig:simple-cap}
  \end{center}
\end{figure*}

\subsubsection{Least Privilege and Selective Access Right Delegation}
When a new process is first created by Fred, it holds no capabilities
(Figure~\ref{fig:simple-cap-newproc}), and therefore has no intrinsic
access rights.  Instead, the creating process -- in this case $p1$ --
is given a capability to the new process conveying \textbf{take} and
\textbf{grant} rights.  The take right allows $p1$ to read a
capability from $p3$.  The grant right allows $p1$ to write a
capability into $p3$.  Once the process is created, $p1$ can choose to
populate it with as many or as few capabilities as desired.  It is
therefore possible to construct processes that have the least amount
of privilege that is necessary to perform their function.
\begin{figure*}[htbp]
  \begin{center}
    \leavevmode
    \psfig{figure=simple-cap-newproc.eps}
    \caption{Creation of a new process using capabilities.}
    \label{fig:simple-cap-newproc}
  \end{center}
\end{figure*}

\subsubsection{Rights and Information Transfer Control}
Because $p1$ and $p2$ have no ability to exchange capabilities or
data, it is not possible to transfer either access rights or
information between them.  If Wilma has access to restricted data,
there is no way for this data to be transmitted to Fred.

In order for access rights to be exchanged, one of the processes must
hold a capability conveying either \textbf{take} or \textbf{grant}
authority to the other, such as the dashed capability in
Figure~\ref{fig:simple-cap-xfer}.  Only if such a capability is
present can access rights be transferred between the two parties.
\begin{figure*}[htbp]
  \begin{center}
    \leavevmode
    \psfig{figure=simple-cap-xfer.eps}
    \caption{Authority transfer with capabilities.}
    \label{fig:simple-cap-xfer}
  \end{center}
\end{figure*}

Similarly, the absence of a capability conveying a suitable
combination of read and write permissions on an object accessable to
both Fred and Wilma prevents unauthorized transfer of information.
The definition of ``suitable combination'' is modestly tricky, and is
covered in detail in Chapter~\ref{cha:dimtake}.  Briefly, if Wilma can
write an object that Fred can read, information can be transferred
from Wilma to Fred.  Note, however, that this is \emph{only} possible
if the two parties hold appropriate authorities, and that such a
transfer is therefore authorized.\footnote{This dissertation does not
  address considerations of covert channels.}  Because the granting of
authorities can be controlled, it is possible to construct initial
conditions in which such information transfers are not possible.  This
dissertation proves the correctness of one mechanism for doing so in
Chapter~\ref{cha:proof}.

\subsubsection{Endogenous Verification}

One flaw in the capability model as defined by Van Horn
\cite{Dennis:Capabilities} is the \emph{inability} to verify from
within the system that no information transfer is possible via a given
capability.  Even if the holder traverses all objects reachable from
that capability, it is possible that one of these objects will later
be modified by another party in such a way as to increase the
authority accessible to $p1$.

\begin{figure*}[htbp]
  \begin{center}
    \leavevmode
    \psfig{figure=endogenous.eps}
    \caption{Failure of endogenous read-only check.}
    \label{fig:endogenous}
  \end{center}
\end{figure*}
Suppose one wishes to know that process $p1$ cannot write data to any
object accessable from capability (1) in Figure~\ref{fig:endogenous}.
For this to be true, either
\begin{itemize}
\item Capability (1) does not convey \textbf{take} or \textbf{write}
  authority, \emph{or}
\item No capability similar to capability (2) exists that conveys
  \textbf{grant} authority, \emph{or}
\item The \textbf{take} authority of capability (1) is somehow reduced
  so as to guarantees transitive read-only access.
\end{itemize}

The first is feasible, but is inconveniently restrictive, as one would
like to pass structures connected by capabilities.  Locating all
capabilities similar to (2) requires an exogenous (i.e. external)
perspective on the system.  Such a search requires the authority to
examine all objects. Such authority is unlikely to be granted, and in
any case a search of all objects is prohibitively expensive in large
systems.  Transitive read-only access is not included in the
capability model described by Van Horn.  Chapter~\ref{cha:dimtake}
shows how it can be added, and what impact this addition has on the
capability access model.


%%%   In access list systems, possession of authority does not imply the
%%%   right to transfer authority, even if authorized channels of transfer
%%%   exist.

%%%   In access list based systems, any principal with ``own'' rights to a
%%%   given object can grant authority to another principal, and therefore
%%%   to the programs executing on behalf of that principal.  The grant
%%%   operation does not require that an authorized channel exist between
%%%   grantor and grantee.\footnote{A weak case can be made that the
%%%     entire file system is such a channel.  Architectures incorporating
%%%     file systems generally treat access to the file system as an
%%%     authority intrinsic to all processes.  In such architectures, the
%%%     file system is an unavoidable channel rather than an authorized
%%%     channel.}

%%%   Lacking explicit rights transfer authorization, access list systems
%%%   do not provide for endogenous verification of restrictions on rights
%%%   transfer.

%%%   Lacking any means \emph{other} than principal to determine
%%%   authorization, access list systems cannot accomplish this.


%%% \index{ACL|see{access control list}}
%%% Note that the access control list model of access control does not
%%% support any of these properties.  In contrast, the capability access
%%% control model directly supports least privilege, access right
%%% delegation, and rights transfer authorization.  With straightforward
%%% modification, it can be made to support endogenous verification.
%%% \index{access control list!vs. component software|)}

%%% \index{capability system!challenges}
%%% Given this, the capability model \emph{may} be able to meet the access
%%% control requirements of component based application architectures.
%%% Advocates have long claimed that they do.  In spite of this, no
%%% specific capability architecture has been formally described that
%%% demonstrates these properties, nor has the correctness of such an
%%% architecture with respect to certain essential security policies been
%%% shown.

%%% Supposing that such an architecture existed, no existing
%%% \emph{implementation} of a capability system has delivered performance
%%% competitive with access list based, monolithic system implementations.
%%% Quite the contrary, previous implementations have proven almost
%%% universally disappointing.  Further, capability architectures appear
%%% to most naturally express themselves as dekernelized designs, and a
%%% great deal of convincing evidence has accumulated to cast doubt on the
%%% potential performance of such designs.

In summary, \emph{all} of the desired properties can be handled within
the capability model with minimal modification, and none within the
access control list model.  The problem is to show that the
performance difficulties associated with capability systems can be
overcome.  This dissertation demonstrates quantitatively that it can.

The balance of the introduction outlines the contributions of this
dissertation in greater detail, and describes the structure of the
balance of the document.
\section{Capability Access Model}

The first contribution of this dissertation is the {\dimtake} access
model.  The basic capability access model is {\takegrant}, originally
described by Jones, Lipton, and Snyder \cite{Jones:linear} and
subsequently enhanced by Bishop \cite{Bishop:transfer}.  This model
does not address the access implications of object metadata, which is
used in all real capability systems.  In addition, it does not
incorporate provision for transitive read-only access rights.

The need for an access model incorporating object metadata becomes
apparent when memory management is exported from the supervisor into
potentially unpriveleged user-mode programs.  Such programs indirectly
manipulate memory mapping tables, and consequently alter the access
rights of the programs they manipulate.  An access model is needed
that accounts for the effect of such manipulations.  Mapping
structures manipulable by the memory manager are often opaque to the
client process even though the objects mapped are not.  Provision for
indirection through such opaque metadata must be incorporated into the
access model.

For reasons described above, it is desirable to have an access right
that allows the holder to traverse capability-linked structures
without thereby gaining write access to anything.  Here again, the
access model needs to be enhanced.

The {\dimtake} access model incorporates a small set of enhancements
to the earlier {\takegrant} model addressing both needs.  These
extensions bridge the gap from the {\takegrant} model to the behavior
of real systems.  Where the {\takegrant} model describes the behavior
of a hypothetical capability system, the {\dimtake} model describes
the behavior of several that have actually been implemented.  In
particular, it completely describes the access rights associated with
externalized memory management.

\section{Formal Verification}

The second contribution of this dissertation is \swmodel, a formal
model for a capability architecture that can be used to specify and
demonstrate the correctness of security policies, and a verification
of one security policy in particular: confinement.

\subsection{Confinement}

\index{confinement!informal discussion} Simply stated, the
\term{confinement property} \cite{Lampson:Confinement} asks whether a
program is able to disclose information.  If it cannot, the program is
said to be \term{confined}.  In practice, I will relax the test of
confinement to ask whether a program is able to disclose information
\emph{via unauthorized channels.}  Also, I will consider only overt
channels of communication.  The limitation of covert channel bandwidth
is beyond the scope of this dissertation.

\index{access rights!de jure} \index{access rights!de facto} \index{de
  jure access!first discussed} \index{de facto access!first discussed}
Confinement considers both \emph{de jure} and \emph{de facto} means of
information transfer.  \term{De jure} access exists if a program $p1$
can, by some sequence of operations, come to have explicit authority
to communicate directly with $p2$.  \term{De facto} access exists if
$p1$ and $p2$ can communicate indirectly.\footnote{The terms \emph{de
    jure} and \emph{de facto} as used here is due to
  \cite{Bishop:transfer}.}  If program $p1$ can write to a shared
memory region, and program $p2$ can read from that memory region, then
$p1$ has de facto write access to $p2$.  It can therefore disclose
information to $p2$ and is not confined.  Confinement also takes into
account the transfer of access rights.  If $p2$ can grant to $p1$ some
authority that allows $p1$ to disclose information to $p2$, $p1$ is
not confined.

\index{confinement!and nontrusted binaries} The confinement
restriction allows sensitive information to be handed to
non-inspectable (i.e. binary) code without risk of disclosure.
In addition, it allows non-trusted code to be executed within an
environment whose safety can be formally verified.  Finally,
confinement is a precondition to multilevel secure (MLS) systems; the
ability to construct leak free environments is a precursor to building
environments that leak only in controlled directions.

\subsection{Non-confinement of ACL-derived Systems}

\index{access control list!vs. confinement|(}
The confinement property cannot be enforced in ACL-derived systems.
It follows that such systems cannot enforce multilevel security
policies.

Let $P$ be the set of processes in a system, and $P_{fred}$ be the
subset of $P$ containing processes running on behalf of user $fred$.
Let $p_{restricted} \in P_{fred}$ be some process that should be confined,
and $p_{unsafe}$ be any other process.  In an access list system, it
is possible for $p_{unsafe}$ to create a new object $o$, and to grant
write access to $fred$.  Having done so, $p_{unsafe}$ has effectively
granted write access to all members of $P_{fred}$, and therefore to
$p_{restricted}$.

It follows immediately that $p_{restricted}$ cannot be confined unless
all processes having access to the object space reachable from
$p_{restricted}$ are trusted.  In UNIX systems, for example, this can
be approximated by means of the \emph{chroot} mechanism if prior
planning on the part of an administrator is feasible and carefully
executed.  The \emph{chroot} approach
is not satisfactory for downloaded active content or for independently
executed subprograms, and breaks down completely if networking
services are enabled.  Further, it does not interact well with ongoing
software maintenance.  The set of trusted programs is large, and
developers will inevitably make mistakes as they are modified over
time.

\index{Windows~95/NT, non-confinement} \index{VMS, non-confinement}
\index{MVS, non-confinement} Because the confinement property is not
enforceable within the ACL model, most widely deployed operating
systems -- including UNIX (or more generally, POSIX) systems,
Windows~95, Windows~NT, VMS, and MVS -- do not \emph{and cannot}
enforce this restriction.  For many protection systems, its
enforceability is undecidable \cite{Harrison:Protection}.  One
researcher has proven in \cite{Karger:Thesis} that it is impossible to
achieve in capability systems as originally defined by Van Horn in
\cite{Dennis:Capabilities}.\index{access control list!vs.
  confinement|)} 
This will be discussed further in Chapter~\ref{cha:related}.
Boebert et al. have shown that a closely related property known as
the \emph{*-property} similarly cannot be enforced by conventional
capability systems \cite{Boebert:star-property}.

\subsection{Formal Model}
In this dissertation, I identify a family of capability architectures
that \emph{do} enforce the confinement property.  Further, I will
present a proof of correctness, constructed in collaboration with Sam
Weber, of a set of preconditions under which confinement is assured.
By showing that the operational semantics of this family support the
access control model, and that the confinement requirement can be
enforced by that access control model, it is demonstrated that:

\index{formal verification steps}
\index{proof!steps}
\begin{enumerate}
\item The operational semantics is correct with respect to the access
  control requirements.
\item The rules by which the confinement policy is enforced are
  correct with respect to the access control model.
\item The specified system is \emph{in principle} able to enforce at
  least one non-trivial security policy.
\end{enumerate}
While several existing systems are believed to support the confinement
property in practice \cite{Karger:Thesis,KeyKOS:Factory,PSOS:Report}, no successful
proof of the rules for confinement in these systems has been shown to
date.

\section{EROS: A Capability System}

\index{capability system!performance issues|(}
The performance of protection mechanisms is as important as the
security policies they support.  If the capability model cannot be
reduced to practice in an implementation with reasonable performance,
it is not especially useful.  

The third contribution of this dissertation is a demonstration of how
the EROS architecture relates to the {\swmodel} formal model.  By
virtue of this mapping, the EROS design is in fact covered by both the
model and the proof.

The fourth contribution of this dissertation is EROS, a careful
reduction to practice of one member of the {\swmodel} family.  The
EROS implementation demonstrates that capability systems can deliver
monolithic performance in all of the areas that have traditionally
been bottlenecks for such systems.

As the architecture I will propose is both a capability architecture
and a dekernelized system architecture, it is useful to briefly
identify the shortcomings associated with both types of systems.

\subsection{Intrinsic Penalty}

\index{capability system!intrinsic penalty}
It should first be noted that there is no \emph{intrinsic} penalty
associated with the use of capabilities.  Without exception,
\emph{all} modern operating systems use some form of capability-like
descriptor to represent open files, networking, and interprocess
communication channels.  This is done because the cost of checking
access lists with every operation is prohibitive, and because such
objects must somehow be designated.

Performance issues begin to arise when this model is extended to cover
more primitive abstractions such as memory.  The main problem in this
area appears to be the prohibitive cost of indirect address
translation.

\subsection{Indirected Translation}

\index{capability system!indirect translation}
With the exception of KeyKOS \cite{KeyKOS:Architecture}, existing
capability architectures have suffered from excessively complicated
memory systems.  Many of these architectures were envisioned at a time
when the main memory to CPU speed ratio was 5:1.  
\index{HYDRA/C.mmp}
\index{i432}
\index{CAL/TSS}
HYDRA/C.mmp
\cite{Wulf:HYDRA}, Intel's i432 \cite{Organick:i432}, CAL/TSS, and
several other of these architectures \cite{Levy:CapabilitySystems}
incorporated features requiring multiply indirected address
translation.  Setting aside the implementation complexity of such
memory architectures, multiply indirected translation is clearly
infeasible when the main memory to CPU speed ratio now exceeds 1:40 in
commodity machines.

\index{KeyKOS}
KeyKOS adopts a more conventional hierarchically translated addressing
scheme. While a rough quantitative evaluation was performed
\cite{KeyKOS:NanoKernel}, no thorough quantitative evaluation was ever
performed.
\index{Mach!compared to KeyKOS}\index{KeyKOS!compared to Mach}
Rough benchmarks suggest that the C language
implementation of KeyKOS compared approximately to Mach
2.5, which is
now generally agreed to have been unacceptably slow
\cite{MACH4:Evolving}.
\index{capability system!performance issues|)}

\subsection{Persistence and Dekernelization}

\index{capability system!dekernelization|(}
\index{dekernelization performance|(}
A secondary performance concern devolves from the need for
persistence.  If the capability architecture stops at the secondary
storage boundary, two issues arise:

\begin{itemize}
\item Most of the security and reliability benefits of capability
  architectures are lost, as the security of the in-memory system is
  compromised by the security of the underlying store.
\item Assurance of secure system startup becomes exceedingly
  difficult, as the first few processes must be granted their
  authorities by some means outside the protection system.
\end{itemize}

The simplest resolution to these concerns is to render the entire
system (processes and all) persistent.  If this is done, runtime state
is not lost with system shutdown and the need to ensure the secure
reconstruction of runtime authority is eliminated.  This solution
carries with it two requirements:

\begin{description}
\item[\textbf{All storage is accountable.}]  
  \index{resource management}\index{storage accountability}
  Any storage allocated to
  a process must be backed by real, persistent storage that is
  recoverable in the event of failure.  Efficient allocation
  strategies for this storage are potentially complex and application
  specific, and should therefore be dekernelized.
\item[\textbf{Memory fault handling is dekernelized.}] Since the
  kernel consequently lacks the ability to resolve demand-allocation
  page faults, handling of such faults must be accomplished by
  non-supervisor code.
\end{description}

The Mach effort suggests that the external fault handling requirement
can be satisfied only at a significant performance penalty.
\index{Mach!default pager}Ultimately, this penalty led to the Mach
default pager being re-integrated into the kernel.
\index{Mach!compared to Chorus}This problem also existed in early
versions of Chorus \cite{Chorus:Overview}.  If storage
\emph{allocation} is also externalized, this can only exacerbate the
performance problems discovered in the externalized default pager.

\index{L3|see{L4}}
\index{Mach!fast IPC}
\index{L4!fast IPC}
In the
intervening years since KeyKOS and Mach 2.5, Ford
\cite{MACH4:Evolving}, and Liedtke \cite{L3:IPC,L3:SpaceMux} have made
remarkable progress in speeding up conventional microkernel designs.
My research goal became to learn if these advances could be applied
equally well to capability systems within the {\swmodel} family, and
specifically to EROS.

\index{capability system!persistent store design}
\index{resource management!accountability of storage}
In addition to issues
of memory fault handling, the practical implementation of persistence
requires that the number of primitive on-disk object types be
minimized.  This in turn encourages an architecture in which
abstractions such as memory regions and processes are crafted from
more primitive components.  Given this, I must verify that memory map
construction and process fabrication are not prohibitively expensive.
\index{capability system!dekernelization|)}
\index{dekernelization performance|)}

\subsection{Evaluation Criteria}

Given the concerns raised above, my evaluation of EROS uses
microbenchmarks to focus on three facets of system performance:

\begin{enumerate}
\item \textbf{Native Performance.}  How fast are the native mechanisms
  of the operating system, most notably the invocation of
  kernel provided services and the native interprocess communication
  mechanisms?
\item \textbf{Memory management performance.} In the particular area
  that has plagued previous dekernelized designs, how well does EROS
  do?
\item \textbf{Comparative performance.} To the extent that indicative
  comparisons are possible in an incomplete system, how does EROS fare
  when compared to a mature monolithic system: Linux?
\end{enumerate}
These measurements are the subject of Chapter~\ref{cha:eval}.

In general, the results show that EROS does quite well.  Only one
benchmark shows any significant loss of performance relative to
conventional designs, and this benchmark is not critical to native
application performance.  In most benchmarks EROS performance
significantly \emph{exceeds} that of Linux, most notably on a few
benchmarks that are exceptionally important for POSIX applications.

The final contribution associated with this dissertation is the EROS
system itself, which will be published with or shortly after the
acceptance of this dissertation.  

\section{Outline}

The balance of this dissertation proceeds as follows.
Chapter~\ref{cha:arch} gives an overview of the virtual machine
architecture shared by EROS and KeyKOS without reference to the
implementation.  Chapter~\ref{cha:system} describes the operating
system architecture that has constructed on this virtual machine.

With Chapter~\ref{cha:dimtake}, the dissertation switches to more
formal considerations, extending the capability access control model
to handle both indirection and transitive read-only authority.
Chapter~\ref{cha:model} defines \swmodel, a formal model for a family
of capability systems that includes EROS.  Chapter~\ref{cha:proof},
developed jointly with Sam Weber, provides a formal definition of the
confinement property and a proof that the {\swmodel} family can
enforce this constraint.

Chapter~\ref{cha:eros} presents a high performance reduction to
practice of the architecture described in Chapter~\ref{cha:arch}.  It
also identifies the execution paths that have turned out to be
critical to overall system performance.  Chapter~\ref{cha:os-impl}
describes the reduction to practice of the system components
identified in Chapter~\ref{cha:system}.  Chapter~\ref{cha:eval}
provides a quantitative evaluation of the resulting system, which
demonstrates the overall success of the EROS effort and one areas in
which the current design could be significantly improved; both with
little impact on the system overall.

Chapter~\ref{cha:related} describes related efforts, with particular
attention to those systems from which the EROS effort has borrowed.
Chapter~\ref{cha:conclusion} summarizes the results of this work and
offers suggestions for future work.

