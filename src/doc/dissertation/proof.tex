\chapter{Proof of Confinement}
\label{cha:proof}

Having described the {\swmodel} model, one can now explore the
question of whether {\swmodel} can enforce confinement, and if so,
what preconditions are sufficient to ensure this enforcement.  This
proof was jointly developed with Sam Weber of the University of
Pennsylvania.

\section{Informal Discussion of Confinement}

\index{confinement!informal discussion|(}
The confinement problem was first articulated by Butler Lampson in
1973 \cite{Lampson:Confinement}.  Allowing for evolution of system
architectures, the confinement problem is stated in
Definition~\ref{defn:ideal-confinement}.
\begin{ildefinition}[The Ideal Confinement Property]
  Given an initial system configuration, ensure that some process $p$
  does not modify the state of any entity in the system except by
  means of authorized channels.  A program so restricted is said to be
  \term{confined}.
  \label{defn:ideal-confinement}
\end{ildefinition}
A confined program is free to \emph{read} any state that it is
authorized to, but it may not disclose information to any unauthorized
party.

\index{confinement!and multilevel security}
Confinement is a precondition to multilevel security (MLS)
\cite{Bell:model:vol1,Bell:model:vol2,Bell:model:vol3}, and to
enforcing the requirements of lattice structured information flow
\cite{Denning:Lattice}.  Both MLS and lattice constrained flow may be
viewed as a statement of suitable rules for the choice of authorized
channels.  Unless the behavior of programs can be restricted to the
use of authorized channels (i.e. confined), policies for defining
these channels are premature.

\subsection{Enforcement of Confinement}
\index{confinement!enforcement} An enforcement mechanism for the
confinement property might be built by means of a \index{reference
  monitor}reference monitor.  Every object in the system is tagged as
``authorized'' or ``unauthorized,'' and writes to unauthorized objects
are made to generate exceptions.  Any process that attempts to mutate
an unauthorized object has violated the confinement property.

In practice, this approach is difficult to implement.  There may be
many independently confined subsystems, and the division between
authorized and unauthorized references can be altered dynamically in
ways that cause previously isolated subsystems to become
interconnected.  Transfers of authority have transitive effects in the
access graph.  In the limit, the reference monitor might be forced to
construct and maintain an $N \times N$ access matrix in which the
legal accesses between entities are recorded.

The principal advantage to the access matrix approach is that it is
dynamic. It checks access violations on the basis of what the program
actually does, rather than on the basis of what it \emph{might} do.
The principal \emph{disadvantage} is that it is prohibitively
expensive to implement.  While there are a variety of implementation
strategies for such an access matrix, all have unfortunate
consequences in both space and time.

\subsection{A Conservative Approximation}
\index{confinement!authorized channels}
The alternative strategy is to adopt a conservative approximation to
the ideal confinement property, given in
Definition~\ref{defn:conservative-confinement}. This is the definition
of confinement that we shall adopt here.
\begin{ildefinition}[The Conservative Confinement Property]
  Given an initial system configuration, ensure that some process $p$
  \emph{cannot} modify the state of any entity in the system except by
  means of authorized channels.  A program so restricted is said to be
  \term{confined}.
  \label{defn:conservative-confinement}
\end{ildefinition}

The principal focus of the proof is to show that any operation that can
be performed under Definition~\ref{defn:ideal-confinement} is permissible
under Definition~\ref{defn:conservative-confinement}.   To show this, we must
show that:
\begin{itemize}
\item There exists some initial configuration in which the mutations
  authorized for an allegedly confined process $p$ satisfied the
  confinement property (this is trivial).
\item For any initial configuration of an {\swmodel} system and any
  execution, all mutations performed were authorized.
\end{itemize}
There are two interesting complications in the second part:
\begin{itemize}
\item Access rights in a capability system are transferable, so
  operations that copy capabilities must be examined to see that the
  authorities transferred are suitably constrained.
\item The authority to perform interprocess calls implies the
  authority to reply, yielding a semi-reflexive access graph.
\end{itemize}
\index{confinement!informal discussion|)}

\section{Outline of Proof}
\label{sec:method}

\index{confinement!proof outline|(}
\index{proof!mutable set|defined}
\index{proof!readable set|defined}
Consider a {\swmodel} system having a set of processes $P$, in which
there is an allegedly confined process $p_{confined} \in P$.  Given
this configuration, we can generate two sets: the resources
that are mutable by $p_{confined}$, and the set of resources that are
readable by other processes in the system.  Let $C$ be the set of
capabilities initially held by $p_{confined}$, $C_{auth} \subseteq C$
the set of authorized capabilities whose resources $p$ may permissably
mutate, and $C_{outside}$ the set of all capabilities held by
processes in $P - \set{p_{confined}}$.
\begin{ildefinition}[Definition of Confinement]
\label{def:confinement}
A process holding initial
capabilities $C$ is confined if and only if:
\begin{equation*}
  (mutable(C) - mutable(C_{auth})) \intersect
  readable(C_{outside}) = \emptyset
\label{confine:static}
\end{equation*}
\end{ildefinition}

Given the access rights of \swmodel, it is trivial to show that
\begin{equation*}
   (\caprights{k} \subseteq \set{\indir{\nrtweak},\access{\nrtweak}})
   \implies mutable(\set{k}) = \emptyset
\end{equation*}
This gives rise directly to a straightforward conservative test for confinement.
Define
\begin{displaymath}
\begin{array}{rcl}
  C_{safe} & = & \set{k|k \in C \text{ and }
    \caprights{k} \subseteq \set{\indir{\nrtweak},\access{\nrtweak}}} \\
  C_{holes} & = & C - C_{safe} \\
\end{array}
\end{displaymath}
A system is confined if and only if $C_{holes} \subseteq C_{auth}$.

Given that the access rights enforce confinement under the stated
initial conditions, it remains to show that the operational semantics
of the system satisfy the requirements of the access rights.  
The goal is to
show that if the initial capabilities of a process are confined, no
subsequent execution can violate the confinement boundary.

\index{proof!mutated relation|defined}
\index{proof!accessed relation|defined}
To do this, we must define two relations $mutated(\cdot)$ and
$accessed(\cdot)$, which describe the resources that are actually
modified (resp. read) by processes proceeding by all possible stepwise
executions from the start condition. These are inductively defined
based on the operational semantics of \swmodel.  We must then show that the
dynamic behavior satisfies the constraints of the conservative analysis.
That
is, we must show that for all sets $C$ of capabilities:
\begin{eqnarray*}
  mutated(C) \subseteq mutable(C) \\
  accessed(C) \subseteq readable(C)
\label{confine:def}
\end{eqnarray*}

Since both $mutable(\cdot)$ and $mutated(\cdot)$ are empty for the
initially confined capabilities, the required conditions can be
simplified to:
\begin{eqnarray*}
  (mutated(C_{auth}) \subset mutable(C_{auth}) \\
  (accessed(C_{outside}) \subset readable(C_{outside})
\label{confine:simpledef}
\end{eqnarray*}
\index{confinement!proof outline|)}

\section{Verification}

\index{confinement!verification|(}
\index{proof!verification|(}
To state the EROS security requirements we must be able to determine
the set of entities that a given subsystem has mutated during the
course of an execution.  Similarly, we have to define the entities a
subsystem has read from.  We define a function {$\mutated{}{\cdot}$}
(Definition~\ref{def:mutated}) with the intended meaning that if {$ S_0
  \mv{\alpha_1} S_1 \mv{\alpha_2} \ldots S_n$} is an execution, and
{$E \subseteq \resource$}, then {$\mutated{E}{S_0\ldots S_n}$} is the
set of entities that have been affected by the entities {$E$} in the
execution.

\begin{ildefinition}[The \ensuremath{\mathbf{mutated(\cdot)}} relation]
\label{def:mutated}
If {$S_0 \mva{p_1}{\alpha_1} \ldots S_n$} is an execution,
{$E \subseteq \existed{S_0}$}, and
{$ e_i = S_0 \mva{p_1}{\alpha_1} \ldots S_i$} are subexecutions, then
\begin{equation*}
\begin{array}{rcl}
\mutated{E}{S_0} & = & E \\
\mutated{E}{e_1} & = &E\\
&&\union 
\begin{cases}
p &\text{if {$E \intersect \readfrom{\alpha} \not= \emptyset$}}\\
\emptyset& \text{otherwise}
\end{cases}\\
&&\union
\begin{cases}
\wroteto{\alpha} &\text{if {$p \in E$}}\\
\emptyset &\text{otherwise}
\end{cases}\\
\mutated{E}{e_n} &= &
\mutated{\mutated{E}{e_{n-1}}}
    {S_{n-1} \mva{p_n}{\alpha_n}S_{n}}
\end{array}
\end{equation*}
The definitions of {$\readfrom{\alpha}$} and {$\wroteto{\alpha}$} are
given in Definition~\ref{def:sw:readfrom}.
\end{ildefinition}

Intuitively, we treat mutation as a form of ``contagion'' that
propagates through the system.  If we are concerned with the subsystem
{$E \subseteq \resource$}, then initially just the members of {$E$}
are mutated.  However, if any mutated entity modifies another resource
(such as by calling it), then the other entity becomes mutated itself.
Also, if a resource happens to read information from a mutated
resource, the reader is mutated.  Once mutated, a resource is forever
mutated.  Mutation \emph{is} information flow.

\index{proof!wroteto|defined}
\index{proof!readfrom|defined}
We use the auxiliary definitions for {$\wroteto{\cdot}$} and
{$\readfrom{\cdot}$} which for each action indicate which resources
the executing process could have affected or been affected by,
respectively.  Their definitions are given in
Definition~\ref{def:sw:readfrom}.

\begin{ildefinition}[The Readfrom and Wroteto Relations]
  \label{def:sw:readfrom}
  \begin{equation*}
    \begin{array}{lll}
      \alpha & \readfrom{\alpha} & \wroteto{\alpha} \\ \hline\hline
      \at{refstart}{p} & {\set{\target{p}}} & - \\
      \at{rdwalk}{p,k} & {\set{\target{\stateref{S}(p)}}} & - \\
      \at{wrwalk}{p,k} & {\set{\target{\stateref{S}(p)}}} & - \\
      \at{execwalk}{p,k} & {\set{\target{\stateref{S}(p)}}} & - \\
      \at{read}{p} & {\set{\target{\stateref{S}(p)}}} & \set{\target{p}} \\
      \at{write}{p} & {\set{\target{p}}} & \set{\target{\stateref{S}(p)}} \\
      \at{fetch}{p,k} & {\set{\target{\stateref{S}(p)}}} & \set{\target{p}} \\
      \at{store}{p,k} & {\set{\target{p}}} &  \set{\target{\stateref{S}(p)}} \\
      \at{remove}{p,k} & {\set{\target{p}}} &\set{\target{\stateref{S}(p)}} \\
      \at{send}{p,a} & {\set{\target{p}}} & \set{\target{\stateref{S}(p)}} \\
      \at{call}{p,a} & {\set{\target{p}}} & \set{\target{\stateref{S}(p)}} \\
      \at{return}{p,a} & {\set{\target{p}}} & \set{\target{\stateref{S}(p)}} \\
      \at{create}{p,a} & \set{\target{p}} & -\\ 
      \at{destroy}{p}& {\set{\target{p}}} & -\\
    \end{array}
  \end{equation*}
\end{ildefinition}

\index{proof!read|defined}
The notion of a subsystem having read the values of other resources
can be considered the inverse of mutation: instead of information
flowing out from the subsystem in question, it is flowing inward.
Therefore, we define the {$\mathbf{read}$} function
(Definition~\ref{defn:read}) in terms of {$\mathbf{mutated}$}: if
{$x$} mutated {$y$}, then {$y$} must have read from {$x$}.

\begin{ildefinition}[The $\mathbf{read}$ Function]
  \label{defn:read}
  If {$e$} is an execution of a system whose initial state is 
  {$S_0$}, and {$E \subseteq \existed{S_0}$}, then
  \begin{equation*}
    \haveread{E}{e} = \set{x | \mutated{\set{x}}{e} \intersect E \neq \emptyset}
  \end{equation*}
\end{ildefinition}

\subsection{Access Control}

\index{proof!access control model}
\index{access control!SW model}
The EROS capability system defines a particular access control
mechanism.  This mechanism determines what information flow is
possible between system resources.  In this section we formalize this
mechanism, so that later we can verify:
\begin{itemize}
\item that these mechanisms correspond to the actual behavior of the
  system, and
\item that the algorithms used by the system services are correct with
  respect to the access control mechanism, in that they enforce the
  confinement policy.
\end{itemize}
We are particularly interested in deriving meaningful, conservative
approximations of the set of entities that a given subsystem might be
able to mutate and the set of entities that the subsystem might gain
information about.  Formally, if {$S \in \state$}, and {$E \subseteq
  \resource$}, the intended meaning of {$\mutable{S}{E}$} (resp.
{$\readable{S}{E}$}) is the set of entities that {$E$} directly or
indirectly mutates (resp. reads) from some execution beginning with
state {$S$}. Note that the mutable and readable functions are
parameterized by a single state -- the operating system has to be able
to make these judgments based only on the current system state, unlike
the requirements which can state what happens during an entire
execution.

\index{proof!weak access}
\index{access rights!weak}
The weak access restriction, while essential to the architecture,
introduces significant complication.  Intuitively, one can draw a
directed, labeled graph showing the relationship between all the
resources in the system.  Since all interactions between resources
occur via capabilities, and capabilities cannot be forged, a graph
traversal can be done to compute whether any given resources can
affect each other.  This is similar to a transitive closure, with the
following added complexities:
\begin{itemize}
\item The resource relationships are restricted by capability
  rights.
\item Weak capabilities modify the attributes of the capabilities that
  are fetched through them.
\item Some capabilities result in two-way interactions between
  resources, while others are one-way.
\end{itemize}

We will represent the relationship between two resources as an element
in a complete partial order (Definition~\ref{def:cpo-attr}).  This partial
order is complete by virtue of the restriction imposed in
Section~\ref{sec:rights}.
As notation, let {$\top = \emptyset \in \accrightd$}.  If {$ A \subseteq
  \accrightd$}, let the least upper bound of {$A$}, {$\lub{A}$}, be
defined in the usual manner.
\begin{ildefinition}[The complete partial order of access rights]
\label{def:cpo-attr}
Let {$\accrightd$} be the cpo where {$\accrightd = \accright \union \set{\bot}$}
and {$\leq$} is defined by
\begin{align*}
\bot \leq x & \text{ \ \ \ }\forall x \in \accrightd\\
x \supseteq y \implies x \leq y & \text{ \ \ \ }\forall x,y \in \accright
\end{align*}
\end{ildefinition}

\index{proof!access relationship}
The access relationship from {$x_1$} to {$x_2$} is the least upper
bound of the attributes of the capabilities that {$x_1$} might be able
to obtain to {$x_2$}.  This relationship is \emph{not} symmetric.
This representation relies on the fact that if {$k_1$} and {$k_2$} are
capabilities which are identical except for possibly having different
attributes, then {$\caprights{k_1} \leq \caprights{k_2}$} means that any
operation that can be performed using {$k_1$} can also be done using
{$k_2$}.

\index{proof!direct access relation|(defined}
We first define the \emph{direct access relation}
(Definition~\ref{defn:diracc}) between resources, which describes the
relationship between resources in a particular state.  Using this, we
will define the \emph{potential access relation} which defines the
relationships that might exist in the future.

There are two oddities in the definition of {$\dar{S}$}.  First, if
{$n \in \resource$} has a start or resume capability {$k$}, then {$n$}
is related to {$\target{k}$}, despite the fact that only a process can
invoke such a capability.  Second, this relationship is symmetric:
{$\target{k}$} is also related to {$n$}.  The intuition behind this is
that if {$x$} is related to {$y$}, then any process that has access to
{$x$} can obtain access of the appropriate kind to {$y$}.  If {$x$}
has a start (or resume) capability to {$y$}, then a process that can
access {$x$} might \emph{call} that capability, leaving {$y$} with a
resume capability back to that process.  Thus, a start or resume
capability means that there is an implicit backwards relationship from
{$y$} to {$x$}.
\begin{ildefinition}[Definition of $\dar{S}$]
  \label{defn:diracc}
  If {$S\in \state$}, then we define 
  {$\dar{S}:\resource\cross\resource \rightarrow \accrightd$} by
  \begin{equation*}
    \dar{S}(x,y) =  \lub{\set{a| (x,y,a) \in \mathbf{DASet}}}
  \end{equation*}
  where
  \begin{equation*}
    \begin{array}{ccll}
      \mathbf{DASet} = & & \set{(c,\target{k}, \caprights{k})
        &| c \in \object , 
        k \in \stateobj{S}(c)} \\
      &\union & \set{(\target{k},c,\top) &| c \in \object,
        k \in \stateobj{S},\\
      & & & k \in \startcap \union \resumecap}\\
    \end{array}
  \end{equation*}
\end{ildefinition}
\index{proof!direct access realtion|)}

\index{proof!potential access relation|(defined}
We construct {$\potacc{S}$} (Definition~\ref{defn:potacc}) by using every capability indicated in
{$\dar{S}$} to fetch every possible capability from {$\dar{S}$},
obtaining a new, stronger relationship, and then repeating.
\begin{ildefinition}[Definition of $\potacc{S}$]
  \label{defn:potacc}
  If {$S \in \state$}, then the potential access relation, {$\potacc{S}$} is
  the limit of the series {$T_0, T_1, T_2, \ldots$} where
  \begin{align*}
    T_0 &= \dar{S}\\
    \forall x,y \text{ \ \ \ }T_{i+1}(x,y) &= \lub{\set{T_i(x,y),
        \mathbf{combine}(T_i)(x,y)}}\\
  \end{align*}
  If {$A$} is a {\swmodel}\ resource relationship, then
  {$\mathbf{combine}(A):\resource\cross\resource \rightarrow \accrightd$} is
  defined to be:
  \begin{equation*}
    \mathbf{combine}(A)(x,z) = \lub{\set{a| \exists y\in \resource 
        \text{ such that } a = \mathbf{transAccess}(x,y,z)}}
  \end{equation*}
  where
  \begin{equation*}
    \begin{array}{ll}
      \mathbf{transAccess}(x,y,z) = & \lub{\set{a| \exists ry \in A(x,y), rz
          \in A(y,z) \\
          & \text{ such that } a = \mathbf{transRights}(ry, rz)}}\\
    \end{array}
  \end{equation*}
  and
  \begin{gather*}
    \mathbf{transRights}(r1,r2) = \begin{cases}
      \set{r2} & \text{ if } r1 = \indir{\nrtweak} \text{ and }
      r2 \in \set{\indir{\nrtweak},\access{\nrtweak}}\\
      \set{r2} & \text{ if } r1 = \indir{\nrtread} \text{ and }
      r2 \in \set{\indir{\nrtread},\access{\nrtread}}\\
      \set{r2} & \text{ if } r1 = \indir{\nrtwrite} \text{ and }
      r2 \in \set{\indir{\nrtwrite},\access{\nrtwrite}}\\
      \set{r2} & \text{ if } r1 = \indir{\nrtexec} \\
      & \text{ and }
      r2 \in \set{\indir{\nrtexec},\access{\nrtexec}}\\
      \set{r2} & \text{ if } r1 \in \set{\access{\nrtread},\access{\nrtexec}}\\
      \set{\indir{\nrtweak}} & \text{ if } r1 = \indir{\nrtweak} \text{ and }
      r2 = \indir{\nrtread}\\
      \set{\indir{\nrtweak}} & \text{ if } r1 = \indir{\nrtread} \text{ and }
      r2 = \indir{\nrtweak}\\
      \set{\access{\nrtweak}} & \text{ if } r1 = \indir{\nrtweak} \text{ and }
      r2 = \access{\nrtread}\\
      \set{\access{\nrtweak}} & \text{ if } r1 = \indir{\nrtread} \text{ and }
      r2 = \access{\nrtweak}\\
      \bot   & \text{ otherwise}
    \end{cases}
  \end{gather*}
  
  The definition of {$\potacc{S}$} is well defined.  That is, the
  sequence {$T_0, T_1, \ldots$} converges.
\end{ildefinition}
\index{proof!potential access relation|)}

\index{proof!mutable relation|defined}
\index{proof!readable relation|defined}
Finally, with {$\potacc{S}$} we can define {$\mathbf{mutable}$} and
{$\mathbf{readable}$} (Definition~\ref{def:mutable}).
\begin{ildefinition}[The \ensuremath{\mathbf{mutable(\cdot)}} and
  \ensuremath{\mathbf{readable(\cdot)}} relations]
\label{def:mutable}
If {$S \in \state$}, then
\begin{align*}
\mutable{S}{E} & = \set{y|\exists x \in E, \potacc{S}(x,y) \subseteq
  \set{\access{\nrtweak}}}\\
\readable{S}{E} & = \set{x| E \intersect \mutable{S}{\set{x}} \neq \emptyset}
\end{align*}
\end{ildefinition}

\subsection{Verification Proof}
\label{sec:proof}

We can now state the major theorem
(Theorem~\ref{swthm:mutablevsmutated}).  In any execution of the
system, anything that was actually mutated or read by a subsystem was
considered mutable or readable by the operating system.  A subtle
point now arises. During the execution objects and processes may have
been created.  Since the operating system had no way of anticipating
such creations, it couldn't have stated anything about the effects of
these objects and processes.

In EROS, new objects (including processes) can only be created by
existing processes and can only have a subset of the authority of
their creators.  For this reason, we can restrict our consideration to
only those resources that existed at the time the
{$\mathbf{mutable}/\mathbf{readable}$} relations were computed: new
processes cannot have any additional power than those that existed
previously.

\index{proof!system state}
We first define the set of resources that exist at a given state of the
system, and then use this to state our main theorem.
\begin{ildefinition}[Definition of $\existed{S}$]
  If {$S$} is a state, then {$\existed{S}$} is defined to be the following
  subset of {$\resource$}:
  \begin{equation*}
    \existed{S} = \resource - \process \union \stateexist{S} \union \statedead{S}
  \end{equation*}
\end{ildefinition}
\begin{theorem}[Main verification theorem]
  \label{swthm:mutablevsmutated}
  If {$ S_0 \mv{\alpha_1} S_1 \dots \mv{\alpha_n} S_n$}
  is an execution, then for any 
  {$ E \subseteq \existed{S_0}$},
  \begin{gather*}
    \mutated{E}{S_0 \mv{\alpha_1} \ldots S_n} \intersect \existed{S_0}
    \subseteq \mutable{S_0}{E} \\
    \haveread{E}{S_0 \mv{\alpha_1} \ldots S_n} \intersect \existed{S_0}
    \subseteq \readable{S_0}{E}
  \end{gather*}
\end{theorem}

The principal lemmas that allow us to prove this theorem are shown in
Principal~Lemmas~\ref{swlemma:mutabledecreases}
and~\ref{swlemma:onestepmutated}.  These lemmas state the essential
properties of EROS which account for its security.  The
\index{proof!authority reduction lemma} \textbf{Execution Reduces
  Authority} lemma states that barring outside intervention, the
authority that a subsystem can obtain only decreases during an
execution: the subsystem can lose capabilities, but cannot create any.
The \textbf{Mutation Implies Mutable} lemma states that if a resource
becomes mutated by an operation, then it must have previously been
mutable.
\begin{princlemma}[Execution Reduces Authority]
  \label{swlemma:mutabledecreases}

  If {$ S_0 \mva{p}{\alpha} S_1$}, then for all {$E$},
  \begin{equation*}
    \mutable{S_1}{E} \intersect \existed{S_0}
    \subseteq \mutable{S_0}{E \intersect \existed{S_0}}
  \end{equation*}
\end{princlemma}
\begin{princlemma}[Mutation Implies Mutable]
  \label{swlemma:onestepmutated}

  If {$ S_0 \mva{p}{\alpha} S_1$}, then for all {$E$},
  \begin{equation*}
    \mutated{E}{S_0 \mva{p}{\alpha} S_1} - E \subseteq
    \mutable{S_0}{E}
  \end{equation*}
\end{princlemma}
Both of these lemmas are properties of single step transitions, and
can therefore be shown to hold by straightforward case analysis.  With
these properties, the main theorem follows.
\begin{proofoutline}[Main Theorem]
We proceed by induction on the value of {$n$}.  The base case is
trivial.
\\
In the induction step, let {$e_i$} denote the execution 
{$S_0 \mva{p_1}{\alpha_1}\ldots \mva{p_i}{\alpha_i} S_i$}.
Assume that for all sets {$F$},
\begin{equation*}
\mutated{F}{e_{n-1}} \intersect \existed{S_0} \subseteq
 \mutable{S_0}{F}
\end{equation*}
We want to show 
\begin{equation*}
\mutated{E}{e_n} \intersect \existed{S_0}
 \subseteq \mutable{S_0}{E}
\end{equation*}
This follows because:
\\
\\
\begin{tabular}{lllp{1.45in}}
\multicolumn{4}{l}{$\mutated{E}{e_n} \intersect \existed{S_0}$} \\
\hspace{.15in} & $=$ &
     $\mutated{\mutated{E}{e_{n-1}}}{S_{n-1} 
                 \mva{p_{n}}{\alpha_{n}}S_n} \intersect \existed{S_0}$
               & \\
  & $\subseteq$ & $(\mutable{S_{n-1}}{\mutated{E}{e_{n-1}}}
                     \union \mutated{E}{e_{n-1}})$
         & \emph{by Lemma~\ref{swlemma:onestepmutated}} \\
  & &  $\hspace{4em}\intersect \existed{S_0}$
              & \\
  & $\subseteq$ & $(\mutable{S_{n-1}}{\mutated{E}{e_{n-1}}}
                     \union \mutable{S_0}{E})$
              & \emph{induction hyp.}\\
  & &  $\hspace{4em}\intersect \existed{S_0}$
              & \\
  & $\subseteq$ & $((\mutable{S_0}{\mutated{E}{e_{n-1}}) \intersect \existed{S_0}}$
              & \emph{by Lemma~\ref{swlemma:mutabledecreases}}\\
  & &  $\hspace{4em} \union \mutable{S_0}{E}) \intersect \existed{S_0}$
              & \\
  & $\subseteq$ & $\mutable{S_0}{\mutable{S_0}{E}} \union
  \mutable{S_0}{E}$
              & \emph{induction hyp., \hfill \break $mutable$() is}\\
  & $=$ & $\mutable{S_0}{E}$ & \emph{monotonic by}\\
  &  &  & \emph{definition}
\end{tabular}
\\
\\
The proof that {$\mathbf{read}$} and {$\mathbf{readable}$} are related
follows easily from the definitions.
\end{proofoutline}
\index{confinement!verification|)}
\index{proof!verification|)}

\section{Discussion}

Certain aspects of the verification proof are not apparent from the
formal content.

\leadin{Correspondence.} Any operation that can be performed in EROS
can be performed in \swmodel.  {\Swmodel} meets the security
requirements. The behavior of {\swmodel} is a superset of EROS's
formal behavior.  Therefore we are assured that EROS also meets the
security requirements.

\index{proof!non-modeled state}
For example, reading a page in EROS produces the value that was last
written.  \Swmodel, however, does not keep track of the contents of
pages -- pages hold user level data and {\Swmodel}'s security relies only
on kernel data.  This is safe, since it \emph{increases} the possible
behavior of the system, and has the advantage that we do not need to
keep track of the page values.  Also, we demonstrate that the security
of EROS does not rely upon this aspect of its behavior.

\index{proof!requirements statement}
\leadin{Requirements Statement.} The definition of the confinement
requirement is independent of both the access control model and the
{\swmodel} operational semantics.  Failing to preserve this
independence would result in a meaningless verification: ``EROS
provides the security that results from its security policy.''

A confined process should not be able to affect any non-authorized
entities outside the confinement boundary.  Given an execution {$e$}
and a set of entities {$E$} we want to define a function
{$\mutated{E}{e}$} whose value is the set of entities that were
affected by {$E$} in the execution.  By doing so, we state what the
communication channels of the system are.

The {$\mathbf{mutated}$} relation is transitive. Suppose a process
{$p$} modifies a resource {$x$}, and another process {$q$}
subsequently reads {$x$}.  In this sequence of events, {$p$} has
mutated {$q$}, even though the two processes never directly
communicated.  Our definition of {$\mathbf{mutated}$} takes this form
of indirect communication into account.

\index{proof!access control}
\index{constructor!access rights check|(}
\leadin{Access Control.} The confinement mechanism defines a security
policy that is assured by the EROS (and {\Swmodel}) access control model.
While the confinement requirements are stated in terms of the
execution of the system, the security policy must be statically
verifiable.  By looking only at a small portion of the current system
state, the constructor must be able to judge the confinement of its
products.

The function {$\mutable{S}{E}$} takes a single state {$S$} and from
that computes the set of all the entities that might in the future be
mutated by the entities {$E$}.  Although this function is not actually
computed by the operating system (for obvious efficiency reasons), it
is the basis for the access checks performed by the operational
semantics.  The check performed by the constructor therefore provides
a conservative verification of the requirements.
\index{constructor!access rights check|)}

\index{proof!correctness}
\leadin{Proof of Correctness.}
Finally, we have to show that the system's security policies implement
its requirements.  For example, we must show that if {$e$} is an
execution of the system whose initial state is {$S_0$}, and if {$E$}
is any set of resources, then
\begin{equation*}
\mutated{E}{e} \subseteq \mutable{S_{0}}{E}
\end{equation*}
In other words, if some entity was mutated in an execution, then the
security policy must have said that it was originally mutable.  This
statement relies upon properties of both the operational semantics,
and the security policy.  One of the aims of this work is to state
these properties in such a way as to be applicable to other
situations.

\section{Closing Comments}
\index{constructor!relation to verification proof|(}
It is useful to ``close the loop'' on the theoretical portion of this
dissertation by relating its results back to the EROS constructor
(Section~\ref{sec:constructor}) and the hole check that the
constructor performs.

\index{proof!and EROS constructor}
The constructor builds a new process {$p$} from a set $C$ of initial
capabilities held by the constructor.  As part of the construction
request, the client of the constructor passes in a set $C_{auth}$ of
\emph{authorized holes}.  The constructor considers a capability
``safe'' if it is inherently discrete (base case) or it is a
constructor capability to a frozen constructor whose yield is confined
(inductive case).  Discrete capabilities prohibit write access, and
(by virtue of the $\weaken{\cdot}$ operation) prohibit the retrieval
of any capability that might \emph{permit} write access.  In short,
discrete capabilities are those which will add no elements to
{$\mutable{}{p}$}.

By means of the \textbf{process tool} it is possible for one
constructor to identify another.  The constructor itself is trusted
software.  Once it has been established that a capability is a start
capability to a constructor, that capability can safely be invoked to
determine if the constructor is frozen, and also whether the yield of
the constructor is confined.

The end result is that arbitrarily complex, confined subsystems may be
built by means of the constructor mechanism.  Further, these
subsystems may share (in read-only fashion) resources (e.g., program
images) with subsystems that are \emph{not} confined.
In practice, EROS constructors are sufficiently useful that they have
become the standard means of instantiating new programs.  Not all
programs in EROS are confined, but \emph{most} are.  Notable
exceptions are programs such as device drivers that speak to the
outside world.
\index{constructor!relation to verification proof|)}

