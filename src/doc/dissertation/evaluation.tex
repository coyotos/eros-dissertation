\part{Evaluation}
\chapter{Quantitative Evaluation}
\label{cha:eval}

\index{KeyKOS!relationship to EROS}
\index{EROS!relationship to KeyKOS}
\index{POSIX!compatibility box}
The EROS architecture is a refinement of the KeyKOS architecture,
which is in turn an outgrowth of the GNOSIS\index{GNOSIS} design, a system that
originated at Tymshare Inc.  GNOSIS was originally motivated by the
need for a robust, secure platform for reliable timesharing services.
A ``compatibility box'' for IBM's OS operating system (and later for
POSIX) was incorporated from the beginning.

\index{Orange Book|see{Rainbow Series}}
\index{Rainbow Series}
Multiuser operating systems designed along the lines of the so-called
``rainbow series'' \cite{DoD:Orange} security requirements tacitly
assume that the authorized user base is a ``mostly collaborative''
community, and that the security goal is therefore to keep
unauthorized parties from gaining access to the machine.  As a time
sharing service provider, Tymshare was concerned both with the
reliabilty of their service and with the fact that a given machine's
user base did not come from a single community.  Users from competing
companies are adversaries; GNOSIS sought to deal with providing
security in an environment of what was euphemistically termed
``mutually suspicious users.''

\index{EROS!application focus}
While the EROS work has continued in this vein, the focus has changed.
EROS is targeted at mission critical, sensitive or
performance intensive service provisioning.  Such servers run a small
set of mission critical applications, porting server applications is a
viable option if a significant benefit can be achieved in performance
or uptime.  This is especially true of ``thin servers,'' where much of
the value proposition derives from the fact that the platform is a
closed, self-maintaining system.

\index{POSIX!compatibility}
\index{POSIX!benchmark}
\index{benchmark!POSIX}
That said, the multi-platform source level compatibility provided by
POSIX means that running POSIX targeted application is an important
consideration for a services platform.  The increasing need for
compatibility places significant performance pressure on the design,
but also makes the need for fault and information containment all the
more important.  If mission critical applications are to share
computing resources with relatively less trusted or less robust
applications, fault isolation becomes an essential operational
requirement.


%%% Computers have become significantly more powerful since the GNOSIS
%%% system was first conceived, to the point where servers now run a broad
%%% spectrum of applications and therefore can no longer be treated as
%%% closed systems.  


While the rising popularity of ``architecture neutralizing
platforms''\index{architecture neutralizing platform}
\index{Java}
such as Java may significantly alter the picture, the resulting design
assumption for EROS is hybrid usage: most applications will run in one
or more functionally enhanced compatibility environments while a few
critical applications will be ported to the native platform.  Security
and isolation are provided both by partitioning the critical services
from the non-critical applications and by running multiple, mutually
fault isolated compatibility environments.  To evaluate the
performance of the EROS system, then, both native and compatibility
benchmarks must be considered.

\section{Evaluation Method and Environment}

\index{EROS!evaluation method|(}
The EROS measurements presented here are taken from a dual processor
200 Mhz Pentium Pro machine based on Tyan Inc.'s Titan-Pro
motherboard. In all measurements
presented here, the second processor is disabled.  The Titan-Pro
motherboard is based on Intel's i82371 PIIX (Triton) chipset.  The
machine is configured with 32 megabytes of 60ns DRAM and a 512
kilobyte sync-burst cache module.  The lmbench utilities report memory
latencies of 15ns, 60ns, and 200ns to the level one, level two, and
main memories respectively.

All of the benchmark figures reported here are in-memory benchmarks.
The speed of the PCI bus, its associated controller hardware, and the
underlying disk system therefore do not play any role in the results
reported here.

\index{Linux}
Where Linux benchmarks are given, the system measured is the RedHat
4.2 system running the 2.0.30 Linux kernel.  At the time of this
writing, the 2.2.0 kernel was just being released and is not yet
widely deployed.

\index{lmbench}
The measurement methods borrow calibration and evaluation methods
from lmbench-2alpha11 \cite{McVoy:lmbench}.  One change was made
throughout: EROS measurements make direct use of the hardware cycle
counter for timing rather than the POSIX time() system call. Given the
number of iterations used in the benchmarks, the change in the cost of
the timing call itself should have no impact on the numbers.

In general, care has been taken to ensure that evaluation and
comparison of the results can focus on the differences in semantics
between the two systems and the putative equivalence of the mechanisms
compared rather than on the measurement techniques used.

None of the benchmarks reported here (for either EROS or Linux) take
advantage of the fast system call trap provided by later Pentium Pro
and Pentium II cores.

\section{Native Performance}
\index{EROS!performance!bottlenecks|(}
\index{benchmark!EROS|(}
The bottlenecks for native execution are:
\begin{itemize}
\item \emph{Valid address translation}. This measures how quickly EROS
  memory objects can be converted into native mapping tables that are
  directly interpretable by the hardware.  Valid address translation
  determines the efficiency with which two processes that share a
  memory region may establish coordination of their mapping structures
  so as to make use of each other's address validations (e.g., multiple
  threads, shared libraries).  It also indicates the efficiency with
  which one process that receives a memory object from another may
  validate and use that object, which covers most of the uses of
  so-called ``temporary files'' in conventional systems.
\item \emph{Kernel capability invocation}. While it is unusual for
  EROS applications to directly invoke kernel implemented
  capabilities, many of the supporting services provided by the EROS
  operating system must do so.  The performance of such invocations is
  therefore of some concern.
\item \emph{Interprocess invocation performance}. EROS relies
  heavily on dividing applications into small processes to provide
  fault isolation and exploit request parallelism.  Also, interprocess
  communication plays a role in essentially every resource allocation
  operation.  As such, the speed with which information can be
  transmitted from one process to another is a vital concern.
%%% \item \emph{Space allocation}. Since EROS does not have
%%%   kernel allocated storage, all requests for storage allocation must
%%%   be directed to a user level storage manager.  The performance of
%%%   this storage manager is therefore critical to nearly all
%%%   applications.
\item \emph{Heap and Stack Allocation}. In EROS, as in other systems,
  application startup is largely dominated by the rate at which
  demand-zero and demand-copy page faults can be satisfied.
\item \emph{Process Creation}. EROS processes take on many of the
  attributes of objects in other systems, and may be created
  dynamically while processing requests.  While persistence eliminates
  the startup and teardown serialization costs for processes that
  mediate databases or similar \index{reference monitor}reference
  monitors, many services that would be constructed using shared
  libraries in other systems are constructed using processes in EROS.
\end{itemize}
Each of these operations needs to be measured.  

\index{persistence!checkpoint}Because the EROS system provides
transparent persistence with negligible ambient overhead, native
services are generally \emph{not} critically dependent on the
performance of bulk data serialization and deserialization (i.e. file
I/O) \emph{except} where interprocess communication is concerned.  The
two have dramatically different performance envelopes, and EROS
performs very well in the one that matters.

The following subsections cover operations that either have no
equivalent in UNIX systems or deserve detailed discussion.  The
remaining native operation evaluations are deferred to the POSIX
comparison section, as POSIX provides a useful reference for
evaluating the meaning of these results.

\subsection{Address Validation}
\label{sec:addr-validate}

\index{benchmark!EROS!address validation|(}
\index{memory!address translation!performance|(}
EROS address spaces are simply memory objects.  The performance with
which native page table entries can be constructed for in-core memory
objects is therefore a performance critical path.  To evaluate this
path, a large memory object is constructed and validated.  Its
mapping table entries are then explicitly invalidated (by hand), and a
timed pass is made summing the first word of each page.

\subsubsection{Building Page Table Entries}

The ``full reconstruction'' result in
Table~\ref{tab:valid-addr-construction} includes the time of the
summation loop, the page table entry construction, and (where needed)
the page directory entry construction.  Because the summation loop
loads the first line of each page, it self-contends in both the
primary and secondary caches. As a result, the cost of summation
accounts for a sizable portion (3.3\%) of the total time reported.
While this overhead is essentially constant across operating systems,
it dominates the benchmark in the most common case.

\begin{table}[htb]
  \begin{center}
    \begin{tabular}{llll}
      \textbf{Item} & \textbf{2048 Pages} & \textbf{Per Page} \\
      Linux v2.0.30 & 249.85 ms & 122 $\mu$S \\
      EROS -- Full Reconstruction & 16.54 ms & 8.08 $\mu$S  \\
      EROS -- Normal Behavior & 597.74 $\mu$S & 0.29 $\mu$S \\
      EROS -- Summation loop without faults& 566 $\mu$S & 0.27 $\mu$S\\
    \end{tabular}
    \caption{Address validation latency (200Mhz Pentium Pro)}
    \label{tab:valid-addr-construction}
  \end{center}
\end{table}

\subsubsection{Recovering Page Tables}
The full reconstruction benchmark measures the cost of constructing
mapping table entries from an in-core memory object, but does
\emph{not} accurately characterize the typical cost of sharing or
remapping that object.  The EROS implementation goes to considerable
lengths to preserve mapping tables and to reuse these tables when
possible.  EROS applications are expected to take steps to choose
suitable mapping boundaries for mapped objects.  Unless otherwise
directed, the linker relocates large programs such that code and
mutable data fall in different page tables, with the result that code
mappings can be shared (and rapidly reconstructed) across all copies
of the application.

One consequence of this care in reuse is that unmapped memory objects
do not lose their page tables if the object has been mapped at a
natural mapping boundary.  The page table remains bound to the memory
object, and when remapped will be reused by the kernel.  A more
representative measurement of typically expected performance is
therefore shown in the ``normal behavior'' result in
Table~\ref{tab:valid-addr-construction}.  This result includes the
time of the summation loop and the time to rediscover and re-map the
page directories for the object.

Given the latency of demand-zero page fault handling (200Mhz Pentium
Pro: 69.07 $\mu$S), I was initially puzzled at the apparently higher
latency to revalidate page tables (200 Mhz Pentium Pro: 1024 entries
per table times 0.29 $\mu$S gives 297 $\mu$S).  The reason proves to
lie in the cost of the summation loop itself.  Demand-zero operations
are quite fast on both the Pentium Pro and Pentium II processors
\emph{if} the zeroed memory is aligned to a cache line boundary and a
multiple of the line size; the processor takes advantage of these
properties to avoid loading the associated cache lines from main
memory.  \index{memory!address translation!performance|)}
\index{benchmark!EROS!address validation|)}

\subsection{Kernel Capability Invocation}
\label{sec:kerncap}

\index{benchmark!EROS!kernel capability invocation|(}
The kernel capability invocation test measures the basic overhead of
the capability invocation and dispatch logic of the EROS kernel.  This
number provides a \emph{floor} for the cost of kernel invocations.  To
test this, the ``report capability type'' request is made of a number
capability.  The latency of this operation (200Mhz Pentium Pro) is 3.69
$\mu$S.  This compares favorably with the best tuned variant (Linux) of
the equivalent UNIX path, which takes 3.00$\mu$S.

The kernel capability invocation cost is actually somewhat
\emph{higher} than the minimal interprocess invocation cost.  There
are three reasons for this:
\begin{itemize}
\item Interprocess invocation is an extremely important case and has
  been carefully hand optimized.
\item The general invocation path must handle a number of
  possibilities that do not occur in the fast IPC path.
\item The x86 is has a very small number of registers, with the
  consequence that much of the state managed by the general path
  cannot be kept in registers by the compiler.
\end{itemize}
\index{benchmark!EROS!kernel capability invocation|)}

\subsection{Interprocess Invocation}
\index{interprocess communication!EROS|(}
\index{benchmark!EROS!interprocess invocation|(}

Interprocess invocation is by far the most frequently performed
capability invocation in a working EROS system.  In practice, such
invocations may be divided into two categories:
\begin{itemize}
\item Control transfer invocations conveying a minimum of state that
  can be transferred via registers.
\item Bulk data movement, in which data is transferred using the
  optional string argument to the IPC invocation and metadata is
  conveyed in registers.  Data transfer in such invocations is
  generally one way; the response invocation typically carries
  only a status code.
\end{itemize}

\subsubsection{Control Transfer IPCs}
\index{memory!small address spaces}
EROS implements variable size address spaces using segment enforced
protection in order to improve performance.  In consequence, there are
two categories of transfer to consider: transfers that require a
modification of the address space register and transfers that do not.
In addition, it is useful to explore how these transfers compose,
because the heap extension operation relies on this composition.

To measure these, three benchmarks have been constructed.  The first
performs a call and return between two large spaces.  The second
performs a call from a large space to a small space that returns to
the large space.  The third test examines transitivity: large space A
to small space to large space B to small space and back to large space
A.  No data string is transferred in these tests.  The results are
shown in Table~\ref{tab:ipc-nostring-costs}.
\begin{table}[htb]
  \begin{center}
    \begin{tabular}{llll}
      & \textbf{Round-Trip} & \textbf{L1 Miss} & \textbf{L2 Miss} \\
      \textbf{Test case} & \textbf{Latency} & \textbf{Equivalents} & \textbf{Equivalents} \\
      Large-Large & 7.16 $\mu$S & 120 & 35 \\
      Large-Small & 5.60 $\mu$S & 93 & 28 \\
      Large-Small-Large & 15.05 $\mu$S & 251 & 75 \\
    \end{tabular}
    \caption{Interprocess control transfer latency}
    \label{tab:ipc-nostring-costs}
  \end{center}
\end{table}

It has become customary in IPC performance measurements to
report one-way latencies by dividing round trip latencies in two.  By
this metric the EROS results are quite good, but the resulting
measurement is misleading.  It is very rare for a client to call a
service without obtaining some reply.  As such, the round trip latency
more accurately captures the true overhead of interprocess
invocation.  For purposes of comparative cost evaluation, the table
also shows the number of L1 misses and L2 misses that could be
processed in the same amount of time.

\subsubsection{Bulk Data IPCs}

Bulk data transfers move strings from one process to another.  The
EROS capability invocation interface permits up to 65536 bytes to be
moved in a single invocation.  The cost of small transfers is
dominated by the cost of context switching, but larger transfers are
dominated by the cost of the copy itself.
Figure~\ref{fig:ipc-bulk-latency} compares the cost of the EROS IPC
operation to the cost of a \emph{bcopy}() operation moving the same
number of bytes.
\begin{figure}[htb]
  \begin{center}
    \leavevmode
    \psfig{figure=ipc.lat.eps}
    \caption{EROS bulk data IPC latency}
    \label{fig:ipc-bulk-latency}
  \end{center}
\end{figure}

%%% \subsection{Storage Allocation}
%%%
%%% Pages: 7697, of which 6028 is tree manip.
%%%
%%% Nodes: 7696, of which 6028 is tree manip.
\index{interprocess communication!EROS|)}
\index{benchmark!EROS!interprocess invocation|)}
\index{EROS!performance!bottlenecks|)}
\index{benchmark!EROS|)}

\section{POSIX Comparison Measurements}

\index{POSIX!benchmark|(}
\index{benchmark!POSIX|(}
The construction of a POSIX compatibility box remains future work.
Until this is done, a direct comparison using established POSIX
benchmarks remains infeasible.  What \emph{can} be done at this stage
is to determine what operations are performance critical in a POSIX
implementation, implement these operations using EROS objects and
programs, and measure the resulting implementations.  While less than
perfect, this comparison provides a strong indicator of the
performance that might be expected from a complete POSIX compatibility
implementation.

\index{lmbench}
\index{benchmark!lmbench}
The \emph{lmbench} \cite{McVoy:lmbench} benchmark suite was
constructed by observing performance critical operations in real POSIX
applications and constructing microbenchmarks that measure the
performance of these operations.  By constructing EROS implementations
of these services and performing equivalent measurements, a reasonable
first order indicator of EROS ``compatibility box'' performance on
these operations can be obtained.

Speaking broadly,
the lmbench suite can be divided into a few categories;
\begin{itemize}
\item \emph{System} benchmarks, which evaluate the performance of the
  underlying machine.  These facilitate comparative evaluation across
  implementations of the same system architecture.  While I report
  these numbers as a calibration of the hardware used in the
  evaluation, I will not consider them further.  All benchmarks shown
  here were run on the same machine, and as such the hardware
  performance is a neutral factor in the comparison.
\item \emph{Network} benchmarks, which expose how effectively the
  measured operating system interacts with other systems.  To date,
  EROS has no networking implementation, and the absence of this
  comparison is by far the most important shortcoming in the current
  evaluation.
\item \emph{POSIX} benchmarks, which explore the performance of
  various POSIX operations that have proven critical in existing
  applications.  Modeling these operations is the principal focus of
  the benchmarking effort.
\end{itemize}
In the discussion that follows, I will focus on several of the POSIX
benchmarks, and more specifically on the subset of these that are
in-memory benchmarks.  A comparison of network performance would also
be appropriate, but as noted, EROS does not yet have a TCP/IP
implementation with which to compare.

Of the in-memory POSIX benchmark set, the critical measurements are
those that examine interprocess communication speed, process creation,
page fault handling, and file creation.  For each of the POSIX
abstractions tested by these benchmarks, I have constructed
equivalent EROS abstractions and benchmarks.  
%%% The resulting comparison
%%% is imperfect; these numbers may be viewed as a qualitative indicator
%%% of what performance can be realized from the POSIX implementation when
%%% it is completed.
\index{Mach!external pager|(}
In addition to these benchmarks, I have assembled a benchmark that
examines the cost of growing the heap.  The last is \emph{not} a
critical metric in current POSIX systems, but has proven to be a
source of serious performance deficiencies in previous dekernelized
systems such as Mach.  It is therefore important to demonstrate that
the EROS implementation is ``fast enough.''

\subsection{In-memory Benchmarks}

Table~\ref{tab:in-mem-benchmarks} shows the in-memory POSIX benchmarks
and their EROS equivalents.  Each of these benchmarks is explained
below.

\begin{table}[htb]
  \begin{center}
    \begin{tabular}{lll}
      \textbf{Benchmark} & \textbf{Linux (2.0.30)} & \textbf{EROS}\\
      Trivial system call & 3.00 $\mu$S & 3.68 $\mu$S \\
      Null I/O & 4.20 $\mu$S & 4.03 $\mu$S \\
      Page validation & 122 $\mu$S & 8.08 $\mu$S \\
      Heap growth & 77.90 $\mu$S & 69.07 $\mu$S \\
      Pipe Latency & 25.00 $\mu$S & 18.65 $\mu$S \\
      Pipe Bandwidth & 96 Mbyte/sec & 114.60 Mbyte/sec \\
      Directed Context Switch (0k) &  5.05 $\mu$S & 3.58 $\mu$S (Large-Large) \\
      Process Creation & 3.99 ms & 2.79 ms \\
    \end{tabular}
    \caption{In-memory POSIX benchmarks}
    \label{tab:in-mem-benchmarks}
  \end{center}
\end{table}

\subsubsection{Trivial System Call}

\index{benchmark!POSIX!trivial system call|(}
The trivial system call benchmark measures the cost to enter the
kernel and demultiplex the requested operation.  In the Linux test,
the system call used is \emph{getppid}(), which cannot be implemented
by a user level library.  The EROS test, as previously described in
Section~\ref{sec:kerncap}, invokes a zero number capability to obtain
its type.
\index{benchmark!POSIX!trivial system call|)}

\subsubsection{Null I/O}

\index{benchmark!POSIX!null I/O|(}
The trivial system call benchmark measures the cost to enter the
kernel and demultiplex the requested operation.  The distinction
between this test and the trivial system call test is that this test
must traverse the respective object location logic of the two
systems.  The test then performs an operation that the object system
can legitimately optimize in such a way as to avoid performing any
actual data motion.

In the Linux test, the operation performed is a zero-length read on a
test file.  The EROS test performs a zero-length read on a page
capability.
\index{benchmark!POSIX!null I/O|)}

\subsubsection{Page Validation}
\index{benchmark!POSIX!page validation|(}
The page validation test measures the cost to construct mapping table
entries for objects whose state is already in memory.   The Linux
version of this benchmark creates a small file (small enough to stay
in memory), memory maps this file, unmaps it, remaps it, and then
makes a pass to sum the data in the file.

The equivalent EROS benchmark constructs a memory object and inserts
this memory object into the benchmark process' address space, which is
a large address space.  For reasons previously described in
Section~\ref{sec:addr-validate}, simply unmapping and remapping the
object does not provide a directly comparable test because the page
tables containing the object mappings are not actually destroyed.  The
number reported in the table above is the number obtained by
explicitly tearing down the memory object sufficiently to destroy
\emph{all} mappings.

The resulting comparison is heavily biased against EROS.  In practice,
a memory object that was intended for reuse would not be explicitly
torn apart in this fashion.  Table~\ref{tab:true-addr-validate} shows
a more realistic picture of the performance that might be expected
from the EROS implementation in practice.
\begin{table}[htb]
  \begin{center}
    \begin{tabular}{llll}
      \textbf{Benchmark} & \textbf{Linux (2.0.30)} & \textbf{EROS
      (teardown)} & \textbf{EROS (actual)} \\
      Page validation & 122 $\mu$S & 8.08 $\mu$S & 0.29 $\mu$S\\
    \end{tabular}
    \caption{Page validation latencies}
    \label{tab:true-addr-validate}
  \end{center}
\end{table}
\index{benchmark!POSIX!page validation|)}

\subsubsection{Heap Growth}

\index{benchmark!POSIX!heap growth|(}
The heap growth benchmark measures the cost to grow a memory region by
a single page.  In both POSIX and EROS systems, this operation is most
commonly induced by calls to \emph{malloc}() that in turn call the
\emph{sbrk}() system call.  An equivalent effect is seen when the
stack must be grown downwards by one page.

POSIX systems typically satisfy heap growth requests by allocating a
page from the swap area and appending this page to the end of the
memory object.  The page in question is known to be initially zero, so
no disk I/O is required to allocate it.  An in-memory bitmap of
allocated swap pages must be updated to indicate that the
corresponding swap page is now allocated.

The equivalent EROS operation is considerably more complicated in its
implementation.  When the page fault occurs, the kernel redirects the
fault to a memory keeper, which in turn buys the necessary storage
from a space bank.  In order to implement space bank destruction, the
space bank tracks storage on a per-object basis in a binary tree
structure.  The space bank returns the requested storage to the
keeper, which places it in the memory object and returns control to
the user application.  The figure reported is for a heap growth page
fault taken by a large address space.
\index{Mach!external pager|)}
\index{benchmark!POSIX!heap growth|)}

\subsubsection{Pipe Latency and Bandwidth}
\index{benchmark!POSIX!pipe measurements|(}

The pipe latency test measures the speed with which data can be
inserted into a pipe.  In EROS, a pipe equivalent is constructed using
an intervening process.  The essential moral of this story is that
pipe buffers larger than half the L1 data cache size are
counterproductive; the cost of cache misses considerably outweighs the
cost of context switching \emph{if} the context switch implementation
is fast enough.

The pipe bandwidth test measures the rate at which data can be moved
through a pipe from one process to another.  The difference between
the two numbers is almost entirely due to the difference in buffering
and context switch overheads.
\index{benchmark!POSIX!pipe measurements|)}

\subsubsection{Directed Context Switch}

\index{benchmark!POSIX!context switch|(}
The context switch test measures the speed with which control can be
transferred from one process to another.  For directness of
comparison, the EROS figure shown is for a context switch between
large spaces; Linux does not implement any equivalent to small spaces.

The number reported for EROS is simply half of the zero payload IPC
number reported in Table~\ref{tab:ipc-nostring-costs}.
\index{benchmark!POSIX!context switch|)}

\subsubsection{Process Creation}

\index{benchmark!POSIX!process creation|(}
The process creation benchmark examines the cost to start a new
process running a different binary image than the creator.  In POSIX,
this is accomplished by a \emph{fork}() call followed immediately in
the child by a call to \emph{exec}().  The EROS implementation is to
request that a constructor instantiate a new copy of the desired
program.

Note that this is \emph{not} a direct comparison, in that the
operations are not done in the same way.  The EROS approach requires
coordinated interaction between four processes (constructor, process
creator, space bank, and benchmark program), and the program
constructed validates a considerable number of pages within its
address space before control is returned to the caller (more space
bank calls).  The number shown should therefore considerably
\emph{exceed} the cost of implementing \emph{fork}+\emph{exec} in a
POSIX emulator.  Individually fast operations do not always compose
gracefully to yield fast implementations of higher level abstractions.
The key point here is to note that the EROS operations compose well.
\index{benchmark!POSIX!process creation|)}

\subsection{File Latencies}

\index{benchmark!POSIX!file creation and I/O|(}
Files do not play as important a role in EROS as they do in Linux; the
equivalent usage is confined to data serialization for purposes of
interchange.  To approximate the cost of file manipulation, a ``file
system'' object was implemented that exports objects whose semantics
is essentially that of traditional files.  The resulting latencies are
shown in Table~\ref{tab:file-latency}.

\begin{table}[htb]
  \begin{center}
    \begin{tabular}{lll}
      \textbf{File Size} & \textbf{Linux (2.0.30)} & \textbf{EROS} \\
      0 Kbytes & 74.00 $\mu$S & 52.05 $\mu$S \\
      1 Kbytes & 80.58 $\mu$S & 95.50 $\mu$S \\
      4 Kbytes & 88.81 $\mu$S & 114.00 $\mu$S \\
      10 Kbytes & 105.37 $\mu$S & 221.84 $\mu$S \\
    \end{tabular}
    \caption{File creation latencies}
    \label{tab:file-latency}
  \end{center}
\end{table}

These results are the only significantly disappointing results
discovered so far.  There are two reasons for the performance shown.

%%% I am unable to account for these results, and need to
%%% look into them further.  Once initialized, the 0 Kbyte file creation
%%% logic should take something closer to 7 $\mu$S, with the 1K, 4K, and
%%% 10K cases taking 23.43 $\mu$S, 33.27 $\mu$S, and 97 $\mu$S
%%% respectively.  My present suspicion is that unnecessary memory
%%% allocation is being done by the file system implementation, which
%%% would fully account for the discrepancy.

The main source of degradation is that the string transfers are not
making use of the fast path.  A bug in the current implementation is
causing a page table entry on the recipient side of the transfer to
become invalidated. The effect of this bug is that the fast path
invokes the slow path in order to get the page table entry
reconstructed.  The resulting difference in performance accounts for
nearly all of the high overhead shown in this benchmark.  Note that
this bug does \emph{not} invalidate the basic premise; the remaining
benchmarks clearly demonstrate that bulk data transfers with
adequate bandwidth are possible.


\index{interprocess communication!need for scatter/gather}
The second issue is structural.  Because EROS lacks a scatter/gather
mechanism in its IPC system, the EROS implementation must copy the
data twice: once into the receive buffer of the file system
implementation and a second time into the ``official'' locations
within the file system object.  The corresponding Linux benchmark
measures the time to copy the data to the buffer cache, from which
(hopefully) the data will be directly copied by DMA to the device.
The discrepancy in performance shown here suggests strongly that data
cache contention is imposing significant marginal cost.

%%% \section{Performance Challenges}
%%% EROS is a unique system in several regards.  First, it uses
%%% capabilities pervasively for all system resources down to the page
%%% level, and provides no other mechanism for naming or protection.
%%% Capability systems have a regrettably well deserved reputation for
%%% poor performance.

%%% Second, the EROS virtual machine is policy neutral except where the
%%% constraints of security dictate otherwise.  A considerable amount of
%%% existing literature suggests that exporting policy decisions to user
%%% level results in serious performance bottlenecks, both because of the
%%% privilege crossings necessitated by kernel encapsulation of resources
%%% and also because of the cost of context switches between policy
%%% managers and the processes they manage.  In the area of memory
%%% management, EROS extends user managed policy beyond fault handling to
%%% primitive storage allocation, and therefore introduces an
%%% \emph{additional} layer of such costs not attempted by previous
%%% systems.

%%% The resulting system arguably should suffer from the collective
%%% disadvantages of both capabilities and microkernels.  Preliminary
%%% benchmarks give strong indication that it does not.  On the contrary,
%%% these preliminary measurements suggest that EROS-based systems should
%%% achieve parity with systems such as Linux in many operations, and
%%% exceed their performance in certain areas.
\index{benchmark!POSIX!file creation and I/O|)}
\index{EROS!benchmarks|see{benchmark}}
\index{benchmark!POSIX|)}
\index{POSIX!benchmark|)}

\section{Closing Comments}

EROS delivers considerably better performance than any previous
capability system for which measurements are available.  This is
because the EROS objects and protection architecture map directly to
the entities supported by the hardware, which is a significant
departure from previous systems.

With the benefit of hindsight, the omission of scatter/gather from the
EROS IPC mechanism was a mistake.  Both in the file system benchmark
and in partially completed user level networking implementations, the
absence of a scatter/gather mechanism proved to be a source of
significant frustration.  It is now well established that network
protocol implementations are heavily degraded by marginal data copies.
The latest generation of the EROS IPC design anticipates this change,
and will support its compatible introduction when time permits.

The main point established by these measurements is that EROS offers
acceptable performance on three criteria: its native primitive
operations are quite fast, they compose acceptably to provide
acceptably fast higher level services for native applications, and
with the exception of file I/O they are good enough to construct
emulations that significantly outperform the equivalent \emph{native}
abstractions in other systems.

