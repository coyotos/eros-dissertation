\chapter{The EROS Operating System}
\label{cha:system}

\index{EROS!operating system|(defined}
Having described the EROS virtual machine, I now turn to the
description of a prototype operating system built on top of this
virtual machine.  The core of this operating system provides the
following services:
\begin{itemize}
\item A trusted storage manager (the \term{space bank}).
\item A trusted mechanism for process fabrication and teardown (the
  \term{process creator}).
\item A trusted mechanism for creation of confined processes (the \term{constructor}).
\item Common memory fault handling services, which are trustworthy by
  virtue of the previous two mechanisms (e.g., the \term{virtual copy
    space keeper}).
\end{itemize}
In some sense, these services provide only the skeleton of an
operating system.  The current prototype omits some obvious components
required for any real implementation.  These include:
\begin{itemize}
\item \emph{Terminal handling}.  A tentative line discipline
  implementation exists, but has not been adequately tested.
\item \emph{User authentication}.  A mechanism must be constructed to
  authenticate users and re-connect them to their sessions.
\item \emph{Operating environment}.  
  \index{KeyKOS!operating environments}
  Some sort of common ``operating
  environment'' providing utility programs and the like must be
  constructed.  Sufficient progress has been made on a UNIX emulator
  to know that this can be done in EROS, and several operating
  environments were completed for KeyKOS, but no complete operating
  environment exists for the current implementation.
\item \emph{Database system}.  A native database implementation would
  be extremely useful.
\item \emph{Network protocol implementation}.  Some means to talk to
  the outside world is clearly desirable.
\end{itemize}
I draw attention to these omissions to make clear what has \emph{not}
yet been done in connection with the EROS effort, and note that all of
these services existed in EROS's predecessor, KeyKOS.

\section{Bootstrap and Persistence}

\index{EROS!bootstrap|(}
\index{bootstrap|(}
A significant (and deliberate) omission in the design of the EROS
virtual machine is any provision for bootstrap.  There exists no
mechanism within the virtual machine to establish an initial running
system image.  This design decision was inherited from KeyKOS, and
grew out of early work in GNOSIS\index{GNOSIS} (the predecessor to KeyKOS) to
establish that the system startup procedure could be rendered secure.

\subsection{Phases in the Security Argument}
\index{EROS!security analysis phases|(}Security properties of the
remainder of the system are straightforward to establish once an
initial arrangement of objects and services has been successfully
loaded and arranged (the ``primordial objects'').  Security analysis
for a system can therefore be divided approximately into three stages.

\subsubsection{Bootstrap Analysis}
\index{bootstrap!secure}The bootstrap analysis first asks whether the
initial conditions of the machine are well defined and adequately
constrained at the time the operating system first gains control of
the machine.  This involves a considerable and difficult analysis of
the hardware, and examination of the small bit of code that
constitutes the disk bootstrap loader.  This analysis phase is outside
the scope of this dissertation, but has been addressed elsewhere by
Arbaugh \cite{Arbaugh:secure-bootstrap}.  Disk bootstrap loaders are
quite small, and exhaustive inspection of the loader itself is
therefore practical.

\subsubsection{Setup Phase Analysis}
The setup phase is the phase during which the operating system creates
a primordial system image from which the balance of the system's
execution proceeds.  This commonly includes creating initial
process(es), starting authentication agents, and verifying the
consistency of disk data structures.  Security analysis of this phase
is concerned with showing that the primordial arrangement is
successfully established.  The execution analysis then endeavors to
show that this arrangement, once established, enforces the desired
security properties.

\subsubsection{Execution Phase}

Proceeding from a given a set of primordial conditions, execution
phase security properties are more easily
established.\footnote{Typically, execution phase security properties
  are established by demonstrating some fatal flaw in the primordial
  conditions, at which point further evaluation becomes moot.}  If the
virtual machine is suitably designed and the conditions of the
primordial system are carefully and correctly structured, many
execution phase security properties can be established using
operational semantic arguments that proceed from the primordial state
of the system.
\index{EROS!security analysis phases|)}

\subsection{Eliminating the Setup Phase}
\index{persistence!boot process}
Setup phase correctness arguments are considerably more difficult than
execution phase correctness arguments.  During the setup phase,
certain authorities must be granted to the primordial components by
fiat. Care must then be taken to ensure that these authorities are not
mishandled, and that they arrive correctly at the intended authority
levels by the time the execution phase begins.  The problem is that
these initial grants cannot be justified using the protection
mechanisms of the system itself.

EROS circumvents the need for setup phase arguments by implementing a
persistent virtual machine.  The primordial system image is hand
crafted and loaded onto a distribution disk or CD.  This primordial
system is loaded by the bootstrap loader when it first runs and is
subsequently cloned onto the target system disk.  The setup phase
itself is not performed by the system. As a result, the need for
security analysis concerning the establishment of the primordial image
at system startup is eliminated; the problem is reduced to ``Given
this initial image, does the system enforce its security contract?''

\subsubsection{Construction of Primordial Image}
\index{KeyKOS!primordial image construction}
The KeyKOS and GNOSIS implementations constructed an in-memory
primordial system image by means of special logic incorporated in the
IPL kernel.  After boot, the kernel would relocate this in-memory
image and write it to the disk.  Collectively, this process was known
as the ``big bang'' \cite{KeyKOS:NanoKernel}.
  
\index{EROS!primordial image} \index{object!primordial image} In
performing EROS experiments, it was convenient to be able to construct
experimental system images which, for example, did not include a
storage allocator.  EROS therefore replaces the big bang process with
a ``linking loader'' that accepts programs and descriptions of the
relationships between these programs written in an image description
language.  From these, the image compiler automatically crafts a disk
image reflecting this image description.  This facilitates the
construction of ``lean'' systems in which dynamic storage allocation
is not required, or in which constrained storage availability may lend
itself to simpler system implementations, e.g., of storage allocators
or fault handlers.\footnote{The EROS storage manager is designed to
  handle terabytes of disk storage, but runs in a 4 gigabyte address
  space.  This makes the code and the associated data structures
  considerably more complex than one might prefer.  In, say, an
  environment limited to less than a gigabyte of storage a completely
  different storage management design would be more appropriate.}

This solution incidentally suggests a solution to a question that has
long plagued cosmologists.  From the perspective of a running EROS
system, the universe has always existed in some configuration.

\subsubsection{Disk Consistency and Persistence}
\index{persistence!and disk reliability}
The other half of the setup phase problem is ensuring that disk data
structures are consistent.

Modern disk drives provide sufficiently good checking of sector level
reads that corruption of data during the disk write phase will
generally be detected during a subsequent read.  Disk level I/O
failure is therefore not addressed by EROS.\footnote{There is a real
  and valid concern, however, that the \emph{bussing} to these
  reliable disks is subject to a measurable (and uncomfortably high)
  error rate due to lack of checking on the bus.  EROS computes
  in-memory checksums on its state that could be used to catch such
  errors, but the current implementation does not store the resulting
  values to the disk.}

In the absence of low level disk errors, disk inconsistencies can only
arise if inconsistent data is written to the disk.  In most operating
systems, there exist windows of vulnerability during which a hard
reset of the machine may result in a disk image that violates the
consistency requirements of the operating system.  UNIX, for example,
frequently reports link count errors resulting from the fact that the
inode link count field and the directory update are not performed in a
single transaction.  If a reset occurs between the two updates, the
count must be corrected by running a consistency checker that knows
the low level file system structure (the \emph{fsck} program).  In the
face of the multithreaded nature of high performance file system
implementations, the recovery strategies used by such utilities are
necessarily heuristic and ad hoc.

\index{KeyKOS!checkpoint mechanism}
\index{EROS!checkpoint mechanism}
\index{checkpoint mechanism}
\index{persistence!checkpoint mechanism}
EROS, KeyKOS, and GNOSIS choose to eliminate this problem by
transparently transacting all modifications of the disk.
Periodically, a snapshot is taken of the system and written
asynchronously to a write-ahead log on the disk.  When the entire
snapshot has been written, a single header block is updated to
indicate that the stabilization transaction has completed.  If the
system fails, it resumes from the most recently stabilized snapshot,
which is consistent by virtue of transacted construction.

\subsection{The Primordial System Image}

\index{EROS!primordial image|(}
\index{object!primordial image|(}
Figure~\ref{fig:primordial} shows a typical primordial operating
system image suitable for the EROS virtual machine.  This image
consists of a minimum set of primordial services, a mechanism for
obtaining virtual copy (i.e. copy on write) address spaces, and
mechanisms for obtaining instances of other services (the ``typical
service constructor,'' which is replicated for each service included
in the primordial image).
\begin{figure*}[htb]
  \begin{center}
    \leavevmode
    \psfig{figure=primordial-image.eps}
    \caption{Primordial system image.}
    \label{fig:primordial}
  \end{center}
\end{figure*}
Provided that the six objects in the minimal configuration exist in
the arrangement shown, it is \emph{possible} for the system software
to implement a system that will not improperly disclose information.
\index{EROS!primordial image|)}
\index{object!primordial image|)}
\index{bootstrap|)}
\index{EROS!bootstrap|)}


\section{Storage Management}
\label{sec:spacebank}

\index{object!space bank|see{space bank}}
\index{space bank|(defined}
\index{resource management|(}
An EROS system contains some number of disks that are divided into
\term{ranges}.  Each range is made up of page frames.  Each page frame
contains either a data page, a capability page, or a block of nodes.
Each of these objects is named by a unique object identifier, which is
divided into a frame identifier and an object index relative to the
containing frame.  Frame identifiers are sequentially numbered within
a particular range.

Allocation and deallocation of objects is the job of the \term{space
  bank}.  On request, a space bank will allocate data pages,
capability pages, or nodes out of unused storage and return the newly
allocated objects to the requestor.\footnote{The space bank is
  expected to remember which disk frames are in use, and will not hand
  out space that is already allocated.  Management of per-frame
  tagging is handled by the virtual machine implementation.}  A space
bank will also deallocate any storage that it has allocated.
Limits may be imposed on the total storage that a particular bank will
allocate.

\subsection{The Prime Bank}
\index{space bank!prime bank}\index{prime space bank|see{space bank,
    prime bank}}All ranges are initially owned by a distinguished
space bank known as the \term{prime space bank}, which wields the
superrange capability.  This bank is part of the primordial system
image, and is ultimately responsible for all of the storage in the
system.\footnote{In personal discussions, Norm Hardy has proposed a
  design to meet more stringent security requirements in which storage
  would be segregated for reasons of physical security by having
  multiple ``prime'' banks.}

Every space bank with the exception of the prime bank has a parent
space bank from which it was created and from which its storage is
obtained.  Storage allocated from a given bank is also allocated from
each of its parents in upward-recursive fashion.  Each space bank keeps
track of the objects that it has allocated.  On destruction of the
bank, storage allocated by that bank is either deallocated (returned
to the free pool) or is inherited by the bank's parent.

\subsubsection{Mutual Authentication}
\index{space bank!authentication}
One potential security attack is to compromise the provider of
storage.  If an attacker is able to contrive to hand you a storage
allocator that they control, the content of any storage it allocates
is obviously compromised.  To preclude this attack, space banks wield
the process tool by special dispensation, and will use this
service to identify other space banks as authentic.  The prime bank
implements a distinguished interface that does not permit allocation
or deallocation, but will perform the \term{identify bank}
operation.  This interface is made generally available by the system
software to facilitate bank authentication.

\subsection{Indirect Implementation}

\index{space bank!and indirect invocation}
\index{capability!invocation!and space bank}
In the typical case, there is a one to one relationship between
objects and processes in an EROS system.  While the EROS interprocess
invocation mechanism is fast, it is not so fast that one wants to
perform recursive invocations up the bank tree in order to allocate
storage.  To avoid this, the space bank implementation takes advantage
of indirect invocation (Section~\ref{sec:call-indirection}).  All
banks are actually implemented by a single process, and this process
manages the upward recursion internally.

This implementation makes space banks relatively light weight (a
single node must be allocated per bank), and processes may allocate
storage from multiple banks.  Space banks can therefore be created as
part of a storage management strategy, and/or to segregate ephemeral
allocations from long term allocations.  A server handling multiple
clients, for example, can establish a separate bank for each client
and use this bank for all memory allocation related to that client.
When the client declares that its activities are completed, the
per-client bank can be destroyed, deleting all of the storage
associated with that client in a single operation.

\index{space bank!and ephemeral storage}
A similar strategy can be used for optimistic ephemeral allocation.
Consider constructing a bitmap under conditions of bounded memory.
The bitmap construction code can allocate all bitmap related storage
from a dedicated bank.  If the bitmap construction proceeds
successfully, this storage can then be inherited upward into the main
storage pool of the program.  If the bitmap construction fails for
lack of storage, the bank and its storage can be destroyed without
explicitly deconstructing the memory object.  This design is
particularly useful when programming in source languages that make use
of exception handling.
\index{resource management|)}
\index{space bank|)}

\section{Process Creation}
The second primordial service is the creation of processes.  

\index{capability!process tool}
As described in Section~\ref{sec:proc-tool}, processes may be
fabricated by any program wielding the process tool.  Since
the EROS virtual machine includes no process specific data structures,
there is no need to be concerned with resource allocation for process
state; this is accomplished by the space bank when the nodes that
constitute the process are allocated.

Surprisingly, the process tool's amplify operation does not present a
security concern as long as wielders take care not to reveal the brand
capabilities that they have used.  In spite of this, the process tool
is not generally available in the EROS operating system.  Instead,
processes are created and destroyed by objects known as \term{process
  creators}.

\subsection{The Process Creator}
\index{process!creation!process creator|(defined}
\index{object!process creator|see{process creator}}
Process creators are the only objects in the EROS operating system
that create processes using the process tool.  Every process creator
possesses a unique \term{brand} that it applies to all of the processes it
creates.  In practice, this brand is simply a distinguished start
capability to the process creator itself.  Process creators perform
three operations: create process, identify process, and destroy
process.

\subsubsection{Create Process}
The \term{create process} operation takes a space bank as an argument,
allocates the required nodes from this bank, creates a process
capability to the root node, and returns this capability to the
caller.

\subsubsection{Identify Process}
\index{process!identification}
\index{process!authentication}
The \term{identify process} operation answers whether a particular
process was created by this process creator.  The process creator
accepts a process, start, or resume capability and uses the
process tool to amplify this capability into a node
capability.  If the amplify operation succeeds, the process
creator says ``yes,'' otherwise it says ``no.''  This operation is
used by constructors (Section~\ref{sec:constructor}) to authenticate
that a process actually implements the service it claims to provide.

\subsubsection{Destroy Process}
\index{process!destruction}
The \term{destroy process} operation uses the process tool
and the process creator's brand to fetch a node capability to the
caller's process root node.  It disassembles the process and returns
its storage to the specified space bank.  Finally, it returns to a
capability provided by the caller.

The destroy process operation resolves a somewhat thorny problem
relating to process storage allocation: the constituent nodes and
pages of a process must be returned to a space bank, but if this is
done incautiously the process will cease to exist and will therefore
be unable to return to the party that destroyed it.  Further, the
process must not (in general) be able to obtain a node capability to
itself, because it would thereby be able to extract its own brand
capability and forge other processes that would appear to be created
by the same process creator.

Instead, the process is given a capability to its own process creator.
When the process is asked to self destruct, it tears down all of its
state except for a last small portion, and then invokes its process
creator.  The process creator takes responsibility for returning its
remaining storage and returning a result to the process that requested
the destruction.
\index{process!creation!process creator|)}

\subsection{The Process Creator Creator}
\index{process!creation!process creator creator|defined}
Because process creators must be used to create processes, and because
unique brands for different kinds of processes are helpful, it is
desirable to have a ready source (storage permitting) of process
creators.

The \term{process creator creator} (PCC) is a program that accepts a
space bank and builds a new process creator from that bank.  PCC is
the source of all process creators in the EROS operating system.  Much
as the process creator provides destruction services for its products,
PCC provides destruction services for process creators.

\section{Service Fabrication}
\label{sec:constructor}

\index{process!constructor|see{constructor}}
\index{object!constructor|see{constructor}}
\index{constructor|(defined}
Processes created by the process creator are well-formed and have a
preset brand, but little else.  In particular, they have no address
space.

In principle, any program may use a process creator to build a
process, populate this process with an address space, and perform a
\textbf{call} or \textbf{send} operation to initiate this new process.
In practice, it isn't generally done this way, for two reasons:
\begin{itemize}
\item Processes require a certain amount of environment setup before
  they can usefully execute.  For example, they usually want a copy on
  write address space.
\item Such a process cannot be authenticated by its user.
\end{itemize}
Both of these issues are addressed by the constructor.

\subsection{Constructors}

A \term{constructor} is a program that builds other programs.
Initially a constructor is \emph{unfrozen}, and may be populated by
the holder with capabilities that serve as a template for the process
that the constructor will create.  While in this state, the
constructor is also told the initial program counter at which the
process should begin execution.  Once the program has been populated,
the constructor is \emph{frozen}, after which the constructor will
no longer accept capabilities to be placed in the process.

\index{constructor!frozen}
Frozen constructors will create new instances of the program whose
template has been loaded into them.  This instance is referred to as
the \term{yield} of the constructor.  Every constructor possesses an
associated process creator dedicated to that constructor.  Using this
process creator, the constructor is able to say whether some
particular process was created by this constructor.  Frozen
constructors can also examine the capabilities contained in their
template and say whether any of these capabilities constitute
``holes'' through which information may be leaked.  If a capability is
either discrete (in the sense of discrim, Section~\ref{sec:discrim}),
or is a capability to a frozen constructor whose \emph{yield} is
discrete, it is not a hole.  All other capabilities are holes.

This mechanism provides a means by which clients may obtain services
whose external communications are controlled exclusively by the client
program.  It raises two additional issues: constructor authentication
and mutable address spaces.

\subsection{Constructor Authentication}

\index{constructor!authentication} Constructors will tell requestors
that their yield is discrete.  To know if the answer can be believed,
both constructors and constructor clients must have the means to ask
``Is this object a constructor?''

As with process creators, all constructors originate from a common
object.  This object is known as the \term{metaconstructor}, and is
considered a constructor by special dispensation (the code of the
metaconstructor and the constructor programs differ only
superficially).  Because it is the constructor of all constructors,
the metaconstructor answers the question ``Is this your yield?''
affirmatively when it is handed a process that is a constructor.  The
metaconstructor capability is generally available, and can therefore
be used to authenticate constructors.

\subsection{The Protospace}

\index{constructor!protospace}\index{protospace|see{constructor,
    protospace}}The problem with the hole check as described is that
it is very stringent.  In particular, if the template held by the
constructor contains a mutable address space, it will not pass the
hole check.  Because the initial address space image may be retained
by the author of the program, writes to the initial address space must
be considered a potential source of information leaks.  Unfortunately,
very few programs can operate without a mutable address
space.\footnote{This is especially true on the x86, which has only
  seven usable registers.}

\index{virtual copy!constructor}\index{constructor!virtual copy
  constructor}The solution to this problem is to provide a
\term{virtual copy constructor} (Section~\ref{sec:virt-copy-space})
for the initial address space rather than the initial address space
itself.  This constructor possesses a read-only, weak capability to
the initial address space image of the program.  A weak, read-only
capability is discrete, and therefore passes the discretion check.
The virtual copy address space constructor therefore passes the
discretion check, so any program built using this constructor is
discrete.  The problem is then reduced to getting this constructor
called and transferring control to the resulting space.  This is the
job of the protospace.

The \term{protospace} is a single page, read-only (and therefore
discrete) memory object containing a hand coded program
(Algorithm~\ref{alg:protospace}).  When a constructor builds a new
program, it installs the protospace as the initial address space of
the new program.  The address space capability provided in the
constructor template is passed to the protospace program in a well
known capability register.  Even without a mutable address space, the
protospace is able to test the template address space capability to
learn if it is a constructor.  If so, it invokes this constructor to
build the address space for the new process.  Now having a (hopefully
valid) address space capability in hand, it performs the \term{set
  space and PC} operation on its process to change the running address
space and program counter in a single atomic operation.\footnote{This
  operation is more commonly referred to among the EROS research group
  as the \term{lobotomize} operation.}  This sequence is later
reversed to accomplish program teardown.
\begin{algorithm}
\caption{Protospace Algorithm}
\label{alg:protospace}
\begin{algorithmic}
\STATE tc $\leftarrow$ \emph{template address space capability};
\IF{is\_constructor(tc)}
\STATE (result,tc) $\leftarrow$ create\_yield(tc);
\IF{result $\not=$ \textbf{OK}}
\STATE \textbf{return} result; \emph{// usually space exhaustion}
\ENDIF
\ENDIF
\STATE lobotomize(tc,\emph{starting pc});
\end{algorithmic}
\end{algorithm}

The protospace incidentally solves another problem: it transfers
responsibility for initialization from the constructor to the program
itself.  Once the protospace is set in motion, the constructor becomes
available for further invocation; it is the process itself that
returns to the original caller of the constructor.  In consequence,
programs that require long running initialization do not cause the
constructor to be blocked while this initialization occurs.
\index{constructor|)}

\section{Virtual Copy Spaces}
\label{sec:virt-copy-space}

\index{copy on write|see{virtual copy space}}
\index{virtual copy!address space|(defined}
Given the mechanisms provided by the EROS virtual machine, it is
straightforward to construct virtual copy spaces as user level
objects.  These objects are known as \term{Virtual Copy Spaces}, and
provide lazy copy on write services relative to an initial space.  If
no initial space is provided, the default initial space is the
\term{primordial zero space}, a fully populated, immutable memory
object containing zeros.\footnote{All of which are contained by a
  single page and a small number of nodes.}

A virtual copy space is a kept memory object whose memory keeper is
the \term{virtual copy memory keeper}, and whose initial contents are
read-only.  When a write fault occurs due to a memory store reference,
control is transferred to the memory keeper.  This keeper clones the
necessary portion of the metadata required to insert a replacement
page, purchases a new writable page, copies the content of the
original page into this page, and inserts the new page into the
address space (Figure~\ref{fig:vcs-trick},
\index{virtual copy!algorithm}
Algorithm~\ref{alg:virt-copy}).  It then resumes the faulting program.
\begin{figure*}[htb]
  \begin{center}
    \leavevmode
    \psfig{figure=vcs-trick.eps}
    \caption{Virtual Copy implementation.}
    \label{fig:vcs-trick}
  \end{center}
\end{figure*}
\begin{algorithm}
\caption{Virtual Copy Algorithm}
\label{alg:virt-copy}
\begin{algorithmic}
\STATE root\_cap $\leftarrow$ \emph{root of memory object};
\STATE \emph{// }root\_cap \emph{is read-write by virtue of upcall}
\STATE blss $\leftarrow$ \emph{size of memory object (biased log)};
\STATE addr $\leftarrow$ \emph{offset of fault within memory object};
\STATE slot\_cap $\leftarrow$ root\_cap;
\REPEAT
\STATE cap $\leftarrow$ slot\_cap;
\STATE slot $\leftarrow$ slot\_index(addr, blss);
\STATE slot\_cap $\leftarrow$ fetch(cap,slot);
\IF{is\_read\_only(slot\_cap)}
\IF{is\_node(slot\_cap)}
\STATE new\_node $\leftarrow$ buy\_node(bank);
\STATE clone(new\_node,slot\_cap); \emph{copies RO node to new node}
\STATE store(cap,new\_node,slot)); \emph{Update containing node}
\STATE slot\_cap $\leftarrow$ new\_node;
\ENDIF
\ENDIF
\STATE blss $\leftarrow$ blss - 1;
\UNTIL{is\_page\_cap(slot\_cap)}
\STATE new\_pg $\leftarrow$ buy\_page(bank);
\STATE clone(new\_pg,slot\_cap); \emph{copies RO page to new page}
\STATE store(cap,new\_page,slot)); \emph{Update containing node}
\STATE return; \emph{// restart faulted process};
\end{algorithmic}
\end{algorithm}
All storage in the copied space is allocated from its own space bank,
supplied when the virtual copy space was created.  To the extent
possible both data and metadata from the original space are reused; no
copy is performed unnecessarily.

At any time, the address space managed by a virtual copy keeper can be
frozen.  Spaces frozen by the virtual copy keeper become immutable.
The \term{freeze} operation returns a constructor that will create new
virtual copy spaces whose ``original space'' is the frozen space.
Virtual copy spaces are therefore recursively constructable.
\index{virtual copy!address space|)}

\section{Session Management}
\label{sec:session-management}

\index{session management|(defined}
While no implementation of session management exists in the EROS
prototype, a description of the operating system would not be complete
without a sketch of how session management is accomplished in this
style of system.

All terminals are guarded by ``front end'' objects.  The terminal
front end (\term{termfe}) sits between the client and the actual
terminal device (Figure~\ref{fig:termfe}).  On the client side, all
data is passed through to the terminal itself.  On the terminal side,
all keystrokes except the ``secure attention'' and the ``disconnect''
operation are passed through to the client.
\begin{figure*}[htb]
  \begin{center}
    \leavevmode
    \psfig{figure=termfe.eps}
    \caption{Terminal front end.}
    \label{fig:termfe}
  \end{center}
\end{figure*}

All of the exceptional cases causes the client to be placed ``on
hold.''  This appears to the client code as though the user typed the
flow control key and walked away.  While the client is on hold, access
to the terminal is transferred to the control channel, which is held
by the session manager.  On receipt of control, the session manager
uses the control channel to nullify the terminal device connection,
and activates a distinct \textbf{termfe} on the same terminal device
that is connected to a login authenticator session.  This session is
owned by the login authenticator, which also holds a control channel
to its session.

\index{login authentication}
The login authenticator performs whatever form of authentication is
appropriate to the system.  When authentication is successfully
accomplished, the login authenticator reaches into its database of
(user, session) pairs and extracts the primary session associated with
this user.  It disconnects its own \textbf{termfe} from the terminal,
reconnects the user's primary session, and takes the user session
\textbf{termfe} off of ``hold.''

Conceptually, the user's login session never actually exits.  If the
user runs in an operating environment that expects a new ``shell''
whenever a login occurs, this can be simulated by a suitable front end
object.
\index{session management|)}

\section{Closing Comments}

With the exception of the session manager described in
Section~\ref{sec:session-management}, the objects described in this
chapter are implemented and working (along with several others omitted
here).  It should be emphasized, however, that operating system
implementation must at this stage be considered a prototype.  Its
description is included because the operating system structure has
implications for system security and performance.  Without an
understanding of at least these facilities, neither the formal
examination of Chapters~\ref{cha:model} and~\ref{cha:proof} nor the
performance evaluation presented in Chapter~\ref{cha:eval} can be
understood in context.

With respect to the proof construction, the correctness of the space
bank implementation is crucial.  In particular, the space bank must
never reallocate an object that is already in use.  The formal
evaluation will finesse this issue by assuming that there is a large
finite pool of unallocated objects, and that these objects are never
reused.  The space bank accomplishes this by rescinding access to
deallocated objects and re-inserting these objects into the
unallocated pool.

With respect to performance, the principal issues that must be
addressed by an operating system constructed on top of the EROS
virtual machine are storage allocation, page fault handling, and
invocation of user defined objects (i.e. interprocess communication).
Storage allocation (or deallocation) must be performed in association
with every process creation, every memory exception, and every
serialization of objects into some canonical interchange format
(files).  Each of these operations also requires multiple user object
invocations.  The implementation of the space bank and the virtual
copy object described here have been carefully performed to satisfy
these performance demands.  The underlying virtual machine
implementation provides an exceptionally fast interprocess invocation
mechanism.

\subsection{Allocation vs. Access}
An interesting side note to this design is that it is straightforward
to design systems in which there is a clean separation between the
ability to allocate system resources and the ability to examine the
content of those resources.  A system administrator must create the
initial operating environment for each user.  Typically, this is done
by first creating a user specific subspace bank and allocating the
entire operating environment from the storage authorized for that
user.  The administrator retains a copy of the user's space bank
capability so that the account can be destroyed along with all of its
storage.

Note, however, that possession of this space bank capability does
\emph{not} convey to the administrator any ability to examine the data
stored in that space by the user.  Conceptually, the user has a
contract to use their environment in any way they see fit, and the
administrator has no authority, need, \emph{or ability} to inspect the
user's premises. 

Two recent legal decisions suggest that this separation of concerns
may be highly desirable from a standpoint of liability.

In Stratton Oakmont v. Prodigy Services Co.,\footnote{1995 WL
  323710 (N.Y. Sup. Ct. May 24, 1995).} the Court held that
  \emph{because} Prodigy exercised editorial control over content, it
  played the role of an originating publisher:
\begin{quotation}
  PRODIGY held itself out as an on-line service that exercised
  editorial control over the content of messages posted on its
  computer bulletin boards, thereby expressly differentiating itself
  from its competition and expressly likening itself to a newspaper.
\end{quotation}
Prodigy was therefore held liable for libelous statements made on its
``Money Talk'' bulletin board.  


In Smith v. California,\footnote{361 U.S. 147, 152-53, 80 S.Ct. 215,
  218-19, 4 L.Ed.2d 205 (1959).} the Court struck down on First
Ammendmant grounds an ordinance that imposed liability on a bookseller
for possession of an obscene book, regardless of whether the
bookseller had knowledge of the book's contents.  In Cubby v.
CompuServe,\footnote{776 F. Supp. 135 (S.D.N.Y.  1991).} the Court
extended the same protection to Compuserve because Compuserve was
\emph{unable} to exercise editorial control:
\begin{quotation}
  While CompuServe may decline to carry a given publication
  altogether, in reality, once it does decide to carry a publication,
  it will have little or no editorial control over that publication's
  contents. This is especially so when CompuServe carries the
  publication as part of a forum that is managed by a company
  unrelated to CompuServe.
\end{quotation}
In the eyes of the law, companies that act as redistributors or common
carriers are held to the lesser legal standard of ``know or have
reason to know'' where liability is concerned.\index{EROS!operating
  system|)}
