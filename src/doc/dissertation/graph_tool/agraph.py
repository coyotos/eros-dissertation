#!/usr/bin/python
#
# Python script to compute a potential access graph given
# a direct access graph.
#

import pdb;
import copy
import sys

debug = 0;

# Initialize the rights variables.  It's important that none of these start
# with 'p' or 'o', as those names will be taken by objects.

ri = "ri";
ra = "ra";
wi = "wi";
wa = "wa";
ti = "ti";
ta = "ta";
gi = "gi";
ga = "ga";
dti = "dti";
dta = "dta";
dgi = "dgi";
dga = "dga";
ci = "ci";
ca = "ca";

fra = "(ra)";
fwa = "(wa)";
fta = "(ta)";
fga = "(ga)";
fdta = "(dta)";
fdga = "(dga)";
fca = "(ca)";

indir_rights = [ri, wi, ti, gi, dti, dgi, ci ];
acc_rights = [ra, wa, ta, ga, dta, dga, ca ];
dim_rights = [ra, ri, dta, dti ];
rights = [ri, ra, wi, wa, ti, ta, gi, ga, dti, dta, dgi, dga, ci, ca ];

#
# Mapping from de jure to de facto rights.  The absence of de facto 
# indirect rights is resolved by simply mapping those back to the de 
# jure rights.  de facto rights, of course, also map to themselves.
de_facto = { ra : fra, 
	     ri : ri,
	     wa: fwa, 
	     wi : wi,
	     ta : fta, 
	     ti : ti,
	     ga : fga, 
	     gi : gi,
	     dta : fdta, 
	     dti : dti,
	     dga: fdga, 
	     dgi : dgi,
	     ca : fca,
	     ci : ci,
	     fra : fra,
	     fwa : fwa,
	     fta : fta,
	     fga : fga,
	     fdta : fdta,
	     fdga : fdga,
	     fca : fca
	     };

# Reflected effective access:
reflect = { fra : fwa,
	    fwa : fra,
	    fta : fga,
	    fga : fta,
	    fdta : fdga,
	    fdga : fdta,
	    ca : ca,
	    fca : fca
	    };

access = { ri : ra,
	   wi : wa,
	   ti : ta,
	   gi : ga,
	   dti : dta,
	   dgi : dga,
	   ci : ca
	   };

#import string
#import os
#import glob
#import regex

namespace = {};
objects = [];

def publish(x):
    namespace[x] = globals()[x]

def error(s):
#    print "Error in \"%s\" at line %d: %s" % (current_file, current_line, s)
    print "Error: %s" % s
    sys.exit(1)

def good_rights(list):
    for x in list:
#	print rights;
#	print x;
	if (not x in rights): return 0;
    return 1;

class Node:
    def __init__(self, label):
	self.name = label;
	self.caps = {};
	self.isProcess = 0;

    def __repr__(self):
	s = "";
	k = self.caps.keys();
	k.sort(lambda x,y: x.name > y.name);
	for i in k:
	    r = self.caps[i];
	    r.sort();

	    if (s == ""):
		s = "    (%s, %s)" % (i.name, r);
	    else:
		s = "%s,\n    (%s, %s)" % (s, i.name, r);
	return s;

    def add_cap(self, dest, rights_list):
	result = 0;
	if (not dest in self.caps.keys()):
	    self.caps[dest] = [];
	for r in rights_list:
	    if (not r in self.caps[dest]):
		self.caps[dest].append(r);
		result = 1;
	return result;

    def cap(self, dest, rights_list):
	if (not dest in objects):
	    print "Destination \"%s\" not in object table" % dest.name;
	    return 0;
	elif (not good_rights(rights_list)):
	    print "Rights list %s no good" % repr(rights_list);
	    return 0;
	else:
	    return self.add_cap(dest, rights_list);
		

#
# Functions that will be "live" in the file-reading environment:
#

def Object(x):
    if (x in namespace.keys()):
	print("Name %s already bound\n", x);
	return;
    nd = Node(x);
    objects.append(nd);
    namespace[x] = nd;
    return;

def Process(x):
    if (x in namespace.keys()):
	print("Name %s already bound\n", x);
	return;

    nd = Node(x)
    objects.append(nd);
    namespace[x] = nd;
    nd.isProcess = 1;

class Graph:
    def __init__(self, o):
	self.objects = o;
	self.changed = 0;
	
    def __repr__(self):
	return repr(self.objects);

    def __repr__(self):
	s = "";
	o = self.objects;
	o.sort(lambda x,y: x.name > y.name);
	for i in o:
	    if (i.isProcess): ty = "process"
	    else:	      ty = "object"

	    if (s == ""): s = "%s <%s>:\n%s" % (i.name, ty, i);
	    else:	  s = "%s\n%s <%s>:\n%s" % (s, i.name, ty, i);
	return s;

    def mark_procs(self):
	for a in objects:
	    for b in a.caps.keys():
		if ca in a.caps[b] and b.isProcess == 0:
		   b.isProcess = 1;
		   self.changed = 1;

    def de_facto(self):
	for a in objects:
	    for b in a.caps.keys():
		#
		# simplification step -- promote all de jure rights 
		# to de facto rights to simplify the closure 
		# operations later.
		#
		for r in a.caps[b]:
		    if (not (de_facto[r] in a.caps[b])):
			if a.add_cap(b, [de_facto[r]]):
			    self.changed = 1;

    def reflect(self):
	for a in objects:
	    if debug>1: print "Reflecting %s" % a.name;
	    for b in a.caps.keys():
		#
		# reflection step -- make sure all of the rights
		# are reflected by their de facto counterpart.
		#
		for r in a.caps[b]:
		    if r in reflect.keys():
			if b.add_cap(a, [reflect[r]]):
			    if debug>1: print "added %s->%s[%s] because %s->%s[%s]" % (b.name, a.name, reflect[r], a.name, b.name, r)
			    self.changed = 1;
	    if debug>1: print self;

    def combine(self):
	for a in objects:
	    if not a.isProcess:
		continue;

	    for b in a.caps.keys():
		# if a can grant to b, anything in a is in b:
		if fga in a.caps[b]:
		   for k in a.caps.keys():
		       if (b.add_cap(k, a.caps[k])):
			  self.changed = 1;

		# if a can dimgrant from b, anything diminishable in a
		# is in b:
		if fdga in a.caps[b]:
		    for r in dim_rights:
			for k in a.caps.keys():
			    if r in a.caps[k]:
				if (b.add_cap(k, [r])):
				    self.changed = 1;

		# if a can call b, anything in a is in b:
		if fca in a.caps[b]:
		   for k in a.caps.keys():
			if (b.add_cap(k, a.caps[k])):
			    self.changed = 1;

		for c in b.caps.keys():
		    for r in indir_rights:
			# indirection rights accumulate upward:
			if r in a.caps[b] and r in b.caps[c]:
			    if a.add_cap(c, [r]):
				self.changed = 1;

			# so does access by way of indirection
			if r in a.caps[b] and access[r] in b.caps[c]:
			    if (a.add_cap(c, [access[r]])):
				self.changed = 1;

		    # if a can call b, anything in b is in a:
		    if fca in a.caps[b]:
			for r in acc_rights:
			    if r in b.caps[c]:
				if (a.add_cap(c, [r])):
				    self.changed = 1;

		    # if a can take from b, anything in b is in a:
		    if fta in a.caps[b]:
			for r in acc_rights:
			    if r in b.caps[c]:
				if (a.add_cap(c, [r])):
				    self.changed = 1;

		    # if a can dimtake from b, anything diminishable in b 
		    # is in a:
		    if fdta in a.caps[b]:
			for r in dim_rights:
			    if r in b.caps[c]:
				if (a.add_cap(c, [r])):
				    self.changed = 1;

		    # "colluding process" cases.
		    if b.isProcess:
			if fta in a.caps[b]:
			    for r in rights:
				if r in b.caps[c]:
				    if (a.add_cap(c, [r])):
					self.changed = 1;

			if fga in a.caps[b] and fga in b.caps[c]:
			    if (a.add_cap(c, [fga])):
				self.changed = 1;

			if fwa in a.caps[b] and fwa in b.caps[c]:
			    if (a.add_cap(c, [fwa])):
				self.changed = 1;

			if fra in a.caps[b] and fra in b.caps[c]:
			    if (a.add_cap(c, [fra])):
				self.changed = 1;

		# cases where c is also a process:
		for c in objects:
		    if c.isProcess and b in c.caps.keys():
			# "monkey in middle" cases.
			if fwa in a.caps[b] and fra in c.caps[b]:
			    if (a.add_cap(c, [fwa])):
				self.changed = 1;
			if fga in a.caps[b] and fta in c.caps[b]:
			    if (a.add_cap(c, [fga])):
				self.changed = 1;


    # trim -- wherever there is both a de jure and a de facto right,
    # remove the de facto right.  It makes the graph much clearer.
    def trim(self):
	for a in objects:
	    for b in a.caps.keys():
		# always mutate a copy when running an iterator!
		# note that this is a *shallow* copy.
		x = copy.copy(a.caps[b]);
		for r in a.caps[b]:
		    if r in acc_rights and de_facto[r] in a.caps[b]:
			x.remove(de_facto[r]);
		a.caps[b] = x;

    def close(self):
    	iter = 1;
	more = 1;
	while more:
	    self.changed = 0;
	    self.mark_procs();
	    if debug: print "Pass %d after mark procs:" % iter;
	    if debug: print self;
	    self.de_facto();
	    if debug: print "Pass %d after de facto excalation:" % iter;
	    if debug: print self;
	    self.reflect();
	    if debug: print "Pass %d after reflection:" % iter;
	    if debug: print self;
	    self.combine();
	    if debug: print "Pass %d after combine rules:" % iter;
	    if debug: print self;
	    more = self.changed;
	    iter = iter + 1;
	self.trim();


def prepare_loader():
    global namespace, objects

    namespace = {};
    objects = [];

    # publish the appropriate symbols:
    for i in rights:
	publish(i);
    publish("Process");
    publish("Object");

################################################
##
## Now process the graph description file.
##
################################################

def load(s):
    prepare_loader();

    execfile(s, namespace);

    g = Graph(objects);

    return g;

if (__name__ == "__main__"):
    g = load(sys.argv[1]);
    print("Before:");
    print g;
    print("After:");
    g.close();
    print g;
else:
    print "Imported module %s" % __name__;

#for line in template.readlines():
#    if (line == "%targets\n"):
#	dump_targets(out)
#    elif (line == "%options\n"):
#	dump_options(out)
#    elif (line == "%objects\n"):
#	for o in obj_file_list:
#	    out.write("OBJECTS += %s\n" % o)
#    elif (line == "%depend\n"):
#	for f in src_file_list:
#	    ofile = os.path.basename(f)
#	    suffix =  os.path.splitext(ofile)[1]
#	    ofile = os.path.splitext(ofile)[0]
#	    out.write("%s.o: $(TOP)/%s\n" % (ofile, f))
#	    if (suffix == ".c"):
#		out.write("\t$(C_BUILD)\n\n")
#	    elif (suffix == ".cxx"):
#		out.write("\t$(CXX_BUILD)\n\n")
#	    elif (suffix == ".S"):
#		out.write("\t$(ASM_BUILD)\n\n")
#
#	    out.write(".%s.m: $(TOP)/%s\n" % (ofile, f))
#	    if (suffix == ".c"):
#		out.write("\t$(C_DEP)\n\n")
#	    elif (suffix == ".cxx"):
#		out.write("\t$(CXX_DEP)\n\n")
#	    elif (suffix == ".S"):
#		out.write("\t$(ASM_DEP)\n\n")
#    else:
#	out.write(line)
#
#out.close()
#
