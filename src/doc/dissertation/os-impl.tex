\chapter{Operating System Implementation}
\label{cha:os-impl}

Because most of the ``policies'' of traditional operating systems are
handled by user level programs in an EROS system, a high performance
reduction to practice of the EROS virtual machine is not enough to
yield a high performance system.  Any user defined objects involved in
managing performance critical policies must also have high performance
implementations.

\index{constructor}
In EROS, there are four such user defined objects: the constructor,
the process creator, the virtual copy keeper, and the space bank.  The
constructor and process creator play key roles in the instantiation of
new processes.  The virtual copy keeper and the space bank jointly
determine the performance of demand-zero and demand-copy faults.  The
space bank also plays a key role in the performance of such things as
files.

In practice, measurement shows that the performance of the ``obvious''
implementations of the constructor and process creator is amply
sufficient.  For the most part, these programs do not perform any
complicated operations; their performance is mostly a function of the
underlying kernel capability invocation performance.  Where
optimizations are possible, they are primarily concerned with avoiding
calls that are gratuitous, such as using the space bank's ``allocate
three nodes'' operation when appropriate rather than allocating three
nodes one at a time.

In this chapter, then, I describe the implementation of the virtual
copy keeper and the space bank.

\section{The Virtual Copy Keeper}

\index{virtual copy!address space!implementation|(}
The virtual copy keeper handles both demand-zero and demand-copy
memory object faults.  All EROS memory objects are ultimately
constructed from the \term{primordial zero space}, a fully populated
memory object containing immutable zero pages. 

\subsection{Phylogeny of Memory Objects}
\index{memory object!construction of|(}
\index{constructor!memory object|(}
When a new memory space is to be constructed, a new ``virtual copy''
of the primordial zero space is constructed by invoking the ``zero
space constructor.''  This constructor builds a new virtual copy
keeper and hands it the space to copy from (in this case the
primordial zero space) and the offset of the last non-zero byte (in
this case zero).

\index{memory object!copy on write}
\index{memory object!demand zero}
Initially, the resulting memory space has no modifiable pages.  As
portions of the space are modified, the virtual copy keeper is
invoked.  With each invocation it performs one of two operations:
\begin{itemize}
\item If the reference is invalid, an appropriate size sub-portion of
  the primordial zero space is inserted.  This allows existing objects
  to grow.
\item If the reference causes a permission violation, the read-only
  metadata (nodes) and data (a page) on the reference path are copied
  into read-write copies.
\end{itemize}
Figure~\ref{fig:virt-copy} shows the structural modification
associated with such a copy:
\begin{figure*}[htb]
  \begin{center}
    \leavevmode
    \psfig{figure=vcs-trick.eps}
    \caption{Virtual Copy implementation.}
    \label{fig:virt-copy}
  \end{center}
\end{figure*}

Modifications proceed in this fashion until the new space has been
written to the satisfaction of the program that manipulates it (e.g., a
linker).  If further virtual copies of the resulting space are to be
made, the space is now frozen, and a new constructor is produced that
will build new virtual copies of this frozen memory object.

The point in this description is that the virtual copy keeper can be
assured of the internal structure of the resulting object.  The
primordial zero space is a tree of nodes whose leaves are pages.  All
virtual copies (recursively) from this initial zero space have been
constructed by some virtual copy keeper, so \emph{their} internal
structure is known.  This simplifies the keeper program by eliminating
the need to type check the capabilities within the memory object.
\index{memory object!construction of|)}
\index{constructor!memory object|)}

\subsection{Optimized Traversal}

\index{virtual copy!address space!traversal}
\index{memory object!copy on write}
Unfortunately, knowing the memory object structure does not eliminate
the need to perform the tree traversal.  This traversal requires two
kernel capability invocations in each step: one to fetch the
capability for the next layer down, and the other to check the type of
this capability to determine if it is a page capability.

Since the virtual copy keeper allocates all of the mutable nodes and
pages, and knows that the immutable nodes and pages are frozen, it
also knows that any two traversals driven by the same address will
arrive at the same object.  Further, because the virtual copy keeper
knows the size of a node (32 slots), it knows that the address space
may be treated as divided into chunks of 32 pages, and that if a
recently traversed address $a$ and the current address $b$ fall within
the same 32 page region they will arrive at the same leaf node in any
traversal of the tree.

\index{memory object!heaps}
\index{memory object!stacks}
Heaps and stacks grow in mostly linear fashion.  By caching the
outcome of the last traversal, the total number of traversals is
reduced by a factor of 32.  In consequence, the number of kernel
capability invocations associated with virtual copy handling is
greatly reduced.

While the current implementation does not do so, the same optimization
can be applied in chunks of 1024 pages.  In large applications, this
would have much the same effect as the optimization described here,
only at one step greater remove. I have not implemented this
optimization primarily because current programs appear to have no need
for it.
\index{virtual copy!address space!implementation|)}

\section{The Space Bank}
\index{space bank!implementation|(}

The space bank is the source of all storage in an EROS system.  It
allocates and deallocates nodes, data pages, and capability pages,
enforces storage allocation restrictions, and ensures that storage is
never multiply allocated.

\index{space bank!design challenges}
For efficiency reasons, the space bank implementation must perform its
actions in a way that is friendly to the aging policy and the layout
of the disk store.  Policies that impact node and page allocation
locality can greatly impact the number of metadata reads associated
with loading large objects into memory.  On the other hand, there are
strong incentives to reallocate objects that have been recently
deallocated (and are therefore still in memory) in preference to
objects that are unlikely to be in memory.  Space banks must be
relatively cheap to create, because the best indicator that two
allocations have a locality relationship is the fact that they are
allocated from the same space bank.

While doing all of the above, the space bank must store information
about which objects have been allocated, to do which it must supply
its own heap management.  Space banks cannot rely on virtual copy
keepers to supply their address spaces because virtual copy keepers
buy their storage from space banks.

Finally, space banks must do all of the above without ever generating
an exception; there is no keeper to catch their mistakes, and no way
to recover the storage allocation map if it becomes corrupted.

\subsection{Organization of the Store}

\index{object store!organization|(}
\index{ranges!disk organization}
Every EROS volume is divided into \term{ranges}.  A range can be
either a \term{boot range}, a \term{range table}, or an \term{object
  range} (Figure~\ref{fig:volume-layout}).\footnote{For historical
  reasons, the implementation retains a range type for bad use in bad
  block mapping.  At the time the implementation was started ESDI and
  ST506 drives were still common.  These drives had a relatively high
  incidence of post-format block dropout, and did not do automatic
  sector remapping.  By the time the implementation was completed,
  even ``boat anchor'' class machines had moved to either the ATA
  (E)IDE or SCSI interface specifications, and the most advanced
  drives to fast and wide (Ultra) SCSI and to EATA DMA or Ultra DMA
  EIDE interfaces.  All of these interfaces require the drive to
  perform transparent sector remapping, and the bad block range type
  is not supported in the current implementation.}  Every volume
begins with a boot range at offset zero.  In addition to a copy of the
boot code, this range identifies the location of the primary and
secondary (if present) range table(s).  Range tables are single-page
ranges that identify the locations of all ranges (including boot
ranges and range tables) within the volume.  A range table entry
includes the type, start (in sectors relative to the volume start)
size (in sectors), starting object identifier (if appropriate), and
number of objects in each range.  To facilitate recovery, it is
customary to place the primary range table in the page following the
boot range, and the secondary at the end of the volume.\footnote{Note
  that in contrast to inode tables, these tables are updated only when
  ranges on the volume are being modified, so it is exceedingly
  unlikely that a write failure on the drive will corrupt them.  The
  primary concern is therefore to ensure that only one range table is
  knocked out by an emergent bad sector or track on the drive.}
\begin{figure*}[htb]
  \begin{center}
    \leavevmode
    \psfig{figure=volume-layout.eps}
    \caption{Layout of EROS Volume.}
    \label{fig:volume-layout}
  \end{center}
\end{figure*}
\index{object store!organization|)}

\subsubsection{Object Ranges}
\index{ranges!disk organization|(}
\index{range!capability!disk realization}
\index{capability!interpretation of OID}
Object ranges consist of contiguously numbered page frames.  Object
identifiers (OIDs) simply concatenate a frame number and an object
index relative to a frame; this allows the location of any object on
the disk to be determined by extracting the frame number, locating a
range containing that frame, and issuing a page frame read at the
appropriate sector offset relative to the start of that range.  Data
pages and capability pages occupy a full frame.  
\index{object store!node pot}
Nodes are packed into
page sized units called \term{node pots}.  

\index{ranges!duplexing|(}
The object identifiers associated with two ranges are either
completely disjoint or perfectly overlapping; if they match, the two
ranges are replicates of each other.  Given this, it is mostly true
that objects whose object identifiers are numerically close are
located on the disk reasonably close to each other.

\index{ranges!object frame types|(}
\index{object store!metadata}
An object frame holds only one type of object at a time; when a range
key is used to fabricate an object within a given frame whose type
does not match the current frame type, all objects within that frame
are rescinded, the new object type of that frame is recorded, and a
zero frame of the appropriate type is written to that frame.  In
principle, the independence of frame type and object location can be
used to localize objects and their associated metadata on the disk,
and allows all per-range metadata maintained by the space bank to be
recorded within the associated range.\footnote{In the KeyKOS
  implementation, ranges were segregated by object type: node range,
  page range, etc.  This created two problems: if actual object
  allocations proved to be sufficiently inconsistent with the
  predicted demand for each type, it might become necessary to
  reorganize the disk; in the original KeyKOS design there was no way
  to do such reorganization on a live system.
  
  Perhaps more unfortunate, metadata for node ranges had to be taken
  from page ranges.  The space bank therefore needed to incorporate
  code to migrate this metadata out of the way if the page range from
  which it was allocated was later deleted.  Such relocation is
  potentially expensive and necessary only because the underlying
  store was partitioned.}  In practice, the metadata/data localization
possible in this design is not used effectively by the current space
bank design.

\index{ranges!range header}
Object ranges have a reserved page  at the start of the range called a
\term{range  header}.  This  header  records the  number  of the  last
checkpoint that was successfully  migrated to the range; discrepancies
in this  number can be used  to determine when  replicates have become
detached and must  be brought up to date.  As a  sanity check, it also
holds the size of the range.
\index{ranges!disk organization|)}

\subsubsection{VM-Level Metadata}
\index{object store!metadata|(}\index{allocation count}Every page
has a small amount of metadata that must be stored on the disk.  This
includes the allocation count for the contained page and the object
type of the associated frame.\footnote{Call counts are carried only by
  nodes, and reside within the node structure itself.  } \index{object
  store!page pot} This data will not fit within the page frame, and is
therefore recorded in a separate metadata frame called a \term{page
  pot}, which resides in the same range as its associated object
frames.  The page pot also records whether a given frame is zeroed
(either all zero data or all zero number capabilities).

On the x86 family, pages are 4096 bytes, so 819 pages can be described
in a single page frame if the metadata is organized carefully.  The
actual layout of a range is therefore a single range header page
followed by some number of clusters, each of which consists of a
single page pot followed by up to 819 object frames
(Figure~\ref{fig:object-range-layout}).
\begin{figure*}[htb]
  \begin{center}
    \leavevmode
    \psfig{figure=object-range-layout.eps}
    \caption{Layout of on-disk object range.}
    \label{fig:object-range-layout}
  \end{center}
\end{figure*}

In practice, this design proves to be very simple to implement at the
level of the kernel object loader: an initial computation of the
object frame offset is done without regard to the presence of page
pots.  This frame is then biased by the cluster index relative to the
start of the frame:

\begin{samepage}
\begin{indented}
\begin{verbatim}
unbiasedFrame := (frameOf(oid) - frameOf(range.firstOID));
biasedFrame := unbiasedFrame+(unbiasedFrame/FramesPerCluster)+2;
startingSector := biasedFrame*sectorsPerPage + range.startSector;
\end{verbatim}
\end{indented}
\end{samepage}
Note that none of this computation requires disk accesses for metadata
traversal.  The page pot for the frame must be loaded if it is not
already in memory, but this load can be initiated in parallel with the
actual frame load.  In practice, the page pot is almost always in
memory, and most page loads are of newly allocated (and therefore
zeroed) page frames.  The fact that these frames are zeroed can be
determined by examining the page pot, and in such cases the frame load
for the actual object can be completely elided.
\index{object store!metadata|)}
\index{ranges!object frame types|)}
\index{ranges!duplexing|)}

\subsection{Space Bank Allocation Strategy}

\index{resource management|(}
\index{space bank!allocation strategy|(}
All EROS space banks are implemented by a single program.  As a
result, there are many logical banks contending for the same pool of
storage.  Each logical bank has a unique in-memory bank structure.

\subsubsection{Idealized Design}
\index{space bank!idealized design}
\index{space bank!locality}
Ignoring any concern for preferential allocation of recently
deallocated objects, an ideal space bank design would adopt a three
level allocation strategy.  At the top level, individual banks would
allocate objects from disk frames.  When necessary, new disk frames
would be allocated from a physically contiguous run (of, say 16
frames) that had previously been allocated to that bank.  At the
bottom, banks would allocate runs of disk frames from a global pool.

While preserving locality and simplicity, this design does very poorly
in systems having a small amount of disk space, because it reserves
storage in extents whose size is large relative to typical memory
object sizes.  More important, this strategy does not cope well with
disks whose storage is approaching exhaustion.  There are a
potentially large number of banks, and ``stealing'' frames from runs
owned by other banks might therefore require a relatively large
search.

Also, there is a problem associated with \emph{de}allocation in this
design: freed frames may not be freed from the currently active run,
leading to situations in which a mature object range could have many
runs having only a few available frames apiece (fragmentation).

\subsubsection{The Allocation Cache}
\index{space bank!allocation cache}
The actual design uses a two level allocation strategy known as the
\term{allocation cache}.  A ``line'' in this cache consists of a window
of mostly sequential frames.  When allocating a new frame, a logical
bank determines its cache ``line'' by a hash on its bank pointer.  The
current cache is designed as a 1+1 set cache; every bank hashes to a
unique preferred line and falls back to another line in the cache.

When both the primary and fall back allocation cache lines are empty, a
cache line refill is performed.  This refill proceeds by performing a
scan of the available object ranges to locate available object frames.
This scan proceeds until the line has been filled or all object frames
from a given subrange (see below) have been allocated.  If a cache
line fill operation discovers that no frames are available, the
strategy of last resort is to steal frames from any allocation cache
line that has them.

\index{space bank!locality}
This strategy yields object frames that are well localized, but are
not necessarily contiguous.  Matters are considerably helped by the
fact that individual object deallocations are quite rare; it is far
more common for an entire bank to be deallocated at once, which
returns well localized objects to the available pool.  Also, there are
cases in which two active banks may allocate frames from the same
range at some cost in locality to both banks.

The lack of perfect locality inherent in this allocation strategy is
less important in EROS than it might at first appear.  While batched
reads may be performed by sufficiently clever virtual machine
implementations, a given process is never stalled on more than one
page frame load at a time.  Object range write traffic is both sorted
and batched, and occurs asynchronously with respect to all running
processes.  The final result is that the strategy described here
\emph{appears} to be sufficient for most general purpose uses.

That said, there are applications that have a real need for fine grain
locality control.  It is envisioned that these applications will make
use of distinguished contiguous space banks that truly guarantee
contiguous extents.

One problem that is not solved by the simple allocation cache
mechanism described here is avoidance of frame retags.  Converting a
data page frame to a capability page frame is cheap.  Changing either
type of page frame to a node pot is cheap.  Converting a node pot back
to a page frame type is expensive; all nodes within that frame must be
loaded by the virtual machine and their allocation counts consulted to
ensure that allocation counts advance monotonically across type
conversions.  The space bank implementation minimizes the frequency of
this interconversion by maintaining separate allocation caches for
node pots and other object frames.

\subsubsection{Allocation and Deallocation Tracking}

\index{space bank!allocation tracking|(}
The allocation cache fill mechanism in turn relies on a list of active
ranges and a per-range allocation bitmap.  The current EROS space bank
implementation supports a generous but bounded number of total ranges;
several times the number of ranges that would reasonably
appear in any real system.\footnote{This is not an inherent design
  limitation, merely a shortcut in the research implementation.}

For each range, the space bank must track which frames have already
been allocated.  This is accomplished by the simple expedient of
maintaining a hierarchically structured allocation bitmap.  The
leading object frames of every range contain pages.  Each of these
pages contains a bitmap for as many pages as it can hold (a subrange).

In addition to the overall allocation bitmap, each logical bank is
required to keep track of the storage it has allocated.  Storage can
be reclaimed only by the back that currently ``owns'' that storage,
which is either the allocating bank or the parent of that bank if the
bank was destroyed without deallocating its storage.  
\index{red-black tree}
Every bank
maintains a red-black tree of the storage that it has allocated.  To
amortize storage overhead, leaf nodes in this tree cover a block of
frames and record which portions of the block are allocated.

When an object is deallocated, the bank first marks it deallocated
within the red-black tree.  If this deallocation causes the entire
frame to become deallocated, a check is made to see if this frame's
entry is still in the allocation cache; if so it is marked free
there.  If all else fails, the frame is freed by simply marking it
free in the range's allocation bitmap.
\index{space bank!allocation tracking|)}
\index{resource management|)}
\index{space bank!allocation strategy|)}

\subsection{End Result}

Given the asynchronous nature of EROS disk transfers, the correct
metric of locality for EROS is track level, but not sub-track level.
The goal is to perform as many reads or writes as possible from a
single head in a single disk rotation.

\index{space bank!locality}
The space bank design presented here makes no attempt to optimize
locality across tracks. On modern disk drives there is no registration
of tracks from one platter to the next.  The head to head latency time
is therefore just as large as the local seek latency, and there is
consequently no incentive to prefer one to the other.  The primary
goal is to avoid long seeks within a single batch of reads.  In
practice, the space bank design constitutes a reasonable compromise
between intra-memory-object locality and seek latency.

\index{space bank!fragmentation in deallocation}The object
deallocation strategy makes a half-hearted attempt to encourage object
reuse within a single bank by restoring deallocated frames to the
allocation cache when possible.  Over time, however, there is a
tendency for the mechanism described to have a window in each object
range that constitutes the ``hot allocation zone,'' and for this zone
to slowly migrate in circular fashion within the object range.  
I have performed preliminary measurements exploring the performance
impact of this migration, and it appears that no smarter
reallocation strategy is really required.
Objects are zeroed on deallocation, and this fact is recorded in the
associated page pot.  The main requirement is therefore to ensure that
the associated page pot is in memory when an object is allocated and
that the type tag is changed from node pot to anything else as seldom
as possible.  The allocation cache code is well situated to initiate
prefetches of page pots, and the separation of the two allocation
caches minimizes interconversion.  \index{space bank!implementation|)}

\section{Closing Comments}
It is a commonly held view among operating system architects that one
purpose of operating systems is to provide common abstractions that
allow programs to manipulate things like memory objects without undue
awareness of the specific properties of the underlying hardware.

There are two things wrong with this perspective:
\begin{itemize}
\item It doesn't work. The register set and mapping granularity of the
  underlying machine are necessarily exposed to programs.
\item It defeats useful optimizations.  
\end{itemize}

Any program that manipulates memory objects is necessarily aware of
issues like page size and mapping congruence restrictions (as are
imposed by some virtual caches).  The optimizations performed by the
space bank and the virtual copy keeper \emph{rely} on knowledge of the
underlying representation.  To borrow a line from Allan Kay: ``Simple
things should be simple, hard things should be possible.''

Most EROS programs do not take advantage of knowledge of the
underlying hardware.  Tools provided ``out of the box,'' such as the
virtual copy keeper, are usually sufficient to manage memory objects,
and therefore to handle such details.  That said, the ability to build
special purpose managers for memory objects is available.  Examples of
situations in which this is useful include large memory objects whose
allocation may be extent based, whose allocation pattern is less
predictable than heap growth, or whose content needs to be paged
against a different working set than that of the main program.  All of
these are possible in EROS \emph{because} the underlying
representation is exposed.

The key question, then, is ``What is the \emph{cost} of exporting
things like storage allocation and fault handling outside of the
kernel?''  In this regard, the EROS design has very much the feel of a
microkernel design. A considerable amount of optimization effort and
data collection on various microkernels -- most notably Mach, but also
Chorus and others -- suggests that this cost is prohibitive. Analysis
ofthis question is the subject of the next chapter.

