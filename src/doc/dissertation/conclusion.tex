\part{Conclusion}
\chapter{Conclusions and Future Work}
\label{cha:conclusion}

This dissertation makes four primary contributions:
\begin{itemize}
\item The {\DimTake} access model, an extension of the capability
  access model proposed by Jones, Lipton, and Snyder and enhanced by
  Bishop to address object metadata, opaque indirection, and certain
  additional access checks.
\item {\Swmodel,} a formal model of capability architectures that
  allows precise statements to be made about access and operations.
  Using {\swmodel,} it is possible to prove that the conditions
  imposed by some implementation actually enforce the security policy
  they are designed to support.  {\Swmodel} is the first formal model
  of any complete, general purpose operating system architecture for
  which such proofs have been favorably completed; previously
  successful proof efforts have proven only negative results.
\item The EROS operating system, a realization of the {\swmodel}
  model that is covered by both the SW model and the proof of
  confinement presented here.
\item The EROS implementation, which yields measured performance
  competative with, and in many cases exceeding, the performance of
  conventionally architected ACL-based operating systems and
  microkernels.  Neither the use of capabilities nor aggressive
  dekernelization needs to be a limiting factor in system
  performance.
\end{itemize}

\section{The {\DimTake} Access Model}

The {\takegrant} access model \cite{Jones:linear} as extended by
\cite{Bishop:transfer} is the most commonly referenced access model
for understanding access and authority transfer in capability
systems.  
When applied to real systems, the {\takegrant} model exhibits
several shortcomings:

\begin{itemize}
\item It does not model opaque indirection.
\item It therefore does not model systems that support dekernelized
  mapping management.
\item It does not incorporate access rights sufficient to enforce a
  basic security policy: the confinement policy
  \cite{Lampson:Confinement}.
\item It does not incorporate access rights sufficient to model
  secure copy on write implementations.
\item As originally stated, it incorporates hierarchical
  assumptions about process invocations.
\end{itemize}

The {\DimTake} access model extends the {\takegrant} with two small
extensions, indirection and weak access, and incorporates a more
flexible interprocess communication mechanism.  The extensions
bridge the gap from the original model to the behavior of real
systems, in particular to the behavior of file systems and memory
mapping.  The more flexible interprocess communication mechanism
subsumes the cases covered by the original model but also accounts
for the IPC behavior of a larger number of capability systems.

This model is ultimately what prompted the proof attempt that
resulted in {\swmodel} and the confinement proof presented in this
dissertation.

\section{{\Swmodel} Model}

{\Swmodel} is a formal model of the objects, actions, and
operational semantics of a broad class of capability systems.  No
previous model at this level of detail has previously been
articulated.  The {\swmodel} allows allows precise statements to be
made about access and operations.  {\Swmodel} therefore makes it
possible to prove that the conditions imposed by some implementation
actually enforce the security policy they are designed to support.
{\Swmodel} defines a family of capability systems, of which any realization
satisfies the proven security properties.

Convincing informal arguments have existed both for
\cite{Boebert:star-property,Karger:Thesis,KeyKOS:Factory} and
against \cite{Boebert:star-property,Karger:Thesis} the feasibility
of enforcing the confinement policy with various capability systems.
While a formal treatment exists \cite{Harrison:Protection} showing
that access control list systems \emph{cannot} enforce confinement,
no comparable formal treatment has been given to capability systems
before now.

In collaboration with Sam Weber, I have proven that capability systems
modeled by {\swmodel} can enforce a non-trivial security policy: the
confinement policy \cite{Lampson:Confinement}.  {\Swmodel} is the
first formal model of any complete, general purpose operating system
architecture for which such proofs have been successfully completed.
Previous attempts to prove such properties using theorem proving or
model checking have failed due to the size of the problem.  Attempts
to prove such properties for access list based protection models have
shown that the problem in such models is in general undecidable, and
that in the decidable cases the access list model does \emph{not}
enforce the desired properties \cite{Harrison:Protection}.  {\Swmodel}
and the accompanying proof show that a previously untried
specification and proof strategy yields to relatively straightforward
proofs about security policies.

%%%   Separately, the proof presented here is the first proof that
%%%   capability systems can enforce significant security policies.

The proof presented here is the first successful formal treatment of
the issue.  It shows that in capability systems with a finite number
of objects, the problem is decidable, and the proof shows that with
minor modification, capability systems \emph{can} enforce non-trivial
security policies.

\section{EROS Operating System}

The Extremely Reliable Operating System (EROS) is one realization of
an SW system.  This dissertation discusses the EROS virtual machine
architecture of EROS, and describes how the capabilities defined by
that architecture are mapped to {\swmodel}.  By virtue of this
mapping, the EROS design is covered by both the SW model and the
proof of confinement presented here.

EROS is therefore the first operating system whose architecture has
been formally shown to enforce useful security policies.  While
further work is needed to carry this proof through to the silicon,
this proof is a significant advance in the state of the art for
formal operating system analysis.

\section{EROS Implementation}

Finally, this dissertation presents a prototype implementation of
the EROS system on the Intel x86 machine architecture.  By mapping
EROS virtual machine abstractions to hardware-supported objects,
this implementation yields measured performance competative with,
and in many cases exceeding, the performance of conventionally
architected ACL-based operating systems and microkernels.  The
quantitative evaluation presented here shows that neither the use of
capabilities nor aggressive dekernelization needs to be a limiting
factor in system performance.

As a result of this prototype and evaluation, it is now known that:

\begin{itemize}
\item Fast capability systems are feasible on commodity hardware.
\item Dekernelized systems can meet, and sometimes exceed,
  monolithic system performance.
\item Providing security neet not penalize performance.
\end{itemize}

The EROS prototype is a monolithic implementation; the kernel
incorporates device driver and storage management function that is
traditionally exported from microkernel systems.  EROS is also
dekernelized: both memory object management and storage allocation
is performed \emph{outside} the kernel.  Such a system might be
expected to suffer from the weaknesses of both designs.

By structuring the implementation as layers of successively more
generic representations and using cache-like consistency management
mechanisms \cite{EROS:Caching}, the EROS implementation matches the
interprocess communication performance of microkernel systems such
as L4 \cite{L3:IPC} (as discussed in \cite{EROS:IPC}) and carefully
constructed real-time systems such as QNX \cite{QNX:Overview} (whose
IPC times are slower than L4).  At the same time, the EROS operating
system meets and sometimes exceeds the performance of monolithic
systems as discussed in Chapter~\ref{cha:eval}.

\section{Future Work}

While the preliminary results from EROS are promising, only a small
fraction of the work applied to competing research projects elsewhere
has been applied to EROS.  Several areas need further exploration
before EROS can stand up favorably to non-expert evaluation.

\subsubsection{POSIX Environment}

\index{POSIX!compatibility environment}
Before EROS can usefully be deployed to a broad audience, the POSIX
compatibility environment must be completed.  This is essential for
self hosting.  More important, it is essential to allow an ``apples to
apples'' evaluation of EROS to be made relative to other systems.

A partial start on a compatibility environment (Dionysix) was
undertaken by University of Pennsylvania undergraduates in 1993.  At
the time, EROS was not sufficiently stable to support this effort, and
the Dionysix project was set aside.  The Dionysix design is basically
sound, and should be carried through.

\subsubsection{Networking Support}

A second requirement for broad deployment is an implementation of the
usual and customary networking support.  An efficient implementation
of TCP/IP in a dekernelized environment presents serious challenges;
address space based protection mechanisms induce additional data
copies, and such copies are well known to be significant and expensive
in the context of networking implementations.  Ultimately, it may
prove necessary to integrate such support within the EROS kernel.

\subsubsection{Java Environment}

\index{POSIX!vs. Java}
\index{Java!compatibility environment}
A possible direction to pursue for EROS is the creation of a
high performance protected Java environment.  While many attempts have
been made to augment Java with serious protection, most of the
potentially promising attempts have been built on POSIX platforms,
which are quite slow at the essential supporting operations.

A native protected Java platform implemented on EROS does not require
full POSIX compatibility, and could take advantage of EROS's greater
efficiency at protection oriented operations.  It would also provide a
basis for direct comparison across platforms provided that the same
JIT compilation technology was used in both implementations.

\subsubsection{Investigation of Demultiplexing}

\index{POSIX!impact of select@impact of \emph{select}()}
One question that has seen limited attention in the message oriented
microkernel literature is the impact of eliminating the POSIX
\emph{select}() system call.  The \emph{select}() call allows a single
process to wait for events of interest on a large number of
simultaneous descriptors.  The \emph{select}() mechanism eliminates
the need for large numbers of threads and therefore reduces the cost
of coordinating data access among these threads.  It remains to be
seen what overhead is induced if multiple threads must be used
instead.

The \emph{select}() operation is not readily implemented in systems
relying on user defined objects.  First, the operations performed by
these objects cannot be readily classified as ``input, output, other''
as the select interface desires.  Second, the necessary operations to
allow a user defined object to ``communicate'' its availability to
other processes may carry higher cost than the obvious multithreaded
design.

\subsubsection{Performance of Checkpointing and Persistence}

EROS implements a very different type of store than has become
customary in other systems, and a different approach to stabilizing
user data.  It has been suggested that the checkpoint mechanism
induces unnecessary overhead, in that a fair portion of the data
included in a given snapshot is ephemeral or efficiently
reconstructable.  No quantitative evaluation has been done to compare
this overhead to the overhead of metadata I/O, nor is it immediately
clear what the right criterion for comparison should be.

A related issue is the fact the EROS kernel has very limited knowledge
of the high level semantics of the objects it is loading from the
disk.  It does not, for example, have any obvious place to stand where
it is able to notice that access to a particular memory object is
sequential, and that performance might therefore be improved by
prefetching.  It would be interesting to investigate both user-hinted
prefetch and mechanisms for dynamic prefetch adaptivity.

%%% \chapter{Old Conclusion}


%%% In this chapter, I present a summary of the contributions provided by
%%% the EROS experiment, and outline future work that should be pursued in
%%% the context of EROS or similar designs.

%%% \section{Major Research Results}

%%% After eight years of work, what conclusions can be drawn from the EROS
%%% experiment?

%%% \subsubsection{No Performance Penalty}

%%% The fatal weakness of past capability systems has been poor
%%% performance. As a primitive access control mechanism, capabilities are
%%% actually simpler than access control lists, but no realization of a
%%% capability architecture has come close to competing with access
%%% control list architectures.

%%% The EROS experiment conclusively shows that there is at least one
%%% point in the design space at which capability systems meet or exceed
%%% the performance of access control list based competitors.

%%% \subsubsection{Cost of Dekernelization}

%%% A second hotly contested issue in the literature has been the ultimate
%%% practicality of microkernel architectures.  It has been argued that
%%% the core optimizations of microkernels can also be applied in
%%% monolithic systems, and that dekernelizing resource allocation and
%%% fault handling imposes excessive cost in light of the intrinsic cost
%%% of interprocess communication.  Until now, no microkernel emulation
%%% has exceeded the performance of a monolithic implementation.

%%% In practice, the key challenge has been dekernelized memory
%%% management.  Among monolithic systems, the fastest implementation of
%%% memory fault handling is currently provided by Linux, and the EROS
%%% implementation outperforms this implementation by 11.3\%.  Projecting
%%% from prior work reported in \cite{EROS:IPC}, the current EROS IPC
%%% implementation remains conservatively 20\% slower than it needs to be,
%%% raising the realizable headroom to over 15\%.

%%% Further, EROS is dramatically better than conventional systems at
%%% reusing memory structures, which significantly reduces its process
%%% startup and memory sharing overheads.  Indeed, the one metric of
%%% importance on which EROS does \emph{not} exceed monolithic performance
%%% is in-memory file handling, which is not a metric that matters to
%%% native EROS applications.

%%% \subsubsection{Confinement is Feasible}

%%% With the rapidly increasing emphasis on downloaded code, the ability
%%% to restrict a program's behavior and access rights to something finer
%%% than those of its user has become an imperative.  A great deal of
%%% energy has gone into various ``sandboxing'' techniques, but most
%%% sandboxing designs have not been backed by either a principled model
%%% or a careful evaluation of potential information flow.  As a result,
%%% most of the currently deployed sandboxing strategies have been rapidly
%%% defeated.

%%% Harrison et al. \cite{Harrison:Protection} have shown that the
%%% necessary safety restrictions for effective sandboxing cannot \emph{in
%%%   principle} be successfully imposed using conventional access control
%%% mechanisms.  The constraints imposable by access list mechanisms do
%%% not satisfy the requirements, and in any case the program whose
%%% behavior is to be restricted runs on behalf of an authorized
%%% principal.  The security objective for such code is to enforce a more
%%% restrictive set of constraints than those that are generally
%%% authorized to a particular principal.

%%% The EROS constructor mechanism, in contrast, both enforces the
%%% required restrictions and has been shown to be architecturally sound.
%%% While flaws in particular implementations may result in this mechanism
%%% being compromised, these flaws can be repaired as they are exposed.
%%% The corresponding flaws in other known confinement mechanisms cannot
%%% in principle be corrected.

%%% \subsubsection{Formal Access Model and Evaluation Method}

%%% I have defined {\dimtake}, an extension of the {\takegrant} model
%%% that addresses both indirect reference and endogenously verifiable
%%% enforcement of access restrictions.  The new model provides a basis
%%% for evaluating access control enforceability in other systems.

%%% Perhaps more important, I have shown that proofs of architectural
%%% correctness with respect to enforcing security policies are feasible,
%%% and (in collaboration with Sam Weber) provided an example of how such
%%% proofs may be successfully concluded without excessive proof
%%% mechanism.

%%% \subsubsection{Working Artifact}

%%% The last contribution of this effort is EROS itself, a careful
%%% reduction to practice of a high performance capability system.  The
%%% EROS implementation demonstrates that high performance operating
%%% system design and implementation techniques can be applied to
%%% capability systems with positive effect.  EROS delivers favorable
%%% performance on conventional system microbenchmarks, and exceptional
%%% performance on protection domain crossing, which is a critical
%%% facilitating operation for decomposed application architectures.
%%% Further, the EROS primitive mechanisms demonstrably compose without
%%% the performance penalties predicted by architects of monolithic
%%% designs.

%%% It is my hope that EROS may prove useful as a starting point for
%%% future capability system investigations.

%%% \section{Continuing Work}

%%% While the preliminary results from EROS are promising, only a small
%%% fraction of the work applied to competing research projects elsewhere
%%% has been applied to EROS.  Several areas need further exploration
%%% before EROS can stand up favorably to non-expert evaluation.

%%% \subsubsection{POSIX Environment}

%%% \index{POSIX!compatibility environment}
%%% Before EROS can usefully be deployed to a broad audience, the POSIX
%%% compatibility environment must be completed.  This is essential for
%%% self hosting.  More important, it is essential to allow an ``apples to
%%% apples'' evaluation of EROS to be made relative to other systems.

%%% A partial start on a compatibility environment (Dionysix) was
%%% undertaken by University of Pennsylvania undergraduates in 1993.  At
%%% the time, EROS was not sufficiently stable to support this effort, and
%%% the Dionysix project was set aside.  The Dionysix design is basically
%%% sound, and should be carried through.

%%% \subsubsection{Networking Support}

%%% A second requirement for broad deployment is an implementation of the
%%% usual and customary networking support.  An efficient implementation
%%% of TCP/IP in a dekernelized environment presents serious challenges;
%%% address space based protection mechanisms induce additional data
%%% copies, and such copies are well known to be significant and expensive
%%% in the context of networking implementations.  Ultimately, it may
%%% prove necessary to integrate such support within the EROS kernel.

%%% \subsubsection{Java Environment}

%%% \index{POSIX!vs. Java}
%%% \index{Java!compatibility environment}
%%% A possible direction to pursue for EROS is the creation of a
%%% high performance protected Java environment.  While many attempts have
%%% been made to augment Java with serious protection, most of the
%%% potentially promising attempts have been built on POSIX platforms,
%%% which are quite slow at the essential supporting operations.

%%% A native protected Java platform implemented on EROS does not require
%%% full POSIX compatibility, and could take advantage of EROS's greater
%%% efficiency at protection oriented operations.  It would also provide a
%%% basis for direct comparison across platforms provided that the same
%%% JIT compilation technology was used in both implementations.

%%% \subsubsection{Investigation of Demultiplexing}

%%% \index{POSIX!impact of select@impact of \emph{select}()}
%%% One question that has seen limited attention in the message oriented
%%% microkernel literature is the impact of eliminating the POSIX
%%% \emph{select}() system call.  The \emph{select}() call allows a single
%%% process to wait for events of interest on a large number of
%%% simultaneous descriptors.  The \emph{select}() mechanism eliminates
%%% the need for large numbers of threads and therefore reduces the cost
%%% of coordinating data access among these threads.  It remains to be
%%% seen what overhead is induced if multiple threads must be used
%%% instead.

%%% The \emph{select}() operation is not readily implemented in systems
%%% relying on user defined objects.  First, the operations performed by
%%% these objects cannot be readily classified as ``input, output, other''
%%% as the select interface desires.  Second, the necessary operations to
%%% allow a user defined object to ``communicate'' its availability to
%%% other processes may carry higher cost than the obvious multithreaded
%%% design.

%%% \subsubsection{Performance of Checkpointing and Persistence}

%%% EROS implements a very different type of store than has become
%%% customary in other systems, and a different approach to stabilizing
%%% user data.  It has been suggested that the checkpoint mechanism
%%% induces unnecessary overhead, in that a fair portion of the data
%%% included in a given snapshot is ephemeral or efficiently
%%% reconstructable.  No quantitative evaluation has been done to compare
%%% this overhead to the overhead of metadata I/O, nor is it immediately
%%% clear what the right criterion for comparison should be.

%%% A related issue is the fact the EROS kernel has very limited knowledge
%%% of the high level semantics of the objects it is loading from the
%%% disk.  It does not, for example, have any obvious place to stand where
%%% it is able to notice that access to a particular memory object is
%%% sequential, and that performance might therefore be improved by
%%% prefetching.  It would be interesting to investigate both user-hinted
%%% prefetch and mechanisms for dynamic prefetch adaptivity.

%%% \section{The Future of Systems Research}

%%% \index{POSIX!vs. innovation}
%%% The EROS experiment proposes a completely different set of operating
%%% system abstractions and interfaces than has been popularized by the
%%% POSIX architecture.  A worthwhile question to ask at this point is:
%%% ``Is there any future in such experiments?''  If the results of such
%%% experiments cannot ultimately be deployed or adapted into existing
%%% systems, what benefit can accrue from such work?  Two experiences led
%%% me to take a dim view of this future as the EROS work progressed.

%%% \index{KeyKOS!performance}
%%% \index{performance vs. acceptance}
%%% In the late 1980s and early 1990s, KeyKOS provided substantially
%%% better security, performance, and reliability on challenging
%%% applications (VISA transaction processing) than competing systems.  In
%%% an era when most systems were demonstrating MTTF rates of two to seven
%%% days under commodity usage, KeyKOS demonstrated an MTTF exceeding 18
%%% months under heavy transaction loads.  In spite of this, customers
%%% were unwilling to risk critical business operations on a relatively
%%% unknown system.  For reasons of basic software economics, POSIX
%%% compatibility was ultimately more important than the benefits that
%%% KeyKOS offered.

%%% \index{POSIX!compatibility} \index{benchmark!POSIX compatibility} At
%%% about the same time, a trend started in the operating system research
%%% community to demand better quantitative evaluation.  In part, this was
%%% a reaction to incomplete evaluation within the microkernel community.
%%% One result was the creation of a set of microbenchmarks around the
%%% POSIX interface accompanied by social pressure within the field to use
%%% these microbenchmarks for quantitative comparison as a checklist item
%%% for publication.  This shift significantly increased the burden on any
%%% researcher who stands up and says ``POSIX is not the answer.''  While
%%% the benefits of quantitative evaluation are clear, the insistence on
%%% \emph{comparative} benchmarks on novel system designs can be very
%%% frustrating.

%%% Recently, two trends have taken strong hold that potentially reverse
%%% this trend.  The first is Java.  While Java is widely touted as an
%%% \index{architecture neutralizing platform}architecture neutralizing platform for instruction sets, it is
%%% \emph{also} an architecture neutralizing platform for operating
%%% systems.  While various POSIX features are exposed by the Java runtime
%%% libraries, it is not necessary to implement or emulate all of the
%%% POSIX semantics to construct a perfectly acceptable Java environment.
%%% The presence of such a common neutral platform lends itself to more
%%% direct comparison across operating systems with a contained amount of
%%% work.

%%% The second encouraging trend is the emergence of application specific
%%% servers, sometimes known as ``thin servers''.  This trend is an
%%% unavoidable consequence of the falling price of hardware and the pace
%%% of obsolescence on the low end. It is both easier and cheaper to
%%% configure and install a new machine than to rearrange resources on
%%% existing machines to support a new service, and the cost of
%%% maintenance and system management is now understood to be the largest
%%% component of IS department budgets.  The test of a thin server is not
%%% whether it runs POSIX, but whether it provides good NFS or HTTP (or
%%% some such) support.  In such environments, the underlying operating
%%% system is invisible, and the purchaser may largely ignore it in making
%%% a technology decision.

%%% Taken together, these trends offer new opportunities for novel
%%% operating systems.  System implementors are now free to pursue
%%% application specific vertical integration across traditional layering
%%% boundaries.  Alternatively, they are free to port existing server
%%% applications to a new operating system and then evaluate these servers
%%% in black box style.

%%% In short, basic operating system research appears to be entering a new
%%% era of opportunity.  No period of comparable freedom to change basic
%%% abstractions has existed in the last 25 years.  Hopefully, systems
%%% like EROS will be able to influence these designs.

