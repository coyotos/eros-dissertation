\chapter{Related Work}
\label{cha:related}
For an overview of capability architecture and design, excellent
coverage may be found in \cite{Gehringer:CapSystems}.  A more
detailed survey of various early capability systems may be found in
\cite{Levy:CapabilitySystems}.

Several systems have influenced or directly impacted the architecture,
design, or implementation of EROS. These range from CAP and
HYDRA/C.mmp, perhaps the best known of the original capability
systems, to more recent microkernel designs such as Mach and L4.

Similarly, there have been previous efforts to provide formal models
for authority and information transfer in capability systems, and
previous attempts to arrive at proofs of correctness for security
policies in such systems.

In this chapter I review selected related work in these areas, 
with an eye toward understanding how such work relates to EROS and how
EROS differs from what has gone before.

\section{Authority and Information Flow}

Several authors have proposed models for information flow
\cite{Cohen:info-transmission,Denning:Lattice,Bell:model:vol1,Bell:model:vol2,Bell:model:vol3}
and access rights leakage.  Feiertag \cite{Feiertag:Multilevel}
considers the interactions between information flow and access rights,
but in a highly restrictive system model.  Harrison et al. have shown
that access control list systems cannot prevent information
transmission between principals \cite{Harrison:Protection}.

\subsection{Take-Grant}

\index{take-grant|(}
\index{take-grant!vs. diminish-take|(}
\index{diminish-take!vs. take-grant|(}
The most directly relevant access model for EROS is the {\takegrant}
model \cite{Jones:linear,Snyder:Synthesis}, described in detail in
Section~\ref{sec:take-grant}.  Bishop and Snyder
\cite{Bishop:transfer} have examined the information flow consequences
of access right propagation in the {\takegrant} model.  {\Takegrant}
appears to be the first attempt to formalize access rights. The
techniques and approach are instructive, and have been largely ignored
in more recent language based security work
\cite{Balfanz:SecureMultiJava,Islam:FlexxGuard,Wallach:Introspection}.
The {\takegrant} model is similar enough to the {\dimtake} model to
warrant extended discussion.

My original formulation of an access rights model for {\swmodel} was
performed without knowledge of the {\takegrant} model.  In the
interest of framing the new model in the context of previous work, I
eventually reformulated {\dimtake} following the style and terms of
the {\takegrant} model. The essential differences between the two
models are {\dimtake}'s inclusion of indirection and weak access
rights, and the different handling of the \textbf{call} operation.

\subsubsection{Indirect Reference}
\index{diminish-take!indirect access}
Indirect reference has been used by every capability system built to
date.  All current memory protection mechanisms incorporate some form
of address mapping mechanism that uses indirection, and all operating
systems built on these mechanisms incorporate some interface by which
this mapping metadata may be modified by suitably authorized programs.
The inability to model the impact of such metadata on the access model
is a serious limitation in {\takegrant}.

Mark Miller has noted that both indirection and weak reference can be
modeled by injecting trusted processes into the take-grant model.
This approach requires that proofs of correctness be made for the
algorithms executed by such processes, and also that the interjection
of the agent code be completely transparent to the caller.  The
approach breaks down if a capability type checking operation is
included in the system design.

\subsubsection{Weak Access}
\index{diminish-take!weak access}
Weak access guarantees transitive read-only access.  This provides a
means to allow a confined subsystem to safely use prebuilt capability
structures without exhaustively examining the content of those
structures.  In particular, this greatly simplifies the construction
of memory objects.

If the structures to be shared can be statically constructed prior to
client execution, it is possible to achieve the same result by
abstracting up one level and using trusted construction agents that
obey some suitably restricted language for structure assembly.  These
agents build structures following a specification provided by the
developer, but to which the developer's agents have no direct access.
The main argument against this approach is the significant increase in
number and complexity of objects that must be correctly implemented to
ensure that information does not leak across unauthorized boundaries.

\index{copy on write}
A second concern with this approach arises in contexts requiring
dynamically generated content, such as copy-on-write behavior.  In the
current EROS design, the virtual copy keeper may be trusted by virtue
of the fact that it contains no initial mutable capabilities (this is
a constructive argument) and that its authority to access the
underlying address space is by way of a weak capability.  In the
absence of the weak mechanism, a copy of the input space would need
to be made at the time it is frozen, which would carry a substantial
performance penalty.

\subsubsection{The Call Operation}

\index{diminish-take!call operation}
As originally described in \cite{Jones:linear}, the
{\takegrant} model included a \textbf{call} operation that created a
new process in stack-like fashion.  In later descriptions of the
model, this operation was dropped; all of the behavior supported by
the \textbf{call} operation as originally described was possible given
only the other access rights.
The original description of the \textbf{call} operation imposed stack
discipline, that eliminated the {\takegrant} model's ability to
model coroutine behavior.

The real challenge in the \textbf{call} operation, which was not
addressed in any of the {\takegrant} literature, was the fact that
coroutine call is a reflexive operation; the ability to reply to the
caller means that call rights are bidirectional.  {\Dimtake} addresses
this point fully, and demonstrates that it does not substantially
change the ability of a system to restrict information flow.

%%%He cites KeyKOS, which accomplished both objectives, eight pages
%%%later.  It becomes apparent from the text that he did not recognize
%%%the significance of the KeyKOS ``sense'' capability (a capability
%%%whose rights are described by the \Metagape\ rights set
%%%\set{\indir{\nrtweak},\access{\nrtweak}}).  It also appears that he
%%%did not consider the possibility of construction under trusted audit
%%%as a means of achieving confinement within the take/grant model.
\index{take-grant!vs. diminish-take|)}
\index{diminish-take!vs. take-grant|)}
\index{take-grant|)}

\subsection{Multilevel Security}
\index{MLS|see{multilevel security}}
\index{multilevel security|(}

\index{information flow!lattice model}
\index{information flow!multilevel security}
\index{lattice model}
\index{access control!lattice model}
Denning proposed a lattice model for information flow
\cite{Denning:Lattice} that described the possible information
transfer in multilevel secure systems \cite{Feiertag:Multilevel}.
The proof of confinement presented for EROS is a precursor step to
multilevel security.  Only when the communications of a process can be
completely constrained by confinement can one begin to safely relax the
communication constraints to permit upward communication within a
lattice if the lattice policy is desired.

\index{multilevel security!vs. capability systems} Paul Karger
\cite{Karger:Thesis} has asserted that an unmodified capability system
cannot enforce confinement.  In light of the fact that this is the
only outstanding contradiction in the literature to the proof
presented in this dissertation, I discussed this point in depth with
Karger in person.  It emerged that his working definition of
confinement was different from the one used in this dissertation, and
that by ``unmodified capability system'' he meant a capability system
as defined by Van Horn in \cite{Dennis:Capabilities}.  His assertion
should therefore be taken to mean that a {\takegrant} system cannot
enforce multilevel security.

The root of the problem lies in the fact that a {\takegrant} system
allows capabilities to be transferred from a less secure compartment
to a more secure compartment.  These capabilities can then be used by
the more restricted program to write data that is accessible to the
less secure program.  In EROS, the combination of branding and
weakening can be used to construct \index{reference monitors}reference
monitors that restrict downward information flow.  An
upward-transferred directory object, for example, can be detected as
such by the monitor, and can be encapsulated by a directory monitor
that returns monitored files.  These monitored files in turn guarantee
that writes to these files are not permitted.

While the introduction of such \index{reference monitors}reference
monitors does allow multilevel security to be enforced, Karger's
objection remains accurate in the sense that the correctness of this
solution relies on the correctness of the monitors, and this
correctness cannot be shown within the access model.
\index{multilevel security|)}

\subsection{BAN Logic}

The BAN logic
\cite{Burrows:LogicAuthentication,Abadi:AuthenticationSemantics} is a
logic and accompanying semantics for authentication protocols.  These
protocols operate by evolving a commonly held set of beliefs between
two or more parties based on the exchange of various pieces of
protected information. The end goal is for all parties to agree on the
identity of a principal.

BAN logic does not make assumptions about the authority to transmit
information.  Indeed, a particular concern of authentication protocols
is to defend against \emph{unauthorized} communications.  The purpose
of the BAN logic is to examine whether beliefs evolving from the steps
of a particular authentication protocol are well founded.

If ``principals must be authenticated'' is taken to be a security
policy, the BAN logic provides a mechanism for determining whether a
particular algorithm for authentication is correct.  It should be
noted, however, that the logic itself rests on several assumptions
detailed in \cite{Burrows:LogicAuthenticationScope}, and one that is
not: the assumption that encryption keys can be kept secret.  In the
absence of a means for controlling information flow, the ability to
satisfy this assumption must be treated with skepticism. The
confinement proof of Chapter~\ref{cha:proof} shows that encryption
keys can be protected by members of the {\swmodel} family.
Conversely, the proofs of \cite{Harrison:Protection} show that
equivalent protection is either undecidable or infeasible in access
list systems.

\section{Correctness of Confinement: PSOS}

\index{confinement!PSOS|(} \index{proof!PSOS|(} \index{PSOS|(} To my
knowledge, the proof of correctness for confinement presented in
Chapters~\ref{cha:model} and~\ref{cha:proof} is the first successful
proof of its kind.  Prior to this proof, the only attempt to arrive at
a proof of this kind is PSOS \cite{PSOS:Report}.

PSOS is a capability system constructed at SRI in the mid 1970s.  The
PSOS team created both a formal specification language (SPECIAL, the
SPECIfication and Assertion Language) and a development methodology
(HDM) for secure systems, and used these to construct a provably
multilevel secure system.  Along the way, they built a number of
impressive automated theorem proving tools.  Their report includes
both the critical portions of the system specification and a proof
sketch of the security properties of this system.

Unfortunately, the proof sketch outlined in \cite{PSOS:Report} is
flawed.  The proof sketched in that document demonstrates that the
specification as written in SPECIAL matches the desired security
properties, but it fails to demonstrate that the operational semantics
of the system architecture actually satisfies the specification.
Indeed, the whole approach was abandoned in favor of the Secure Ada
Target \cite{Boebert:SecureAdaTarget}.  The {\swmodel} proof is the
first proof to accomplish
this.\index{PSOS|)}\index{proof!PSOS|)}\index{confinement!PSOS|)}

\section{Capability Systems}
\index{KeyKOS!relationship to EROS} \index{EROS!relationship to
  KeyKOS} No discussion of related work on a capability system would
be complete without addressing the University of California's CAL/TSS,
the Cambridge CAP system, and Carnegie Mellon's HYDRA/C.mmp effort.
Since EROS is architecturally derived from KeyKOS, I also describe the
differences between these two systems.

\subsection{CAL/TSS}
\index{CAL/TSS|(}

The CAL system \cite{Levy:CapabilitySystems} predated HYDRA and
Multics, and was borrowed from heavily by both.  The project was
halted before producing a fully usable system, so it is difficult to
draw performance conclusions from the result.  Like HYDRA/C.mmp, CAL
is a pure capability system.  Also like HYDRA, its most interesting
attribute for the purposes of this dissertation is its messaging
system.

\index{interprocess communication!CAL/TSS} CAL provides two message
types: \emph{events} and \emph{operations}.  An \textbf{event} is a
single word message transmitted on an event channel.  Each channel
provides storage for a fixed number of events, and a process can
receive in either blocking or non-blocking fashion on multiple event
channels.  If the event channel is full, the event is lost.  The
balance of the discussion here will focus on operations.

The closest analogue to a message in CAL is the {\bf operation}.  
An operation is performed by invoking a capability with a list of
parameters.  The parameter list is bundled up into a message and send
to the recipient domain.  The underlying assumption of this model is
that capability invocations follow a call/return style of usage.

The most notable difference between the CAL messaging
system and the HYDRA messaging system lies in what was removed:
\begin{itemize}
\item CAL has no notion of ports or channels.  A message is sent to a
  domain. The capability names the entry point in the domain at which
  the recipient thread should resume.
\item CAL messages have no embodiment outside of the sending and
  receiving process.  The message has no independent identity.
\item Messages are unbuffered.  If the recipient domain is
  unavailable, the sender will block until the recipient is available
  to process the operation.
\end{itemize}
As a result of these simplifications, the CAL messaging system is
considerably faster than that of HYDRA.

\leadin{Payload} A CAL message consists of an unbounded number of {\em
  parameters}, each of which may be a word or a
capability.\footnote{I elide the complexities of ``fixed''
  capabilities and parameter words because they do not bear on the
  problems of message transport.} Capabilities reside in
system-managed trusted storage, and are named by their capability list
slot number.

\leadin{Invocation} The CAL messaging system supplies a procedural
discipline for message invocation.  While HYDRA implemented operations
called {\bf Send}, {\bf Receive} and {\bf Reply}, and these were
frequently used according to a call/return discipline, the entire
description of CAL operations is framed in terms of invoking a service
and blocking for a response. One of the stated objectives of the CAL
design was to adopt a client/server model of application construction,
and the message design was constructed accordingly.

\leadin{Threading} In contrast to HYDRA, recipients in CAL are
activated by thread migration associated with the operation rather
than performing a {\bf Receive} operation.  The occupancy of a domain
is captured in per-domain runtime state information, and operation
queueing is done when the receiving domain is busy. This is analogous
to the \emph{available} state of EROS.

Lampson identifies a few aspects of the CAL system that could be
improved with the benefit of hindsight \cite{Lampson:Reflections}.
Most significantly, he suggests that the concepts of domains and
processes, which were separated for reasons of efficiency, should have
been unified.  This would have eliminated the need for two independent
messaging systems, resulting in considerable simplification of the
underlying system.  This idea was incorporated in KeyKOS, and was
carried forward into EROS.
\index{CAL/TSS|)}

\subsection{CAP}

\index{CAP|(}
CAP is a capability system implemented in microcoded hardware
\cite{Wilkes:CAP} at Cambridge University.  The CAP project started in
1970 as an investigation of memory protection architectures, and most
of its capability structures consequently focus on memory objects.
Much of the memory architecture of the later x86 family is anticipated
in the CAP design.  A discussion of the protection architecture of CAP
is sketched in \cite{Herbert:CAP}.


As with EROS, capabilities and data in CAP are partitioned.  In CAP,
this partitioning is enforced by tagging each memory segment with a
type bit indicating whether it contains capabilities or data.

One of the more interesting aspects of the CAP memory architecture is
the incorporation of \emph{relative capabilities}.  The process model
of CAP assumed nested processes (similar to the nested process model
currently explored by Ford and Lepreau
\cite{Ford:RVM,Ford:CpuInheritance}).  The initial memory of lower
level processes was most commonly allocated from the memory of its
parent process.  In practice this nesting never exceeded a depth of
two in the CAP system, and indirection costs associated with relative
capabilities were therefore never fully explored.

\index{interprocess communication!CAP}
The unit of protection in CAP is the \emph{protected procedure}.  In
modern terminology, a protected procedure is best thought of as a
module accompanied by a set of authorities.  Protected procedures are
finer grain than processes in CAP, and processes may export to other
processes the ability to directly call a protected procedure that it
contains.  Cook \cite{Cook:CAP} explores the cost of this invocation
mechanism, normalizing results to the cost of a memory reference.  A
protected invocation passing one capability and returning one
capability with no other payload took 222.3 memory reference times.
In evaluating this cost, it must be remembered that CAP implemented no
data cache, and that this number is therefore a multiple of
\emph{main} memory reference.  While comparisons are necessarily
suspect in light of the differences between the two architectures, the
nearest equivalent EROS implementation (which transfers and returns
four capabilities) takes 110 main memory times to perform a call and
return.
\index{CAP|)}

\subsection{HYDRA/C.mmp}
\index{HYDRA/C.mmp|(}

The most notable characteristic of Hydra is its novel messaging
architecture.  This architecture stands at the opposite extreme to the
EROS design around most axes: it is connection oriented, uses buffered
messages, and implements both multicast and unbounded payloads.

\index{IPC|see{interprocess communication}}
\index{interprocess communication!HYDRA}
The HYDRA messaging architecture \cite{Wulf:HYDRA} is built around
four concepts: {\em ports}, {\em connections}, {\em messages}, and {\em
  replies}.  Ports provide the endpoints for a connection. Input and
output ports are distinguished, and an arbitrary number of output
connections can be made to a single input port (fan-in=1,
fan-out=many). The message subsystem is built on a store and forward
model.  Collectively, these features support the construction of an
essentially arbitrary message topology, up to and including circular
paths.

Hydra messages have unique identities.  Receipt of a message means
that the message has been placed in a ``message slot,'' but does not
imply that the message data has been copied into the receiver address
space. A receiving process possesses a dynamically sized pool of
message slots, and copies the message payload into its address space
when it reads the message.

A HYDRA domain can receive a message, examine and modify its content,
and then forward the same message (in the sense of having the same
identity) through an output port to (possibly more than one) other
domain(s).  Each message contains a ``reply stack,'' which is
essentially a list of all of the processes that have performed a {\bf
  send} on the message. Replies can be directed to the most recent
sender, or to any sender on the reply stack.

With the benefit of hindsight, Wulf et. al. identified several
weaknesses in the Hydra message subsystem \cite{Wulf:HYDRA}.

\leadin{Complexity} The complexity of the topology features, in Wulf's
view, led to a cumbersome interface.  In addition, the distinction
between ports and channels did not mesh well with the protection
architecture.  Wulf notes that the capabilities involved named ports,
and one often wanted to manipulate a (port, channel) pair to forward a
channel.  This was difficult because (port, channel) pairs were not
separately named entities.

\leadin{Second Class Citizens} Messages in Hydra are not ``objects,''
which means that they cannot be migrated to secondary storage.  One
consequence is that messages often clog the system's main memory.  In
Wulf's view, the solution was to make messages persistent, first class
objects.

\leadin{Timed Receive}
Messages are received by domains which are assumed to be running and
actively waiting for a message to arrive (i.e. there is a {\bf
  Receive} service call).  In hindsight, the designers wished for a
mechanism to bound the amount of time that such a domain would be tied
down waiting on a port.

\leadin{Messages vs. Calls} Wulf notes that messages were actually
faster than the procedure call mechanism in Hydra, which led people to
build many small protection domains in preference to using procedures.
In retrospect, he notes that development energies would have been
better spent on improving the performance of the call instruction than
implementing such a rich connective fabric.

\leadin{Payload} Messages in Hydra are of unbounded length.  Wulf et
al. note that the common cases of message sends were file block
transmission and transmission of two to three parameters, which
suggests that a small bounded message might have resulted in a simpler
and more efficient system.  Given the trend to smaller and smaller
domains that was evident in Hydra, the ability to send more than one
capability within a message would have been of value.
\index{HYDRA/C.mmp|)}



\subsection{KeyKOS}

\index{KeyKOS!relationship to EROS}
\index{EROS!relationship to KeyKOS}
EROS is a direct architectural descendant of the KeyKOS system
\cite{KeyKOS:NanoKernel,KeyKOS:Architecture}.  Architecturally, the
differences between the two systems are largely evolutionary.  The
EROS capability invocation mechanism has been completely redesigned in
the wake of subsequent advances by Liedtke \cite{L3:IPC} and Ford
\cite{MACH4:Evolving}, and transparent address space multiplexing
\cite{L3:SpaceMux} has been implemented in the x86 version of the EROS
kernel.

The main contributions of EROS relative to KeyKOS are a more capable
IPC implementation, the proof of correctness for the KeyKOS
confinement mechanism, and a more careful performance analysis.

\section{Access Control List Extensions}

Abadi et. al describe an extended access control list mechanism in
\cite{Abadi:AuthenticationCalculus} with several advantages over the
base model.  Their paper introduces the notion of ``role based''
authorization, in which an access list for object $o$ can say that $o$
is accessable to a principal $p$ acting in role $r$ (written $p as
r$), but not to principal $p$ acting in other roles.  The paper
addresses both the completeness of the access list specification
language and the resulting legal transformations and simplifications
on access lists that are feasible and safe.  It does not explore
whether higher-level security policies can be enforced using
role-based access control lists.

While role-based access control lists do not fully address the issue
of least privilege they go a long way toward solving the practical
problem.  In a role-based system, one can define an \emph{untrusted}
role, and run untrusted software under the role $myself\ as\ untrusted$,
thereby restricting the access rights available to that software.
Role-based access control lists do not address any of the other
requirements of \ref{fig:cap-properties-reprise} (a reproduction of
Figure~\ref{fig:cap-properties}).  
\begin{figure}[htbp]
  \begin{center}
    \begin{description}
    \item[\textbf{Least Privilege}] \index{least privilege, principle
        of}\index{principle!least privilege}Programs should have no more
      authority than they require.
    \item[\textbf{Selective Access Right Delegation}] \index{access
        rights!selective delegation}\index{delegation, access
        right}\index{principle!selective delegation}A program that
      possesses authority should be able to selectively delegate that
      authority to its component subprograms.
      
      It must be possible to run components with lesser authority than
      that of their principal.
    \item[\textbf{Rights Transfer Control}] \index{rights transfer
        control}\index{principle!rights transfer control}A (sub)program
      should be able to receive additional authority only if that
      authority is granted via an explicitly authorized channel.
    \item[\textbf{Information Transfer Control}] \index{information
        transfer control}\index{principle!information transfer control}A
      (sub)program should be able to exchange informatoin only by way of
      an explicitly authorized channel.
    \item[\textbf{Endogenous Verification}] \index{endogenous
        verification}\index{principle!endogenous verification}It must be
      possible to verify from within the system itself that certain
      restrictions on rights transfer and information flow are met.

      In many systems, such tests are possible only if the examiner is in
      a position to stand outside the system and examine all of its
      state at some instant.
%%%   Systems must be \emph{seen} to be secure.\footnote{``Justice must
%%%     not only be done, it must be seen to be done.'' Roy v. Jones, 349
%%%     F. Supp.  315, 319 (W.D. Pa.  1972), aff'd,
%%%     {\textless}=42{\textgreater} 484 F.2d 96 (3d Cir. 1973)}
    \end{description}
    
    \caption{Basic properties supported by capabilities}
    \label{fig:cap-properties-reprise}
  \end{center}
\end{figure}


Selective access right delegation is not supported by role-based
access control lists, because \emph{access} to an object is different
from \emph{control} of that object.  To selectively delegate access,
the principal must have sufficient authority to modify the access
control lists on the delegated objects.  This could be resolved if a
notion of role ``membership'' or dominance were introduced, and if any
user were authorized to add an access list entry conveying a subset of
the authority they already hold.

The role-based access control model similarly does not provide for the
restriction of access rights transfer. It remains possible within this
model for a third party who controls an object to grant rights to 
$p\ as\ untrusted$, for example.  The model therefore does not defend
well against trojan horses.  Because it cannot restrict the transfer
of access rights, it cannot restrict the transfer of information
between colluding parties.

Finally, role-based access control lists do not provide for efficient
endogenous verification.  While it is possible for a program to scan
the initial state of the system to determine that $p\ as\ untrusted$
does not have undesirable access rights, there is no way to prevent
those access rights from changing as execution proceeds.


\section{Microkernels}

Two microkernel systems are particularly relevant to the EROS system:
L4 and Mach version 4.

\subsection{L4}

\index{L4|(}
L4 is a small microkernel designed with one overriding goal:
constructing the fastest IPC mechanism possible without sacrificing
protection.  L4 provides no process model, no memory management
services, no scheduler, no device drivers, and no persistent store.
It is intended that all of these will be implemented by
non-microkernel applications.

\index{interprocess communication!L4|(}
The most interesting aspect of L4 is its careful attention to the
systemic interactions between address spaces, data motion, and
inteprocess communication (IPC) efficiency.  The result is an IPC
mechanism that is currently the fastest implementation in existence on
all platforms supported by L4.

Liedtke's success in this work directly prompted EROS's re-evalution of
the capability invocation mechanism.  The research question posed by
EROS was whether similar efficiencies could be obtained under two
additional constraints:
\begin{itemize}
\item Invocation must be restricted to those holding an authorizing
  capability.  L4 allows any process to invoke any other process.
\item All capability invocations must continue to obey the same
  interface, which is essential to supporting transparent
  \index{reference monitor}reference monitors.
\end{itemize}

One point that Liedtke argued was essential to the performance
achieved by L4 was to keep the critical working set small.  Given that
the EROS kernel implements a process model, flexible memory
management, a persistent store, and device drivers, there was reason
to question whether a similarly small working set could be achieved.

\index{interprocess communication!EROS}
Work on the first EROS IPC redesign basically showed that it can
\cite{EROS:IPC}.  That version is cycle for cycle comparable to L4 in
terms of performance.  While the latest generation of the EROS IPC
mechanism has not yet been as carefully optimized, comparable results
should be achievable in the current design.

A Linux port has been built on top of the L4 nucleus
\cite{Hartig:Performance}.  This port demonstrates the feasibility of
operating system rehosting, but runs at a 5\% to 10\% performance
penalty relative to Linux.  This hit comes primarily from the fact
that the Linux memory management logic has not been adapted to make
direct use of L4 primitives.  Work on producing such an integrated
port is currently underway (personal communication).

In its current incarnation as IBM's Lava nucleus, L4 remains an
incomplete system.  While L4's IPC mechanism is extremely fast, it
remains unclear whether its memory management primitives compose in a
sufficiently flexible fashion to support a high performance decomposed
system running in native form.\index{interprocess communication!L4|)}\index{L4|)}

\subsection{Mach 4}

\index{Mach|(}
\index{interprocess communication!Mach|(}
Bryan Ford pursued a redesign and reimplementation of the Mach 4 IPC
mechanism in the wake of Liedtke's success \cite{MACH4:Evolving}.
Mach's IPC implementation has been a continuing source of
disappointment within the microkernel community.  Ford explored what
benefit might result if message buffering was eliminated and thread
migration were introduced.

While remarkable gains arise from these two changes, nothing short of
a complete redesign of the Mach IPC interface can make it competative.
There are two basic problems with the Mach design:
\begin{itemize}
\item There are simply too many options.  The number of interactions
  between the IPC invocation descriptor and the control flow of the
  IPC implementation preclude good instruction cache density in the
  critical path.
\item Allocating memory during IPC destroys cache locality.  The Mach
  invocation mechanism requires that new ``slots'' be allocated in the
  recipient's port table, which requires that the table be searched
  for available slots and potentially grown dynamically.
\end{itemize}

Ford was extremely generous with his insights and experience resulting
from this implementation. Without the benefit of his comments, it is
unlikely that EROS would have achieved its current performance.
\index{interprocess communication!Mach|)}
\index{Mach|)}

\section{Closing Comments}

The discussion of related work has omitted some systems that arguably
should be included.  Chorus Syst\`{e}mes, for example, has implemented
a descriptor oriented distributed system built on a nucleus with a
relatively high performance invocation mechanism
\cite{Chorus:Overview}.  Like Mach, Chorus evolved from monolithic
systems, and the performance of the two systems is roughly comparable.
A comparison of the two is available in the literature.  Chorus had no
direct impact on EROS.

Little is to be gained by an exhaustive survey of prior operating
systems.  Instead, I have chosen to focus here on work that either
directly influenced EROS or so closely bears on the issues addressed
in this dissertation that comparative evaluation is required.

