%% -*- latex -*-
%%
\abstractsection
\centerline{\textbf{\LARGE EROS: A CAPABILITY SYSTEM}}
\bigbreak
\centerline{Jonathan Strauss Shapiro}
\centerline{David J. Farber}
\centerline{Jonathan M. Smith}
\bigbreak
\bigbreak

Capability-based operating systems have logical advantages over
access-control list based systems for security, and potential
advantages for performance as well.

This dissertation makes four contributions.  First, it extends the
capability access model proposed by Jones, Lipton, and Snyder and
enhanced by Bishop to address object metadata, opaque indirection, and
certain additional access checks.  These extensions bridge the gap
from the original model to the behavior of real systems, and are
necessary to account for securely dekernelized memory and file
management.

Second, it provides a formal model of capability architectures,
{\swmodel,} that allows precise statements to be made about access and
operations.  {\Swmodel} makes it possible to prove that the conditions
imposed by some implementation actually enforce the security policy
they are designed to support. In particular, I have shown this for the
confinement policy.  {\Swmodel} defines a family of capability
systems, of which any realization satisfies the proven security
properties.  Derived from the EROS architecture, the {\swmodel} family
includes at least one complete, general purpose operating system
architecture that has actually been implemented.  {\Swmodel} is the
first formal model of its kind for which proofs of security policies
have been favorably completed; previously successful proof efforts
have proven only negative results.

Third, this dissertation presents the Extremely Reliable Operating
System (EROS).  EROS is one realization of an {\swmodel} system.  It
discusses the EROS virtual machine architecture, and describes how
the capabilities defined by that architecture are mapped to
{\swmodel}.  By virtue of this mapping, the EROS design is covered
by both the {\swmodel} model and the proof of confinement presented
here.

Finally, this dissertation presents a prototype implementation of
the EROS system on the Intel x86 machine architecture.  By mapping
EROS virtual machine abstractions to hardware-supported objects,
this implementation yields measured performance competative with,
and in many cases exceeding, the performance of conventionally
architected ACL-based operating systems and microkernels.
Quantitative evaluation presented here shows that neither the use of
capabilities nor aggressive dekernelization needs to be a limiting
factor in system performance.


%%% Capability systems have claimed 
%%% both
%%% security 
%%% and reliability
%%% advantages over access list systems and their derivatives.  In spite
%%% of this, no specific capability architecture has been identified that
%%% demonstrates these properties, nor has the correctness of such an
%%% architecture with respect to certain critical security policies been
%%% shown.  Supposing that such an architecture existed, no existing
%%% \emph{implementation} of a capability system has delivered performance
%%% competitive with access list based, monolithic system architectures.

%%% This dissertation formally defines a family of capability systems,
%%% \swmodel, that supports the enforcement of application defined
%%% security policies and is amenable to high performance implementation.
%%% In collaboration with Sam Weber, I have crafted a formal definition
%%% of the confinement property and proven that {\swmodel} can enforce
%%% this property.  This is the first semantically rich capability model
%%% for which the confinement property has been shown to be endogenously
%%% decidable.

%%% This dissertation also presents a careful reduction to practice of one
%%% member of the {\swmodel} family: EROS.  The EROS implementation
%%% demonstrates that high performance operating system design and
%%% implementation techniques can be applied to capability systems with
%%% equivalent results.  EROS delivers favorable performance on
%%% conventional system microbenchmarks, and exceptional performance on
%%% protection domain crossing, which is a critical facilitating operation
%%% for dis-integrated application architectures.

%%% The results of this effort show that fast capability systems are
%%% pragmatically feasible.  In addition, they suggest that existing
%%% application environments can be ported to a capability substrate
%%% without substantial loss of performance, which is a necessary
%%% precondition to the practical acceptance of this technology.
