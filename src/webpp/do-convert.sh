#!/bin/sh
FILE_NAME=`echo ${PATH_TRANSLATED} | sed 's@/home/eros/public_html/www/eros-src/@@'`
DIR_NAME=`dirname ${PATH_INFO}`
DIR_LABEL=`dirname ${FILE_NAME}`

# Locate the appropriate tags file:
CUR_DIR=`dirname ${FILE_NAME}`
TRUE_DIR=`dirname ${PATH_TRANSLATED}`
TAGS_PATH="./"
THE_TAGS_FILE=""
while true
do
#    echo CUR_DIR ${CUR_DIR} > /dev/console
#    echo TRUE_DIR ${TRUE_DIR}/${TAGS_PATH} > /dev/console

    if [ -r "${TRUE_DIR}/${TAGS_PATH}TAGS" ]
    then
	THE_TAGS_FILE="${TAGS_PATH}TAGS"
	break;
    fi

    if [ "${CUR_DIR}" = "." ]
    then
	break;
    fi
    if [ "${CUR_DIR}" = "/" ]
    then
	break;
    fi

    CUR_DIR=`dirname ${CUR_DIR}`
    TAGS_PATH="../${TAGS_PATH}"
done

# Raw HTML files just get passed through directly.  If it's a source file,
# find the enclosing TAGS file if any:
case ${PATH_TRANSLATED} in
   *.html)  # we should never see this
	    exit -1;;
   *.c|*.cxx|*.h|*.hxx)
	    TAGS_FILE=${THE_TAGS_FILE}
	    if [ -n "${TAGS_FILE}" ]
	    then
		TAGOPT="-t ${TRUE_DIR}/${TAGS_PATH}TAGS -p ${TAGS_PATH}"
	    fi
	    ;;
   *.S|*.l|*.y)
	    TAGS_FILE=${THE_TAGS_FILE}
	    ;;
   *)
	    ;;
esac


cat <<EOF
<html>
<style>
<!--
BODY {
	background: #ffffe0;
	line-height: 115%;
	font-size: 100%;
	color: black;
}

/* following not highlighted unless CSS supported. */
span.kwd {
    color: purple;
}
span.tkwd {
    color: purple;
}

font.string {
    color: green;
}

b.type {
    font-weight: bold;
    color: blue;
}

b.macro {
    font-weight: bold;
    color: blue;
}

b.func {
    font-weight: bold;
    color: blue;
}

b.preproc {
    font-weight: bold;
}

em.comment {
    font-style: italic;
    color: red;
}

font.incfile {
    /* text-decoration: underline; */
}
-->
</style>
EOF
echo "<title>${FILE_NAME}</title>"
echo '<BODY BGCOLOR="#FFEEDD">'
echo "<font color=red>File: <b>${FILE_NAME}</b> "
echo "(Automatically pretty-printed by webpp...)</font><br>"
echo "<font color=red>Directory: <a href=\"${DIR_NAME}\">${DIR_LABEL}</a></font><br>"
if [ -n "${TAGS_FILE}" ]
then
    echo "<font color=red>Cross-reference: <a href=\"${TAGS_FILE}\">TAGS</a></font><br>"
fi

echo "&nbsp;<br>"
echo "<hr>"
#echo "<b>${PATH_INFO}</b><br>"
#echo "<b>${PATH_TRANSLATED}</b><br>"

XLATE=/home/shap/tools/webpp/miscpretty
EXPAND=expand

isaout=`file ${PATH_TRANSLATED} | grep 'executable,'`

if [ -n "$isaout" ]
then
    EXPAND="objdump --headers"
else
    case ${PATH_TRANSLATED} in
	*.S)        XLATE=/home/shap/tools/webpp/asmpretty ;;
	*.c)        XLATE=/home/shap/tools/webpp/cpretty ;;
	*.cxx)      XLATE=/home/shap/tools/webpp/cpretty ;;
	*.h)        XLATE=/home/shap/tools/webpp/cpretty ;;
	*.l)        XLATE=/home/shap/tools/webpp/cpretty ;;
	*.y)        XLATE=/home/shap/tools/webpp/cpretty ;;
	*.o)        EXPAND="objdump --headers" ;;
	*.hxx)      XLATE=/home/shap/tools/webpp/cpretty ;;
	*.sh)       XLATE=/home/shap/tools/webpp/shellpretty ;;
	*.map)      XLATE=/home/shap/tools/webpp/mappretty ;;
	*.mk)       XLATE=/home/shap/tools/webpp/makepretty ;;
	*/Makefile) XLATE=/home/shap/tools/webpp/makepretty ;;
	*/TAGS)     EXPAND=/home/shap/tools/webpp/etagspretty;
                    XLATE=cat;
		    TAGOPT="";;
	*.html)     ;;  # prevent mishandling
	*)          XLATE=/home/shap/tools/webpp/miscpretty;;
    esac
fi

# echo > /dev/console
# echo "${EXPAND} ${PATH_TRANSLATED} | ${XLATE} ${TAGOPT}" > /dev/console
${EXPAND} ${PATH_TRANSLATED} | ${XLATE} ${TAGOPT}
#${EXPAND} ${PATH_TRANSLATED} | ${XLATE}
echo "</body>"
echo "</html>"
