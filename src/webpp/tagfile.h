/* -*- Mode: c -*- */
/*
 * Copyright 1998 by Jonathan Shapiro.
 *
 * This file is part of EROS, the Extremely Reliable Operating System.
 * EROS is NOT free software, but may be redistributed under the terms of
 * the EROS License Agreement.
 *
 * A copy of the EROS License Agreement has been included with the
 * distribution, and may be found in the file legal/license/EROS.html.
 */

/* External interface to support routines that load in a tags file */

typedef struct etags_entry {
  const char *filename;
  const char *classname;
  const char *symname;
  int line;
  int entry_type;
} etags_entry;

extern etags_entry **etags_ci_sym_index;
extern etags_entry **etags_ci_qual_index;
extern etags_entry **etags_qual_index;

extern int etags_nsym;
extern const char *etags_globfun;

int etags_load(const char *);
etags_entry **etags_find_class(const char *name);
etags_entry **etags_find_fn(const char *classname, const char *symname);
etags_entry **etags_find_unique_fn(const char *symname);
const char *etags_find_parent_class(const char *name);
