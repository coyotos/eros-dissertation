#define EROS_DOC_WEB "http://www.eros-os.org"

unsigned int cur_line = 1;

const char *begin_comment = "<em class=comment>";
const char *end_comment = "</em>";
const char *begin_kwd = "<span class=kwd>";
const char *end_kwd = "</span>";
const char *begin_tkwd = "<span class=tkwd>";
const char *end_tkwd = "</span>";
const char *begin_type = "<b class=type>";
const char *end_type = "</b>";
const char *begin_macro = "<b class=macro>";
const char *end_macro = "</b>";
const char *begin_func = "<b class=func>";
const char *end_func = "</b>";
const char *begin_string = "<font color=green class=string>";
const char *end_string = "</font>";
const char *begin_incfile = "<font color=green class=incfile>";
const char *end_incfile = "</font>";
const char *begin_preproc = "<b class=preproc>";
const char *end_preproc = "</b>";

#if 0
char *begin_comment = "<em class=comment>";
char *end_comment = "</em>";
char *begin_kwd = "<span class=kwd>";
char *end_kwd = "</span>";
char *begin_tkwd = "<span class=tkwd>";
char *end_tkwd = "</span>";
char *begin_type = "<b class=type>";
char *end_type = "</b>";
char *begin_macro = "<b class=macro>";
char *end_macro = "</b>";
char *begin_func = "<b class=func>";
char *end_func = "</b>";
char *begin_string = "<font color=green class=string>";
char *end_string = "</font>";
char *begin_incfile = "<font color=green class=incfile>";
char *end_incfile = "</font>";
char *begin_preproc = "<b class=preproc>";
char *end_preproc = "</b>";
#endif

#if 0
char *begin_comment = "<em class=comment>";
char *end_comment = "</em>";
char *begin_kwd = "<span class=kwd>";
char *end_kwd = "</span>";
char *begin_tkwd = "<span class=tkwd>";
char *end_tkwd = "</span>";
char *begin_type = "<b class=type>";
char *end_type = "</b>";
char *begin_macro = "<b class=macro>";
char *end_macro = "</b>";
char *begin_func = "<b class=func>";
char *end_func = "</b>";
char *begin_string = "<font color=green class=string>";
char *end_string = "</font>";
char *begin_incfile = "<font color=green class=incfile>";
char *end_incfile = "</font>";
char *begin_preproc = "<b class=preproc>";
char *end_preproc = "</b>";
#endif

#if 0
char *begin_comment = "<em class=comment>";
char *end_comment = "</em>";
char *begin_kwd = "<span class=kwd>";
char *end_kwd = "</span>";
char *begin_tkwd = "<span class=tkwd>";
char *end_tkwd = "</span>";
char *begin_type = "<b class=type>";
char *end_type = "</b>";
char *begin_macro = "<b class=macro>";
char *end_macro = "</b>";
char *begin_func = "<b class=func>";
char *end_func = "</b>";
char *begin_string = "<font color=green class=string>";
char *end_string = "</font>";
char *begin_incfile = "<font color=green class=incfile>";
char *end_incfile = "</font>";
char *begin_preproc = "<b class=preproc>";
char *end_preproc = "</b>";
#endif
#if 0
char *begin_comment = "<em class=comment>";
char *end_comment = "</em>";
char *begin_kwd = "<span class=kwd>";
char *end_kwd = "</span>";
char *begin_tkwd = "<span class=tkwd>";
char *end_tkwd = "</span>";
char *begin_type = "<b class=type>";
char *end_type = "</b>";
char *begin_macro = "<b class=macro>";
char *end_macro = "</b>";
char *begin_func = "<b class=func>";
char *end_func = "</b>";
char *begin_string = "<font color=green class=string>";
char *end_string = "</font>";
char *begin_incfile = "<font color=green class=incfile>";
char *end_incfile = "</font>";
char *begin_preproc = "<b class=preproc>";
char *end_preproc = "</b>";
#endif
