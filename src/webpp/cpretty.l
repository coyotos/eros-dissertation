/* -*- Mode: c -*- */
/*
 * Copyright 1998 by Jonathan Shapiro.
 *
 * This file is part of EROS, the Extremely Reliable Operating System.
 * EROS is NOT free software, but may be redistributed under the terms of
 * the EROS License Agreement.
 *
 * A copy of the EROS License Agreement has been included with the
 * distribution, and may be found in the file legal/license/EROS.html.
 */

/* This translator assumes that the input has been run through the
   expand program, so that there are not tabs.  Keeping track of input 
   position is otherwise more of a pain in the butt than I wish to
   hassle with. */

%{
#include <getopt.h>
#include <string.h>
#include "config.h"
#include "tagfile.h"
  
char *tagfile = 0;
char *tagpath = 0;
char *curclass = 0;
int  want_curclass = 0;
 
void output_line_start(int n);

#define end_line() \
    do { \
       printf("<br>"); \
       cur_line++; \
       output_line_start(cur_line); \
    } while(0)
 
#ifndef output
#define output(x) fputc(x, yyout);
#endif

#undef yyunput
void print_func_def(char *);
void print_ident(char * ident, int is_member);
void setcurclass(const char *);
%}

KWD ((if)|(else)|(for)|(while)|(do)|(return)|(void))
TYPE ((struct)|(enum)|(class)|(typedef)|(public)|(private)|(protected))
IDENT [a-zA-Z_][a-zA-Z0-9_]*
     
%x COMMENT CPLUS_COMMENT STRING TYPENAME PREPROC PRE2 INCFILE
%%
\/\*  { printf(begin_comment); ECHO; BEGIN(COMMENT); }
\/\/  { printf(begin_comment); ECHO; BEGIN(CPLUS_COMMENT); }
\"    { ECHO; printf(begin_string); BEGIN(STRING); }
{KWD} { printf(begin_kwd); ECHO; printf(end_kwd); }
^{TYPE} { want_curclass = 1;
          printf(begin_tkwd); ECHO; printf(end_tkwd); BEGIN(TYPENAME); }
{TYPE} { want_curclass = 0;
         printf(begin_tkwd); ECHO; printf(end_tkwd); BEGIN(TYPENAME); }
^({IDENT}::)*{IDENT}/[ \t]*\( {
  print_func_def(yytext);
}
^{IDENT}::operator[ \t]+((new)|(delete))/[ \t]*\( {
  print_func_def(yytext);
}

{IDENT}/[ \t]*:: { if (tagpath && etags_find_class(yytext)) {
                printf("<a href=\"%sTAGS#class_name_%s\">",
		       tagpath, yytext);
		ECHO;
                printf("</a>");
             }
             else
	       ECHO;
           } 
\.{IDENT} {
  printf(".");print_ident(&yytext[1], 1);
}
-\>{IDENT} {
  printf("-&gt;");print_ident(&yytext[2], 1);
}
({IDENT}::)*{IDENT} {
  print_ident(yytext,0);
}
::{IDENT} {
  print_ident(yytext,0);
}
{IDENT} { ECHO; }

^#     { printf(begin_preproc); ECHO; BEGIN(PREPROC); }

<COMMENT>\n   { printf(end_comment); end_line(); printf(begin_comment); }

<COMMENT>\*\/ { ECHO; printf(end_comment); BEGIN(0); }
<CPLUS_COMMENT>\n { printf(end_comment); end_line(); BEGIN(0); }
<COMMENT>legal\/license\/EROS\.html { printf("<a href=\"%s/%s\">%s</a>",
				 EROS_DOC_WEB, yytext,
				 yytext); }
<CPLUS_COMMENT>legal\/license\/EROS\.html { printf("<a href=\"%s/%s\">%s</a>",
				 EROS_DOC_WEB, yytext,
				 yytext); }

<STRING>\"  { printf(end_string); ECHO; BEGIN(0); }
<STRING>[^\\]\\\" { ECHO; }

<TYPENAME>{IDENT}/[ \t]*[:{] {
  if (want_curclass) setcurclass(yytext);
  printf(begin_type); ECHO; printf(end_type); BEGIN(0);
}
<TYPENAME>{IDENT} { printf(begin_type); ECHO; printf(end_type); BEGIN(0); }
<TYPENAME>[ \t]  { printf("&nbsp;"); }
<TYPENAME>:      { BEGIN(0); REJECT; }
<TYPENAME>.      { BEGIN(0); REJECT; }

<PREPROC>include { ECHO; BEGIN(PRE2); }
<PREPROC>define  { ECHO; BEGIN(PRE2); }
<PREPROC>ifdef   { ECHO; BEGIN(PRE2); }
<PREPROC>ifndef  { ECHO; BEGIN(PRE2); }
<PREPROC>{IDENT} { ECHO; printf(end_preproc); BEGIN(0); }

<PRE2>\n      { printf(end_preproc); end_line(); BEGIN(0); }
<PRE2>{IDENT} { printf(end_preproc);
                printf(begin_macro); ECHO; printf(end_macro);
		BEGIN(0); }
<PRE2>\"      { printf(end_preproc); ECHO;
                printf(begin_incfile); BEGIN(INCFILE); }
<PRE2>\<      { printf(end_preproc); printf("&lt;");
                printf(begin_incfile); BEGIN(INCFILE); }

<INCFILE>\\\> { printf("\\&gt;"); }
<INCFILE>\\\" { printf("\\\""); }
<INCFILE>\>  { printf(end_incfile); printf("&gt;"); BEGIN(0); }
<INCFILE>\"  { printf(end_incfile); ECHO; BEGIN(0); }

<*>\<     { printf("&lt;"); }
<*>\>     { printf("&gt;"); }
<*>&     { printf("&amp;"); }
<*>" "   { printf("&nbsp;"); }
<*>\n   { end_line(); }
<*>.    { ECHO; }

%%
void
setcurclass(const char *name)
{
  if (curclass)
    free(curclass);

  if (name == 0) {
    curclass = 0;
    return;
  }

  curclass = malloc(strlen(name) + 1);
  strcpy(curclass, name);
}

void
print_func_def(char *name)
{
  char *colon_pos = strrchr(name, ':');
  char *residual = name;

  if (colon_pos) {
    residual = colon_pos + 1;
    colon_pos--;		/* back up to first colon */

    *colon_pos = 0;
  }

  printf(begin_func);

  if (tagpath && colon_pos && etags_find_class(name)) {
    printf("<a href=\"%sTAGS#class_name_%s\">%s",
	   tagpath, name, name);
    setcurclass(name);
    printf("</a>::");
  }
  else if (colon_pos){
    printf("%s::", name);
    setcurclass(0);
  }

  printf(residual);
  printf(end_func);
}

void
print_ident(char *name, int is_member)
{
  char *colon_pos = strrchr(name, ':');
  char *symname = name;
  const char *classname = 0;
  etags_entry **psym = 0;

  if (colon_pos) {
    symname = colon_pos + 1;
    colon_pos--;			/* back up to first colon */

    *colon_pos = 0;
    classname = name;
  }

  /* If this was a spefically-qualified GLOBAL name, then classname
     will be the empty string. Patch that here to defeat the search
     logic below: */
  if (classname && *classname == 0)
    classname = etags_globfun;

  if (tagpath) {
    if (classname) {
      /* Check only as a fully qualified name.  This covers explicitly 
	 global functions as well. */
      psym = etags_find_fn(classname, symname);
    }
    else if (is_member == 0) {
      /* If this is not a member function invocation, try to search
	   the parent class hierarchy of the current class (if any)
	   and its parent classes:
	 */
      const char *search_class = curclass;

      while (search_class && !psym) {
	psym = etags_find_fn(search_class, symname);
	search_class = etags_find_parent_class(search_class);
      }

      /* Failing that, attempt to find a global function of the
	 desired name: */
      if (psym == 0)
	psym = etags_find_fn(etags_globfun, symname);
    }
    else {
      /* Failing that, attempt to find any uniquely named
	 function whose name matches what was requested. This is an
	 incomplete solution, but it works in many cases because
	 overloading is less common than people think it is.

	 Also, heavily overloaded functions tend to be utility
	 functions, and experienced users do not want those
	 cross-linked anyway. */

      if (!psym)
	psym = etags_find_unique_fn(symname);
    }
  }
  
  if (psym) 
    printf("<a href=\"%s%s#%d\">",
	   tagpath,
	   (*psym)->filename, (*psym)->line);

  if (classname == 0)
    printf(symname);
  else if (classname == etags_globfun)
    printf("::%s", symname);
  else
    printf("%s::%s", classname, symname);

  if (psym)
    printf("</a>");
}

void output_line_start(int n)
{
  char strline[20];
  char *lnptr = strline;
  
  sprintf(strline, "% 5d", n);

  printf("<a name=\"%d\"></a>", cur_line);

  while (*lnptr == ' ') {
    printf("&nbsp;");
    lnptr++;
  }

  while (*lnptr)
    putchar(*lnptr++);
  
  printf("&nbsp;", n);
}

main(int argc, char *argv[])
{
  int c;
  int opterr = 0;
  extern int optind;
  extern char *optarg;

  while ((c = getopt(argc, argv, "t:p:")) != EOF) {
    switch(c) {
    case 't':
      tagfile = optarg;
      break;
    case 'p':
      tagpath = optarg;
      break;
    default:
      opterr++;
      break;
    }
  }
    
  if (opterr) {
    fprintf(stderr, "Usage: %s [ -t tagfile -p tagpath ]\n", argv[0]);
    exit(1);
  }
    
  fprintf(stderr, "Loading tagfile \"%s\"\n", tagfile);
  
  if (tagfile)
    etags_load(tagfile);
  else
    tagfile = tagpath = 0;

  printf("<tt>");
  output_line_start(1);

  yylex();

  printf("</tt>");
}

int yywrap()
{
  return 1;
}
