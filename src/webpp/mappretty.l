/* -*- Mode: c -*- */
/*
 * Copyright 1998 by Jonathan Shapiro.
 *
 * This file is part of EROS, the Extremely Reliable Operating System.
 * EROS is NOT free software, but may be redistributed under the terms of
 * the EROS License Agreement.
 *
 * A copy of the EROS License Agreement has been included with the
 * distribution, and may be found in the file legal/license/EROS.html.
 */

/* This translator assumes that the input has been run through the
   expand program, so that there are not tabs.  Keeping track of input 
   position is otherwise more of a pain in the butt than I wish to
   hassle with. */

%{
#include "config.h"

#ifndef output
#define output(x) fputc(x, yyout);
#endif

#undef yyunput
%}

KWD ((hide)|(at)|(with)|(run)|(new)|(pc)|(sp)|(space)|(priority)|(misc)|(subseg)|(pages)|(small)|(segtree)|(segment)|(program)|(symbol)|(empty)|(brand)|(start)|(domain)|(node)|(page)|(number)|(blss)|(as)|(key)|(reg)|(ro)|(weak)|(nc))
IDENT [a-zA-Z_][\.a-zA-Z0-9_]*
     
%x COMMENT CPLUS_COMMENT STRING PREPROC PRE2 INCFILE
%%
\/\*  { printf(begin_comment); ECHO; BEGIN(COMMENT); }
\/\/  { printf(begin_comment); ECHO; BEGIN(CPLUS_COMMENT); }
\"    { ECHO; printf(begin_string); BEGIN(STRING); }
{KWD} { printf(begin_kwd); ECHO; printf(end_kwd); }
^{IDENT}/[ \t]*=     { printf(begin_func); ECHO; printf(end_func); }
{IDENT} { ECHO; }

^#     { printf(begin_preproc); ECHO; BEGIN(PREPROC); }

<COMMENT>\*\/ { ECHO; printf(end_comment); BEGIN(0); }
<CPLUS_COMMENT>\n { printf(end_comment); printf("<br>"); BEGIN(0); }
<COMMENT>legal\/license\/EROS\.html { printf("<a href=\"%s/%s\">%s</a>",
				 EROS_DOC_WEB, yytext,
				 yytext); }
<CPLUS_COMMENT>legal\/license\/EROS\.html { printf("<a href=\"%s/%s\">%s</a>",
				 EROS_DOC_WEB, yytext,
				 yytext); }

<STRING>\"  { printf(end_string); ECHO; BEGIN(0); }
<STRING>[^\\]\\\" { ECHO; }

<PREPROC>" "     { printf("&nbsp;"); }
<PREPROC>include { ECHO; BEGIN(PRE2); }
<PREPROC>define  { ECHO; BEGIN(PRE2); }
<PREPROC>{IDENT} { ECHO; printf(end_preproc); BEGIN(0); }

<PRE2>\n      { printf(end_preproc); printf("<br>"); BEGIN(0); }
<PRE2>{IDENT} { printf(end_preproc);
                printf(begin_macro); ECHO; printf(end_macro);
		BEGIN(0); }
<PRE2>\"      { printf(end_preproc); ECHO;
                printf(begin_incfile); BEGIN(INCFILE); }
<PRE2>\<      { printf(end_preproc); printf("&lt;");
                printf(begin_incfile); BEGIN(INCFILE); }

<INCFILE>\\\> { printf("\\&gt;"); }
<INCFILE>\\\" { printf("\\\""); }
<INCFILE>\>  { printf(end_incfile); printf("&gt;"); BEGIN(0); }
<INCFILE>\"  { printf(end_incfile); ECHO; BEGIN(0); }

<*>\<     { printf("&lt;"); }
<*>\>     { printf("&gt;"); }
<*>&     { printf("&amp;"); }
<*>" "   { printf("&nbsp;"); }
<*>\n   { printf("<br>"); ECHO; }
<*>.    { ECHO; }

%%
main(int argc, char *argv[])
{
  int c;
  int opterr = 0;
  extern int optind;
  extern char *optarg;

  while ((c = getopt(argc, argv, "")) != EOF) {
    switch(c) {
    default:
      opterr++;
      break;
    }
  }
    
  if (opterr) {
    fprintf(stderr, "Usage: %s\n", argv[0]);
    exit(1);
  }
    
  printf("<tt>");
  yylex();
  printf("</tt>");
}

int yywrap()
{
  return 1;
}
