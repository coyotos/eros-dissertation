/* -*- Mode: c -*- */
/*
 * Copyright 1998 by Jonathan Shapiro.
 *
 * This file is part of EROS, the Extremely Reliable Operating System.
 * EROS is NOT free software, but may be redistributed under the terms of
 * the EROS License Agreement.
 *
 * A copy of the EROS License Agreement is included with the documentation
 * distribution, and may be found in the file legal/license/EROS.html.
 */

/* This translator assumes that the input has been run through the
   expand program, so that there are not tabs.  Keeping track of input 
   position is otherwise more of a pain in the butt than I wish to
   hassle with. */

%{
#include "config.h"
 
#ifndef output
#define output(x) fputc(x, yyout);
#endif

#undef yyunput
%}

KWD ((if)|(then)|(else)|(elif)|(fi)|(case)|(esac)|(exit)|(for)|(do)|(done)|(set))
IDENT [a-zA-Z_][a-zA-Z0-9_]*
     
%x EOL_COMMENT FUNCNAME STRING SQSTRING
%%
#     { printf(begin_comment); ECHO; BEGIN(EOL_COMMENT); }
{KWD} { printf(begin_kwd); ECHO; printf(end_kwd); }
\"    { ECHO; printf(begin_string); BEGIN(STRING); }
\'    { ECHO; printf(begin_string); BEGIN(SQSTRING); }
^{IDENT}/\(     { printf(begin_func); ECHO; printf(end_func); }
{IDENT} { ECHO; }

<EOL_COMMENT>legal\/license\/EROS\.html { printf("<a href=\"%s/%s\">%s</a>",
					      EROS_DOC_WEB, yytext,
					      yytext); }
<EOL_COMMENT>\n { printf(end_comment); printf("<br>"); BEGIN(0); }

<STRING>\"  { printf(end_string); ECHO; BEGIN(0); }
<STRING>[^\\]\\\" { ECHO; }

<SQSTRING>\'  { printf(end_string); ECHO; BEGIN(0); }
<SQSTRING>[^\\]\\\' { ECHO; }

<*>\<     { printf("&lt;"); }
<*>\>     { printf("&gt;"); }
<*>&     { printf("&amp;"); }
<*>" "   { printf("&nbsp;"); }
<*>\n   { printf("<br>"); ECHO; }
<*>.    { ECHO; }

%%
main(int argc, char *argv[])
{
  int c;
  int opterr = 0;
  extern int optind;
  extern char *optarg;

  while ((c = getopt(argc, argv, "")) != EOF) {
    switch(c) {
    default:
      opterr++;
      break;
    }
  }
    
  if (opterr) {
    fprintf(stderr, "Usage: %s\n", argv[0]);
    exit(1);
  }
    
  printf("<tt>");
  yylex();
  printf("</tt>");
}

int yywrap()
{
  return 1;
}
