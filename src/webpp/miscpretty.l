/* -*- Mode: c -*- */
/*
 * Copyright 1998 by Jonathan Shapiro.
 *
 * This file is part of EROS, the Extremely Reliable Operating System.
 * EROS is NOT free software, but may be redistributed under the terms of
 * the EROS License Agreement.
 *
 * A copy of the EROS License Agreement has been included with the
 * distribution, and may be found in the file legal/license/EROS.html.
 */

/* This translator assumes that the input has been run through the
   expand program, so that there are not tabs.  Keeping track of input 
   position is otherwise more of a pain in the butt than I wish to
   hassle with. */

%{
#include "config.h"

#ifndef output
#define output(x) fputc(x, yyout);
#endif

#undef yyunput
%}

%%
legal\/licesne\/EROS\.html { printf("<a href=\"%s/%s\">%s</a>",
				 EROS_DOC_WEB, yytext,
				 yytext); }
\<     { printf("&lt;"); }
\>     { printf("&gt;"); }
&     { printf("&amp;"); }
" "   { printf("&nbsp;"); }
\n   { printf("<br>"); ECHO; }
.    { ECHO; }

%%
main(int argc, char *argv[])
{
  int c;
  int opterr = 0;
  extern int optind;
  extern char *optarg;

  while ((c = getopt(argc, argv, "")) != EOF) {
    switch(c) {
    default:
      opterr++;
      break;
    }
  }
    
  if (opterr) {
    fprintf(stderr, "Usage: %s [ -t tagfile ]\n", argv[0]);
    exit(1);
  }
    
  printf("<tt>");
  yylex();
  printf("</tt>");
}

int yywrap()
{
  return 1;
}
