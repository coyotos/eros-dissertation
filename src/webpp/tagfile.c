/* -*- Mode: c -*- */
/*
 * Copyright 1998 by Jonathan Shapiro.
 *
 * This file is part of EROS, the Extremely Reliable Operating System.
 * EROS is NOT free software, but may be redistributed under the terms of
 * the EROS License Agreement.
 *
 * A copy of the EROS License Agreement has been included with the
 * distribution, and may be found in the file legal/license/EROS.html.
 */

/* Support routines to load in a tags file */
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifndef MAP_FAILED
/* Redhat 4.2 failed to define this... */
#define MAP_FAILED	((__ptr_t) -1)
#endif

#include "tagfile.h"

#ifndef EOF
#define EOF -1
#endif

static int tagfd = -1;

static char *filebuf;
static int filepos;
static int filesz = 0;

static char *curfile = 0;

const char * etags_globfun = "(global functions)";

typedef struct inherit_chain {
  const char *parent;
  const char *classname;
} inherit_chain;

static inherit_chain *inheritance_table = NULL;
static int ninherit = 0;
static int maxinherit = 0;
static inherit_sorted = 0;

typedef int (*qsortfn)(const void *, const void *);

static int
compare_inherit(const inherit_chain *i1, const inherit_chain *i2)
{
  int cmp;
  
  assert(i1->classname);
  assert(i2->classname);

  return strcmp(i1->classname, i2->classname);
}

const char *
etags_find_parent_class(const char *name)
{
  inherit_chain ic, *result;
  ic.classname = name;
  
  if (inherit_sorted == 0) {
    qsort(inheritance_table, ninherit, sizeof(inherit_chain),
		   (qsortfn) compare_inherit);
    inherit_sorted = 1;
  }

  result = bsearch(&ic, inheritance_table, ninherit,
		   sizeof(inherit_chain),
		   (qsortfn) compare_inherit);

  if (result == 0)
    return 0;
  
  return result->parent;
}

static void
add_inherit(char *classnm, char *parent)
{
#define IGROWTH_FACTOR 256
  
  if (ninherit == maxinherit) {
    maxinherit += IGROWTH_FACTOR;

    inheritance_table =
      (inherit_chain *) realloc(inheritance_table,
				sizeof(inherit_chain) * maxinherit);
  }
  
  inheritance_table[ninherit].classname = classnm;
  inheritance_table[ninherit].parent = parent;

  ninherit++;
  inherit_sorted = 0;
}

#define esyntax(s) \
    do { \
       printf("<br>tag file syntax error: " s); \
       return -1; \
    } while (0)

#define mhere()  ( (filepos < filesz) ? filebuf[filepos] : EOF)
#define mgetc()  ( (filepos < filesz) ? filebuf[filepos++] : EOF)
#define mungetc(c) do { filepos--; } while (0)
#define mskip(n) do { filepos += n; } while (0)
#define mlooking_at(s) (!strncmp(s, &filebuf[filepos], strlen(s)))
#define mischar(c) ( (filepos < filesz && filebuf[filepos] == c ) || \
                     (filepos >= filesz && c == EOF) )
#define mnullify()  do { filebuf[filepos++] = 0; } while (0)
#define mskipto(c) while (mgetc() != c)

char *
mextract(char c)
{
  char *here = &filebuf[filepos];

  while (filebuf[filepos++] != c)
    ;

  filebuf[filepos-1] = 0;

  return here;
}

etags_entry **etags_ci_sym_index = 0;
etags_entry **etags_ci_qual_index = 0;
etags_entry **etags_qual_index = 0;
etags_entry **etags_sym_index = 0;

static etags_entry *etags_sym_table = NULL;

int etags_nsym = 0;
static int maxsym = 0;

static void
add_entry(char *symname, char *filename, int line)
{
  char *colon_pos;

  /* certain special cases not to add: */
  if (strcmp(symname, "sizeof") == 0)
    return;
  
#define GROWTH_FACTOR 256
  
  if (etags_nsym == maxsym) {
    maxsym += GROWTH_FACTOR;

    etags_sym_table =
      (etags_entry *) realloc(etags_sym_table, sizeof(etags_entry) * maxsym);
  }

  etags_sym_table[etags_nsym].classname = etags_globfun;
  etags_sym_table[etags_nsym].symname = symname;
  etags_sym_table[etags_nsym].filename = filename;
  etags_sym_table[etags_nsym].line = line;

  if (colon_pos = strrchr(symname, ':')) {
    /* colon_pos has position of SECOND colon */
    etags_sym_table[etags_nsym].symname = colon_pos + 1;

    colon_pos--;		/* now points to FIRST colon */
    etags_sym_table[etags_nsym].classname = symname;
    *colon_pos = 0;
  }

  etags_nsym++;
}

/******************************************
 * Sorting functions
 ******************************************/

static int
compare_ci_by_symname(const etags_entry **e1, const etags_entry **e2)
{
  int cmp;
  
  assert((*e1)->classname);
  assert((*e2)->classname);

  cmp = strcasecmp((*e1)->symname, (*e2)->symname);
  if (cmp)
    return cmp;

  return strcasecmp((*e1)->classname, (*e2)->classname);
}

static int
compare_by_symname(const etags_entry **e1, const etags_entry **e2)
{
  int cmp;
  
  assert((*e1)->classname);
  assert((*e2)->classname);

  cmp = strcmp((*e1)->symname, (*e2)->symname);
  if (cmp)
    return cmp;

  return strcmp((*e1)->classname, (*e2)->classname);
}

static int
compare_ci_by_qualname(const etags_entry **e1, const etags_entry **e2)
{
  int cmp;
  
  assert((*e1)->classname);
  assert((*e2)->classname);

  cmp = strcasecmp((*e1)->classname, (*e2)->classname);
  if (cmp)
    return cmp;

  return strcasecmp((*e1)->symname, (*e2)->symname);
}

static int
compare_by_qualname(const etags_entry **e1, const etags_entry **e2)
{
  int cmp;
  
  assert((*e1)->classname);
  assert((*e2)->classname);

  cmp = strcmp((*e1)->classname, (*e2)->classname);
  if (cmp)
    return cmp;

  return strcmp((*e1)->symname, (*e2)->symname);
}

static int
compare_by_classname(const etags_entry **e1, const etags_entry **e2)
{
  int cmp;
  
  assert((*e1)->classname);
  assert((*e2)->classname);

  return strcmp((*e1)->classname, (*e2)->classname);
}

static int
compare_by_symonly(const etags_entry **e1, const etags_entry **e2)
{
  int cmp;
  
  assert((*e1)->symname);
  assert((*e2)->symname);

  return strcmp((*e1)->symname, (*e2)->symname);
}

/******************************************
 * Searching functions
 ******************************************/
etags_entry **
etags_find_class(const char *name)
{
  etags_entry se, *pse, **result;
  se.classname = name;
  pse = &se;

  result = bsearch(&pse, etags_qual_index, etags_nsym,
		   sizeof(etags_entry *),
		   (qsortfn) compare_by_classname);
  return result;
}

etags_entry **
etags_find_fn(const char *classname, const char *symname)
{
  etags_entry se, *pse, **result;
  se.classname = classname;
  se.symname = symname;
  pse = &se;

  result = bsearch(&pse, etags_qual_index, etags_nsym,
		   sizeof(etags_entry *),
		   (qsortfn) compare_by_qualname);

  return result;
}

etags_entry **
etags_find_unique_fn(const char *symname)
{
  etags_entry se, *pse, **result;
  se.symname = symname;
  pse = &se;

  result = bsearch(&pse, etags_sym_index, etags_nsym,
		   sizeof(etags_entry *),
		   (qsortfn) compare_by_symonly);

  if (result == 0)
    return result;
  
  if (result - etags_sym_index) {
    if (strcmp(result[-1]->symname, symname) == 0)
      return 0;
  }

  if (result - etags_sym_index < etags_nsym) {
    if (strcmp(result[1]->symname, symname) == 0)
      return 0;
  }

  /* Now we know it's a unique match... */
  return result;
}

int
etags_load(const char *fname)
{
  {
    struct stat tagfile_stat;
  
    if (stat(fname, &tagfile_stat) < 0) {
      printf("unable to stat tag file \"%s\"", fname);
      return -1;
    }

    filesz = tagfile_stat.st_size;
  }
  
  if ((tagfd = open(fname, O_RDONLY)) < 0) {
    printf("unable to open tag file \"%s\"", fname);
    return -1;
  }

  filebuf = mmap( 0, filesz, PROT_READ | PROT_WRITE,
		  MAP_PRIVATE, tagfd, 0 /* offset */);
  if (filebuf == MAP_FAILED) {
    printf("unable to mmap tag file \"%s\"", fname);
    return -1;
  }

  while (filepos < filesz) {
    int is_cplusplus = 0;

    if (mgetc() != '\f')
      esyntax("missing formfeed");

    if (mgetc() != '\n')
      esyntax("missing linefeed");

    /* Looking at a file name.  Extract it for future use */
    curfile = mextract(',');
    mskipto('\n');

    {
      char *file_ext = strrchr(curfile, '.');
      if (file_ext && (
		       strcmp(file_ext, ".cxx") == 0 ||
		       strcmp(file_ext, ".cpp") == 0 ||
		       strcmp(file_ext, ".cc") == 0 ||
		       strcmp(file_ext, ".C")
		       ))
	is_cplusplus = 1;
    }
    
    /* Now processing symbols until we see another formfeed. */
    while ( !mischar('\f') && !mischar(EOF) ) {
      if (mlooking_at("enum") ||
	  mlooking_at("typedef") ||
	  mlooking_at("#define")) {
	mskipto('\n');
      }
      else if (mlooking_at("struct") ||
	  mlooking_at("class")) {
	/* stuff in braces relies on EROS-specific regexp */
	{
	  mskipto('\177');

	  if (mischar(':')) {
	    char *child;
	    char *parent;
	    
	    assert(mlooking_at(":public "));
	    mskipto(' ');

	    child = mextract(' ');
	    parent = mextract('\001');

	    add_inherit(child, parent);
	  }
	}
	mskipto('\n');
      }
      /* following is EROS-specific asm hack. */
      else if (mlooking_at("ENTRY")||
	       mlooking_at("GEXT")) {
	char *fn_line_str;
	char *fn_name;
	int fn_line;
	extern char *cplus_demangle(char *, int);
	
	mskipto('(');
	fn_name = mextract(')');
	mskipto('\177');

	fn_line_str = mextract(',');
	fn_line = strtol(fn_line_str, 0, 0);

#if 1
	{
	  char *demangled_name = cplus_demangle(fn_name, 0);
	  if (demangled_name)
	    fn_name = demangled_name;
	}
#endif
	add_entry(fn_name, curfile, fn_line);
	mskipto('\n');
      }
      else {
	char *sym_spec;
	int len;
	
	char *fn_line_str;
	char *fn_name;
	int fn_line;
	
	sym_spec = mextract('\177'); /* pull whole regexp */
	
	len = strlen(sym_spec);

	if (sym_spec[len-1] == '(') {
	  /* If there is a supplemental regexp, we can recognize it from 
	     the fact that the next character is not a number.  Take the 
	     thing between the DEL and the ^A as the sym_spec
	     instead. */
	
	  if (!isdigit(mhere()) ) 
	    sym_spec = mextract('\001');
	
	  while (isspace(*sym_spec))
	    sym_spec++;

	  len = strlen(sym_spec);

	  fn_name = &sym_spec[len-1];

	  /* bash the closing '(' */
	  if (*fn_name == '(')
	    *fn_name-- = 0;

	  {
	    char *operator_name;
	    int is_desired_operator = 0;
	    
	    /* Now back up to the beginning of the symbol name: */
	    while (isalnum(*fn_name) ||
		   *fn_name == '_' ||
		   *fn_name == ':')
	      fn_name--;

	    if (is_cplusplus &&
		isspace(*fn_name)) {
	      if ((strcmp(fn_name+1, "delete") == 0) ||
		  (strcmp(fn_name+1, "new") == 0))
		is_desired_operator = 1;

	      operator_name = fn_name;
	      fn_name++;
	      
	      while (isspace(*operator_name))
		operator_name--;

	      while (isalnum(*operator_name) ||
		     *operator_name == '_' ||
		     *operator_name == ':')
		operator_name--;

	      operator_name++;

	      if (strncmp(operator_name, "operator ",
			  strlen("operator ")) == 0) {
		if (is_desired_operator)
		  fn_name = operator_name;
		else
		  fn_name = 0;
	      }
	    }
	    else
	      fn_name++;
	  }
	
	  if (fn_name) {
	    fn_line_str = mextract(',');
	    fn_line = strtol(fn_line_str, 0, 0);

	    add_entry(fn_name, curfile, fn_line);
	  }
	}

	mskipto('\n');
      }
    }
  }      

  {
    int i;
    
    etags_ci_sym_index = malloc(sizeof(etags_entry *) * etags_nsym);
    etags_sym_index = malloc(sizeof(etags_entry *) * etags_nsym);
    etags_ci_qual_index = malloc(sizeof(etags_entry *) * etags_nsym);
    etags_qual_index = malloc(sizeof(etags_entry *) * etags_nsym);

    for (i = 0; i < etags_nsym; i++)
      etags_ci_sym_index[i] = &etags_sym_table[i];

    bcopy(etags_ci_sym_index, etags_sym_index,
	  sizeof(etags_entry *) * etags_nsym);
    bcopy(etags_ci_sym_index, etags_ci_qual_index,
	  sizeof(etags_entry *) * etags_nsym);

    bcopy(etags_ci_qual_index, etags_qual_index,
	  sizeof(etags_entry *) * etags_nsym);

    qsort(etags_ci_qual_index, etags_nsym, sizeof(etags_entry *),
	  (qsortfn) compare_ci_by_qualname);
    qsort(etags_qual_index, etags_nsym, sizeof(etags_entry *),
	  (qsortfn) compare_by_qualname);
    qsort(etags_ci_sym_index, etags_nsym, sizeof(etags_entry *),
	  (qsortfn) compare_ci_by_symname);
    qsort(etags_sym_index, etags_nsym, sizeof(etags_entry *),
	  (qsortfn) compare_by_symname);
  }
  
  return 0;
}
