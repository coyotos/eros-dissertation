/* -*- Mode: c -*- */
/*
 * Copyright 1998 by Jonathan Shapiro.
 *
 * This file is part of EROS, the Extremely Reliable Operating System.
 * EROS is NOT free software, but may be redistributed under the terms of
 * the EROS License Agreement.
 *
 * A copy of the EROS License Agreement has been included with the
 * distribution, and may be found in the file legal/license/EROS.html.
 */

/* Turn the output of etags into a browsable listing. */

#include <stdio.h>
#include "tagfile.h"

/******************************************
 * Listing functions
 ******************************************/
void
generate_listing()
{
  int n_index, n_output, n_row;
  int pass;
  char letter = 0;
  const char *curclass = 0;
  int i;
  int have_classes = 0;
  
  /************************************
   * First output the letter index
   ************************************/

  printf("<h1>List of Functions</h1>");

  printf("<h2>Sorted by name:</h2><p><ul>");

  for (i = 0; i < etags_nsym; i++) {
    if (etags_ci_sym_index[i]->classname != etags_globfun)
      have_classes = 1;
    
    if (toupper(*etags_ci_sym_index[i]->symname) != letter) {
      if (letter != 0)
	printf("&nbsp;&nbsp;");
      letter = toupper(*etags_ci_sym_index[i]->symname);
      
      if (letter == '_')
	printf("<a href=\"#fn_name_uscore\">&lt;underscore&gt;</a>");
      else
	printf("<a href=\"#fn_name_%c\">%c</a>", letter, letter);
    }
  }

  printf("</ul></p>");

  if (have_classes) {
    /*************************************************************
     * Make a pass counting the number of class table entries:
   *************************************************************/

    curclass = 0;
  
    for (i = 0, n_index = 0, n_output = 0; i < etags_nsym; i++) {
      if (curclass && strcmp(curclass, etags_ci_qual_index[i]->classname) == 0)
	continue;

      curclass = etags_ci_qual_index[i]->classname;

#if 0
      printf("<br><a href=\"#class_name_%s\">%s</a>",
	     curclass ? curclass : "global_fns",
	     curclass ? curclass : "(global functions)");
#endif

      n_index++;
    }
  
    curclass = 0;
  
    n_row = (n_index + 3) / 4;

    /*************************************************************
   * Output the class name table
   *************************************************************/

    printf("<h2>Sorted by class:</h2>");

    printf("<p><ul><table>");
    for (pass = 0; pass < n_row; pass++) {
      printf("<tr>");
      for (i = 0, n_index = 0; i < etags_nsym; i++) {
	int row;
      
	if (curclass && strcmp(curclass, etags_ci_qual_index[i]->classname) == 0)
	  continue;

	curclass = etags_ci_qual_index[i]->classname;
    
	if (n_index % n_row == pass) {
	  printf("<td><a href=\"#class_name_%s\">%s</a></td>",
		 curclass != etags_globfun ? curclass : "global_fns", curclass);
	}
      
	n_index++;
      }
      printf("</tr>");
    }
  
    printf("</table></ul></p>");

  }
  /*************************************************************
   * Output the functions by first letter
   *************************************************************/

  printf("<h1>Functions Listed by Name</h1>");

  letter = 0;
  
  for (i = 0; i < etags_nsym; i++) {
    if (toupper(*etags_ci_sym_index[i]->symname) != letter) {
      if (letter != 0)
	printf("</ul></p>");

      letter = toupper(*etags_ci_sym_index[i]->symname);

      if (letter == '_')
	printf("<h2><a name=\"fn_name_uscore\">(underscore)</h2><p><ul>");
      else
	printf("<h2><a name=\"fn_name_%c\">%c</h2><p><ul>",
	       letter, letter);
    }
    else
      printf("<br>");

    printf("<a href=\"%s#%d\">%s</a>",
	   etags_ci_sym_index[i]->filename, etags_ci_sym_index[i]->line,
	   etags_ci_sym_index[i]->symname);

    if (etags_ci_sym_index[i]->classname != etags_globfun)
      printf("&nbsp;(class <a href=\"#class_name_%s\">%s</a>)",
	     etags_ci_sym_index[i]->classname,
	     etags_ci_sym_index[i]->classname);
  }

  printf("</ul></p>");

  if (have_classes) {
    /*************************************************************
     * Output the functions by class
     *************************************************************/

    printf("<h1>Functions Listed by Containing Class</h1>");

    curclass = 0;
  
    for (i = 0; i < etags_nsym; i++) {
      if (!curclass ||
	  strcmp(etags_ci_qual_index[i]->classname, curclass) != 0) {
	if (curclass)
	  printf("</ul></p>");

	curclass = etags_ci_qual_index[i]->classname;

	printf("<h2><a name=\"class_name_%s\">%s</h2><p><ul>",
	       curclass != etags_globfun ? curclass : "global_fns", curclass);
      }
      else
	printf("<br>");

      printf("<a href=\"%s#%d\">", etags_ci_qual_index[i]->filename,
	     etags_ci_qual_index[i]->line);

      if (curclass != etags_globfun)
	printf("%s::", curclass);
    
      printf("%s()</a>", etags_ci_qual_index[i]->symname);
    }

    printf("</ul></p>");
  }
}


main(int argc, char *argv[])
{
  if (etags_load(argv[1]) >= 0) {
    generate_listing();
    exit(0);
  }
  else
    exit (1);			/* lazy man's cleanup code */
}

