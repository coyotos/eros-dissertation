#
# Copyright (C) 1998, 1999, Jonathan S. Shapiro.
#
# This file is part of the EROS Operating System.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#


unexport DIRS
unexport ETAGDIRS
unexport GENERATED
unexport CLEANLIST

ifndef GCCWARN
include $(EROS_SRC)/build/lib/make/makevars.mk
endif

# The following variables depend on things set in the makefile:
GCCFLAGS=$(CFLAGS) $(OPTIM) $(INC) $(DEF)
GPLUSFLAGS=-fdefault-inline -fno-implicit-templates $(OPTIM) $(GPLUS_OPTIM) $(INC) $(DEF)

ifndef CLEANDIRS
CLEANDIRS=$(DIRS)
endif


SEDHACK=sed 's,'$*'\.o[ :]*,'$@' &,g'
DEP_SEDHACK=sed 's,^[^:]*:[:]*,'$@' &,g'
#DEP_SEDHACK=sed 's,'$*'\.o[ :]*,'$@' &,g'

C_DEP=@$(GCC) $(GCCFLAGS) $(GCCWARN) -E -M $< | $(DEP_SEDHACK) > $@
CXX_DEP=@$(GPLUS) $(GPLUSFLAGS) $(GPLUSWARN) -E -M $< | $(DEP_SEDHACK) > $@
ASM_DEP=@$(GPLUS) $(GCCFLAGS) -E -M $< | $(DEP_SEDHACK) > $@

C_BUILD=$(GCC) $(GCCFLAGS) $(GCCWARN) -c $<
CXX_BUILD=$(GPLUS) $(GPLUSFLAGS) $(GPLUSWARN) -c $<
ASM_BUILD=$(GPLUS) $(GCCFLAGS) -c $< -o $@
#
# OLD autodependency rules -- retained for the time being.
#

# use GPLUS to allow C++ style comments:
#.S.m:
#	@$(GPLUS) $(GCCFLAGS) -E -M $< | $(SEDHACK) > $@
#
#.s.m:
#	@echo "Please rewrite .s file as .S file"
#
#.c.m:
#	@$(GCC) $(GCCFLAGS) $(GCCWARN) -E -M $< | $(SEDHACK) > $@
#
#
#.cxx.m:
#	@$(GPLUS) $(GPLUSFLAGS) $(GPLUSWARN) -E -M $< | $(SEDHACK) > $@


.%.m: %.S
	@$(GPLUS) $(GCCFLAGS) -E -M $< | $(SEDHACK) > $@

.%.m: %.s
	@echo "Please rewrite .s file as .S file"

.%.m: %.c
	@$(GCC) $(GCCFLAGS) -E -M $< | $(SEDHACK) > $@


.%.m: %.cxx
	@$(GPLUS) $(GPLUSFLAGS) $(GPLUSWARN) -E -M $< | $(SEDHACK) > $@

#
# Object construction rules
#

.S.o:
	$(GPLUS) $(GCCFLAGS) -c $< -o $@

.c.o:
	$(GCC) $(GCCFLAGS) $(GCCWARN) -c $<

.cxx.o:
	$(GPLUS) $(GPLUSFLAGS) $(GPLUSWARN) -c $<

.ltx.dvi:
	latex $<
	latex $<

.fig.gif:
	@if [ \! -x $(NETPBMDIR)/ppmtogif ]; then\
		echo "ERROR: Please set the NETPBMDIR environment variable to identify"; \
		echo "       the location of the NetPBM directory."; \
		exit 1;\
	fi
	fig2dev -L ppm $< $*.ppm
	$(NETPBMDIR)/ppmtogif -t '#ffffff' $*.ppm > $@
	-rm -f $*.ppm

.PHONEY : depend nodepend recursive-depend all recursive-all install recursive-install clobber do-clobber recursive-clobber walk clean do-clean recursive-clean

depend: recursive-depend DEPEND
recursive-depend:
ifneq "$(DIRS)" ""
	@for i in $(DIRS); do \
		if [ -d "$$i" ]; then\
			$(MAKE) -C $$i $(MAKERULES) depend; \
			if [ $$? -ne 0 ]; then\
				echo "*** RECURSIVE BUILD STOPS ***";\
				exit 1;\
			fi; \
		fi; \
	done
endif

all: recursive-all
recursive-all:
ifneq "$(DIRS)" ""
	@for i in $(DIRS); do \
		if [ -d "$$i" ]; then\
			$(MAKE) -C $$i $(MAKERULES) all; \
			if [ $$? -ne 0 ]; then\
				echo "*** RECURSIVE BUILD STOPS ***";\
				exit 1;\
			fi; \
		fi; \
	done
endif

install: recursive-install
recursive-install:
ifneq "$(DIRS)" ""
	@for i in $(DIRS); do \
		if [ -d "$$i" ]; then\
			$(MAKE) -C $$i $(MAKERULES) install; \
			if [ $$? -ne 0 ]; then\
				echo "*** RECURSIVE BUILD STOPS ***";\
				exit 1;\
			fi; \
		fi; \
	done
endif

# The following hack fixes up directory dependencies, and ALSO ensures
# that the .m files will be rebuilt when appropriate:
DEPEND: $(patsubst %.o,.%.m,$(OBJECTS))
#	@if [ -n "$(OBJECTS:.o=.m)" ]; then\
#		cat *.m | sed "s, $(EROS_ROOT), \$$(EROS_ROOT),g" > DEPEND; \
#	fi
#	@touch DEPEND    # in case no .m files
#	@-rm -f *.m



#local-depend: 
#	@sed -e '/^### Output of make depend:/,$$d' Makefile > new.Makefile
#	@echo "### Output of make depend:" >> new.Makefile
#	@if [ -n "$(OBJECTS:.o=.m)" ]; then\
#		cat *.m | sed "s, $(EROS_ROOT), \$$(EROS_ROOT),g" >> new.Makefile; \
#	fi
#	@-rm -f *.m
#	@mv Makefile old.Makefile
#	@mv new.Makefile Makefile
#	@-rm -f old.Makefile

all install: DEPEND

nodepend:
	-find . -name '*.m' -exec rm {} \;
	-find . -name '.*.m' -exec rm {} \;
	-find . -name 'DEPEND' -exec rm {} \;

## The following piece of idiocy works around the fact that the
## autodependency files may refer to stuff that has been cleaned,
## and that this can therefore perversely cause the clean target to
## fail.
clean: nodepend
	$(MAKE) $(MAKERULES) do-clean

do-clean: recursive-clean
	-rm -f *.o *.m core *~ new.Makefile  ".#"*
	-rm -f .*.m sysgen.map
	-rm -f *.dvi *.blg *.aux *.log *.toc $(CLEANLIST)

recursive-clean:
ifneq "$(CLEANDIRS)" ""
	@for i in $(CLEANDIRS); do \
		if [ -d "$$i" ]; then\
			$(MAKE) -C $$i $(MAKERULES) do-clean; \
			if [ $$? -ne 0 ]; then\
				echo "*** RECURSIVE BUILD STOPS ***";\
				exit 1;\
			fi; \
		fi; \
	done
endif

## The following piece of idiocy works around the fact that the
## autodependency files may refer to stuff that has been clobbered,
## and that this can therefore perversely cause the clobber target to
## fail.
clobber: nodepend
	$(MAKE) $(MAKERULES) do-clobber

do-clobber: recursive-clobber
	-rm -f *.o *.m core *~ new.Makefile  ".#"*
	-rm -f .*.m sysgen.map $(TARGETS)
	-rm -f *.dvi *.blg *.aux *.log *.toc $(CLEANLIST)

recursive-clobber:
ifneq "$(CLEANDIRS)" ""
	@for i in $(CLEANDIRS); do \
		if [ -d "$$i" ]; then\
			$(MAKE) -C $$i $(MAKERULES) do-clobber; \
			if [ $$? -ne 0 ]; then\
				echo "*** RECURSIVE BUILD STOPS ***";\
				exit 1;\
			fi; \
		fi; \
	done
endif

ifdef ETAGDIRS
ETAGEXPAND=$(patsubst %,%/*.c,$(ETAGDIRS))
ETAGEXPAND+=$(patsubst %,%/*.cxx,$(ETAGDIRS))
ETAGEXPAND+=$(patsubst %,%/*.hxx,$(ETAGDIRS))
ETAGEXPAND+=$(patsubst %,%/*.h,$(ETAGDIRS))

ASM_ETAGEXPAND+=$(patsubst %,%/*.S,$(ETAGDIRS))

ETAGFILES=$(wildcard $(ETAGEXPAND))
ASM_ETAGFILES=$(wildcard $(ASM_ETAGEXPAND))
unexport ETAGEXPAND
unexport ETAGFILES
unexport ASM_ETAGEXPAND
unexport ASM_ETAGFILES

#	--regex='/^struct[ \t]+\([A-Za-z0-9_]+\)[ \t]*:[ \t]*public[ \t]+\([A-Za-z0-9_]+\)/:public \1 \2/' \
#	--regex='/^class[ \t]+\([A-Za-z0-9_]+\)[ \t]*:[ \t]*public[ \t]+\([A-Za-z0-9_]+\)/:public \1 \2/' \

tags: local-tags recursive-tags
local-tags:
	etags \
	--regex='/^struct[ \t]+\([A-Za-z0-9_]+\)[ \t]*:[ \t]*public[ \t]+\([A-Za-z0-9_]+\)/:public \1 \2/' \
	--regex='/^class[ \t]+\([A-Za-z0-9_]+\)[ \t]*:[ \t]*public[ \t]+\([A-Za-z0-9_]+\)/:public \1 \2/' \
	   $(ETAGFILES)
ifneq ($(ASM_ETAGFILES),)
	etags --append --lang=none \
		--regex='/^\(ENTRY\|GEXT\)([A-Za-z0-9_\.]+)/' \
		$(ASM_ETAGFILES)
endif
else
tags: recursive-tags
endif
recursive-tags:
ifneq "$(DIRS)" ""
	@for i in $(DIRS); do \
		if [ -d "$$i" ]; then\
			$(MAKE) -C $$i $(MAKERULES) tags; \
			if [ $$? -ne 0 ]; then\
				echo "*** RECURSIVE BUILD STOPS ***";\
				exit 1;\
			fi; \
		fi; \
	done
endif

generated: $(GENERATED) recursive-generated
recursive-generated:
ifneq "$(DIRS)" ""
	@for i in $(DIRS); do \
		if [ -d "$$i" ]; then\
			$(MAKE) -C $$i $(MAKERULES) generated; \
			if [ $$? -ne 0 ]; then\
				echo "*** RECURSIVE BUILD STOPS ***";\
				exit 1;\
			fi; \
		fi; \
	done
endif

# This is a debugging target..
walk:
ifneq "$(DIRS)" ""
	@for i in $(DIRS); do \
		$(MAKE) -C $$i $(MAKERULES) walk; \
		if [ $$? -ne 0 ]; then\
			echo "*** RECURSIVE BUILD STOPS ***";\
			exit 1;\
		fi; \
	done
endif
