#
# Copyright (C) 1998, 1999, Jonathan S. Shapiro.
#
# This file is part of the EROS Operating System.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#

# Cancel the old suffix list, because the order matters.  We want assembly 
# source to be recognized in preference to ordinary source code, so the
# .S, .s cases must come ahead of the rest.
.SUFFIXES:
.SUFFIXES: .S .s .cxx .c .y .l .o .dvi .ltx .gif .fig

#
# Set up default values for these variables so that a build in an improperly
# configured environment has a fighting chance:
#
ifndef EROS_TARGET
EROS_TARGET=i486
endif
ifndef EROS_ROOT
EROS_ROOT=$(HOME)/eros
endif
ifndef EROS_XENV
EROS_XENV=$(EROS_ROOT)/xenv
endif
ifndef EROS_CONFIG
EROS_CONFIG=DEFAULT
export EROS_CONFIG
endif

INSTALL=$(EROS_SRC)/build/lib/make/erosinstall

#
# First, set up variables for building native tools:
#
GAWK=gawk
PYTHON=python

NATIVE_STRIP=strip
NATIVE_OBJDUMP=objdump
NATIVE_SIZE=size
NATIVE_AR=ar
NATIVE_LD=ld
NATIVE_RANLIB=ranlib

NATIVE_GCC=gcc
ifndef GCCWARN
GCCWARN=-Wall -Winline -Werror -Wno-format
endif

NATIVE_GPLUS=g++
ifndef GPLUSWARN
GPLUSWARN=-Wall -Winline -Werror -Wno-format
endif

#
# Then variables for building EROS binaries:
#
EROS_GCC=$(NATIVE_GCC)
EROS_GPLUS=$(NATIVE_GPLUS)
EROS_LD=$(NATIVE_LD)
EROS_AR=$(NATIVE_AR)
EROS_RANLIB=$(NATIVE_RANLIB)
EROS_OBJDUMP=$(NATIVE_OBJDUMP)
EROS_STRIP=$(NATIVE_STRIP)
EROS_SIZE=$(NATIVE_SIZE)

#
# Then variables related to installation and test generation:
#

HOST_FD=/dev/fd0H1440 

###############################################################
#
# DANGER, WILL ROBINSON!!!!
#
# The EROS_HD environment variable is defined to something
# harmless here **intentionally**.  There are too many ways
# to do grievous injuries to a misconfigured host environment
# by setting a default.
#
# It is intended that the intrepid UNIX-based developer should
# pick a hard disk partition, set that up with lilo or some
# such, make it mode 666 from their UNIX environment, and
# then set EROS_HD to name that partition device file.  This
# is how *I* work, but you do this at your own risk!!
#
###############################################################
ifndef EROS_HD
EROS_HD=/dev/null
endif

#
# This is where the target environment makefile gets a chance to override
# things:
#
ifndef EROS_HOSTENV
EROS_HOSTENV=linux-xenv
endif

include $(EROS_SRC)/build/lib/make/$(EROS_HOSTENV).mk

# search for ppmtogif in all the obvious places:
ifndef NETPBMDIR
ifneq "" "$(findstring /usr/bin/ppmtogif,$(wildcard /usr/bin/*))"
NETPBMDIR=/usr/bin
endif
endif

ifndef NETPBMDIR
ifneq "" "$(findstring /usr/bin/X11/ppmtogif,$(wildcard /usr/bin/X11/*))"
NETPBMDIR=/usr/bin/X11
endif
endif

ifndef NETPBMDIR
ifneq "" "$(findstring /usr/local/netpbm/ppmtogif,$(wildcard /usr/local/netpbm/*))"
NETPBMDIR=/usr/local/netpbm
endif
endif

ifndef EROS_FD
EROS_FD=$(HOST_FD)
endif

#
# Now for the REALLY SLEAZY part: if this makefile is performing a
# cross build, smash the native tool variables with the cross tool 
# variables.  The clean thing to do would be to separate the rules 
# somehow, but this is quicker:
ifdef CROSS_BUILD
GCC=$(EROS_GCC)
GPLUS=$(EROS_GPLUS)
LD=$(EROS_LD)
AR=$(EROS_AR)
OBJDUMP=$(EROS_OBJDUMP)
SIZE=$(EROS_SIZE)
STRIP=$(EROS_STRIP)
RANLIB=$(EROS_RANLIB)
GPLUS_OPTIM=$(EROS_GPLUS_OPTIM)
endif
ifndef CROSS_BUILD
GCC=$(NATIVE_GCC)
GPLUS=$(NATIVE_GPLUS)
LD=$(NATIVE_LD)
AR=$(NATIVE_AR)
OBJDUMP=$(NATIVE_OBJDUMP)
SIZE=$(NATIVE_SIZE)
STRIP=$(NATIVE_STRIP)
RANLIB=$(NATIVE_RANLIB)
GPLUS_OPTIM=$(NATIVE_GPLUS_OPTIM)
endif

#
# in all the places where this is run, we actually want the environmental
# definitions set for the target environment.
#

EROS_CPP=$(EROS_GCC) -x c++ -E -undef -nostdinc -D$(EROS_TARGET)


DOMCRT0=$(EROS_ROOT)/lib/$(EROS_TARGET)/crt0.o
RUNCRT0=RUNCRT0_OBSOLETE
DOMLINKOPT=-N -Ttext 0x0 -nostdlib -static -e _start -L$(EROS_ROOT)/lib -L$(EROS_ROOT)/lib/$(EROS_TARGET)

DOMLINK=$(EROS_LD)

DOMLIB=$(EROS_ROOT)/lib/libdomdbg.a 
DOMLIB += $(EROS_ROOT)/lib/libkey.a 
DOMLIB += $(EROS_ROOT)/lib/libdomc.a
DOMLIB += $(EROS_ROOT)/lib/libdomgcc.a
DOMLIB += $(EROS_ROOT)/lib/$(EROS_TARGET)/libdomain.a
DOMLIB += $(EROS_ROOT)/lib/$(EROS_TARGET)/crtn.o
